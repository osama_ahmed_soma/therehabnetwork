<?php

header("Location: /dev/app/admin");
exit();

//Max size for execution and memory limit
ini_set ('max_execution_time', 150);
ini_set ('memory_limit', '128M');


$valid_passwords = array ("admin" => "myvirtualdoctor22");
$valid_users = array_keys($valid_passwords);

$user = $_SERVER['PHP_AUTH_USER'];
$pass = $_SERVER['PHP_AUTH_PW'];

$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

if (!$validated) {
	header('WWW-Authenticate: Basic realm="My Realm"');
	header('HTTP/1.0 401 Unauthorized');
	die ("Not authorized");
}


define( 'MVD_ADMIN', true );

//required files
require("includes/application_top.php");
if ( $do == 'ajax' ) {
	require_once 'pages/ajax.php';
	exit;
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo SITE_NAME; ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>AdminLTE/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>AdminLTE/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
		 folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>AdminLTE/dist/css/skins/_all-skins.min.css">

	<?php if ( $do == 'profile' ) { ?>
		<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>css/bootstrap-formhelpers.min.css">
	<?php } ?>

	<?php if ( $do == 'doctors' ) { ?>
		<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/dataTables.bootstrap.css">
		<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/fontawesome-iconpicker/fontawesome-iconpicker.min.css">
	<?php } ?>

	<?php if ( $do == 'doctors' && $sub_page == 'new' ) { ?>
		<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>css/bootstrap-formhelpers.min.css">
	<?php } ?>

	<?php if ( $do == 'users' && $sub_page == 'all' ) { ?>
		<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/dataTables.bootstrap.css">
	<?php } ?>

	<?php if ( $do == 'users' && $sub_page == 'new' ) { ?>
		<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>css/bootstrap-formhelpers.min.css">
	<?php } ?>

	<?php if ( $do == 'health_tips' && $sub_page == 'new' ) { ?>
		<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>css/bootstrap-formhelpers.min.css">
	<?php } ?>

	<?php if ( $do == 'health_tips' && $sub_page == 'all' ) { ?>
		<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/dataTables.bootstrap.css">
	<?php } ?>

	<?php if ( $do == 'seo' && $sub_page == 'new' ) { ?>
		<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>css/bootstrap-formhelpers.min.css">
	<?php } ?>

	<?php if ( $do == 'seo' && $sub_page == 'all' ) { ?>
		<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/dataTables.bootstrap.css">
	<?php } ?>

	<?php if ( $do == 'ratings' ) { ?>
		<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/dataTables.bootstrap.css">
	<?php } ?>


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- jQuery 2.1.4 -->
	<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js"></script>
</head>

<?php if( $do == "login" ) { ?>
	<?php require(SITE_ROOT.'pages/'.$do.'.php');?>

<?php } elseif( $do == "forgot_password" ) { ?>
	<?php require(SITE_ROOT.'pages/'.$do.'.php');?>

<?php } else { ?>

	<body class="hold-transition skin-blue sidebar-mini fixed">
	<!-- Site wrapper -->
	<div class="wrapper">

		<?php require_once "header.php"; ?>

		<!-- =============================================== -->

		<?php require_once "left_menu.php"; ?>

		<!-- =============================================== -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<?php require(SITE_ROOT . 'pages/' . $do . '.php'); ?>
		</div><!-- /.content-wrapper -->

		<?php require_once "footer.php"; ?>

		<?php require_once 'right_menu.php'; ?>

	</div><!-- ./wrapper -->

	<!-- Bootstrap 3.3.5 -->
	<script src="<?php echo HTTP_SERVER; ?>AdminLTE/bootstrap/js/bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?php echo HTTP_SERVER; ?>AdminLTE/dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?php echo HTTP_SERVER; ?>AdminLTE/dist/js/demo.js"></script>

	<?php if ( $do == 'profile' ) { ?>
		<script src="<?php echo HTTP_SERVER; ?>js/bootstrap-formhelpers.min.js"></script>
	<?php } ?>

	<?php if ( $do == 'doctors' ) { ?>
		<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/fontawesome-iconpicker/fontawesome-iconpicker.min.js"></script>
	<?php } ?>

	<?php if ( $do == 'doctors' && $sub_page == 'new' ) { ?>
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script src="<?php echo HTTP_SERVER; ?>js/bootstrap-formhelpers.min.js"></script>
	<?php } ?>

	<?php if ( $do == 'users' && $sub_page == 'all' ) { ?>
		<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<?php } ?>

	<?php if ( $do == 'health_tips' && $sub_page == 'all' ) { ?>
		<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<?php } ?>

	<?php if ( $do == 'seo' && $sub_page == 'all' ) { ?>
		<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<?php } ?>

	<?php if ( $do == 'health_tips' && $sub_page == 'new' ) { ?>
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script src="<?php echo HTTP_SERVER; ?>js/bootstrap-formhelpers.min.js"></script>
	<?php } ?>

	<?php if ( $do == 'seo' && $sub_page == 'new' ) { ?>
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script src="<?php echo HTTP_SERVER; ?>js/bootstrap-formhelpers.min.js"></script>
	<?php } ?>

	<?php if ( $do == 'users' && $sub_page == 'new' ) { ?>
		<script src="<?php echo HTTP_SERVER; ?>js/bootstrap-formhelpers.min.js"></script>
	<?php } ?>

	<?php if ( $do == 'ratings' && $sub_page == 'fields' ) { ?>
		<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo HTTP_SERVER; ?>AdminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<?php } ?>

	</body>
	</html>
	<?php
	}
	exit();
?>