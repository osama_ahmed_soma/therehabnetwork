<?php

define('MVD_SITE', true);

require("../includes/application_top.php");

$first_name = sanitize($_POST['first_name']);
$last_name = sanitize($_POST['last_name']);
$email_add = sanitize($_POST['email_add']);
$phone_number = sanitize($_POST['phone_number']);
$ip_address = $_SERVER['REMOTE_ADDR'];
$subject = 'My Virtual Doctor - Lander 1 Submission';

$url = HTTP_SERVER . 'app/email_api/lander?first_name=' . urlencode($first_name) . '&last_name=' . urlencode($last_name) . '&email_add=' . urlencode($email_add) . '&phone_number=' . urlencode($phone_number) . '&subject=' . urlencode($subject) . '&ip_address=' . urlencode($ip_address);

$curl = curl_init();
curl_setopt_array($curl, array(
	CURLOPT_RETURNTRANSFER => 1,
	CURLOPT_URL => $url,
	CURLOPT_USERAGENT => 'Core PHP'
));
$resp = curl_exec($curl);

if (!$resp) {
	die("Not sent . " . curl_error($curl));
	die;
}
curl_close($curl);

$json = json_decode($resp);

if (is_object($json) && isset($json->status) && $json->status !== 'sent') {
	$msg = "Not send";
} else {
	header('Location:index.php?status=success');
}