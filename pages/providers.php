
<script type="text/javascript">
    function calculateROI() {
        var numofproviders = $('#numofproviders').val();
        var expectedvisits = $('#expectedvisits').val();
        var averagecharge = $('#averagecharge').val();

        if (numofproviders == "" || expectedvisits == "" || averagecharge == "") {

            if (numofproviders == "") {
                $('#numofproviders').focus();
                return false;
            }
            if (expectedvisits == "") {
                $('#expectedvisits').focus();
                return false;
            }
            if (averagecharge == "") {
                $('#averagecharge').focus();
                return false;
            }

            return false;
        } else {

            var numofproviders = parseFloat($('#numofproviders').val());
            var expectedvisits = parseFloat($('#expectedvisits').val());
            var averagecharge = parseFloat($('#averagecharge').val());

            var result = numofproviders * expectedvisits * averagecharge * 260;
            $('#result').text("$" + result.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            //toogle element
            $("#collapseExample").hide();
            $("#collapseExample").toggle('slow');
        }


        //$('#result').text(result.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    }

    function resetField(fieldId) {
        var fieldValue = $('#' + fieldId).val();
        if (isNaN(fieldValue)) {
            $('#' + fieldId).val("");
            $('#' + fieldId).focus();
        }
    }


</script>
<section class="step-practice-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center practice-heading clearfix">
                <h1>For Providers <span>A Comprehensive Telehealth Platform That Will Improve Your Practice's Financial &amp; Operational Efficiency</span>
                </h1>
            </div>
            <div class="col-md-11 practice-sec clearfix">
                <div class="col-lg-5 col-md-6 col-sm-6 practice-moniter">
                    <img src="<?php echo HTTP_SERVER; ?>img/moniter.png" alt="" style="margin-top: 18px;">
                </div>
                <div class="col-lg-1 arrows-connect hidden visible-lg">
                    <img src="<?php echo HTTP_SERVER; ?>img/arrows-connect.png" alt="">
                </div>
                <div class="col-lg-6 pull-right  col-md-6 col-sm-12  col-xs-12">
                    <div class="col-md-12 col-sm-4 steps">
                        <div class="col-md-3 text-center">
                            <div class="step-icon"><i class="fa fa-thumbs-o-up"></i></div>

                        </div>
                        <div class="col-md-9 step-detail">
                            <div class="col-md-12 step-heading">Easy to Use</div>
                            <div class="col-md-12 step-list">
                                <li><i class="fa fa-check-circle"></i> Simple, streamlined, user-friendly interface</li>
                                <li><i class="fa fa-check-circle"></i> Accessible from any device with an internet
                                    connection
                                </li>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-4 steps">
                        <div class="col-md-3 text-center">
                            <div class="step-icon"><i class="fa fa-line-chart"></i></div>

                        </div>
                        <div class="col-md-9 step-detail">
                            <div class="col-md-12 step-heading">Efficient</div>
                            <div class="col-md-12 step-list">
                                <li><i class="fa fa-check-circle"></i> Seamless appointment scheduling</li>
                                <li><i class="fa fa-check-circle"></i> Convenient access to a patient's medical history
                                </li>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-4 steps">
                        <div class="col-md-3 text-center">
                            <div class="step-icon"><i class="fa fa-shield"></i></div>
                        </div>
                        <div class="col-md-9 step-detail">
                            <div class="col-md-12 step-heading">Secure</div>
                            <div class="col-md-12 step-list">
                                <li><i class="fa fa-check-circle"></i> HIPAA Compliant</li>
                                <li><i class="fa fa-check-circle"></i> Encrypted Communications 24/7</li>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-12 signup-btn clearfix">
                <a href="<?php echo HTTP_SERVER; ?>app/register/doctor" class="btn btn-info sing" style="left: 10px;position: relative;">Get Started with a
                    Risk-Free 90-Day Trial</a><br><br>
                <div class="col-md-12 text-center"> Already have an account? <a
                        href="<?php echo HTTP_SERVER; ?>app/login">Sign In Here</a></div>
            </div>

        </div>
    </div>
</section>


<section class="Finding our-platform clearfix">

    <div class="container">

        <div class="row">

            <div class="col-md-8">

                <h2>Interactive 2-Way Consults</h2>

                <p>My Virtual Doctor’s powerful turnkey telemedicine solution lets you find operational and financial
                    efficiencies while extending your ability to reach patients through secure one-on-one HD video
                    streaming, audio, and text message consultations. Our cutting-edge technology is not only fast,
                    convenient, and easy to use, but it also offers you and your patients a seamless healthcare
                    experience and optimal flexibility. It’s ideal for creating valuable patient engagement online and
                    delivering high-quality healthcare to patients who need it most. The software is integration-ready
                    and designed to scale with a growing practice.</p>

            </div>

            <div class="col-md-4 text-center">

                <img class="img-with-animation" data-delay="0" height="100%" data-animation="fade-in"
                     src="<?php echo HTTP_SERVER; ?>img/image_01.jpg" alt="" style="opacity: 1;">

            </div>

        </div>

    </div>

</section>

<section class="Finding our-platform grey clearfix">

    <div class="container">

        <div class="row">

            <div class="col-md-4 text-center">

                <img class="img-with-animation" data-delay="0" height="100%" data-animation="fade-in"
                     src="<?php echo HTTP_SERVER; ?>img/image_03.png" alt="" style="opacity: 1;">

            </div>

            <div class="col-md-8">

                <h2>Highest Level of HIPAA Compliance and Data Encryption</h2>

                <p>Protecting a patient’s privacy and limiting liability are top priorities for any physician.
                    Consultations via My Virtual Doctor’s platform are fully HIPAA compliant while using a quarantined
                    data encrypted to ensure the highest level of security at all times. We employ comprehensive and
                    stringent security policies, procedures, and protocols to keep your information safe so that you can
                    confidently use our platform with utmost peace of mind. </p>


            </div>


        </div>

    </div>

</section>

<section class="Finding our-platform clearfix">

    <div class="container">

        <div class="row">

            <div class="col-md-8">

                <h2>Valuable Telemedicine Directory Listings</h2>

                <p>Our software enables you to build a detailed physician profile that showcases both your education and
                    experience while enhancing your online presence and creating brand awareness. My Virtual Doctor’s
                    directory listing casts a wider net in search results, effectively attracting targeted traffic to
                    your profile and generating new business for you and your practice. In addition, the platform allows
                    for review submissions by verified patients only, thereby boosting your credibility and protecting
                    your reputation.</p>

            </div>

            <div class="col-md-4 text-center">

                <img class="img-with-animation" data-delay="0" data-animation="fade-in"
                     src="<?php echo HTTP_SERVER; ?>img/image_04.png" alt="" style="opacity: 1;">

            </div>

        </div>

    </div>

</section>

<section class="Finding our-platform grey clearfix">

    <div class="container">

        <div class="row">

            <div class="col-md-4 text-center">

                <img class="img-with-animation" data-delay="0" height="100%" data-animation="fade-in"
                     src="<?php echo HTTP_SERVER; ?>img/image_02.png" alt="" style="opacity: 1;">

            </div>

            <div class="col-md-8">

                <h2>Modern and Dynamic Healthcare Delivery</h2>

                <p>My Virtual Doctor’s software helps you achieve and maintain a competitive advantage by helping you
                    reach and engage increasingly tech-savvy patients, while still accommodating patients not up to date
                    on the latest technologies. Our easy to use platform can be accessed from anywhere and at any time
                    using a multitude of Internet-enabled devices including smartphones, laptops, tablets, and desktop
                    computers. This effectively reduces cancellations and no-shows while increasing retention rates. We
                    do all the work so you can focus on what you do best: providing the best healthcare possible to your
                    patient population.</p>


            </div>


        </div>

    </div>

</section>

<section class="Finding our-platform clearfix">

    <div class="container">

        <div class="row">

            <div class="col-md-8">

                <h2>Advanced Administrative Backend</h2>

                <p>Specifically designed for healthcare providers, My Virtual Doctor’s administrative application
                    strategically integrates with standard medical administrative practices and the systems you already
                    have in place. From consultation scheduling and payment processing to reporting and analytics, our
                    telemedicine platform allows you and your administrative personnel to manage your responsibilities
                    easily and proficiently.</p>

            </div>

            <div class="col-md-4 text-center">

                <img class="img-with-animation" data-delay="0" height="100%" data-animation="fade-in"
                     src="<?php echo HTTP_SERVER; ?>img/image_05.png" alt="" style="opacity: 1;">

            </div>

        </div>

    </div>

</section>

<div class="calculator-main">
    <div class="container">
        <h1>My Virtual Doctor is simplifying telemedicine </h1>
        <p>Find out how much you can increase the revenue of your practice by implementing our turnkey telemedicine
            solution.</p>
        <div class="calculator-box-main">
            <div class="calculator-box">
                <h6>Number of Providers</h6>
                <p><input class="form-control ng-pristine ng-valid" id="numofproviders" type="text"
                          name="numofproviders" required onkeyup="resetField('numofproviders')"></p>
            </div>
            <div class="calculator-box">
                <h6>Expected number of telemedicine visits per day, per provider</h6>
                <p><input class="form-control ng-pristine ng-valid" id="expectedvisits" type="text"
                          name="expectedvisits" required onkeyup="resetField('expectedvisits')"></p>
            </div>
            <div class="calculator-box">
                <h6>Average charge for telemedicine visits<br><span>If you're not sure, use $75 </span></h6>
                <p><input class="form-control ng-pristine ng-valid" id="averagecharge" type="text" name="averagecharge"
                          required onkeyup="resetField('averagecharge')" value="75"></p>

            </div>
            <p>
                <button id="my_button" class="btn btn-primary" type="button" onclick="return calculateROI();">
                    Calculate Revenue
                </button>
            </p>
            <div class="collapse" id="collapseExample">
                <div class="card card-block">
                    <p>By adding My Virtual Doctor to your practice, you'd increase revenue by </p>
                    <h1><span id="result"></span><span>/year</span></h1>
                    <a class="green-outline-button text-center" href="<?php echo HTTP_SERVER; ?>app/register/doctor">Get
                        Started</a>
                </div>
            </div>
            <div id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                </div>
            </div>
        </div>
    </div>
</div>

<section class="grey clearfix" id="footer" style="background:#36a5bc;">

    <div class="container">

        <div class="col-md-12">

            <div class="row">

                <div class="col-md-8 col-md-offset-2 text-center">

                    <h1 class=" Virtual-heading">Are you ready to market your practice more effectively?</h1>

                </div>

                <div class="col-md-12">

                    <div class="col-md-8  col-md-offset-2 text-center request-form" id="getstrt">

                        <h2 class="simple pra">Sign Up Today </h2> <br/>

                        <?php
                        if(isset($_REQUEST['submit'])){
                            $submit = $_REQUEST['submit'];
                            $InputNameFirst = $_REQUEST['InputNameFirst'];
                            $InputNameLast = $_REQUEST['InputNameLast'];
                            $InputEmail = $_REQUEST['InputEmail'];
                            $InputPhone = $_REQUEST['InputPhone'];
                            $mail = new PHPMailer;
                            $mail->SMTPDebug = 2;
                            $mail->Host = "mail.myvirtualdoctor.com";
                            $mail->Port = 25;
                            $mail->SMTPAuth = false;
                            $mail->setFrom($InputEmail, $InputNameFirst.' '.$InputNameLast);
//                            $mail->setFrom('info@myvirtualdoctor.com');
                            $mail->addAddress('info@myvirtualdoctor.com');
//                            $mail->addAddress($InputEmail);
                            $mail->Subject = $submit.' ('.$InputNameFirst.' '.$InputNameLast.')';
                            $mail->Body    = 'Dear Administrator,

'.$submit.'

First Name: '.$InputNameFirst.'
Last Name: '.$InputNameLast.'
Email Address: '.$InputEmail.'
Phone: '.$InputPhone.'

Thank you,
My Virtual Doctor';
                            if(!$mail->send()) {
                                echo '<p class="text-danger">Message could not be sent.</p>';
//                                echo 'Mailer Error: ' . $mail->ErrorInfo;
                            } else {
                                echo '<p class="text-success">Message has been sent</p>';
                            }
                        }
                        ?>

                        <form role="form" id="demo-form" action="#" method="post">

                            <div id="form_message"></div>

                            <input type="hidden" name="token"
                                   value="373353344136304f3341345049334249304458324e50514a574443554f4e4b">

                            <input type="text" placeholder="First Name" id="InputNameFirst" name="InputNameFirst"
                                   value="" class="form-control" required="">

                            <input type="text" placeholder="Last Name" id="InputNameLast" name="InputNameLast" value=""
                                   class="form-control" required="">

                            <input type="text" placeholder="Email" id="InputEmail" name="InputEmail" value=""
                                   class="form-control" required="">

                            <input type="text" placeholder="Phone" id="InputPhone" name="InputPhone" value=""
                                   class="form-control" required="">

                            <!--<input type="text" placeholder="Clinic" id="InputClinic" name="InputClinic" value="" class="form-control" required=""> -->

                            <!-- <input type="submit" value="REQUEST DEMO"  name="submit" id="submit" class="btn btn-info request-btn" /> -->

                            <button type="submit" name="submit" id="submit" value="MVD - Request for Demo"
                                    class="btn btn-info pull-right request-btn">Request a Demo
                            </button>
                            <span class="or-sub">OR</span>
                            <button type="submit" name="submit" id="submit" value="MVD - 90-Day Free Trial Request"
                                    class="btn btn-info pull-left strt-free-btn">Start a 90-Day Free Trial
                            </button>


                        </form>

                    </div>


                </div>

            </div>

        </div>

    </div>

</section>	