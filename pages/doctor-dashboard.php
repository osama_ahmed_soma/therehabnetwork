<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
?>

<section class="grey clearfix" id="dashboard-main-top-section"  style="background:url(<?php echo HTTP_SERVER; ?>/img/back.png);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1 class=" Virtual-heading">Your Physician Telehealth Hub</h1>
            </div>
            <div class="col-md-12">
                <div class="col-md-12 text-center">
                    <h2 class="simple pra">Making online healthcare delivery simple and profitable.</h2>
                </div>

            </div>


        </div>
    </div>
</section>

<section class="clearfix profile-page">
    <div class="container">
        <div class="row">

            <div class="col-sm-4 col-md-3 sidebar">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#showprofile">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<a class="show-profile" href="#"></a>						
					</button>
					
				</div>
				<div class="collapse navbar-collapse row" id="showprofile">
                <div class="list-group sidebar">
                    <span href="#" class="list-group-item ">
                    	Your Docter Profile <span class="badge">Edit</span>
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped active" role="progressbar"
                                 aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                                40%
                            </div>
                        </div>
                    </span>
                    <ul>
                        <li>
                            <a href="<?php echo HTTP_SERVER . 'doctor-dashboard/dashboard.html' ?>" class="list-group-item <?php echo $sub_page == '' || $sub_page == 'dashboard' ? 'active' : ''; ?>">
                                <i class="fa fa-calendar"></i> Dashboard
                            </a>
                        </li>
                        <!--<li>
                            <a href="<?php echo HTTP_SERVER . 'doctor-dashboard/calendar.html' ?>" class="list-group-item <?php echo $sub_page == 'calendar' ? 'active' : ''; ?>">
                                <i class="fa fa-calendar"></i>  Appointments 
                            </a>
                        </li>-->


                        <li>
                            <a href="#" class="list-group-item <?php echo ($sub_page == 'schedule' || $sub_page == 'plans' || $sub_page == 'timings') ? 'active' : ''; ?>" data-toggle="collapse" data-target="#schedule">
                                <i class="fa fa-users"></i> Schedule <span class="dropdown"><i class="fa fa-chevron-down"></i></span>
                            </a>
                            <ul id="schedule" class="collapse <?php echo ($sub_page == 'schedule' || $sub_page == 'plans' || $sub_page == 'timings') ? 'in' : ''; ?>">
                                <li> <a href="<?php echo HTTP_SERVER . 'doctor-dashboard/schedule.html' ?>" class="list-group-item <?php echo $sub_page == 'schedule' ? 'active' : ''; ?>"><i class="fa fa-users"></i> Appointments</a></li>
                                <li> <a href="<?php echo HTTP_SERVER . 'doctor-dashboard/plans.html' ?>" class="list-group-item <?php echo $sub_page == 'plans' ? 'active' : ''; ?>"><i class="fa fa-users"></i> Consultation Plans</a></li>
                                <li> <a href="<?php echo HTTP_SERVER . 'doctor-dashboard/timings.html' ?>" class="list-group-item <?php echo $sub_page == 'timings' ? 'active' : ''; ?>"><i class="fa fa-users"></i> Availability Timings</a></li>
                            </ul>
                        </li>

                       
                        <li style="display:none;">
                            <a href="#" class="list-group-item" data-toggle="collapse" data-target="#consults">
                                <i class="fa fa-users"></i> Inbox <span class="dropdown"><i class="fa fa-chevron-down"></i></span>
                            </a>
                            <ul id="consults" class="collapse">
                                <li> <a href="<?php echo HTTP_SERVER . 'doctor-dashboard/compose.html' ?>" class="list-group-item "><i class="fa fa-users"></i> Compose</a></li>
                                <li> <a href="<?php echo HTTP_SERVER . 'doctor-dashboard/inbox.html' ?>" class="list-group-item "><i class="fa fa-users"></i> Inbox</a></li>
                                <li> <a href="#" class="list-group-item "><i class="fa fa-users"></i> Consults</a></li>
                                <li> <a href="#" class="list-group-item "><i class="fa fa-users"></i> Consults</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo HTTP_SERVER . 'patient/support.html' ?>" class="list-group-item <?php echo $sub_page == 'support' ? 'active' : ''; ?>">
                                <i class="fa fa-users"></i> Blog
                            </a>
                        </li>
                        
                        <?php /*?><li>
                            <a href="<?php echo HTTP_SERVER . 'patient/files.html' ?>" class="list-group-item <?php echo $sub_page == 'files' ? 'active' : ''; ?>">
                                <i class="fa fa-list-ul"></i> Files
                            </a>
                        </li><?php */?>
                        
                        <li>
                            <a href="<?php echo HTTP_SERVER . 'doctor-dashboard/payment_method.html' ?>" class="list-group-item <?php echo $sub_page == 'payment_method' ? 'active' : ''; ?>">
                                <i class="fa fa-credit-card"></i> Payment Method
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo HTTP_SERVER . 'doctor-dashboard/setting.html' ?>" class="list-group-item  <?php echo $sub_page == 'setting' ? 'active' : ''; ?>" data-toggle="collapse " data-target="#settings">
                                <i class="fa fa-users"></i> Settings <span class="dropdown"><i class="fa fa-chevron-down"></i></span>
                            </a>
                            <ul id="settings" class="collapse <?php echo $sub_page == 'setting' ? 'in' : ''; ?>">
                                <li class="active"><a href="#home" data-toggle="tab" class="list-group-item" >General Information</a></li>
                                <li><a href="#profile" class="list-group-item" data-toggle="tab">Profile</a></li>
                                <li><a href="#workplaces" class="list-group-item"  data-toggle="tab">Workplaces</a></li>
                                <li><a href="#specializations" class="list-group-item" data-toggle="tab">Specializations</a></li>
                                <li><a href="#biodata" class="list-group-item" data-toggle="tab">Biodata</a></li>
                                <li><a href="#languages" class="list-group-item" data-toggle="tab">Languages</a></li>
                                <li><a href="#procedures" class="list-group-item" data-toggle="tab">Procedures Attempted</a></li>
                                <li><a href="#conditions" class="list-group-item" data-toggle="tab">Conditions Treated</a></li>
                                <li><a href="#education" class="list-group-item" data-toggle="tab">Education and Training</a></li>
                                <li><a href="#awards" class="list-group-item" data-toggle="tab">Awards & Distinctions</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo HTTP_SERVER . 'logout.html' ?>" class="list-group-item">
                                <i class="fa fa-unlock-alt"></i> Log Out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
</div>

            <div class="col-md-9 col-sm-8 ">
                <?php
                if ( $sub_page != '' ) {
                    require_once SITE_ROOT . 'pages/modules/' . $do .'/'. $sub_page .'.php';
                } else {
                    require_once SITE_ROOT . 'pages/modules/' . $do .'/dashboard.php';
					//echo SITE_ROOT . 'pages/modules/' . $do .'/calendar.php';
                }	
                ?>
            </div>

        </div>
    </div>
</section>