<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}

//get category data
$location_data = array();

try {
	$query = "SELECT id, categoryName, categoryType, categoryFeatured FROM " . DB_PREFIX . "category WHERE `categoryType` = 'location' AND categoryStatus = 1 order by `categoryOrder` asc";
	$result = $db->prepare($query);
	$result->execute();

	if ( $result->rowCount() > 0 ) {
		$location_data = $result->fetchAll();
	}

} catch (Exception $Exception) {
	exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}
?>






<section  class="privacypolicy-page">
	<div class="container">
		<div class="row">
          <div class="privacypolicy-main">
         <h1>MY VIRTUAL DOCTOR ONLINE PRIVACY POLICY</h1>
		<p><b>1</b>. Introduction.MY VIRTUAL DOCTOR, Inc. (“MY VIRTUAL DOCTOR”, “we”, “us”, or “our”) operates the website located at <a href="<?php echo HTTP_SERVER; ?>"> <?php echo HTTP_SERVER; ?>"</a>, other related websites and mobile applications (collectively, the “Site”). Through these sites, we operate various online services that enable members (“Members”) to receive telehealth services from healthcare providers located in the U.S.. This Online Privacy Policy (“Privacy Policy’) explains MY VIRTUAL DOCTOR’s privacy practices that apply to the services (collectively, the “Services”) we provide through our Sites.</p>
		<p>Please read the Privacy Policy carefully so that you can understand how MY VIRTUAL DOCTOR collects, uses, and discloses information from and/or about you when you use a Site or the Services. If you do not understand any aspect of our Privacy Policy, please feel free to contact using the information found at the end of the Privacy Policy.</p>
		<p>This Privacy Policy is a part of the MY VIRTUAL DOCTOR Terms of Use so by accepting the Terms of Use, you are consenting to the use and disclosure of your personal information as outlined in this Privacy Policy.</p>
		<p><b>2</b>. HIPAA. In addition, the healthcare providers from the MY VIRTUAL DOCTOR Medical Group that you interact with when using the Services are required by the Health Insurance Portability and Accountability Act (“HIPAA”) to describe their privacy practices in document called a Notice of Privacy Practices. You can access a copy of this notice on the patient portal.</p>
		<p><b>3</b>. Third Party Sites. This site contains links to other sites. MY VIRTUAL DOCTOR does not share personal information with those sites, unless specifically authorized by you. MY VIRTUAL DOCTOR is not responsible for the privacy policies of any site linked to this website and not wholly owned and controlled by MY VIRTUAL DOCTOR, its affiliates and subsidiaries. We aim to work with trusted partners and organizations which are also bound by state and federal laws governing privacy and security; however, we encourage users to be aware when you leave our Site and to review any privacy policy of a non-MY VIRTUAL DOCTOR site.</p>
		<p><b>4</b>. Important Definitions. When we use the term “Personal Information” in this Privacy Policy, we mean information about you that is personally identifiable to you, such as your contact information (e.g., name, address, email address, or telephone number), personally identifiable health or medical information (“Health Information”), and any other non-public information that is associated with such information (collectively, “Personal Information”). </p>
		<p>When we use the term “De-Identified Information”, we mean information about you but that does not identify you.</p>
		<p><b>5</b>. The Personal Information we collect or maintain may include:</p>
		  <ul>
		   <li><b>a</b>.	Your name, age, email address, username, password, and other registration information.</li>
		   <li><b>b</b>.	Health Information that you provide us, which may include information or records relating to your medical or health history, health status and laboratory testing results, diagnostic images, and other health related information.</li>
		   <li><b>c</b>.	Health information about you prepared by the health care provider(s) who provide the Services through the Site such as medical records, treatment and examination notes, and other health related information.</li>
		   <li><b>d</b>.	Billing information that you provide us.</li>
		   <li><b>e</b>.	Information about the computer or mobile device you are using, such as what Internet browser you use, the kind of computer or mobile device you use, and other information about how you use the Site.</li>
		   <li><b>f</b>.	Other information you input into the Site or related services.</li>
		  
		  
		  </ul>
		  <p><b>6</b>. We may use your Personal Information for the following purposes (subject to applicable legal restrictions):</p>
		  
		<ul>
		   <li><b>a</b>.	To provide you with the Services</li>
		   <li><b>b</b>.	To improve the services offered – by performing quality reviews and similar activities</li>
		   <li><b>c</b>.	To create De-identified Information such as aggregate statistics relating to the use of the Service</li>
		   <li><b>d</b>.	To notify you when Site updates are available,</li>
		   <li><b>e</b>.	To market and promote our Site and the Services offered to you through the Site</li>
		   <li><b>f</b>.	To fulfill any other purpose for which you provide us Personal Information</li>
		   <li><b>g</b>.	For any other purpose for which you give us authorization</li>
		  
		  
		  </ul>
		
		<p>You can “opt out” of receiving direct marketing or market research information by emailing us at <a href="#"> info@myvirtualdoctor.com</a>.</p>
		<p><b>7</b>. We may also disclose Personal Information that we collect or you provide as permitted or allowed by law, as follows:</p>
		
		<ul>
		 <li><b>a</b>.	To affiliated medical groups.</li>
		 <li><b>b</b>.	To contractors, service providers and other third parties we use to support our business and who are bound by contractual obligations to keep personal information confidential and use it only for the purposes for which we disclose it to them.</li>
		 <li><b>c</b>.	As required by law, which can include providing information as required by law, regulation, subpoena, court order, legal process or government request.</li>
		 <li><b>d</b>.	When we believe in good faith that disclosure is necessary to protect your safety or the safety of others, to protect our rights, to investigate fraud, or to respond to a government request.</li>
		 <li><b>e</b>.	To a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution or other sale or transfer of some or all of MY VIRTUAL DOCTOR’s assets, whether as a going concern or as part of bankruptcy, liquidation or similar proceeding, in which Personal Information maintained by the Site is among the assets transferred.</li>
		 </ul>
		 <p><b>8</b>. Information we Collect via technology: As you use the Site or the Service, certain information may be passively collected by Cookies, navigational data like Uniform Resource Locators (URLs) and third party tracking services, including:</p>
		 <ul>
		 <li><b>a</b>.	Site Activity Information. We may keep track of some of the actions you take on the Site, such as the content of searches you perform on the Site.</li>
		  <li><b>b</b>.	Access Device and Browser Information. When you access the Site from a computer or other device, we may collect anonymous information from that device, such as your Internet protocol address, browser type, connection speed and access times.</li>
		 <li><b>c</b>.	Cookies. A cookie is a small file containing a string of characters that is sent to your computer when you visit a website. When you visit the Site again, the cookie allows that site to recognize your browser. Cookies may store unique identifiers, user preferences and other information. You can reset your browser to refuse all cookies or to indicate when a cookie is being sent. However, some Site features or services may not function properly without cookies. We use cookies to improve the quality of our service, including for storing user preferences, tracking user trends and providing relevant advertising to you.</li>
		 <li><b>d</b>.	Real-Time Location. Certain features of the Site use GPS technology to collect real-time information about the location of your device so that the Site can connect you to a health care provider who is licensed or authorized to provide services in the state where you are located.</li>
		 <li><b>e</b>.	Google Analytics. We use Google Analytics to help analyze how users use the Site. Google Analytics uses Cookies to collect information such as how often users visit the Site, what pages they visit, and what other sites they used prior to coming to the Site. We use the information we get from Google Analytics only to improve our Site and Services. Google Analytics collects only the IP address assigned to you on the date you visit the Site, rather than your name or other personally identifying information. Although Google Analytics plants a persistent Cookie on your web browser to identify you as a unique user the next time you visit the Site, the Cookie cannot be used by anyone but Google. Google’s ability to use and share information collected by Google Analytics about your visits to the Site is restricted by the Google Analytics Terms of Use and the Google Privacy Policy.</li>
		 
		</ul>
		 
		<p><b>9</b>. De-Identified Information. We may use De-Identified Information created by us without restriction.</p>
		<p><b>10</b>. Security of Information Collected. We use industry standard physical, technical and administrative security measures and safeguards to protect the confidentiality and security of your Personal Information. Specifically, the Site is protected by SSL 3.0 technology, the leading security protocol for data transfer on the Internet. However, since the Internet is not a 100% secure environment, we cannot guarantee, ensure, or warrant the security of any information you transmit to us. There is no guarantee that information may not be accessed, disclosed, altered, or destroyed by breach of any of our physical, technical, or managerial safeguards. Even though there are many benefits to using this Site, as with all electronic communications there are some risks such as (i) failure of hardware, software and/or Internet connections; we are not responsible for failures, distortions, delays, or other problems resulting from equipment configuration, connection, signal power, hardware, software or any equipment used to access the internet; and (ii) no guarantee that the confidentiality or security of electronic transmissions via the Internet can be assured due to potentially unsecure computers and links. This could result in your data becoming lost or intercepted during transmission. It is your responsibility to protect the security of your login information and to use good judgment before deciding to send information via the Internet. Please note that e-mails and other communications you send to us through our Site are not encrypted, and we strongly advise you not to communicate any confidential information through these means.</p>
		<p><b>11</b>. Modification of Information. You can update some of your personal information through our Site. Requests to modify any information may also be submitted directly to info@myvirtualdoctor.com</p>
		<p><b>12</b>. Deletion of Information. You can close your online account by emailing us at <a href="#"> info@myvirtualdoctor.com</a>. If you close your account, we will no longer use your online account information or share it with third parties. We may, however, retain a copy of the information for archival purposes, and to avoid identity theft or fraud.</p>
		<p><b>13</b>. Report Violations. You should report any security violations to us by sending an email to <a href="#">info@myvirtualdoctor.com</a></p>
		<p><b>14</b>. A Special Note about Children and Minors: My Virtual Doctor does not knowingly allow individuals under the age of 18 to create accounts that allow access to our Site or use the Services. If you are below the age of 18 you are not permitted to use this Site or the Services.</p>
		<p><b>15</b>. Other Applicable Law. California Residents have special protections under state law regarding the access and use of personal information. CA Civil Code Sec. 1798.80-1978.84.</p>
		<p><b>16</b>. Changes to our Privacy Policy. We may change this Privacy Policy from time to time in the future. We will post any revised version of the Privacy Policy on this page and at other places we deem appropriate. Continued use of our Service following notice of such changes will indicate your acknowledgement of such changes and agreement to be bound by the terms and conditions of such changes. By using the Site, you are agreeing to our collection, use and disposal of Personal Information and other data as described in this Privacy Policy, both as it exists now and as it is changed from time to time.</p>
		<p><b>17</b>. Questions and How to Contact Us. If you have any questions, concerns, complaints or suggestions regarding or Privacy Policy or otherwise need to contact us, please email us at <a href="#"> info@myvirtualdoctor.com.com</a>, or contact us by US postal mail at the following address:</p>
		<p class="p-Virtual-adres">My Virtual Doctor, LLC.<br>
7451 Wiles Road<br>
Suite 105<br>
Coral Springs, FL 33067<br>
</p>
		
			
</div> 


         </div>    
    </div>    
</section>

