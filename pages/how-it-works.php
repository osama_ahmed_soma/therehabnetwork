<section class="grey clearfix" id="service"  style="background:url(img/back.png);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1 class=" Virtual-heading">Easy Like Sunday Morning.   </h1>
            </div>
            <div class="col-md-12">
                <div class="col-md-6  col-md-offset-3 text-center">
                    <h2 class="simple pra">We love our simple, secure process. <br />
                        And we know you will too. </h2>
                </div>

            </div>


        </div>
    </div>
</section>

<section class="Finding clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-md-push-9 col-sm-4 col-xs-12 text-right-md text-center-xs">
                    <img src="<?php echo HTTP_SERVER; ?>img/how-1.png" class="img-responsive dp-center-xs">
                </div>
                <div class="col-md-9 col-md-pull-3 col-sm-8 col-xs-12 text-right-md mb-sm-20  text-center-xs">
                    <h1 class="Finding-heading"> Finding a Telehealth Doctor Is No <br />Longer A Game Of Chance</h1>
                    <p class="Finding-pra">Currently if you want to do an online doctor consultation, chances are the company you consult with will randomly placed you with the first doctor available. However, we think it’s a safe bet you wouldn’t let any random doctor give you a consultation. Now you don’t have to worry.  Sit back and take your time finding the best doctor available by reading verified reviews and in-depth information on each doctor. </p>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-3  col-sm-4 col-xs-12 text-left mb-md-10">
                    <img src="<?php echo HTTP_SERVER; ?>img/how-2.png" class="img-responsive dp-center-xs">
                </div>
                <div class="col-md-9  col-sm-8 col-xs-12 text-left text-center-xs">
                    <h1 class="Finding-heading"> A Consolidated Telehealth  <br />Patient Platform</h1>
                    <p class="Finding-pra">Currently if you want to do an online doctor consultation, chances are the company you consult with will randomly placed you with the first doctor available. However, we think it’s a safe bet you wouldn’t let any random doctor give you a consultation. Now you don’t have to worry.  Sit back and take your time finding the best doctor available by reading verified reviews and in-depth information on each doctor.  </p>
                </div>
            </div>
        </div>
    </div>
</section>