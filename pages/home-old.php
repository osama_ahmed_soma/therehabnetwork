<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}

//get category data
$location_data = array();

try {
	$query = "SELECT id, categoryName, categoryType, categoryFeatured FROM " . DB_PREFIX . "category WHERE `categoryType` = 'location' AND categoryStatus = 1 order by `categoryOrder` asc";
	$result = $db->prepare($query);
	$result->execute();

	if ( $result->rowCount() > 0 ) {
		$location_data = $result->fetchAll();
	}

} catch (Exception $Exception) {
	exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}
?>

<section id="banner" style="background:url(<?php echo HTTP_SERVER; ?>/img/home-banner.jpg) center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="banner-cnt">
                	<h1>Feeling Better is Just a Click Away</h1>
					<?php /*?>We match patient needs and preferences to physician expertise and experience, helping you find and choose the best virtual doctors in America. It’s personalized online healthcare the way it should be.<?php */?>
                   Now you can get personalized care from your trusted physician anywhere
you are. Save time and money by scheduling your next doctor’s appointment online.

					<div><a href="<?php echo HTTP_SERVER . 'signup.html' ?>" class="btn btn-dark-blue">Get Started</a></div>
				</div>
			</div>
		</div>
	</div>
</section>


<section  class="step-choose-section how-it-works-home">
	<div class="container">
		<div class="row">
        	<div class="col-md-12 text-center practice-heading clearfix">
            	<h1>How It Works</h1>
            </div>
        	<div class="col-md-4 col-sm-4 col-xs-12 step-main">
            	<div class="col-md-12 text-center sm-icon-sec">
                	<div class="step-icon"><i  class="fa fa-desktop"></i><span><i  class="fa fa-plus"></i></span></div>
                    <div class="step-heading">Step 1 </div>
                </div>
                <div class="col-md-12 step-detail">
                	<div class="col-lg-12 text-center">Signup instantly via email of social media account</div>
                </div>          
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 step-main">
            	<div class="col-md-12 text-center sm-icon-sec">
                	<div class="step-icon"><i  class="fa fa-user-md"></i><span><i  class="fa fa-search"></i></span></div>
                    <div class="step-heading">Step 2 </div>
                </div>
                <div class="col-md-12 step-detail">
                	<div class="col-lg-12 text-center">Find your doctor using our fast and user friendly system</div>
                </div>          
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 step-main">
            	<div class="col-md-12 text-center sm-icon-sec">
                	<div class="step-icon"><i  class="fa fa-calendar"></i><span><i  class="fa fa-youtube-play"></i></span></div>
                    <div class="step-heading">Step 3</div>
                </div>
                <div class="col-md-12 step-detail">
                	<div class="col-lg-12 text-center">Schedule a video consultation and get started</div>
                </div>          
            </div>
            
        </div>
        
    </div>    
</section>

<section  class="about-us-home" style="background:url(<?php echo HTTP_SERVER; ?>img/about-section.jpg) center center;">
	<div class="container">
		<div class="row">
        	<!--<div class="col-md-12 text-center practice-heading clearfix">
            	<h1>About Us</h1>
            </div>-->
        	<p style="font-weight: bold;">My Virtual Doctor was designed to take the stress out of doctor’s appointments by merging telehealth technology and personalized care from certified physicians. Our simple to use, HIPPA compliant platform securely allows patients and physicians to communicate through live video to effectively diagnose and treat common ailments. We provide high quality medical care on the go to get you feeling better in no time.</p>
      	 </div>   
    </div>    
</section>


<section  class="step-choose-section how-it-works-home">
	<div class="container">
		<div class="row">
        	<div class="col-md-12 text-center practice-heading clearfix">
            	<h1>Common Conditions We Treat</h1>
            </div>
        	
            <div class="col-md-12 clearfix how-it-ul-sec">
            	<ul>
                	<li>Flu</li>
                	<li>Cough</li>
                	<li>Ear problems</li>
                	<li>Allergies</li>
                	<li>Nausea / Vomiting</li>
                	<li>Urinary problems / UTI</li>
                	<li>Insect Bites</li>
                	<li>Sore throats</li>
                	<li>Vomiting</li>
                </ul>
            </div>
            <div class="col-md-12 clearfix how-it-btn-sec">
            	<a href="#" class="btn btn-light">More Medical Conditions</a>
            </div>
        </div>
        
    </div>    
</section>
<section id="testimonials" class=" txt-center">
    	<div class="container">
        <div class="row">
        <h2 class="heading">We love our Patients <br><span>and they love us!</span></h2>
         <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
         	  
            <ul class="carousel-inner">    
            <!-- Start -->
            <li class="item active">
                <div class="testText col-md-10 fn mauto">
                    <p>
                        So thankful for this amazing online system. We are traveling with our four kids, one of whom spiked a very high fever on the road. Turns out she has strep and needs an antibiotic. Glad to avoid the local ER, and there aren't any walk in clinics in this small town...which would also cost more and likely take longer. Thanks MVD for saving our vacation!
                    </p>
                </div>
                <div class="clearfix">
                    James Mathew<br>
                    <span class="f12">Software Engineer, AND Microtech</span>
                </div>
            </li>
            <!-- END -->

            <!-- Start -->
            <li class="item">
                <div class="testText col-md-10 fn mauto">
                    <p>
                        Since I am a mom and a full-time employee, which gives me very little time for anyone in our household to be sick. MyVirtualDoctor has saved us tons of time in a waiting room and always gets us well when needed with just a few clicks! Love this application and The dedicated doctors who make it successful!
                    </p>
                </div>
                <div class="clearfix">
                   Nina smith<br>
                    <span class="f12">Sales Representative, Micola Motors</span>
                </div>
            </li>
            <!-- END -->
        </ul>
        
        <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                </ol>
        </div>
        </div> </div>
   </section>

