<div class="log-back">
    <div class="container">
        <div class="logfrom-main">
            <h1>
                See Your Doctor Within Minutes</h1>
            
            <div class="logfrom-left">
                <div class="logfrom-left-back"></div>
                
            </div>
            <div class="signup-right bbm-modal__topbar">
                 <h2 class="text-center">Let's Get Started</h2>
                <p class="text-center" style="    font-size: 22px;">Creating an account is Free!</p>
                <div id="register_form_error"></div>
                <form data-role="form" name="registrationForm" id="registrationForm" class="ng-invalid ng-invalid-required ng-dirty">
                    <div>
                        <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                        <div id="firstname-wrapper" class="form-group has-feedback margin-bottom-sm" style="    width: 49%;    float: left;	">
                            <input class="form-control ng-pristine ng-valid" type="text" name="userFname" id="userFname" placeholder="First Name">
                        </div>
                        <div id="lastname-wrapper" class="form-group has-feedback margin-bottom-sm" style="    width: 50%;    float: left;    margin-right: 0px !important;">
                            <input class="form-control ng-pristine ng-valid" type="text" name="userLname" id="userLname" placeholder="Last Name">
                        </div>
						 <div id="username-wrapper" class="form-group has-feedback margin-bottom-sm">
                            <input class="form-control ng-isolate-scope ng-pristine ng-invalid ng-invalid-required ng-valid-email" autocomplete="off" type="text" name="userEmail" id="userEmail" placeholder="Email address">
                        </div>
						 <div id="lastname-wrapper" class="form-group has-feedback margin-bottom-sm">
                            <input class="form-control ng-pristine ng-valid" type="text" name="dob" id="dob" placeholder="Date Of Birth (mm/dd/yyyy)">
                        </div>
                       
                       
                       
                        <div id="consent-box" class="form-group has-feedback" style="    margin-bottom: 3px;">
                           <span class="required-checkbox consent-needed">
                           <input type="checkbox" name="consent" id="consent" class="cursor ng-isolate-scope ng-pristine ng-invalid ng-invalid-required">
                           </span>
                            <div>
                                <p class="tou-wrapper">
                                    <label class="cursor"style="    font-size: 16px;">I agree to My Virtual Doctor's</label>
                                    </br>
                                </p>
                                <p class="tou-Terms" style="    font-size: 15px !important;"><span><a target="_blank" href="<?php echo HTTP_SERVER; ?>termandconditions.html" class="external">Terms of Use</a> and <a target="_blank" href="<?php echo HTTP_SERVER; ?>privacypolicy.html" class="external">Privacy Policy</a>	</span>
                            </div>
                        </div>
                    </div>
                <button type="submit" id="btn-login" class="btn btn-info sing login btn-block" style="font-size: 23px; height: 54px;    font-weight: 600;    text-transform: uppercase;">Sign Up </button>

                <!--<h5> Already have an account? <a href="<?php echo HTTP_SERVER . 'login.html' ?>" class="login">Log In Here   </a>
                <br> Doctor <a href="<?php echo HTTP_SERVER . 'doctor-login.html' ?>" class="login">Sign in</a></h5> -->
                </form>
                <h5> Already a member?<a href="<?php echo HTTP_SERVER . 'app/login' ?>"> Click here to login</a></h5>
                
                <div class="bbm-modal__bottombar hidden" style="margin-top: 0px;padding: 5px; text-align: center;">
                    Forgot password? <a href="<?php echo HTTP_SERVER . 'password-reset.html' ?>" class="login">Reset your password here   </a>
                </div>
                <div class="bbm-modal__bottombar hidden" style="margin-top: 0px;padding: 5px; text-align: center;">
                    New to My Virtual Doctor?   <a class="signup" href="<?php echo HTTP_SERVER . 'signup.html' ?>">Create a free account now</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(function($){

        //reigster_form
        $('#registrationForm').submit(function() {
            var th = $(this);

            //validate form
            var userFname = jQuery.trim( $('#userFname').val() );
            var userLname = jQuery.trim( $('#userLname').val() );

            var userEmail = jQuery.trim( $('#userEmail').val() );

//            var password = jQuery.trim( $('#password').val() );

//            var userTimeZone = jQuery.trim( $('#userTimeZone').val() );

            var errorStr = '';

            if ( userFname == '' ) {
                errorStr += '<p><strong>First Name</strong> field is required.<p>';
            }

            if ( userLname == '' ) {
                errorStr += '<p><strong>Last Name</strong> field is required.</p>';
            }

            if (userEmail == '') {
                errorStr += '<p><strong>Email address</strong> field is required.<p>';
            }

            /*if ( password == '' ) {
                errorStr += '<p><strong>Password</strong> field is required.</p>';
            }

            if ( userTimeZone == '' ) {
                errorStr += '<p><strong>Time Zone</strong> field is required.</p>';
            }*/

            if ($('#consent').is(":not(:checked)") ) {

                errorStr += '<p>Please agree with <strong>Terms of Use</strong></p>';

            }

            if ( errorStr != '' ) {
                var str = '<div class="alert alert-danger" role="alert">' + errorStr + '</div>';
                $('#register_form_error').html(str);
                return false;
            } else {
                $('#register_form_error').html('');
            }


            var data = $( "#registrationForm" ).serializeArray();

            th.find('button').attr('disabled', true);

            $.ajax({

                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=signup_patient",
                data: data,
                method:"POST",
                dataType:"json"

            }).error(function () {

                th.find('button').removeAttr('disabled');

            }).done(function () {

                th.find('button').removeAttr('disabled');

            }).success(function (data) {
                //data = JQuery.parseJSON(data);
                if ( data.success ) {
                    var str = "";
                    str += '<div class="alert alert-success alert-dismissible" role="alert"><div style="width: 80px; height: 80px; margin: 0px auto;"><img src="<?php echo HTTP_SERVER; ?>/img/1471994079_Tick_Mark_Dark.png" style="position: relative;width: 80px;margin: 0px;padding: 0px;" /></div> \<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\<strong>Success!</strong> ' + data.success + '</div>';
                    $('#register_form_error').html(str);
                    $()
                    setTimeout(function(){
                        $("#registrationForm").hide();
//                        window.location = '<?php //echo HTTP_SERVER; ?>//patient/profile-settings.html';
                    }, 3000);
                }

                if ( data.error ) {
                    var str = '<div class="alert alert-danger alert-dismissible" role="alert">';
                    $.each(data.error, function(idx, error){
                        str +=  error;
                    });
                    str += '</div>';
                    $('#register_form_error').html(str);
                }

            }, 'json');

            return false;
        });
    });
</script>