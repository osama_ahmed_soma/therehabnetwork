<div class="log-back">
    <div class="container">
        <div class="logfrom-main">
          
            
            <h1>
                See a Doctor Within Minutes</h1>
            <h3>Find highly educated and experienced online</br> doctors based on your preferences</h3>
            <div class="logfrom-left">
                <div class="logfrom-left-back"></div>
                <ul class="list-unstyled">
                    <li>
                        <img src="<?php echo HTTP_SERVER; ?>img/PhysiciansIcon.png"  class="logfrom-icon"/>
                        <p>
                            <span>Physicians can treat most common non-emergency medical conditions over video.</span>
                        </p>
                    </li>
                    <li>
                        <img src="<?php echo HTTP_SERVER; ?>img/PsychologistsIcon.png" class="logfrom-icon"/>
                        <p>
                            <span>Psychologists and psychiatrists can address non-emergency emotional or mental health issues.</span>
                        </p>
                    </li>
                    <li>
                        <img src="<?php echo HTTP_SERVER; ?>img/dolar.png" class="logfrom-icon"/>
                        <p>
                            <span>Risk free. If we're unable to treat you through video, we offer a money-back guarantee.</span>
                        </p>
                    </li>
                </ul>
            </div> 
            <div class="signup-right bbm-modal__topbar">
                <h2 class="text-center">Log In</h2>
                <p class="text-center" style="    font-size: 16px;">Welcome back!</p>
                <div id="patient_login_error"></div>
                <form data-role="form" name="patient_login_form" id="patient_login_form" class="ng-invalid ng-invalid-required ng-dirty">
                    <div>
                        <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                        <div id="username-wrapper" class="form-group has-feedback margin-bottom-sm">
                            <input class="form-control ng-isolate-scope ng-pristine ng-invalid ng-invalid-required ng-valid-email" autocomplete="off" type="text" name="userEmail" id="userEmail" placeholder="Email address">
                        </div>
                        <div id="password-wrapper" class="form-group has-feedback">
                            <input name="loginPassword" type="password" class="form-control ng-isolate-scope ng-pristine ng-valid ng-valid-minlength" autocomplete="off" placeholder="Password" id="loginPassword">
                        </div>
                    </div>
                    <button type="submit" id="btn-login" class="btn btn-info sing login btn-block" style="font-size: 21px; height: 43px;">Submit </button>

                    <!--<h5> Forgot password? <a href="<?php echo HTTP_SERVER . 'password-reset.html' ?>" class="login">Reset your password here   </a>
                        <br> New to My Virtual Doctor? <a href="<?php echo HTTP_SERVER . 'signup.html' ?>" class="login">Create a free account now</a></h5> -->
                </form>
                <div class="bbm-modal__bottombar" style="margin-top: 0px;padding: 5px; text-align: center;">
                    Forgot password? <a href="<?php echo HTTP_SERVER . 'password-reset.html' ?>" class="login">Reset your password here   </a>
                </div>
                <div class="bbm-modal__bottombar" style="margin-top: 0px;padding: 5px; text-align: center;">
                    New to My Virtual Doctor?   <a class="signup" href="<?php echo HTTP_SERVER . 'signup.html' ?>">Create a free account now</a>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(function($){
        //login form
        $('#patient_login_form').submit(function(){
            var th = $(this);
            var data = $( "#patient_login_form" ).serializeArray();

            th.find('button').attr('disabled', true);

            $.ajax({

                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=patient_login",
                data:data,
                method:"POST",
                dataType:'json'

            }).error(function () {

                th.find('button').removeAttr('disabled');

            }).done(function () {

                th.find('button').removeAttr('disabled');

            }).success(function (data) {
                console.log(data);
                //data = JQuery.parseJSON(data);
                if ( data.success ) {
                    window.location = '<?php echo HTTP_SERVER; ?>patient.html';
                }

                if ( data.error ) {
                    var str = '<div class="alert alert-danger" role="alert">';
                    $.each(data.error, function(idx, error){
                        str += '<p><strong>Error!</strong> ' + error + '</p>';
                    });
                    str += '</div>';
                    $('#patient_login_error').html(str);
                }

            }, 'json');

            return false;
        });
    });
</script>