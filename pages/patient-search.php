
<div class="tab-content">

<section class="dashboard-new-section-main patient-dashbaord-section-main clearfix tab-pane fade in active" role="tabpanel" id="dasboard">
    <div class="container">
    	
    	<div class="admin-pro hidden clearfix">
        		<div class="col-sm-2 col-md-2 text-center">
                   
                      <div  id="upload_photo_div" class="user-display-picture">
                        <img alt="1" class="avatar" src="<?php echo HTTP_SERVER; ?>images/user/full/ziku.jpeg">
            
                      </div>
                      <div class="change-photo">
                        
                        <a href="#" class="btn btn-info">Edit Profile</a>
                      </div>
           			
				</div>
                
                <div class="user-info col-sm-10 col-md-10">
                        <div class="last-updated-info pull-right">
                            <span id="last_updated_at">Last Updated On 05/28/2016</span>
                        </div>
                        <div>
                          <span class="title">Adam Nadler</span>
                        </div>
                        <div>
                          <span id="profile_percentage">Your profile is <strong>92%</strong> complete</span>
                        </div>
                        <div class="progress">
                          <div id="profile_progress_bar" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 92%;">
                            <span class="sr-only"></span>
                          </div>
                        </div>
                        <div>
                          <span class="notification" id="pc_condition">Please complete <strong><a data-remote="true" href="/patients/life_style/info">My Health</a></strong> questions</span>
                        </div>
                </div>
           
                
        </div>
    	<div class="admin-home-pro-banner clearfix" style="background: linear-gradient(rgba(187, 178, 69, 0.07), rgba(103, 103, 103, 0.06)), url(<?php echo HTTP_SERVER; ?>img/34601926.jpg) center center;background-size: cover;">
                	<div class="dr-see">
                    	<div class="dr-icon"><i class="fa fa-user-md"></i></div>
                        <div class="dr-text">
                        	<div class="dr-txt-head">See a Doctor</div>
                            <div class="dr-txt-pra">Find a Provider and make an appointment</div>
                        </div>
                    </div>
                
                <div class="user-search-section ">
                	
                    <div class="dr-search">
                    	<!-- Search Section --->
                        <form method="get" action="<?php echo HTTP_SERVER ?>search.html">
                        <div class="input-group hm-search">
                            <span class="input-group-addon">
                            <i class="fa  fa-user-md"></i></span>
                                <select name="location" data-placeholder="Select a state" class="chosen-select form-control paitent-dropdown" tabindex="-1" style="display: none;">
									<option value=""></option>
									<option value="8">Alabama</option><option value="9">Alaska</option><option value="20">Arizona</option><option value="21">Arkansas</option><option value="22">California</option><option value="26">Colorado </option><option value="27">Connecticut </option><option value="28">Delaware </option><option value="29">Florida </option><option value="30">Georgia</option><option value="31">Hawaii </option><option value="32">Idaho</option><option value="33">Illinois</option><option value="34">Indiana</option><option value="35">Iowa</option><option value="36">Kansas</option><option value="37">Kentucky</option><option value="38">Louisiana</option><option value="39">Maine </option><option value="40">Maryland </option><option value="41">Massachusetts</option><option value="42">Michigan</option><option value="43">Minnesota</option><option value="44">Mississippi</option><option value="45">Missouri</option><option value="46">Montana</option><option value="47">Nebraska </option><option value="48">Nevada </option><option value="49">New Hampshire</option><option value="50">New Jersey</option><option value="51">New Mexico </option><option value="52">New York </option><option value="53">North Carolina </option><option value="54">North Dakota</option><option value="55">Ohio </option><option value="56">Oklahoma</option><option value="57">Oregon </option><option value="58">Pennsylvania</option><option value="59">Rhode Island</option><option value="60">South Carolina</option><option value="61">South Dakota </option><option value="62">Tennessee </option><option value="63">Texas </option><option value="64">Utah </option><option value="65">Vermont </option><option value="66">Virginia </option><option value="67">Washington </option><option value="68">West Virginia </option><option value="69">Wisconsin </option><option value="70">Wyoming</option>																	</select>
                            <div class="chosen-container chosen-container-single" style="width: 264px;" title="">
                               
                                <div class="chosen-drop">
                                    <div class="chosen-search">
                                        <input type="text" autocomplete="off" tabindex="5">
                                    </div>
                                    <ul class="chosen-results"></ul>
                                </div>
                            </div>
                        </div>
                   		
                        
                        <div class="input-group hm-search">
                            <span class="input-group-addon">
                                <i class="fa fa-user-md"></i>
                            </span>
                            
                            <div class="chosen-container chosen-container-single" style="width: 270px;" title="">
                               <input type="text" class="chosen-single chosen-default wth-100" autocomplete="off" placeholder="Physicians Name">
                               <div class="chosen-drop">
                                   <div class="chosen-search">
                                        <input type="text" class="chosen-single chosen-default" autocomplete="off" tabindex="5" placeholder="Physicians Name">
                                   </div>
                                    <ul class="chosen-results"></ul>
                               </div>
                           </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Search</button>
                        </form>
                        <!--  End Search Section --->
                        
                        
                    </div>
                </div>
         </div>
  
            <div class="dnsm-list clearfix">
            
            	<div id="doctorlisting" class="searchlisting">
                 		
                        <div class="searchresult-sec">Search Result</div>
                        
                       <div id="searchResult" class="clearfix">
                                    <div class="col-md-3 col-sm-6 doctor-list clearfix">
                        	<div class="searchlist-br clearfix">
                            <div class="col-sm-12 col-md-12 col-xs-12 doctor-list-img">
                               <img src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/doctors/full/getProviderImage.png" alt="" class="img-rounded btn-block">
								
                                <div class="experince" style="display:none;">EXPERIENCE <br> <strong>16                                        YEARS</strong></div>
                                         <h4>
                                       
                                                                                    <span class="offline"><i class="fa fa-user-times"></i> Offline</span>
                                                                            </h4>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xs-12 ">
                               <div class="doctor-rating-name-section">
                                   
  <h4><a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-sherri-l-dehaas.html"> Dr. Sherri L. Dehaas</a> </h4>
                                    <div class="top_space">
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span class="glyphicon glyphicon-star"></span></div></div></span><input type="hidden" class="rating" value="0" data-readonly="" data-fractions="2">
                                                                           </div>
                                </div>
                                                            </div>
                            
                            <div class="col-sm-12 col-md-12 col-xs-12 pull-right">
                                <!--
                                <div
                                    class="col-md-10 col-sm-12 col-xs-12 text-center doctor-mini-rating-col pull-right">
                                                                            <div class="name">This Doctor Is Powered By:</div>
                                        <div class="company"><img
                                                src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png"/>
                                        </div>
                                                                    </div>
                                -->
                                
                                <button type="submit" class="btn btn-default  dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11">
                                    Consult Now
                                </button>
                            </div>
                            <div class="doctor-mini-btn-col doc-profile-detail" style="display:none;">
                                <button type="submit" class="btn btn-default dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11"><i class="fa fa-play"></i>
                                    Consult Now
                                </button>
                                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-calendar"></i>
                                    Book Appointment
                                </button>
                            </div><div class="hover-effect-view"> <a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-sherri-l-dehaas.html"><span class="btn btn-info">View Profile</span></a></div>
                        </div> </div>
                                                <div class="col-md-3 col-sm-6 doctor-list clearfix">
                        	<div class="searchlist-br clearfix">
                            <div class="col-sm-12 col-md-12 col-xs-12 doctor-list-img">
                               <img src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/doctors/full/getProviderImage.jpg" alt="" class="img-rounded btn-block">
								
                                <div class="experince" style="display:none;">EXPERIENCE <br> <strong>13                                        YEARS</strong></div>
                                         <h4>
                                       
                                                                                    <span class="online"><i class="fa fa-user"></i> Online</span>
                                                                            </h4>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xs-12 ">
                               <div class="doctor-rating-name-section">
                                   
  <h4><a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-christine-weber.html">Dr. Christine Weber</a> </h4>
                                    <div class="top_space">
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span class="glyphicon glyphicon-star"></span></div></div></span><input type="hidden" class="rating" value="0" data-readonly="" data-fractions="2">
                                                                           </div>
                                </div>
                                                            </div>
                            
                            <div class="col-sm-12 col-md-12 col-xs-12 pull-right">
                                <!--
                                <div
                                    class="col-md-10 col-sm-12 col-xs-12 text-center doctor-mini-rating-col pull-right">
                                                                            <div class="name">This Doctor Is Powered By:</div>
                                        <div class="company"><img
                                                src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png"/>
                                        </div>
                                                                    </div>
                                -->
                                
                                <button type="submit" class="btn btn-default  dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11">
                                    Consult Now
                                </button>
                            </div>
                            <div class="doctor-mini-btn-col doc-profile-detail" style="display:none;">
                                <button type="submit" class="btn btn-default dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11"><i class="fa fa-play"></i>
                                    Consult Now
                                </button>
                                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-calendar"></i>
                                    Book Appointment
                                </button>
                            </div><div class="hover-effect-view"> <a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-christine-weber.html"><span class="btn btn-info">View Profile</span></a></div>
                        </div> </div>
                                                <div class="col-md-3 col-sm-6 doctor-list clearfix">
                        	<div class="searchlist-br clearfix">
                            <div class="col-sm-12 col-md-12 col-xs-12 doctor-list-img">
                               <img src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/doctors/full/getProviderImage_1.jpg" alt="" class="img-rounded btn-block">
								
                                <div class="experince" style="display:none;">EXPERIENCE <br> <strong>20                                        YEARS</strong></div>
                                         <h4>
                                       
                                                                                    <span class="offline"><i class="fa fa-user-times"></i> Offline</span>
                                                                            </h4>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xs-12 ">
                               <div class="doctor-rating-name-section">
                                   
  <h4><a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-peter-antall.html">Dr. Peter Antall</a> </h4>
                                    <div class="top_space">
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span class="glyphicon glyphicon-star"></span></div></div></span><input type="hidden" class="rating" value="0" data-readonly="" data-fractions="2">
                                                                           </div>
                                </div>
                                                            </div>
                            
                            <div class="col-sm-12 col-md-12 col-xs-12 pull-right">
                                <!--
                                <div
                                    class="col-md-10 col-sm-12 col-xs-12 text-center doctor-mini-rating-col pull-right">
                                                                            <div class="name">This Doctor Is Powered By:</div>
                                        <div class="company"><img
                                                src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png"/>
                                        </div>
                                                                    </div>
                                -->
                                
                                <button type="submit" class="btn btn-default  dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11">
                                    Consult Now
                                </button>
                            </div>
                            <div class="doctor-mini-btn-col doc-profile-detail" style="display:none;">
                                <button type="submit" class="btn btn-default dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11"><i class="fa fa-play"></i>
                                    Consult Now
                                </button>
                                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-calendar"></i>
                                    Book Appointment
                                </button>
                            </div><div class="hover-effect-view"> <a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-peter-antall.html"><span class="btn btn-info">View Profile</span></a></div>
                        </div> </div>
                                                <div class="col-md-3 col-sm-6 doctor-list clearfix">
                        	<div class="searchlist-br clearfix">
                            <div class="col-sm-12 col-md-12 col-xs-12 doctor-list-img">
                               <img src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/doctors/full/getProviderImage_2.jpg" alt="" class="img-rounded btn-block">
								
                                <div class="experince" style="display:none;">EXPERIENCE <br> <strong>21                                        YEARS</strong></div>
                                         <h4>
                                       
                                                                                    <span class="online"><i class="fa fa-user"></i> Online</span>
                                                                            </h4>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xs-12 ">
                               <div class="doctor-rating-name-section">
                                   
  <h4><a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-ingrid-antall.html">Dr. Ingrid Antall</a> </h4>
                                    <div class="top_space">
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: hidden;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;"><span class="glyphicon glyphicon-star"></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: hidden;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;"><span class="glyphicon glyphicon-star"></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: hidden;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;"><span class="glyphicon glyphicon-star"></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 25%;"><span class="glyphicon glyphicon-star"></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div></span><input type="hidden" class="rating" value="3.25" data-readonly="" data-fractions="2">
                                                                           </div>
                                </div>
                                                            </div>
                            
                            <div class="col-sm-12 col-md-12 col-xs-12 pull-right">
                                <!--
                                <div
                                    class="col-md-10 col-sm-12 col-xs-12 text-center doctor-mini-rating-col pull-right">
                                                                            <div class="name">This Doctor Is Powered By:</div>
                                        <div class="company"><img
                                                src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png"/>
                                        </div>
                                                                    </div>
                                -->
                                
                                <button type="submit" class="btn btn-default  dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11">
                                    Consult Now
                                </button>
                            </div>
                            <div class="doctor-mini-btn-col doc-profile-detail" style="display:none;">
                                <button type="submit" class="btn btn-default dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11"><i class="fa fa-play"></i>
                                    Consult Now
                                </button>
                                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-calendar"></i>
                                    Book Appointment
                                </button>
                            </div><div class="hover-effect-view"> <a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-ingrid-antall.html"><span class="btn btn-info">View Profile</span></a></div>
                        </div> </div>
                                                <div class="col-md-3 col-sm-6 doctor-list clearfix">
                        	<div class="searchlist-br clearfix">
                            <div class="col-sm-12 col-md-12 col-xs-12 doctor-list-img">
                               <img src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/doctors/full/getProviderImage_3.jpg" alt="" class="img-rounded btn-block">
								
                                <div class="experince" style="display:none;">EXPERIENCE <br> <strong>22                                        YEARS</strong></div>
                                         <h4>
                                       
                                                                                    <span class="offline"><i class="fa fa-user-times"></i> Offline</span>
                                                                            </h4>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xs-12 ">
                               <div class="doctor-rating-name-section">
                                   
  <h4><a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-lisa-thompson.html">Dr. Lisa Thompson</a> </h4>
                                    <div class="top_space">
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: hidden;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;"><span class="glyphicon glyphicon-star"></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: hidden;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;"><span class="glyphicon glyphicon-star"></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: hidden;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;"><span class="glyphicon glyphicon-star"></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: hidden;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;"><span class="glyphicon glyphicon-star"></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 50%;"><span class="glyphicon glyphicon-star"></span></div></div></span><input type="hidden" class="rating" value="4.5" data-readonly="" data-fractions="2">
                                                                           </div>
                                </div>
                                                            </div>
                            
                            <div class="col-sm-12 col-md-12 col-xs-12 pull-right">
                                <!--
                                <div
                                    class="col-md-10 col-sm-12 col-xs-12 text-center doctor-mini-rating-col pull-right">
                                                                            <div class="name">This Doctor Is Powered By:</div>
                                        <div class="company"><img
                                                src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png"/>
                                        </div>
                                                                    </div>
                                -->
                                
                                <button type="submit" class="btn btn-default  dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11">
                                    Consult Now
                                </button>
                            </div>
                            <div class="doctor-mini-btn-col doc-profile-detail" style="display:none;">
                                <button type="submit" class="btn btn-default dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11"><i class="fa fa-play"></i>
                                    Consult Now
                                </button>
                                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-calendar"></i>
                                    Book Appointment
                                </button>
                            </div><div class="hover-effect-view"> <a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-lisa-thompson.html"><span class="btn btn-info">View Profile</span></a></div>
                        </div> </div>
                                                <div class="col-md-3 col-sm-6 doctor-list clearfix">
                        	<div class="searchlist-br clearfix">
                            <div class="col-sm-12 col-md-12 col-xs-12 doctor-list-img">
                               <img src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/doctors/full/getProviderImage_1.png" alt="" class="img-rounded btn-block">
								
                                <div class="experince" style="display:none;">EXPERIENCE <br> <strong>5                                        YEARS</strong></div>
                                         <h4>
                                       
                                                                                    <span class="online"><i class="fa fa-user"></i> Online</span>
                                                                            </h4>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xs-12 ">
                               <div class="doctor-rating-name-section">
                                   
  <h4><a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr--mia-zakaria.html">DR.  Mia Zakaria</a> </h4>
                                    <div class="top_space">
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span class="glyphicon glyphicon-star"></span></div></div></span><input type="hidden" class="rating" value="0" data-readonly="" data-fractions="2">
                                                                           </div>
                                </div>
                                                            </div>
                            
                            <div class="col-sm-12 col-md-12 col-xs-12 pull-right">
                                <!--
                                <div
                                    class="col-md-10 col-sm-12 col-xs-12 text-center doctor-mini-rating-col pull-right">
                                                                            <div class="name">This Doctor Is Powered By:</div>
                                        <div class="company"><img
                                                src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png"/>
                                        </div>
                                                                    </div>
                                -->
                                
                                <button type="submit" class="btn btn-default  dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11">
                                    Consult Now
                                </button>
                            </div>
                            <div class="doctor-mini-btn-col doc-profile-detail" style="display:none;">
                                <button type="submit" class="btn btn-default dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11"><i class="fa fa-play"></i>
                                    Consult Now
                                </button>
                                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-calendar"></i>
                                    Book Appointment
                                </button>
                            </div><div class="hover-effect-view"> <a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr--mia-zakaria.html"><span class="btn btn-info">View Profile</span></a></div>
                        </div> </div>
                                                <div class="col-md-3 col-sm-6 doctor-list clearfix">
                        	<div class="searchlist-br clearfix">
                            <div class="col-sm-12 col-md-12 col-xs-12 doctor-list-img">
                               <img src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/doctors/full/getProviderImage_2.png" alt="" class="img-rounded btn-block">
								
                                <div class="experince" style="display:none;">EXPERIENCE <br> <strong>18                                        YEARS</strong></div>
                                         <h4>
                                       
                                                                                    <span class="offline"><i class="fa fa-user-times"></i> Offline</span>
                                                                            </h4>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xs-12 ">
                               <div class="doctor-rating-name-section">
                                   
  <h4><a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-sandra-k-wiita.html">Dr. Sandra K. Wiita</a> </h4>
                                    <div class="top_space">
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span class="glyphicon glyphicon-star"></span></div></div></span><input type="hidden" class="rating" value="0" data-readonly="" data-fractions="2">
                                                                           </div>
                                </div>
                                                            </div>
                            
                            <div class="col-sm-12 col-md-12 col-xs-12 pull-right">
                                <!--
                                <div
                                    class="col-md-10 col-sm-12 col-xs-12 text-center doctor-mini-rating-col pull-right">
                                                                            <div class="name">This Doctor Is Powered By:</div>
                                        <div class="company"><img
                                                src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png"/>
                                        </div>
                                                                    </div>
                                -->
                                
                                <button type="submit" class="btn btn-default  dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11">
                                    Consult Now
                                </button>
                            </div>
                            <div class="doctor-mini-btn-col doc-profile-detail" style="display:none;">
                                <button type="submit" class="btn btn-default dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11"><i class="fa fa-play"></i>
                                    Consult Now
                                </button>
                                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-calendar"></i>
                                    Book Appointment
                                </button>
                            </div><div class="hover-effect-view"> <a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-sandra-k-wiita.html"><span class="btn btn-info">View Profile</span></a></div>
                        </div> </div>
                                                <div class="col-md-3 col-sm-6 doctor-list clearfix">
                        	<div class="searchlist-br clearfix">
                            <div class="col-sm-12 col-md-12 col-xs-12 doctor-list-img">
                               <img src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/doctors/full/getProviderImage_4.jpg" alt="" class="img-rounded btn-block">
								
                                <div class="experince" style="display:none;">EXPERIENCE <br> <strong>13                                        YEARS</strong></div>
                                         <h4>
                                       
                                                                                    <span class="online"><i class="fa fa-user"></i> Online</span>
                                                                            </h4>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xs-12 ">
                               <div class="doctor-rating-name-section">
                                   
  <h4><a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-asit-vora.html"> Dr. Asit Vora</a> </h4>
                                    <div class="top_space">
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span class="glyphicon glyphicon-star"></span></div></div></span><input type="hidden" class="rating" value="0" data-readonly="" data-fractions="2">
                                                                           </div>
                                </div>
                                                            </div>
                            
                            <div class="col-sm-12 col-md-12 col-xs-12 pull-right">
                                <!--
                                <div
                                    class="col-md-10 col-sm-12 col-xs-12 text-center doctor-mini-rating-col pull-right">
                                                                            <div class="name">This Doctor Is Powered By:</div>
                                        <div class="company"><img
                                                src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png"/>
                                        </div>
                                                                    </div>
                                -->
                                
                                <button type="submit" class="btn btn-default  dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11">
                                    Consult Now
                                </button>
                            </div>
                            <div class="doctor-mini-btn-col doc-profile-detail" style="display:none;">
                                <button type="submit" class="btn btn-default dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11"><i class="fa fa-play"></i>
                                    Consult Now
                                </button>
                                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-calendar"></i>
                                    Book Appointment
                                </button>
                            </div><div class="hover-effect-view"> <a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-asit-vora.html"><span class="btn btn-info">View Profile</span></a></div>
                        </div> </div>
                                                <div class="col-md-3 col-sm-6 doctor-list clearfix">
                        	<div class="searchlist-br clearfix">
                            <div class="col-sm-12 col-md-12 col-xs-12 doctor-list-img">
                               <img src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/doctors/full/getProviderImage_5.jpg" alt="" class="img-rounded btn-block">
								
                                <div class="experince" style="display:none;">EXPERIENCE <br> <strong>11                                        YEARS</strong></div>
                                         <h4>
                                       
                                                                                    <span class="offline"><i class="fa fa-user-times"></i> Offline</span>
                                                                            </h4>
                            </div>
                            <div class="col-sm-12 col-md-12 col-xs-12 ">
                               <div class="doctor-rating-name-section">
                                   
  <h4><a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-margaret-miller.html"> Dr Margaret Miller</a> </h4>
                                    <div class="top_space">
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span class="glyphicon glyphicon-star"></span></div></div></span><input type="hidden" class="rating" value="0" data-readonly="" data-fractions="2">
                                                                           </div>
                                </div>
                                                            </div>
                            
                            <div class="col-sm-12 col-md-12 col-xs-12 pull-right">
                                <!--
                                <div
                                    class="col-md-10 col-sm-12 col-xs-12 text-center doctor-mini-rating-col pull-right">
                                                                            <div class="name">This Doctor Is Powered By:</div>
                                        <div class="company"><img
                                                src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png"/>
                                        </div>
                                                                    </div>
                                -->
                                
                                <button type="submit" class="btn btn-default  dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11">
                                    Consult Now
                                </button>
                            </div>
                            <div class="doctor-mini-btn-col doc-profile-detail" style="display:none;">
                                <button type="submit" class="btn btn-default dark-pink consult_now" data-src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/images/category/full/Amwell_.png" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11"><i class="fa fa-play"></i>
                                    Consult Now
                                </button>
                                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-calendar"></i>
                                    Book Appointment
                                </button>
                            </div><div class="hover-effect-view"> <a href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor/dr-margaret-miller.html"><span class="btn btn-info">View Profile</span></a></div>
                        </div> </div>
                                            </div>
               </div>
            
        </div>
    </div>
</section>

</div>

<script>
$('.nav-pills div a').click(function (e) {
  $('.nav-pills div.active').removeClass('active')
  $(this).parent('div').addClass('active')
});

	
$('#provider-btn').click(function() {
	$('.provider-btn-div').css('display','block');	
});

$('#healthconditions-yes').click(function() {
	$('#healthconditions-div').css('display','block');	
});
$('#healthconditions-no').click(function() {
	$('#healthconditions-div').css('display','none');	
});

$('#medications-yes').click(function() {
	$('#medications-div').css('display','block');	
});
$('#healthconditions-no').click(function() {
	$('#medications-div').css('display','none');	
});

$('#allergies-yes').click(function() {
	$('#allergies-div').css('display','block');	
});
$('#allergies-no').click(function() {
	$('#allergies-div').css('display','none');	
});

$('#surgeries-yes').click(function() {
	$('#surgeries-div').css('display','block');	
});
$('#surgeries-no').click(function() {
	$('#surgeries-div').css('display','none');	
});


$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'top'
    });   
/* end dot nav */
});

</script>