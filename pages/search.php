<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

$search_name = "";
$search_location = "";
if ( isset($_GET['searchName']) && $_GET['searchName'] != '' ) {
    $search_name = sanitize( $_GET['searchName'], 3 );
}

if ( isset($_GET['location']) && $_GET['location'] != '' ) {
    $search_location = intval( $_GET['location'] );
}

//get category data
$location_data = array();

try {
    $query = "SELECT id, categoryName, categoryType, categoryFeatured FROM " . DB_PREFIX . "category WHERE `categoryType` = 'location' AND categoryStatus = 1 order by `categoryOrder` asc";
    $result = $db->prepare($query);
    $result->execute();

    if ( $result->rowCount() > 0 ) {
        $location_data = $result->fetchAll();
    }

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}


//get latest 20 doctors profile
$arr = array();

$select = "SELECT doc.id, doc.url, doc.doctor_name, doc.specialization, doc.degree, doc.fees, doc.location, doc.experience,
                 doc.sex, doc.featured_image, doc.ratings, doc.ratingCount, cat.categoryImage, cat.categoryName, cat.categoryDesc";
$select = "SELECT doc.id, doc.url, doc.doctor_name, doc.specialization, doc.degree, doc.fees, doc.location, doc.experience,
                 doc.sex, doc.featured_image, doc.ratings, doc.ratingCount";

$from = " FROM `" . DB_PREFIX . "doctors` as doc, `" . DB_PREFIX . "category` as cat, `" . DB_PREFIX . "category_data` as cat_data";
$from = " FROM `" . DB_PREFIX . "doctors` as doc";

$where = " WHERE cat.id = cat_data.categoryId AND doc.id = cat_data.postId AND cat.categoryType='company' AND doc.`status` = 'publish'";
$where = " WHERE doc.`status` = 'publish'";

$order = ' ORDER BY doc.id asc';

$limit = ' LIMIT 0,12';

if ( ( is_numeric($search_location) && $search_location > 0 ) ) {
    $where .= " AND exists (select * from `" . DB_PREFIX . "category_data` as c where c.postId=doc.id AND c.categoryType='location' AND c.categoryId = ?)";
    $arr[] = $search_location;
}

if ( $search_name != '' ) {
    $search_name1 = '%' . $search_name . '%';
    $where .= ' AND doc.doctor_name like ?';
    $arr[] = $search_name1;
}

$query = $select . $from . $where . $order . $limit;

try {
    $result = $db->prepare( $query );

    if ( !empty($arr) ) {
        $result->execute( $arr );
    } else {
        $result->execute( );
    }

    $search_data = $result->fetchAll();

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}

//get total row count
$query2 = "SELECT count(doc.id) as cnt" . $from . $where;

try {
    $result = $db->prepare( $query2 );

    if ( !empty($arr) ) {
        $result->execute( $arr );
    } else {
        $result->execute( );
    }

    $total_count = $result->fetch();
    $total_count = $total_count['cnt'];

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}

?>

<div class="tab-content">

    <section class="dashboard-new-section-main patient-dashbaord-section-main clearfix tab-pane fade in active" role="tabpanel" id="dasboard">
        <div class="container">
            <div class="admin-home-pro-banner clearfix" style="background: linear-gradient(rgba(187, 178, 69, 0.07), rgba(103, 103, 103, 0.06)), url(<?php echo HTTP_SERVER; ?>img/34601926.jpg) center center;background-size: cover;">
                <div class="dr-see">
                    <div class="dr-icon"><i class="fa fa-user-md"></i></div>
                    <div class="dr-text">
                        <div class="dr-txt-head">See a Doctor</div>
                        <div class="dr-txt-pra">Find a Provider and make an appointment</div>
                    </div>
                </div>

                <div class="user-search-section ">

                    <div class="dr-search">
                        <!-- Search Section --->
                        <form method="get" action="<?php echo HTTP_SERVER ?>search.html" id="search_form">
                            <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                            <div class="input-group hm-search">
                            <span class="input-group-addon"><i class="fa  fa-user-md"></i></span>
                                <select name="category[location][]" id="location_top" data-placeholder="Select a state" class="chosen-select form-control paitent-dropdown" tabindex="-1" style="display: none;">
                                    <option value=""></option>
                                    <?php if ( !empty( $location_data )): ?>
                                        <?php
                                        foreach ( $location_data as $item ) {
                                            $sel = ($item['id'] == $search_location) ? 'selected="selected"' : '';
                                            echo "<option value='{$item['id']}' $sel>{$item['categoryName']}</option>";
                                        }
                                        ?>
                                    <?php endif; ?>
                                </select>
                                <div class="chosen-container chosen-container-single" style="width: 264px;" title="">

                                    <div class="chosen-drop">
                                        <div class="chosen-search">
                                            <input type="text" autocomplete="off" tabindex="5">
                                        </div>
                                        <ul class="chosen-results"></ul>
                                    </div>
                                </div>
                            </div>


                            <div class="input-group hm-search">
                            <span class="input-group-addon">
                                <i class="fa fa-user-md"></i>
                            </span>

                                <div class="chosen-container chosen-container-single" style="width: 270px;" title="">
                                    <input type="text" name="searchName" value="<?php echo $search_name; ?>" class="chosen-single chosen-default wth-100" autocomplete="off" placeholder="Physicians Name" />
                                    <div class="chosen-drop">
                                        <div class="chosen-search">
                                            <input type="text" class="chosen-single chosen-default"  tabindex="5" autocomplete="off" tabindex="5" placeholder="Physicians Name">
                                        </div>
                                        <ul class="chosen-results"></ul>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary" id="searchByName">Search</button>
                        </form>
                        <!--  End Search Section --->

                    </div>
                </div>
            </div>

            <div class="dnsm-list clearfix">

                <div id="doctorlisting" class="searchlisting">

                    <div class="searchresult-sec">Search Result</div>


                    <div id="search_result">
                        <div id='searchResult' class="clearfix">
                            <?php
                            if ( !empty($search_data) ) {
                                $i = 0;
                                //echo "<div id='searchResult'>";
                                foreach ($search_data as $data):
                                    if($i == 3){
                                        //echo '<div class="clearfix hidden-sm hidden-xs"><hr /></div>'; $i = 0;
                                    }
                                    $i++;
                                ?>

                                    <div class="col-md-3 col-sm-6 doctor-list clearfix">
                                        <div class="searchlist-br clearfix">
                                            <div class="col-sm-12 col-md-12 col-xs-12 doctor-list-img">
                                                <img src="<?php echo HTTP_SERVER . 'images/doctors/full/' . $data['featured_image']; ?>" alt="<?php echo $data['doctor_name']; ?>" class="img-rounded btn-block">
                                                <div class="experince" style="display:none;">EXPERIENCE <br> <strong><?php echo $data['experience']; ?> YEARS</strong></div>
                                                <h4>
                                                    <?php if ($i % 2 == 0): ?>
                                                        <span class="online"><i class="fa fa-user"></i> Online</span>
                                                    <?php else: ?>
                                                        <span class="offline"><i class="fa fa-user-times"></i> Offline</span>
                                                    <?php endif; ?>
                                                </h4>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                <div class="doctor-rating-name-section">
                                                    <h4><a href="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>"> <?php echo $data['doctor_name']; ?></a> </h4>
                                                    <div class="top_space">
                                                        <input type="hidden" class="rating" value="<?php echo $data['ratings']; ?>" data-readonly data-fractions="2"/>
                                                        <?php /*?>  <small class="clearfix"><?php echo $data['ratings']; ?> Star based on <?php echo $data['ratingCount']; ?> Reviews </small><?php */?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-12 col-md-12 col-xs-12" style="text-align: center;">
                                                <?php /*
                                                <div class="col-md-10 col-sm-12 col-xs-12 text-center doctor-mini-rating-col pull-right">
                                                    <?php if ($data['categoryImage']) { ?>
                                                        <div class="name">This Doctor Is Powered By:</div>
                                                        <div class="company"><img
                                                                src="<?php echo HTTP_SERVER . 'images/category/full/' . $data['categoryImage']; ?>"/>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                */ ?>
                                                <!--
                                                <a type="submit" class="btn btn-default  dark-pink consult_now" href="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>" data-name="Amwell" data-url="http://www.amwell.com">
                                                    Consult Now
                                                </a> -->
                                            </div>
                                            <div class="doctor-mini-btn-col doc-profile-detail" style="display:none;">
                                                <button type="submit" class="btn btn-default dark-pink consult_now" data-src="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11"><i class="fa fa-play"></i>
                                                    Consult Now
                                                </button>
                                                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-calendar"></i>
                                                    Book Appointment
                                                </button>
                                            </div>
                                            <div class="hover-effect-view">
                                                <a href="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>"><span class="btn btn-info">View Profile</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                endforeach;
                                //echo "</div>";
                            } else { ?>

                                <h2 class="no-results">No Results Found...</h2>

                            <?php } ?>
                        </div>

                        <!-- show load more button -->
                        <div style="text-align: center;" id="loadMoreDiv">
                            <div class="col-md-12" style=" margin: 0px 0px 30px;"> <button type="submit" class="btn btn-default btn-block dark-pink" id="loadMore" data-page="1" style="padding: 8px 10px;"><i class="fa fa-chevron-down"></i> More Doctors <i class="fa fa-chevron-down"></i></button></div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </section>

</div>

<!-- Modal -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;">
                <div class="loding-more" id="modal_image"></div>
                <div class="loding-more">
                    <p>My Virtual Doctor is sending you to <span id="company_url"></span></p>
                </div>
                <p style="color: #bcbcbc;">You are being redirected to begin your consult. Thank you for using My Virtual Doctor.</p>
                <div class="loding-bar">
                    <img src="img/heart.gif" />
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(function() {

        function call_ajax( page ) {
            var load = true;
            if ( typeof page === 'undefined' ) {
                page = 0;
                load = false;
            }

            var data = $( "#search_form" ).serializeArray();
            data.push({ name: "page_now", value: page });

            $('input[type="checkbox"]').attr('disabled', true);

            $.ajax({
                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=search_form",
                data: data,
                method: "POST",
                dataType: "json"
            }).error(function (err) {
                $('input[type="checkbox"]').attr('disabled', false);
                $('#loadMore').removeAttr('disabled');
                console.log(err);

            }).done(function () {
                $('input[type="checkbox"]').attr('disabled', false);
                $('#loadMore').removeAttr('disabled');

            }).success(function (data) {
                if ( data.success ) {

                    if (data.search_result) {
                        var str = "";
                        var i = 0, j = 0;
                        $.each(data.search_result, function( idx, result ) {

                            if(i == 3){
                                //str += '<div class="clearfix hidden-sm hidden-xs"><hr /></div>';
                                i = 0;
                            }
                            i++;
                            j++;

                            str += '<div class="col-md-3 col-sm-6 doctor-list clearfix"> \
                                <div class="searchlist-br clearfix"> \
                                <div class="col-sm-12 col-md-12 col-xs-12 doctor-list-img"> \
                                <img src="<?php echo HTTP_SERVER . 'images/doctors/full/' ?>' + result.featured_image +'" alt="' + result.doctor_name + '" class="img-rounded btn-block"> \
                                <div class="experince" style="display:none;">EXPERIENCE <br> <strong>' + result.experience + ' YEARS</strong></div> \
                                <h4>';
                                if (i % 2 == 0) {
                                    str += '<span class="online"><i class="fa fa-user"></i> Online</span>';
                                } else {
                                    str += '<span class="offline"><i class="fa fa-user-times"></i> Offline</span>';
                                }
                                str += '</h4>\
                                </div>\
                                <div class="col-sm-12 col-md-12 col-xs-12 ">\
                                <div class="doctor-rating-name-section">\
                                <h4><a href="<?php echo HTTP_SERVER . 'doctor/'; ?>' +  result.url + '.html">' + result.doctor_name + '</a> </h4> \
                                <div class="top_space"> \
                                <input type="hidden" class="rating" value="' + result.ratings +'" data-readonly data-fractions="2"/> \
                                <?php /*?>  <small class="clearfix">' + result.ratings + ' Star based on '+ result.ratingCount + ' Reviews </small><?php */?> \
                                </div>\
                                </div>\
                                </div>\
                                <div class="col-sm-12 col-md-12 col-xs-12" style="text-align: center;">\
                                <?php /*
                                <div class="col-md-10 col-sm-12 col-xs-12 text-center doctor-mini-rating-col pull-right">
                                    <?php if ($data['categoryImage']) { ?>
                                        <div class="name">This Doctor Is Powered By:</div>
                                        <div class="company"><img
                                                src="<?php echo HTTP_SERVER . 'images/category/full/' . $data['categoryImage']; ?>"/>
                                        </div>
                                    <?php } ?>
                                </div>
                                */ ?>\
                                <?php /*<a type="submit" class="btn btn-default  dark-pink consult_now" href="<?php echo HTTP_SERVER . 'doctor/'; ?>' + result.url + '.html" data-name="Amwell" data-url="http://www.amwell.com">\
                                Consult Now */ ?>\
                            </a>\
                            </div>\
                            <div class="doctor-mini-btn-col doc-profile-detail" style="display:none;">\
                                <button type="submit" class="btn btn-default dark-pink consult_now" data-src="<?php echo HTTP_SERVER . 'doctor/'; ?>' + result.url + '.html" data-name="Amwell" data-url="http://www.amwell.com" data-toggle="modal" data-target="#myModal11"><i class="fa fa-play"></i>\
                                Consult Now\
                            </button>\
                            <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-calendar"></i>\
                                Book Appointment\
                            </button>\
                            </div>\
                            <div class="hover-effect-view">\
                                <a href="<?php echo HTTP_SERVER . 'doctor/'; ?>' + result.url + '.html"><span class="btn btn-info">View Profile</span></a>\
                            </div>\
                            </div>\
                            </div>';
                        });



                        if ( data.page_now && data.page_now > 0 ) {
                            $('#loadMore').data('page', data.page_now);
                            $('#loadMoreDiv').show()
                        }

                        if ( j < 8 ) {
                            $('#loadMoreDiv').hide();
                        }

                        if ( load === false ) {
                            $('#searchResult').html(str);
                        } else {
                            $('#searchResult').append(str);
                        }

                        $('input.rating').rating();
                    }
                    else if ( data.message ) {
                        $('#searchResult').html('<h2 class="no-results">' + data.message + '</h2>');
                        $('#loadMoreDiv').hide()
                    }
                    else if ( data.message_end ) {
                        $('#loadMoreDiv').hide();
                    }
                }
                else if(data.error) {
                    $('#searchResult').html('<h2 class="no-results">' + data.error + '</h2>');
                }
                else {
                    alert('Unknown Error!');
                }
            }, 'json');
        }

        $('#searchResult').on('click', '.consult_now', function(){
            $('#company_url').html( $(this).data('url') );
            $('#modal_image').html('<img src="'+ $(this).data('src') +'" style="height: 56px; text-align: center;">')

        });

        $('#loadMoreDiv').on('click', '#loadMore', function() {
            var page = $(this).data('page');

            //disable me
            $(this).attr('disabled','disabled');

            //call ajax function
            call_ajax(page);

        });

        $('.all').click(function() {
            var th = $(this);

            if( th.prop('checked') ) {
                // something when checked
                th.parent().parent().parent().find('input[type="checkbox"]').prop('checked', false);
                th.prop('checked', true);

                //call ajax
                call_ajax();

            } else {
                // something else when not
                call_ajax();
            }
        });

        $('input[type="checkbox"]').click(function(){
            var th = $(this);

            if ( th.hasClass('all') ) {
                return;
            }

            if( th.prop('checked') ) {
                // something when checked
                th.parent().parent().parent().find('input.all').prop('checked', false);
                th.prop('checked', true);
                call_ajax();
            } else {
                // something else when not
                call_ajax();
            }


        });

        $(window).bind('resize load', function() {
            if ($(this).width() < 767) {
                $('#refine-search').removeClass('in');
                $('#refine-search').addClass('out');
            } else {
                $('#refine-search').removeClass('out');
                $('#refine-search').addClass('in');
            }
        });

        $("#distance").slider({min  : 5, max  : 100, value: [5, 100], tooltip: 'hide', labelledby: ['distance-a', 'distance-b']});
        $("#distance").on("slide", function(slideEvt) {
            var val = "" + slideEvt.value;
            val = val.replace(',', ' - ');
            $("#distance_val").text(val);
        });
        $("#distance").on("slideStop", function(slideEvt) {
            call_ajax();
        });

        $("#experience").slider({min  : 0, max  : 50, value: [0, 50], tooltip: 'hide', labelledby: ['experience-a', 'experience-b']});
        $("#experience").on("slide", function(slideEvt) {
            var val = "" + slideEvt.value;
            val = val.replace(',', ' - ');
            $("#experience_val").text(val);
        });
        $("#experience").on("slideStop", function(slideEvt) {
            call_ajax();
        });

        $('#ratings').on('change', function () {
            $("#ratings_val").text($(this).val());
            call_ajax();
        });

        $('.clear_ratings').click(function(){
            $('#ratings').rating('rate', 0);
            $("#ratings_val").text('');
            call_ajax();
        });

        $("#location_top").chosen().change( function(){
            var val = $(this).val();

            $('#location-main').find('input[type="checkbox"]').prop('checked', false);

            if ( val != '' ) {
                $('#location_'+val).prop('checked', true)
            } else {
                $('#location-main').find('input.all').prop('checked', true);
            }

            call_ajax();
        });

        $("#speciality_top").chosen().change( function(){
            var val = $(this).val();

            $('#speciality-main').find('input[type="checkbox"]').prop('checked', false);

            if ( val != '' ) {
                $('#speciality_'+val).prop('checked', true)
            } else {
                $('#speciality-main').find('input.all').prop('checked', true);
            }

            call_ajax();
        });

        $('body').on('click', '#reset_filters', function( e ){
            e.preventDefault();
            //clear all checkbox
            $('body').find('input[type="checkbox"]').prop('checked', false);
            $('body').find('input.all').prop('checked', true);

            //clear distance filters
            $('#ratings').rating('rate', 0);
            $("#ratings_val").text('');

            //reset experience slider
            $('#experience').slider('destroy');
            $("#experience").slider({min  : 0, max  : 50, value: [0, 50], tooltip: 'hide', labelledby: ['experience-a', 'experience-b']});
            $("#experience_val").text('');
            $("#experience").on("slide", function(slideEvt) {
                var val = "" + slideEvt.value;
                val = val.replace(',', ' - ');
                $("#experience_val").text(val);
            });
            $("#experience").on("slideStop", function(slideEvt) {
                call_ajax();
            });


            //reset select box
            //will do it later

            //now call ajax function
            call_ajax();
        });

        $('body').on('click', '#searchByName', function( e ) {
            e.preventDefault();

            //call ajax
            call_ajax();
        });


    });
</script>