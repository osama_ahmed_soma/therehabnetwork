<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}?>
<section id="banner">
    <div class="container">
        <div class="row">
            <?php
            if ( !isset($fb) ) {
                $fb_config = [
                    'app_id' => '558726107616262',
                    'app_secret' => '4c571b508a5e25aa5c81031748e82426',
                    'default_graph_version' => 'v2.2',
                ];
                $fb = new Facebook\Facebook( $fb_config );
            }

            $helper = $fb->getRedirectLoginHelper();

            try {
                $accessToken = $helper->getAccessToken();
            } catch(Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch(Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            if (! isset($accessToken)) {
                if ($helper->getError()) {
                    header('HTTP/1.0 401 Unauthorized');
                    echo "Error: " . $helper->getError() . "\n";
                    echo "Error Code: " . $helper->getErrorCode() . "\n";
                    echo "Error Reason: " . $helper->getErrorReason() . "\n";
                    echo "Error Description: " . $helper->getErrorDescription() . "\n";
                } else {
                    //header('HTTP/1.0 400 Bad Request');
                    echo 'Bad request';
                }
                exit;
            }

            // The OAuth 2.0 client handler helps us manage access tokens
            $oAuth2Client = $fb->getOAuth2Client();

            // Get the access token metadata from /debug_token
            $tokenMetadata = $oAuth2Client->debugToken($accessToken);

            // Validation (these will throw FacebookSDKException's when they fail)
            $tokenMetadata->validateAppId($fb_config['app_id']);
            // If you know the user ID this access token belongs to, you can validate it here
            //$tokenMetadata->validateUserId('123');
            $tokenMetadata->validateExpiration();


            if (! $accessToken->isLongLived()) {
                // Exchanges a short-lived access token for a long-lived one
                try {
                    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                    exit;
                }
                //$accessToken->getValue()
            }

            $_SESSION['fb_access_token'] = (string) $accessToken;


            try {
                // Returns a `Facebook\FacebookResponse` object
                $response = $fb->get('/me?fields=id,email,first_name,gender,last_name', $accessToken->getValue());
                $user = $response->getGraphUser();

                if ( $user['gender'] == 'male' ) {
                    $user['gender'] = 1;
                } else {
                    $user['gender'] = 2;
                }

            } catch(Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch(Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            if ( $user['id'] ) {
                $profile_url = HTTP_SERVER . 'patient.html';
                try {
                    $sql = "SELECT * FROM " . DB_PREFIX . "users WHERE fb_user_id = ? AND userLevel = 4 LIMIT 1";

                    $result = $db->prepare($sql);
                    $result->execute( array($user['id']) );

                    if ( $result->rowCount() > 0 ) {
                        $row = $result->fetch();

                        $_SESSION["mvdoctorVisitorUserFname"] = $row['userFname'];
                        $_SESSION["mvdoctorVisitorUserLname"] = $row['userLname'];
                        $_SESSION["mvdoctorVisitorUserLevel"] = $row['userLevel'];
                        $_SESSION["mvdoctorVisitorUserTitle"] = $row['userTitle'];
                        $_SESSION["mvdoctorVisitornUserId"]   = $row['userId'];
                        $_SESSION["mvdoctorVisitorUserEmail"] = $row['userEmail'];

                        //redirect to account page
                        redirectPage($profile_url);

                    } else {
                        //create new account
                        $options = [
                            'cost' => 11,
                            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
                        ];

                        $randomPassword = substr(session_id(),0,8);
                        $user_key = getRandomString(20);
                        $new_pass = password_hash(SITE_KEY . $randomPassword . $user_key, PASSWORD_DEFAULT, $options);

                        try {
                            /* create new password */
                            $query = "INSERT INTO `" . DB_PREFIX . "users`  (`userLogin`, `fb_user_id`, `userKey`, `userPassword`, `userFname`, `userLname`, `userGender`, `userEmail`, `userLevel`, `userStatus`, `added`)
                            VALUES (?, ?, ?, ?,  ?, ?, ?, ?, 4, 1, ?)";
                            $result = $db->prepare($query);
                            $result->execute(array($user['email'], $user['id'], $user_key, $new_pass, $user['first_name'], $user['last_name'], $user['gender'], $user['email'], time()));

                            //start new session

                            $_SESSION["mvdoctorVisitorUserFname"] = $user['first_name'];
                            $_SESSION["mvdoctorVisitorUserLname"] = $user['last_name'];
                            $_SESSION["mvdoctorVisitorUserLevel"] = '4';
                            $_SESSION["mvdoctorVisitorUserTitle"] = '';
                            $_SESSION["mvdoctorVisitornUserId"]   = $db->lastInsertId();
                            $_SESSION["mvdoctorVisitorUserEmail"] = $user['email'];

                            //sending email
                            // additional header pieces for errors, From cc's, bcc's, etc
                            $headers = "From: ". CONTACT_NAME ." <". CONTACT_EMAIL .">\r\n";
                            $headers.="Content-type: text/plain; charset=iso-8859-1\r\n";

                            //subject
                            $subject = "Your My Virtual Doctor Account Has Been Created";

                            //message
                            $message = "Dear {$user['first_name']} {$user['last_name']} \n\n";
                            $message .= "Your My Virtual Doctor account has been created.\n\n Your user name is: \n\n {$user['email']}\n\n Your Password is: \n\n";
                            $message .= "$randomPassword \n\n";
                            $message .= "Please use the new username and password above to login. You can also login via Facebook login option. You can always change your password after logging in to the system";

                            if ( mail($user['email'],$subject,$message,$headers) ) {
                                //redirect to patient page
                                redirectPage($profile_url);

                            } else {
                                echo "<h2>Sending email failed! Click <a href='$profile_url'>Here</a> to go to profile page.</h2>";
                            }

                        } catch (Exception $Exception) {
                            echo "<h2>DataBase Error {$Exception->getCode()}:" . $Exception->getMessage() . "</h2>";
                        }
                    }
                } catch (Exception $Exception) {
                    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
                }
            }

            ?>
        </div>
    </div>
</section>