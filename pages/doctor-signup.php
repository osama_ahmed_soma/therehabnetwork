 <link href="http://code.jquery.com/ui/1.8.24/themes/blitzer/jquery-ui.css" rel="stylesheet"
type="text/css" />
 <script src="http://code.jquery.com/ui/1.8.24/jquery-ui.min.js" type="text/javascript"></script>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>admin/css/bootstrap-formhelpers.min.css">
  <script src="<?php echo HTTP_SERVER; ?>admin/js/bootstrap-formhelpers.min.js"></script>
  
<section class="grey clearfix" id="service"  style="background:url(img/back.png);">
    <div class="container">
		
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1 class=" Virtual-heading">Doctor Onboarding / Signup Form</h1>
            </div>
            <div class="col-md-12">
                <div class="col-md-6  col-md-offset-3 text-center">
                    <h2 class="simple pra">Please fill out the details below that are required for registration</h2>
                </div>

            </div>


        </div>
    </div>
</section>

<section class="Finding clearfix dark-grey">
       <div class="container">    
        <form method="post" action="#" id="docsignup_form">
      
        <div class="mainbox doctor-sign col-md-8 col-sm-8">                    
            <div class="panel panel-info">
                    <div class="panel-body">
                    	 <div class="text-center center-block logo-section-reg"><img src="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/img/logo.png" width="350px"></div>
						
                            <div class="col-md-12">
							<div id='loadingmessage' style='display:none'>
								<img src='img/IconLoading.gif'/>
							</div>
							<div id="reigster_form_error"></div>
                               <div class="tab-content">
                                  <div id="home" class="tab-pane fade in active">
                                  		
                                       <div class="col-md-12">   <div class="reg-heading">	General Information</div>   </div>
                                        
                                     <div class="col-md-4 col-xs-12  pull-left">
                                     <form action="" method="post" enctype="multipart/form-data" id="js-upload-form">
                                    
                                    <div class="upload-drop-zone" id="drop-zone">
                                    Browse file
                                    </div> <div class="form-inline">
									<input type="hidden" name="token" value="<?php echo getToken(); ?>">
									<input type="file" name="featured_image" class="btn btn-default btn-block btn-sm" id="js-upload-submit">
                                    <!--button type="submit" class="btn btn-default btn-user btn-block btn-sm" id="js-upload-submit"><i class="fa fa-upload"></i>  Upload Picture</button-->
                                    </div>
                                    </form>
                                     
                                     </div>
                                    
                                     
                                  
                                     <div class="col-md-8 clearfix">
                                      
                                            <div class="form-group has-feedback">
                                                <label class="control-label">First Name</label>
                                                <input type="text" name="firstname" class="form-control" tabindex="1" required="required"/>
                                            </div>
                                             <div class="form-group has-feedback">
                                               <label class="control-label">Last Name</label>
                                               <input type="text" name="lastname" class="form-control" tabindex="2" required="required"/>
                                            </div>
                                            <div class="form-group has-feedback">
                                               <label class="control-label">Email Address</label>
                                               <input type="text" name="emailAddress" class="form-control" tabindex="3" required="required"/>
                                        	</div>
                                            <div class="form-group  has-feedback">
                                                <label for="userTimeZone">Timezone</label>
                                                <?php echo getTimeZoneHtml( 'userTimeZone', 'true' ); ?>
                                            </div>
                                            <div class="phone-section">
                                                <label for="exampleInputEmail1" class=" btn-block">Phone Number</label>
                                                <div class="form-group  has-feedback">
                                                <input type="text"  name="codephone" class= "form-control col-md-6" placeholder="Country Code" id="codephone" tabindex="5">
                                                </div><div class="form-group  has-feedback">
                                                <input type="text"  name="phone" class="form-control col-md-6" placeholder="Phone number" id="phone" tabindex="6">  </div>
                                           </div>
                                           <div class="phone-section">
                                              <label for="exampleInputEmail1" class=" btn-block">Mobile Number</label>
                                              	<div class="form-group  has-feedback">
                                              <input type="text"  name="codemobile" class="form-control col-md-6" id="codemobile" placeholder="Country code" tabindex="7">  
                                               </div><div class="form-group  has-feedback">
                                                <input type="text" name="mobile" class="form-control col-md-6" placeholder="Mobile number" id="mobile" tabindex="8">   </div>    
                                           </div>
                                       </div> 
                                      
                                       
                                       <div class="col-md-6 clearfix information-reg">
                                       		<div class="form-group has-feedback">
                                                <label class="control-label btn-block">Gender</label>
                                                <span class="form-control radio-btn">
                                               	<input tabindex="9" type="radio" name="gender" value="Male" checked> Male 
            									<input tabindex="10"type="radio" name="gender" value="Female"> Female
                                            	</span>
                                            </div>
                                            <div class="form-group registration-date">
                                            	<label class="control-label" for="registration-date">Date of Birth:</label>
                                                <div class="input-group registration-date-time">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                                        <input class="form-control" name="birth_date" id="birth_date" type="text" placeholder="YYYY-MM-DD" tabindex="12">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix has-feedback">
                                                <label class="control-label">Practicing Since</label>
                                               <input type="text" name="practicesince" class="form-control" placeholder="YYYY" tabindex="14" />
                                            </div>
                                            <div class="form-group has-feedback">
                                                <label class="control-label">Department</label>
                                                <input type="text" name="department" class="form-control" tabindex="16"  />
                                            </div>
                                            <div class="form-group has-feedback">
                                                <label class="control-label">Medical Specialty </label>
                                               <input type="text" name="medicalspecialty" class="form-control" tabindex="17" />
                                            </div>
                                            <div class="form-group has-feedback">
                                                <label class="control-label">Sub Speciality </label>
                                               <input type="text" name="subspeciality" class="form-control" tabindex="18" />
                                            </div>
                                            <div class="form-group has-feedback">
                                                <label class="control-label">Medical Licensed</label>
                                                <input type="text" name="medicallicensed" class="form-control" tabindex="19" />
                                            </div>
                                            <div class="form-group has-feedback">
                                                <label class="control-label">States Licensed</label>
                                                <input type="text" name="stateslicensed" class="form-control" tabindex="20" />
                                            </div>
                                            
                                       </div>
                                       <div class="col-md-6 clearfix information-reg">
                                       		
                                            <div class="form-group has-feedback">
                                                <label class="control-label">Pre-Medical Education</label>
                                               <input type="text" name="education" class="form-control" tabindex="11"/>
                                            </div>
                                            <div class="form-group has-feedback">
                                                <label class="control-label">Internship</label>
                                                <input type="text" name="internship" class="form-control" tabindex="13" />
                                            </div>
                                            <div class="form-group has-feedback">
                                                <label class="control-label">Residency</label>
                                                <input type="text" name="residency" class="form-control" tabindex="15" />
                                            </div>
                                            <div class="form-group has-feedback addres-head-reg">
                                                <h4 class="control-label"> Address Information</h4>
                                            </div>
                                            <div class="form-group has-feedback">
                                                <label class="control-label"> Street Address</label>
                                                <input type="text" name="street" class="form-control"  tabindex="17"/>
                                            </div>
										
										<div class="form-group has-feedback ">
                                                <label class="control-label">Country</label>
                                                  <select class="form-control bfh-countries userCountry" name="country" id="userCountry" data-country="US"   tabindex="18"></select>			
                                                
                                            </div>

											<div class="form-group has-feedback userStateDiv">
                                                <label class="control-label">State</label>
                                                   <select id="userStateInDr" class="form-control bfh-states" name="state" data-state="AL"  data-country="userCountry"   tabindex="19" ></select>				
                                                
                                            </div>
											
											<div class="form-group has-feedback userStateInput" style="display:none;"> 
											 <label class="control-label">State</label>
											<input type="text" class="form-control" id="userStateIn"  name="stateBK"  placeholder="State" value="">
											</div>						
											<script>
										
										function changeStateField()
										{
										 if ($('select.userCountry').val() != 'US')
														 {console.log('else');
															$('.userStateDiv').css( "display", "none" );
															$('#userStateInDr').attr('name', 'stateBK');
															$('#userStateIn').attr('name', 'state');
															$('.userStateInput').css( "display", "block" );
														}
														else
														{	console.log('else');
															$('.userStateInput').css( "display", "none" );
															$('#userStateInDr').attr('name', 'state');
															$('#userStateIn').attr('name', 'stateBK');
															$('.userStateDiv').css( "display", "block" );
														}
										}
										$(function(){  
												
												 $('select.userCountry').change(function()
													{	
														changeStateField();
													});
												});
										</script>
                                            <div class="form-group has-feedback">
                                                <label class="control-label">City</label>
                                              <input type="text" name="city" class="form-control" tabindex="20" />
                                            </div>
                                          
                                            <div class="form-group has-feedback">
                                                <label class="control-label">Zip</label>
                                               <input type="text" name="zip" class="form-control" tabindex="24" />
                                            </div>
                                         
                                         
                                         
                                      
                                          
                                       </div>
                                     
                                     <div class="col-md-12 clearfix">   
                                     <div class="reg-heading">	Login Information</div>   
                                      <div class="form-group">
                                            <label class="control-label">Username</label>
                                           <input type="text" name="username" class="form-control" tabindex="25" />
                                       </div>
                                       <div class="form-group has-feedback">
                                            <label class="control-label">Password</label>
                                           <input type="password" name="password" class="form-control" tabindex="26" />
                                       </div>
                                       <div class="form-group has-feedback">
                                            <label class="control-label">Confirm Password</label>
                                           <input type="password" name="confirmpassword" class="form-control"  <input type="password" name="password" class="form-control" tabindex="27"/>
                                       </div></div>
                                      
                                     
                                      <div class="profile-butten col-md-12 notification">
                                       <button type="submit" class="btn btn-default dark-pink" tabindex="28"><i class="fa fa-check"></i> Save and Submit</button>
                                     </div>  
                                 </div>
                                  
                                  
                                  <!--div id="experience" class="tab-pane fade in ">
                                     <div class="profile-top  clearfix">
                                       <div class="form-group">
                                            <label class="control-label">Username</label>
                                           <input type="text" class="form-control" />
                                       </div>
                                       <div class="form-group has-feedback">
                                            <label class="control-label">Password</label>
                                           <input type="text" class="form-control" />
                                       </div>
                                       <div class="form-group has-feedback">
                                            <label class="control-label">Confirm Password</label>
                                           <input type="text" class="form-control" />
                                       </div>
                                        
                                     </div>
                                     <div class="col-md-12 profile-butten notification">
                                        <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-check"></i> Save</button>
                                        <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-close"></i> Cancel</button>
                                     </div>
                                  </div-->
                            </div>
            			</div>  
        		</div>
			</form>	
       	</div>
    
</section>

<script type="text/javascript">
    jQuery(function($){
		
		$("#birth_date").datepicker({
                    dateFormat: "yy-mm-dd",
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
					 yearRange: "-50:+0", 
                    //minDate: new Date(),
                });
				
        //doctor_signup_form
        $('form#docsignup_form').submit(function(event){
			event.preventDefault();
			var th = $(this);
            var data = $( "#docsignup_form" ).serialize();
			var formData = new FormData($(this)[0]);
			th.find('button').attr('disabled', true);
			$('#loadingmessage').show(); 
		    $.ajax({

                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=doctor_signup_form",
				type:"POST",
                data: formData,
                processData: false,
				contentType: false,
                dataType:"json"

            }).error(function () {

                th.find('button').removeAttr('disabled');
				$('#loadingmessage').hide();

            }).done(function () {

                th.find('button').removeAttr('disabled');
				$('#loadingmessage').hide();

            }).success(function (data) {
				
				
                //data = JQuery.parseJSON(data);
				
                if ( data.success ) {
					var str = "";
					
                    str += '<div class="alert alert-success alert-dismissible" role="alert">\
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                        <strong>Success!</strong> ' + data.success + '</div>';
                    $('#reigster_form_error').html(str);
					
					$('html, body').animate({scrollTop : 0},800);
					
                    setTimeout(function(){
						window.location = '<?php echo HTTP_SERVER; ?>doctors.html';
						
                    }, 3000);
                }

                if ( data.error ) {
                    var str = "";
					$.each(data.error, function(idx, error){
                        str += '<div class="alert alert-danger alert-dismissible" role="alert">\
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                        <strong>Error!<br></strong> ' + error + '</div>';
                    });
                    $('#reigster_form_error').html(str);
					
					$('html, body').animate({scrollTop : 0},800);
                }
				$('#loadingmessage').hide();
            }, 'json');

            return false;
        });
		
		$("#js-upload-submit").on("change", function()
		{
			var files = !!this.files ? this.files : [];
			var ext = $('#js-upload-submit').val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				alert('Only image file is allowed !');
				$('#js-upload-submit').val("");
				$("#drop-zone").css('background-image', 'none');
			}
			else
			{	
				if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
		 
				if (/^image/.test( files[0].type)){ // only image file
					var reader = new FileReader(); // instance of the FileReader
					reader.readAsDataURL(files[0]); // read the local file
				
					reader.onloadend = function(){ // set image data as background of div
						$("#drop-zone").css("background-image", "url("+this.result+")");
						$("#drop-zone").css("background-size", "100% auto");
						$("#drop-zone").css("background-repeat", "no-repeat");
					}
				}
			}
		});	
    });
</script>
