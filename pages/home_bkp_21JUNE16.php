<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}

//get category data
$location_data = array();

try {
	$query = "SELECT id, categoryName, categoryType, categoryFeatured FROM " . DB_PREFIX . "category WHERE `categoryType` = 'location' AND categoryStatus = 1 order by `categoryOrder` asc";
	$result = $db->prepare($query);
	$result->execute();

	if ( $result->rowCount() > 0 ) {
		$location_data = $result->fetchAll();
	}

} catch (Exception $Exception) {
	exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}
?>

<section id="banner" style="background:url(<?php echo HTTP_SERVER; ?>/img/home-banner.jpg) center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="banner-cnt">
                	<h1>Get The Best Personalized Care</h1>
					<?php /*?>We match patient needs and preferences to physician expertise and experience, helping you find and choose the best virtual doctors in America. It’s personalized online healthcare the way it should be.<?php */?>
                    Find highly educated and experienced online doctors based on your preferences
					<div class="btn btn-dark-blue">Get Started</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section  class="step-choose-section how-it-works-home">
	<div class="container">
		<div class="row">
        	<div class="col-md-12 text-center practice-heading clearfix">
            	<h1>How It Works</h1>
            </div>
        	<div class="col-md-4 col-sm-4 col-xs-12 step-main">
            	<div class="col-md-12 text-center sm-icon-sec">
                	<div class="step-icon"><i  class="fa fa-desktop"></i><span><i  class="fa fa-plus"></i></span></div>
                    <div class="step-heading">Step 1 </div>
                </div>
                <div class="col-md-12 step-detail">
                	<div class="col-lg-12 text-center">Signup instantly via email of social media account</div>
                </div>          
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 step-main">
            	<div class="col-md-12 text-center sm-icon-sec">
                	<div class="step-icon"><i  class="fa fa-user-md"></i><span><i  class="fa fa-search"></i></span></div>
                    <div class="step-heading">Step 2 </div>
                </div>
                <div class="col-md-12 step-detail">
                	<div class="col-lg-12 text-center">Find your doctor using our fast and user friendly system</div>
                </div>          
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 step-main">
            	<div class="col-md-12 text-center sm-icon-sec">
                	<div class="step-icon"><i  class="fa fa-calendar"></i><span><i  class="fa fa-youtube-play"></i></span></div>
                    <div class="step-heading">Step 3</div>
                </div>
                <div class="col-md-12 step-detail">
                	<div class="col-lg-12 text-center">Schedule a video consultation and get started</div>
                </div>          
            </div>
            
        </div>
        
    </div>    
</section>

<section  class="about-us-home" style="background:url(<?php echo HTTP_SERVER; ?>/img/about-section.jpg) center center;">
	<div class="container">
		<div class="row">
        	<div class="col-md-12 text-center practice-heading clearfix">
            	<h1>About Us</h1>
            </div>
        	<p>Our mission is to combine the efficiencies of innovative telehealth technology with the personal touch of highly trained and compassionate healthcare professionals so that every patient interaction results in superior care and satisfaction. Through our secure, user-friendly telemedicine platform, we’re making healthcare simple, affordable, and convenient while ensuring every patient has a choice.</p>
      	 </div>   
    </div>    
</section>


<section  class="step-choose-section how-it-works-home">
	<div class="container">
		<div class="row">
        	<div class="col-md-12 text-center practice-heading clearfix">
            	<h1>Common Conditions We Treat</h1>
            </div>
        	
            <div class="col-md-12 clearfix how-it-ul-sec">
            	<ul>
                	<li>Flu</li>
                	<li>Cough</li>
                	<li>Ear problems</li>
                	<li>Allergies</li>
                	<li>Nausea / Vomiting</li>
                	<li>Urinary problems / UTI</li>
                	<li>Insect Bites</li>
                	<li>Sore throats</li>
                	<li>Vomiting</li>
                </ul>
            </div>
            <div class="col-md-12 clearfix how-it-btn-sec">
            	<a href="#" class="btn btn-light">More Medical Conditions</a>
            </div>
        </div>
        
    </div>    
</section>
<section id="testimonials" class=" txt-center">
    	<div class="container">
        <div class="row">
        <h2 class="heading">We love our customers <br><span>and they love us!</span></h2>
         <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
         	  
            <ul class="carousel-inner">    
            <!-- Start -->
            <li class="item active">
                <div class="testText col-md-10 fn mauto">
                    <p>
                        I have been using iDevAffiliate for a number of years and love it. It just works and works so well with the various shopping carts I’ve used with Wordpress over the years. I am not technical and I find the support is second to none. In the past I’ve used the installation service and it’s been carried out very quickly and seamlessly. If you are looking for an affiliate product I totally recommend iDevAffiliate.
                    </p>
                </div>
                <div class="clearfix">
                    Russell Davis,<br>
                    <span class="f12">Coach, Cognitive Hypnotherapist, Trainer &amp; Facilitator</span>
                </div>
            </li>
            <!-- END -->

            <!-- Start -->
            <li class="item">
                <div class="testText col-md-10 fn mauto">
                    <p>
                        I have been using iDevAffiliate for a number of years and love it. It just works and works so well with the various shopping carts I’ve used with Wordpress over the years. I am not technical and I find the support is second to none. In the past I’ve used the installation service and it’s been carried out very quickly and seamlessly. If you are looking for an affiliate product I totally recommend iDevAffiliate.
                    </p>
                </div>
                <div class="clearfix">
                    Russell Davis,<br>
                    <span class="f12">Coach, Cognitive Hypnotherapist, Trainer &amp; Facilitator</span>
                </div>
            </li>
            <!-- END -->
        </ul>
        
        <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                </ol>
        </div>
        </div> </div>
   </section>


