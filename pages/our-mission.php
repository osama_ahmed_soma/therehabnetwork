<section class="grey clearfix" id="service"  style="background: #f8f8f8;">

    <div class="container">

        <div class="row">

            <div class="col-md-12 text-center">

                <h1 class=" Virtual-heading">Connecting Patients to Better Quality Healthcare</h1>

            </div>

            <div class="col-md-12">

                <div class="col-md-6  col-md-offset-3 text-center">

                    <h2 class="simple pra">Through Accessibility. Through Freedom of Choice. Through Empathy.  </h2>

                </div>



            </div>





        </div>

    </div>

</section>



<section class="Finding clearfix">

    <div class="container">

        <div class="row">

            <div class="col-md-12 col-sm-12">

                <div class="col-md-12 text-center Finding-pra">

               <h1> Our Mission</h1>

                <p class="hidden">Our mission is to combine the efficiencies of innovative telehealth technology with the personal touch of highly trained and compassionate healthcare professionals so that every patient interaction results in superior care and satisfaction. Through our secure, user-friendly telemedicine platform, we’re making healthcare simple, affordable, and convenient while ensuring every patient has a choice. </p>
				<p>My Virtual Doctor's purpose is to connect health care providers directly with their patients whenever and wherever care is needed.  By providing innovative telehealth communication solutions, we aim to overcome barriers to access while promoting continuity of care and successful, cost-conscious treatment online.</p>
                </div>
                
                </div>

                

            </div>

        </div>

    </div>

</section>