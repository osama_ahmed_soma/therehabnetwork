<div class="log-back">
    <div class="container">
        <div class="logfrom-main">
            <h1>
                See a Doctor Within Minutes</h1>
            <h3>Find highly educated and experienced online</br> doctors based on your preferences</h3>
            <div class="logfrom-left">
                <div class="logfrom-left-back"></div>
                <ul class="list-unstyled">
                    <li>
                        <img src="<?php echo HTTP_SERVER; ?>img/PhysiciansIcon.png"  class="logfrom-icon"/>
                        <p>
                            <span>Physicians can treat most common non-emergency medical conditions over video.</span>
                        </p>
                    </li>
                    <li>
                        <img src="<?php echo HTTP_SERVER; ?>img/PsychologistsIcon.png" class="logfrom-icon"/>
                        <p>
                            <span>Psychologists and psychiatrists can address non-emergency emotional or mental health issues.</span>
                        </p>
                    </li>
                    <li>
                        <img src="<?php echo HTTP_SERVER; ?>img/dolar.png" class="logfrom-icon"/>
                        <p>
                            <span>Risk free. If we're unable to treat you through video, we offer a money-back guarantee.</span>
                        </p>
                    </li>
                </ul>
            </div>
            <div class="signup-right bbm-modal__topbar">
                <h3 style="color: #0f56a4;;">Reset password</h3>
                <p class="text-center" style="    font-size: 16px;">Hey, we all forget things sometimes. Let’s take care of this for you.

                    Enter your email address below and we’ll send you instructions on how to change your password.</p>
                <div id="patient_reset_password_error"></div>
                <form data-role="form" name="patient_reset_password" id="patient_reset_password" class="ng-invalid ng-invalid-required ng-dirty">
                    <div>
                        <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                        <div id="username-wrapper" class="form-group has-feedback margin-bottom-sm">
                            <input class="form-control ng-isolate-scope ng-pristine ng-invalid ng-invalid-required ng-valid-email" autocomplete="off" type="text" name="emailAddress" id="emailAddress" placeholder="Email address">
                        </div>
                    </div>
                    <button type="submit" id="btn-login" class="btn btn-info sing login btn-block" style="font-size: 21px; height: 43px;">Send </button>
                </form>
                <div class="bbm-modal__bottombar" style="margin-top: 0px;padding: 5px; text-align: center;">
                    Already have an account? <a href="<?php echo HTTP_SERVER . 'login.html' ?>" class="login">Log In Here</a>
                </div>
                <div class="bbm-modal__bottombar" style="margin-top: 0px;padding: 5px; text-align: center;">
                    New to My Virtual Doctor?   <a class="signup" href="<?php echo HTTP_SERVER . 'signup.html' ?>">Create a free account now</a>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(function($){
        //reset password form
        $('#patient_reset_password').submit(function(){
            var th = $(this);
            var data = $( "#patient_reset_password" ).serializeArray();

            th.find('button').attr('disabled', true);

            $.ajax({

                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=patient_forgot_password",
                data:data,
                method:"POST",
                dataType:"json"

            }).error(function () {

                th.find('button').removeAttr('disabled');

            }).done(function () {

                th.find('button').removeAttr('disabled');

            }).success(function (data) {
                //data = JQuery.parseJSON(data);
                if ( data.success ) {
                    var str = "";
                    str += '<div class="alert alert-success alert-dismissible" role="alert">\<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\<strong>Success!</strong> ' + data.success + '</div>';
                    $('#patient_reset_password_error').html(str);
                }

                if ( data.error ) {
                    var str = "";
                    $.each(data.error, function(idx, error){
                        str += '<div class="alert alert-danger alert-dismissible" role="alert">\<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\<strong>Error!</strong> ' + error + '</div>';
                    });
                    $('#patient_reset_password_error').html(str);
                }

            }, 'json');

            return false;
        });
    });
</script>