<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}?>
<section id="banner">
	<div class="container">
		<div class="row">
			<div class="col-md-10 text-center col-md-offset-1">
				<h1><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h1>
				<hr />
				<div class="paragrph f20 regular">
					We could not find the page you were looking for.
					Meanwhile, you may <a href="<?php echo HTTP_SERVER; ?>">return to Home</a> or try using the search form.
				</div>
			</div>
		</div>
	</div>
</section>