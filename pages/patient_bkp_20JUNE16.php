<?php

//deny direct access

if ( !defined('MVD_SITE') ) {

    die('You are not authorized to view this page');

}

?>

<section class="dashboard-new-section patient-new-section clearfix">

    <div class="container">

        <div class="row">

            <div class="nav-pills">

                <ul>

                    <li class="col-md-2 col-sm-2 col-xs-4 dns-icon <?php echo $sub_page == '' || $sub_page == 'dashboard' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-calendar"></i> Dashboard</a></li>

                    <li class="col-md-2 col-sm-2 col-xs-4 dns-icon <?php echo $sub_page == 'health' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/health.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-heart"></i> My Health</a></li>

                    <li class="col-md-2 col-sm-2 col-xs-4 dns-icon <?php echo $sub_page == 'providers' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/providers.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-star"></i>  My Providers</a>



                    </li>

                    <li class="col-md-2 col-sm-2 col-xs-4 dns-icon <?php echo $sub_page == 'pinned-articles' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/pinned-articles.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-paperclip"></i>  Pinned Blogs</a></li>

                    <li class="col-md-2 col-sm-2 col-xs-4 dns-icon <?php echo $sub_page == 'profile-settings' || $sub_page == 'change-password' || $sub_page == 'notification-settings' ? 'active' : ''; ?>"><a href="#vtab1" data-toggle="tab"> <i class="fa fa-gears"></i> Account Settings</a>

                        <ul>

                            <li class="<?php echo $sub_page == 'profile-settings' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/profile-settings.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-gears"></i> Profile</a>
                            	<ul>
                                	<li><a href="#">Dummy</a></li>
                                	<li><a href="#">Dummy</a></li>
                                	<li><a href="#">Dummy</a></li>
                                	<li><a href="#">Dummy</a></li>
                                </ul>
                            </li>

                            <li class="<?php echo $sub_page == 'change-password' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/change-password.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-gears"></i>  Change Password</a>
                            
                                    	<ul>
                                            <li><a href="#">Dummy 1</a></li>
                                            <li><a href="#">Dummy 1</a></li>
                                            <li><a href="#">Dummy 1</a></li>
                                            <li><a href="#">Dummy 1</a></li>
                                        </ul>
                                </li>

                            <li class="<?php echo $sub_page == 'notification-settings' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/notification-settings.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-gears"></i>  Notifications</a></li>

                        </ul>



                    </li>

                    <li class="col-md-2 col-sm-2 col-xs-4 dns-icon <?php echo $sub_page == 'support' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/support.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-users"></i> Help and Support</a></li>

                </ul>

            </div>

        </div>

    </div>

</section>

<div class="tab-content">

    <?php

    if ( $sub_page != '' ) {

        require_once SITE_ROOT . 'pages/modules/' . $do .'/'. $sub_page .'.php';

    } else {

        require_once SITE_ROOT . 'pages/modules/' . $do .'/dashboard.php';

    }

    ?>

</div>

<script type="text/javascript">



    jQuery(function ($){

        $('a.main_tab').on('click', function (e) {

            window.location = $(this).attr('href'); // newly activated tab

            //e.relatedTarget // previous active tab

            return false;

        })





    });



</script>

