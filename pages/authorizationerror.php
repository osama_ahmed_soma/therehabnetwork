<section class="grey clearfix" id="service"  style="background:url(img/back.png);">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class=" Virtual-heading">Your Telehealth Hub</h1>
            </div>
            <div class="col-md-12">
                <div class="col-md-6  col-md-offset-3 text-center">
                    <h2 class="simple pra">Making online healthcare delivery simple and profitable.  </h2>
                </div>

            </div>


        </div>
    </div>
</section>

<section class="Finding clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="col-md-12 text-center Finding-pra">
               <h2>Authorization Error</h2>
			  <div class="alert alert-danger">
  <h4>
  <strong>Error!</strong> You are not authorized to view this page.
  </h4>
</div>
                </div>
                </div>
                
            </div>
        </div>
    </div>
</section>