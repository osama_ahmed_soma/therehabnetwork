<section id="banner" class="resource-banner-setting" style="background:url(http://evisit.com/wp-content/uploads/2016/03/shutterstock_317573531.jpg) center bottom;">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="paragrph f20 regular">
                	<h1>How to Increase Patient Engagement</h1>
					 Get top tips for getting your patients more engaged in their care. <br>
					<a href="" class="btn btn-info">Download </a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="Finding resource-page-tabs clearfix">
	<div class="container">
        <div class="row">
        	<div class="col-md-12">
            
               <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <li><a href="#blog" aria-controls="blog" role="tab" data-toggle="tab"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Blog </a></li>
                    <li class="active"><a href="#eBook" aria-controls="blog" role="tab" data-toggle="tab"> <i class="fa fa-book" aria-hidden="true"></i> eBooks </a></li>
                    <li><a href="#Infographic" aria-controls="blog" role="tab" data-toggle="tab"> <i class="fa fa-picture-o" aria-hidden="true"></i> Infographic</a></li>
                </ul>
            </div>
       </div>
   </div>             
</section>
<div class="tab-content">
<section class="Finding resource-page clearfix tab-pane fade" id="blog">
    <div class="container">
        <div class="row">
        	<div class="col-md-12">
                <h1 class="text-center">Blog</h1>
                <br />
                <div class="col-md-3">
                	<div class="eb-ser">
                        <a href="#"><img src="http://evisit.com/wp-content/uploads/2016/02/How-to-Grow-Your-Practice-Webinar.png" /></a>
                        <h3>3 Simple Ways to Grow Your Practice</h3>
                        <p>Watch now to learn three basic strategies to expand your practice and boost revenue.</p>
                        <a href="#" class="btn btn-info"><i class="fa fa-book"></i> Read More</a>
                    </div>    
                </div>
                <div class="col-md-3">
                	<div class="eb-ser">
                        <a href="#"><img src="http://evisit.com/wp-content/uploads/2016/02/How-to-Choose-a-Telemed-Software.png" /></a>
                        <h3>How to Choose the Right Telehealth Software</h3>
                        <p>How do you ensure you’re picking the best telehealth software for your practice?</p>
                        <a href="#" class="btn btn-info"><i class="fa fa-book"></i> Read More</a>
                    </div>    
                </div>
                <div class="col-md-3">
                	<div class="eb-ser">
                        <a href="#"><img src="http://evisit.com/wp-content/uploads/2016/02/How-to-Build-a-Telemed-Program.png" /></a>
                        <h3>How to Build a Successful Telehealth Program</h3>
                        <p>How do you implement your telehealth solution the right way and get your patients onboard?</p>
                        <a href="#" class="btn btn-info"><i class="fa fa-book"></i> Read More</a>
                    </div>    
                </div>
                <div class="col-md-3">
                	<div class="eb-ser">
                        <a href="#"><img src="http://evisit.com/wp-content/uploads/2016/02/Optimize-Mobile.png" /></a>
                        <h3>How to Use Mobile Devices in Your Practice</h3>
                        <p>Dr. Tom Giannulli of Kareo explains how to make your medical practice mobile-friendly.</p>
                        <a href="#" class="btn btn-info"><i class="fa fa-book"></i> Read More</a>
                    </div>    
                </div>
                
                <div class="col-md-12">
                	<div class="eb-ser">
                        <a href="#" class="btn btn-default"><i class="fa fa-refresh"></i> Load More</a>
                    </div>    
                </div>
			</div>
         
        </div>  
    </div>
</section>

<section class="Finding resource-page  clearfix tab-pane fade in active"  id="eBook">
    <div class="container">
        <div class="row">
        	<div class="col-md-12">
                <h1 class="text-center" >eBooks</h1>
                <br />
                <div class="col-md-3">
                	<div class="eb-ser">
                        <a href="#"><img src="http://evisit.com/wp-content/uploads/2016/02/Is-Telehealth-Right-for-you-free-quiz.png" /></a>
                        <h3>9 Tips to Increase Practice Revenue</h3>
                        <p>Want to get your practice to a healthier bottom line? Learn key strategies to boost revenue fast.</p>
                        <a href="#" class="btn btn-info"><i class="fa fa-download"></i> Download</a>
                    </div>    
                </div>
                <div class="col-md-3">
                	<div class="eb-ser">
                        <a href="#"><img src="http://evisit.com/wp-content/uploads/2016/02/Copy-of-Telehealth-Quiz-for-Doctors-1.png" /></a>
                        <h3>Is Telehealth Right for Your Practice?</h3>
                        <p>Take our quiz and find out if telemedicine is a good fit for your medical practice.</p>
                        <a href="#" class="btn btn-info"><i class="fa fa-download"></i> Download</a>
                    </div>    
                </div>
                <div class="col-md-3">
                	<div class="eb-ser">
                        <a href="#"><img src="http://evisit.com/wp-content/uploads/2016/02/Top-Telemedicine-Myths.png" /></a>
                        <h3>Trends in Telemedicine Whitepaper</h3>
                        <p>Are patients interested in telemedicine? How many physicians think telehealth is a priority? Does telehealth really save on healthcare costs?</p>
                        <a href="#" class="btn btn-info"><i class="fa fa-download"></i> Download</a>
                    </div>    
                </div>
                <div class="col-md-3">
                	<div class="eb-ser">
                        <a href="#"><img src="http://evisit.com/wp-content/uploads/2016/02/Technical-Tips-for-Glitch-Free-Telemedicine-Visit.png" /></a>
                        <h3>How to Have a Glitch-Free Virtual Visit</h3>
                        <p>Get this how-to checklist for physicians and patients with common technical tips, and steps to prepare for a telemedicine visit.</p>
                        <a href="#" class="btn btn-info"><i class="fa fa-download"></i> Download</a>
                    </div>    
                </div>
                <div class="col-md-12">
                	<div class="eb-ser">
                        <a href="#" class="btn btn-default"><i class="fa fa-refresh"></i> Load More</a>
                    </div>    
                </div>
			</div>
         
        </div>  
    </div>
</section>

<section class="Finding resource-page clearfix tab-pane fade "  id="Infographic">
    <div class="container">
        <div class="row">
        	<div class="col-md-12">
                <h1 class="text-center">Infographics</h1>
                <br />
                <div class="col-md-3">
                	<div class="eb-ser">
                        <a href="#"><img src="http://evisit.com/wp-content/uploads/2016/02/healthcare-trends-2015-infographic.jpg" /></a>
                        <h3>The History of Telemedicine</h3>
                        <p>A brief history of telemedicine over the century.</p>
                        <a href="#" class="btn btn-info">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div>    
                </div>
                <div class="col-md-3">
                	<div class="eb-ser">
                        <a href="#"><img src="http://evisit.com/wp-content/uploads/2016/02/Portrait-of-the-Overburdened-Doctor-Infographic.jpg" /></a>
                        <h3>Portrait of the Overburdened Doctor</h3>
                        <p>What does the average doctor’s day look like?</p>
                        <a href="#" class="btn btn-info">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div>    
                </div>
                <div class="col-md-3">
                	<div class="eb-ser">
                        <a href="#"><img src="http://evisit.com/wp-content/uploads/2016/02/How-to-Boost-Patient-Engagement-Infographic.jpg" /></a>
                        <h3>How to Boost Patient Engagement</h3>
                        <p>Find out tactics to increase your patient engagement.</p>
                        <a href="#" class="btn btn-info">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div>    
                </div>
                <div class="col-md-3">
                	<div class="eb-ser">
                        <a href="#"><img src="http://evisit.com/wp-content/uploads/2016/02/Chronic-Disease-Infographic-Telehealth.jpg" /></a>
                        <h3>How Do We Fight Chronic Disease?</h3>
                        <p>The U.S. spends millions of dollars a year on chronic care. How can providers help patients prevent and manage chronic disease?</p>
                        <a href="#" class="btn btn-info">Learn Morew <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div>    
                </div>
                <div class="col-md-12">
                	<div class="eb-ser">
                        <a href="#" class="btn btn-default"><i class="fa fa-refresh"></i> Load More</a>
                    </div>    
                </div>
			</div>
         
        </div>  
    </div>
</section>
</div>
<section class="grey clearfix" id="footer" style="background:url(http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/img/footer.png);">
    <div class="container">
		<div class="col-md-12">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h1 class=" Virtual-heading">Are you ready to market your practice more effectively?</h1>
                </div>
                <div class="col-md-12">
                   <div class="col-md-8  col-md-offset-2 text-center request-form">
                   		<h2 class="simple pra">Sign Up For  A Free Demo Today </h2> <br />
                    	<form role="form" id="demo-form" action="#" method="post">
                         <div id="form_message"></div>
                            <input type="hidden" name="token" value="373353344136304f3341345049334249304458324e50514a574443554f4e4b">
                            <input type="text" placeholder="First Name" id="InputNameFirst" name="InputNameFirst" value="" class="form-control" required="">
                            <input type="text" placeholder="Last Name" id="InputNameLast" name="InputNameLast" value="" class="form-control" required="">
                            <input type="text" placeholder="Email" id="InputEmail" name="InputEmail" value="" class="form-control" required="">
                            <input type="text" placeholder="Phone" id="InputPhone" name="InputPhone" value="" class="form-control" required="">
                             <!--<input type="text" placeholder="Clinic" id="InputClinic" name="InputClinic" value="" class="form-control" required=""> -->
                           <!-- <input type="submit" value="REQUEST DEMO"  name="submit" id="submit" class="btn btn-info request-btn" /> -->
                             <button type="submit" name="submit" id="submit" value="REQUEST DEMO" class="btn btn-info pull-right request-btn">REQUEST DEMO</button>
                            
                        </form>
                    </div>
                
                    
    
                </div>
            </div>
		</div>
	</div>
</section>	

<script>
/*Scroll transition to anchor*/
$("a.toscroll").on('click',function(e) {
    var url = e.target.href;
    var hash = url.substring(url.indexOf("#")+1);
    $('html, body').animate({
        scrollTop: $('#'+hash).offset().top
    }, 500);
    return false;
});
</script>