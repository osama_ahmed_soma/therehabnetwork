<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

$page = "";
if ($_SESSION["mvdoctorVisitorUserLevel"]==4)
{
	$page = "<script>window.location='". HTTP_SERVER ."';</script>";
}
if ($_SESSION["mvdoctorVisitorUserLevel"]==5)
{
	$page = "<script>window.location='". HTTP_SERVER ."doctor-login.html?msg=1';</script>";
}	
session_destroy();

echo $page;
?>