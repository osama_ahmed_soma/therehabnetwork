<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}

?>
<section id="banner" style="background:linear-gradient(rgba(0, 0, 0, 0.32), rgba(0, 0, 0, 0.32)),url(<?php echo HTTP_SERVER; ?>/img/slider-back.jpg) center center;height: 645px;margin-top: -49px;">
   <div class="container">
      <div class="row">
         <div class="col-md-9">
            <div class="banner-cnt new">
               <h1>Feeling Better is Just One Click Away</h1>
               <?php /*?>We match patient needs and preferences to physician expertise and experience, helping you find and choose the best virtual doctors in America. It’s personalized online healthcare the way it should be.<?php */?>
               <p>Now you can get personalized care from your trusted physician anywhere you are. Save time and money by scheduling your next doctor’s appointment online.</p>
               <div><a href="<?php echo HTTP_SERVER ?>app/register/patient" class="btn btn-dark-blue new">Get Started</a></div>
            </div>
         </div>
      </div>
   </div>
</section>

<section  class="step-choose-section how-it-works-home new">
	<div class="container">
		<div class="row">
        	<div class="col-md-12 text-center practice-heading clearfix new">
            	<h1>How It Works</h1>				<p>With My Virtual Doctor, quality care is just a few clicks away!</p>
            </div>
        	<div class="col-md-3  col-sm-6 col-xs-12 step-main">                 <h4>Sign up </h4>
            	<div class="col-md-12 text-center sm-icon-sec new">
                	<div class="step-icon"><i  class="fa fa-check new"></i><span></span></div>
                   
                </div>
                <div class="col-md-12 step-detail new">
                	<div class="col-lg-12 text-center ">Fast, easy and secure</div>
                </div>          
            </div>						<div class="col-md-3  col-sm-6 col-xs-12 step-main">                <h4>Schedule Appointment </h4>            	<div class="col-md-12 text-center sm-icon-sec new">                	<div class="step-icon"><i  class="fa fa-calendar new"></i><span></span></div>                                  </div>                <div class="col-md-12 step-detail new">                	<div class="col-lg-12 text-center">User friendly reminder system</div>                </div>                      </div>
            <div class="col-md-3  col-sm-6 col-xs-12 step-main">                 <h4>See Your Doctor </h4>
            	<div class="col-md-12 text-center sm-icon-sec new">
                	<div class="step-icon"><i  class="fa fa-user-md new"></i><span></span></div>
                    
                </div>
                <div class="col-md-12 step-detail new">
                	<div class="col-lg-12 text-center">Face to face video conference</div>
                </div>          
            </div>
            <div class="col-md-3  col-sm-6 col-xs-12 step-main">               <h4>Feel Better </h4>
            	<div class="col-md-12 text-center sm-icon-sec new">
                	<div class="step-icon"><i  class="fa fa-heart new"></i><span></span></div>
                    
                </div>
                <div class="col-md-12 step-detail new">
                	<div class="col-lg-12 text-center">Live a healthy life</div>
                </div>          
            </div>
            
        </div>
        
    </div>    
</section>

<section  class="about-us-home what-we" style="background: #e9f3fd;    padding: 50px 0px 80px;">
	<div class="container">
		<div class="row">                       <h1>What We Do</h1>
        	 <div class="what-we-left col-md-7">               <p><b>My Virtual Doctor</b> was designed to take the stress out of doctor’s appointments by merging telehealth technology and personalized care from certified physicians. Our simple to use, HIPAA compliant platform securely allows patients and physicians to communicate through live video to effectively diagnose and treat common ailments. We provide high quality medical care on the go to get you feeling better in no time.</p>             </div>			 			 <div class="what-we-right col-md-5">               <img src="img/what-we-img.png"/>             </div>

      	 </div>   
    </div>    
</section>



<section id="testimonials" class=" txt-center new">
    	<div class="container">
        <div class="row">
        <h2 class="heading">We love our Patients <br><span>and they love us!</span></h2>
         <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
         	  
            <ul class="carousel-inner">    
            <!-- Start -->
            <li class="item active">
                <div class="testText col-md-10 fn mauto">
                    <p>
                        As a single mother who has a full-time job, getting to the doctor when one of my children is sick is never easy. That was until our family doctor offered telehealth through My Virtual Doctor. Now getting to see the doctor is easy and stress free.
                    </p>
                </div>
                <div class="clearfix">
                    Nina Smith<br>
                    <span class="f12">Sales Representative, Micola Motors</span>
                </div>
            </li>
            <!-- END -->

            <!-- Start -->
            <li class="item">
                <div class="testText col-md-10 fn mauto">
                    <p>
                       When I am not feeling well my biggest fear is going to the doctors office and being surrounded by other patients in the waiting room with other germs that can possibly make me even worse.  Since my physician made the transition to online appointments through My Virtual Doctor's platform I will never be put in that position again.
                    </p>
                </div>
                <div class="clearfix">
                   Jimmy Mathews<br>
                    <span class="f12">Software Engineer, AND Microtech</span>
                </div>
            </li>
            <!-- END -->
            <!-- Start -->
            <li class="item">
                <div class="testText col-md-10 fn mauto">
                    <p>
                       Having a chronic condition makes my life difficult and unfortunately causes me to be at the medical center more than I could have ever possibly imagined. i felt like I lost a large part of my life and precious time to be with the ones I love. However, today is a different story now that my doctors ACO made telemedicine an option for me to take advantage of. Thanks to My Virtual Doctor i have taken back my life.
                    </p>
                </div>
                <div class="clearfix">
                   Andy Golsalves<br>
                    <span class="f12">Accountant, Satnet Communications</span>
                </div>
            </li>
            <!-- END -->
        </ul>
        
        <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>
        </div>
        </div> </div>
   </section>
		