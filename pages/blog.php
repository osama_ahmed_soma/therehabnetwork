<?php

if ( !defined('MVD_SITE') ) {

    die('You are not authorized to view this page');

}

?>

<section class="grey clearfix" id="service"  style="background: #f8f8f8;">

    <div class="container">

        <div class="row">

            <div class="col-md-8 col-md-offset-2 text-center">

                <h1 class=" Virtual-heading">Telehealth Resources</h1>

            </div>

            <div class="col-md-12">

                <div class="col-md-6  col-md-offset-3 text-center">

                    <h2 class="simple pra">Here is a collection of the most advanced health tips and resources from our experts

                </div>



            </div>





        </div>

    </div>

    </div>

</section>

<section class="body clearfix" style="margin-top: 20px; margin-bottom: 20px;">

    <div class="container">

        <div class="row">

            <?php

			 $siteRootHome_new = SITE_ROOT;

                if ( isset($_GET['id']) && file_exists( $siteRootHome_new . 'pages/modules/blog/single.php' ) ) {

                    require_once $siteRootHome_new . 'pages/modules/blog/single.php';

                } elseif( file_exists( $siteRootHome_new . 'pages/modules/blog/all.php') ) {

                    require_once $siteRootHome_new . 'pages/modules/blog/all.php';

                }

            ?>

        </div>

    </div>

</section>



<!-- Modal 2-->

<div class="modal fade" id="myModalFav" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-body" style="text-align: center;" id="favoriteModal"></div>

        </div>

    </div>

</div>



<script type="text/javascript">

    jQuery(function($){



        $('body').on('click', '.addToFavorite', function(){

            var th = $(this);

            var login = $(this).data('login');

            var post_id = $(this).data('id');



            if ( login == 'yes' && post_id > 0 ) {

                //call ajax

                th.attr('readonly', 'readonly');

                $.ajax({

                    url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=doctor_favorite",

                    data: {post_id: post_id, token: '<?php echo getToken(); ?>', type:'post'},

                    method: "POST",

                    dataType: "json"

                }).error(function (err) {

                    th.removeAttr('readonly');

                    console.log(err);



                }).done(function () {

                    //



                }).success(function (data) {



                    if (data.success) {

                        th.removeClass('addToFavorite').addClass('removeFavorite');

                        th.html('<i class="fa fa-thumb-tack"></i> Unpin Article');

                        var fav_link = '<?php echo HTTP_SERVER ?>patient/pinned-articles.html';

                        var str = '<div class="alert alert-success" role="alert">This article has now been added to Pinned Articles!<br>\

                            You can view all your pinned articles <a href="'+fav_link+'" title="Pinned Articles">here</div>';

                        $('#favoriteModal').html(str);

                        $('#myModalFav').modal();

                    }

                    if ( data.error ) {

                        $('#favoriteModal').html('<div class="alert alert-danger" role="alert">'+data.error+'</div>');

                        th.removeAttr('readonly');

                        $('#myModalFav').modal();

                    }



                }, 'json');

            } else {

                $('#favoriteModal').html('<div class="alert alert-danger" role="alert">Please login to add to pinned article list.</div>');

                $('#myModalFav').modal();

            }

        });



        $('body').on('click', '.removeFavorite', function(){

            var th = $(this);

            var login = $(this).data('login');

            var post_id = $(this).data('id');



            if ( login == 'yes' && post_id > 0 ) {

                //call ajax

                th.attr('readonly', 'readonly');

                $.ajax({

                    url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=remove_doctor_favorite",

                    data: {post_id: post_id, token: '<?php echo getToken(); ?>', type:'post'},

                    method: "POST",

                    dataType: "json"

                }).error(function (err) {

                    th.removeAttr('readonly');

                    console.log(err);



                }).done(function () {

                    //



                }).success(function (data) {



                    if (data.success) {

                        th.removeClass('removeFavorite').addClass('addToFavorite');

                        th.html('<i class="fa fa-thumb-tack"></i> Pin Article');

                        $('#favoriteModal').html('<div class="alert alert-success" role="alert">Article removed.</div>');

                        $('#myModalFav').modal();

                    }

                    if ( data.error ) {

                        $('#favoriteModal').html('<div class="alert alert-danger" role="alert">'+data.error+'</div>');

                        th.removeAttr('readonly');

                        $('#myModalFav').modal();

                    }



                }, 'json');

            } else {

                $('#favoriteModal').html('<div class="alert alert-danger" role="alert">Please login to add to pinned article list.</div>');

                $('#myModalFav').modal();

            }

        });

    });

</script>



<script type="text/javascript">var switchTo5x=true;</script>

<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>

<script type="text/javascript">stLight.options({publisher: "9cd34fbe-305a-497a-ba35-1729f86a19fa", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>



