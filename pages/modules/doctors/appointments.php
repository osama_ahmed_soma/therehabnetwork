<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

checkUserSessionType(5);

$today = strtotime("Today");

try {
    $query = "SELECT sc.*, us.userFname, us.userId, us.userLname, us.userPicture FROM `" . DB_PREFIX . "schedule` as sc, `" . DB_PREFIX . "users` as us WHERE sc.doctorId = ? AND sc.startTime > ? AND sc.patientId = us.userId ORDER BY sc.startTime asc";
    $st = $db->prepare($query);
    $st->execute( array($_SESSION["mvdoctorID"], $today) );

    if ( $st->rowCount() )  {
        $data = $st->fetchAll();
    } else {
        $data = array();
    }
} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}
?>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><div class="dnsm-highlights">Upcoming  <strong>Appointments</strong></div></div>
            <?php if ( empty($data)):  ?>
                <div class="empty-appointments ">No current appointments.</div>

            <?php else:
            echo '<div class="dnsm-list">';

            foreach ( $data as $appointment ) {
                //calculating time difference
                $difference = check_date_relavence(date('Y-m-d', $appointment['startTime']));
                $date_string = '&nbsp;';

                //calculate hour difference
                $date1 = date('Y-m-d h:i A', $appointment['startTime']);
                $date2 = date('Y-m-d h:i A', time());

                $hourdiff = round((strtotime($date1) - strtotime($date2)) / 3600, 1);

                if ($difference == 0 && $hourdiff >= 0) {
                    //echo 'today';
                    $minutes = explode('.', $hourdiff);

                    if (intval($minutes[0]) == 0) {
                        $date_string = 'In about ' . $minutes[1] . ' Minutes';
                    } elseif (intval($minutes[0]) <= 9) {
                        $date_string = 'In about ' . $minutes[0] . ' Hours';
                    } else {
                        $date_string = 'Today';
                    }
                } else if ($difference == 0 && $hourdiff < 0) {

                    $minutes = explode('.', abs($hourdiff));

                    if (intval($minutes[0]) == 0) {
                        $date_string = $minutes[1] . ' Minutes Ago';
                    } elseif (intval($minutes[0]) <= 9) {
                        $date_string = $minutes[0] . ' Hours Ago';
                    }
                } else if ($difference > 1) {
                    //echo 'Future Date';
                } else if ($difference > 0 && $hourdiff >= 0 && $hourdiff <= 9) {
                    //echo 'tomorrow';
                    $minutes = explode('.', $hourdiff);
                    if (intval($minutes[0]) == 0) {
                        $date_string = 'In about ' . $minutes[1] . ' Minutes';
                    } elseif (intval($minutes[0]) <= 9) {
                        $date_string = 'In about ' . $minutes[0] . ' Hours';
                    }

                } else if ($difference > 0) {
                    $date_string = 'Tomorrow';
                } else if ($difference < -1) {
                    //echo 'Long Back';
                } else {
                    //echo 'yesterday';
                    $date_string = 'Yesterday';
                }

                //patient name
                $patient_name = $appointment['userFname'] . ' ' . $appointment['userLname'];
                $date_time = date('Y-m-d', $appointment['startTime']) . 'T' . date('H:i', $appointment['startTime']);
                $userPicture = unserialize(base64_decode($appointment['userPicture']));
                $userPicture = HTTP_SERVER . 'images/user/thumb/' . $userPicture['img2'];
                ?>

                <div class="dnsm-list-col">
                    <div class="dnsm-lc-image"
                         style="background:url('<?php echo $userPicture;?>');">
                        <span><i class="fa fa-user"></i> Online</span>
                    </div>
                    <div class="dnsm-lc-txt-area">
                        <ul>
                            <li class="name"><i class="fa fa-user"></i> <?php echo $patient_name; ?></li>
                            <li class="date"><i class="fa fa-calendar"></i> <?php echo date('jS M, Y', $appointment['startTime']); ?> <i
                                    class="fa fa-clock-o"></i> <?php echo date('h:i A', $appointment['startTime']); ?>
                            </li>
                            <li class="prsm"><i class="fa fa-clipboard"></i> Headache</li>
                        </ul>
                        <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read
                                More</a></p>
                    </div>
                    <div class="dnsm-lc-btn"><a href="#" class="loginUrl">Start Appointment</a></div>
                </div>

            <?php
            }
            echo '</div>';
            endif; ?>

        </div>
    </div>
</section>

<script type="text/javascript">
    jQuery(function($){
        $('.loginUrl').click(function() {
            //ajax call
            var th = $(this);
            th.html('Generating Login URL. Please wait...');

            $.ajax({
                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=get_login_url",
                data: {token: '<?php echo getToken(); ?>'},
                method: "GET",
                dataType: "json"
            }).error(function (err) {
                console.log(err);

            }).done(function () {

            }).success(function (data) {

                if (data.success && data.url) {
                    th.html('Redirecting...');
                    window.location = data.url;
                }
                else {
                    th.html('Video Consultancy Dashboard');
                    alert('Unknown Error!');
                }

            }, 'json');
            return false;
        });
    });
</script>