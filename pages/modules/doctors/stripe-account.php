<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
checkUserSessionType(5);

$error_msg = "";
$success_msg = "";

//save stripe api keys
if ( isset($_POST['doctor_stripe']) ) {
    $data = array_map_deep($_POST, 'sanitize');
    $data = array_map_deep($data, 'trim');


    //error handling
    if ( $data['stripeSecretKey'] == '' ) {
        $error_msg .= "<p>Please enter Stripe API Secret Key.</p>";
    }

    if ( $data['stripePubKey'] == '' ) {
        $error_msg .= "<p>Please enter Stripe API Publishable Key.</p>";
    }


    if ( $error_msg == "" ) {
        try {

            $query = "UPDATE `" . DB_PREFIX . "doctors` SET stripeSecretKey = ?, stripePubKey = ? WHERE id = ?";
            $st = $db->prepare($query);
            $st->execute( array($data['stripeSecretKey'], $data['stripePubKey'], $_SESSION["mvdoctorID"]) );

        } catch (Exception $Exception) {
            die("DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
        }
    }
}

//get stripe api keys
try {
    $query = "SELECT stripeSecretKey, stripePubKey FROM `" . DB_PREFIX . "doctors` WHERE id = ?";
    $st = $db->prepare($query);
    $st->execute(array($_SESSION["mvdoctorID"]));

    if ($st->rowCount()) {
        $row = $st->fetch();

        if ($row['stripeSecretKey'] != '' && $row['stripePubKey'] != '') {
            //check api working or not
            require_once SITE_ROOT . 'external/stripe-php/init.php';

            //below is just to check stripe api is working or not
            try {

                \Stripe\Stripe::setApiKey($row['stripeSecretKey']);

                $ret = \Stripe\Balance::retrieve();

                $success_msg .= "<p>Payment Gateway setup is successful.</p>";

            } catch (Exception $e) {

                $fail_message = $e->getMessage();
                $error_msg .= '<p>' . $fail_message . '</p>';
            }
        }

    } else {
        $row = array();
    }
} catch (Exception $Exception) {
    die("DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}
?>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="reg-heading">
                    Add Payment Method (Stripe) <br/>
                    <span>In order to be able to receive payments from patients. Please enter your Stripe information below. </span>

                </div>

                <div class="alert alert-danger">
                    To find your keys, log into your Stripe.com account and look in the "Account Settings" menu.
                </div>

                <?php if($error_msg != ""): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                        <?php echo $error_msg; ?>
                    </div>
                <?php endif; ?>

                <?php if($success_msg != ""): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4><i class="icon fa fa-check"></i> Alert!</h4>
                        <?php echo $success_msg; ?>
                    </div>
                <?php endif; ?>

                <form method="post" action="">
                    <input type="hidden" name="doctor_stripe" value="1" />
                    <div class="form-group has-feedback">
                        <label class="control-label">Secret Key</label>
                        <input type="text" name="stripeSecretKey" value="<?php echo isset($row['stripeSecretKey']) ? $row['stripeSecretKey'] : ''; ?>" class="form-control"/>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label">Publishable Key</label>
                        <input type="text" name="stripePubKey" value="<?php echo isset($row['stripePubKey']) ? $row['stripePubKey'] : ''; ?>" class="form-control"/>
                    </div>

                    <div class="profile-butten col-md-12 notification">
                        <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-check"></i> Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
