<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

checkUserSessionType(5);

?>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">

                <?php if($_SESSION['success_result']['error_msg'] != "" && $_SESSION['success_result']['tab'] == "settings"): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                        <?php echo $_SESSION['success_result']['error_msg']; ?>
                    </div>
                <?php endif; ?>

                <?php if($_SESSION['success_result']['success_msg'] != "" && $_SESSION['success_result']['tab'] == "settings"): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4><i class="icon fa fa-check"></i> Success!</h4>
                        <?php echo $_SESSION['success_result']['success_msg']; ?>
                    </div>
                <?php endif; ?>
                <form method="post" enctype="multipart/form-data" action="<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=settings_post" id="js-upload-form" name="gi_form" onsubmit="return phonenumber()"  >
                    <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                    <div class="col-md-12 row"> <div class="reg-heading" >General Information</div> </div>
                    <div class="col-md-8 row">

                        <div class="alert alert-danger errorDiv" class="" style="display:none;">
                            <strong>Error! </strong><span class="errorSpan"> </span>
                        </div>
                        <input type="hidden" name="user_profile" value="1">
                        <input type="hidden" name="secureVideoId" value="<?php echo $row['secureVideoId']; ?>" />
                        <div class="form-group has-feedback">
                            <label for="userFname">First Name</label>
                            <input type="text" class="form-control" id="userFname" name="userFname" value="<?php echo $row['userFname'] ?>" placeholder="First Name" required  >
                        </div>
                        <div class="form-group has-feedback">
                            <label for="userLname">Last Name</label>
                            <input type="text" class="form-control" id="userLname" name="userLname" value="<?php echo $row['userLname'] ?>" placeholder="Last Name" required >
                        </div>
                        <div class="form-group has-feedback">
                            <label for="userTitle">Title</label>
                            <input type="text" class="form-control" id="userTitle" name="userTitle" value="<?php echo $row['userTitle'] ?>" placeholder="Title" required >
                        </div>
                        <div class="form-group has-feedback">
                            <label for="userEmail">Email</label>
                            <input type="email" class="form-control" name="userEmail" id="userEmail" value="<?php echo $row['userEmail'] ?>" placeholder="Email" readonly>
                        </div>
                        <div class="form-group">
                            <label for="userAddress">Address</label>
                            <textarea class="form-control input-block" id="userAddress" name="userAddress"  placeholder="Address" required ><?php echo $row['userAddress']; ?></textarea>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="userTimeZone">Timezone</label>
                            <?php echo getTimeZoneHtml( 'userTimeZone', 'true', $row['userTimeZone'] ); ?>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="userGender">Gender</label>
                            <select name="userGender" id="userGender" class="form-control">
                                <option value="male" <?php if( $row['userGender'] == 'male' ){ echo 'selected="selected"'; } ?>>Male</option>
                                <option value="female" <?php if( $row['userGender'] == 'female' ){ echo 'selected="selected"'; } ?>>Female</option>
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="userDob">Date of birth</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="userDob" name="userDob" value="<?php if( trim($row['userDob']) != '' ) { echo $row['userDob']; } ?>" data-mask="<?php echo $row['userDob'] != '' ? $row['userDob'] : ''; ?>" data-inputmask="'alias': 'yyyy/mm/dd'"  placeholder="DOB" class="form-control">
                            </div>
                            <!-- /.input group -->
                        </div>

                        <?php if(isset($row['userCountry']) && $row['userCountry']!='') {
                            $country = $row['userCountry'];
                        } else {
                            $country = 'US';
                        }?>
                        <div class="form-group has-feedback  userCountryDiv ">
                            <label for="userCountry">Country</label>
                            <select class="form-control bfh-countries userCountry" name="userCountry" id="userCountry" blank=0 data-country="<?php echo $country ?>" required >

                            </select>
                        </div>



                        <?php if(isset($row['userState']) && $row['userState']!='') {
                            $userState = $row['userState'];
                        } else {
                            $userState = 'AL';
                        }?>


                        <div class="form-group has-feedback userStateDiv"  <?php if($country !='US'){?>style="display:none;"<?php }?> >
                            <label for="userState">State</label>
                            <select class="form-control bfh-states"   <?php if($country =='US'){?>name="userState" required <?php  }else {?> name="userStateBK" <?php } ?> id="userStateInDr" data-state="<?php echo $userState ?>"  data-country="userCountry"  ></select>
                        </div>

                        <div class="form-group has-feedback userStateInput" <?php if($country =='US'){?>style="display:none;"<?php }?>>
                            <label for="userState">State</label>
                            <input type="text" class="form-control" id="userStateIn"  <?php if($country =='US'){?>name="userStateBK"<?php }else {?> name="userState" required <?php } ?> placeholder="State" value="<?php echo $userState ?>"    >
                        </div>

                        <div class="form-group has-feedback">
                            <label for="userCity">City</label>
                            <input type="text" class="form-control" id="userCity" name="userCity" value="<?php echo $row['userCity'] ?>" placeholder="City"  >
                        </div>


                        <div class="form-group has-feedback">
                            <label for="userZip">ZIP</label>
                            <input type="text" class="form-control" id="userZip" name="userZip" value="<?php echo $row['userZip'] ?>" placeholder="ZIP" required >
                        </div>
                        <div class="form-group has-feedback">
                            <label for="userTaxId">Tax ID</label>
                            <input type="text" class="form-control" id="userTaxId" name="userTaxId" value="<?php echo $row['userTaxId'] ?>" placeholder="Tax ID" required >
                        </div>
                        <div class="form-group has-feedback">
                            <label for="userNationalId">SSN</label>
                            <input type="text" class="form-control" id="userNationalId" name="userNationalId" value="<?php echo $row['userNationalId'] ?>" placeholder="SSN">
                        </div>
                        <div class="form-group has-feedback">
                            <label for="userPhoneHome">Phone - Home</label>
                            <input  type="text" class="form-control" name="userPhoneHome" id="userPhoneHome" value="<?php echo $row['userPhoneHome'] ?>" placeholder="Phone - home(xxx-xxx-xxxx)" required >

                        </div>
                        <div class="form-group has-feedback">
                            <label for="userPhoneCellular">Phone - Cellular</label>
                            <input type="text" class="form-control" name="userPhoneCellular" id="userPhoneCellular" value="<?php echo $row['userPhoneCellular'] ?>" placeholder="Phone - Cellular(xx-xxxxxxxxxx)" required >
                        </div>
                        <div class="form-group has-feedback">
                            <label for="userPhoneBusiness">Phone - Business</label>
                            <input type="text" class="form-control" name="userPhoneBusiness" id="userPhoneBusiness" value="<?php echo $row['userPhoneBusiness'] ?>" placeholder="Phone - Business(xxx-xxx-xxxx)" required >
                        </div>
                        <div class="form-group has-feedback">
                            <label for="userFax">Fax</label>
                            <input type="text" class="form-control" name="userFax" id="userFax" value="<?php echo $row['userFax'] ?>" placeholder="Fax" required >
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12  pull-right drop-zone-main">
                        <div class="upload-drop-zone userProfilePic" id="drop-zone">
                            <?php if ( isset( $doctor_data['featured_image'] ) && !empty($doctor_data['featured_image']) ):   ?>
                                <!-- <input type="hidden" name="userPicture1" value="<?php //echo $row['userPicture']['img1']; ?>" />
                    <input type="hidden" name="userPicture2" value="<?php //echo $row['userPicture']['img2']; ?>" />  -->
                                <img src="<?php echo HTTP_SERVER . 'images/doctors/full/' . $doctor_data['featured_image']; ?>" class="img img-responsive" id="hideimg" />


                            <?php  else: ?>
                                Please upload a picture.
                            <?php endif; ?>
                        </div>
                        <?php if ( isset( $doctor_data['featured_image'] ) && !empty($doctor_data['featured_image']) ){   ?>
                            <button type="button" name="deleteImgBtn" id="deleteImgBtn"  value="Remove Image"  class="btn btn-default dark-pink" onclick="return removePic()" ><i class="fa fa-times"></i> Remove Image</button>
                        <?php }?>
                        <div class="form-inline" style="margin-top: 15px;">
                            <input type="file" id="featured_image" name="featured_image" class="btn btn-default btn-block btn-sm" id="js-upload-submit">
                        </div>
                    </div>
                    <div class="profile-butten notification">
                        <button type="submit" name="btnsetsave" value="Save"  class="btn btn-default dark-pink" ><i class="fa fa-check"></i> Save</button>

                    </div>
                </form>
                <form method="post" id="removeImgForm" action="<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=settings_post" >
                    <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                    <input type="hidden" id="removePicFlag" name="removePicFlag" value="0" />
                    <input type="hidden" id="userEmailID" name="userEmailID" value="<?php echo $row['userEmail'] ?>" />
                    <input type="hidden" id="imageName" name="imageName" value="<?php echo $doctor_data['featured_image'] ?>" />
                </form>
        </div>
    </div>
</section>
<script>
    function removePic() {
        var r = confirm("Are you sure to delete image?");
        if( r == false) {
            return false;
        }
        else {
            $('#removeImgForm').submit();
        }
    }

    function phonenumber() {
        var objRegex = /^[\d -]+$/;

        if(objRegex.test($('#userPhoneHome').val()) != true) {
            $(".errorSpan").text(' Invalid Home phone. ( use digits and hyphens only)');
            $(".errorDiv").css('display', 'block');
            $("#userPhoneHome").css('border-color', 'red');
            window.scrollTo(0, 100);
            return false;
        }
        else {
            $(".errorDiv").css('display', 'none');
            $("#userPhoneHome").css('border-color', '');
        }

        if(objRegex.test($('#userPhoneCellular').val()) != true) {
            $(".errorSpan").text(' Invalid Cellular phone. ( use digits and hyphens only)');
            $(".errorDiv").css('display', 'block');
            $("#userPhoneCellular").css('border-color', 'red');
            window.scrollTo(0, 100);
            return false;
        }
        else {
            $(".errorDiv").css('display', 'none');
            $("#userPhoneCellular").css('border-color', '');
        }

        if(objRegex.test($('#userPhoneBusiness').val()) != true) {
            $(".errorSpan").text(' Invalid Business phone. ( use digits and hyphens only)');
            $(".errorDiv").css('display', 'block');
            $("#userPhoneBusiness").css('border-color', 'red');
            window.scrollTo(0, 100);
            return false;
        }
        else {
            $(".errorDiv").css('display', 'none');
            $("#userPhoneBusiness").css('border-color', '');
        }
    }
</script>