<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

checkUserSessionType(5);

?>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <?php if($_SESSION['success_result']['error_msg'] != "" && $_SESSION['success_result']['tab'] == "biodata"): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo $_SESSION['success_result']['error_msg']; ?>
                </div>
            <?php endif; ?>

            <?php if($_SESSION['success_result']['success_msg'] != "" && $_SESSION['success_result']['tab'] == "biodata"): ?>
                <div class="alert alert-success alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $_SESSION['success_result']['success_msg']; ?>
                </div>
            <?php endif; ?>
            <form method="post"  class="experience_edit_form" id="js-upload-form" action="<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=settings_post" >
                <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                <input type="hidden" name="experience-edit" >
                <div class="col-md-12 row clearfix">
                    <div class="reg-heading">Write something about your practice / experience</div>
                    <div class="box-body no-padding " >
                        <textarea name="data[experience_details]" id="experience" class="experience" rows="10" cols="80"><?php echo isset($doctor_data['experience_details']) ? html_entity_decode($doctor_data['experience_details']) : ''; ?></textarea>
                    </div>
                    <div class="col-md-12 message-section">
                        <div class="col-sm-112 padding_5  col-xs-12 xs-nopadding">
                            <div id="alerts"></div>
                            <div class="profile-butten notification">
                                <button type="submit" id="experience_edit" name="experience_edit" value="Edit" class="btn dark-pink">Save Biodata</button>
                            </div>
                        </div>
                    </div>
                </div>


                <input name="experience_edit" value="edit" type="hidden" />
            </form>

            <script>
                jQuery(function ($) {

                    tinymce.init({
                        selector:'textarea#experience',
                        height: 250,
                        element_format : 'html',
                        image_advtab: true,
                        plugins : 'advlist autolink link image imagetools lists charmap print preview autosave table media code',
                    });

                    $("#experience_edit").click(function () {
                        //$("#biodataExperience").val($('#experience').text());

                        console.log($('.experience').val());

                        //$(".experience_edit_form").submit();
                    });
                });
            </script>
        </div>
    </div>
</section>
