<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

checkUserSessionType(5);

//get all timings for this user
try {
    $query = "SELECT `id`, `dayId`, `duration`, `startTime`,`endTime` FROM `" . DB_PREFIX . "doctor_timing` WHERE doctorId = ?
ORDER BY `dayId` asc, `startTime` asc";
    $st = $db->prepare($query);
    $st->execute(array($_SESSION["mvdoctorID"]));

    if ($st->rowCount()) {
        $data = $st->fetchAll();
    } else {
        $data = array();
    }
} catch (Exception $Exception) {
    die("DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}
?>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <div class="col-md-12 row clearfix" id="doctorTimingDiv">
                <div class="reg-heading">Date &amp; Time <em>All date and times are in UTC Format.</em></div>

                <table class="table table-striped custab" id="doctorTimingTbl">
                    <thead>
                    <tr>
                        <th>Day</th>
                        <th>Duration</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th class="text-right">Select</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (! empty( $data ) ):
                        foreach ( $data as $timing ): ?>
                            <tr class="row_<?php echo $timing['id']; ?>_1">
                                <td><?php echo getNumberToDay( $timing['dayId'] ); ?></td>
                                <td><?php echo number2time( $timing['duration'] ); ?></td>
                                <td><?php echo date('h:i A' ,$timing['startTime']); ?></td>
                                <td><?php echo date('h:i A' ,$timing['endTime']); ?></td>
                                <td class="text-right">
                                    <a class='btn btn-info btn-xs' data-toggle="collapse" data-target="#doctorTiming<?php echo $timing['id']; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                    <a class='btn btn-default dark-pink deletePlanModal' data-id="<?php echo $timing['id']; ?>"><span class="fa fa-minus-circle"></span> Delete</a>
                                </td>
                            </tr>
                            <tr class="row_<?php echo $timing['id']; ?>_2">
                                <td colspan="6">
                                    <!-- Edit Consultations Start here -->
                                    <div class="collapse"  id="doctorTiming<?php echo $timing['id']; ?>">
                                        <form method="post" action="index.php?do=ajax&action=add_doctors_timing" class="doctorTimingForm">
                                            <input type="hidden" name="token" value="<?php echo getToken(); ?>" />
                                            <input type="hidden" name="task" value="edit" />
                                            <input type="hidden" name="id" value="<?php echo $timing['id']; ?>" />
                                            <div class='form-row'>
                                                <div class='col-xs-3 form-group required'>
                                                    <label class='control-label'>Select Day</label>
                                                    <select name="dayId" class="form-control">
                                                        <option value="1" <?php echo isset($timing['dayId']) && $timing['dayId'] == '1' ? "selected='selected'" : ''; ?>>Monday</option>
                                                        <option value="2" <?php echo isset($timing['dayId']) && $timing['dayId'] == '2' ? "selected='selected'" : ''; ?>>Tuesday</option>
                                                        <option value="3" <?php echo isset($timing['dayId']) && $timing['dayId'] == '3' ? "selected='selected'" : ''; ?>>Wednesday</option>
                                                        <option value="4" <?php echo isset($timing['dayId']) && $timing['dayId'] == '4' ? "selected='selected'" : ''; ?>>Thursday</option>
                                                        <option value="5" <?php echo isset($timing['dayId']) && $timing['dayId'] == '5' ? "selected='selected'" : ''; ?>>Friday</option>
                                                        <option value="6" <?php echo isset($timing['dayId']) && $timing['dayId'] == '6' ? "selected='selected'" : ''; ?>>Saturday</option>
                                                    </select>
                                                </div>

                                                <div class='col-xs-3 form-group required'>
                                                    <label class='control-label'>Duration</label>
                                                    <select name="duration" class='form-control' data-live-search="true">
                                                        <option value="1" <?php echo $timing['duration'] == '1' ? 'selected="selected"' : ""; ?>>15 Minutes</option>
                                                        <option value="2" <?php echo $timing['duration'] == '2' ? 'selected="selected"' : ""; ?>>30 Minutes</option>
                                                    </select>
                                                </div>

                                                <div class='col-xs-6 form-group required'>
                                                    <div class='col-xs-12 row'><label class='control-label'>Start Time</label></div>
                                                    <div class='col-xs-5 row'>
                                                        <div class="form-group">
                                                            <div class='input-group date' id="datetimepicker_<?php echo $timing['id']; ?>">
                                                                <input type='text' name="startTime" value="<?php echo date('h:i A', $timing['startTime']); ?>" class="form-control" />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class='pull-right'>
                                                        <button type="submit" class="btn btn-default dark-pink submit update"><i class="fa fa-pencil"></i> Update</button>
                                                        <a class='btn btn-default dark-pink' data-toggle="collapse" data-target="#doctorTiming<?php echo $timing['id']; ?>"><span class="fa fa-ban"></span> Cancel</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <script type="text/javascript">
                                            $(function () {
                                                $('#datetimepicker_<?php echo $timing['id']; ?>').datetimepicker({
                                                    format: 'LT'
                                                });
                                            });
                                        </script>
                                    </div>
                                    <!-- Edit Consultations Ends here -->
                                </td>
                            </tr>

                        <?php endforeach;

                    endif; ?>

                    </tbody>


                </table>

                <!-- Add Timing Start here -->
                <table class="table table-striped custab">
                    <tbody><tr>
                        <td><button type="button" class="btn btn-default dark-pink pull-right" data-toggle="collapse" data-target="#timing-add"><i class="fa fa-plus"></i> Add New Timing </button>	</td>
                    </tr>
                    </tbody>
                </table>

                <div class="collapse"  id="timing-add">
                    <form method="post" action="index.php?do=ajax&action=add_doctors_timing" class="doctorTimingForm">
                        <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                        <input type="hidden" name="task" value="add" />
                        <div class='form-row'>
                            <div class='col-xs-4 form-group required'>
                                <label class='control-label'>Select Day</label>
                                <select name="dayId" class="form-control">
                                    <option value="1">Monday</option>
                                    <option value="2">Tuesday</option>
                                    <option value="3">Wednesday</option>
                                    <option value="4">Thursday</option>
                                    <option value="5">Friday</option>
                                    <option value="6">Saturday</option>
                                </select>
                            </div>

                            <div class='col-xs-2 form-group required'>
                                <label class='control-label'>Duration</label>
                                <select name="duration" class='form-control' data-live-search="true">
                                    <option value="1">15 Minutes</option>
                                    <option value="2">30 Minutes</option>
                                </select>
                            </div>

                            <div class='col-xs-6 form-group required'>
                                <div class='col-xs-12 row'><label class='control-label'>Start Time</label></div>
                                <div class='col-xs-5 row'>
                                    <div class="form-group">
                                        <div class='input-group date' id='doctorTimingsMain'>
                                            <input type='text' name="startTime" class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-xs-7 pull-right'>
                                    <button type="submit" class="btn btn-default dark-pink submit"><i class="fa fa-plus"></i> Add </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Add Timing Ends here -->
            </div>
        </div>
    </div>
</section>

<!-- ajax response modal start here -->
<div class="modal fade" id="ajaxResponseMessage" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Message</h4>
            </div>
            <div class="modal-body">
                <div id="ajax_messages"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- ajax response modal ends here -->

<!-- delete confirmation modal start here -->
<div class="modal fade" id="deletePlansModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirm Deletion</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info" role="alert" id="removeModalPlanContent"><strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Warning!</strong> Are you sure to delete this plan?</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default deletePlan"><i class="fa fa-minus-circle" aria-hidden="true"></i> Delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- delete confirmation modal ends here -->

<script type="text/javascript">
    jQuery(function($){

        //initialize time picker
        $('#doctorTimingsMain').datetimepicker({
            format: 'LT',
            useCurrent: false,
            minDate: '<?php echo date('m/d/Y', time()); ?>',
        });

        //add/edit doctors timing
        $('#doctorTimingDiv').on('submit', '.doctorTimingForm', function(){
            //disable whole form
            var th = $(this);
            th.find('input[type="text"], select').prop('readonly', true);
            th.find('button').prop('disabled', true);
            th.find('button.submit').html('<i class="fa fa-spinner"></i> Please wait...');
            th.find('button.update').html('<i class="fa fa-spinner"></i> Please wait...');

            var type = th.find('input[name="task"]').val();
            var id = '';
            if ( type == 'edit' ) {
                id = th.find('input[name="id"]').val();
            }
            var data = th.serializeArray();
            //console.log(data);

            $.ajax({
                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=add_doctors_timing",
                data: data,
                method: "POST",
                dataType: "json"
            }).error(function (err) {
                th.find('input[type="text"], select').prop('readonly', false);
                th.find('button').prop('disabled', false);
                th.find('button.submit').html('<i class="fa fa-plus"></i> Add');
                th.find('button.update').html('<i class="fa fa-pencil"></i> Update');

                var str = '<div class="alert alert-danger" role="alert"><strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error</strong> Couldn\'t completed ajax request. Please contact site administrator.</div>';
                $('#ajax_messages').html(str + '<p><strong>Response Text: </strong>' + err.responseText).show();
                $('#ajaxResponseMessage').modal('show');

            }).done(function () {
                th.find('button.submit').html('<i class="fa fa-plus"></i> Add');
                th.find('button.update').html('<i class="fa fa-pencil"></i> Update');
                th.find('button').prop('disabled', false);
                th.find('input[type="text"], select').prop('readonly', false);
            }).success(function (data) {
                if ( data.success ) {

                    if ( type == 'add' ) {
                        $('#doctorTimingTbl tbody').append(data.html);
                    }
                    else if ( type == 'edit' ) {
                        //replace tr with data
                        $('tr.row_' + id + '_1').html(data.row1);
                        $('tr.row_' + id + '_2').html(data.row2);
                    }

                    if( data.message ) {
                        var str = '';
                        $.each(data.message, function(index, msg){
                            str += '<div class="alert alert-success alert-dismissible" role="alert">\
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                            <strong><i class="fa fa-check" aria-hidden="true"></i> Success!</strong> ' + msg + ' \
                            </div>';
                        });
                        $('#ajax_messages').html( str );
                        $('#ajaxResponseMessage').modal('show');
                    }

                    th.find('input[type="text"], select').val('');

                } else if(data.error) {
                    var str = '';
                    $.each(data.error, function(index, msg){
                        str += '<div class="alert alert-danger alert-dismissible" role="alert">\
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                            <strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error!</strong> ' + msg + ' \
                            </div>';
                    });
                    $('#ajax_messages').html( str );
                    $('#ajaxResponseMessage').modal('show');
                }
                else {
                    alert('Unknown Error!');
                }
            }, 'json');

            return false;
        });

        //show delete plan modal and update plan_id
        $('#doctorTimingDiv').on('click', '.deletePlanModal', function(){
            plan_id = $(this).data('id');
            $('#deletePlansModal').modal();
        });

        //now delete plan
        $('#deletePlansModal').on('click', '.deletePlan', function(){
            var th = $(this);

            if ( plan_id > 0 ) {

                $('#removeModalPlanContent').html('<i class="fa fa-spinner"></i> Please wait...');
                //call ajax
                th.parent().find('button').prop('readonly', true);
                $.ajax({
                    url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=add_doctors_timing",
                    data: {task: 'delete', id: plan_id, token: '<?php echo getToken(); ?>'},
                    method: "POST",
                    dataType: "json"
                }).error(function (err) {

                    th.parent().find('button').prop('readonly', false);
                    $('#removeModalPlanContent').html('<strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Warning!</strong> Warning!</strong> Are you sure to delete this plan?');
                    console.log(err);

                }).done(function () {

                    th.parent().find('button').prop('readonly', false);
                    $('#removeModalPlanContent').html('<strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Warning!</strong> Warning!</strong> Are you sure to delete this plan?');
                    //hide modal
                    $('#deletePlansModal').modal('hide');

                }).success(function (data) {

                    if (data.success) {

                        $('tr.row_' + plan_id + '_1').remove();
                        $('tr.row_' + plan_id + '_2').remove();

                        if( data.message ) {
                            var str = '';
                            $.each(data.message, function(index, msg){
                                str += '<div class="alert alert-success alert-dismissible" role="alert">\
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                            <strong><i class="fa fa-check" aria-hidden="true"></i> Success!</strong> ' + msg + ' \
                            </div>';
                            });
                            $('#ajax_messages').html( str );
                        }

                    }  else if(data.error) {
                        var str = '';
                        $.each(data.error, function(index, msg){
                            str += '<div class="alert alert-danger alert-dismissible" role="alert">\
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                            <strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error!</strong> ' + msg + ' \
                            </div>';
                        });
                        $('#ajax_messages').html( str );
                    }
                    else {
                        alert('Unknown Error!');
                    }

                }, 'json');

            } else {
                $('#removeModalPlanContent').html('<strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Warning!</strong> Warning!</strong> Are you sure to delete this plan?');
                $('#favoriteModal').html('<div class="alert alert-danger" role="alert">Please login to remove article from list.</div>');
                $('#myModalFav').modal();
            }
        });

    });
</script>