<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
checkUserSessionType(5);

//get all plan
$query = "SELECT * FROM `" . DB_PREFIX ."plans` WHERE doctorId = ?";
$st = $db->prepare($query);
$st->execute(array($_SESSION["mvdoctorID"]));

if ( $st->rowCount() ) {
    $data = $st->fetchAll();
} else {
    $data = array();
}
?>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <div class="col-md-12 row clearfix" id="consultationDiv">
                <div class="reg-heading">Consultation &amp; Prices</div>

                <div id="ajax_messages"></div>

                <table class="table table-striped custab" id="consultationPlanTbl">
                    <thead>
                    <tr>
                        <th>Consultation Duration</th>
                        <th>Add Price</th>
                        <th class="text-right">Select</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (! empty( $data ) ):
                        foreach ( $data as $plan ): ?>
                            <tr class="row_<?php echo $plan['id']; ?>_1">
                                <td><?php echo number2time($plan['time']); ?></td>
                                <td>$<?php echo $plan['price']; ?></td>
                                <td class="text-right">
                                    <a class='btn btn-info btn-xs' data-toggle="collapse" data-target="#consultations<?php echo $plan['id']; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                    <a class='btn btn-default dark-pink deletePlanModal' data-id="<?php echo $plan['id']; ?>"><span class="fa fa-minus-circle"></span> Delete</a>
                                </td>
                            </tr>
                            <tr class="row_<?php echo $plan['id']; ?>_2">
                                <td colspan="4">
                                    <!-- Edit Consultations Start here -->
                                    <div class="collapse"  id="consultations<?php echo $plan['id']; ?>">
                                        <form method="post" action="index.php?do=ajax&action=add_consultation_plan" class="consultaionForm">
                                            <input type="hidden" name="token" value="<?php echo getToken(); ?>" />
                                            <input type="hidden" name="task" value="edit" />
                                            <input type="hidden" name="id" value="<?php echo $plan['id']; ?>" />
                                            <div class='form-row'>
                                                <div class='col-xs-4 form-group required'>
                                                    <label class='control-label'>Consultation Duration</label>
                                                    <select name="time" class='form-control' data-live-search="true">
                                                        <option value="1" <?php echo $plan['time'] == '1' ? 'selected="selected"' : ""; ?>>15 Minutes</option>
                                                        <option value="2" <?php echo $plan['time'] == '2' ? 'selected="selected"' : ""; ?>>30 Minutes</option>
                                                    </select>
                                                </div>

                                                <div class='col-xs-6 form-group required'>
                                                    <div class='col-xs-12 row'><label class='control-label'>Add Price</label></div>
                                                    <div class='col-xs-5 row'>
                                                        <input class='form-control' name="price" value="<?php echo $plan['price']; ?>" type='text' placeholder="Add Price">
                                                    </div>
                                                    <div class='col-xs-7 pull-right'>
                                                        <button type="submit" class="btn btn-default dark-pink submit update"><i class="fa fa-pencil"></i> Update</button>
                                                        <a class='btn btn-default dark-pink' data-toggle="collapse" data-target="#consultations1"><span class="fa fa-ban"></span> Cancel</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- Edit Consultations Ends here -->
                                </td>
                            </tr>

                        <?php endforeach;

                    endif; ?>

                    </tbody>
                </table>





                <!-- Add Consultations Start here -->
                <table class="table table-striped custab">
                    <tbody><tr>
                        <td><button type="button" class="btn btn-default dark-pink pull-right" data-toggle="collapse" data-target="#consultation-add"><i class="fa fa-plus"></i> Add Consultation </button>	</td>
                    </tr>
                    </tbody>
                </table>

                <div class="collapse"  id="consultation-add">
                    <form method="post" action="index.php?do=ajax&action=add_consultation_plan" class="consultaionForm">
                        <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                        <input type="hidden" name="task" value="add" />
                        <div class='form-row'>

                            <div class='col-xs-4 form-group required'>
                                <label class='control-label'>Consultation Duration</label>
                                <select name="time" class='form-control' data-live-search="true">
                                    <option value="1">15 Minutes</option>
                                    <option value="2">30 Minutes</option>
                                </select>
                            </div>

                            <div class='col-xs-6 form-group required'>
                                <div class='col-xs-12 row'><label class='control-label'>Add Price</label></div>
                                <div class='col-xs-5 row'>
                                    <input class='form-control' name="price" type='text' placeholder="Add Price">
                                </div>
                                <div class='col-xs-7 pull-right'>
                                    <button type="submit" class="btn btn-default dark-pink submit"><i class="fa fa-plus"></i> Add </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Add Consultations Ends here -->
            </div>
        </div>
    </div>
</section>

<!-- delete confirmation modal start here -->
<div class="modal fade" id="deletePlansModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirm Deletion</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info" role="alert" id="removeModalPlanContent"><strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Warning!</strong> Are you sure to delete this plan?</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default deletePlan"><i class="fa fa-minus-circle" aria-hidden="true"></i> Delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- delete confirmation modal ends here -->

<script type="text/javascript">
    jQuery(function($){

        var plan_id;

        //add/edit consultation plan
        $('#consultationDiv').on('submit', '.consultaionForm', function(){
            //disable whole form
            var th = $(this);
            th.find('input[type="text"], select').prop('readonly', true);
            th.find('button').prop('disabled', true);
            th.find('button.submit').html('<i class="fa fa-spinner"></i> Please wait...');
            th.find('button.update').html('<i class="fa fa-spinner"></i> Please wait...');

            var type = th.find('input[name="task"]').val();
            var id = '';
            if ( type == 'edit' ) {
                id = th.find('input[name="id"]').val();
            }
            var data = th.serializeArray();
            console.log(data);

            $.ajax({
                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=add_consultation_plan",
                data: data,
                method: "POST",
                dataType: "json"
            }).error(function (err) {
                th.find('input[type="text"], select').prop('readonly', false).val('');
                th.find('button').prop('disabled', false);
                th.find('button.submit').html('<i class="fa fa-plus"></i> Add');
                th.find('button.update').html('<i class="fa fa-pencil"></i> Update');

                console.log(err);

            }).done(function () {
                th.find('input[type="text"], select').prop('readonly', false).val('');
                th.find('button').prop('disabled', false);
                th.find('button.submit').html('<i class="fa fa-plus"></i> Add');
                th.find('button.update').html('<i class="fa fa-pencil"></i> Update');

            }).success(function (data) {
                if ( data.success ) {

                    if ( type == 'add' ) {
                        $('#consultationPlanTbl tbody').append(data.html);
                    }
                    else if ( type == 'edit' ) {
                        //replace tr with data
                        $('tr.row_' + id + '_1').html(data.row1);
                        $('tr.row_' + id + '_2').html(data.row2);
                    }

                    if( data.message ) {
                        var str = '';
                        $.each(data.message, function(index, msg){
                            str += '<div class="alert alert-success alert-dismissible" role="alert">\
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                            <strong><i class="fa fa-check" aria-hidden="true"></i> Success!</strong> ' + msg + ' \
                            </div>';
                        });
                        $('#ajax_messages').html( str );
                    }


                } else if(data.error) {
                    var str = '';
                    $.each(data.error, function(index, msg){
                        str += '<div class="alert alert-danger alert-dismissible" role="alert">\
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                            <strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error!</strong> ' + msg + ' \
                            </div>';
                    });
                    $('#ajax_messages').html( str );
                }
                else {
                    alert('Unknown Error!');
                }
            }, 'json');

            return false;
        });

        //show delete plan modal and update plan_id
        $('#consultationDiv').on('click', '.deletePlanModal', function(){
            plan_id = $(this).data('id');
            $('#deletePlansModal').modal();
        });

        //now delete plan
        $('#deletePlansModal').on('click', '.deletePlan', function(){
            var th = $(this);

            if ( plan_id > 0 ) {

                $('#removeModalPlanContent').html('<i class="fa fa-spinner"></i> Please wait...');
                //call ajax
                th.parent().find('button').prop('readonly', true);
                $.ajax({
                    url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=add_consultation_plan",
                    data: {task: 'delete', id: plan_id, token: '<?php echo getToken(); ?>'},
                    method: "POST",
                    dataType: "json"
                }).error(function (err) {

                    th.parent().find('button').prop('readonly', false);
                    $('#removeModalPlanContent').html('<strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Warning!</strong> Warning!</strong> Are you sure to delete this plan?');
                    console.log(err);

                }).done(function () {

                    th.parent().find('button').prop('readonly', false);
                    $('#removeModalPlanContent').html('<strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Warning!</strong> Warning!</strong> Are you sure to delete this plan?');
                    //hide modal
                    $('#deletePlansModal').modal('hide');

                }).success(function (data) {

                    if (data.success) {

                        $('tr.row_' + plan_id + '_1').remove();
                        $('tr.row_' + plan_id + '_2').remove();

                        if( data.message ) {
                            var str = '';
                            $.each(data.message, function(index, msg){
                                str += '<div class="alert alert-success alert-dismissible" role="alert">\
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                            <strong><i class="fa fa-check" aria-hidden="true"></i> Success!</strong> ' + msg + ' \
                            </div>';
                            });
                            $('#ajax_messages').html( str );
                        }

                    }  else if(data.error) {
                        var str = '';
                        $.each(data.error, function(index, msg){
                            str += '<div class="alert alert-danger alert-dismissible" role="alert">\
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                            <strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error!</strong> ' + msg + ' \
                            </div>';
                        });
                        $('#ajax_messages').html( str );
                    }
                    else {
                        alert('Unknown Error!');
                    }

                }, 'json');

            } else {
                $('#removeModalPlanContent').html('<strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Warning!</strong> Warning!</strong> Are you sure to delete this plan?');
                $('#favoriteModal').html('<div class="alert alert-danger" role="alert">Please login to remove article from list.</div>');
                $('#myModalFav').modal();
            }
        });

    });

</script>