<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

checkUserSessionType(5);

?>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <?php if($_SESSION['success_result']['error_msg'] != "" && $_SESSION['success_result']['tab'] == "education"): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo $_SESSION['success_result']['error_msg']; ?>
                </div>
            <?php endif; ?>

            <?php if($_SESSION['success_result']['success_msg'] != "" && $_SESSION['success_result']['tab'] == "education"): ?>
                <div class="alert alert-success alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $_SESSION['success_result']['success_msg']; ?>
                </div>
            <?php endif; ?>



            <form method="post"  id="js-upload-form" action="<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=settings_post" class="addEducationForm" >
                <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                <input type="hidden" name="education_edit" >
                <input type="hidden" name="action" value="update" id="educationAction" >


                <div class="col-md-12 custyle row clearfix">
                    <div class="reg-heading">Education and Training</div>

                    <table class="table table-striped custab">
                        <thead>
                        <tr>
                            <th>Education Name</th>
                            <th>Institute / College</th>
                            <th>Year of Graduation</th>
                            <th class="text-right">Select</th>
                        </tr>
                        </thead>

                        <?php if ( isset( $doctor_data['education']['name'] ) && !empty( $doctor_data['education']['name'] ) ){ $edu_cnt = count( $doctor_data['education']['name'] )   ?>
                            <?php  for ( $idx = 0; $idx < $edu_cnt; $idx++ ) {  ?>
                                <tr>
                                    <td><?php  echo $doctor_data['education']['name'][$idx];  ?></td>
                                    <td><?php  echo $doctor_data['education']['institute'][$idx];  ?></td>
                                    <td><?php  echo $doctor_data['education']['year'][$idx];  ?></td>

                                    <td class="text-right"><a class='btn btn-info btn-xs' data-toggle="collapse" data-target="#editeducation<?php echo $idx; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                        <button type="button" class="btn btn-info btn-xs dark-pink" data-toggle="modal" data-target="#delteeducationModal<?php echo $idx; ?>"><i class="fa fa-times"></i>Delete</button>

                                    </td>
                                </tr>

                                <div class="modal fade" id="delteeducationModal<?php echo $idx; ?>" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Confirm Deletion</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure to delete this Education Record?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary btn-sm"onclick='deleteEducation("editeducation<?php echo $idx;?>")'  )" >Ok</button>
                                                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <tr><td colspan="4" id="workPlaceDivsInLoop">

                                        <div class="collapse"  id="editeducation<?php echo $idx; ?>">
                                            <div class="form-group has-feedback">
                                                <input type="text" class="form-control col-md-12" id="data[education][<?php echo $idx; ?>]" name="data[education][name][]" value="<?php  echo $doctor_data['education']['name'][$idx];  ?>" placeholder="Education Name">
                                            </div>
                                            <div class="form-group has-feedback">
                                                <input type="text" class="form-control col-md-12" id="userFname" name="data[education][institute][]" value="<?php  echo $doctor_data['education']['institute'][$idx];  ?>" placeholder="Institute">
                                            </div>
                                            <div class="form-group has-feedback">
                                                <input type="text" id="eduYear" class="form-control col-md-12 eduYear"  name="data[education][year][]" value="<?php  echo $doctor_data['education']['year'][$idx];  ?>" placeholder="Year">
                                            </div>
                                            <div class="form-group has-feedback">
                                                <button type="submit" name="education_edit" class="btn btn-default dark-pink"><i class="fa fa-plus"></i> Update</button>

                                                <button type="button"data-toggle="collapse" data-target="#editeducation<?php echo $id; ?>"  class="btn btn-default dark-pink"><i class="fa fa-times"></i> Cancel</button>
                                            </div>
                                        </div>
                                    </td></tr>
                                <?php

                            }
                        }else{ ?>
                            <tr>
                                <td colspan="4"> <div class="alert alert-warning" role="alert">No Records found!!</div></td>

                            </tr>
                        <?php }
                        ?>
                    </table>


                    <table class="table table-striped custab">
                        <tr>
                            <td><button type="button" class="btn btn-default dark-pink pull-right"  id="addEducationShowHide"><i class="fa fa-plus"></i> Add Education </button>	</td>
                        </tr>
                    </table>
                </div>
                <div id="addEducationDiv"></div>
            </form>

        </div>
    </div>
</section>

<script>
    var addDivEducation = ' <div class="addawardsDiv"  id="addeducationDiv"><div class="form-group has-feedback"><input type="text" class="form-control col-md-12"  name="data[education][name][]" value="" placeholder="education Name"> </div><div class="form-group has-feedback"><input type="text" class="form-control col-md-12"  name="data[education][institute][]" value="" placeholder="Institute"> </div><div class="form-group has-feedback"><input type="text" class="form-control col-md-12"  name="data[education][year][]" id="eduYear1" value="" placeholder="Year"> </div><div class="form-group has-feedback"><button type="submit" name="education_edit" class="btn btn-default dark-pink"><i class="fa fa-plus"></i> Save</button>  <button type="button" class="btn btn-default dark-pink" onclick="removeaddEducationDiv()" ><i class="fa fa-plus"></i> Cancel</button></div></div>';

    $("#addEducationShowHide").click(function () {

        $("#addEducationDiv").empty();
        $("#addEducationDiv").append(addDivEducation);
        $("#educationAction").val('add');

        $('#eduYear1').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });

    function removeaddEducationDiv(){

        $("#addEducationDiv").empty();
        $("#educationAction").val('update');
    }

    function deleteEducation(divId2)
    {
        /*  if ( !confirm('Are you sure you want to delete this record ?') ) {
         return false;
         } */
        $("#educationAction").val('delete');
        $("#"+divId2).html('');
        $(".addEducationForm").submit();


    }
</script>