<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
checkUserSessionType(5);
?>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <?php if($_SESSION['success_result']['error_msg'] != "" && $_SESSION['success_result']['tab'] == "profile"): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo $_SESSION['success_result']['error_msg']; ?>
                </div>
            <?php endif; ?>

            <?php if($_SESSION['success_result']['success_msg'] != "" && $_SESSION['success_result']['tab'] == "profile"): ?>
                <div class="alert alert-success alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $_SESSION['success_result']['success_msg']; ?>
                </div>
            <?php endif; ?>
            <form method="post"  id="js-upload-form" action="<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=settings_post" >
                <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                <div class="col-md-12 row">

                    <div class="reg-heading">Write something about yourself</div>
                    <div class="box-body no-padding " >
                        <div class="box-body no-padding " >
                            <textarea name="data[overview]" id="profileoverview" class="experience" rows="10" cols="80"><?php echo isset($doctor_data['overview']) ? html_entity_decode($doctor_data['overview']) : ''; ?></textarea>
                        </div><br />
                        <div class="form-group has-feedback">
                            <label for="specialization">Specialization</label>
                            <input value="<?php echo isset($doctor_data['specialization']) ? $doctor_data['specialization'] : ''; ?>" type="text" class="form-control" name="data[specialization]" id="specialization"  placeholder="Specialization" required >
                        </div>
                        <div class="form-group has-feedback">
                            <label for="degree">Degree</label>
                            <input value="<?php echo isset($doctor_data['degree']) ? $doctor_data['degree'] : ''; ?>" type="text" class="form-control" name="data[degree]" id="degree" placeholder="Degree" required  >
                        </div>
                        <div class="form-group has-feedback">
                            <label for="fees">Fees</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa  fa-usd"></i>
                                </div>
                                <input value="<?php echo isset($doctor_data['fees']) ? $doctor_data['fees'] : ''; ?>" type="number" min="1" max="99999" class="form-control" name="data[fees]" id="fees" placeholder="Fees" >
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="location">Location</label>
                            <input value="<?php echo isset($doctor_data['location']) ? $doctor_data['location'] : ''; ?>" type="text" class="form-control" name="data[location]" id="location" placeholder="Location"  required >
                        </div>
                        <div class="form-group has-feedback">
                            <label for="experience">Experience Years</label>
                            <input value="<?php echo isset($doctor_data['experience']) ? $doctor_data['experience'] : ''; ?>" type="number" class="form-control" min="0" name="data[experience]" id="experience"  placeholder="Experience Years" >
                        </div>

                    </div>
                    <div class="profile-butten notification">
                        <button type="submit"  name="profile_edit" value="Edit"  class="btn btn-default dark-pink"><i class="fa fa-check"></i> Save</button>
                    </div>
            </form>
        </div>
    </div>
</section>

<script>
    jQuery(function ($) {

        tinymce.init({
            selector:'textarea#profileoverview',
            height: 250,
            element_format : 'html',
            image_advtab: true,
            plugins : 'advlist autolink link image imagetools lists charmap print preview autosave table media code',
        });

    });
</script>