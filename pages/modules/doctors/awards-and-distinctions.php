<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

checkUserSessionType(5);

?>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <?php if($_SESSION['success_result']['error_msg'] != "" && $_SESSION['success_result']['tab'] == "award"): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo $_SESSION['success_result']['error_msg']; ?>
                </div>
            <?php endif; ?>

            <?php if($_SESSION['success_result']['success_msg'] != "" && $_SESSION['success_result']['tab'] == "award"): ?>
                <div class="alert alert-success alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $_SESSION['success_result']['success_msg']; ?>
                </div>
            <?php endif; ?>

            <form method="post"  id="js-upload-form" action="<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=settings_post" class="addAwardForm" >
                <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                <input type="hidden" name="award_edit" >
                <input type="hidden" name="action" value="update" id="awardAction" >
                <div class="col-md-12 custyle row clearfix">
                    <div class="reg-heading">Awards & Distinctions</div>

                    <table class="table table-striped custab">
                        <thead>
                        <tr>
                            <th>Award / Distinction Name</th>
                            <th>Venue</th>
                            <th>Year</th>
                            <th class="text-right">Select</th>
                        </tr>
                        </thead>
                        <?php if ( isset( $doctor_data['awards']['name'] ) && !empty( $doctor_data['awards']['name'] ) ){
                            $award_cnt = count( $doctor_data['awards']['name'] )  ?>
                            <?php  for ( $idx = 0; $idx < $award_cnt; $idx++ ) {  ?>
                                <tr>
                                    <td><?php  echo $doctor_data['awards']['name'][$idx];  ?></td>
                                    <td><?php  echo $doctor_data['awards']['venue'][$idx];  ?></td>
                                    <td><?php  echo $doctor_data['awards']['year'][$idx];  ?></td>

                                    <td class="text-right"><a class='btn btn-info btn-xs' data-toggle="collapse" data-target="#editawards<?php echo $idx; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                        <button type="button" class="btn btn-info btn-xs dark-pink " data-toggle="modal" data-target="#delteAwardModal<?php echo $idx; ?>"><i class="fa fa-times"></i> Delete</button>

                                    </td>
                                </tr>

                                <div class="modal fade" id="delteAwardModal<?php echo $idx; ?>" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Confirm Deletion</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure to delete this Award Record?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary btn-sm"onclick='deleteAward("editawards<?php echo $idx;?>")'  >Ok</button>
                                                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <tr><td colspan="4" id="workPlaceDivsInLoop">

                                        <div class="collapse"  id="editawards<?php echo $idx; ?>">
                                            <div class="form-group has-feedback">
                                                <input type="text" class="form-control col-md-12" id="data[awards][<?php echo $idx; ?>]" name="data[awards][name][]" value="<?php  echo $doctor_data['awards']['name'][$idx];  ?>" placeholder="Award / Distinction Name">
                                            </div>
                                            <div class="form-group has-feedback">
                                                <input type="text" class="form-control col-md-12" id="userFname" name="data[awards][venue][]" value="<?php  echo $doctor_data['awards']['venue'][$idx];  ?>" placeholder="Venue">
                                            </div>
                                            <div class="form-group has-feedback">
                                                <input type="text" class="form-control col-md-12 awardYear" id="awardYear" name="data[awards][year][]" value="<?php  echo $doctor_data['awards']['year'][$idx];  ?>" placeholder="Year">
                                            </div>
                                            <div class="form-group has-feedback">
                                                <button type="submit" name="award_edit" class="btn btn-default dark-pink"><i class="fa fa-plus"></i> Update</button>

                                                <button type="button"  data-toggle="collapse" data-target="#editawards<?php echo $idx; ?>"  class="btn btn-default dark-pink"><i class="fa fa-times"></i> Cancel</button>

                                            </div>
                                        </div>
                                    </td></tr>
                                <?php

                            }
                        }else{ ?>
                            <tr>
                                <td colspan="4"> <div class="alert alert-warning" role="alert">No Records found!!</div></td>

                            </tr>
                        <?php }
                        ?>


                    </table>

                    <table class="table table-striped custab">
                        <tr>
                            <td><button type="button" class="btn btn-default dark-pink pull-right"  id="addAwardShowHide"><i class="fa fa-plus"></i> Add Award </button>	</td>
                        </tr>
                    </table>

                </div>
                <div id="addAwardDiv"></div>
            </form>

        </div>
    </div>
</section>

<script>
    var addDivAwatd = ' <div class="addawardsDiv"  id="addawardsDiv"><div class="form-group has-feedback"><input type="text" class="form-control col-md-12" name="data[awards][name][]" value="" placeholder="Award / Distinction Name"> </div><div class="form-group has-feedback"><input type="text" class="form-control col-md-12"  name="data[awards][venue][]" value="" placeholder="Venue"> </div><div class="form-group has-feedback"><input type="text" class="form-control col-md-12" " name="data[awards][year][]" id="awardYear1" value="" placeholder="Year"> </div><div class="form-group has-feedback"><button type="submit" name="award_edit" class="btn btn-default dark-pink"><i class="fa fa-plus"></i> Save</button>  <button type="button" class="btn btn-default dark-pink" onclick="removeaddAwardDiv()" ><i class="fa fa-plus"></i> Cancel</button></div></div>';

    $("#addAwardShowHide").click(function () {
        /* console.log('kjhjg');
         console.log(addDivAwatd); */
        $("#addAwardDiv").empty();
        $("#addAwardDiv").append(addDivAwatd);
        $("#awardAction").val('add');
        $('#awardYear1').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });

    function removeaddAwardDiv(){

        $("#addAwardDiv").empty();
        $("#awardAction").val('update');
    }

    function deleteAward(divId2)
    {
        /*  if ( !confirm('Are you sure you want to delete this record ?') ) {
         return false;
         } */
        $("#awardAction").val('delete');
        $("#"+divId2).html('');
        $(".addAwardForm").submit();


    }
</script>