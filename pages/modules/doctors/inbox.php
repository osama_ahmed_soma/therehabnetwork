<?php

//deny direct access

if ( !defined('MVD_SITE') ) {

    die('You are not authorized to view this page');

}

checkUserSessionType(5);

?>

<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">

    <div class="container">

        <div class="row">

        <div class="inbox-body">
            <div class="inbox-header">
                <h1 class="pull-left">Inbox</h1>
                <form class="form-inline pull-right" action="index.html">
                    <div class="input-group input-medium">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">
                            <button type="submit" class="btn green">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
            <div class="inbox-content"><table class="table table-striped table-advance table-hover">
   
        <div class="top-area-inbox">
            <div class="col-md-6 col-sm-5 col-xs-5">	
                <div class="btn-group input-actions" style="margin-left:0px;">
                    <a class="btn btn-sm blue btn-info btn-outline dropdown-toggle sbold" href="javascript:;" data-toggle="dropdown"> Actions
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-pencil"></i> Mark as Read </a>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-ban"></i> Spam </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-trash-o"></i> Delete </a>
                        </li>
                    </ul>
                </div>
                	 <a href="<?php echo HTTP_SERVER . 'doctor-dashboard/compose.html' ?>" class="btn dark-pink"> Inbox</a>
                	 <a href="<?php echo HTTP_SERVER . 'doctor-dashboard/compose.html' ?>" class="btn btn-info"> Sent</a>
                	 <a href="<?php echo HTTP_SERVER . 'doctor-dashboard/compose.html' ?>" class="btn btn-info"><i class="fa fa-star"></i> Favorites</a>
                	 <a href="<?php echo HTTP_SERVER . 'doctor-dashboard/compose.html' ?>" class="btn dark-pink"> Compose</a>
            </div>
            <div class="pagination-control col-md-6 col-xs-7  col-sm-7 text-right">
                <span class="pagination-info"> 1-30 of 789 </span>
                <a class="btn btn-sm blue btn-outline btn-info">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="btn btn-sm blue btn-outline btn-info">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
     </div>
    <tbody>
        <tr class="unread" data-messageid="1">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="view-message hidden-xs"> Petronas IT </td>
            <td class="view-message "> New server for datacenter needed </td>
            <td class="view-message inbox-small-cells">
                <i class="fa fa-paperclip"></i>
            </td>
            <td class="view-message text-right"> 16:30 PM </td>
        </tr>
        <tr class="unread" data-messageid="2">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="view-message hidden-xs"> Daniel Wong </td>
            <td class="view-message"> Please help us on customization of new secure server </td>
            <td class="view-message inbox-small-cells"> </td>
            <td class="view-message text-right"> March 15 </td>
        </tr>
        <tr data-messageid="3">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star active"></i>
            </td>
            <td class="view-message hidden-xs"> John Doe </td>
            <td class="view-message"> Lorem ipsum dolor sit amet </td>
            <td class="view-message inbox-small-cells"> </td>
            <td class="view-message text-right"> March 15 </td>
        </tr>
        <tr data-messageid="4">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="view-message hidden-xs"> Facebook </td>
            <td class="view-message"> Dolor sit amet, consectetuer adipiscing </td>
            <td class="view-message inbox-small-cells"> </td>
            <td class="view-message text-right"> March 14 </td>
        </tr>
        <tr data-messageid="5">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star inbox-started"></i>
            </td>
            <td class="view-message hidden-xs"> John Doe </td>
            <td class="view-message"> Lorem ipsum dolor sit amet </td>
            <td class="view-message inbox-small-cells"> </td>
            <td class="view-message text-right"> March 15 </td>
        </tr>
        <tr data-messageid="6">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star inbox-started"></i>
            </td>
            <td class="view-message hidden-xs"> Facebook </td>
            <td class="view-message"> Dolor sit amet, consectetuer adipiscing </td>
            <td class="view-message inbox-small-cells">
                <i class="fa fa-paperclip"></i>
            </td>
            <td class="view-message text-right"> March 14 </td>
        </tr>
        <tr data-messageid="7">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star inbox-started"></i>
            </td>
            <td class="view-message hidden-xs"> John Doe </td>
            <td class="view-message"> Lorem ipsum dolor sit amet </td>
            <td class="view-message inbox-small-cells">
                <i class="fa fa-paperclip"></i>
            </td>
            <td class="view-message text-right"> March 15 </td>
        </tr>
        <tr data-messageid="8">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="view-message hidden-xs"> Facebook </td>
            <td class="view-message view-message"> Dolor sit amet, consectetuer adipiscing </td>
            <td class="view-message inbox-small-cells"> </td>
            <td class="view-message text-right"> March 14 </td>
        </tr>
        <tr data-messageid="9">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="view-message hidden-xs"> John Doe </td>
            <td class="view-message view-message"> Lorem ipsum dolor sit amet </td>
            <td class="view-message inbox-small-cells"> </td>
            <td class="view-message text-right"> March 15 </td>
        </tr>
        <tr data-messageid="10">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="view-message hidden-xs"> Facebook </td>
            <td class="view-message view-message"> Dolor sit amet, consectetuer adipiscing </td>
            <td class="view-message inbox-small-cells"> </td>
            <td class="view-message text-right"> March 14 </td>
        </tr>
        <tr data-messageid="11">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star inbox-started"></i>
            </td>
            <td class="view-message hidden-xs"> John Doe </td>
            <td class="view-message"> Lorem ipsum dolor sit amet </td>
            <td class="view-message inbox-small-cells"> </td>
            <td class="view-message text-right"> March 15 </td>
        </tr>
        <tr data-messageid="12">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star inbox-started"></i>
            </td>
            <td class="hidden-xs"> Facebook </td>
            <td class="view-message"> Dolor sit amet, consectetuer adipiscing </td>
            <td class="view-message inbox-small-cells">
                <i class="fa fa-paperclip"></i>
            </td>
            <td class="view-message text-right"> March 14 </td>
        </tr>
        <tr data-messageid="13">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="view-message hidden-xs"> John Doe </td>
            <td class="view-message"> Lorem ipsum dolor sit amet </td>
            <td class="view-message inbox-small-cells">
                <i class="fa fa-paperclip"></i>
            </td>
            <td class="view-message text-right"> March 15 </td>
        </tr>
        <tr data-messageid="14">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="hidden-xs"> Facebook </td>
            <td class="view-message view-message"> Dolor sit amet, consectetuer adipiscing </td>
            <td class="view-message inbox-small-cells"> </td>
            <td class="view-message text-right"> March 14 </td>
        </tr>
        <tr data-messageid="15">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="view-message hidden-xs"> John Doe </td>
            <td class="view-message"> Lorem ipsum dolor sit amet </td>
            <td class="view-message inbox-small-cells"> </td>
            <td class="view-message text-right"> March 15 </td>
        </tr>
        <tr data-messageid="16">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="view-message hidden-xs"> Facebook </td>
            <td class="view-message"> Dolor sit amet, consectetuer adipiscing </td>
            <td class="view-message inbox-small-cells"> </td>
            <td class="view-message text-right"> March 14 </td>
        </tr>
        <tr data-messageid="17">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star inbox-started"></i>
            </td>
            <td class="view-message hidden-xs"> John Doe </td>
            <td class="view-message"> Lorem ipsum dolor sit amet </td>
            <td class="view-message inbox-small-cells"> </td>
            <td class="view-message text-right"> March 15 </td>
        </tr>
        <tr data-messageid="18">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="view-message hidden-xs"> Facebook </td>
            <td class="view-message view-message"> Dolor sit amet, consectetuer adipiscing </td>
            <td class="view-message inbox-small-cells">
                <i class="fa fa-paperclip"></i>
            </td>
            <td class="view-message text-right"> March 14 </td>
        </tr>
        <tr data-messageid="19">
            <td class="inbox-small-cells">
                <div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div> </td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="view-message hidden-xs"> John Doe </td>
            <td class="view-message"> Lorem ipsum dolor sit amet </td>
            <td class="view-message inbox-small-cells">
                <i class="fa fa-paperclip"></i>
            </td>
            <td class="view-message text-right"> March 15 </td>
        </tr>
    </tbody>
</table></div>
                                </div>
</div></div>

