<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
checkUserSessionType(5);
?>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <?php if($_SESSION['success_result']['error_msg'] != "" && $_SESSION['success_result']['tab'] == "language"): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo $_SESSION['success_result']['error_msg']; ?>
                </div>
            <?php endif; ?>

            <?php if($_SESSION['success_result']['success_msg'] != "" && $_SESSION['success_result']['tab'] == "language"): ?>
                <div class="alert alert-success alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $_SESSION['success_result']['success_msg']; ?>
                </div>
            <?php endif; ?>
            <form method="post"  id="js-upload-form" action="<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=settings_post" >
                <input type="hidden" name="token" value="<?php echo getToken(); ?>">

                <input type="hidden" name="language-settings" >

                <div class="col-md-12 row clearfix">
                    <div class="reg-heading">Please specify the languages you can speak</div>
                    <div class="profile-top col-md-4">

                        <?php
                        if ( !empty($language_data) )
                        {
                            foreach ( $language_data as $language_row )
                            {
                                if ( !empty($doctor_category) && in_array($language_row['id'], $doctor_category) )
                                {
                                    $checked = "checked='checked'";
                                }
                                else
                                {
                                    $checked = "";
                                }
                                ?>
                                <div class="checkbox checkbox-Wellness">
                                    <input name="category[education][]" class="education"  id="checkbox<?php echo $language_row['id'];?>" type="checkbox" value="<?php echo $language_row['id'];?>" <?php echo $checked; ?> />
                                    <label for="checkbox<?php echo $language_row['id'];?>">
                                        <?php echo $language_row['categoryName']; ?>
                                    </label>
                                </div>
                                <?php
                            }
                        }
                        else
                        {
                            echo "<tr><td>No data found.</td></tr>";
                        }

                        if ( !empty($education_data) ) {
                            foreach ( $education_data as $education_row ) {
                                if ( !empty($doctor_category) && in_array($education_row['id'], $doctor_category) ) {
                                    $checked = "checked='checked'";
                                } else {
                                    $checked = "";
                                }
                                echo <<<EOD
                                                <input type="checkbox" class="education" name="category[education][]" value="{$education_row['id']}" {$checked} style="display:none;" >

EOD;
                            }
                        }
                        ?>


                    </div>

                </div>

                <div class="profile-butten notification">
                    <button type="submit" name="education"  value="Save"  class="btn btn-default dark-pink"><i class="fa fa-check"></i> Save Languages</button>
                </div>
            </form>
        </div>
    </div>
</section>
