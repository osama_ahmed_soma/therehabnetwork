<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
checkUserSessionType(5);
?>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <?php if($_SESSION['success_result']['error_msg'] != "" && $_SESSION['success_result']['tab'] == "speciality"): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo $_SESSION['success_result']['error_msg']; ?>
                </div>
            <?php endif; ?>

            <?php if($_SESSION['success_result']['success_msg'] != "" && $_SESSION['success_result']['tab'] == "speciality"): ?>
                <div class="alert alert-success alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $_SESSION['success_result']['success_msg']; ?>
                </div>
            <?php endif; ?>
            <form method="post"  id="js-upload-form" action="<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=settings_post">
                <input type="hidden" name="speciality-settings" >
                <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                <div class="col-md-12 row clearfix">
                    <div class="reg-heading">Select Your Specializations</div>
                    <?php
                    if ( !empty($speciality_data) )
                    {
                        foreach ( $speciality_data as $speciality_row )
                        {
                            if ( !empty($doctor_category) && in_array($speciality_row['id'], $doctor_category) )
                            {
                                $checked = "checked='checked'";
                            }
                            else
                            {
                                $checked = "";
                            }
                            ?>
                            <div class="profile-top col-md-4  col-sm-6 col-xs-12">
                                <div class="checkbox checkbox-Wellness">
                                    <input id="checkbox<?php echo $speciality_row['id']; ?>" type="checkbox" <?php echo $checked; ?> name="category[speciality][]" value="<?php echo $speciality_row['id']; ?>" >
                                    <label for="checkbox<?php echo $speciality_row['id']; ?>">
                                        <?php echo $speciality_row['categoryName']; ?>
                                    </label>
                                </div>
                            </div>

                            <?php
                        }

                    } else {
                        echo "<tr><td>No data found.</td></tr>";
                    }
                    ?>

                </div>
                <div class="profile-butten notification">
                    <button type="submit"  name="speciality" value="Save"  class="btn btn-default dark-pink"><i class="fa fa-check"></i> Save Specializations</button>
                </div>
            </form>
        </div>
    </div>
</section>
