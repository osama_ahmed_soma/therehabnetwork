<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
checkUserSessionType(5);
?>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <?php if($_SESSION['success_result']['error_msg'] != "" && $_SESSION['success_result']['tab'] == "workplaces"): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo $_SESSION['success_result']['error_msg']; ?>
                </div>
            <?php endif; ?>

            <?php if($_SESSION['success_result']['success_msg'] != "" && $_SESSION['success_result']['tab'] == "workplaces"): ?>
                <div class="alert alert-success alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $_SESSION['success_result']['success_msg']; ?>
                </div>
            <?php endif; ?>

            <div class="col-md-12 row" id="formsDiv">
                <form method="post" action="<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=settings_post" class="addEditWorkPlace">
                    <input type="hidden" name="token" value="<?php echo getToken(); ?>">

                    <input type="hidden" name="doctor_edit" value="Save" id="doctor_edit" class="btn btn-default dark-pink">
                    <input type="hidden" name="action" id="workAction" value="update" >
                    <div class="reg-heading">Manage Workplaces <span class="pull-right btn-sections"></span></div>
                    <div class=" custyle">
                        <table class="table table-striped custab">
                            <thead>
                            <tr>
                                <th>Workplace Name</th>
                                <th>Location</th>
                                <th>Day/Time</th>
                                <th class="text-right">Select</th>
                            </tr>
                            </thead>
                            <?php

                            $work_places_id = -1;
                            if ( isset( $doctor_data['work_places']['chamber'] ) && !empty( $doctor_data['work_places']['chamber'] ) ){
                                $work = 0;
                                $award_cnt = count( $doctor_data['work_places']['chamber'] );
                                for ( $idx = 0; $idx < $award_cnt; $idx++ ) {
                                    $work = $work + 1;
                                    $work_places_id = $work_places_id + 1;


                                    ?>
                                    <tr>
                                        <td><?php echo $doctor_data['work_places']['chamber'][$idx]; ?></td>
                                        <td><?php echo $doctor_data['work_places']['address1'][$idx]; ?></td>
                                        <td><?php
                                            if ( !empty($doctor_data['work_places']['days']) )
                                            {
                                                $work_places_size = count($doctor_data['work_places']['days']);

                                                for($idx2 = 0; $idx2 < $work_places_size; $idx2++)
                                                {
                                                    ?>
                                                    <?php echo array_key_exists($idx2,$doctor_data['work_places']['days']) ? $doctor_data['work_places']['days'][ $idx2 ] : ''; ?>,

                                                    <?php echo array_key_exists($idx2, $doctor_data['work_places']['time']) ? $doctor_data['work_places']['time'][ $idx2 ] : '';
                                                }}
                                            ?>

                                        </td>
                                        <td class="text-right"><a class='btn btn-info btn-xs' data-toggle="collapse" data-target="#editworkplace<?php echo $idx;?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>



                                            <button type="button" class="btn btn-info btn-xs dark-pink" data-toggle="modal" data-target="#delteWorkPlace<?php echo $idx;?>"><i class="fa fa-times"></i>Delete</button>
                                        </td>
                                    </tr>



                                    <!-- Modal -->
                                    <div class="modal fade" id="delteWorkPlace<?php echo $idx;?>" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Confirm Deletion</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure to delete this Workplace Record?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary btn-sm" onclick="return deleteWorkPlace('editworkplace<?php echo $idx;?>')" >Ok</button>
                                                    <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <tr>
                                        <td colspan="4" id="workPlaceDivsInLoop">
                                            <div class="collapse"  id="editworkplace<?php echo $idx;?>">
                                                <div class="form-group has-feedback">
                                                    <input value="<?php echo isset($doctor_data['work_places']['chamber'][$idx]) ?$doctor_data['work_places']['chamber'][$idx] : ''; ?>" type="text" name="data[work_places][chamber][]" data-name="chamber" data-type="single" class="form-control workplaces_single" placeholder="Workplace name" required>
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <input value="<?php echo $doctor_data['work_places']['address1'][$idx]; ?>" type="text" name="data[work_places][address1][]" data-name="address1" data-type="single" class="form-control workplaces_single" placeholder="Workplace Address" required >
                                                </div>
                                                <?php $countryWorkPlaces = 'US';

                                                if(isset($doctor_data['work_places']['country']) && $doctor_data['work_places']['country'][$idx]!='') {
                                                    $countryWorkPlaces = $doctor_data['work_places']['country'][$idx];
                                                } else {
                                                    $countryWorkPlaces = 'US';
                                                }?>
                                                <div class="form-group has-feedback">
                                                    <select class="form-control bfh-countries userCountry" name="data[work_places][country][]" id="workCountry<?php echo $idx ?>" data-country="<?php echo $countryWorkPlaces ?>" onchange="workplaceState('<?php echo $idx ?>',this,'editworkplace<?php echo $idx;?>')" ></select>
                                                </div>
                                                <?php $workState = 'AL';

                                                if(isset($doctor_data['work_places']['state'][$idx]) &&$doctor_data['work_places']['state'][$idx]!='') {
                                                    $workState = $doctor_data['work_places']['state'][$idx];
                                                } else {
                                                    $workState = 'AL';
                                                }?>
                                                <div class="form-group has-feedback workStateDiv"  <?php if($countryWorkPlaces !='US'){?>style="display:none;"<?php }?> >
                                                    <select class="form-control bfh-states"   <?php if($countryWorkPlaces =='US'){?> name="data[work_places][state][]" <?php }else {?> name="userStateBK" <?php } ?> id="workStateInDr" data-state="<?php echo $workState ?>"  data-country="workCountry<?php echo $idx ?>"></select>
                                                </div>
                                                <div class="form-group has-feedback workStateInput" <?php if($countryWorkPlaces =='US'){?>style="display:none;"<?php }?>>
                                                    <input type="text" class="form-control" id="workStateIn"  <?php if($countryWorkPlaces =='US'){?>name="userStateBK"<?php }else {?>  name="data[work_places][state][]" <?php } ?> placeholder="State" value="<?php echo $workState ?>" required>
                                                </div>
                                                <!--<div class="form-group has-feedback">

                        <select  data-country="US" data-state="<?php echo isset($work_places['state']) ? $work_places['state'] : ''; ?>" name="data[work_places][<?php echo $idx; ?>][state]" data-name="state" data-type="single" class="input-medium form-control bfh-states workplaces_single"></select>

                </div>-->
                                                <div class="form-group has-feedback">
                                                    <input value="<?php echo $doctor_data['work_places']['city'][$idx]; ?>" type="text" name="data[work_places][city][]" data-name="city" data-type="single" class="form-control workplaces_single" placeholder="Workplace City" required >
                                                </div>
                                                <!--    <div class="form-group has-feedback">
                                                           <input type="text" class="form-control" name="Workzip" id="Workzip" value="" placeholder="Workplace zip" required >
                                                    </div> -->
                                                <div class="form-group has-feedback">
                                                    <input value="<?php echo array_key_exists($idx, $doctor_data['work_places']['days']) ? $doctor_data['work_places']['days'][ $idx ] : ''; ?>" type="text" class="form-control" data-name="days" name="data[work_places][days][]" data-type="multi" id="" placeholder="Days" required >
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <input value="<?php echo array_key_exists($idx, $doctor_data['work_places']['time']) ? $doctor_data['work_places']['time'][ $idx ] : ''; ?>" type="text" class="form-control" data-name="time" name="data[work_places][time][]" data-type="multi" placeholder="Time" required >
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <button type="submit" name="doctor_edit" value="Save" id="doctor_edit" class="btn btn-default dark-pink"><i class="fa fa-plus"></i> Update</button>

                                                    <button type="button"  data-toggle="collapse" data-target="#editworkplace<?php echo $idx;?>"  class="btn btn-default dark-pink"><i class="fa fa-times"></i> Cancel</button>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                }


                            }else{ ?>
                                <tr>
                                    <td colspan="4"> <div class="alert alert-warning" role="alert">No Records found!!</div></td>

                                </tr>
                            <?php }


                            ?>
                        </table>

                        <table class="table table-striped custab">
                            <tr>
                                <td><button type="button" id="addWorkShowHide" class="btn btn-default dark-pink pull-right"><i class="fa fa-plus"></i> Add Workplace </button>	</td>
                            </tr>
                        </table>
                    </div>

                    <div id="addWDiv">

                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script>
    var addDiv = ' <div class="addWorkplaceForm"  id="addworkplace"><div class="form-group has-feedback"><input type="text" name="data[work_places][chamber][]" data-name="chamber" data-type="single" class="form-control workplaces_single" placeholder="Workplace name" required></div> <div class="form-group has-feedback"><input type="text"  required name="data[work_places][address1][]" data-name="address1" data-type="single" class="form-control workplaces_single" placeholder="Workplace Address" ></div><div class="form-group has-feedback"><select class="form-control bfh-countries userCountry" name="data[work_places][country][]" id="addworkCountry" data-country="US" onchange="addworkplaceState(<?php echo $work_places_id ?>,this)" ><option value=""></option><option value="AF">Afghanistan</option><option value="AL">Albania</option><option value="DZ">Algeria</option><option value="AS">American Samoa</option><option value="AD">Andorra</option><option value="AO">Angola</option><option value="AI">Anguilla</option><option value="AQ">Antarctica</option><option value="AG">Antigua and Barbuda</option><option value="AR">Argentina</option><option value="AM">Armenia</option><option value="AW">Aruba</option><option value="AU">Australia</option><option value="AT">Austria</option><option value="AZ">Azerbaijan</option><option value="BH">Bahrain</option><option value="BD">Bangladesh</option><option value="BB">Barbados</option><option value="BY">Belarus</option><option value="BE">Belgium</option><option value="BZ">Belize</option><option value="BJ">Benin</option><option value="BM">Bermuda</option><option value="BT">Bhutan</option><option value="BO">Bolivia</option><option value="BA">Bosnia and Herzegovina</option><option value="BW">Botswana</option><option value="BV">Bouvet Island</option><option value="BR">Brazil</option><option value="IO">British Indian Ocean Territory</option><option value="VG">British Virgin Islands</option><option value="BN">Brunei</option><option value="BG">Bulgaria</option><option value="BF">Burkina Faso</option><option value="BI">Burundi</option><option value="CI">Côte d Ivoire</option><option value="KH">Cambodia</option><option value="CM">Cameroon</option><option value="CA">Canada</option><option value="CV">Cape Verde</option><option value="KY">Cayman Islands</option><option value="CF">Central African Republic</option><option value="TD">Chad</option><option value="CL">Chile</option><option value="CN">China</option><option value="CX">Christmas Island</option><option value="CC">Cocos (Keeling) Islands</option><option value="CO">Colombia</option><option value="KM">Comoros</option><option value="CG">Congo</option><option value="CK">Cook Islands</option><option value="CR">Costa Rica</option><option value="HR">Croatia</option><option value="CU">Cuba</option><option value="CY">Cyprus</option><option value="CZ">Czech Republic</option><option value="CD">Democratic Republic of the Congo</option><option value="DK">Denmark</option><option value="DJ">Djibouti</option><option value="DM">Dominica</option><option value="DO">Dominican Republic</option><option value="TP">East Timor</option><option value="EC">Ecuador</option><option value="EG">Egypt</option><option value="SV">El Salvador</option><option value="GQ">Equatorial Guinea</option><option value="ER">Eritrea</option><option value="EE">Estonia</option><option value="ET">Ethiopia</option><option value="FO">Faeroe Islands</option><option value="FK">Falkland Islands</option><option value="FJ">Fiji</option><option value="FI">Finland</option><option value="MK">Former Yugoslav Republic of Macedonia</option><option value="FR">France</option><option value="FX">France, Metropolitan</option><option value="GF">French Guiana</option><option value="PF">French Polynesia</option><option value="TF">French Southern Territories</option><option value="GA">Gabon</option><option value="GE">Georgia</option><option value="DE">Germany</option><option value="GH">Ghana</option><option value="GI">Gibraltar</option><option value="GR">Greece</option><option value="GL">Greenland</option><option value="GD">Grenada</option><option value="GP">Guadeloupe</option><option value="GU">Guam</option><option value="GT">Guatemala</option><option value="GN">Guinea</option><option value="GW">Guinea-Bissau</option><option value="GY">Guyana</option><option value="HT">Haiti</option><option value="HM">Heard and Mc Donald Islands</option><option value="HN">Honduras</option><option value="HK">Hong Kong</option><option value="HU">Hungary</option><option value="IS">Iceland</option><option value="IN">India</option><option value="ID">Indonesia</option><option value="IR">Iran</option><option value="IQ">Iraq</option><option value="IE">Ireland</option><option value="IL">Israel</option><option value="IT">Italy</option><option value="JM">Jamaica</option><option value="JP">Japan</option><option value="JO">Jordan</option><option value="KZ">Kazakhstan</option><option value="KE">Kenya</option><option value="KI">Kiribati</option><option value="KW">Kuwait</option><option value="KG">Kyrgyzstan</option><option value="LA">Laos</option><option value="LV">Latvia</option><option value="LB">Lebanon</option><option value="LS">Lesotho</option><option value="LR">Liberia</option><option value="LY">Libya</option><option value="LI">Liechtenstein</option><option value="LT">Lithuania</option><option value="LU">Luxembourg</option><option value="MO">Macau</option><option value="MG">Madagascar</option><option value="MW">Malawi</option><option value="MY">Malaysia</option><option value="MV">Maldives</option><option value="ML">Mali</option><option value="MT">Malta</option><option value="MH">Marshall Islands</option><option value="MQ">Martinique</option><option value="MR">Mauritania</option><option value="MU">Mauritius</option><option value="YT">Mayotte</option><option value="MX">Mexico</option><option value="FM">Micronesia</option><option value="MD">Moldova</option><option value="MC">Monaco</option><option value="MN">Mongolia</option><option value="ME">Montenegro</option><option value="MS">Montserrat</option><option value="MA">Morocco</option><option value="MZ">Mozambique</option><option value="MM">Myanmar</option><option value="NA">Namibia</option><option value="NR">Nauru</option><option value="NP">Nepal</option><option value="NL">Netherlands</option><option value="AN">Netherlands Antilles</option><option value="NC">New Caledonia</option><option value="NZ">New Zealand</option><option value="NI">Nicaragua</option><option value="NE">Niger</option><option value="NG">Nigeria</option><option value="NU">Niue</option><option value="NF">Norfolk Island</option><option value="KP">North Korea</option><option value="MP">Northern Marianas</option><option value="NO">Norway</option><option value="OM">Oman</option><option value="PK">Pakistan</option><option value="PW">Palau</option><option value="PS">Palestine</option><option value="PA">Panama</option><option value="PG">Papua New Guinea</option><option value="PY">Paraguay</option><option value="PE">Peru</option><option value="PH">Philippines</option><option value="PN">Pitcairn Islands</option><option value="PL">Poland</option><option value="PT">Portugal</option><option value="PR">Puerto Rico</option><option value="QA">Qatar</option><option value="RE">Reunion</option><option value="RO">Romania</option><option value="RU">Russia</option><option value="RW">Rwanda</option><option value="ST">São Tomé and Príncipe</option><option value="SH">Saint Helena</option><option value="PM">St. Pierre and Miquelon</option><option value="KN">Saint Kitts and Nevis</option><option value="LC">Saint Lucia</option><option value="VC">Saint Vincent and the Grenadines</option><option value="WS">Samoa</option><option value="SM">San Marino</option><option value="SA">Saudi Arabia</option><option value="SN">Senegal</option><option value="RS">Serbia</option><option value="SC">Seychelles</option><option value="SL">Sierra Leone</option><option value="SG">Singapore</option><option value="SK">Slovakia</option><option value="SI">Slovenia</option><option value="SB">Solomon Islands</option><option value="SO">Somalia</option><option value="ZA">South Africa</option><option value="GS">South Georgia and the South Sandwich Islands</option><option value="KR">South Korea</option><option value="ES">Spain</option><option value="LK">Sri Lanka</option><option value="SD">Sudan</option><option value="SR">Suriname</option><option value="SJ">Svalbard and Jan Mayen Islands</option><option value="SZ">Swaziland</option><option value="SE">Sweden</option><option value="CH">Switzerland</option><option value="SY">Syria</option><option value="TW">Taiwan</option><option value="TJ">Tajikistan</option><option value="TZ">Tanzania</option><option value="TH">Thailand</option><option value="BS">The Bahamas</option><option value="GM">The Gambia</option><option value="TG">Togo</option><option value="TK">Tokelau</option><option value="TO">Tonga</option><option value="TT">Trinidad and Tobago</option><option value="TN">Tunisia</option><option value="TR">Turkey</option><option value="TM">Turkmenistan</option><option value="TC">Turks and Caicos Islands</option><option value="TV">Tuvalu</option><option value="VI">US Virgin Islands</option><option value="UG">Uganda</option><option value="UA">Ukraine</option><option value="AE">United Arab Emirates</option><option value="GB">United Kingdom</option><option value="US" selected >United States</option><option value="UM">United States Minor Outlying Islands</option><option value="UY">Uruguay</option><option value="UZ">Uzbekistan</option><option value="VU">Vanuatu</option><option value="VA">Vatican City</option><option value="VE">Venezuela</option><option value="VN">Vietnam</option><option value="WF">Wallis and Futuna Islands</option><option value="EH">Western Sahara</option><option value="YE">Yemen</option><option value="ZM">Zambia</option><option value="ZW">Zimbabwe</option></select></div><div class="form-group has-feedback addworkStateDiv" ><select class="form-control bfh-states" name="data[work_places][state][]" id="addworkStateInDr" data-state="US"  data-country="addworkCountry"><option value=""></option><option value="AL" selected>Alabama</option><option value="AK">Alaska</option><option value="AS">American Samoa</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="AF">Armed Forces Africa</option><option value="AA">Armed Forces Americas</option><option value="AC">Armed Forces Canada</option><option value="AE">Armed Forces Europe</option><option value="AM">Armed Forces Middle East</option><option value="AP">Armed Forces Pacific</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">District of Columbia</option><option value="FM">Federated States Of Micronesia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="GU">Guam</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MH">Marshall Islands</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="MP">Northern Mariana Islands</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PW">Palau</option><option value="PA">Pennsylvania</option><option value="PR">Puerto Rico</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VI">Virgin Islands</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option></select></div> <div class="form-group has-feedback addworkStateInput" style="display:none;"> <input type="text" class="form-control" id="addworkStateIn"  name="userStateBK" placeholder="State" value=""></div><div class="form-group has-feedback"><input  required type="text" name="data[work_places][city][]" data-name="city" data-type="single" class="form-control workplaces_single" placeholder="Workplace City" ></div></div><div class="form-group has-feedback"><input required type="text" class="form-control" data-name="days" name="data[work_places][days][]" data-type="multi" id="" placeholder="Days" ></div><div class="form-group has-feedback"><input  required type="text" class="form-control" data-name="time" name="data[work_places][time][]" data-type="multi" placeholder="Time" ></div><div class="form-group has-feedback"><button type="submit" name="doctor_edit" value="Save" class="btn btn-default dark-pink"><i class="fa fa-plus"></i> Add</button>&nbsp;<button type="button" id="hideAddForm" onclick="return removeadddiv()" class="btn btn-default dark-pink"><i class="fa fa-times"></i> Cancel</button></div></div>';

    $("#addWorkShowHide").click(function () {

        $("#addWDiv").empty();
        $("#addWDiv").append(addDiv);
        $("#workAction").val('add');
        //console.log($("#workAction").val());
    });
    function removeadddiv(){
        $("#workAction").val('update');
        $("#addWDiv").empty();

    }

    function deleteWorkPlace(divId)
    {

        // if ( !confirm('Are you sure you want to delete this record ?') ) {
        // return false;
        // }

        $("#workAction").val('delete');
        $("#"+divId).html('');
        //console.log('gjgh');

        $(".addEditWorkPlace").submit();


    }
    function workplaceState(id,elem,mainDiv){
        if ($(elem).val() != 'US')
        {
            $('#'+mainDiv).find('.workStateDiv').css( "display", "none" );
            $('#'+mainDiv).find('#workStateInDr').attr('name', 'userStateBK');
            $('#'+mainDiv).find('#workStateIn').attr('name', 'data[work_places][state][]');
            $('#'+mainDiv).find('.workStateInput').css( "display", "block" );
            $('#'+mainDiv).find('#workStateIn').val( "" );

        }
        else
        {
            $('#'+mainDiv).find('.workStateInput').css( "display", "none" );
            $('#'+mainDiv).find('#workStateInDr').attr('name', 'data[work_places][state][]');
            $('#'+mainDiv).find('#workStateIn').attr('name', 'userStateBK');
            $('#'+mainDiv).find('.workStateDiv').css( "display", "block" );
        }
    }
    function addworkplaceState(id,elem){
        if ($(elem).val() != 'US')
        {
            $('.addworkStateDiv').css( "display", "none" );
            $('#addworkStateInDr').attr('name', 'userStateBK');
            $('#addworkStateIn').attr('name', 'data[work_places][state][]');
            $('.addworkStateInput').css( "display", "block" );
            $('#addworkStateIn').val( "" );

        }
        else
        {
            $('.addworkStateInput').css( "display", "none" );
            $('#addworkStateInDr').attr('name', 'data[work_places][state][]');
            $('#addworkStateIn').attr('name', 'userStateBK');
            $('.addworkStateDiv').css( "display", "block" );
        }
    }
</script>