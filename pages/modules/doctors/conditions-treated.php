<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
checkUserSessionType(5);
?>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <?php if($_SESSION['success_result']['error_msg'] != "" && $_SESSION['success_result']['tab'] == "condition"): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo $_SESSION['success_result']['error_msg']; ?>
                </div>
            <?php endif; ?>

            <?php if($_SESSION['success_result']['success_msg'] != "" && $_SESSION['success_result']['tab'] == "condition"): ?>
                <div class="alert alert-success alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $_SESSION['success_result']['success_msg']; ?>
                </div>
            <?php endif; ?>

            <form method="post"  id="js-upload-form" action="<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=settings_post" class="addconditionForm">
                <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                <input type="hidden" name="condition_edit">
                <input type="hidden" name="action" value="update" id="conditionAction">
                <div class="col-md-12 row custyle clearfix">
                    <div class="reg-heading">Conditions Treated</div>

                    <table class="table table-striped custab">
                        <thead>
                        <tr>
                            <th>Condition</th>
                            <th>Treatment Details </th>
                            <th class="text-right">Select</th>
                        </tr>
                        </thead>
                        <?php
                        $idx = -1;
                        if ( isset( $doctor_data['conditions_treated']['condition_name'] ) && !empty( $doctor_data['conditions_treated']['condition_name'] ) )
                        {
                            $procedure_attempted_cnt = count( $doctor_data['conditions_treated']['condition_name'] )  ;

                            for ( $idx = 0; $idx < $procedure_attempted_cnt; $idx++ )
                            {
                                ?><tr>
                                <td><?php echo isset($doctor_data['conditions_treated']['condition_name'][ $idx ]) ? $doctor_data['conditions_treated']['condition_name'][ $idx ] : '';  ?></td>
                                <td><?php echo isset($doctor_data['conditions_treated']['condition_description'][ $idx ]) ? $doctor_data['conditions_treated']['condition_description'][ $idx ] : '';  ?></td>
                                <td class="text-right"><a class='btn btn-info btn-xs' data-toggle="collapse" data-target="#editcondition<?php echo $idx;?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                    <button type="button" class="btn btn-info btn-xs dark-pink" data-toggle="modal" data-target="#deleteconditionModal<?php echo $idx;?>"><i class="fa fa-times"></i>Delete</button>
                                </td>
                                </tr>
                                <!-- Modal -->
                                <div class="modal fade" id="deleteconditionModal<?php echo $idx;?>" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Confirm Deletion</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure to delete this Condition Treated Record?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary btn-sm"  onclick='deleteCondition("editcondition<?php echo $idx;?>")'  >Ok</button>
                                                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <tr><td colspan="4" id="workPlaceDivsInLoop">
                                        <div class="collapse"  id="editcondition<?php echo $idx;?>">
                                            <div class="form-group col-md-12 row">
                                                <input type="text" class="form-control col-md-12" id="data[conditions_treated][condition_name][]" name="data[conditions_treated][condition_name][]" value="<?php echo $doctor_data['conditions_treated']['condition_name'][ $idx ]; ?>" placeholder="Condition Name">
                                            </div>
                                            <div class="form-group col-md-12 row">
                                                <textarea type="text" class="form-control col-md-12" id="data[conditions_treated][condition_description][]" name="data[conditions_treated][condition_description][]" value="" placeholder="Treatment Detail"><?php echo $doctor_data['conditions_treated']['condition_description'][ $idx ]; ?></textarea>
                                            </div>
                                            <div class="form-group has-feedback">
                                                <button type="submit" name="condition_edit" class="btn btn-default dark-pink"><i class="fa fa-plus"></i> Update</button>
                                                <button type="button"  data-toggle="collapse" data-target="#editcondition<?php echo $idx;?>"  class="btn btn-default dark-pink"><i class="fa fa-times"></i> Cancel</button>
                                            </div>
                                        </div>
                                    </td></tr>
                                <?php
                            }
                        }else{ ?>
                            <tr>
                                <td colspan="4"> <div class="alert alert-warning" role="alert">No Records found!!</div></td>

                            </tr>
                        <?php }
                        ?>

                    </table>

                    <table class="table table-striped custab">
                        <tr>
                            <td><button type="button" class="btn btn-default dark-pink pull-right" id="addConditionShowHide" ><i class="fa fa-plus"></i> Add Condition </button>	</td>
                        </tr>
                    </table>

                </div>
                <div id="addCondDiv"></div>
            </form>
        </div>
    </div>
</section>
<script>
    var addDivCond = '<div class="condAdddiv"  id="condAdddiv"><div class="form-group col-md-12 row"><input type="text" class="form-control col-md-12" id="data[conditions_treated][condition_name][]" name="data[conditions_treated][condition_name][]" value="" placeholder="Condition Name" required></div><div class="form-group col-md-12 row"><textarea type="text" class="form-control col-md-12" id="data[conditions_treated][condition_description][]" name="data[conditions_treated][condition_description][]" value="" placeholder="Treatment Detail" required ></textarea> </div> <div class="form-group has-feedback"> <button type="submit" class="btn btn-default dark-pink" name="condition_edit" ><i class="fa fa-plus"></i> Add</button> <button type="button" class="btn btn-default dark-pink" onclick="return removeaddCondDiv()"  ><i class="fa fa-times"></i> Cancel</button></div></div>';

    $("#addConditionShowHide").click(function () {

        $("#addCondDiv").empty();
        $("#addCondDiv").append(addDivCond);
        $("#conditionAction").val('add');
    });

    function removeaddCondDiv(){

        $("#addCondDiv").empty();
        $("#conditionAction").val('update');
    }

    function deleteCondition(divId1)
    {
        /*  if ( !confirm('Are you sure you want to delete this record ?') ) {
         return false;
         } */
        $("#conditionAction").val('delete');
        $("#"+divId1).html('');
        $(".addconditionForm").submit();


    }
</script>