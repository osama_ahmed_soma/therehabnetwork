<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

$aid = isset($_GET['id']) ? intval($_GET['id']) : 0;

if ( $aid > 0 ) {

    //get post details
    try {
        $select = "SELECT tips.id, tips.url, tips.view, tips.title, tips.excerpt, tips.content, tips.image, doc.doctor_name";
        $table = " FROM `" . DB_PREFIX . "health_tips` as tips, `" . DB_PREFIX . "doctors` as doc";
        $where = " WHERE doc.id = tips.doctorId AND tips.status = 'publish' AND tips.id=? ";
        $lim = " LIMIT 1";

        $sql = $select . $table. $where  . $lim;
        $st = $db->prepare( $sql );
        $st->execute( array($aid) );

        if ( $st->rowCount() > 0 ) {
            $data = $st->fetch();
            $data['image'] = unserialize(base64_decode($data['image']));

            //update view count
            if ( $data['view'] != '' && $data['view'] > 0 ) {
                $view = $data['view'] + 1;
            } else {
                $view = 1;
            }
            if ( $view ) {
                $sql = 'UPDATE ' . DB_PREFIX . 'health_tips SET view = ? WHERE id = ?';
                $st = $db->prepare($sql);
                $st->execute(array($view, $aid));
            }

        } else {
            $data = array();
        }

    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }
}

//check for favorite doctor
/*
 * 0 - user not logged in
 * 1 - user is logged in but not added to favorite
 * 2 - user added this doctor to favorite
 */
$is_doctor_favorite = 0;
if ( isset($_SESSION["mvdoctorVisitornUserId"])  ) {
    $sql = 'SELECT userId  FROM `'. DB_PREFIX .'pinned_post` WHERE `userId` = ? AND postId = ? LIMIT 1';

    try {
        $st = $db->prepare( $sql );
        $st->execute( array( $_SESSION["mvdoctorVisitornUserId"], $aid ) );

        if ( $st->rowCount() > 0 ) {
            $is_doctor_favorite = 2;
        } else {
            $is_doctor_favorite = 1;
        }

    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }
}

//get all comments for this post
if ( $aid > 0 ) {
    try {
        $sql = "SELECT * FROM `" . DB_PREFIX . "comments` WHERE postId = ? ORDER BY `id` asc";
        $st = $db->prepare($sql);
        $st->execute(array($aid));

        $total_comments = $st->rowCount();
        $comments_data = $total_comments > 0 ? $st->fetchAll() : array();
    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }
}

if ( $aid > 0 && is_array($data) && !empty($data) ) {  ?>


<link rel="stylesheet" href="<?php echo HTTP_SERVER ?>/dist/css/social-share-kit.css">
<!-- Vertical sticky share icons-->
    <div class="ssk-sticky  ssk-left ssk-center  ssk-count ssk-lg" data-url="<?php echo HTTP_SERVER . 'blog-for-doctors/' . $data['url'] . '.html'; ?>" data-text="<?php echo sanitize($data['excerpt'], 3); ?>">
        <a href="" class="ssk ssk-facebook"></a>
        <a href="" class="ssk ssk-twitter"></a>
        <a href="" class="ssk ssk-google-plus"></a>
        <a href="" class="ssk ssk-tumblr"></a>
        <a href="" class="ssk ssk-email"></a>
    </div>
    

    <div class="health-detail health-detail-innerpage clearfix">
        <div class="col-sm-12 col-md-12 col-xs-12 row ">
        		<div class="col-md-12 pull-left health-detail">
                 <h1><?php echo $data['title']; ?></h1>
               </div>
            <div class="col-md-12  pull-left health-detail">

                <div class="col-md-4 col-xs-12 pin-buttons text-right pull-right row">
                    <a href="#" class="btn btn-default dark-pink <?php if ($is_doctor_favorite == 2) { echo "removeFavorite"; } else { echo "addToFavorite"; } ?>" data-login="<?php if ($is_doctor_favorite=='0') { echo 'no'; } else { echo 'yes'; } ?>" data-id="<?php echo $aid; ?>" title="Pin now, read later"><i class="fa fa-thumb-tack"></i> <?php if ($is_doctor_favorite == 2) { echo " Unpin Article"; } else { echo " Pin Article"; } ?></a>
                    <a href="<?php echo HTTP_SERVER; ?>blog.html" class="btn btn-default dark-pink"><i class="fa fa-book"></i> Back to Articles</a>
                </div>

                 <div class="col-md-6  col-xs-12 text-left pull-left row">
                     <div class="share-name">Share This Article:
                         <span class='st__hcount' displayText=''></span>
                         <span class='st_facebook_hcount' displayText='Facebook' st_summary="<?php echo sanitize($data['excerpt'],3); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $data['image']['img1']; ?>" st_title="<?php echo sanitize($data['title'],3); ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $data['url'] . '.html'; ?>'></span>
                         <span class='st_twitter_hcount' displayText='Tweet' st_summary="<?php echo sanitize($data['excerpt'],3); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $data['image']['img1']; ?>" st_title="<?php echo sanitize($data['title'],3); ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $data['url'] . '.html'; ?>'></span>
                         <span class='st_email_hcount' displayText='Email' st_summary="<?php echo sanitize($data['excerpt'],3); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $data['image']['img1']; ?>" st_title="<?php echo sanitize($data['title'],3); ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $data['url'] . '.html'; ?>'></span>
                     </div>
                </div>
            </div>
           
        </div>
        <div class="col-md-12">
            <p style="text-align: center; display:none;"><strong>By <?php echo sanitize($data['doctor_name'], 3) ?></strong></p>
            <?php echo html_entity_decode($data['content']); ?>
        </div>
    </div>

    <div class="col-md-12 pull-left health-detail">
        <div class="col-md-4 pin-button col-xs-12 text-right pull-right ">
            <a href="<?php echo HTTP_SERVER; ?>blog.html" class="btn btn-default dark-pink"><i class="fa fa-book"></i> Back to Articles</a>
        </div>
         <div class="col-md-6 text-left pull-left row">
             <div class="share-name">Share This Article:
                 <span class='st__hcount' displayText=''></span>
                 <span class='st_facebook_hcount' displayText='Facebook' st_summary="<?php echo sanitize($data['excerpt'], 3); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $data['image']['img1']; ?>" st_title="<?php echo sanitize($data['title'], 3); ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $data['url'] . '.html'; ?>'></span>
                 <span class='st_twitter_hcount' displayText='Tweet' st_summary="<?php echo sanitize($data['excerpt'], 3); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $data['image']['img1']; ?>" st_title="<?php echo sanitize($data['title'], 3); ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $data['url'] . '.html'; ?>'></span>
                 <span class='st_email_hcount' displayText='Email' st_summary="<?php echo sanitize($data['excerpt'], 3); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $data['image']['img1']; ?>" st_title="<?php echo sanitize($data['title'], 3); ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $data['url'] . '.html'; ?>'></span>
             </div>
        </div>
    </div>
<?php
} else {
    echo '<h2 class="no-results">Invalid Article Link.</h2>';
}?>
<!-- comments sections -->
<div class="col-sm-12">
    <div class="panel panel-white post panel-shadow">
    	<h2>Comments <span><a href="#"><?php echo $total_comments; echo $total_comments > 1 ? ' comments' : ' comment'; ?> <i class="fa fa-angle-down"></i></a></span></h2>
        <div id="comments_container">
        <?php
        //loop through comments
        if ( count($comments_data) > 0 ) {

            $user_info_array = array();

            foreach ( $comments_data as $comment ) {
                //get user information first
                if ( !array_key_exists($comment['userId'], $user_info_array) ) {
                    $sql = 'SELECT userFname,userLname,userEmail,userPicture FROM `' . DB_PREFIX . 'users` WHERE userId = ?';
                    $st = $db->prepare($sql);
                    $st->execute(array($comment['userId']));
                    if ( $st->rowCount() ) {
                        $new_user_data = $st->fetch();
                        if ( $new_user_data['userPicture'] != '' ) {
                            $new_user_data['userPicture'] = unserialize(base64_decode($new_user_data['userPicture']));
                        }
                        if ( isset( $new_user_data['userPicture']['img2'] ) ) {
                            $new_user_data['user_image'] = HTTP_SERVER . 'images/user/thumb/' . $new_user_data['userPicture']['img2'];
                        } else {
                            $new_user_data['user_image'] = get_gravatar($new_user_data["userEmail"],100);
                        }
                        $user_info_array[ $comment['userId'] ] = $new_user_data;
                    }
                }
                //show comment here
                ?>
                <div class="post-heading">
                    <div class="pull-left image">
                        <img src="<?php
                        if ( isset($user_info_array[ $comment['userId'] ]["user_image"]) ) {
                            echo $user_info_array[ $comment['userId'] ]["user_image"];
                        } else {
                            echo 'http://bootdey.com/img/Content/user_1.jpg';
                        } ?>" class="img-circle avatar" alt="user profile image">
                    </div>
                    <div class="pull-left meta">
                        <div class="title h5">
                            <b><?php echo sanitize($user_info_array[ $comment['userId'] ]["userFname"], 3) . ' ' . sanitize($user_info_array[ $comment['userId'] ]["userLname"], 3); ?></b> <h6 class="time"><?php echo timeAgo($comment['added']); ?></h6>

                        </div>
                        <p class="des"><?php echo sanitize($comment['comment'], 3); ?></p>
                    </div>
                </div>
                <?php
            }
        } else {
            //do not show anything
        }
        ?>
        </div>

        <div class="post-footer">
            <div class="input-group">
                <input class="form-control col-md-8" id="blog_comments" name="blog_comments" placeholder="Write a comment" type="text">
                <div class="input-group-addon" id="comment_submit" data-user="<?php if( !isset($_SESSION["mvdoctorVisitornUserId"]) ) { echo 'no'; } else { echo 'yes'; } ?>" data-post="<?php echo $aid; ?>"><i class="fa fa-paper-plane"></i></div>
            </div>
        </div>
    </div>
</div>
<!-- comment section ends here -->




</div>
</div>
</div>
 
<section class="grey clearfix" id="footer" style="background:url(http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/img/footer.png);">
    <div class="container">
    
		<div class="col-md-6">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class=" Virtual-heading">Are you ready to market your practice more effectively? </h1>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12 text-center">
                        <h2 class="simple pra">Join My Virtual Doctor today.</h2>
                        <br /><a href="#" class="btn  btn-primary btn-footer">REQUEST DEMO</a>
                    </div>
                </div>
            </div>
		</div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class=" Virtual-heading">Need to see a doctor? Get better, more affordable healthcare in minutes.</h1>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12 text-center">
                        <h2 class="simple pra">Sign up for a free account, find a physician you love, and book an online consultation now. </h2><br />
                        <a href="#" class="btn  btn-primary btn-footer">SIGN UP</a>
                    </div>

                </div>
            </div>
		</div>
	</div>
</section>

<!-- Modal 2-->
<div class="modal fade" id="myModalFav" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;" id="favoriteModal"></div>
        </div>
    </div>
</div>

<!-- Social Share Kit JS -->
<script src="<?php echo HTTP_SERVER ?>/dist/js/social-share-kit.js"></script>


<script>
   // Init Social Share Kit
    SocialShareKit.init({
        'url': '<?php echo HTTP_SERVER; ?>',
        'twitter': {
			'url': '<?php echo HTTP_SERVER . 'blog/' . $data['url'] . '.html'; ?>',
            'title': '<?php echo sanitize($data['title'], 3); ?>',
       		 'text': '<?php echo sanitize($data['excerpt'], 3); ?>',
            'via': 'myvirtualdoctor'
        }
    });

   jQuery(function($) {

        // Just to disable href for other example icons
        $('.ssk').on('click', function (e) {
            e.preventDefault();
        });
        // Navigation collapse on click
        $('.navbar-collapse ul li a:not(.dropdown-toggle)').bind('click', function () {
            $('.navbar-toggle:visible').click();
        });

        // Email protection
        $('.author-email').each(function () {
            var a = '@', em = 'support' + a + 'social' + 'sharekit' + '.com', t = $(this);
            t.attr('href', 'mai' + 'lt' + 'o' + ':' + em);
            !t.text() && t.text(em);
        });

        // Sticky icons changer
        $('.sticky-changer').click(function (e) {
            e.preventDefault();
            var $link = $(this);
            $('.ssk-sticky').removeClass($link.parent().children().map(function () {
                return $(this).data('cls');
            }).get().join(' ')).addClass($link.data('cls'));
            $link.parent().find('.active').removeClass('active');
            $link.addClass('active').blur();
        });

       $('#comment_submit').click(function(){

           if ( $(this).data('user') == 'yes' ) {
               //check blog_comments
               var comment = $.trim( $('#blog_comments').val() );
               var post_id = $(this).data('post');
               if (comment == '') {
                   $('#favoriteModal').html('<div class="alert alert-danger" role="alert">Please write a comment first.</div>');
                   $('#myModalFav').modal();
               } else {
                   //ajax call
                   var data = {
                       patient_blog: '1',
                       blog_comments: comment,
                       post_id: post_id,
                       token: '<?php echo getToken(); ?>'
                   };
                   $.ajax({
                       url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=blog_comment",
                       data: data,
                       method: "POST",
                       dataType: "json"
                   }).success(function (data) {
                       if ( data.success ) {
                           //append new comment
                           $('#blog_comments').val('');
                           $('#comments_container').append(data.comment_html);
                       }
                       if (data.error) {
                           $('#favoriteModal').html('<div class="alert alert-danger" role="alert">' + data.error +'</div>');
                           $('#myModalFav').modal();
                       }
                   });
               }
           } else {
               //show modal
               $('#favoriteModal').html('<div class="alert alert-danger" role="alert">Please login to add comment.</div>');
               $('#myModalFav').modal();
           }
       });
    });


</script>