<?php

if (!defined('MVD_SITE')) {

    die('You are not authorized to view this page');

}

require_once $siteRootHome_new . 'includes/classes/bootstrap_pagination.php';





$arr = array();

$select = "SELECT tips.id, tips.url, tips.title, tips.image, tips.excerpt, doc.doctor_name, doc.specialization, doc.degree";

$table = " FROM `" . DB_PREFIX . "health_tips` as tips, `" . DB_PREFIX . "doctors` as doc";

$where = " WHERE doc.id = tips.doctorId AND tips.status = 'publish'";







//get pagination terms

$limit = 10; //how many items to show per page

if (isset($_GET['page_now'])) {

    $page = intval($_GET['page_now']);

} else {

    $page = 0;

}



if ($page) {

    $start = ($page - 1) * $limit; //first item to display on this page

} else {

    $start = 0;

}

$lim = " LIMIT $start, $limit";





//get search terms

if ( isset($_GET['s']) && $_GET['s'] != '' ) {

    $where .= " AND (tips.title LIKE ? OR tips.excerpt LIKE ? OR tips.content LIKE ?)";

    $arr[] = '%'.$_GET['s'].'%';

    $arr[] = '%'.$_GET['s'].'%';

    $arr[] = '%'.$_GET['s'].'%';

}



//get order by params

$order = " ORDER BY id desc";



//get latest 10 post

try {





    $sql = $select . $table . $where . $order . $lim;



    $st = $db->prepare($sql);



    if ( !empty($arr) ) {

        $st->execute($arr);

    } else {

        $st->execute();

    }



    if ($st->rowCount() > 0) {

        $data = $st->fetchAll();

    } else {

        $data = array();

    }



} catch (Exception $Exception) {

    exit("DataBase Error {$Exception->getCode()}:" . $Exception->getMessage());

}





//pagination task

try {

    $select = "SELECT COUNT(tips.id) as cnt";

    $sql = $select . $table . $where;



    $st = $db->prepare($sql);

    if ( !empty($arr) ) {

        $st->execute($arr);

    } else {

        $st->execute();

    }



    if ($st->rowCount() > 0) {

        $row = $st->fetch();



        //create a pagin class

        $pagin = new Bootpagin();



        $pagin->total_records = $row['cnt'];

        $pagin->noperpage = $limit;

        //this would give example.php?page=1 url type

        $pagin->url = HTTP_SERVER . 'blog/page/';

        $pagin->val = 'page_now';

        //pagin size for small column or large column in bootstrap (sm or lg )

        //for default pagination size leave value empty like this $pagin->size=''

        //$pagin->size = 'sm';

    }

} catch (Exception $Exception) {

    exit("DataBase Error {$Exception->getCode()}:" . $Exception->getMessage());

}



try {

    $sql = "SELECT id, title, image, url FROM " . DB_PREFIX . "health_tips WHERE status = 'publish' ORDER BY `view` desc LIMIT 10";

    $st = $db->prepare($sql);

    $st->execute();



    if ( $st->rowCount() > 0 ) {

        $most_viewed_data = $st->fetchAll();

    } else {

        $most_viewed_data = array();

    }

} catch (Exception $Exception) {

    exit("DataBase Error {$Exception->getCode()}:" . $Exception->getMessage());

}



try {

    $sql = "SELECT id, title, image, url FROM " . DB_PREFIX . "health_tips WHERE status = 'publish' AND featured = 1 LIMIT 10";

    $st = $db->prepare($sql);

    $st->execute();



    if ( $st->rowCount() > 0 ) {

        $featured_data = $st->fetchAll();

    } else {

        $featured_data = array();

    }

} catch (Exception $Exception) {

    exit("DataBase Error {$Exception->getCode()}:" . $Exception->getMessage());

}

//echoPre($_SERVER);

?>



<div class="col-sm-4 col-md-3 sidebar blog-sidebar">

    <div class="list-group sidebar serach-list">



        <span href="#" class="list-group-item active">

            Search Resources 

        </span>



        <div id="speciality-main" class="collapse in">

            <div class="list-group-item">

                <div class="search-from-side">

                   <button type="submit" class="search-from-side-btn"> Go</button>

                    <form method="get" action="<?php echo HTTP_SERVER . 'blog.html'; ?>">

                        <input type="text" class="form-control search-from-side-input" name="s" id="s"

                           placeholder="Search Resources" required="required" value="<?php echo isset($_GET['s']) ? sanitize($_GET['s']) : ''; ?>">



                        

                    </form>

                </div>

            </div>

        </div>

    </div>



    <?php if ( !empty($featured_data) ):    ?>

        <div class="list-group sidebar serach-list">

            <span href="#" class="list-group-item active">

                Featured Articles   </span>



             <div id="speciality-main" class="collapse in">

                <?php foreach ( $featured_data as $featured ) {

                    $featured['image'] = unserialize(base64_decode($featured['image']));

                ?>

                    <a href="<?php echo HTTP_SERVER . 'blog/' . $featured['url'] . '.html'; ?>" class="list-group-item">

                        <div class="resent-post clearfix">

                            <div class="col-md-4 col-xs-4 row resent-post-img">

                                <img src="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $featured['image']['img1']; ?>" class="img-responsive">

                            </div>

                            <div class="col-md-8 col-xs-8 resent-post-text">

                                <?php echo sanitize($featured['title'], 3); ?>

                            </div>

                            <div class="col-md-12 text-right resent-post-more">Read More</div>

                        </div>

                    </a>

                <?php } ?>

             </div>

        </div>

    <?php endif; ?>



    <?php if ( !empty($most_viewed_data) ):  ?>

        <div class="list-group sidebar serach-list">

            <span href="#" class="list-group-item active">

                Most Viewed </span>

             <div id="most-viewed" class="collapse in">

                <?php foreach ( $most_viewed_data as $most_view ) {

                    $most_view['image'] = unserialize(base64_decode($most_view['image']));

                ?>

                    <a href="<?php echo HTTP_SERVER . 'blog/' . $most_view['url'] . '.html'; ?>" class="list-group-item">

                        <div class="resent-post clearfix">

                            <div class="col-md-4 col-xs-4 row resent-post-img">

                                <img src="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $most_view['image']['img1']; ?>" class="img-responsive">

                            </div>

                            <div class="col-md-8 col-xs-8 resent-post-text">

                                <?php echo sanitize($most_view['title'], 3); ?>

                            </div>

                            <div class="col-md-12 text-right resent-post-more">Read More</div>

                        </div>

                    </a>



                <?php } ?>

             </div>

        </div>

    <?php endif; ?>

</div>



<div class="col-sm-8 col-md-8 col-xs-12">

    

    <div class="recent-post-dropdown clearfix">	

        <div class="dropdown pull-right">		 

            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Featured

            <span class="caret"></span></button>

            <ul class="dropdown-menu">

                <li><a href="#">Date Descending</a></li>

                <li><a href="#">Most Viewed</a></li>

                <li><a href="#">Date Ascending</a></li>

                <li><a href="#">Featured</a></li>

            </ul>

        </div>

	   <div class="Sort-dropdown pull-right"> Sort By</div>

    </div>

    <?php

    if (!empty($data)):

        foreach ($data as $item) {

            $item['image'] = unserialize(base64_decode($item['image']));



            $is_doctor_favorite = 0;



            if ( isset($_SESSION["mvdoctorVisitornUserId"])  ) {

                $sql = 'SELECT userId  FROM `'. DB_PREFIX .'pinned_post` WHERE `userId` = ? AND postId = ? LIMIT 1';



                try {

                    $st = $db->prepare( $sql );

                    $st->execute( array( $_SESSION["mvdoctorVisitornUserId"], $item['id'] ) );



                    if ( $st->rowCount() > 0 ) {

                        $is_doctor_favorite = 2;

                    } else {

                        $is_doctor_favorite = 1;

                    }



                } catch (Exception $Exception) {

                    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );

                }

            }

            $read_more_link = ' <a href="' . HTTP_SERVER . 'blog/' . $item['url'] . '.html'  .'">... Read More</a>';

            $title = sanitize($item['title'], 3);

            $excerpt = trim_words(sanitize($item['excerpt'], 3), 55, $read_more_link);

            $excerpt2 = htmlentities(strip_tags(sanitize($item['excerpt'], 3)));

            ?>

            <div class="health-detail clearfix">

                <div class="col-sm-4 col-xs-6 col-md-2 col-xs-12 health-detail-image">

                    <img src="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" title="<?php echo $title; ?>"/>

                </div>

                <div class="col-sm-8 col-md-10 col-xs-12 ">

                    <div class="health-detail-txt">

                        <a href="<?php echo HTTP_SERVER . 'blog/' . $item['url'] . '.html'; ?>">

                            <?php echo $title; ?>

                        </a>

                    </div>

                    <div class="health-detail-txt txt-green">

                        <?php echo $excerpt; ?>

                    </div>

                    <ul class="medial-info article-media-info">

                        <li> by <strong><?php echo htmlentities( $item['doctor_name'] ); ?></strong></li>

                        <li><i class=" fa fa-stethoscope"></i> <?php echo sanitize($item['specialization'], 3) ?></li>

                        <li><i class=" fa fa-graduation-cap"></i> <?php echo sanitize($item['degree'], 3) ?></li>

                    </ul>

                    <div class="col-md-12  row">

                        <a href="<?php echo HTTP_SERVER . 'blog/' . $item['url'] . '.html'; ?>"

                           class="btn btn-default dark-pink pull-left pinbtn"><i class="fa fa-book"></i> Read Article</a>

                        <a href="#" class="btn btn-default pull-left dark-pink <?php if ($is_doctor_favorite == 2) { echo "removeFavorite"; } else { echo "addToFavorite"; } ?>" data-login="<?php if ($is_doctor_favorite=='0') { echo 'no'; } else { echo 'yes'; } ?>" data-id="<?php echo $item['id']; ?>" title="Pin now, read later"><i class="fa fa-thumb-tack"></i> <?php if ($is_doctor_favorite == 2) { echo " Unpin Article"; } else { echo " Pin Article"; } ?></a>



                        <div class="col-md-7 text-right share-min-class health-social pull-left ">

                            <span class='st__hcount' displayText=''></span>

                            <span class='st_facebook_hcount' displayText='Facebook'

                                  st_summary="<?php echo $excerpt2; ?>"

                                  st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>"

                                  st_title="<?php echo $title; ?>"

                                  st_url='<?php echo HTTP_SERVER . 'blog/' . $item['url'] . '.html'; ?>'></span>

                            <span class='st_twitter_hcount' displayText='Tweet'

                                  st_summary="<?php echo $excerpt2; ?>"

                                  st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>"

                                  st_title="<?php echo $title; ?>"

                                  st_url='<?php echo HTTP_SERVER . 'blog/' . $item['url'] . '.html'; ?>'></span>

                            <span class='st_email_hcount' displayText='Email'

                                  st_summary="<?php echo $excerpt2; ?>"

                                  st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>"

                                  st_title="<?php echo $title; ?>"

                                  st_url='<?php echo HTTP_SERVER . 'blog/' . $item['url'] . '.html'; ?>'></span>

                        </div>

                    </div>

                </div>

            </div>

            <?php

        }

        //call the pagin function

        echo "<div style='text-align: center'>";

        $pagin->pagin();

        echo "</div>";

    else:?>

        <h2 class="no-results">No article found.</h2>

    <?php endif;?>



</div>

<script type="text/javascript">

    jQuery(function($){

        $(".dropdown-menu li a").click(function(){

            $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');

            $(this).parents(".dropdown").find('.btn').val($(this).data('value'));

        });

    });

</script>