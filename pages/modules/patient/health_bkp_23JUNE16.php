<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
checkUserSessionType(4);
?>
<section class="dashboard-new-section-main patient-dashbaord-section-main clearfix tab-pane fade in active" role="tabpanel" id="health">
    <div class="container">
        <div class="row admin-pro clearfix">
            <div class="col-sm-2 col-md-2 text-center">

                <div  id="upload_photo_div" class="user-display-picture">
                    <img alt="1" class="avatar" src="<?php
                    if ( isset($_SESSION["mvdoctorVisitoruserPicture"]) && $_SESSION["mvdoctorVisitoruserPicture"] != '' ) {
                        echo HTTP_SERVER . 'images/user/thumb/' . $_SESSION["mvdoctorVisitoruserPicture"];
                    } else {
                        echo get_gravatar($_SESSION["mvdoctorVisitorUserEmail"],87);
                    } ?>">

                </div>
                <div class="change-photo">

                    <a href="<?php echo HTTP_SERVER . 'patient/profile-settings.html' ?>" class="btn btn-info">Edit Profile</a>
                </div>

            </div>

            <div class="user-info col-sm-10 col-md-10">
                <div class="last-updated-info pull-right">
                    <span id="last_updated_at">Last Updated On 05/28/2016</span>
                </div>
                <div>
                    <span class="title"><?php echo $_SESSION["mvdoctorVisitorUserFname"] . ' ' . $_SESSION["mvdoctorVisitorUserLname"]; ?></span>
                </div>
                <div>
                    <span id="profile_percentage">Your profile is <strong>92%</strong> complete</span>
                </div>
                <div class="progress">
                    <div id="profile_progress_bar" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 92%;">
                        <span class="sr-only"></span>
                    </div>
                </div>
                <div>
                    <span class="notification" id="pc_condition">Please complete <strong><a data-remote="true" href="<?php echo HTTP_SERVER; ?>patient/health.html">My Health</a></strong> questions</span>
                </div>
            </div>


        </div>


        <section class="health-madication-tabs">

            <div class="row">
                <div class="nav-pills">
                    <div class="col-md-2 dns-icon active"><a href="#health-history" data-toggle="tab"><i class="fa fa-heart"></i> My Health History</a></div>
                    <div class="col-md-2 dns-icon"><a href="#providers" data-toggle="tab"><i class="fa fa-user-md"></i> My Providers</a></div>
                    <div class="col-md-2 dns-icon"><a href="#history" data-toggle="tab"><i class="fa fa-calendar"></i> Consultation History</a></div>
                    <div class="col-md-2 dns-icon"><a href="#records" data-toggle="tab"><i class="fa fa-calendar-plus-o"></i> My Records</a></div>
                </div>
            </div>

        </section>
        <div class="tab-content">
            <div class="row haelth-section clearfix tab-pane fade in active" role="tabpanel" id="health-history">
                <div class="hs-list myhealth">
                    <div class="hs-l-main">
                        <div class="hs-l-icon"><i class="fa fa-heartbeat"></i></div>
                        <div class="hs-l-heading">My Health Conditions</div>
                        <div class="hs-l-questions">Do you have any health conditions?</div>
                        <div class="hs-l-questions-checkbox">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default " id="healthconditions-yes">Yes
                                    <input type="radio" name="option" id="Yes" autocomplete="off" chacked>
                                </label>

                                <label class="btn btn-default active" id="healthconditions-no">No
                                    <input type="radio" name="option" id="No" autocomplete="off">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="hs-l-questions-txt desc" id="healthconditions-div"  style="display: none;">
                        <div class="more-info select-yes" >
                            <div class="row table-container my-conditions my">
                                <div class="table" id="health_conditions_table">
                                    <div class="table-row tbl-header">
                                        <div class="table-cell">My Conditions</div>
                                        <div class="table-cell">Reported Date</div>
                                        <div class="table-cell">Source</div>
                                        <div class="table-cell"></div>
                                    </div>
                                    <div class="table-row no-records">
                                        <div class="table-cell">No medical conditions reported</div>
                                        <div class="table-cell"></div>
                                        <div class="table-cell"></div>
                                        <div class="table-cell delete"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="add-condition">
                                <div class="clearfix">
                                    <div class="col-md-10 row"> <input class="form-control input" id="healthCondition" placeholder="Start typing to search for health conditions (Example: Diabetes)" type="text"></div>
                                    <div class="col-md-2"> <button type="button" id="add_health_condition" class="btn btn-info btn-block pull-right">Add Condition</button></div>
                                    <br>
                                </div>
                                <div class="message health_message">Condition name not found. Check spelling or click Add.</div>
                            </div>
                        </div>


                    </div>

                </div>



                <div class="hs-list myhealth">
                    <div class="hs-l-main">
                        <div class="hs-l-icon"><i class="fa fa-medkit"></i></div>
                        <div class="hs-l-heading">My Medications</div>
                        <div class="hs-l-questions">Are you currently taking any medication?</div>
                        <div class="hs-l-questions-checkbox">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default " id="medications-yes">Yes
                                    <input type="radio" name="options" id="Yes" autocomplete="off" chacked>
                                </label>

                                <label class="btn btn-default active" id="medications-no">No
                                    <input type="radio" name="options" id="No" autocomplete="off">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="hs-l-questions-txt"  id="medications-div" style="display: none;">
                        <div class="more-info select-yes">
                            <div class="row table-container my-conditions my">
                                <div class="table" id="health_conditions_table">
                                    <div class="table-row tbl-header">
                                        <div class="table-cell">My Medications</div>
                                        <div class="table-cell">Frequency</div>
                                        <div class="table-cell">	Reported Date</div>
                                        <div class="table-cell">Source</div>
                                    </div>
                                    <div class="table-row no-records">
                                        <div class="table-cell">No medications reported</div>
                                        <div class="table-cell"></div>
                                        <div class="table-cell"></div>
                                        <div class="table-cell delete"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="add-condition">
                                <div class="clearfix">
                                    <div class="col-md-10 row"> <input class="form-control input" id="healthCondition" placeholder="Start typing to search for health conditions (Example: Diabetes)" type="text"></div>
                                    <div class="col-md-2"> <button type="button" id="add_health_condition" class="btn btn-info btn-block pull-right">Add Condition</button></div>
                                    <br>
                                </div>
                                <div class="message health_message">No medications reported</div>
                            </div>
                        </div>


                    </div>

                </div>



                <div class="hs-list myhealth">
                    <div class="hs-l-main">
                        <div class="hs-l-icon"><i class="fa fa-stethoscope"></i></div>
                        <div class="hs-l-heading">My Allergies</div>
                        <div class="hs-l-questions">Do you have any Allergies or Drug Sensitivities?</div>
                        <div class="hs-l-questions-checkbox">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default " id="allergies-yes">Yes
                                    <input type="radio" name="options" id="Yes" autocomplete="off" chacked>
                                </label>

                                <label class="btn btn-default active" id="allergies-no">No
                                    <input type="radio" name="options" id="No" autocomplete="off">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="hs-l-questions-txt" id="allergies-div" style="display: none;">
                        <div class="more-info select-yes">
                            <div class="row table-container my-conditions my">
                                <div class="table" id="health_conditions_table">
                                    <div class="table-row tbl-header">
                                        <div class="table-cell">
                                            My Allergies</div>
                                        <div class="table-cell">Severity</div>
                                        <div class="table-cell">Reaction</div>
                                        <div class="table-cell">Source</div>
                                    </div>
                                    <div class="table-row no-records">
                                        <div class="table-cell">No allergies reported</div>
                                        <div class="table-cell"></div>
                                        <div class="table-cell"></div>
                                        <div class="table-cell delete"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="add-condition">
                                <div class="clearfix">
                                    <div class="col-md-10 row"> <input class="form-control input" id="healthCondition" placeholder="Start typing to search for health conditions (Example: Diabetes)" type="text"></div>
                                    <div class="col-md-2"> <button type="button" id="add_health_condition" class="btn btn-info btn-block pull-right">Add Condition</button></div>
                                    <br>
                                </div>
                                <div class="message health_message">Have no allergies or drug sensitivities</div>
                            </div>
                        </div>


                    </div>

                </div>



                <div class="hs-list myhealth">
                    <div class="hs-l-main">
                        <div class="hs-l-icon"><i class="fa fa-user-md"></i></div>
                        <div class="hs-l-heading">My Surgeries and Procedures</div>
                        <div class="hs-l-questions">Have you ever had any surgeries or medical procedures?</div>
                        <div class="hs-l-questions-checkbox">
                            <div class="btn-group health-check" data-toggle="buttons">
                                <label class="btn btn-default " id="surgeries-yes">Yes
                                    <input type="radio" name="medical" id="option2" autocomplete="off" value="" >
                                </label>

                                <label class="btn btn-default active" id="surgeries-yes">No
                                    <input type="radio" name="medical" id="option2_2" autocomplete="off" value="">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="hs-l-questions-txt"  id="surgeries-div" style="display: none;">

                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="surgery-name">
                                <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                                Ankle
                            </label>
                        </div>

                    </div>

                </div>

            </div>


            <div class="row haelth-section clearfix tab-pane fade" role="tabpanel" id="providers">
                <?php
                $query = "SELECT `doctorId` FROM `". DB_PREFIX ."favorite_doctor` WHERE userId=?";

                try {
                    $st = $db->prepare( $query );
                    $st->execute(array($_SESSION["mvdoctorVisitornUserId"]));

                    if ( $st->rowCount() > 0 ) {
                        $all_doctors = $st->fetchAll();
                    }

                } catch (Exception $Exception) {
                    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
                }

                if ( !empty( $all_doctors ) ) {
                    foreach ( $all_doctors as $all_doctor ) {
                        $doctor_id[] = $all_doctor['doctorId'];
                    }

                    $select = "SELECT doc.id, doc.url, doc.doctor_name, doc.specialization, doc.degree, doc.fees, doc.location, doc.experience,
                 doc.sex, doc.featured_image, doc.ratings, doc.ratingCount";

                    $from = " FROM `" . DB_PREFIX . "doctors` as doc";

                    $where = " WHERE doc.`status` = 'publish'";

                    $order = ' ORDER BY doc.id asc';

                    $where .= ' AND doc.id in (' . implode(',', $doctor_id) . ')';

                    $query = $select . $from . $where . $order;

                    $st = $db->prepare($query);
                    $st->execute();

                    if ( $st->rowCount() > 0 ) {
                        $search_data = $st->fetchAll();
                    } else {
                        $search_data = array();
                    }
                }

                if ( !empty($search_data) ) {
                    $i = 0;
                    //echo "<div id='searchResult'>";
                    foreach ($search_data as $data): $i++
                        ?>
                        <div class="dnsm-list-col">
                            <div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="<?php echo $data['id']; ?>" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url('');">
                            	<img src="<?php echo HTTP_SERVER . 'images/doctors/full/' . $data['featured_image']; ?>" />
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong><?php echo $data['experience']; ?> YEARS</strong></div>
                            </div>

                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li class="name"><a href="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>"><i class="fa fa-user"></i> <?php echo sanitize($data['doctor_name'], 3); ?></a></li>
                                    <li>
                                        <input type="hidden" class="rating" value="<?php echo $data['ratings']; ?>" data-readonly data-fractions="2"/>
                                        <br>
                                        <small><?php echo $data['ratings']; ?> Star based
                                            on <?php echo $data['ratingCount']; ?> Reviews
                                        </small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> <?php echo $data['specialization']; ?></li>
                                    <li><i class=" fa fa-graduation-cap"></i><?php echo $data['degree']; ?></li>
                                    <li><i class=" fa fa-dollar"></i><?php echo $data['fees']; ?></li>
                                    <li><i class=" fa fa-hospital-o"></i><?php echo $data['location']; ?></li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
                        <?php
                    endforeach;

                } else { ?>

                    <h2 class="no-results">No Provider added to favorite.</h2>

                <?php } ?>

                <div class="hs-list myhealth hidden">
                    <div class="hs-l-main-no-pad">
                        <div class="hs-l-heading">Primary Care Provider</div>
                        <div class="hs-l-questions">MDLIVE can send a report of your consultation to primary care provider for continuity of care.</div>
                        <div class="hs-l-questions-checkbox">
                            <div class="btn btn-info pull-right" id="provider-btn">Add Provider</div>
                        </div>
                    </div>
                </div>
                <div class="hs-l-questions-txt provider-btn-div hidden" style="display: none;">
                    <div class="more-info select-yes">

                        <div class="add-condition">
                            <div class="clearfix">
                                <div class="col-md-4">
                                    <label>First Name:</label>
                                    <input class="form-control input" placeholder="First Name" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>Middle Name:</label>
                                    <input class="form-control input" placeholder="Middle Name" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>Last Name:</label>
                                    <input class="form-control input" placeholder="Last Name" type="text">
                                </div>
                                <div class="col-md-6">
                                    <label>Email:</label>
                                    <input class="form-control input" placeholder="Email" type="text">
                                </div>
                                <div class="col-md-6">
                                    <label>Practice Name:</label>
                                    <input class="form-control input" placeholder="Practice Name" type="text">
                                </div>

                                <div class="col-md-6">
                                    <label>Address:</label>
                                    <input class="form-control input" placeholder="Address" type="text">
                                </div>
                                <div class="col-md-6">
                                    <label>Address (Line 2):</label>
                                    <input class="form-control input" placeholder="Address (Line 2)" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>City:</label>
                                    <input class="form-control input" placeholder="City" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>State:</label>
                                    <input class="form-control input" placeholder="State" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>Country:</label>
                                    <input class="form-control input" placeholder="Country" type="text">
                                </div>

                                <div class="col-md-4">
                                    <label>Cell:</label>
                                    <input class="form-control input" placeholder="Cell" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>Preferred Phone:</label>
                                    <input class="form-control input" placeholder="Preferred Phone" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>Fax:</label>
                                    <input class="form-control input" placeholder="Fax" type="text">
                                </div>

                                <div class="col-md-2 "> <button type="button" id="add_health_condition" class="btn btn-info btn-block pull-right">Add</button></div>

                            </div>

                        </div>
                    </div>


                </div>

            </div>
            <div class="row haelth-section clearfix tab-pane fade" role="tabpanel" id="history">
                <div class="">
                    <div class="col-md-12 text-center"><i class="fa fa-calendar"></i></div>
                    <div class="col-md-12 text-center">No Previous visit</div>
                    <div class="col-md-12 text-center">You will see your visit history here</div>
                </div>

            </div>

            <div class="row haelth-section clearfix tab-pane fade" role="tabpanel" id="records">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Upload files</strong> <small> </small></div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="document-name">Name of Document:</label>
                                <input type="text" class="form-control" id="document-name" placeholder="">
                            </div>
                            <div class="input-group image-preview">
                                <input placeholder="" type="text" class="form-control image-preview-filename" disabled="disabled">
                                <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;"> <span class="glyphicon glyphicon-remove"></span> Clear </button>
                                    <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input"> <span class="glyphicon glyphicon-folder-open"></span> <span class="image-preview-input-title">Browse</span>
                                    <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/>
                                    <!-- rename it -->
                                </div>
                                <button type="button" class="btn btn-labeled btn-default"> <span class="btn-label"><i class="glyphicon glyphicon-upload"></i> </span>Upload</button>
                                </span> </div>
                            <!-- /input-group image-preview [TO HERE]-->

                            <br />

                            <!-- Drop Zone -->
                            <div class="upload-drop-zone" id="drop-zone"> Or drag and drop files here </div>
                            <br />
                            <!-- Progress Bar -->
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"> <span class="sr-only">60% Complete</span> </div>
                            </div>
                            <br />
                            <!-- Upload Finished -->
                            <div class="js-upload-finished">
                                <h4>Upload history</h4>
                                <div class="list-group"> <a href="#" class="list-group-item list-group-item-danger"><span class="badge alert-danger pull-right">23-11-2014</span>amended-catalogue-01.xls</a> <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">23-11-2014</span>amended-catalogue-01.xls</a> <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">23-11-2014</span>amended-catalogue-01.xls</a> <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">23-11-2014</span>amended-catalogue.xls</a> </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>


    </div>
</section>

<!-- Modal 2-->
<div class="modal fade" id="myModalFav" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;" id="favoriteModal"></div>
        </div>
    </div>
</div>

<!-- Modal 3 -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModalFav">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-info" role="alert" id="removeModalFavContent"><strong>Warning!</strong> Are you sure you want to remove this doctor from favorites ?</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default removeFavorite">Remove</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    jQuery(function($) {

        $('.nav-pills div a').click(function (e) {
            $('.nav-pills div.active').removeClass('active')
            $(this).parent('div').addClass('active')
        });


        $('#provider-btn').click(function() {
            $('.provider-btn-div').css('display','block');
        });

        $('#healthconditions-yes').click(function() {
            $('#healthconditions-div').css('display','block');
        });
        $('#healthconditions-no').click(function() {
            $('#healthconditions-div').css('display','none');
        });

        $('#medications-yes').click(function() {
            $('#medications-div').css('display','block');
        });
        $('#healthconditions-no').click(function() {
            $('#medications-div').css('display','none');
        });

        $('#allergies-yes').click(function() {
            $('#allergies-div').css('display','block');
        });
        $('#allergies-no').click(function() {
            $('#allergies-div').css('display','none');
        });

        $('#surgeries-yes').click(function() {
            $('#surgeries-div').css('display','block');
        });
        $('#surgeries-no').click(function() {
            $('#surgeries-div').css('display','none');
        });

        $('body').on('click', '.consult_now', function(){
            //
            $('#company_url').html( $(this).data('url') );
            $('#modal_image').html('<img src="'+ $(this).data('src') +'" style="height: 56px; text-align: center;">')

        });

        var login, doctor_id;

        $('body').on('click', '.removeFavorite', function(){
            var th = $(this);

            if ( login == 'yes' && doctor_id > 0 ) {

                $('#removeModalFavContent').html('Please wait...');
                //call ajax
                th.attr('readonly', 'readonly');
                $.ajax({
                    url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=remove_doctor_favorite",
                    data: {doctor_id: doctor_id, token: '<?php echo getToken(); ?>'},
                    method: "POST",
                    dataType: "json"
                }).error(function (err) {
                    th.removeAttr('readonly');
                    console.log(err);

                }).done(function () {
                    //

                }).success(function (data) {

                    if (data.success) {
                        //th.parent().parent().remove();
                        var url = '<?php echo HTTP_SERVER . 'patient/providers.html'; ?>';
                        window.location = url;
                    }
                    if ( data.error ) {
                        $('#favoriteModal').html('<div class="alert alert-danger" role="alert">'+data.error+'</div>');
                        th.removeAttr('readonly');
                        $('#myModalFav').modal();
                    }

                }, 'json');

            } else {
                $('#removeModalFavContent').html('<strong>Warning!</strong> Are you sure you want to remove this doctor from favorite list ?');
                $('#favoriteModal').html('<div class="alert alert-danger" role="alert">Please login to remove doctor from favorite.</div>');
                $('#myModalFav').modal();
            }
        });

        //removeFavoriteModal
        $('body').on('click', '.removeFavoriteModal', function(){
            login = $(this).data('login');
            doctor_id = $(this).data('id');
            $('#removeModalFav').modal();
        });

    });
</script>
