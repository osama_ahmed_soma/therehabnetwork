<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

checkUserSessionType(4);

$query = "SELECT `doctorId` FROM `". DB_PREFIX ."favorite_doctor` WHERE userId=?";

try {
    $st = $db->prepare( $query );
    $st->execute(array($_SESSION["mvdoctorVisitornUserId"]));

    if ( $st->rowCount() > 0 ) {
        $all_doctors = $st->fetchAll();
    }

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}

if ( !empty( $all_doctors ) ) {
    foreach ( $all_doctors as $all_doctor ) {
        $doctor_id[] = $all_doctor['doctorId'];
    }

    $select = "SELECT doc.id, doc.url, doc.doctor_name, doc.specialization, doc.degree, doc.fees, doc.location, doc.experience,
                 doc.sex, doc.featured_image, doc.ratings, doc.ratingCount";

    $from = " FROM `" . DB_PREFIX . "doctors` as doc";

    $where = " WHERE doc.`status` = 'publish'";

    $order = ' ORDER BY doc.id asc';

    $where .= ' AND doc.id in (' . implode(',', $doctor_id) . ')';

    $query = $select . $from . $where . $order;

    $st = $db->prepare($query);
    $st->execute();

    if ( $st->rowCount() > 0 ) {
        $search_data = $st->fetchAll();
    } else {
        $search_data = array();
    }
}
?>
<section class="dashboard-new-section-main myproviders-main patient-dashbaord-section-main clearfix tab-pane fade in active" role="tabpanel" id="provider">
    <div class="container">

        <div class="row">
            <div class="col-md-12  clearfix"><br>
                <div class="dnsm-highlights">My <strong> Providers</strong></div>
            </div>
            <div class="dnsm-list clearfix">
                <?php
                if ( !empty($search_data) ) {
                    $i = 0;
                    //echo "<div id='searchResult'>";
                    foreach ($search_data as $data): $i++
                ?>
                    <div class="dnsm-list-col">
                        <div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="<?php echo $data['id']; ?>" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                        <div class="dnsm-lc-image" style="background:url('');">
                        	<img src="<?php echo HTTP_SERVER . 'images/doctors/full/' . $data['featured_image']; ?>" />
                            <span><i class="fa fa-user"></i> Online</span>
                            <div class="experince">EXPERIENCE <br> <strong><?php echo $data['experience']; ?> YEARS</strong></div>
                        </div>

                        <div class="dnsm-lc-txt-area">
                            <ul>
                                <li class="name"><a href="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>"><i class="fa fa-user"></i> <?php echo sanitize($data['doctor_name'], 3); ?></a></li>
                                <li>
                                    <input type="hidden" class="rating" value="<?php echo $data['ratings']; ?>" data-readonly data-fractions="2"/>
                                    <br>
                                    <small><?php echo $data['ratings']; ?> Star based
                                        on <?php echo $data['ratingCount']; ?> Reviews
                                    </small>
                                </li>
                                <li><i class=" fa fa-stethoscope"></i> <?php echo $data['specialization']; ?></li>
                                <li><i class=" fa fa-graduation-cap"></i><?php echo $data['degree']; ?></li>
                                <li><i class=" fa fa-dollar"></i><?php echo $data['fees']; ?></li>
                                <li><i class=" fa fa-hospital-o"></i><?php echo $data['location']; ?></li>
                            </ul>
                            <p></p>
                        </div>
                        <div class="dnsm-lc-btn"> <a href="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                        <div class="dnsm-lc-btn"> <a href="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                    </div>
                <?php
                    endforeach;

                } else { ?>

                    <h2 class="no-results">No Provider added to favorite.</h2>

                <?php } ?>

            </div>

        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;">
                <div class="loding-more" id="modal_image"></div>
                <div class="loding-more">
                    <p>My Virtual Doctor is sending you to <span id="company_url"></span></p>
                </div>
                <p style="color: #bcbcbc;">You are being redirected to begin your consult. Thank you for using My Virtual Doctor.</p>
                <div class="loding-bar">
                    <img src="../img/heart.gif" />
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal 2-->
<div class="modal fade" id="myModalFav" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;" id="favoriteModal"></div>
        </div>
    </div>
</div>

<!-- Modal 3 -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModalFav">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-info" role="alert" id="removeModalFavContent"><strong>Warning!</strong> Are you sure you want to remove this doctor from favorites ?</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default removeFavorite">Remove</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    jQuery(function($) {
        $('body').on('click', '.consult_now', function(){
            //
            $('#company_url').html( $(this).data('url') );
            $('#modal_image').html('<img src="'+ $(this).data('src') +'" style="height: 56px; text-align: center;">')

        });

        var login, doctor_id;

        $('body').on('click', '.removeFavorite', function(){
            var th = $(this);

            if ( login == 'yes' && doctor_id > 0 ) {

                $('#removeModalFavContent').html('Please wait...');
                //call ajax
                th.attr('readonly', 'readonly');
                $.ajax({
                    url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=remove_doctor_favorite",
                    data: {doctor_id: doctor_id, token: '<?php echo getToken(); ?>'},
                    method: "POST",
                    dataType: "json"
                }).error(function (err) {
                    th.removeAttr('readonly');
                    console.log(err);

                }).done(function () {
                    //

                }).success(function (data) {

                    if (data.success) {
                        //th.parent().parent().remove();
                        var url = '<?php echo HTTP_SERVER . 'patient/providers.html'; ?>';
                        window.location = url;
                    }
                    if ( data.error ) {
                        $('#favoriteModal').html('<div class="alert alert-danger" role="alert">'+data.error+'</div>');
                        th.removeAttr('readonly');
                        $('#myModalFav').modal();
                    }

                }, 'json');

            } else {
                $('#removeModalFavContent').html('<strong>Warning!</strong> Are you sure you want to remove this doctor from favorite list ?');
                $('#favoriteModal').html('<div class="alert alert-danger" role="alert">Please login to remove doctor from favorite.</div>');
                $('#myModalFav').modal();
            }
        });

        //removeFavoriteModal
        $('body').on('click', '.removeFavoriteModal', function(){
            login = $(this).data('login');
            doctor_id = $(this).data('id');
            $('#removeModalFav').modal();
        });

    });
</script>