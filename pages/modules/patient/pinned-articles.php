<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

checkUserSessionType(4);

require_once $siteRootHome .'includes/classes/bootstrap_pagination1.php';

$limit = 10; //how many items to show per page
if ( isset( $_GET['page_now'] ) ) {
    $page = intval( $_GET['page_now'] );
} else {
    $page = 0;
}

if ( $page ) {
    $start = ($page - 1) * $limit; //first item to display on this page
} else {
    $start = 0;
}

//get all pinned post
$query = "SELECT `postId` FROM `". DB_PREFIX ."pinned_post` WHERE userId=?";

try {
    $st = $db->prepare( $query );
    $st->execute(array($_SESSION["mvdoctorVisitornUserId"]));

    if ( $st->rowCount() > 0 ) {
        $all_posts = $st->fetchAll();
    }

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}

if ( !empty($all_posts) ) {
    foreach ( $all_posts as $all_post ) {
        $pinned_articles[] = $all_post['postId'];
    }

    try {
        $select = "SELECT tips.id, tips.url, tips.title, tips.image, tips.excerpt, doc.doctor_name, doc.specialization, doc.degree";
        $table = " FROM `" . DB_PREFIX . "health_tips` as tips, `" . DB_PREFIX . "doctors` as doc";
        $where = " WHERE doc.id = tips.doctorId AND tips.status = 'publish'";
        $where .= ' AND tips.id in (' . implode(',', $pinned_articles) . ')';
        $order = " ORDER BY tips.id desc";
        $lim = " LIMIT $start, $limit";

        $sql = $select . $table. $where . $order . $lim;

        $st = $db->prepare( $sql );
        $st->execute( );

        if ( $st->rowCount() > 0 ) {
            $data = $st->fetchAll();
        } else {
            $data = array();
        }

    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }


    //pagination task
    try {
        $sql = "SELECT COUNT(tips.id) as cnt " . $table . $where . $order;

        $st = $db->prepare($sql);
        $st->execute();

        if ( $st->rowCount() > 0 ) {
            $row = $st->fetch();

            //create a pagin class
            $pagin = new Bootpagin();

            $pagin->total_records = $row['cnt'];
            $pagin->noperpage = $limit;
            //this would give example.php?page=1 url type
            $pagin->url = HTTP_SERVER . '?do=patient&page=pinned-articles';
            $pagin->val = 'page_now';
            //pagin size for small column or large column in bootstrap (sm or lg )
            //for default pagination size leave value empty like this $pagin->size=''
            //$pagin->size = 'sm';
        }
    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }
}


?>
<section class="dashboard-new-section-main pinnedarticles-main patient-blog-section clearfix tab-pane fade in active" role="tabpanel" id="Pinned">
    <div class="container">
        <div class="dnsm-list clearfix">
            <div id="bloglisting">
                <?php  if ( isset($data) && !empty($data) ):
                    foreach ( $data as $item ) {
                    $item['image'] = unserialize(base64_decode($item['image']));
                ?>
                    <div class="dnsm-list-col">
                        <div class="dnsm-lc-image" style="background:url('');"><img src="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" /></div>
                        <div class="dnsm-lc-txt-area">
                            <ul>
                                <li> <i class="fa fa-clipboard"></i><a href="<?php echo HTTP_SERVER . 'blog/' . $item['url'] . '.html'; ?>"><?php echo sanitize($item['title']); ?></a></li>
                                <ul class="medial-info article-media-info">
                                    <li> by <strong> <?php echo $item['doctor_name'] ?></strong></li>
                                    <li><i class=" fa fa-stethoscope"></i> <?php echo $item['specialization'] ?></li>
                                    <li><i class=" fa fa-graduation-cap"></i> <?php echo $item['degree'] ?></li>
                                </ul>
                            </ul>
                            <p><?php echo trim_words(sanitize($item['excerpt'],3), 30); ?></p>
                        </div>
                        <div class=" health-social">
                            <span class='st_facebook_large' displayText='Facebook' st_summary="<?php echo sanitize($item['excerpt'],3); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo sanitize($item['title'],3); ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['url'] . '.html'; ?>'></span>
                            <span class='st_twitter_large' displayText='Tweet' st_summary="<?php echo sanitize($item['excerpt'],3); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo sanitize($item['title'],3); ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['url'] . '.html'; ?>'></span>
                            <span class='st_email_large' displayText='Email' st_summary="<?php echo sanitize($item['excerpt'],3); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo sanitize($item['title'],3); ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['url'] . '.html'; ?>'></span>
                        </div>
                        <div class="dnsm-lc-btn unpin">
                            <a href="#" class="btn-block removeFavoriteModal" data-login="<?php if ( !isset($_SESSION["mvdoctorVisitornUserId"]) ) { echo 'no'; } else { echo 'yes'; } ?>" data-id="<?php echo $item['id']; ?>" alt="Unpin Article" title="Unpin Article"><i class="fa fa-remove"></i> Unpin Article</a>
                        </div>
                    </div>
                <?php
                    }
                    //call the pagin function
                    echo "<div style='text-align: center; clear: both;'>";
                    $pagin->pagin();
                    echo "</div>";

                else:
                    echo '<h2 class="no-results">You have not pinned any blogs yet.  Read the blog <a href="'.HTTP_SERVER.'/blog.html">here</a>.</h2>';
                endif;
                ?>
            </div>
        </div>
    </div>
</section>

<!-- Modal 2-->
<div class="modal fade" id="myModalFav" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;" id="favoriteModal"></div>
        </div>
    </div>
</div>

<!-- Modal 3 -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModalFav">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-info" role="alert" id="removeModalFavContent"><strong>Warning!</strong> Are you sure you want to remove this article from list ?</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default removeFavorite">Remove</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
    jQuery(function($){
        var login, post_id;
        $('body').on('click', '.removeFavorite', function(){
            var th = $(this);
            //var login = $(this).data('login');
            //var post_id = $(this).data('id');

            if ( login == 'yes' && post_id > 0 ) {

                $('#removeModalFavContent').html('Please wait...');
                //call ajax
                th.attr('readonly', 'readonly');
                $.ajax({
                    url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=remove_doctor_favorite",
                    data: {post_id: post_id, token: '<?php echo getToken(); ?>'},
                    method: "POST",
                    dataType: "json"
                }).error(function (err) {
                    th.removeAttr('readonly');
                    console.log(err);

                }).done(function () {
                    //

                }).success(function (data) {

                    if (data.success) {
                        //th.parent().parent().parent().parent().remove();
                        var url = '<?php echo HTTP_SERVER . 'patient/pinned-articles.html'; ?>';
                        window.location = url;
                    }
                    if ( data.error ) {
                        $('#favoriteModal').html('<div class="alert alert-danger" role="alert">'+data.error+'</div>');
                        th.removeAttr('readonly');
                        $('#myModalFav').modal();
                    }

                }, 'json');

            } else {
                $('#removeModalFavContent').html('<strong>Warning!</strong> Are you sure you want to remove this article from list ?');
                $('#favoriteModal').html('<div class="alert alert-danger" role="alert">Please login to remove article from list.</div>');
                $('#myModalFav').modal();
            }
        });

        //removeFavoriteModal
        $('body').on('click', '.removeFavoriteModal', function(){
            login = $(this).data('login');
            post_id = $(this).data('id');
            $('#removeModalFav').modal();
        });

    });
</script>
