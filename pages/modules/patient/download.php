<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
checkUserSessionType(4);
$id  =   isset($_GET["id"]) ? sanitize($_GET["id"],4) : '';


if($id > 0)
{

	try {
			$query = "SELECT * FROM " . DB_PREFIX . "my_record_images WHERE myRecordPatientId = ? AND myRecordImageId = ?  order by myRecordImageId DESC";
			$result = $db->prepare($query);
			$result->execute(array($_SESSION["mvdoctorVisitornUserId"],$id));
		
			if ( $result->rowCount() > 0 ) {
				$my_img_data = $result->fetch();
			}
			else
			{
				$my_img_data = array();
			}
		
		} catch (Exception $Exception) {
			exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
	}
	

								
	
	if(count($my_img_data )  > 0)
	{
		
			
			$file = SITE_ROOT.'images/uploads/my_records/' . $my_img_data['myRecordNewImageName']; // for live
			//$file = 'http://noman/Projects/MTA/mpchelas/new/files/efts/' . $tmpFile; // for local
			//echo $file; exit;
	
			if (!$file) {
				die('file not found'); //Or do something 
			} else {
				// Set headers
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Disposition: attachment; filename=".$my_img_data['myRecordOrigName']);
				header("Content-Type: text/plain");
				header("Content-Transfer-Encoding: binary");
				// Read the file from disk
				readfile($file); 
				exit;
			
		}
	}

}
?>
<!--
<script>
$(document).ready(function(e) {
	$("body").hide();
    
});
</script>-->