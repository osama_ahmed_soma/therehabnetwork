<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

checkUserSessionType(4);

//get category data
$location_data = array();

try {
    $query = "SELECT id, categoryName, categoryType, categoryFeatured FROM " . DB_PREFIX . "category WHERE `categoryType` = 'location' AND categoryStatus = 1 order by `categoryOrder` asc";
    $result = $db->prepare($query);
    $result->execute();

    if ( $result->rowCount() > 0 ) {
        $location_data = $result->fetchAll();
    }

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}


//get appointments
$today = strtotime("Today");

try {
    $query = "SELECT sc.* FROM `" . DB_PREFIX . "schedule` as sc WHERE sc.patientId = ? AND sc.startTime > ? ORDER BY sc.startTime asc";
    $st = $db->prepare($query);
    $st->execute( array($_SESSION["mvdoctorVisitornUserId"], $today) );

    if ( $st->rowCount() )  {
        $data = $st->fetchAll();
    } else {
        $data = array();
    }

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}

//get latest 6 blog post
try {
    $select = "SELECT tips.id, tips.url, tips.title, tips.image, tips.excerpt";
    $table = " FROM `" . DB_PREFIX . "health_tips` as tips";
    $where = " WHERE tips.status = 'publish'";
    $order = " ORDER BY tips.id desc";
    $lim = " LIMIT 0, 6";

    $sql = $select . $table. $where . $order . $lim;

    $st = $db->prepare( $sql );
    $st->execute( );

    if ( $st->rowCount() > 0 ) {
        $tips_data = $st->fetchAll();
    } else {
        $tips_data = array();
    }

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}
?>
<section class="dashboard-new-section-main patient-dashbaord-section-main clearfix tab-pane fade in active" role="tabpanel" id="dasboard">
    <div class="container">
        <div class="admin-pro clearfix">
            <div class="col-sm-3 col-md-2 text-center">

                <div  id="upload_photo_div" class="user-display-picture">
                    <img alt="1" class="avatar" src="<?php
                    if ( isset($_SESSION["mvdoctorVisitoruserPicture"]) && $_SESSION["mvdoctorVisitoruserPicture"] != '' ) {
                        echo HTTP_SERVER . 'images/user/thumb/' . $_SESSION["mvdoctorVisitoruserPicture"];
                    } else {
                        echo get_gravatar($_SESSION["mvdoctorVisitorUserEmail"],87);
                    } ?>">

                </div>
                <div class="change-photo">

                    <a href="<?php echo HTTP_SERVER . 'patient/profile-settings.html' ?>" class="btn btn-info">Edit Profile</a>
                </div>

            </div>

            <div class="user-info col-sm-9 col-md-10">
                <div class="last-updated-info pull-right">
                    <span id="last_updated_at">Last Updated On 05/28/2016</span>
                </div>
                <div>
                    <span class="title"><?php echo $_SESSION["mvdoctorVisitorUserFname"] . ' ' . $_SESSION["mvdoctorVisitorUserLname"]; ?></span>
                </div>
                <div>
                    <span id="profile_percentage">Your profile is <strong>92%</strong> complete</span>
                </div>
                <div class="progress">
                    <div id="profile_progress_bar" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 92%;">
                        <span class="sr-only"></span>
                    </div>
                </div>
                <div>
                    <span class="notification" id="pc_condition">Please complete <strong><a data-remote="true" href="<?php echo HTTP_SERVER; ?>patient/health.html">My Health</a></strong> questions</span>
                </div>
            </div>


        </div>
        <div class="admin-home-pro-banner clearfix" style="background: linear-gradient(rgba(187, 178, 69, 0.07), rgba(103, 103, 103, 0.06)), url(<?php echo HTTP_SERVER; ?>img/34601926.jpg) center bottom;background-size: cover;    background-position: 0px -80px;">
            <div class="dr-see">
                <div class="dr-icon"><i class="fa fa-user-md"></i></div>
                <div class="dr-text">
                    <div class="dr-txt-head">See a Doctor</div>
                    <div class="dr-txt-pra">Find a Provider and make an appointment</div>
                </div>
            </div>

            <div class="user-search-section ">

                <div class="dr-search">
                    <!-- Search Section --->
                    <form method="get" action="<?php echo HTTP_SERVER ?>search.html">
                        <div class="input-group hm-search">
                            <span class="input-group-addon">
                            <i class="fa  fa-user-md"></i></span>
                            <select name="location" data-placeholder="Select a state" class="chosen-select form-control paitent-dropdown" tabindex="-1" style="display: none;">
                                <option value=""></option>
                                <?php if ( !empty( $location_data )): ?>
                                    <?php
                                    foreach ( $location_data as $item ) {
                                        echo "<option value='{$item['id']}'>{$item['categoryName']}</option>";
                                    }
                                    ?>
                                <?php endif; ?>
                            </select>
                            <div class="chosen-container chosen-container-single" style="width: 264px;" title="">

                                <div class="chosen-drop">
                                    <div class="chosen-search">
                                        <input type="text" autocomplete="off" tabindex="5">
                                    </div>
                                    <ul class="chosen-results"></ul>
                                </div>
                            </div>
                        </div>


                        <div class="input-group hm-search">
                            <span class="input-group-addon">
                                <i class="fa fa-user-md"></i>
                            </span>

                            <div class="chosen-container chosen-container-single" style="width: 270px;" title="">
                                <input type="text" class="chosen-single chosen-default wth-100" name="searchName" autocomplete="off" placeholder="Physicians Name">
                                <div class="chosen-drop">
                                    <div class="chosen-search">
                                        <input type="text" class="chosen-single chosen-default" autocomplete="off" tabindex="5" placeholder="Physicians Name">
                                    </div>
                                    <ul class="chosen-results"></ul>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Search</button>
                    </form>
                    <!--  End Search Section --->


                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12  clearfix"><br>
                <div class="dnsm-highlights">Upcoming <strong>Appointments</strong>
                    <?php if ( !empty($data) && count($data) > 3 ):  ?>
                    <div class="left-right-main">
                        <!-- Left and right controls -->
                        <a class="left" href="#doctorlisting" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right" href="#doctorlisting" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>

            <?php if ( empty($data) ):  ?>
                <div class="container">
                    <h5 style="text-align: center;">No current appointments.</h5>
                </div>
            <?php else: ?>

            <div class="dnsm-list clearfix">

                <div id="doctorlisting" class="carousel slide" data-ride="carousel"  data-interval="false">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php
                        $i = 0;
                        $first = true;
                        foreach ( $data as $appointment ) {
                            //calculating time difference
                            $difference = check_date_relavence(date('Y-m-d', $appointment['startTime']));
                            $date_string = '&nbsp;';

                            //calculate hour difference
                            $date1 = date('Y-m-d h:i A', $appointment['startTime']);
                            $date2 = date('Y-m-d h:i A', time());

                            $hourdiff = round((strtotime($date1) - strtotime($date2)) / 3600, 1);

                            if ($difference == 0 && $hourdiff >= 0) {
                                //echo 'today';
                                $minutes = explode('.', $hourdiff);

                                if (intval($minutes[0]) == 0) {
                                    $date_string = 'In about ' . $minutes[1] . ' Minutes';
                                } elseif (intval($minutes[0]) <= 9) {
                                    $date_string = 'In about ' . $minutes[0] . ' Hours';
                                } else {
                                    $date_string = 'Today';
                                }
                            } else if ($difference == 0 && $hourdiff < 0) {

                                $minutes = explode('.', abs($hourdiff));

                                if (intval($minutes[0]) == 0) {
                                    $date_string = $minutes[1] . ' Minutes Ago';
                                } elseif (intval($minutes[0]) <= 9) {
                                    $date_string = $minutes[0] . ' Hours Ago';
                                }
                            } else if ($difference > 1) {
                                //echo 'Future Date';
                            } else if ($difference > 0 && $hourdiff >= 0 && $hourdiff <= 9) {
                                //echo 'tomorrow';
                                $minutes = explode('.', $hourdiff);
                                if (intval($minutes[0]) == 0) {
                                    $date_string = 'In about ' . $minutes[1] . ' Minutes';
                                } elseif (intval($minutes[0]) <= 9) {
                                    $date_string = 'In about ' . $minutes[0] . ' Hours';
                                }

                            } else if ($difference > 0) {
                                $date_string = 'Tomorrow';
                            } else if ($difference < -1) {
                                //echo 'Long Back';
                            } else {
                                //echo 'yesterday';
                                $date_string = 'Yesterday';
                            }

                            $date_time = date('Y-m-d', $appointment['startTime']) . 'T' . date('H:i', $appointment['startTime']);

                            //get doctor info
                            $query = "SELECT id, doctor_name, overview, url, specialization, degree, fees, location, experience, specialization, degree,
                                      sex, featured_image FROM `" . DB_PREFIX . "doctors` WHERE id = ? LIMIT 1";
                            $st = $db->prepare($query);
                            $st->execute(array($appointment['doctorId']));

                            if ($st->rowCount()) {
                                $doctor_data = $st->fetch();
                                $doctor_name = $doctor_data['doctor_name'];
                                $doctor_url = HTTP_SERVER . 'doctor/' . $doctor_data['url'] . '.html';
                                $readmore = "<a href='{$doctor_url}'>Read More</a>";
                            } else {
                                $doctor_name = '';
                                $doctor_url = "#";
                            }

                            if ( $i == 0 ) {
                                //start a new item div
                                echo '<div class="item';
                                if ( $first == true ) {
                                    echo ' active';
                                    $first = false;
                                }
                                echo '">';
                            }
                            //now add appointment div data
                            ?>
                            <div class="dnsm-list-col">
                                <div class="dnsm-lc-image" style="background:url('');">
                                	<img src="<?php echo HTTP_SERVER . 'images/doctors/full/' . $doctor_data['featured_image']; ?>" />
                                    <span><i class="fa fa-user"></i> Online</span>
                                </div>
                                <div class="dnsm-lc-txt-area">
                                    <ul>
                                        <li class="name"><a href="<?php echo $doctor_url; ?>"><i class="fa fa-user"></i> <?php echo sanitize($doctor_data['doctor_name'], 3); ?></a></li>
                                        <li class="date"><i class="fa fa-calendar"></i> <?php echo date('jS M, Y', $appointment['startTime']); ?> <i class="fa fa-clock-o"></i><?php echo date('h:i A', $appointment['startTime']); ?></li>
                                        <li><i class="fa fa-stethoscope"></i> <?php echo $doctor_data['specialization']; ?></li>
                                        <li><i class="fa fa-graduation-cap"></i> <?php echo $doctor_data['degree']; ?></li>
                                    </ul>
                                    <p><?php echo trim_words( $doctor_data['overview'], 10, $readmore );?></p>
                                </div>
                                <div class="dnsm-lc-btn"> <a href="#" class="btn-block loginUrl">Start Appointment</a></div>
                            </div>

                        <?php
                            if ( $i == 2 ) {
                                echo "</div>";
                                $i = 0;
                            } else {
                                $i++;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>

            <?php endif; ?>
        </div>
</section>

<section class="dashboard-new-section-main patient-blog-section clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <div class="col-md-12 clearfix"><div class="dnsm-highlights">Recent  <strong>Activity</strong>

                    <div class="left-right-main">
                        <!-- Left and right controls -->
                        <a class="left" href="#bloglisting" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right" href="#bloglisting" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                </div></div>



            <div class="dnsm-list clearfix">
                <div id="bloglisting" class="carousel slide" data-ride="carousel"  data-interval="false">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php
                        $i = 0;
                        $first = true;

                        foreach ( $tips_data as $tips ) {

                            $url = HTTP_SERVER . 'blog/' . $tips['url'] . '.html';
                            $tips['image'] = unserialize(base64_decode($tips['image']));

                            if ( $i == 0 ) {
                                //start a new item div
                                echo '<div class="item';
                                if ( $first == true ) {
                                    echo ' active';
                                    $first = false;
                                }
                                echo '">';
                            }
                        ?>
                            <div class="dnsm-list-col">
                                <div class="dnsm-lc-image" style="background:url('');">
                                	<img src="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $tips['image']['img1']; ?>" />
                                </div>
                                <div class="dnsm-lc-txt-area">
                                    <ul>
                                        <li> <i class="fa fa-clipboard"></i><?php echo sanitize($tips['title'],3); ?></li>
                                    </ul>
                                    <p><?php echo trim_words(sanitize($tips['excerpt'],3), 30, " <a href='$url'> Read More</a>"); ?></p>
                                </div>

                            </div>
                        <?php
                            if ( $i == 2 ) {
                                echo "</div>";
                                $i = 0;
                            } else {
                                $i++;
                            }

                        }
                        if ( $i != 2 ) {
                            echo "</div>";
                        }

                        ?>
                    </div>
                </div>
            </div>
        </div>
</section>
<script type="text/javascript">
    jQuery(function($){
        $('.loginUrl').click(function() {
            //ajax call
            var th = $(this);
            th.html('Generating Login URL. Please wait...');

            $.ajax({
                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=get_login_url",
                data: {token: '<?php echo getToken(); ?>'},
                method: "GET",
                dataType: "json"
            }).error(function (err) {
                console.log(err);

            }).done(function () {

            }).success(function (data) {

                if (data.success && data.url) {
                    th.html('Redirecting...');
                    window.location = data.url;
                }
                else {
                    th.html('Video Consultancy Dashboard');
                    alert('Unknown Error!');
                }

            }, 'json');

            return false;
        });
    });
</script>