<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

checkUserSessionType(4);

$error_msg = array();
$success_msg = "";

if ( isset( $_POST['user_profile'] ) ) {
    $usr_email = sanitize($_POST['userEmail']);
    unset($_POST['userEmail']);
    $data = array_map_deep($_POST, 'sanitize');
    $data = array_map_deep($data, 'trim');
	
	if(!empty($data['userDob']))
	{
		$arr = explode("/",$data['userDob']);
		if(count($arr) > 1)
		{
			$data['userDob'] = $arr[2].'-'.$arr[0].'-'.$arr[1];
		}
		
		
	}

    if ( isset($_FILES['user_image']['name']) && $_FILES['user_image']['name'] != ''  ) {
        //delete old image
        if ( isset($data['userPicture1']) && $data['userPicture1'] != '' ) {
            unlink(SITE_ROOT . 'images/user/full/' . $data['userPicture1']);
        }

        if ( isset($data['userPicture2']) && $data['userPicture2'] != '' ) {
            unlink(SITE_ROOT . 'images/user/thumb/' . $data['userPicture2']);
        }

        //add new image
        $image = uploadUserImage();

        if ( $image['status'] == 'success' ) {
            $data['userPicture'] = base64_encode(serialize($image['data']));
            $_SESSION["mvdoctorVisitoruserPicture"] = $image['data']['img2'];
        } else {
            die ("Image Upload Error" . $image['data']);
        }
    }

    unset($data['userPicture1']);
    unset($data['userPicture2']);

    //check for snap id user
    require_once $siteRoot .'securevideo/securevideo_lib.php';
    $secure_lib = new Securevideo_Lib();

    if ($data['secureVideoId'] == '') {
        //create patient
        $patient_data = array(
            "FullName"      => $data['userFname'] . ' ' . $data['userLname'],
            "EmailAddress"  => $usr_email,
            "TimeZoneWindowsId" => isset( $data['userTimeZone'] ) && $data['userTimeZone'] != '' ? $data['userTimeZone'] : 'UTC',
            //"SmsNumber"     => $data['userPhoneHome'],
            'HelpNumber' => '212-555-5555',
            'SystemRoles'   => 'P',
            'ShouldAutoDeliverCode' => 'E' //send emails
        );

        $patient_create_ret = $secure_lib->createNewUser($patient_data);

        if ( $patient_create_ret['status'] == '1' ) {
            $data['secureVideoId'] = $patient_create_ret['data']->SystemUserId;
        } else {
            //display error message
            $error_msg[] = $patient_create_ret['message'];
        }
    } else {
        //update securevideo user
        $patient_data = array(
            "FullName"      => $data['userFname'] . ' ' . $data['userLname'],
            "EmailAddress"  => $usr_email,
            "TimeZoneWindowsId" => isset( $data['userTimeZone'] ) && $data['userTimeZone'] != '' ? $data['userTimeZone'] : 'UTC',
            //"SmsNumber"     => $data['userPhoneHome'],
            'HelpNumber' => '212-555-5555',
            'SystemRoles'   => 'P',
            'ShouldAutoDeliverCode' => 'E' //send emails
        );

        $patient_create_ret = $secure_lib->updateUser($data['secureVideoId'], $patient_data);

        if ( $patient_create_ret['status'] == '1' ) {
            //$data['secureVideoId'] = $patient_create_ret['data']->SystemUserId;
        } else {
            //display error message
            $error_msg[] = $patient_create_ret['message'];
        }
    }




    $ret = fastInsertUpdate( 'update', DB_PREFIX . 'users', $data, array('user_profile', 'userId', 'userEmail'), array('userId' => $_SESSION["mvdoctorVisitornUserId"]));

    if ( is_array($ret) && array_key_exists('status', $ret) && $ret['status'] == 'success' ) {
        $success_msg = "Data has been saved.";
        $_SESSION["mvdoctorVisitorUserFname"] = sanitize( $data['userFname'] );
        $_SESSION["mvdoctorVisitorUserLname"] = sanitize( $data['userLname'] );
        //$_SESSION["mvdoctorVisitorUserTitle"] = sanitize( $data['userTitle'] );
        $_SESSION["mvdoctorVisitorUserEmail"] = sanitize( $usr_email );

        $_SESSION["mvdoctorVisitorUserAddress"] = sanitize( $data['userAddress'] );
        $_SESSION["mvdoctorVisitorUserCity"] = sanitize( $data['userCity'] );
        $_SESSION["mvdoctorVisitorUserCountry"] = sanitize( $data['userCountry'] );
        $_SESSION["mvdoctorVisitorUserState"] = sanitize( $data['userState'] );
        $_SESSION["mvdoctorVisitorUserZip"] = sanitize( $data['userZip'] );
        $_SESSION['mvdoctorVisitorSecureVideoId'] = intval( $data['secureVideoId'] );

    } else {
        $error_msg[] = $ret['error'];
    }

    //get latest data
    $sql = "SELECT * FROM ". DB_PREFIX . "users WHERE userId = ? LIMIT 1";

    try {
        $result = $db->prepare( $sql );
        $result->execute( array( $_SESSION["mvdoctorVisitornUserId"] ) );

        if ( $result->rowCount() > 0 ) {
            $row = $result->fetch();
        }

    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }

}

//get current data
try {
    $sql = "SELECT * FROM ". DB_PREFIX . "users WHERE userId = ? AND (userLevel = 4 OR userLevel = 5) AND userStatus=1 LIMIT 1";
    $result = $db->prepare( $sql );
    $result->execute( array( $_SESSION["mvdoctorVisitornUserId"] ) );

    if ( $result->rowCount() > 0 ) {
        $row = $result->fetch();
        if ( $row['userPicture'] != '' ) {
            $row['userPicture'] = unserialize(base64_decode($row['userPicture']));
        }

        //echoPre($row);
    }

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}
?>
<section class="dashboard-new-section-main patient-dashbaord-section-main clearfix tab-pane fade in active" role="tabpanel" id="dasboard">
    <div class="container">
        <?php if( !empty($error_msg)):
            foreach ( $error_msg as $error ):
                ?>
                <div class="alert alert-danger alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo $error; ?>
                </div>
                <?php
            endforeach;
        endif; ?>

        <?php if($success_msg != ""): ?>
            <div class="alert alert-success alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                <?php echo $success_msg; ?>
            </div>
        <?php endif; ?>
        <form method="post" enctype="multipart/form-data" id="js-upload-form">
            <div class="col-md-8 row">
                <input type="hidden" name="user_profile" value="1" />
                <input type="hidden" name="secureVideoId" value="<?php echo $row['secureVideoId']; ?>" />
                <div class="form-group has-feedback">
                    <label for="userFname">First Name</label>
                    <input type="text" class="form-control" id="userFname" name="userFname" value="<?php echo $row['userFname'] ?>" placeholder="First Name" required="required">
                </div>
                <div class="form-group has-feedback">
                    <label for="userLname">Last Name</label>
                    <input type="text" class="form-control" id="userLname" name="userLname" value="<?php echo $row['userLname']; ?>" placeholder="Last Name" required="required">
                </div>
                <!--
                <div class="form-group has-feedback">
                    <label for="userTitle">Title</label>
                    <input type="text" class="form-control" id="userTitle" name="userTitle" value="<?php echo $row['userTitle'] ?>" placeholder="Title eg: Software Engineer">
                </div> -->
                <div class="form-group has-feedback">
                    <label for="userEmail">Email</label>
                    <input type="email" class="form-control" name="userEmail" id="userEmail" value="<?php echo $row['userEmail'] ?>" placeholder="Email" readonly="readonly" />
                </div>
                <div class="form-group has-feedback">
                    <label for="userTimeZone">Timezone</label>
                    <?php echo getTimeZoneHtml( 'userTimeZone', 'true', $row['userTimeZone'] ); ?>
                </div>
                <div class="form-group">
                    <label for="userAddress">Address</label>
                    <textarea class="form-control input-block" id="userAddress" name="userAddress" placeholder="Address" required="required"><?php echo $row['userAddress']; ?></textarea>
                </div>
                <div class="form-group has-feedback">
                    <label for="userCountry">Country</label>
                    <select class="form-control bfh-countries" name="userCountry" id="userCountry" data-country="<?php echo $row['userCountry']; ?>"></select>
                </div>
                <div class="form-group has-feedback">
                    <label for="userCity">City</label>
                    <input type="text" class="form-control" id="userCity" name="userCity" value="<?php echo $row['userCity'] ?>" placeholder="City">
                </div>
                <div class="form-group has-feedback">
                    <label for="userState">State</label>
                    <select class="form-control bfh-states" name="userState" id="userState" data-state="<?php echo $row['userState'] ?>"  data-country="userCountry"></select>
                </div>
                <div class="form-group has-feedback">
                    <label for="userZip">ZIP</label>
                    <input type="text" class="form-control" id="userZip" name="userZip" value="<?php echo $row['userZip'] ?>" placeholder="ZIP">
                </div>
                <div class="form-group has-feedback">
                    <label for="userGender">Gender</label>
                    <select name="userGender" id="userGender" class="form-control">
                        <option value="male" <?php if( $row['userGender'] == 'male' ){ echo 'selected="selected"'; } ?>>Male</option>
                        <option value="female" <?php if( $row['userGender'] == 'female' ){ echo 'selected="selected"'; } ?>>Female</option>
                    </select>
                </div>
                <div class="form-group has-feedback">
                    <label for="userDob">Date of birth</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        
                        <input type="text" id="userDob" name="userDob" value="<?php if( trim($row['userDob']) != '' ) { echo date("m/d/Y",strtotime($row['userDob'])); } ?>" data-mask="<?php echo $row['userDob'] != '' ? date("m/d/Y",strtotime($row['userDob'])) : ''; ?>" data-inputmask="'alias': 'mm/dd/yyyy'" class="form-control" required="required">
                    </div>
                    <!-- /.input group -->
                </div>

                <div class="form-group has-feedback">
                    <label for="userPhoneHome">Phone - Home</label>
                    <input type="text" class="form-control" name="userPhoneHome" id="userPhoneHome" value="<?php echo $row['userPhoneHome'] ?>" placeholder="Phone - home"  required="required" />
                </div>
                <div class="form-group has-feedback">
                    <label for="userPhoneCellular">Phone - Mobile</label>
                    <input type="text" class="form-control" name="userPhoneCellular" id="userPhoneCellular" value="<?php echo $row['userPhoneCellular'] ?>" placeholder="Phone - Mobile" />
                </div>
                <div class="form-group has-feedback">
                    <label for="userPhoneBusiness">Phone - Business</label>
                    <input type="text" class="form-control" name="userPhoneBusiness" id="userPhoneBusiness" value="<?php echo $row['userPhoneBusiness'] ?>" placeholder="Phone - Business">
                </div>
                <div class="form-group has-feedback">
                    <label for="userFax">Fax</label>
                    <input type="text" class="form-control" name="userFax" id="userFax" value="<?php echo $row['userFax'] ?>" placeholder="Fax" />
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12  pull-right drop-zone-main">
                <div class="upload-drop-zone" id="drop-zone">
                    <?php
                    $imgpath="";
                    if ($_SESSION["mvdoctorVisitorUserLevel"]=="4")
                    {
                        $imgpath = 'images/user/full/';
                    }
                    else
                    {
                        $imgpath = 'images/doctors/full/';
                    }
                    ?>
                    <?php if ( is_array( $row['userPicture'] ) ):   ?>
                        <input type="hidden" name="userPicture1" value="<?php echo $row['userPicture']['img1']; ?>" />
                        <input type="hidden" name="userPicture2" value="<?php echo $row['userPicture']['img2']; ?>" />
                        <img src="<?php echo HTTP_SERVER . $imgpath . $row['userPicture']['img1']; ?>" class="img img-responsive" />
                    <?php  else: ?>
                        Please upload a picture.
                    <?php endif; ?>
                </div>
                <div class="form-inline" style="margin-top: 15px;">
                    <input type="file" name="user_image" class="btn btn-default btn-block btn-sm" id="js-upload-submit">
                </div>
            </div>
            <div class="profile-butten notification">
                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-check"></i> Save</button>
                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-close"></i> Cancel</button>
            </div>
        </form>
    </div>
</section>

<script type="text/javascript">
    jQuery(function($){
        $('#userDob').datetimepicker({
            format: 'MM/DD/YYYY'
        });
    });
</script>