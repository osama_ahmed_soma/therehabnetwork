<?php


//deny direct access

if ( !defined('MVD_SITE') ) {

    die('You are not authorized to view this page');

}



checkUserSessionType(4);



$error_msg = array();

$success_msg = "";


 $query = "SELECT * FROM `" . DB_PREFIX . "users` WHERE userId = ?";

    $st = $db->prepare($query);

    $st->execute( array($_SESSION["mvdoctorVisitornUserId"]) );



    if ( $st->rowCount() )  {

        $row = $st->fetch();

    } else {

        $row = array();

    }

if ( isset( $_POST['change_password'] ) ) {

    $tab = "password";

    $currentPassword = sanitize($_POST["currentPassword"]);

    $newPassword = sanitize($_POST["newPassword"]);

    $confirmPassword = sanitize($_POST["confirmPassword"]);



    if($currentPassword == "" or $newPassword == "" or $confirmPassword == ""){

        $error_msg[] = "All fields are required!";
	} else if(strlen($newPassword)<8 || strlen($confirmPassword)<8){
		
		 $error_msg[] = "Password must be 8 characters long!";
		 
	} else if(!preg_match('/[A-Z]+/', $newPassword) && !preg_match('/[0-9]+/', $newPassword) && !preg_match('/[A-Z]+/', $confirmPassword) && !preg_match('/[0-9]+/', $confirmPassword)){

		
		 $error_msg[] = "Password must have one capital letter, one number!";

    } else {



        if($newPassword == $confirmPassword) {





            $hash = $row['userPassword'];

            $password_to_check = SITE_KEY . $currentPassword . $row['userKey'];

            //echo $db_password . '<br>' . $hash;

            if ( password_verify($password_to_check, $hash) ) {

                $options = [

                    'cost' => 11,

                    'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),

                ];



                $user_key = getRandomString(20);

                $new_pass = password_hash(SITE_KEY . $newPassword . $user_key, PASSWORD_DEFAULT, $options);



                try {

                    /* Store new hash in db */

                    $query = "UPDATE " . DB_PREFIX . "users set userKey = ?, userPassword = ? WHERE userId = ?";

                    $result = $db->prepare($query);

                    $result->execute(array($user_key , $new_pass, $row['userId']));

                } catch (Exception $Exception) {

                    exit("DataBase Error {$Exception->getCode()}:" . $Exception->getMessage());

                }

                $success_msg = "Your password has been changed successfully.";

                /*

                //sending email

                // additional header pieces for errors, From cc's, bcc's, etc

                $headers = "From: ". CONTACT_NAME ." <". CONTACT_EMAIL .">\r\n";

                $headers.="Content-type: text/plain; charset=iso-8859-1\r\n";



                //subject

                $subject = "Your My Virtual Doctor CMS Password Has Been Changed";



                //message

                $message = "Dear {$row['userFname']} {$row['userLname']} \n\n";

                $message .= "Your password has been changed and your new password is: \n\n";

                $message .= "$newPassword \n\n";

                $message .= "Please use the new password above to login from now on. You can always change your password after logging in to the system";



                if ( mail($row['userEmail'],$subject,$message,$headers) ) {

                    $success_msg = "Your password has been changed successfully.";

                } else {

                    $error_msg[] = "Sending email failed!";

                }

                */



            } else {

                $error_msg[] = "Your current password is incorrect. Please try again!";

            }



        } else {

            $error_msg[] = "Your New and Confirm Password fields do not match!";

        }

    }

}



?>

<section class="dashboard-new-section-main patient-dashbaord-section-main clearfix tab-pane fade in active" role="tabpanel" id="dasboard">

    <div class="container">

        <?php if( !empty($error_msg)):

            foreach ( $error_msg as $error ):

                ?>

                <div class="alert alert-danger alert-dismissible">

                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>

                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>

                    <?php echo $error; ?>

                </div>

                <?php

            endforeach;

        endif; ?>



        <?php if($success_msg != ""): ?>

            <div class="alert alert-success alert-dismissible">

                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>

                <h4><i class="icon fa fa-check"></i> Alert!</h4>

                <?php echo $success_msg; ?>

            </div>

        <?php endif; ?>



        <form class="form-horizontal" method="post">

            <input type="hidden" name="change_password" value="1">

            <div class="form-group col-md-6 no-pad">

                <label for="inputName" class="col-sm-12 control-label">Current Password</label>

                <div class="col-sm-12">

                    <input type="password" class="form-control" id="currentPassword" name="currentPassword" placeholder="Current Password">

                </div>

            </div>

            <div class="form-group col-md-6 no-pad">

                <label for="inputEmail" class="col-sm-12 control-label">New Password</label>

                <div class="col-sm-12">

                    <input type="password" class="form-control" id="newPassword" name="newPassword" placeholder="New Password">

                </div>

            </div>

            <div class="form-group col-md-12 no-pad">

                <label for="inputName" class="col-sm-12 control-label">Confirm New Password</label>

                <div class="col-sm-12">

                    <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Confirm New Password">

                </div>

            </div>

            <div class="profile-butten notification">

                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-check"></i> Save</button>

                <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-close"></i> Cancel</button>

            </div>

        </form>

    </div>

</section>