<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
checkUserSessionType(4);
?>
<section class="dashboard-new-section-main patient-dashbaord-section-main clearfix tab-pane fade in active" role="tabpanel" id="dasboard">
    <div class="container">
        <div class="profile-top boder">
            <h2 class="Notifications-email"> Emails</h2>
            <div class="checkbox checkbox-Wellness ">
                <input id="checkbox1" type="checkbox">
                <label for="checkbox1">
                    Wellness Reminders
                </label>
            </div>
            <div class="checkbox checkbox-Wellness">
                <input id="checkbox2" type="checkbox">
                <label for="checkbox2">
                    Updates on email
                </label>
            </div>
            <div class="checkbox checkbox-Wellness">
                <input id="checkbox3" type="checkbox">
                <label for="checkbox3">
                    General Fitness Program updates
                </label>
            </div>
        </div>
        <div class="profile-butten">
            <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-check"></i> Save</button>
            <button type="submit" class="btn btn-default dark-pink"><i class="fa fa-close"></i> Cancel</button>
        </div>
    </div>
</section>