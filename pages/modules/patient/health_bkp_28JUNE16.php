<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
checkUserSessionType(4);
?>
<section class="dashboard-new-section-main patient-dashbaord-section-main clearfix tab-pane fade in active" role="tabpanel" id="health">
    <div class="container">
        <div class="row admin-pro clearfix">
            <div class="col-sm-2 col-md-2 text-center">

                <div  id="upload_photo_div" class="user-display-picture">
                    <img alt="1" class="avatar" src="<?php
                    if ( isset($_SESSION["mvdoctorVisitoruserPicture"]) && $_SESSION["mvdoctorVisitoruserPicture"] != '' ) {
                        echo HTTP_SERVER . 'images/user/thumb/' . $_SESSION["mvdoctorVisitoruserPicture"];
                    } else {
                        echo get_gravatar($_SESSION["mvdoctorVisitorUserEmail"],87);
                    } ?>">

                </div>
                <div class="change-photo">

                    <a href="<?php echo HTTP_SERVER . 'patient/profile-settings.html' ?>" class="btn btn-info">Edit Profile</a>
                </div>

            </div>

            <div class="user-info col-sm-10 col-md-10">
                <div class="last-updated-info pull-right">
                    <span id="last_updated_at">Last Updated On 05/28/2016</span>
                </div>
                <div>
                    <span class="title"><?php echo $_SESSION["mvdoctorVisitorUserFname"] . ' ' . $_SESSION["mvdoctorVisitorUserLname"]; ?></span>
                </div>
                <div>
                    <span id="profile_percentage">Your profile is <strong>92%</strong> complete</span>
                </div>
                <div class="progress">
                    <div id="profile_progress_bar" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 92%;">
                        <span class="sr-only"></span>
                    </div>
                </div>
                <div>
                    <span class="notification" id="pc_condition">Please complete <strong><a data-remote="true" href="<?php echo HTTP_SERVER; ?>patient/health.html">My Health</a></strong> questions</span>
                </div>
            </div>


        </div>


        <section class="health-madication-tabs">

            <div class="row">
                <div class="nav-pills">
                    <div class="col-md-2 dns-icon active"><a href="#health-history" data-toggle="tab"><i class="fa fa-heart"></i> My Health History</a></div>
                    <div class="col-md-2 dns-icon"><a href="#providers" data-toggle="tab"><i class="fa fa-user-md"></i> My Providers</a></div>
                    <div class="col-md-2 dns-icon"><a href="#history" data-toggle="tab"><i class="fa fa-calendar"></i> Consultation History</a></div>
                    <div class="col-md-2 dns-icon"><a href="#records" data-toggle="tab"><i class="fa fa-calendar-plus-o"></i> My Records</a></div>
                </div>
            </div>

        </section>
        
        
        <!-- My Conditon Section Start -->
        
        		<?php 
					try {
						$query = "SELECT * FROM " . DB_PREFIX . "health_condtions WHERE healthConditionPatientId = ? AND `healthConditionType` = ?  order by healthConditionId DESC";
						$result = $db->prepare($query);
						$result->execute(array($_SESSION["mvdoctorVisitornUserId"], 'my_condition'));
					
						if ( $result->rowCount() > 0 ) {
							$my_condition_data = $result->fetchAll();
						}
						else
						{
							$my_condition_data = array();
						}
					
					} catch (Exception $Exception) {
						exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
					}
					?>
        <div class="tab-content">
            <div class="row haelth-section clearfix tab-pane fade in active" role="tabpanel" id="health-history">
                <div class="hs-list myhealth">
                    <div class="hs-l-main">
                        <div class="hs-l-icon"><i class="fa fa-heartbeat"></i></div>
                        <div class="hs-l-heading">My Health Conditions</div>
                        <div class="hs-l-questions quesiton_my_conditoin">
                        <?php 
						
						if(count($my_condition_data) == 0)
						{
						?>
                        Do you have any health conditions?
                        <?php 
						}
						else
						{
						  echo '&nbsp;'; 	
						}
						
						?>
                        </div>
                        <div class="hs-l-questions-checkbox">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default <?php if(count($my_condition_data) > 0){ ?>active<?php } ?>" id="healthconditions-yes">Yes
                                    <input type="radio" name="option" id="Yes" autocomplete="off" chacked>
                                </label>

                                <label class="btn btn-default <?php if(count($my_condition_data) == 0){ ?>active<?php } ?>" id="healthconditions-no">No
                                    <input type="radio" name="option" id="No" autocomplete="off">
                                </label>
                            </div>
                        </div>
                    </div>
                    
                     
                    <form id="my_condition" name="my_health_condition" method="post">
                    <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                    <div class="hs-l-questions-txt desc" id="healthconditions-div"  style="display: <?php if(count($my_condition_data) > 0){ ?>block<?php }else{ ?>none<?php } ?>;">
                        <div class="more-info select-yes" >
                            <div class="row table-container my-conditions my">
                                <div class="table" id="health_conditions_table">
                                    <div class="table-row tbl-header">
                                        <div class="table-cell">My Conditions</div>
                                        <div class="table-cell">Reported Date</div>
                                        <div class="table-cell">Source</div>
                                        <div class="table-cell"></div>
                                    </div>
                                    
                                    <?php 
									
									if(count($my_condition_data) > 0)
									{
										foreach($my_condition_data as $rs):
										
									?>
                                           <div class="table-row no-records no-records-delete" id="my_conditon_<?php echo $rs['healthConditionId'] ?>">
                                           <div class="table-cell"><?php echo $rs['healthConditionDesc'] ?></div>
                                           <div class="table-cell"><?php echo date('m/d/Y',strtotime($rs['healthCondtionAddedDate'])) ?></div>
                                           <div class="table-cell"><?php echo $rs['healthConditionSource'] ?></div>
                                           <div class="table-cell delete remove_my_condtion_model" data-login="yes" data-id="<?php echo $rs['healthConditionId']; ?>" data-type="1" style="cursor:pointer">Delete</div>
                                           </div>
                                            
                                   <?php 
								   		endforeach;
								   	}
									else
									{
										?>
                                            <div class="table-row no-records no-records-delete">
                                                <div class="table-cell">No medical conditions reported</div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell delete" style="cursor:pointer"></div>
                                                
                                              </div>
                                         
										<?php
									}
								   ?>
									
                                </div>
                            </div>
                            <div class="add-condition">
                            	
                                <div class="clearfix">
                                    <div class="col-md-10 row"> <input class="form-control input healthConditionDesc" autocomplete="off" id="healthConditionDesc" name="healthConditionDesc" placeholder="Start typing to search for health conditions (Example: Diabetes)" type="text"></div>
                                    <div class="col-md-2"> <button type="submit" id="add_health_condition" class="btn btn-info btn-block pull-right">Add Condition</button></div>
                                    
                                    
                                    <br>
                                </div>
                                <div class="message health_message" id="my_condition_error"></div>
                            </div>
                        </div>


                    </div>
                    <input type="hidden" name="healthConditionType" value="my_condition" />
                    <input type="hidden" name="healthConditionSource" value="Self Reported" />
                    <input type="hidden" name="conditonCount" id="conditonCount" value="<?php echo count($my_condition_data); ?>" />
                    </form>

                </div>

		<!-- My condtion section close -->
        
        
        <?php 
		
				try {
						$query = "SELECT * FROM " . DB_PREFIX . "health_condtions WHERE healthConditionPatientId = ? AND `healthConditionType` = ?  order by healthConditionId DESC";
						$result = $db->prepare($query);
						$result->execute(array($_SESSION["mvdoctorVisitornUserId"], 'my_medication'));
					
						if ( $result->rowCount() > 0 ) {
							$my_medication_data = $result->fetchAll();
						}
						else
						{
							$my_medication_data = array();
						}
					
					} catch (Exception $Exception) {
						exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
					}
					?>
		
		
		<form id="my_medication" name="my_health_medication" method="post">
           <input type="hidden" name="token" value="<?php echo getToken(); ?>">

                <div class="hs-list myhealth">
                    <div class="hs-l-main">
                        <div class="hs-l-icon"><i class="fa fa-medkit"></i></div>
                        <div class="hs-l-heading">My Medications</div>
                       <div class="hs-l-questions quesiton_my_medication">
                        <?php 
						
						if(count($my_medication_data) == 0)
						{
						?>
                        Do you have any health conditions?
                        <?php 
						}
						else
						{
						  echo '&nbsp;'; 	
						}
						
						?>
                        </div>
                        <div class="hs-l-questions-checkbox">
                            <div class="btn-group" data-toggle="buttons">
                               <label class="btn btn-default <?php if(count($my_medication_data) > 0){ ?>active<?php } ?>" id="medications-yes">Yes
                                    <input type="radio" name="option" id="Yes" autocomplete="off" chacked>
                                </label>

                                <label class="btn btn-default <?php if(count($my_medication_data) == 0){ ?>active<?php } ?>" id="medications-no">No
                                    <input type="radio" name="option" id="No" autocomplete="off">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="hs-l-questions-txt"  id="medications-div" style="display: <?php if(count($my_medication_data) > 0){ ?>block<?php }else{ ?>none<?php } ?>;">
                        <div class="more-info select-yes">
                            <div class="row table-container my-conditions my">
                                <div class="table" id="health_medication_table">
                                    <div class="table-row tbl-header">
                                        <div class="table-cell">My Medications</div>
                                        <div class="table-cell">Dosage</div>
                                        <div class="table-cell">Frequency</div>
                                        <div class="table-cell">Reported Date</div>
                                        <div class="table-cell">Source</div>
                                        <div class="table-cell"></div>
                                    </div>
                                    
                                    
                                    <?php 
									
									if(count($my_medication_data) > 0)
									{
										foreach($my_medication_data as $rs):
										
									?>
                                           <div class="table-row no-records no-records-delete-my-medication" id="my_medication_<?php echo $rs['healthConditionId'] ?>">
                                           <div class="table-cell"><?php echo $rs['healthConditionDesc'] ?></div>
                                           <div class="table-cell"><?php echo $rs['healthMedicationDosage'] ?></div>
                                            <div class="table-cell"><?php echo $rs['healthMedicationFrequency'] ?> - <?php echo $rs['healthMedicationHourly'] ?></div>
                                           <div class="table-cell"><?php echo date('m/d/Y',strtotime($rs['healthCondtionAddedDate'])) ?></div>
                                           <div class="table-cell"><?php echo $rs['healthConditionSource'] ?></div>
                                           <div class="table-cell delete remove_my_condtion_model" data-login="yes" data-id="<?php echo $rs['healthConditionId']; ?>" data-type="2" style="cursor:pointer">Delete</div>
                                           </div>
                                            
                                   <?php 
								   		endforeach;
								   	}
									else
									{
										?>
                                            <div class="table-row no-records no-records-delete-my-medication">
                                                <div class="table-cell">No medications reported</div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell delete" style="cursor:pointer"></div>
                                                
                                              </div>
                                         
										<?php
									}
								   ?>
									
									
                                    
                                    
                                   

                                </div>
                            </div>
                            <div class="add-condition">
                                <div class="clearfix">
                                    <div class="col-md-10 row"> <input class="form-control input healthMedicationDesc" autocomplete="off" id="healthMedicationDesc" name="healthConditionDesc"  placeholder="Start typing to search for medications (Example: Ibuprofen)" type="text"></div>
                                    <div class="col-md-2"> <button  type="submit" id="add_health_medication" class="btn btn-info btn-block pull-right">Add Medication</button></div>
                                    
                                    <div class="myhealth-more row" style="display: block;">
        <div class="col-sm-4 col-md-4">
          <label id="lbl_dosage">Dosage*</label>
          <input type="text" placeholder="1 pill" aria-labelledby="lbl_dosage" id="healthMedicationDosage" name="healthMedicationDosage">
        </div>
        <div class="col-sm-5 col-md-5">
          <label id="lbl_frequency_description">Frequency</label>
          <select name="healthMedicationFrequency" aria-label="number of doses" id="healthMedicationFrequency">
            <option value="Once">Once</option>
            <option value="Twice">Twice</option>
            <option value="Three times">Three times</option>
            <option value="Four times">Four times</option>
            <option value="Five times">Five times</option>
            <option value="Six times">Six times</option>
            <option value="As Needed">As Needed</option>
          </select>
          <select name="healthMedicationHourly" aria-describedby="lbl_frequency_description" aria-label="time period" id="healthMedicationHourly">
            <option value="Hourly">Hourly</option>
            <option value="Daily">Daily</option>
            <option value="Weekly">Weekly</option>
            <option value="Monthly">Monthly</option>
          </select>
        </div>
        
      </div>
                                    <br>
                                </div>
                                <div class="message health_message" id="my_medication_error"></div>
                            </div>
                        </div>


                    </div>

                </div>
               		<input type="hidden" name="healthConditionType" value="my_medication" />
                    <input type="hidden" name="healthConditionSource" value="Self Reported" />
                    <input type="hidden" name="medicationCount" id="medicationCount" value="<?php echo count($my_medication_data); ?>" />
			</form>
            
            
            <?php 
		
				try {
						$query = "SELECT * FROM " . DB_PREFIX . "health_condtions WHERE healthConditionPatientId = ? AND `healthConditionType` = ?  order by healthConditionId DESC";
						$result = $db->prepare($query);
						$result->execute(array($_SESSION["mvdoctorVisitornUserId"], 'my_allergy'));
					
						if ( $result->rowCount() > 0 ) {
							$my_allergy_data = $result->fetchAll();
						}
						else
						{
							$my_allergy_data = array();
						}
					
					} catch (Exception $Exception) {
						exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
					}
					?>
            
            
            <form id="my_allergy" name="my_health_allergy" method="post">
           <input type="hidden" name="token" value="<?php echo getToken(); ?>">


                <div class="hs-list myhealth">
                    <div class="hs-l-main">
                        <div class="hs-l-icon"><i class="fa fa-stethoscope"></i></div>
                        <div class="hs-l-heading">My Allergies</div>
                        <div class="hs-l-questions quesiton_my_allergy">
                        <?php 
						
						if(count($my_allergy_data) == 0)
						{
						?>
                         Do you have any Allergies or Drug Sensitivities?
                        <?php 
						}
						else
						{
						  echo '&nbsp;'; 	
						}
						
						?>
                        </div>
                        <div class="hs-l-questions-checkbox">
                         	<div class="btn-group" data-toggle="buttons">
                             <label class="btn btn-default <?php if(count($my_allergy_data) > 0){ ?>active<?php } ?>" id="allergies-yes">Yes
                                    <input type="radio" name="option" id="Yes" autocomplete="off" chacked>
                                </label>

                                <label class="btn btn-default <?php if(count($my_allergy_data) == 0){ ?>active<?php } ?>" id="allergies-no">No
                                    <input type="radio" name="option" id="No" autocomplete="off">
                                </label>
                             </div>
                        </div>
                    </div>
                    <div class="hs-l-questions-txt" id="allergies-div" style="display: <?php if(count($my_allergy_data) > 0){ ?>block<?php }else{ ?>none<?php } ?>;">
                        <div class="more-info select-yes">
                            <div class="row table-container my-conditions my">
                                <div class="table" id="health_allergy_table">
                                    <div class="table-row tbl-header">
                                        <div class="table-cell">
                                            My Allergies</div>
                                        <div class="table-cell">Severity</div>
                                        <div class="table-cell">Reaction</div>
                                        <div class="table-cell">Date</div>
                                        <div class="table-cell">Source</div>
                                    </div>
                                    <?php
                                    if(count($my_allergy_data) > 0)
									{
										foreach($my_allergy_data as $rs):
										
									?>
                                           <div class="table-row no-records no-records-delete-my-allergy" id="my_allergy_<?php echo $rs['healthConditionId'] ?>">
                                           <div class="table-cell"><?php echo $rs['healthConditionDesc'] ?></div>
                                           <div class="table-cell"><?php echo $rs['healthMedicationSeverity'] ?></div>
                                           <div class="table-cell"><?php echo $rs['healthMedicationReaction'] ?></div>
                                           <div class="table-cell"><?php echo date('m/d/Y',strtotime($rs['healthCondtionAddedDate'])) ?></div>
                                           <div class="table-cell"><?php echo $rs['healthConditionSource'] ?></div>
                                           <div class="table-cell delete remove_my_condtion_model" data-login="yes" data-id="<?php echo $rs['healthConditionId']; ?>" data-type="3" style="cursor:pointer">Delete</div>
                                           </div>
                                            
                                   <?php 
								   		endforeach;
								   	}
									else
									{
										?>
                                            <div class="table-row no-records no-records-delete-my-allergy">
                                                <div class="table-cell">No allergies reported</div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell delete" style="cursor:pointer"></div>
                                                
                                              </div>
                                         
										<?php
									}
								   ?>
                                   

                                </div>
                            </div>
                            <div class="add-condition">
                                <div class="clearfix">
                                    <div class="col-md-10 row"> <input class="form-control input" autocomplete="off" id="healthAllergyDesc" name="healthConditionDesc"  placeholder="Start typing to search for allergies (Example: Penicillin)" type="text"></div>
                                    <div class="col-md-2"> <button  type="submit" id="add_health_allergy" class="btn btn-info btn-block pull-right">Add Allergy</button></div>
                                    
                                    <div class="myhealth-more row" style="display: block;">
          <div class="col-sm-4 col-md-4">
            <label id="lbl_severity">Severity</label>
            <select name="healthMedicationSeverity" id="healthMedicationSeverity" aria-labelledby="lbl_severity">
              <option value="Unspecified">Not Sure</option>
              <option value="Mild">Mild</option>
              <option value="Moderate">Moderate</option>
              <option value="Severe">Severe</option>
            </select>
          </div>
          <div class="col-sm-5 col-md-5">
            <label id="lbl_reaction">Reaction*</label>
            <input type="text" maxlength="80" placeholder="Rashes on the skin" aria-labelledby="lbl_reaction" id="healthMedicationReaction" name="healthMedicationReaction">
<!--            <div class="message allergy_reaction_message col-md-offset-3">Can't be blank</div>-->
          </div>
          
        </div>
                                    <br>
                                </div>
                                <div class="message health_message" id="my_allergy_error"></div>
                            </div>
                        </div>


                    </div>

                </div>

					<input type="hidden" name="healthConditionType" value="my_allergy" />
                    <input type="hidden" name="healthConditionSource" value="Self Reported" />
                    <input type="hidden" name="allergyCount" id="allergyCount" value="<?php echo count($my_allergy_data); ?>" />
			</form>
            
            <?php
					try {
						$query = "SELECT * FROM " . DB_PREFIX . "health_condtions WHERE healthConditionPatientId = ? AND `healthConditionType` = ?  order by healthConditionId DESC";
						$result = $db->prepare($query);
						$result->execute(array($_SESSION["mvdoctorVisitornUserId"], 'my_surgery'));
					
						if ( $result->rowCount() > 0 ) {
							$my_surgery_data = $result->fetchAll();
						}
						else
						{
							$my_surgery_data = array();
						}
					
					} catch (Exception $Exception) {
						exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
					}
					?>
 			<form id="my_surgery" name="my_health_surgery" method="post">
          		 <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                <div class="hs-list myhealth">
                    <div class="hs-l-main">
                        <div class="hs-l-icon"><i class="fa fa-user-md"></i></div>
                        <div class="hs-l-heading">My Surgeries and Procedures</div>
                        <div class="hs-l-questions quesiton_my_surgery">
                        <?php 
						
						if(count($my_surgery_data) == 0)
						{
						?>
                         	Have you ever had any surgeries or medical procedures?
                        <?php 
						}
						else
						{
						  echo '&nbsp;'; 	
						}
						
						?>
                        </div>
                        
                        <div class="hs-l-questions-checkbox">
                            <div class="btn-group health-check" data-toggle="buttons">
                                 <label class="btn btn-default <?php if(count($my_surgery_data) > 0){ ?>active<?php } ?>" id="surgeries-yes">Yes
                                    <input type="radio" name="option" id="Yes" autocomplete="off" chacked>
                                </label>

                                <label class="btn btn-default <?php if(count($my_surgery_data) == 0){ ?>active<?php } ?>" id="surgeries-no">No
                                    <input type="radio" name="option" id="No" autocomplete="off">
                                </label>
                            </div>
                        </div>
                    </div>
                     <div class="hs-l-questions-txt"  id="surgeries-div" style="display: <?php if(count($my_surgery_data) > 0){ ?>block<?php }else{ ?>none<?php } ?>;">
                        <div class="more-info select-yes">
                            <div class="row table-container my-conditions my">
                                <div class="table" id="health_surgery_table">
                                    <div class="table-row tbl-header">
                                        <div class="table-cell">My Surgeries</div>
                                        <div class="table-cell">Reported Date</div>
                                        <div class="table-cell">Source</div>
                                        <div class="table-cell"></div>
                                    </div>
                                    <?php
                                    if(count($my_surgery_data) > 0)
									{
										foreach($my_surgery_data as $rs):
										
									?>
                                           <div class="table-row no-records no-records-delete-my-surgery" id="my_surgery_<?php echo $rs['healthConditionId'] ?>">
                                           <div class="table-cell"><?php echo $rs['healthConditionDesc'] ?></div>
                                           <div class="table-cell"><?php echo date('m/d/Y',strtotime($rs['healthCondtionAddedDate'])) ?></div>
                                           <div class="table-cell"><?php echo $rs['healthConditionSource'] ?></div>
                                           <div class="table-cell delete remove_my_condtion_model" data-login="yes" data-id="<?php echo $rs['healthConditionId']; ?>" data-type="4" style="cursor:pointer">Delete</div>
                                           </div>
                                            
                                   <?php 
								   		endforeach;
								   	}
									else
									{
										?>
                                            <div class="table-row no-records no-records-delete-my-surgery">
                                                <div class="table-cell">No surgeries reported</div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell delete" style="cursor:pointer"></div>
                                                
                                              </div>
                                         
										<?php
									}
								   ?>
                                   

                                </div>
                            </div>
                            <div class="add-condition">
                                <div class="clearfix">
                                    <div class="col-md-10 row"> <input class="form-control input" autocomplete="off" id="healthSurgeryDesc" name="healthConditionDesc"  placeholder="Start typing to search for surgeries" type="text"></div>
                                    <div class="col-md-2"> <button  type="submit" id="add_health_surgery" class="btn btn-info btn-block pull-right">Add Surgery</button></div>
                                    
                                    
                                    <br>
                                </div>
                                <div class="message health_message" id="my_surgery_error"></div>
                            </div>
                        </div>


                    </div>

                </div>
					<input type="hidden" name="healthConditionType" value="my_surgery" />
                    <input type="hidden" name="healthConditionSource" value="Self Reported" />
                    <input type="hidden" name="surgeryCount" id="surgeryCount" value="<?php echo count($my_surgery_data); ?>" />
			</form>
            </div>


            <div class="row haelth-section clearfix tab-pane fade" role="tabpanel" id="providers">
                <?php
                $query = "SELECT `doctorId` FROM `". DB_PREFIX ."favorite_doctor` WHERE userId=?";

                try {
                    $st = $db->prepare( $query );
                    $st->execute(array($_SESSION["mvdoctorVisitornUserId"]));

                    if ( $st->rowCount() > 0 ) {
                        $all_doctors = $st->fetchAll();
                    }

                } catch (Exception $Exception) {
                    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
                }

                if ( !empty( $all_doctors ) ) {
                    foreach ( $all_doctors as $all_doctor ) {
                        $doctor_id[] = $all_doctor['doctorId'];
                    }

                    $select = "SELECT doc.id, doc.url, doc.doctor_name, doc.specialization, doc.degree, doc.fees, doc.location, doc.experience,
                 doc.sex, doc.featured_image, doc.ratings, doc.ratingCount";

                    $from = " FROM `" . DB_PREFIX . "doctors` as doc";

                    $where = " WHERE doc.`status` = 'publish'";

                    $order = ' ORDER BY doc.id asc';

                    $where .= ' AND doc.id in (' . implode(',', $doctor_id) . ')';

                    $query = $select . $from . $where . $order;

                    $st = $db->prepare($query);
                    $st->execute();

                    if ( $st->rowCount() > 0 ) {
                        $search_data = $st->fetchAll();
                    } else {
                        $search_data = array();
                    }
                }

                if ( !empty($search_data) ) {
                    $i = 0;
                    //echo "<div id='searchResult'>";
                    foreach ($search_data as $data): $i++
                        ?>
                        <div class="dnsm-list-col">
                            <div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="<?php echo $data['id']; ?>" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url('');">
                            	<img src="<?php echo HTTP_SERVER . 'images/doctors/full/' . $data['featured_image']; ?>" />
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong><?php echo $data['experience']; ?> YEARS</strong></div>
                            </div>

                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li class="name"><a href="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>"><i class="fa fa-user"></i> <?php echo sanitize($data['doctor_name'], 3); ?></a></li>
                                    <li>
                                        <input type="hidden" class="rating" value="<?php echo $data['ratings']; ?>" data-readonly data-fractions="2"/>
                                        <br>
                                        <small><?php echo $data['ratings']; ?> Star based
                                            on <?php echo $data['ratingCount']; ?> Reviews
                                        </small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> <?php echo $data['specialization']; ?></li>
                                    <li><i class=" fa fa-graduation-cap"></i><?php echo $data['degree']; ?></li>
                                    <li><i class=" fa fa-dollar"></i><?php echo $data['fees']; ?></li>
                                    <li><i class=" fa fa-hospital-o"></i><?php echo $data['location']; ?></li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
                        <?php
                    endforeach;

                } else { ?>

                    <h2 class="no-results">No Provider added to favorite.</h2>

                <?php } ?>

                <div class="hs-list myhealth hidden">
                    <div class="hs-l-main-no-pad">
                        <div class="hs-l-heading">Primary Care Provider</div>
                        <div class="hs-l-questions">MDLIVE can send a report of your consultation to primary care provider for continuity of care.</div>
                        <div class="hs-l-questions-checkbox">
                            <div class="btn btn-info pull-right" id="provider-btn">Add Provider</div>
                        </div>
                    </div>
                </div>
                <div class="hs-l-questions-txt provider-btn-div hidden" style="display: none;">
                    <div class="more-info select-yes">

                        <div class="add-condition">
                            <div class="clearfix">
                                <div class="col-md-4">
                                    <label>First Name:</label>
                                    <input class="form-control input" placeholder="First Name" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>Middle Name:</label>
                                    <input class="form-control input" placeholder="Middle Name" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>Last Name:</label>
                                    <input class="form-control input" placeholder="Last Name" type="text">
                                </div>
                                <div class="col-md-6">
                                    <label>Email:</label>
                                    <input class="form-control input" placeholder="Email" type="text">
                                </div>
                                <div class="col-md-6">
                                    <label>Practice Name:</label>
                                    <input class="form-control input" placeholder="Practice Name" type="text">
                                </div>

                                <div class="col-md-6">
                                    <label>Address:</label>
                                    <input class="form-control input" placeholder="Address" type="text">
                                </div>
                                <div class="col-md-6">
                                    <label>Address (Line 2):</label>
                                    <input class="form-control input" placeholder="Address (Line 2)" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>City:</label>
                                    <input class="form-control input" placeholder="City" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>State:</label>
                                    <input class="form-control input" placeholder="State" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>Country:</label>
                                    <input class="form-control input" placeholder="Country" type="text">
                                </div>

                                <div class="col-md-4">
                                    <label>Cell:</label>
                                    <input class="form-control input" placeholder="Cell" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>Preferred Phone:</label>
                                    <input class="form-control input" placeholder="Preferred Phone" type="text">
                                </div>
                                <div class="col-md-4">
                                    <label>Fax:</label>
                                    <input class="form-control input" placeholder="Fax" type="text">
                                </div>

                                <div class="col-md-2 "> <button type="button" id="add_health_condition" class="btn btn-info btn-block pull-right">Add</button></div>

                            </div>

                        </div>
                    </div>


                </div>

            </div>
            <div class="row haelth-section clearfix tab-pane fade" role="tabpanel" id="history">
                <div class="">
                    <div class="col-md-12 text-center"><i class="fa fa-calendar"></i></div>
                    <div class="col-md-12 text-center">No Previous visit</div>
                    <div class="col-md-12 text-center">You will see your visit history here</div>
                </div>

            </div>

            <div class="row haelth-section clearfix tab-pane fade" role="tabpanel" id="records">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Upload files</strong> <small> </small></div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="document-name">Name of Document:</label>
                                <input type="text" class="form-control" id="document-name" placeholder="">
                            </div>
                            <div class="input-group image-preview">
                                <input placeholder="" type="text" class="form-control image-preview-filename" disabled="disabled">
                                <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;"> <span class="glyphicon glyphicon-remove"></span> Clear </button>
                                    <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input"> <span class="glyphicon glyphicon-folder-open"></span> <span class="image-preview-input-title">Browse</span>
                                    <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/>
                                    <!-- rename it -->
                                </div>
                                <button type="button" class="btn btn-labeled btn-default"> <span class="btn-label"><i class="glyphicon glyphicon-upload"></i> </span>Upload</button>
                                </span> </div>
                            <!-- /input-group image-preview [TO HERE]-->

                            <br />

                            <!-- Drop Zone -->
                            <div class="upload-drop-zone" id="drop-zone"> Or drag and drop files here </div>
                            <br />
                            <!-- Progress Bar -->
                            <div class="progress" id="status1">
                               <!-- <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"> <span class="sr-only">60% Complete</span> </div>-->
                            </div>
                            <br />
                            <!-- Upload Finished -->
                            <div class="js-upload-finished">
                                <h4>Upload history</h4>
                                <div class="list-group"> <a href="#" class="list-group-item list-group-item-danger"><span class="badge alert-danger pull-right">23-11-2014</span>amended-catalogue-01.xls</a> <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">23-11-2014</span>amended-catalogue-01.xls</a> <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">23-11-2014</span>amended-catalogue-01.xls</a> <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">23-11-2014</span>amended-catalogue.xls</a> </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>


    </div>
</section>

<!-- Modal 2-->
<div class="modal fade" id="myModalFav" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;" id="favoriteModal"></div>
        </div>
    </div>
</div>

<!-- Modal 3 -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModalFav">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-info" role="alert" id="removeModalFavContent"><strong>Warning!</strong> Are you sure you want to remove this doctor from favorites ?</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default removeFavorite">Remove</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Modal 3 -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModalCon">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-info" role="alert" id="removeModalConContent"><strong>Warning!</strong> Are you sure you want to remove this condition?</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default remove_my_condtion">Remove</button>
                <button type="button" class="btn btn-default closediv" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    jQuery(function($) {

        $('.nav-pills div a').click(function (e) {
            $('.nav-pills div.active').removeClass('active')
            $(this).parent('div').addClass('active')
        });


        $('#provider-btn').click(function() {
            $('.provider-btn-div').css('display','block');
        });

        $('#healthconditions-yes').click(function() {
            $('#healthconditions-div').css('display','block');
        });
        $('#healthconditions-no').click(function() {
            $('#healthconditions-div').css('display','none');
        });

        $('#medications-yes').click(function() {
            $('#medications-div').css('display','block');
        });
		 $('#medications-no').click(function() {
            $('#medications-div').css('display','none');
        });
      /*  $('#healthconditions-no').click(function() {
            $('#medications-div').css('display','none');
        });*/

        $('#allergies-yes').click(function() {
            $('#allergies-div').css('display','block');
        });
        $('#allergies-no').click(function() {
            $('#allergies-div').css('display','none');
        });

        $('#surgeries-yes').click(function() {
            $('#surgeries-div').css('display','block');
        });
        $('#surgeries-no').click(function() {
            $('#surgeries-div').css('display','none');
        });

        $('body').on('click', '.consult_now', function(){
            //
            $('#company_url').html( $(this).data('url') );
            $('#modal_image').html('<img src="'+ $(this).data('src') +'" style="height: 56px; text-align: center;">')

        });
		
		
		var login, doctor_id;

        $('body').on('click', '.removeFavorite', function(){
            var th = $(this);

            if ( login == 'yes' && doctor_id > 0 ) {

                $('#removeModalFavContent').html('Please wait...');
                //call ajax
                th.attr('readonly', 'readonly');
                $.ajax({
                    url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=remove_doctor_favorite",
                    data: {doctor_id: doctor_id, token: '<?php echo getToken(); ?>'},
                    method: "POST",
                    dataType: "json"
                }).error(function (err) {
                    th.removeAttr('readonly');
                    console.log(err);

                }).done(function () {
                    //

                }).success(function (data) {

                    if (data.success) {
                        //th.parent().parent().remove();
                        var url = '<?php echo HTTP_SERVER . 'patient/providers.html'; ?>';
                        window.location = url;
                    }
                    if ( data.error ) {
                        $('#favoriteModal').html('<div class="alert alert-danger" role="alert">'+data.error+'</div>');
                        th.removeAttr('readonly');
                        $('#myModalFav').modal();
                    }

                }, 'json');

            } else {
                $('#removeModalFavContent').html('<strong>Warning!</strong> Are you sure you want to remove this doctor from favorite list ?');
                $('#favoriteModal').html('<div class="alert alert-danger" role="alert">Please login to remove doctor from favorite.</div>');
                $('#myModalFav').modal();
            }
        });

        //removeFavoriteModal
        $('body').on('click', '.removeFavoriteModal', function(){
            login = $(this).data('login');
            doctor_id = $(this).data('id');
            $('#removeModalFav').modal();
        });

       

       

    });
	
	
	
	
	

    jQuery(function($){

        //For My Conditon
        $('#my_condition').submit(function() {
			
            var th = $(this);

            //validate form
            var healthCondition = jQuery.trim( $('#healthConditionDesc').val() );
			var count = parseInt(document.getElementById("conditonCount").value);
            

            var errorStr = '';

            if ( healthCondition == '' ) {
                errorStr += '<p><strong>Condition Name</strong> not found. Check spelling or click Add.<p>';
            }

            if ( errorStr != '' ) {
                var str = '<div class="alert alert-danger" role="alert">' + errorStr + '</div>';
                $('#my_condition_error').html(str);
				setTimeout(function(){ $('#my_condition_error').html(''); }, 1000);
                return false;
            } else {
                $('#my_condition_error').html('');
            }


            var data = $( "#my_condition" ).serializeArray();

            th.find('button').attr('disabled', true);

            $.ajax({

                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=health_patient",
                data: data,
                method:"POST",
                dataType:"json"

            }).error(function () {

                th.find('button').removeAttr('disabled');

            }).done(function () {

                th.find('button').removeAttr('disabled');

            }).success(function (data) {
                //data = JQuery.parseJSON(data);
                if ( data.success ) {
					$('#healthConditionDesc').val('');
					
					count++;
					document.getElementById("conditonCount").value = count;
					$('.quesiton_my_conditoin').html('&nbsp;');
					//alert(data.success);
					$('.no-records-delete').remove();
					$('#health_conditions_table').append(data.success);
					
					if(count<=0)
					{
						$('.quesiton_my_conditoin').html('Do you have any health conditions?');
						
					}
                }
				

                if ( data.error ) {
                    var str = '<div class="alert alert-danger alert-dismissible" role="alert">';
                    $.each(data.error, function(idx, error){
                        str +=  error;
                    });
                    str += '</div>';
                    $('#my_condition_error').html(str);
					setTimeout(function(){ $('#my_condition_error').html(''); }, 1000);
                }

            }, 'json');

            return false;
        });
        //For My Medication
        $('#my_medication').submit(function() {
			
			
			
            var th = $(this);

            //validate form
            var healthCondition = jQuery.trim( $('#healthMedicationDesc').val() );
			var healthMedicationDosage = jQuery.trim( $('#healthMedicationDosage').val() );
			var count = parseInt(document.getElementById("medicationCount").value);
            

            var errorStr = '';

            if ( healthCondition == '' ) {
                errorStr += '<p><strong>Medication Name</strong> not found. Check spelling or click Add.<p>';
            }
			else  if ( healthMedicationDosage  == '' ) {
                errorStr += '<p>Dosage field can not be empty<p>';
            }

            if ( errorStr != '' ) {
                var str = '<div class="alert alert-danger" role="alert">' + errorStr + '</div>';
                $('#my_medication_error').html(str);
				setTimeout(function(){ $('#my_medication_error').html(''); }, 1000);
                return false;
            } else {
                $('#my_medication_error').html('');
            }


            var data = $( "#my_medication" ).serializeArray();

            th.find('button').attr('disabled', true);

            $.ajax({

                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=health_patient",
                data: data,
                method:"POST",
                dataType:"json"

            }).error(function () {

                th.find('button').removeAttr('disabled');

            }).done(function () {

                th.find('button').removeAttr('disabled');

            }).success(function (data) {
                //data = JQuery.parseJSON(data);
                if ( data.success ) {
					$('#healthMedicationDesc').val('');
					//alert(data.success)
					
					count++;
					document.getElementById("medicationCount").value = count;
					$('.quesiton_my_medication').html('&nbsp;');
					//alert(data.success);
					$('.no-records-delete-my-medication').remove();
					$('#health_medication_table').append(data.success);
					
					if(count<=0)
					{
						$('.quesiton_my_medication').html('Are you currently taking any medication?');
						
					}
                }
				

                if ( data.error ) {
                    var str = '<div class="alert alert-danger alert-dismissible" role="alert">';
                    $.each(data.error, function(idx, error){
                        str +=  error;
                    });
                    str += '</div>';
                    $('#my_medication_error').html(str);
					setTimeout(function(){ $('#my_medication_error').html(''); }, 1000);
                }

            }, 'json');

            return false;
        });
		 //For My Alergy
		
		$('#my_allergy').submit(function() {
			
			
			
            var th = $(this);

            //validate form
            var healthCondition = jQuery.trim( $('#healthAllergyDesc').val() );
			var healthMedicationReaction = jQuery.trim( $('#healthMedicationReaction').val() );
			var count = parseInt(document.getElementById("allergyCount").value);
            

            var errorStr = '';

            if ( healthCondition == '' ) {
                errorStr += '<p><strong>Allergy</strong> not found. Check spelling or click Add.<p>';
            }
			else  if ( healthMedicationReaction == '' ) {
                errorStr += '<p>Reaction field can not be empty<p>';
            }

            if ( errorStr != '' ) {
                var str = '<div class="alert alert-danger" role="alert">' + errorStr + '</div>';
                $('#my_allergy_error').html(str);
				setTimeout(function(){ $('#my_allergy_error').html(''); }, 1000);
                return false;
            } else {
                $('#my_allergy_error').html('');
            }


            var data = $( "#my_allergy" ).serializeArray();

            th.find('button').attr('disabled', true);

            $.ajax({

                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=health_patient",
                data: data,
                method:"POST",
                dataType:"json"

            }).error(function () {

                th.find('button').removeAttr('disabled');

            }).done(function () {

                th.find('button').removeAttr('disabled');

            }).success(function (data) {
                //data = JQuery.parseJSON(data);
                if ( data.success ) {
					//alert(data.success)
					$('#healthAllergyDesc').val('');
					count++;
					document.getElementById("allergyCount").value = count;
					$('.quesiton_my_allergy').html('&nbsp;');
					//alert(data.success);
					$('.no-records-delete-my-allergy').remove();
					$('#health_allergy_table').append(data.success);
					
					if(count<=0)
					{
						$('.quesiton_my_allergy').html('Are you currently taking any medication?');
						
					}
                }
				

                if ( data.error ) {
                    var str = '<div class="alert alert-danger alert-dismissible" role="alert">';
                    $.each(data.error, function(idx, error){
                        str +=  error;
                    });
                    str += '</div>';
                    $('#my_allergy_error').html(str);
					setTimeout(function(){ $('#my_allergy_error').html(''); }, 1000);
                }

            }, 'json');

            return false;
        });
		
		$('#my_surgery').submit(function() {
			
			
			
			
			
            var th = $(this);

            //validate form
            var healthCondition = jQuery.trim( $('#healthSurgeryDesc').val() );
			var count = parseInt(document.getElementById("surgeryCount").value);
            

            var errorStr = '';

            if ( healthCondition == '' ) {
                errorStr += '<p><strong>Surgery</strong> not found. Check spelling or click Add.<p>';
            }
			

            if ( errorStr != '' ) {
                var str = '<div class="alert alert-danger" role="alert">' + errorStr + '</div>';
                $('#my_surgery_error').html(str);
				setTimeout(function(){ $('#my_surgery_error').html(''); }, 1000);
                return false;
            } else {
                $('#my_surgery_error').html('');
            }


            var data = $( "#my_surgery" ).serializeArray();

            th.find('button').attr('disabled', true);

            $.ajax({

                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=health_patient",
                data: data,
                method:"POST",
                dataType:"json"

            }).error(function () {

                th.find('button').removeAttr('disabled');

            }).done(function () {

                th.find('button').removeAttr('disabled');

            }).success(function (data) {
                //data = JQuery.parseJSON(data);
                if ( data.success ) {
					//alert(data.success)
					$('#healthSurgeryDesc').val('');
					count++;
					document.getElementById("surgeryCount").value = count;
					$('.quesiton_my_surgery').html('&nbsp;');
					//alert(data.success);
					$('.no-records-delete-my-surgery').remove();
					$('#health_surgery_table').append(data.success);
					
					if(count<=0)
					{
						$('.quesiton_my_surgery').html('Are you currently taking any medication?');
						
					}
                }
				

                if ( data.error ) {
                    var str = '<div class="alert alert-danger alert-dismissible" role="alert">';
                    $.each(data.error, function(idx, error){
                        str +=  error;
                    });
                    str += '</div>';
                    $('#my_surgery_error').html(str);
					setTimeout(function(){ $('#my_surgery_error').html(''); }, 1000);
                }

            }, 'json');

            return false;
        });
    
	
	
	// var login, post_id;
	 
        var login, post_id,type;
        $('body').on('click', '.remove_my_condtion', function(){
			
            var th = $(this);
			if(type==1)
			{
				var count = parseInt(document.getElementById("conditonCount").value);
			}
			else if(type==2)
			{
				var count = parseInt(document.getElementById("medicationCount").value);
			}
			else if(type==3)
			{
				var count = parseInt(document.getElementById("allergyCount").value);
			}
			else if(type==4)
			{
				var count = parseInt(document.getElementById("surgeryCount").value);
			}
			
            //var login = $(this).data('login');
            //var post_id = $(this).data('id');

            if ( login == 'yes' && post_id > 0 ) {

                $('#removeModalConContent').html('Please wait...');
                //call ajax
                th.attr('readonly', 'readonly');
                $.ajax({
                    url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=remove_health_patient",
                    data: {healthConditionId: post_id, token: '<?php echo getToken(); ?>'},
                    method: "POST",
                    dataType: "json"
                }).error(function (err) {
                    th.removeAttr('readonly');
                    console.log(err);

                }).done(function () {
                    //

                }).success(function (data) {
					//alert(data.success);

                    if (data.success) {
                        
						$('#healthMedicationDesc').val('');
						//document.getElementById("conditonCount").value = count;
						
						if(type==1)
						{
							$("#my_conditon_"+post_id).hide();
							count--;
							document.getElementById("conditonCount").value = count;
						}
						else if(type==2)
						{
							//alert(post_id);
							$("#my_medication_"+post_id).hide();
							count--;
							document.getElementById("medicationCount").value = count;
						}
						else if(type==3)
						{
							$("#my_allergy_"+post_id).hide();
							count--;
							document.getElementById("allergyCount").value = count;
						}
						else if(type==4)
						{
							$("#my_surgery_"+post_id).hide();
							count--;
							document.getElementById("surgeryCount").value = count;
						}
						$('.closediv').click();
						var html = '';
						if(count<=0)
						{
							
							
						if(type==1)
						{
							html = '<div class="table-row no-records no-records-delete"><div class="table-cell">No medical conditions reported</div><div class="table-cell"></div><div class="table-cell"></div><div class="table-cell delete" style="cursor:pointer"></div></div>';
							$('.no-records-delete').remove();
							$('.quesiton_my_conditoin').html('Do you have any health conditions?');
							$('#health_conditions_table').append(html);
						}
						else if(type==2)
						{
							
							html = '<div class="table-row no-records no-records-delete-my-medication"><div class="table-cell">No medications reported</div><div class="table-cell"></div><div class="table-cell"><div class="table-cell"></div><div class="table-cell"></div><div class="table-cell delete" style="cursor:pointer"></div></div>';
							
							$('.quesiton_my_medication').html('Are you currently taking any medication?');
							//alert(data.success);
							$('.no-records-delete-my-medication').remove();
							$('#health_medication_table').append(html);
						}
						else if(type==3)
						{
							html = '<div class="table-row no-records no-records-delete-my-medication"><div class="table-cell">No allergies reported</div><div class="table-cell"></div><div class="table-cell"></div><div class="table-cell delete" style="cursor:pointer"></div></div>';
							
							$('.quesiton_my_allergy').html('Do you have any Allergies or Drug Sensitivities?');
							//alert(data.success);
							$('.no-records-delete-my-allergy').remove();
							$('#health_allergy_table').append(html);
						}
						else if(type==4)
						{
							html = '<div class="table-row no-records no-records-delete-my-surgery"><div class="table-cell">No surgeries reported</div><div class="table-cell"></div><div class="table-cell"></div><div class="table-cell delete" style="cursor:pointer"></div></div>';
							
							$('.quesiton_my_surgery').html('Have you ever had any surgeries or medical procedures?');
							//alert(data.success);
							$('.no-records-delete-my-surgery').remove();
							$('#health_surgery_table').append(html);
						}
							
							
							
							
							
							
						}
                    }
                    if ( data.error ) {
                        $('#favoriteModal').html('<div class="alert alert-danger" role="alert">'+data.error+'</div>');
                        th.removeAttr('readonly');
                        $('#myModalFav').modal();
                    }

                }, 'json');

            } else {
               
                $('#removeModalCon').modal();
            }
        });

        //removeFavoriteModal
        $('body').on('click', '.remove_my_condtion_model', function(){
            login = $(this).data('login');
            post_id = $(this).data('id');
			type = $(this).data('type');
			
			if(type==1)
			{
				$('#removeModalConContent').html('<strong>Warning!</strong> Are you sure you want to remove this condition?');
			}
			else if(type==2)
			{
				$('#removeModalConContent').html('<strong>Warning!</strong> Are you sure you want to remove this condition?');
			}
			else if(type==3)
			{
				$('#removeModalConContent').html('<strong>Warning!</strong> Are you sure you want to remove this condition?');
			}
			else if(type==4)
			{
				$('#removeModalConContent').html('<strong>Warning!</strong> Are you sure you want to remove this condition?');
			}
			
            $('#removeModalCon').modal();
        });

    
	
		
	$(".healthMedicationDesc").autocomplete({
		source: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=drug_search",
		minLength: 1
	});	
	
	
	
	});
	
	
	
	
/*jQuery(function($) {*/
function sendFileToServer(formData,status)
{
    var uploadURL ="<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=file_upload"; //Upload URL
    var extraData ={}; //Extra Data.
    var jqXHR=$.ajax({
            xhr: function() {
            var xhrobj = $.ajaxSettings.xhr();
            if (xhrobj.upload) {
                    xhrobj.upload.addEventListener('progress', function(event) {
                        var percent = 0;
                        var position = event.loaded || event.position;
                        var total = event.total;
                        if (event.lengthComputable) {
                            percent = Math.ceil(position / total * 100);
                        }
                        //Set progress
                        status.setProgress(percent);
                    }, false);
                }
            return xhrobj;
        },
    url: uploadURL,
    type: "POST",
    contentType:false,
    processData: false,
        cache: false,
        data: formData,
        success: function(data){
		alert(data);
            status.setProgress(100);
 
            $("#status1").append("File upload Done<br>");         
        }
    }); 
 
    status.setAbort(jqXHR);
}
 
var rowCount=0;
function createStatusbar(obj)
{
     rowCount++;
     var row="odd";
     if(rowCount %2 ==0) row ="even";
     this.statusbar = $("<div class='statusbar "+row+"'></div>");
     this.filename = $("<div class='filename'></div>").appendTo(this.statusbar);
     this.size = $("<div class='filesize'></div>").appendTo(this.statusbar);
     this.progressBar = $("<div class='progressBar'><div></div></div>").appendTo(this.statusbar);
     this.abort = $("<div class='abort'>Abort</div>").appendTo(this.statusbar);
     obj.after(this.statusbar);
 
    this.setFileNameSize = function(name,size)
    {
        var sizeStr="";
        var sizeKB = size/1024;
        if(parseInt(sizeKB) > 1024)
        {
            var sizeMB = sizeKB/1024;
            sizeStr = sizeMB.toFixed(2)+" MB";
        }
        else
        {
            sizeStr = sizeKB.toFixed(2)+" KB";
        }
 
        this.filename.html(name);
        this.size.html(sizeStr);
    }
    this.setProgress = function(progress)
    {       
        var progressBarWidth =progress*this.progressBar.width()/ 100;  
        this.progressBar.find('div').animate({ width: progressBarWidth }, 10).html(progress + "% ");
        if(parseInt(progress) >= 100)
        {
            this.abort.hide();
        }
    }
    this.setAbort = function(jqxhr)
    {
        var sb = this.statusbar;
        this.abort.click(function()
        {
            jqxhr.abort();
            sb.hide();
        });
    }
}
function handleFileUpload(files,obj)
{
	alert(files[1]);
	alert(files[2]);
	alert(files[3]);
	alert(files[4]);
	alert(files[5]);
   for (var i = 0; i < files.length; i++) 
   {
        var fd = new FormData();
        fd.append('file', files[i]);
 
        var status = new createStatusbar(obj); //Using this we can set progress.
        status.setFileNameSize(files[i].name,files[i].size);
        sendFileToServer(fd,status);
 
   }
}
$(document).ready(function()
{
var obj = $("#drop-zone");
obj.on('dragenter', function (e) 
{
    e.stopPropagation();
    e.preventDefault();
    $(this).css('border', '2px solid #0B85A1');
});
obj.on('dragover', function (e) 
{
     e.stopPropagation();
     e.preventDefault();
});
obj.on('drop', function (e) 
{
 
     $(this).css('border', '2px dotted #0B85A1');
     e.preventDefault();
     var files = e.originalEvent.dataTransfer.files;
 
     //We need to send dropped files to Server
     handleFileUpload(files,obj);
});
$(document).on('dragenter', function (e) 
{
    e.stopPropagation();
    e.preventDefault();
});
$(document).on('dragover', function (e) 
{
  e.stopPropagation();
  e.preventDefault();
  obj.css('border', '2px dotted #0B85A1');
});
$(document).on('drop', function (e) 
{
    e.stopPropagation();
    e.preventDefault();
});
 
});


/*});*/
	
	

</script>

<style>
#dragandrophandler
{
border:2px dotted #0B85A1;
width:400px;
color:#92AAB0;
text-align:left;vertical-align:middle;
padding:10px 10px 10 10px;
margin-bottom:10px;
font-size:200%;
}
.progressBar {
    width: 200px;
    height: 22px;
    border: 1px solid #ddd;
    border-radius: 5px; 
    overflow: hidden;
    display:inline-block;
    margin:0px 10px 5px 5px;
    vertical-align:top;
}
 
.progressBar div {
    height: 100%;
    color: #fff;
    text-align: right;
    line-height: 22px; /* same as #progressBar height if we want text middle aligned */
    width: 0;
    background-color: #0ba1b5; border-radius: 3px; 
}
.statusbar
{
    border-top:1px solid #A9CCD1;
    min-height:25px;
    width:700px;
    padding:10px 10px 0px 10px;
    vertical-align:top;
}
.statusbar:nth-child(odd){
    background:#EBEFF0;
}
.filename
{
display:inline-block;
vertical-align:top;
width:250px;
}
.filesize
{
display:inline-block;
vertical-align:top;
color:#30693D;
width:100px;
margin-left:10px;
margin-right:5px;
}
.abort{
    background-color:#A8352F;
    -moz-border-radius:4px;
    -webkit-border-radius:4px;
    border-radius:4px;display:inline-block;
    color:#fff;
    font-family:arial;font-size:13px;font-weight:normal;
    padding:4px 15px;
    cursor:pointer;
    vertical-align:top
    }

</style>
