<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
checkUserSessionType(4);
?>
<section class="dashboard-new-section-main patient-blog-section clearfix tab-pane fade in active" role="tabpanel" id="Pinned">
    <div class="container">
        No contents are available now...
    </div>
</section>
