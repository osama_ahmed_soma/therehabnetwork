<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

unset( $_POST['token'] );

$result['success'] = 'yes';

$data = array_map_deep($_POST, 'stripslashes');

$category = isset( $data['category'] ) ? $data['category'] : array();

//$select = "SELECT doc.id, doc.url, doc.doctor_name, doc.specialization, doc.degree, doc.fees, doc.location, doc.experience, doc.sex, doc.featured_image, doc.ratings, doc.ratingCount, cat.categoryImage, cat.categoryName, cat.categoryDesc";
$select = "SELECT doc.id, doc.url, doc.doctor_name, doc.specialization, doc.degree, doc.fees, doc.location, doc.experience, doc.sex, doc.featured_image, doc.ratings, doc.ratingCount";

//$from = " FROM `" . DB_PREFIX . "doctors` as doc, `" . DB_PREFIX . "category` as cat, `" . DB_PREFIX . "category_data` as cat_data";
$from = " FROM `" . DB_PREFIX . "doctors` as doc";

//$where = " WHERE cat.id = cat_data.categoryId AND doc.id = cat_data.postId AND cat.categoryType='company' AND doc.`status` = 'publish'";
$where = " WHERE doc.`status` = 'publish'";


$arr = array();

//category filter
if ( is_array($category) && !empty($category) ) {

    foreach ($category as $cat_name => $cat_ids) {

        if (in_array('all', $cat_ids) || (array_key_exists('0', $cat_ids) && $cat_ids[0] == '' ) ) {
            continue;
        }

        $arr[] = $cat_name;

        $sql = "(";
        $cnt = count($cat_ids) - 1;
        for ($idx = 0; $idx <= $cnt; $idx++) {
            $sql .= "?";
            if ($idx < $cnt) {
                $sql .= ', ';
            }
            $arr[] = $cat_ids[$idx];
        }
        $sql .= ')';

        $where .= " AND exists (select * from `" . DB_PREFIX . "category_data` as c where c.postId=doc.id AND c.categoryType = ? AND c.categoryId IN $sql)";
    }
}

//insurance
$insurance = isset( $data['insurance'] ) ? $data['insurance'] : array();

if ( is_array($insurance) && !empty( $insurance ) ) {

    if ( in_array('all', $insurance) ) {
        //do nothing, ie select all insurance
    } else {

        if ( in_array('1', $insurance) && in_array('2', $insurance) ) {
            //do nothing, ie select all insurance
        } else {
            $where .= " AND doc.insurance = ?";
            $arr[] = $insurance[0];
        }

    }
}

//gender
$gender = isset( $data['gender'] ) ? $data['gender'] : array();

if ( is_array($gender) && !empty( $gender ) ) {

    if ( in_array('all', $gender) ) {
        //do nothing, ie select all insurance
    } else {

        if ( in_array('male', $gender) && in_array('female', $gender) ) {
            //do nothing, ie select all insurance
        } else {
            $where .= " AND doc.sex = ?";
            $arr[] = $gender[0];
        }

    }
}

//experience
$experience = isset( $data['experience'] ) ? $data['experience'] : '';

if ( $experience != '' ) {
    $experience = explode(',' , $experience);

    if ( is_array($experience) && count($experience) == 2 ) {
        //sort
        sort($experience, SORT_NUMERIC);
        $where .= " AND (doc.experience >= ? AND doc.experience <= ?)";
        $arr[] = $experience[0];
        $arr[] = $experience[1];

    }
}

//ratings
$ratings = isset( $data['ratings'] ) ? $data['ratings'] : 0;
if ( $ratings > 0 ) {
    $where .= ' AND doc.ratings >= ?';
    $arr[] = $ratings;
}



//search by doctor name
$searchName = isset( $data['searchName'] ) ? $data['searchName'] : '';

if ( $searchName != '' ) {
    $searchName = '%' . $searchName . '%';
    $where .= ' AND doc.doctor_name like ?';
    $arr[] = $searchName;
}

$order = " ORDER BY doc.id asc";

//pagination term

//get total row count
$query2 = "SELECT count(doc.id) as cnt" . $from . $where . $order;


try {
    $st = $db->prepare( $query2 );

    if ( !empty($arr) ) {
        $st->execute( $arr );
    } else {
        $st->execute();
    }

    $total_count = $st->fetch();
    $total_count = $total_count['cnt'];

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}



$limit_count = 8;
$page_now = isset($_POST['page_now']) ? intval($_POST['page_now']) : 0;

$start = $page_now ? ( $page_now * $limit_count ) : 0;
$end = ( $page_now * $limit_count ) + $limit_count;

$limit = " LIMIT $limit_count OFFSET $start";

//$result['total_count'] = $total_count;
//$result['start'] = $start;
//$result['end'] = $end;

if ( $total_count < $start ) {
    $result['message_end'] = "All results are loaded.";
    $result['page_now'] = 0;

} else {

    $query = $select . $from . $where . $order . $limit;

    $result['query'] = $query;
    $result['values'] = $arr;

    try {

        $st = $db->prepare( $query );

        if ( !empty($arr) ) {
            $st->execute( $arr );
        } else {
            $st->execute( );
        }

        if ( $st->rowCount() > 0 ) {
            $result['search_result'] = $st->fetchAll();
        } else {
            if ( $page_now > 0 ) {
                $result['message_end'] = "All results are loaded.";
                $result['page_now'] = 0;
            } else {
                $result['message'] = "No Results Found...";
            }

        }
    } catch (Exception $Exception) {
        $result['error'] =  "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( );
    }
}

$result['page_now'] = ++$page_now;

header('Content-Type: application/json');
echo json_encode($result);
exit;
?>