<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

$ret = array();
$error = false;

//check user is logged in or not
if( !isset($_SESSION["mvdoctorVisitornUserId"]) ) {
    $ret['error'][] = 'Please login to first to view this resource.';

    header('Content-Type: application/json');
    echo json_encode($ret);
    exit;
}

$task = isset($_REQUEST['task']) ? strip_tags($_REQUEST['task']) : '';

switch ( $task ) {
    case 'add':
        $doctorId = $_SESSION["mvdoctorID"]; //doctorId
        $dayId = isset($_POST['dayId']) ? intval($_POST['dayId']) : '';
        $startTime = isset($_POST['startTime']) ? trim( strip_tags( $_POST['startTime'] ) ) : '';
        $duration = isset($_POST['duration']) ? intval($_POST['duration']) : '';

        //validate user inputes
        if ( $doctorId == '' ) {
            $ret['error'][] = 'Please login to add timing.';
        }
        if ( $dayId == '' ) {
            $ret['error'][] = "Please select a day from list.";
        }
        if ( $duration == '' ) {
            $ret['error'][] = "Please select schedule duration.";
        } elseif ( !in_array($duration, array(1,2)) ) {
            $ret['error'][] = "Invalid schedule duration selected.";
        }

        if ( $startTime == '' ) {
            $ret['error'][] = 'Please enter a startTime.';
        } else { //check start time and end time for database
            //1. convert to timestamp
            $duration_in_minutes = ($duration == 1) ? 15 : 30;
            $time_array = explode(' ', $startTime);
            $time = $time_array[0] . ':00' . ' ' . $time_array[1];
            $time_explode = explode(':', $time);
            if (!in_array($time_explode[1], array('00', '15', '30', '45'))) {
                $ret['error'][] = 'Only available minute will be 0, 15, 30, or 45';
                $error = true;
            }
        }

        //check db for time
        if ( !array_key_exists('error', $ret) ) {
            $start_timestamp = strtotime('2000-01-01 ' . $time);
            $end_timestamp = strtotime('2000-01-01 ' . $time . ' + ' . $duration_in_minutes . ' minutes' );

            //2. check database for valid start and end time
            try {
                $query = "SELECT * FROM `" . DB_PREFIX . "doctor_timing` WHERE doctorId = ? AND dayId = ?";
                $st = $db->prepare($query);
                $st->execute(array($doctorId, $dayId));
            } catch ( Exception $ex ) {
                $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
            }
            if ( $st->rowCount() ) {
                $timing_datas = $st->fetchAll();
                $error = false;
                //check start time is ok
                foreach ( $timing_datas as $timing_data ) {
                    if ( $start_timestamp >= $timing_data['startTime'] && $start_timestamp < $timing_data['endTime'] ) {
                        $ret['error'][] = "Selected time frame is not available. " . date('h:i:s A', $timing_data['startTime']) . " to " . date('h:i:s A', $timing_data['endTime']) .
                            " time slot is already in use. Please select another time slot.";
                        $error = true;
                        break;
                    }
                }

                //check end time is ok
                if ( $error == false ) {
                    foreach ( $timing_datas as $timing_data ) {
                        if ( $end_timestamp > $timing_data['startTime'] && $end_timestamp <= $timing_data['endTime'] ) {
                            $ret['error'][] = "Selected time frame is not available. " . date('h:i:s A', $timing_data['startTime']) . " to " . date('h:i:s A', $timing_data['endTime']) .
                                " time slot is already in use. Please select another time slot.";
                            $error = true;
                            break;
                        }
                    }
                }
            }

        }

        //check plan price added or not
        if ( !array_key_exists('error', $ret) ) {
            try {
                $query = "SELECT * FROM `" . DB_PREFIX . "plans` WHERE doctorId = ? AND (`time` = 1 or `time` = 2 ) AND price > 0 ";
                $st = $db->prepare($query);
                $st->execute(array($doctorId));

                if ( ! $st->rowCount() ) {
                    $ret['error'][] = "You didn't added any consultation plan for Duration: " . number2time($duration) . '. Please add a <a href="' . HTTP_SERVER . 'doctor-dashboard/plans.html">Consultation Plan</a> first.';
                }

            } catch (Exception $ex) {
                $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
            }
        }



        if ( !array_key_exists('error', $ret) ) {
            //now insert into table
            try {
                $query = "INSERT INTO `" . DB_PREFIX . "doctor_timing` (`doctorId`, `dayId`, `duration`, `startTime`, `endTime`, `status`, `added`)
                VALUES (?, ?, ?, ?, ?, ?, ?)";

                $st = $db->prepare($query);
                $st->execute( array($doctorId, $dayId, $duration, $start_timestamp, $end_timestamp, '1', time()) );

                $ret['success'] = 'success';
                $ret['message'][] = 'Schedule Timing added successfully.';

                $id = $db->lastInsertId();
                $day_text = getNumberToDay( $dayId );
                $duration_text = number2time( $duration );
                $start_time = date('h:i A', $start_timestamp);
                $end_time = date('h:i A', $end_timestamp);
                $token = getToken();

                $select_options_for_duration = '';
                foreach(array(1,2) as $key => $value) {
                    $sel = '';
                    if ( $duration == $value ) {
                        $sel = 'selected="selected"';
                    }
                    $text = number2time($value);
                    $select_options_for_duration .= "<option value='$value' $sel>$text</option>";
                }


                $day_array = array(
                    '1' => 'Monday',
                    '2' => 'Tuesday',
                    '3' => 'Wednesday',
                    '4' => 'Thursday',
                    '5' => 'Friday',
                    '6' => 'Saturday',
                );

                $select_options_for_day = '';
                foreach ( $day_array as $day_key => $day_name ) {
                    $sel = '';
                    if ( $dayId == $day_key ) {
                        $sel = 'selected="selected"';
                    }

                    $select_options_for_day .= "<option value='$day_key' $sel>$day_name</option>";
                }


                //now prepare the html
                $str = <<<EOD
            <tr class="row_{$id}_1">
                <td>{$day_text}</td>
                <td>{$duration_text}</td>
                <td>{$start_time}</td>
                <td>{$end_time}</td>
                <td class="text-center">
                    <a class='btn btn-info btn-xs' data-toggle="collapse" data-target="#doctorTiming{$id}"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                    <a class='btn btn-default dark-pink'><span class="fa fa-minus-circle"></span> Delete</a>
                </td>
            </tr>
            <tr class="row_{$id}_2">
                <td colspan="4">
                    <!-- Edit Consultations Start here -->
                    <div class="collapse"  id="doctorTiming{$id}">
                        <form method="post" action="index.php?do=ajax&action=add_doctors_timing" class="doctorTimingForm">
                            <input type="hidden" name="token" value="{$token}" />
                            <input type="hidden" name="task" value="edit" />
                            <input type="hidden" name="id" value="{$id}" />
                            <div class='form-row'>
                                        <div class='col-xs-3 form-group required'>
                                            <label class='control-label'>Select Day</label>
                                            <select name="dayId" class="form-control">$select_options_for_day</select>
                                        </div>

                                        <div class='col-xs-3 form-group required'>
                                            <label class='control-label'>Duration</label>
                                            <select name="duration" class='form-control' data-live-search="true">$select_options_for_duration</select>
                                        </div>

                                        <div class='col-xs-6 form-group required'>
                                            <div class='col-xs-12 row'><label class='control-label'>Start Time</label></div>
                                            <div class='col-xs-5 row'>
                                                <div class="form-group">
                                                    <div class='input-group date' id='doctorTimings'>
                                                        <input type='text' name="startTime" id="datetimepicker_{$id}" value="{$start_time}" class="form-control" />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='pull-right'>
                                                <button type="submit" class="btn btn-default dark-pink submit update"><i class="fa fa-pencil"></i> Update</button>
                                                <a class='btn btn-default dark-pink' data-toggle="collapse" data-target="#doctorTiming{$id}"><span class="fa fa-ban"></span> Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#datetimepicker_{$id}').datetimepicker({
                                            format: 'LT'
                                        });
                                    });
                                </script>
                    </div>
                    <!-- Edit Consultations Ends here -->
                </td>
            </tr>
EOD;
                $ret['html'] = $str;

            } catch (Exception $ex) {
                $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
            }
        }

        break;

    case 'edit':
        $id = isset($_POST['id']) ? intval($_POST['id']) : '';
        $doctorId = $_SESSION["mvdoctorID"]; //doctorId
        $dayId = isset($_POST['dayId']) ? intval($_POST['dayId']) : '';
        $startTime = isset($_POST['startTime']) ? trim(strip_tags( $_POST['startTime'] ) ) : '';
        $duration = isset($_POST['duration']) ? floatval($_POST['duration']) : '';

        //validate user inputes
        if ( $doctorId == '' ) {
            $ret['error'][] = 'Please login to add timing.';
        }
        if ( $dayId == '' ) {
            $ret['error'][] = "Please select a day from list.";
        }
        if ( $duration == '' ) {
            $ret['error'][] = "Please select schedule duration.";
        } elseif ( !in_array($duration, array(1,2)) ) {
            $ret['error'][] = "Invalid schedule duration selected.";
        }

        if ( $startTime == '' ) {
            $ret['error'][] = 'Please enter a startTime.';
        } else { //check start time and end time for database
            //1. convert to timestamp
            $duration_in_minutes = ($duration == 1) ? 15 : 30;
            $time_array = explode(' ', $startTime);
            $time = $time_array[0] . ':00' . ' ' . $time_array[1];
            $time_explode = explode(':', $time);
            if (!in_array($time_explode[1], array('00', '15', '30', '45'))) {
                $ret['error'][] = 'Only available minute will be 0, 15, 30, or 45';
                $error = true;
            }
        }

        if ( !array_key_exists('error', $ret) ) {
            $start_timestamp = strtotime('2000-01-01 ' . $time);
            $end_timestamp = strtotime('2000-01-01 ' . $time . ' + ' . $duration_in_minutes . ' minutes' );

            //2. check database for valid start and end time
            try {
                $query = "SELECT * FROM `" . DB_PREFIX . "doctor_timing` WHERE doctorId = ? AND dayId = ? AND id != ?";
                $st = $db->prepare($query);
                $st->execute(array($doctorId, $dayId, $id));
            } catch ( Exception $ex ) {
                $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
            }
            if ( $st->rowCount() ) {
                $timing_datas = $st->fetchAll();
                $error = false;
                //check start time is ok
                foreach ( $timing_datas as $timing_data ) {
                    if ( $start_timestamp >= $timing_data['startTime'] && $start_timestamp < $timing_data['endTime'] ) {
                        $ret['error'][] = "Selected time frame is not available. " . date('h:i:s A', $timing_data['startTime']) . " to " . date('h:i:s A', $timing_data['endTime']) .
                            " time slot is already in use. Please select another time slot.";
                        $error = true;
                        break;
                    }
                }

                //check end time is ok
                if ( $error == false ) {
                    foreach ( $timing_datas as $timing_data ) {
                        if ( $end_timestamp > $timing_data['startTime'] && $end_timestamp <= $timing_data['endTime'] ) {
                            $ret['error'][] = "Selected time frame is not available. " . date('h:i:s A', $timing_data['startTime']) . " to " . date('h:i:s A', $timing_data['endTime']) .
                                " time slot is already in use. Please select another time slot.";
                            $error = true;
                            break;
                        }
                    }
                }
            }
        }

        //check plan price added or not
        if ( !array_key_exists('error', $ret) ) {
            try {
                $query = "SELECT * FROM `" . DB_PREFIX . "plans` WHERE doctorId = ? AND (`time` = 1 or `time` = 2 ) AND price > 0 ";
                $st = $db->prepare($query);
                $st->execute(array($doctorId));

                if ( ! $st->rowCount() ) {
                    $ret['error'][] = "You didn't added any consultation plan for Duration: " . number2time($duration) . '. Please add a <a href="' . HTTP_SERVER . 'doctor-dashboard/plans.html">Consultation Plan</a> first.';
                }

            } catch (Exception $ex) {
                $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
            }
        }

        if ( !array_key_exists('error', $ret) ) {
            //now insert into table
            try {
                $query = "UPDATE `" . DB_PREFIX . "doctor_timing`  SET `dayId` = ?, `duration` = ?, `startTime` = ?, `endTime` = ?
                WHERE `doctorId` = ? and `id` = ?";

                $st = $db->prepare($query);
                $st->execute( array( $dayId, $duration, $start_timestamp, $end_timestamp, $doctorId, $id) );

                $ret['success'] = 'success';
                $ret['message'][] = 'Schedule timing updated successfully.';

                $day_text = getNumberToDay($dayId);
                $duration_text = number2time( $duration );
                $start_time = date('h:i A', $start_timestamp);
                $end_time = date('h:i A', $end_timestamp);
                $token = getToken();

                $select_options_for_duration = '';
                foreach(array(1,2) as $key => $value) {
                    $sel = '';
                    if ( $duration == $value ) {
                        $sel = 'selected="selected"';
                    }
                    $text = number2time($value);
                    $select_options_for_duration .= "<option value='$value' $sel>$text</option>";
                }


                $day_array = array(
                    '1' => 'Monday',
                    '2' => 'Tuesday',
                    '3' => 'Wednesday',
                    '4' => 'Thursday',
                    '5' => 'Friday',
                    '6' => 'Saturday',
                );

                $select_options_for_day = '';
                foreach ( $day_array as $day_key => $day_name ) {
                    $sel = '';
                    if ( $dayId == $day_key ) {
                        $sel = 'selected="selected"';
                    }

                    $select_options_for_day .= "<option value='$day_key' $sel>$day_name</option>";
                }


                //now prepare the html
                $str1 = <<<EOD
                <td>{$day_text}</td>
                <td>{$duration_text}</td>
                <td>{$start_time}</td>
                <td>{$end_time}</td>
                <td class="text-center">
                    <a class='btn btn-info btn-xs' data-toggle="collapse" data-target="#doctorTiming{$id}"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                    <a class='btn btn-default dark-pink'><span class="fa fa-minus-circle"></span> Delete</a>
                </td>
EOD;
                $str2 = <<<EOD
                <td colspan="4">
                    <!-- Edit Consultations Start here -->
                    <div class="collapse"  id="doctorTiming{$id}">
                        <form method="post" action="index.php?do=ajax&action=add_doctors_timing" class="doctorTimingForm">
                            <input type="hidden" name="token" value="{$token}" />
                            <input type="hidden" name="task" value="edit" />
                            <input type="hidden" name="id" value="{$id}" />
                            <div class='form-row'>
                                        <div class='col-xs-3 form-group required'>
                                            <label class='control-label'>Select Day</label>
                                            <select name="dayId" class="form-control">$select_options_for_day</select>
                                        </div>

                                        <div class='col-xs-3 form-group required'>
                                            <label class='control-label'>Duration</label>
                                            <select name="duration" class='form-control' data-live-search="true">$select_options_for_duration</select>
                                        </div>

                                        <div class='col-xs-6 form-group required'>
                                            <div class='col-xs-12 row'><label class='control-label'>Start Time</label></div>
                                            <div class='col-xs-5 row'>
                                                <div class="form-group">
                                                    <div class='input-group date' id='doctorTimings'>
                                                        <input type='text' name="startTime" id="datetimepicker_{$id}" value="{$start_time}" class="form-control" />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='pull-right'>
                                                <button type="submit" class="btn btn-default dark-pink submit update"><i class="fa fa-pencil"></i> Update</button>
                                                <a class='btn btn-default dark-pink' data-toggle="collapse" data-target="#doctorTiming{$id}"><span class="fa fa-ban"></span> Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#datetimepicker_{$id}').datetimepicker({
                                            format: 'LT'
                                        });
                                    });
                                </script>
                    </div>
                    <!-- Edit Consultations Ends here -->
                </td>
EOD;
                $ret['row1'] = $str1;
                $ret['row2'] = $str2;

            } catch (Exception $ex) {
                $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
            }
        }

        break;

    case 'delete':

        $doctorId = $_SESSION["mvdoctorID"]; //doctorId
        $id = isset($_POST['id']) ? intval($_POST['id']) : '';

        if ( $doctorId == '' ) {
            $ret['error'][] = 'Please login to add timing.';
        }
        if ( $id == '' || $id <= 0 ) {
            $ret['error'][] = 'Invalid plan id.';
        }

        if ( !array_key_exists('error', $ret) ) {
            //now insert into table
            try {
                $query = "DELETE FROM `" . DB_PREFIX . "doctor_timing` WHERE `doctorId` = ? and `id` = ? LIMIT 1";

                $st = $db->prepare($query);
                $st->execute( array($doctorId, $id) );

                $ret['success'] = 'success';
                $ret['message'][] = 'Consultation plan deleted successfully.';

            } catch (Exception $ex) {
                $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
            }
        }

        break;
    default:


        break;
}



header('Content-Type: application/json');
echo json_encode($ret);
exit;