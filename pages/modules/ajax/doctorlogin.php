<?php

//deny direct access

if ( !defined('MVD_SITE') ) {

    die('You are not authorized to view this page');

}



$loginError="";

$createSessions = 0;

$ret = array();



//posting form

if( isset($_POST['userEmail'], $_POST['loginPassword']) ) {

    $loginUser = sanitize( $_POST["userEmail"] );

    $loginPassword = sanitize( $_POST["loginPassword"] );



    if ( $loginUser != "" && $loginPassword != "") {

        global $db;

        $loginOk = false;



			try {

				$query = "SELECT * FROM ". DB_PREFIX ."users WHERE userEmail = ? or userLogin = ? AND userStatus=1 AND userLevel = 5 LIMIT 1";

				$result = $db->prepare($query);

				 $result->execute( array($loginUser, $loginUser) );



			} catch (Exception $Exception) {

				$ret['error'][] = "DataBase Error {$Exception->getCode()}:" . $Exception->getMessage();

			}



		//if ( $result->rowCount() > 0) {



        $row = $result->fetch();

		if ( intval( $row['userLevel'] ) == 5) {



            if ( $row['userPicture'] != '' ) {

                $row['userPicture'] = unserialize(base64_decode($row['userPicture']));

            }



            $hash = $row['userPassword'];

            $password_to_check = SITE_KEY . $loginPassword . $row['userKey'];

            //echo $db_password . '<br>' . $hash;

            if ( password_verify( $password_to_check, $hash ) ) {

                //echo 'Login ok';

                $loginOk = true;

                //if password need rehash with a new algorithm

                $options = [

                    'cost' => 11,

                    'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),

                ];



                if ( password_needs_rehash($hash, PASSWORD_DEFAULT, $options) ) {

                    $user_key = getRandomString(20);

                    $new_pass = password_hash(SITE_KEY . $loginPassword . $user_key, PASSWORD_DEFAULT, $options);



                    try {

                        /* Store new hash in db */

                        $query = "UPDATE ". DB_PREFIX ."users set userKey = ?, userPassword = ? WHERE userId = ?";

                        $result = $db->prepare($query);

                        $result->execute(array($user_key , $new_pass, $row['userId']));

                    } catch (Exception $Exception) {

                        $ret['error'][] = "DataBase Error {$Exception->getCode()}:" . $Exception->getMessage();

                    }

                }



            } else {

                $ret['error'][] = "Incorrect Login Information, Please try again.";

            }



        } else {

            $ret['error'][] = "Incorrect Login Information, Please try again.";

        }





        if ( $loginOk ) {

            $userStatus = intval( $row['userStatus'] );



            if ($userStatus !== 1) {

                $ret['error'][] = "Your account has been disabled !";

            } else {

                //get doctor id

                $query = "SELECT id,featured_image FROM `" . DB_PREFIX ."doctors` WHERE `userEmail` = ? LIMIT 1";

                $st = $db->prepare($query);

                $st->execute( array( $row['userEmail'] ) );

                if ( $st->rowCount() ) {

                    $row2 = $st->fetch();

                    $_SESSION["mvdoctorID"] = intval( $row2['id'] );

                    $_SESSION["mvdoctorVisitoruserPicture"] = $row2['featured_image'];

                }

                //declaring sessions

                $_SESSION["mvdoctorVisitorUserFname"] = sanitize( $row['userFname'] );

                $_SESSION["mvdoctorVisitorUserLname"] = sanitize( $row['userLname'] );

                $_SESSION["mvdoctorVisitorUserLevel"] = sanitize( $row['userLevel'] );

                $_SESSION["mvdoctorVisitorUserTitle"] = sanitize( $row['userTitle'] );

                $_SESSION["mvdoctorVisitornUserId"]   = intval( $row['userId'] );

                $_SESSION["mvdoctorVisitorUserEmail"] = sanitize( $row['userEmail'] );



                $_SESSION['mvdoctorVisitorSecureVideoId'] = intval( $row['secureVideoId'] );





                $ret['success'] = 'ok';

            }

        }

    } else {

        $ret['error'][] = "Email / Password should not be blank";

    }



} else {

    $ret['error'] = "Please Enter Email Address and Password.";

}



header('Content-Type: application/json');

echo json_encode($ret);

exit;

?>



