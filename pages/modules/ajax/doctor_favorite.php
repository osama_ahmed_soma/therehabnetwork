<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}

$ret = array();
//check for login user
if ( isset($_SESSION["mvdoctorVisitornUserId"])  ) {
	//
	$doctor_id = isset($_POST['doctor_id']) ? intval($_POST['doctor_id']) : 0;
	$post_id = isset($_POST['post_id']) ? intval($_POST['post_id']) : 0;

	if ( $doctor_id > 0 ) {
		//insert if not exits
		$time = time();
		$query = "INSERT INTO `" . DB_PREFIX . "favorite_doctor` (userId, doctorId, added)
			SELECT * FROM (SELECT {$_SESSION['mvdoctorVisitornUserId']}, $doctor_id, $time) AS tmp
			WHERE NOT EXISTS (
				SELECT id FROM `" . DB_PREFIX . "favorite_doctor` WHERE doctorId = $doctor_id AND userId = {$_SESSION['mvdoctorVisitornUserId']}
			) LIMIT 1;";
		try {
			$st = $db->prepare($query);
			$st->execute( );

			$ret['success'] = 'yes';
		} catch (Exception $Exception) {
			$ret['error'] =  "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( );
		}

	} elseif ( $post_id > 0 ) {
		//insert if not exits
		$time = time();
		$query = "INSERT INTO `" . DB_PREFIX . "pinned_post` (userId, postId, added)
			SELECT * FROM (SELECT {$_SESSION['mvdoctorVisitornUserId']}, $post_id, $time) AS tmp
			WHERE NOT EXISTS (
				SELECT id FROM `" . DB_PREFIX . "pinned_post` WHERE postId = $post_id AND userId = {$_SESSION['mvdoctorVisitornUserId']}
			) LIMIT 1;";
		try {
			$st = $db->prepare($query);
			$st->execute( );

			$ret['success'] = 'yes';
		} catch (Exception $Exception) {
			$ret['error'] =  "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( );
		}

	} else {
		//error: invalid doctor id
		$ret['error'] = "Invalid Doctor.";
	}

} else {
	//user not logged in, show error message
	$ret['error'] = 'Please login first.';
}

header('Content-Type: application/json');
echo json_encode($ret);
exit;