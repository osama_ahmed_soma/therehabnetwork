<?php

//deny direct access

if ( !defined('MVD_SITE') ) {

    die('You are not authorized to view this page');

}



$ret = array();



if( isset($_POST['userEmail']) ) {

    $emailAddress = sanitize($_POST["userEmail"]);



    if( $emailAddress == "" ) {

        $error = 1;

        $ret['error'][] = "Please enter a valid Email Address or Login Id!";

    } else {

        $sqlEmail = "SELECT * FROM " . DB_PREFIX . "users WHERE userEmail = ? or userLogin = ? AND userLevel = 4 LIMIT 1";



        $result = $db->prepare($sqlEmail);

        $result->execute( array($emailAddress, $emailAddress) );



        if ( $result->rowCount() > 0 ) {



            $row = $result->fetch();



            $userId = $row['userId'];

            $userFname = $row['userFname'];

            $userEmail = $row['userEmail'];

            $userLname = $row['userLname'];



            $options = [

                'cost' => 11,

                'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),

            ];



            $randomPassword = substr(session_id(),0,8);

            $user_key = getRandomString(20);

            $new_pass = password_hash(SITE_KEY . $randomPassword . $user_key, PASSWORD_DEFAULT, $options);



            try {

                /* Store new hash in db */

                $query = "UPDATE " . DB_PREFIX . "users set userKey = ?, userPassword = ? WHERE userId = ?";

                $result = $db->prepare($query);

                $result->execute(array($user_key , $new_pass, $row['userId']));

            } catch (Exception $Exception) {

                $ret['error'][] = "DataBase Error {$Exception->getCode()}:" . $Exception->getMessage();

            }



            //sending email

            // additional header pieces for errors, From cc's, bcc's, etc

            $headers = "From: ". CONTACT_NAME ." <". CONTACT_EMAIL .">\r\n";

            $headers.="Content-type: text/plain; charset=iso-8859-1\r\n";



            //subject

            $subject = "Your My Virtual Doctor CMS Password Has Been Reset";



            //message

            $message = "Dear $userFname $userLname \n\n";

            $message .= "Your password has been reset and your new password is: \n\n";

            $message .= "$randomPassword \n\n";

            $message .= "Please use the new password above to login from now on. You can always change your password after logging in to the system";



            if ( mail($userEmail,$subject,$message,$headers)) {

                $ret['success'] = "Your password has been changed and the new password has been emailed to you.";

            } else {

                $ret['error'][] = "Sending email failed!";

            }



        } else {

            $error = 1;

            $ret['error'][] = "The Email Address / Login Id you entered was not found in the database!";

        }

    }

} else {

    $ret['error'][] = "Please enter a valid Email Address or Login Id!";

}

header('Content-Type: application/json');

echo json_encode($ret);

exit;

?>