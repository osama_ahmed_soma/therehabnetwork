<?php

//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

//check call is from $do=ajax
if ( $do !== 'ajax' ) {
    die('You are not authorized to view this page');
}


require_once $siteRoot .'securevideo/securevideo_lib.php';
$secure_lib = new Securevideo_Lib();

$ret = array();


function RandomString($length = 10)
{
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}



if(!empty($_FILES['file']['name']) && $_FILES['file']['size']<= MAX_IMG_SIZE)
{
		$ret['post'] = $_FILES['file'];
		$fileName  =   isset($_FILES['file']['name']) ? sanitize($_FILES['file']['name'],4) : '';
		$fileType      =   isset($_FILES['file']['type']) ? sanitize($_FILES['file']['type'],4) : '';
		$fileTempName    =   isset($_FILES['file']['tmp_name']) ? sanitize($_FILES['file']['tmp_name'],4) : '';
		$fileSize    =   isset($_FILES['file']['size']) ? sanitize($_FILES['file']['size'],4) : '';
		$myRecordImageTitle    =   isset($_POST['myRecordImageTitle']) ? sanitize($_POST['myRecordImageTitle'],4) : '';
	
		$RandomString = RandomString();
		
		if(!empty($myRecordImageTitle))
		{
			$myRecordOrigName = str_replace(" ","-",$myRecordImageTitle).'.'.substr($fileName,strlen($fileName)-3,3);
			$newfilename = $_SESSION["mvdoctorVisitornUserId"].'_'.$RandomString.'_'.$myRecordOrigName;
		}
		else
		{
			$myRecordOrigName = $fileName;
			$newfilename = $_SESSION["mvdoctorVisitornUserId"].'_'.$RandomString.'_'.$myRecordOrigName;
		}
	
		//Health Codition
	
		if ( !array_key_exists('error', $ret) ) {
	
			
	
			try {
				/* create new password */
				$query = "INSERT INTO `" . DB_PREFIX . "my_record_images`  (`myRecordPatientId`,`myRecordImageTitle`,`myRecordImageName`,`myRecordOrigName`,`myRecordNewImageName`)
								VALUES (?,?,?,?,?)";
				$result = $db->prepare($query);
				$result->execute(array($_SESSION["mvdoctorVisitornUserId"], $myRecordImageTitle,$fileName,$myRecordOrigName,$newfilename));
				
				
				
				copy($fileTempName,SITE_ROOT.'images/uploads/my_records/'.$newfilename);
	
				$successHtml = '';
				try {
						$query = "SELECT * FROM " . DB_PREFIX . "my_record_images WHERE myRecordPatientId = ?  order by myRecordImageId DESC";
						$result = $db->prepare($query);
						$result->execute(array($_SESSION["mvdoctorVisitornUserId"]));
					
						if ( $result->rowCount() > 0 ) {
							$my_img_data = $result->fetchAll();
						}
						else
						{
							$my_img_data = array();
						}
					
					} catch (Exception $Exception) {
						exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
				}
	
					if(count($my_img_data) > 0)
					{
						foreach($my_img_data as $rs):
								$successHtml .= '<a id="my_record_'.$rs['myRecordImageId'].'" href="'.HTTP_SERVER.'patient/'.$rs['myRecordImageId'].'/download.html" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">'.date('m/d/Y',strtotime($rs['myRecordImageAddedDate'])).'</span>'.$rs['myRecordOrigName'].'</a><span class="remove_my_condtion_model" data-login="yes" data-id="'.$rs['myRecordImageId'].'" data-type="5" data-filename="'.$rs['myRecordNewImageName'].'" id="my_record1_'.$rs['myRecordImageId'].'">X</span>';
						endforeach;
					}
					else
					{
						$successHtml = '<a href="javascript:" class="list-group-item list-group-item-danger">No Record Found</a>';
					}
	
	
	
	
	
				echo $successHtml;
	
			} catch (Exception $Exception) {
				echo  "DataBase Error {$Exception->getCode()}:" . $Exception->getMessage();
			}
		}
	
	} else {
		echo "Sorry, your file is too large.";
	}







?>