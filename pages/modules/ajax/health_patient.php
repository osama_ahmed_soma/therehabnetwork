<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

//check call is from $do=ajax
if ( $do !== 'ajax' ) {
    die('You are not authorized to view this page');
}


require_once $siteRoot .'securevideo/securevideo_lib.php';
$secure_lib = new Securevideo_Lib();

$ret = array();



if($_POST['healthConditionType']=='my_condition')
{
	

	if( isset($_POST['healthConditionDesc']) ) {
		$ret['post'] = $_POST;
		$healthConditionDesc      =   isset($_POST['healthConditionDesc']) ? sanitize($_POST['healthConditionDesc'],4) : '';
		$healthConditionType      =   isset($_POST['healthConditionType']) ? sanitize($_POST['healthConditionType'],4) : '';
		$healthConditionSource    =   isset($_POST['healthConditionSource']) ? sanitize($_POST['healthConditionSource'],4) : '';
	
		//Health Codition
	
		if ( !array_key_exists('error', $ret) ) {
	
			
	
			try {
				/* create new password */
				$query = "INSERT INTO `" . DB_PREFIX . "health_condtions`  (`healthConditionPatientId`,`healthConditionDesc`,`healthConditionType`,`healthConditionSource`)
								VALUES (?, ?, ?, ?)";
				$result = $db->prepare($query);
				$result->execute(array($_SESSION["mvdoctorVisitornUserId"], $healthConditionDesc, $healthConditionType, $healthConditionSource));
	
				$successHtml = '';
				try {
						$query = "SELECT * FROM " . DB_PREFIX . "health_condtions WHERE healthConditionPatientId = ? AND `healthConditionType` = ?  order by healthConditionId DESC";
						$result = $db->prepare($query);
						$result->execute(array($_SESSION["mvdoctorVisitornUserId"], $healthConditionType));
					
						if ( $result->rowCount() > 0 ) {
							$my_condition_data = $result->fetchAll();
						}
						else
						{
							$my_condition_data = array();
						}
					
					} catch (Exception $Exception) {
						exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
	}
	
					if(count($my_condition_data) > 0)
					{
						foreach($my_condition_data as $rs):
								$successHtml .= '<div class="table-row no-records no-records-delete" id="my_conditon_'.$rs['healthConditionId'].'"><div class="table-cell">'.$rs['healthConditionDesc'].'</div><div class="table-cell">'.date('m/d/Y',strtotime($rs['healthCondtionAddedDate'])).'</div><div class="table-cell">'.$rs['healthConditionSource'].'</div><div class="table-cell delete remove_my_condtion_model" data-login="yes" data-id="'.$rs['healthConditionId'].'" data-type="1" style="cursor:pointer">Delete</div></div>';
						endforeach;
					}
					else
					{
								$successHtml = '<div class="table-row no-records no-records-delete"><div class="table-cell">No medical conditions reported</div><div class="table-cell"></div><div class="table-cell"></div><div class="table-cell delete" style="cursor:pointer"></div></div>';
					}
	
	
	
	
	
				$ret['success'] = $successHtml;
	
			} catch (Exception $Exception) {
				$ret['error'][] = "DataBase Error {$Exception->getCode()}:" . $Exception->getMessage();
			}
		}
	
	} else {
		$ret['error'][] = "Please enter a valid Email Address and Password.";
	}
}
else if($_POST['healthConditionType']=='my_medication')
{
	
	if( isset($_POST['healthConditionDesc']) ) {
		$ret['post'] = $_POST;
		$healthConditionDesc      =   isset($_POST['healthConditionDesc']) ? sanitize($_POST['healthConditionDesc'],4) : '';
		$healthMedicationFrequency    =   isset($_POST['healthMedicationFrequency']) ? sanitize($_POST['healthMedicationFrequency'],4) : '';
		$healthMedicationDosage    =   isset($_POST['healthMedicationDosage']) ? sanitize($_POST['healthMedicationDosage'],4) : '';
		$healthMedicationHourly    =   isset($_POST['healthConditionSource']) ? sanitize($_POST['healthMedicationHourly'],4) : '';
		$healthConditionType      =   isset($_POST['healthConditionType']) ? sanitize($_POST['healthConditionType'],4) : '';
		$healthConditionSource    =   isset($_POST['healthConditionSource']) ? sanitize($_POST['healthConditionSource'],4) : '';
	
		//Health Codition
	
		if ( !array_key_exists('error', $ret) ) {
	
			
	
			try {
				/* create new password */
				$query = "INSERT INTO `" . DB_PREFIX . "health_condtions`  (`healthConditionPatientId`,`healthConditionDesc`,`healthMedicationFrequency`,`healthMedicationDosage`,`healthMedicationHourly` ,`healthConditionType`,`healthConditionSource`)
								VALUES (?, ?, ?, ?, ?, ?, ?)";
				$result = $db->prepare($query);
				$result->execute(array($_SESSION["mvdoctorVisitornUserId"], $healthConditionDesc,$healthMedicationFrequency,$healthMedicationDosage,$healthMedicationHourly, $healthConditionType, $healthConditionSource));
	
				$successHtml = '';
				try {
						$query = "SELECT * FROM " . DB_PREFIX . "health_condtions WHERE healthConditionPatientId = ? AND `healthConditionType` = ?  order by healthConditionId DESC";
						$result = $db->prepare($query);
						$result->execute(array($_SESSION["mvdoctorVisitornUserId"], $healthConditionType));
					
						if ( $result->rowCount() > 0 ) {
							$my_condition_data = $result->fetchAll();
						}
						else
						{
							$my_condition_data = array();
						}
					
					} catch (Exception $Exception) {
						exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
	}
	
					if(count($my_condition_data) > 0)
					{
						foreach($my_condition_data as $rs):
								$successHtml .= '<div class="table-row no-records no-records-delete-my-medication" id="my_medication_'.$rs['healthConditionId'].'"><div class="table-cell">'.$rs['healthConditionDesc'].'</div><div class="table-cell">'.$rs['healthMedicationDosage'].'</div><div class="table-cell">'.$rs['healthMedicationFrequency'].' - '.$rs['healthMedicationHourly'].'</div><div class="table-cell">'.date('m/d/Y',strtotime($rs['healthCondtionAddedDate'])).'</div><div class="table-cell">'.$rs['healthConditionSource'].'</div><div class="table-cell delete remove_my_condtion_model" data-login="yes" data-type="2" data-id="'.$rs['healthConditionId'].'" style="cursor:pointer">Delete</div></div>';
						endforeach;
					}
					else
					{
								$successHtml = '<div class="table-row no-records no-records-delete-my-medication"><div class="table-cell">No medical conditions reported</div><div class="table-cell"></div><div class="table-cell"></div><div class="table-cell delete" style="cursor:pointer"></div></div>';
					}
	
	
	
	
	
				$ret['success'] = $successHtml;
	
			} catch (Exception $Exception) {
				$ret['error'][] = "DataBase Error {$Exception->getCode()}:" . $Exception->getMessage();
			}
		}
	
	} else {
		$ret['error'][] = "Please enter a valid Email Address and Password.";
	}
}
else if($_POST['healthConditionType']=='my_allergy')
{
	
	if( isset($_POST['healthConditionDesc']) ) {
		$ret['post'] = $_POST;
		$healthConditionDesc      =   isset($_POST['healthConditionDesc']) ? sanitize($_POST['healthConditionDesc'],4) : '';
		$healthMedicationReaction      =   isset($_POST['healthMedicationReaction']) ? sanitize($_POST['healthMedicationReaction'],4) : '';
		$healthMedicationSeverity      =   isset($_POST['healthMedicationSeverity']) ? sanitize($_POST['healthMedicationSeverity'],4) : '';
		$healthConditionType      =   isset($_POST['healthConditionType']) ? sanitize($_POST['healthConditionType'],4) : '';
		$healthConditionSource    =   isset($_POST['healthConditionSource']) ? sanitize($_POST['healthConditionSource'],4) : '';
	
		//Health Codition
	
		if ( !array_key_exists('error', $ret) ) {
	
			
	
			try {
				/* create new password */
				$query = "INSERT INTO `" . DB_PREFIX . "health_condtions`  (`healthConditionPatientId`,`healthConditionDesc`,`healthMedicationSeverity`,`healthMedicationReaction`,`healthConditionType`,`healthConditionSource`)
								VALUES (?, ?, ?, ?, ?, ?)";
				$result = $db->prepare($query);
				$result->execute(array($_SESSION["mvdoctorVisitornUserId"], $healthConditionDesc,$healthMedicationSeverity,$healthMedicationReaction, $healthConditionType, $healthConditionSource));
	
				$successHtml = '';
				try {
						$query = "SELECT * FROM " . DB_PREFIX . "health_condtions WHERE healthConditionPatientId = ? AND `healthConditionType` = ?  order by healthConditionId DESC";
						$result = $db->prepare($query);
						$result->execute(array($_SESSION["mvdoctorVisitornUserId"], $healthConditionType));
					
						if ( $result->rowCount() > 0 ) {
							$my_condition_data = $result->fetchAll();
						}
						else
						{
							$my_condition_data = array();
						}
					
					} catch (Exception $Exception) {
						exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
	}
	
					if(count($my_condition_data) > 0)
					{
						foreach($my_condition_data as $rs):
								$successHtml .= '<div class="table-row no-records no-records-delete-my-allergy" id="my_allergy_'.$rs['healthConditionId'].'"><div class="table-cell">'.$rs['healthConditionDesc'].'</div><div class="table-cell">'.$rs['healthMedicationSeverity'].'</div><div class="table-cell">'.$rs['healthMedicationReaction'].'</div><div class="table-cell">'.date('m/d/Y',strtotime($rs['healthCondtionAddedDate'])).'</div><div class="table-cell">'.$rs['healthConditionSource'].'</div><div class="table-cell delete remove_my_condtion_model" data-login="yes" data-type="3" data-id="'.$rs['healthConditionId'].'" style="cursor:pointer">Delete</div></div>';
						endforeach;
					}
					else
					{
								$successHtml = '<div class="table-row no-records no-records-delete-my-allergy"><div class="table-cell">No allergies reported</div><div class="table-cell"></div><div class="table-cell"></div><div class="table-cell delete" style="cursor:pointer"></div></div>';
					}
	
	
	
	
	
				$ret['success'] = $successHtml;
	
			} catch (Exception $Exception) {
				$ret['error'][] = "DataBase Error {$Exception->getCode()}:" . $Exception->getMessage();
			}
		}
	
	} else {
		$ret['error'][] = "Please enter a valid Email Address and Password.";
	}
}
else if($_POST['healthConditionType']=='my_surgery')
{
	

	if( isset($_POST['healthConditionDesc']) ) {
		$ret['post'] = $_POST;
		$healthConditionDesc      =   isset($_POST['healthConditionDesc']) ? sanitize($_POST['healthConditionDesc'],4) : '';
		$healthConditionType      =   isset($_POST['healthConditionType']) ? sanitize($_POST['healthConditionType'],4) : '';
		$healthConditionSource    =   isset($_POST['healthConditionSource']) ? sanitize($_POST['healthConditionSource'],4) : '';
	
		//Health Codition
	
		if ( !array_key_exists('error', $ret) ) {
	
			
	
			try {
				/* create new password */
				$query = "INSERT INTO `" . DB_PREFIX . "health_condtions`  (`healthConditionPatientId`,`healthConditionDesc`,`healthConditionType`,`healthConditionSource`)
								VALUES (?, ?, ?, ?)";
				$result = $db->prepare($query);
				$result->execute(array($_SESSION["mvdoctorVisitornUserId"], $healthConditionDesc, $healthConditionType, $healthConditionSource));
	
				$successHtml = '';
				try {
						$query = "SELECT * FROM " . DB_PREFIX . "health_condtions WHERE healthConditionPatientId = ? AND `healthConditionType` = ?  order by healthConditionId DESC";
						$result = $db->prepare($query);
						$result->execute(array($_SESSION["mvdoctorVisitornUserId"], $healthConditionType));
					
						if ( $result->rowCount() > 0 ) {
							$my_condition_data = $result->fetchAll();
						}
						else
						{
							$my_condition_data = array();
						}
					
					} catch (Exception $Exception) {
						exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
	}
	
					if(count($my_condition_data) > 0)
					{
						foreach($my_condition_data as $rs):
								$successHtml .= '<div class="table-row no-records no-records-delete-my-surgery" id="my_surgery_'.$rs['healthConditionId'].'"><div class="table-cell">'.$rs['healthConditionDesc'].'</div><div class="table-cell">'.date('m/d/Y',strtotime($rs['healthCondtionAddedDate'])).'</div><div class="table-cell">'.$rs['healthConditionSource'].'</div><div class="table-cell delete remove_my_condtion_model" data-login="yes" data-id="'.$rs['healthConditionId'].'" data-type="4" style="cursor:pointer">Delete</div></div>';
						endforeach;
					}
					else
					{
								$successHtml = '<div class="table-row no-records-delete-my-surgery"><div class="table-cell">No medical conditions reported</div><div class="table-cell"></div><div class="table-cell"></div><div class="table-cell delete" style="cursor:pointer"></div></div>';
					}
	
	
	
	
	
				$ret['success'] = $successHtml;
	
			} catch (Exception $Exception) {
				$ret['error'][] = "DataBase Error {$Exception->getCode()}:" . $Exception->getMessage();
			}
		}
	
	} else {
		$ret['error'][] = "Please enter a valid Email Address and Password.";
	}
}





header('Content-Type: application/json');
echo json_encode($ret);
exit;
?>