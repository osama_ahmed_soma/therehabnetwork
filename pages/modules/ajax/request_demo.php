<?php
//deny direct access
 if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
 header('Content-Type: application/json');
 
 
$ret = array();

if( isset($_POST)) 
	{
		if( isset($_POST['InputNameFirst'] , $_POST['InputNameLast'] , $_POST['InputEmail'] , $_POST['InputPhone'] , $_POST['InputClinic'])){
			$InputNameFirst = sanitize($_POST["InputNameFirst"],3);
			$InputNameLast = sanitize($_POST["InputNameLast"],3);
			$InputEmail = sanitize($_POST["InputEmail"],3);
			$InputPhone = sanitize($_POST["InputPhone"],3);
			$InputClinic = sanitize($_POST["InputClinic"],3);
			if( $InputNameFirst == "" ) {
			$ret['error'][] = "Please enter a valid Name.";
			//$ret['error'][] = $InputName;
			}
			if( $InputNameLast == "" ) {
			$ret['error'][] = "Please enter a valid First Name.";
			//$ret['error'][] = $InputName;
			}
			elseif( $InputEmail == "" ) {
			$ret['error'][] = "Please enter a valid Last Name.";
			}
			elseif( $InputPhone == "" ) {
			$ret['error'][] = "Please enter a valid Phone.";
			}
			elseif( $InputClinic == "" ) {
			$ret['error'][] = "Please enter Valid Clinic.";
			}
			else
			{
				$ip_address = $_SERVER['REMOTE_ADDR'];
				 $to = 'sale@myvirtualdoctor.com';
				 $subject = 'MyVirtualDoctor - Demo Request Form';
				 $message = '
				 <html>
				 <head>
				  <title>MyVirtualDoctor - Request a demo form <Form></Form></title>
				 </head>
				 <body>
				  <p>First Name:  '.$InputNameFirst.',</p>	
				  <p>Email:  '.$InputNameLast.',</p>
				  <p>Massage: '.$InputEmail.'</p>
				  <p>Massage: '.$InputPhone.'</p>
				  <p>Massage: '.$InputClinic.'</p>
				  <p>IP Address: '.$ip_address.'</p>
				 </body> 
				 </html>
				 ';
				 
				 // ------------------------------------------------------------
				 // SET HEADERS FOR HTML MAIL
				 // ------------------------------------------------------------
				 $headers  = 'MIME-Version: 1.0' . "\r\n";
				 $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
				 //$headers .= 'To: <'.$txbEmail.'>' . "\r\n";
				 $headers .= 'From: MyVirtualDoctor - Contact Form <'.$InputEmail.'>' . "\r\n";
				 //$headers .= 'Cc: anothermail@foo.org' . "\r\n";
				 //$headers .= 'Bcc: '.$from_address.'' . "\r\n";
				 
				 // ------------------------------------------------------------
				 // SEND E-MAIL
				 // ------------------------------------------------------------
				 $email_form = mail($to, $subject, $message, $headers);
				 if ($email_form){
					$ret['success'][] = "Your message sent successfully .";	
				}
				else{
				$ret['error'][] = "There was some error submittng the form. Please try again.";	
				}			
			}
		
			
		}
			else {
					$ret['error'][] = "There was some error submittng the form. Please try again.";	
			}
	}
	
 echo json_encode($ret);
		exit;
?>