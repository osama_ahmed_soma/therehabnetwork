<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

$ret = array();
$error = false;

//check user is logged in or not
if( !isset($_SESSION["mvdoctorVisitornUserId"]) ) {
    $ret['error'][] = 'Please login first to view this resource.';

    header('Content-Type: application/json');
    echo json_encode($ret);
    exit;
}

$doctorId       = isset($_POST['doctorId']) ? intval($_POST['doctorId']) : ''; //doctorId
$years          = isset($_POST['years']) ? intval($_POST['years']) : '';
$months         = isset($_POST['months']) ? intval($_POST['months']) + 1 : '';
$date           = isset($_POST['date']) ? intval($_POST['date']) : '';

//check for valid inputs

if ( $doctorId == '' ) {
    $ret['error'][] = "Invalid doctor selected.";
    $error = true;
} elseif ( is_numeric($doctorId) ) {
    //check database for valid doctor id
    try {
        $query = "SELECT id from `" . DB_PREFIX . "doctors` WHERE id = ? and status = 'publish' LIMIT 1";
        $st = $db->prepare($query);
        $st->execute( array($doctorId) );

        if ( !$st->rowCount() ) {
            $ret['error'][] = "Doctor not found in database.";
        }

    } catch (Exception $ex) {
        $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
    }
}

if ( $years == '' || $months == '' || $date == '' ) {
    $ret['error'][] = "Invalid date selected.";
    $error = true;
}

//get all plans for this doctor
$plans_info = array();
if ( !$error ) {
    try {
        $query = "SELECT `time`, `price` FROM `" . DB_PREFIX . "plans` WHERE `doctorId` = ? LIMIT 2";
        $st = $db->prepare($query);
        $st->execute(array($doctorId));

        if ( $st->rowCount() ) {
            foreach ( $st->fetchAll()  as $row ) {
                $plans_info[ $row['time'] ] = $row['price'];
            }
        } else {
            $ret['error'][] = "Doctor didn't added any consultation plans yet, please contact with doctor.";
            $error = true;
        }

    } catch (Exception $Exception) {
        $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
    }
}

//check user input date is today and future
$date_diff = check_date_relavence("$years-$months-$date");

if ( $date_diff < 0  ) {
    //date is in past
    $ret['error'][] = "Selected date ($years-$months-$date) is from past. Please select another date.";
    $error = true;
}


if ( !$error ) {
    $day = date('w', strtotime("$years-$months-$date"));
    $str = "";
    $plans = array();
    //get all timing for this day
    try {
        $query = "SELECT `id`, `dayId`, `duration`, `startTime`,`endTime` FROM `" . DB_PREFIX . "doctor_timing` WHERE `doctorId` = ? AND `dayId` = ?";
        $st = $db->prepare($query);
        $st->execute( array( $doctorId, $day) );

        if ( $st->rowCount() ) {
            $data = $st->fetchAll();

            //now create button html for available timing

            foreach ( $data as $row ) {


                //1. extract time
                $temp_time = date('h:i:s A', $row['startTime']);
                $new_time = strtotime( "$years-$months-$date $temp_time" );

                //this is for plans section and payment section
                $row['consultationDuration'] = number2time( $row['duration'] );
                $row['consultationDate'] = date('l F d, Y', $new_time);
                $row['consultationTime'] = date('h:i A', $new_time);
                $row['consultationFee'] = $plans_info[ $row['duration'] ];
                $plans[ $row['id'] ] = $row;

                $time_is_past = 0;
                $already_assigned = 0;

                //check plan is past current date time or not
                $date_diff = check_date_relavence("$years-$months-$date");

                if ( $date_diff == 0 ) {
                    //check for current time here
                    $start_time_string = date('h:i:s A', $row['startTime']);

                    $current_time = strtotime("$years-$months-$date " . date('h:i:s A'));
                    $patient_start_time = strtotime("$years-$months-$date $start_time_string");
                    if ($current_time >= $patient_start_time) {
                        $time_is_past = 1;
                    }
                }


                //check this timing is already in use or not in schedule table
                try {
                    $query = "SELECT id FROM `" . DB_PREFIX . "schedule` WHERE `startTime` = ? AND `doctorId` = ?";
                    $st = $db->prepare($query);
                    $st->execute( array($new_time, $doctorId) );

                    $disabled = "";
                    $class = 'selectTiming';

                    if ( $st->rowCount() ) {
                        $already_assigned = 1;

                    }

                    if ( $already_assigned == 1 || $time_is_past == 1 ) {
                        $disabled = 'disabled="disabled"';
                        $class = 'timingNotAvailable';
                    }

                    $start_time_text = date('h:i A', $row['startTime']);
                    $end_time_text = date('h:i A', $row['endTime']);

                    $str .=<<<EOD
                <button type="button" class='btn btn-default $class' data-plan_id="{$row['id']}" $disabled>$start_time_text - $end_time_text</button>
EOD;
                } catch (Exception $ex) {
                    $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
                }
            }


        } else {
            $str = '<div class="alert alert-danger" role="alert">No timing added by doctor. Please contact with doctor</div>';
        }
        $ret['success'] = 'yes';
        $ret['html'] = $str;
        $ret['plans'] = $plans;
    } catch (Exception $ex) {
        $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
    }
}



















header('Content-Type: application/json');
echo json_encode($ret);
exit;