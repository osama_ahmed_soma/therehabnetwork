<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}

$ret = array();
//check for login user
if ( isset($_SESSION["mvdoctorVisitornUserId"])  ) {
	//
	$myRecordImageId = isset($_POST['myRecordImageId']) ? intval($_POST['myRecordImageId']) : 0;
	$myRecordNewImageName = isset($_POST['myRecordNewImageName']) ? intval($_POST['myRecordNewImageName']) : 0;


if ( $myRecordImageId > 0 ) {
		//delete from favorite list

		$query = "DELETE FROM `" . DB_PREFIX . "my_record_images` WHERE myRecordImageId = ? AND myRecordPatientId = ? LIMIT 1;";
		try {
				$st = $db->prepare($query);
				$st->execute( array($myRecordImageId, $_SESSION['mvdoctorVisitornUserId']) );
				
				@unlink(SITE_ROOT.'images/uploads/my_records/' . $myRecordNewImageName);
				$ret['success'] = 'yes';
				
		} catch (Exception $Exception) {
			$ret['error'] =  "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( );
		}

	} else {
		//error: invalid doctor id
		$ret['error'] = "Invalid file.";
	}

} else {
	//user not logged in, show error message
	$ret['error'] = 'Please login first.';
}

header('Content-Type: application/json');
echo json_encode($ret);
exit;