<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

$loginError="";
$createSessions = 0;
$ret = array();

//posting form
if( isset($_POST['userEmail'], $_POST['loginPassword']) ) {
    $loginUser = sanitize( $_POST["userEmail"] );
    $loginPassword = sanitize( $_POST["loginPassword"] );

    if ( $loginUser != "" && $loginPassword != "") {
        global $db;
        $loginOk = false;

        try {
            $query = "SELECT * FROM ". DB_PREFIX ."users WHERE userEmail = ? or userLogin = ? AND userStatus=1 AND userLevel = 4 LIMIT 1";
            $result = $db->prepare($query);
            $result->execute( array($loginUser, $loginUser) );
		} catch (Exception $Exception) {
            $ret['error'][] = "DataBase Error {$Exception->getCode()}:" . $Exception->getMessage();
        }
		
		//if ( $result->rowCount() > 0) {
		$row = $result->fetch();
		if ( intval( $row['userLevel'] ) == 4 )
		{	
		    if ( $row['userPicture'] != '' ) {
                $row['userPicture'] = unserialize(base64_decode($row['userPicture']));
            }

            $hash = $row['userPassword'];
            $password_to_check = SITE_KEY . $loginPassword . $row['userKey'];
            //echo $db_password . '<br>' . $hash;
            if ( password_verify( $password_to_check, $hash ) ) {
                //echo 'Login ok';
                $loginOk = true;
                //if password need rehash with a new algorithm
                $options = [
                    'cost' => 11,
                    'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
                ];

                if ( password_needs_rehash($hash, PASSWORD_DEFAULT, $options) ) {
                    $user_key = getRandomString(20);
                    $new_pass = password_hash(SITE_KEY . $loginPassword . $user_key, PASSWORD_DEFAULT, $options);

                    try {
                        /* Store new hash in db */
                        $query = "UPDATE ". DB_PREFIX ."users set userKey = ?, userPassword = ? WHERE userId = ?";
                        $result = $db->prepare($query);
                        $result->execute(array($user_key , $new_pass, $row['userId']));
                    } catch (Exception $Exception) {
                        $ret['error'][] = "DataBase Error {$Exception->getCode()}:" . $Exception->getMessage();
                    }
                }

            } else {
                $ret['error'][] = "Incorrect Login Information, Please try again.";
            }

        } else {
            $ret['error'][] = "Incorrect Login Information, Please try again.";
        }


        if ( $loginOk ) {
            $userStatus = $row['userStatus'];

            if ($userStatus != "1") {
                $ret['error'][] = "Your account has been disabled !";
            } else {
                //declaring sessions
                $_SESSION["mvdoctorVisitorUserFname"] = sanitize( $row['userFname'] );
                $_SESSION["mvdoctorVisitorUserLname"] = sanitize( $row['userLname'] );
                $_SESSION["mvdoctorVisitorUserLevel"] = sanitize( $row['userLevel'] );
                $_SESSION["mvdoctorVisitorUserTitle"] = sanitize( $row['userTitle'] );
                $_SESSION["mvdoctorVisitornUserId"]   = sanitize( $row['userId'] );
                $_SESSION["mvdoctorVisitorUserEmail"] = sanitize( $row['userEmail'] );

                $_SESSION["mvdoctorVisitorUserAddress"] = sanitize( $row['userAddress'] );
                $_SESSION["mvdoctorVisitorUserCity"] = sanitize( $row['userCity'] );
                $_SESSION["mvdoctorVisitorUserCountry"] = sanitize( $row['userCountry'] );
                $_SESSION["mvdoctorVisitorUserState"] = sanitize( $row['userState'] );
                $_SESSION["mvdoctorVisitorUserZip"] = sanitize( $row['userZip'] );

                $_SESSION['mvdoctorVisitorSecureVideoId'] = intval( $row['secureVideoId'] );

                if ( isset( $row['userPicture']['img2'] ) ) {
                    $_SESSION["mvdoctorVisitoruserPicture"] = sanitize( $row['userPicture']['img2'] );
                }

                $ret['success'] = 'ok';
            }
        }
    } else {
        $ret['error'][] = "Email / Password should not be blank";
    }

} else {
    $ret['error'][] = "Please Enter Email Address and Password.";
}

header('Content-Type: application/json');
echo json_encode($ret);
exit;
?>

