<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

$err = "";
$ret = array();
$file = "";
$img ="";

require_once $siteRoot .'securevideo/securevideo_lib.php';
$secure_lib = new Securevideo_Lib();

if( isset($_POST['firstname'], $_POST['password']) ) {	
	if (isset($_FILES['featured_image']))
	{
		$file = $_FILES['featured_image']['name'];
	}
	$emailAddress = $_POST["emailAddress"];
    $firstname = $_POST["firstname"];
	$lastname = $_POST["lastname"];    
	$codephone = $_POST["codephone"];	
	$phone = $codephone."-".$_POST["phone"];	
	$codemobile = $_POST["codemobile"];	
	$mobile = $codemobile."-".$_POST["mobile"];
	if($mobile=='-')
	{$mobile = '';}
	$gender = $_POST["gender"];
	$birth_date = $_POST["birth_date"];		
	$practicesince = $_POST["practicesince"];		
	$department = $_POST["department"];		
	$medicalspecialty = $_POST["medicalspecialty"];		
	$subspeciality = $_POST["subspeciality"];		
	$medicallicensed = $_POST["medicallicensed"];		
	$stateslicensed = $_POST["stateslicensed"];		
	$education = $_POST["education"];		
	$internship = $_POST["internship"];		
	$residency = $_POST["residency"];		
	$street = $_POST["street"];		
	$city = $_POST["city"];		
	$country = $_POST["country"];		
	$state = $_POST["state"];		
	$zip = $_POST["zip"];		
	$username = $_POST["username"];		
	$password = $_POST["password"];		
	$confirmpassword = $_POST["confirmpassword"];
	$secureVideoId = isset($_POST['secureVideoId']) ? $_POST['secureVideoId'] : '';
	$userTimeZone = isset($_POST['userTimeZone']) ? strip_tags( $_POST['userTimeZone'] ) : '';
	
	$i = 0;
	if( $firstname == "" ) 
	{		
		$i = $i + 1;
		$err = $err . "- Firstname field is empty<br>";    
	} 	
	if( $lastname == "" ) 
	{		
		$i = $i + 1;
		$err = $err . "- Lastname field is empty<br>";    
	} 	
	if( $emailAddress == "" ) 
	{		
		$i = $i + 1;
		$err = $err . "- Email address is not valid<br>";    
	} 	elseif( $emailAddress != "" && !filter_var($emailAddress, FILTER_VALIDATE_EMAIL) ) {		
		$err = $err . "Email address is not valid<br>";  
	} 	
	if( $phone == "" ) 
	{		
		$i = $i + 1;
		$err = $err . "- Phone number is not valid<br>";  
	}
	if( $mobile == "" ) 
	{		
		$i = $i + 1;
		$err = $err . "- Mobile number is not valid<br>";   
	}
	if( $birth_date == "" ) 
	{	
		$i = $i + 1;
		$err = $err . "- DOB is not valid<br>";   
	} 
	if ( $practicesince == "" ) 
	{	
		$i = $i + 1;
		$err = $err . "- Practicing year is empty<br>";    
	}	
	else
	{
		if (!is_numeric($practicesince))
		{
			$err = $err . "- Practicing year is empty<br>";     
		}
	}
	if ( $department == "" ) 
	{		
		$i = $i + 1;
		$err = $err . "- Department field is empty<br>";  
	}	
	if( $medicalspecialty == "" ) 
	{	
		$i = $i + 1;
		$err = $err . "- must be medical specialty<br>";   
	}	
	if( $subspeciality == "" ) 
	{		
		$i = $i + 1;
		$err = $err . "- must be sub speciality<br>";    
	}	
	if( $medicallicensed == "" ) 
	{		
		$i = $i + 1;
		$err = $err . "- must be medical licensed<br>";    
	}	
	if( $stateslicensed == "" ) 
	{		
		$i = $i + 1;
		$err = $err . "- must be states licensed<br>";    
	}	
	if( $education == "" ) 
	{		
		$i = $i + 1;
		$err = $err . "- must be pre-medical education<br>";    
	}	
	if( $internship == "" ) 
	{		
		$i = $i + 1;
		$err = $err . "- must be internship<br>";    
	}	
	if( $residency == "" ) 
	{	
		$i = $i + 1;
		$err = $err . "- must be residency<br>";    
	}	
	if( $street == "" ) 
	{	
		$i = $i + 1;
		$err = $err . "- must be street<br>";    
	}	
	if ($file=="")	
	{	
		$i = $i + 1;
		$err = $err . "- select upload file<br>";	
	}	
	
	if( $city == "" ) 
	{	
		$i = $i + 1;
		$err = $err . "- must be city<br>";    
	}	
	if( $state == "" ) 
	{	
		$i = $i + 1;
		$err = $err . "- select state<br>";    
	}
	
	if( $country == "" ) 
	{	
		$i = $i + 1;
		$err = $err . "- select country<br>";    
	}	
	if( $zip == "" ) 
	{
		$i = $i + 1;
		$err = $err . "- must be zip<br>";    
	}
	else
	{
		if (!is_numeric($zip))
		{
			$err = $err . "- must be numbers in zip<br>";     
		}
	}
	
	if( $username == "" ) 
	{	
		$i = $i + 1;
		$err = $err . "- must be username<br>";    
	}	
	if( $password == "" ) 
	{	
		$i = $i + 1;
		$err = $err . "- must be password<br>";    
	} 
	elseif($password !=$confirmpassword)
	{
		$err = $err . "- password and confrim password should match<br>";	
	}

	//check for timezone
	if ( $userTimeZone == '' ) {
		$i += 1;
		$err .= ' - please select a timezone.';
	}

	//check securevideo email exist or not
	if ( $emailAddress != '' && $secure_lib->userEmailExist($emailAddress) ) {
		$err = $err . "- Email address already exist in database. Please enter another email.<br>";
	}

	
	if ($err!="")	
	{	
		if ($i=="20")
		{
			$ret['error'][] = "Please enter complete information.";	
		}	
		else
		{
			$ret['error'][] = $err;	
		}	
	}
	else
	{
        $sqlEmail = "SELECT * FROM " . DB_PREFIX . "users WHERE userEmail = ? or userLogin = ? AND userLevel = 5 LIMIT 1";        
		$result = $db->prepare($sqlEmail);        
		$result->execute( array($emailAddress, $emailAddress) );
        if ( $result->rowCount() > 0 ) 
		{    
			$ret['error'][] = "Email address already exist in database. Please enter another email.";       
		}
		else 
		{
			 if ( isset($_FILES['featured_image']['name']) && $_FILES['featured_image']['name'] != ''  ) 
			{
				$image = uploadDoctorPicture();

				if ( $image['status'] == 'success' ) {
					$img = $image['data']['img1'];
					//$_SESSION["mvdoctorVisitoruserPicture"] = $image['data']['img2'];
				} 
				else 
				{
					die ("Image Upload Error" . $image['data']);
				}
				
				 $options = [
                'cost' => 11,
                'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
            ];

            $user_key = getRandomString(20);
			$new_pass = password_hash(SITE_KEY . $password . $user_key, PASSWORD_DEFAULT, $options);
	        try 
			{		
				//$birth = strtotime($birth_date);
				//$date = date("Y-m-d",$birth);

				$patient_data = array(
						"FullName"      => $firstname . ' ' . $lastname,
						"EmailAddress"  => $emailAddress,
						"TimeZoneWindowsId" => isset( $userTimeZone ) && $userTimeZone != '' ? $userTimeZone : 'UTC',
						//"SmsNumber"     => $data['userPhoneHome'],
						'HelpNumber' => '212-555-5555',
						'SystemRoles'   => 'H,P',
						'ShouldAutoDeliverCode' => 'E' //send emails
				);

				$patient_create_ret = $secure_lib->createNewUser($patient_data);

				if ( $patient_create_ret['status'] == '1' ) {
					$secureVideoId = $patient_create_ret['data']->SystemUserId;
				} else {
					//display error message
					$ret['error'][] = $patient_create_ret['message'];
				}

				//fix doctor url
				$doctor_name = $firstname .' ' . $lastname;
				$doctor_url = '';

				$url = trim( strtolower( $doctor_name ) );
				$url = preg_replace("/[^A-Za-z0-9 -]/", '', $url);
				$url = str_replace(' ', '-', $url);

				//check url length
				if ( strlen( $url ) > 200 ) {
					$url  = substr( $url, 0, 200 );
				}

				$prefix = 1;
				$old_url = $url;

				do {
					try {
						$query = "SELECT id FROM `" . DB_PREFIX . 'doctors` WHERE url = ?';
						$st = $db->prepare($query);
						$st->execute( array( $url ) );
						if ( $st->rowCount() > 0 ) {
							$url = $old_url . '-' . $prefix;
							$prefix++;
						} else {
							$doctor_url = $url;
							$good_to_go = true;
							break;
						}
					} catch (Exception $Exception) {
						exit("DataBase Error {$Exception->getCode()}:" . $Exception->getMessage());
					}

				} while (1);
				
				$querydoc = "INSERT INTO `" . DB_PREFIX . "doctors`  (`doctor_name`, `url`, `featured_image`, `experience_details`, `specialization`,`degree`,`sex`,`userEmail`)	VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
				
				$resultdoc = $db->prepare($querydoc);

				$resultdoc->execute(array($doctor_name, $doctor_url, $img, $internship, $medicalspecialty, $education, $gender, $emailAddress));

				$new_doctor_id = $db->lastInsertId();

				//fix category problems for doctor
				$query = "INSERT INTO `" . DB_PREFIX . "category_data` (categoryId,categoryType,postId,postType)
				VALUES (83,'company', {$new_doctor_id}, 'doctors')";

				$st = $db->prepare($query);
				$st->execute();
				//end category problem fix


				$query = "INSERT INTO `" . DB_PREFIX . "users`  (`userLogin`, `secureVideoId`, `userKey`, `userPassword`,`userFname`,`userLname`,`userAddress`,`userGender`,`userDob`,`userCountry`,`userCity`,`userState`,`userZip`,`userPhoneHome`,`userPhoneCellular`, `userEmail`, `userLevel`, `userStatus`, `added`)                            VALUES (?,?,?,?, ?, ?, ?,?,?,?,?,?,?,?,?,?, 5, 1, ?)";
				$result = $db->prepare($query);
				
				$result->execute(array($username, $secureVideoId, $user_key, $new_pass,$firstname,$lastname,$street,$gender,$birth_date,$country,$city,$state,$zip,$phone,$mobile, $emailAddress, time()));
				$last = $db->lastInsertId();
				
				$_SESSION["mvdoctorVisitorUserFname"] = $firstname;

                $_SESSION["mvdoctorVisitorUserLname"] = $lastname;

                $_SESSION["mvdoctorVisitorUserLevel"] = '5';

                $_SESSION["mvdoctorVisitorUserTitle"] = '';

                $_SESSION["mvdoctorVisitornUserId"]   = (string)$last;
				
				
                $_SESSION["mvdoctorVisitorUserEmail"] = $emailAddress;
				$_SESSION["mvdoctorID"] = $new_doctor_id;
				$_SESSION["mvdoctorVisitoruserPicture"] = $img;
				
                //start new session				
				$ret['success'] = "Your account has been successfully created. Redirecting to your account dashboard now...";


            }
			catch (Exception $Exception) 
			{       
				$ret['error'][] = "DataBase Error {$Exception->getCode()}:" . $Exception->getMessage();            
			}   
			
			} 	
                 
		}     
	}  
} 
else 
{
    $ret['error'][] = "Please enter complete information.";
}
header('Content-Type: application/json');
echo json_encode($ret);
exit;
?>



