<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}

$ret = array();
//check for login user
if ( isset($_SESSION["mvdoctorVisitornUserId"])  ) {
	//
	$healthConditionId = isset($_POST['healthConditionId']) ? intval($_POST['healthConditionId']) : 0;

if ( $healthConditionId > 0 ) {
		//delete from favorite list

		$query = "DELETE FROM `" . DB_PREFIX . "health_condtions` WHERE healthConditionId = ? AND healthConditionPatientId = ? LIMIT 1;";
		try {
				$st = $db->prepare($query);
				$st->execute( array($healthConditionId, $_SESSION['mvdoctorVisitornUserId']) );
				$ret['success'] = 'yes';
				
		} catch (Exception $Exception) {
			$ret['error'] =  "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( );
		}

	} else {
		//error: invalid doctor id
		$ret['error'] = "Invalid Condition.";
	}

} else {
	//user not logged in, show error message
	$ret['error'] = 'Please login first.';
}

header('Content-Type: application/json');
echo json_encode($ret);
exit;