<?php
//deny direct access


$docid = "";
$docemail = "";

if (isset($_POST['btnsetsave']))
{
	$_POST = array_map_deep($_POST, 'sanitize');
	$_POST = array_map_deep($_POST, 'trim');

	$userEmail = isset( $_POST["userEmail"] ) ? strip_tags( $_POST['userEmail'] ) : '';
    $userFname = $_POST["userFname"];	
	$userLname = $_POST["userLname"];	
	$userTitle = $_POST["userTitle"];	
	$userAddress = $_POST["userAddress"];	
	$userGender = $_POST["userGender"];	
	$userDob = $_POST["userDob"];	
	$userCountry = $_POST["userCountry"];	
	$userState = $_POST["userState"];	
	$userCity = $_POST["userCity"];	
	$userZip = $_POST["userZip"];	
	$userTaxId = $_POST["userTaxId"];	
	$userNationalId = $_POST["userNationalId"];	
	$userPhoneHome = $_POST["userPhoneHome"];	
	$userPhoneCellular = $_POST["userPhoneCellular"];	
	$userPhoneBusiness = $_POST["userPhoneBusiness"];	
	$userFax = $_POST["userFax"];	
	$userPic = "";
	$secureVideoId = isset($_POST['secureVideoId']) ? $_POST['secureVideoId'] : '';
	$userTimeZone = isset($_POST['userTimeZone']) ? strip_tags( $_POST['userTimeZone'] ) : '';
	
	$err = "";
	if ($userFname=="")
	{
		$err = $err . "-Enter first name.";
	}
	if ($userLname=="")
	{
		$err = $err . "-Enter lastname name.";
	}
	if ($userAddress=="")
	{
		$err = $err . "-Enter Address.";
	}

	if ($userCountry=="")
	{
		$err = $err . "-Enter Country.";
	}
	if ($userState=="")
	{
		$err = $err . "-Enter State.";
	}
	
	if ($userCity=="")
	{
		$err = $err . "-Enter City.";
	}
	

	if ($userZip=="")
	{
		$err = $err . "-Enter Zip.";
	}
	if ($userPhoneHome=="")
	{
		$err = $err . "-Enter Home Phone.";
	}
	if ($userPhoneCellular=="")
	{
		$err = $err . "-Enter Cellular Phone.";
	}
	if ($userPhoneBusiness=="")
	{
		$err = $err . "-Enter Business Phone.";
	}
	if ($err=="")
	{ 
		if ( isset($_FILES['featured_image']['name']) && $_FILES['featured_image']['name'] != ''  ) {
			//delete old image
			/* if ( isset($data['userPicture2']) && $data['userPicture2'] != '' ) {
				unlink(SITE_ROOT . 'images/doctors/full/' . $data['userPicture2']);
			} */
			 // unlink(SITE_ROOT.'images/doctors/full/' . $data['featured_image']['name']);

			//add new image
			$image = uploadDoctorPicture();

			if ( $image['status'] == 'success' ) {
				
				$userPic = $data['userPicture'] = $image['data']['img1'];
				// $_SESSION["mvdoctorVisitoruserPicture"] = $image['data']['img2'];
				 
				 try {
					$query = "UPDATE " . DB_PREFIX . "doctors set featured_image = ? WHERE userEmail = ?";
					$result = $db->prepare($query);
					$result->execute(array($userPic,$userEmail))	;
					} 
				catch (Exception $Exception) 
					{
						exit("DataBase Error {$Exception->getCode()}:" . $Exception->getMessage());
					}
				
			} else {
				die ("Image Upload Error" . $image['data']);
			}
		}

		
		try 
		{
			//check for snap id user
			require_once $siteRoot .'securevideo/securevideo_lib.php';
			$secure_lib = new Securevideo_Lib();

			if ($secureVideoId == '') {
				//create doctor account
				$patient_data = array(
						"FullName"      => $userFname . ' ' . $userLname,
						"EmailAddress"  => $userEmail,
						"TimeZoneWindowsId" => isset( $userTimeZone ) && $userTimeZone != '' ? $userTimeZone : 'UTC',
						//"SmsNumber"     => $data['userPhoneHome'],
						'HelpNumber' => '212-555-5555',
						'SystemRoles'   => 'H,P',
						'ShouldAutoDeliverCode' => 'E' //send emails
				);

				$patient_create_ret = $secure_lib->createNewUser($patient_data);

				if ( $patient_create_ret['status'] == '1' ) {
					$secureVideoId = $patient_create_ret['data']->SystemUserId;
				} else {
					//display error message
					$_SESSION['success_result']['error_msg'] = $patient_create_ret['message'];
				}
			} else {
				//update securevideo user
				$patient_data = array(
						"FullName"      => $userFname . ' ' . $userLname,
						"EmailAddress"  => $userEmail,
						"TimeZoneWindowsId" => isset( $userTimeZone ) && $userTimeZone != '' ? $userTimeZone : 'UTC',
						//"SmsNumber"     => $data['userPhoneHome'],
						'HelpNumber' => '212-555-5555',
						'SystemRoles'   => 'H,P',
						'ShouldAutoDeliverCode' => 'E' //send emails
				);

				$patient_create_ret = $secure_lib->updateUser($secureVideoId, $patient_data);

				if ( $patient_create_ret['status'] == '1' ) {
					$secureVideoId = $patient_create_ret['data']->SystemUserId;
				} else {
					//display error message
					$_SESSION['success_result']['error_msg'] = $patient_create_ret['message'];
				}
			}



			/* Store new hash in db */
			$query = "UPDATE " . DB_PREFIX . "users set secureVideoId = ?, userTimeZone = ?, userFname = ?, userLname = ?,userTitle=?,userAddress=?,userGender=?,userDob=?,userCountry=?,userState=?,userCity=?,userZip=?,userTaxId=?,userNationalId=?,userPhoneHome=?,userPhoneCellular=?,userPhoneBusiness=?,userFax=? WHERE userId = ?";
			$result = $db->prepare($query);
			
			$result->execute(array($secureVideoId, $userTimeZone, $userFname , $userLname, $userTitle,$userAddress,$userGender,$userDob,$userCountry,$userState,$userCity,$userZip,$userTaxId,$userNationalId,$userPhoneHome,$userPhoneCellular,$userPhoneBusiness,$userFax,$_SESSION["mvdoctorVisitornUserId"]));

			$_SESSION["mvdoctorVisitorUserFname"] = sanitize( $userFname );
			$_SESSION["mvdoctorVisitorUserLname"] = sanitize( $userLname );
			$_SESSION["mvdoctorVisitorUserTitle"] = sanitize( $userTitle );

			//update doctors table
			$doctor_name = $userFname . ' ' . $userLname;
			$query = 'UPDATE `' . DB_PREFIX . 'doctors` SET doctor_name = ? WHERE id = ?';
			$st = $db->prepare( $query );
			$st->execute( array($doctor_name, $_SESSION['mvdoctorID']) );
		}
		catch (Exception $Exception) 
		{
			exit("DataBase Error {$Exception->getCode()}:" . $Exception->getMessage());
		}
		$_SESSION['success_result']['success_msg']= "Your Information has been updated successfully.";
    }
	else
	{
		$_SESSION['success_result']['error_msg'] = $err;
	} 
	header('location:'.$_SERVER["HTTP_REFERER"]);
}

if (isset($_POST['removePicFlag']))
{
	$userEmail = $_POST["userEmailID"];
	try 
	{
		$imageName = $_POST["imageName"];
		$userImage = '';
		unlink(SITE_ROOT . 'images/doctors/full/' . $imageName);
		
		$query = "UPDATE " . DB_PREFIX . "doctors set featured_image = ? WHERE userEmail = ?";
		$result = $db->prepare($query);
		$result->execute(array($userImage,$userEmail))	;
	} 
	catch (Exception $Exception) 
	{
		exit("DataBase Error {$Exception->getCode()}:" . $Exception->getMessage());
	}
	$_SESSION['success_result']['success_msg']= "Your Image has been removed successfully.";
	header('location:'.$_SERVER["HTTP_REFERER"]);
}


if ( isset($_POST['doctor_edit'] )) 
{

$tab = "workplaces";
$_SESSION['success_result']['tab'] = "workplaces";


    $docEmail = $_SESSION['mvdoctorVisitorUserEmail'];
	
	if(isset($_POST['data']['work_places'])){
	
	$work_places = $_POST['data']['work_places'];
    $data = array_map_deep($_POST['data'], 'stripslashes');
	$data['work_places'] = base64_encode(serialize($data['work_places']));
	}
	else{
		$data['work_places'] ='';
	}
	$result = fastInsertUpdate( 'update', DB_PREFIX . 'doctors', $data, array(), array('userEmail' => $docEmail) );
    $_SESSION['success_result']['success_msg']= '';
	if(isset($_POST['action']) && $_POST['action']!='' )
	{
		/* echo $_POST['action'];
		exit; */
		switch($_POST['action']){
			case 'add':
				$_SESSION['success_result']['success_msg'] = "Your workplace has been Added successfully.";
				break;
			case 'delete':
				$_SESSION['success_result']['success_msg']= "Your workplace has been Deleted successfully.";
				break;
			case 'update':
				$_SESSION['success_result']['success_msg']= "Your workplace has been Updated successfully.";	
				break;
			
			
		}
	}
	
	header('location:'.$_SERVER["HTTP_REFERER"]);
	
	
}

if ( isset($_POST['education_edit'] )) 
{


$_SESSION['success_result']['tab'] = "education";


    $docEmail = $_SESSION['mvdoctorVisitorUserEmail'];
	
	if(isset($_POST['data']['education'])){
	
	$education = $_POST['data']['education'];
    $data = array_map_deep($_POST['data'], 'stripslashes');
	$data['education'] = base64_encode(serialize($data['education']));
	}
	else{
		$data['education'] ='';
	}
	$result = fastInsertUpdate( 'update', DB_PREFIX . 'doctors', $data, array(), array('userEmail' => $docEmail) );
    $_SESSION['success_result']['success_msg']= '';
	if(isset($_POST['action']) && $_POST['action']!='' )
	{
		/* echo $_POST['action'];
		exit; */
		switch($_POST['action']){
			case 'add':
				$_SESSION['success_result']['success_msg'] = "Your education has been Added successfully.";
				break;
			case 'delete':
				$_SESSION['success_result']['success_msg']= "Your education has been Deleted successfully.";
				break;
			case 'update':
				$_SESSION['success_result']['success_msg']= "Your education has been Updated successfully.";	
				break;
			
			
		}
	}
	
	header('location:'.$_SERVER["HTTP_REFERER"]);
	
	
}

if ( isset($_POST['award_edit'] )) 
{

		$tab = "award";
		$_SESSION['success_result']['tab'] = "award";
    $docEmail = $_SESSION['mvdoctorVisitorUserEmail'];
	
	if(isset($_POST['data']['awards'])){
	
	$awards = $_POST['data']['awards'];
    $data = array_map_deep($_POST['data'], 'stripslashes');
	$data['awards'] = base64_encode(serialize($data['awards']));
	}
	else{
		$data['awards'] ='';
	}
	$result = fastInsertUpdate( 'update', DB_PREFIX . 'doctors', $data, array(), array('userEmail' => $docEmail) );
    
//	$_SESSION['success_result']['success_msg'] "Your Awards record have been updated successfully.";	
	 $_SESSION['success_result']['success_msg'] = '';
	if(isset($_POST['action']) && $_POST['action']!='' )
	{
		/* echo $_POST['action'];
		exit; */
		switch($_POST['action']){
			case 'add':
				$_SESSION['success_result']['success_msg'] ="Your Award record has been Added successfully.";
				break;
			case 'delete':
				$_SESSION['success_result']['success_msg']= "Your Award record has been Deleted successfully.";
				break;
			case 'update':
				$_SESSION['success_result']['success_msg'] ="Your Award record has been Updated successfully.";	
				break;
			
			
		}
	}
	header('location:'.$_SERVER["HTTP_REFERER"]);
}

if ( isset($_POST['procedure_edit'] )) 
{

	$tab = "procedure";
	$_SESSION['success_result']['tab'] ="procedure";
    $docEmail = $_SESSION['mvdoctorVisitorUserEmail'];
	if(isset($_POST['data']['procedure_attempted']))
	{
	$procedures = $_POST['data']['procedure_attempted'];
    $data = array_map_deep($_POST['data'], 'stripslashes');
	
	
	$data['procedure_attempted'] = base64_encode(serialize($data['procedure_attempted']));
	}
	else {
	$data['procedure_attempted'] ="";
	}
	$result = fastInsertUpdate( 'update', DB_PREFIX . 'doctors', $data, array(), array('userEmail' => $docEmail) );
    
	//$_SESSION['success_result']['success_msg'] " have been updated successfully.";	
	 $_SESSION['success_result']['success_msg'] ='';
	if(isset($_POST['action']) && $_POST['action']!='' )
	{
		/* echo $_POST['action'];
		exit; */
		switch($_POST['action']){
			case 'add':
				$_SESSION['success_result']['success_msg'] ="Your Procedure record has been Added successfully.";
				break;
			case 'delete':
				$_SESSION['success_result']['success_msg'] ="Your Procedure record has been Deleted successfully.";
				break;
			case 'update':
				$_SESSION['success_result']['success_msg'] ="Your Procedure record has been Updated successfully.";	
				break;
			
			   
		}
	}
	header('location:'.$_SERVER["HTTP_REFERER"]);
}

if ( isset($_POST['condition_edit'] )) 
{

	$tab = 'condition';
	$_SESSION['success_result']['tab'] = 'condition';
    $docEmail = $_SESSION['mvdoctorVisitorUserEmail'];
	if(isset($_POST['data']['conditions_treated']))
	{
	$conditions = $_POST['data']['conditions_treated'];
    $data = array_map_deep($_POST['data'], 'stripslashes');
	
	
	$data['conditions_treated'] = base64_encode(serialize($data['conditions_treated']));
	}
	else {
	$data['conditions_treated'] ="";
	}
	$result = fastInsertUpdate( 'update', DB_PREFIX . 'doctors', $data, array(), array('userEmail' => $docEmail) );
    
	//$_SESSION['success_result']['success_msg'] "Conditons records have been updated successfully.";
		 $_SESSION['success_result']['success_msg'] ='';
	if(isset($_POST['action']) && $_POST['action']!='' )
	{
		/* echo $_POST['action'];
		exit; */
		switch($_POST['action']){
			case 'add':
				$_SESSION['success_result']['success_msg']= "Your Conditon record has been Added successfully.";
				break;
			case 'delete':
				$_SESSION['success_result']['success_msg']= "Your Conditon record has been Deleted successfully.";
				break;
			case 'update':
				$_SESSION['success_result']['success_msg']= "Your Conditon record has been Updated successfully.";	
				break;
			
			
		}
	}
	header('location:'.$_SERVER["HTTP_REFERER"]);
}

if ( isset($_POST['profile_edit'] )) 
{
$tab = "profile";
$_SESSION['success_result']['tab'] = "profile";
	
	$docid = $_SESSION['mvdoctorVisitornUserId'];
	$docEmail = $_SESSION['mvdoctorVisitorUserEmail'];
	$data['specialization'] = $_POST['data']['specialization'];	
	$data['degree'] = $_POST['data']['degree'];	
	$data['fees'] = $_POST['data']['fees'];	
	$data['location'] = $_POST['data']['location'];	
	$data['experience'] = $_POST['data']['experience'];	
   
   $data = array_map_deep($_POST['data'], 'stripslashes');
	
	        //$result = fastInsertUpdate( 'update', DB_PREFIX . 'doctors', $data, array(), array('id' => $actId) );
			$result = fastInsertUpdate( 'update', DB_PREFIX . 'doctors', $data, array(), array('userEmail' => $docEmail) );
    
	$_SESSION['success_result']['success_msg'] ="Your profile settings are updated successfully.";
	header('location:'.$_SERVER["HTTP_REFERER"]);
}


if ( isset($_POST['experience_edit'] )) 
{
	$tab = 'biodata';
	$_SESSION['success_result']['tab'] = 'biodata';
	$experience_details = $_POST['data']['experience_details'];
	
	
	$data['experience_details'] = $experience_details;
    $data = array_map_deep($_POST['data'], 'stripslashes');
	$docid = $_SESSION['visitorDocId'];
    $result = fastInsertUpdate( 'update', DB_PREFIX . 'doctors', $data, array(), array('id' => $docid) );
    
	$_SESSION['success_result']['success_msg'] ="Your experience has been updated successfully.";	
	header('location:'.$_SERVER["HTTP_REFERER"]);
}

try
{
	
	if (isset($_POST['education']) || (isset($_POST['speciality'])) ) 
	{
		$docid = $_SESSION['visitorDocId'];
		$var = "";
		if (isset($_POST['education']))
		{
			$tab = 'education';
			$_SESSION['success_result']['tab'] = 'education';
			$var = "education";
		}
		if (isset($_POST['speciality']))
		{
			$tab = 'speciality';
			$_SESSION['success_result']['tab'] = 'speciality';
			$var = "speciality";
			
		}
		
		//delete old categories
		
		try {
			$sql = "DELETE FROM `" . DB_PREFIX . "category_data` WHERE postId = ? AND postType = ? and categoryType=?";
			$st = $db->prepare( $sql );
			$st->execute( array($docid, 'doctors',$var ) );
		} catch (Exception $Exception) {
			exit("DataBase Error {$Exception->getCode()}:" . $Exception->getMessage());
		}
	
		if(isset($_POST['category'])){
		$category = array_map_deep($_POST['category'], 'intval');
		
		//now insert categories
		$sql = "INSERT INTO `" . DB_PREFIX . "category_data` (categoryId, categoryType, postId, postType) VALUES ";
		$insertQuery = array();
		$insertData = array();
		$n = 0;

		foreach ($category as $cat => $cat_ids) {

			foreach ($cat_ids as $cat_id) {
				$insertQuery[] = "(:categoryId{$n}, :categoryType{$n}, :postId{$n}, :postType{$n})";
				$insertData['categoryId' . $n] = $cat_id;
				$insertData['categoryType' . $n] = $cat;
				$insertData['postId' . $n] = $docid;
				$insertData['postType' . $n] = 'doctors';
				$n++;
			}
		}


		if (!empty($insertQuery)) 
		{

			try {

				$sql .= implode(', ', $insertQuery);
				$stmt = $db->prepare($sql);
				$stmt->execute($insertData);

				/* $url = "index.php?do=$do&page=$sub_page&act=edit&docid=$docid&msg=2";
				redirectPage($url);
				exit; */

			} catch (Exception $Exception) {
				exit("DataBase Error {$Exception->getCode()}:" . $Exception->getMessage());
			}
		}
		}
		
		if(isset($_POST['education-settings'])){
		$tab = 'education';
		$_SESSION['success_result']['tab'] = 'education';
		$_SESSION['success_result']['success_msg']= "Your education settings have been updated successfully.";
		}
		elseif(isset($_POST['speciality-settings'])){
		$tab = 'speciality';
		$_SESSION['success_result']['tab'] = 'speciality';
		$_SESSION['success_result']['success_msg'] ="Your specialization settings have been updated successfully.";
		}
		elseif(isset($_POST['language-settings'])){
		$tab = 'language';
		$_SESSION['success_result']['tab'] ='language';
		$_SESSION['success_result']['success_msg'] ="Your language settings have been updated successfully.";
		}
		header('location:'.$_SERVER["HTTP_REFERER"]);
    }
}
catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }
?>