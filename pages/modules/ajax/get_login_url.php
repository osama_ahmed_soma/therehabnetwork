<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

$ret = array();
$error = false;

//check user is logged in or not
if( !isset($_SESSION["mvdoctorVisitornUserId"]) && !isset($_SESSION["mvdoctorVisitorSecureVideoId"]) ) {
    $ret['error'][] = 'Please login to first to view this resource.';

    header('Content-Type: application/json');
    echo json_encode($ret);
    exit;
}

require_once $siteRoot .'securevideo/securevideo_lib.php';
$secure_lib = new Securevideo_Lib();

$login_url = $secure_lib->userLogin($_SESSION["mvdoctorVisitorSecureVideoId"]);

if ( $login_url['status'] == '1') {

    $ret['success'] = 'success';
    $ret['url'] = $login_url['data']->LoginUri;

}


header('Content-Type: application/json');
echo json_encode($ret);
exit;
