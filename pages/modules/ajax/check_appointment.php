<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

$ret = array();
$error = false;

//check user is logged in or not
if( !isset($_SESSION["mvdoctorVisitornUserId"]) ) {
    $ret['error'][] = 'Please login to first to view this resource.';

    header('Content-Type: application/json');
    echo json_encode($ret);
    exit;
}


//sanitize user inputs
$data = array_map_deep($_POST, 'sanitize');
$data = array_map_deep($data, 'trim');

//get user inputs
$years          = isset($data['years']) ? intval($data['years']) : '';
$months         = isset($data['months']) ? intval($data['months']) + 1 : '';
$date           = isset($data['date']) ? intval($data['date']) : '';
$plan_id        = isset($data['plan_id']) ? intval($data['plan_id']) : '';
$doctorId       = isset($data['doctorId'] ) ? intval($data['doctorId']) : '';

$patient_concern    = isset($data['patient_concern']) ? base64_encode(serialize($data['patient_concern'])) : base64_encode(serialize(array()));

$task           = isset($data['task']) ? $data['task'] : '';
$name_on_card   = isset($data['name_on_card']) ? $data['name_on_card'] : '';
$address_1      = isset($data['address_1']) ? $data['address_1'] : '';
$address_2      = isset($data['address_2']) ? $data['address_2'] : '';
$userCity       = isset($data['userCity']) ? $data['userCity'] : '';
$userCountry    = isset($data['userCountry']) ? $data['userCountry'] : '';
$userState      = isset($data['userState']) ? $data['userState'] : '';
$userZip        = isset($data['userZip']) ? $data['userZip'] : '';
$userPhone      = isset($data['userPhone']) ? $data['userPhone'] : '';
$full_name      = isset($data['name_on_card']) && $data['name_on_card'] != '' ? $data['name_on_card'] : $_SESSION["mvdoctorVisitorUserFname"] . ' ' . $_SESSION["mvdoctorVisitorUserLname"];


//check for valid doctor
$query = "SELECT doc.id, doc.userEmail, doc.stripeSecretKey, usr.secureVideoId FROM `" . DB_PREFIX . "doctors` as doc, `" . DB_PREFIX ."users` as usr
        WHERE doc.id = ? AND doc.status = 'publish' AND doc.userEmail = usr.userEmail LIMIT 1";
$st = $db->prepare($query);
$st->execute(array($doctorId));

if ( ! $st->rowCount() ) {
    $error = true;
    $ret['error'][] = "Invalid doctor selected.";
} else {
    $doctor_data = $st->fetch();
}

//check user input for date
if ($years == '' || $months == '' || $date == '') {
    $ret['error'][] = "Invalid date and time selected ($years-$months-$date). Please check your input.";
    $error = true;
}

//get timing information and check for valid timing added
$plan_data = array();
if ($error == false) {
    try {
        $query = "SELECT `id`, `dayId`, `duration`, `startTime`, `endTime` FROM `" . DB_PREFIX . "doctor_timing` WHERE `id` = ? AND `doctorId` = ? LIMIT 1";
        $st = $db->prepare($query);
        $st->execute(array($plan_id, $doctorId));

        if (!$st->rowCount()) {
            $ret['error'][] = "Invalid plan selected. Please check your input.";
            $error = true;
        } else {
            $plan_data = $st->fetch();
            $start_time_string = date('h:i:s A', $plan_data['startTime']);
            $end_time_string = date('h:i:s A', $plan_data['endTime']);
            $hours = date('h', $plan_data['startTime']);
            $minutes = date('i', $plan_data['startTime']);

            $schedule_date = "$years-$months-$date";
            $patient_plan_start = strtotime("$years-$months-$date $start_time_string");
            $patient_plan_ends = strtotime("$years-$months-$date $end_time_string");

            //get pricing data for selected plan
            try {
                $query = "SELECT `price` FROM `" . DB_PREFIX . "plans` WHERE doctorId = ? AND `time` = ? LIMIT 1";
                $st = $db->prepare($query);
                $st->execute(array($doctorId, $plan_data['duration']));

                if (!$st->rowCount()) {
                    $ret['error'][] = "Doctor didn't added any consultation plans yet, please contact with doctor.";
                } else {
                    $tmp = $st->fetch();
                    $plan_data['price'] = $tmp['price'];
                }


            } catch (Exception $ex) {
                $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
            }

        }

    } catch (Exception $ex) {
        $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
    }
}

//check date
if ($error == false) {

    //check user input date is today and future
    $date_diff = check_date_relavence("$years-$months-$date");

    if ( $date_diff < 0  ) {
        //date is in past
        $ret['error'][] = "Selected date ($years-$months-$date) is from past. Please select another date.";
        $error = true;
    } elseif ( $date_diff == 0 ) {
        //check for current time here
        $current_time = strtotime("$years-$months-$date " . date('h:i:s A'));
        $patient_start_time = strtotime("$years-$months-$date $start_time_string");
        if ($current_time >= $patient_start_time) {
            $error = true;
            $ret['error'][] = "Invalid time selected. Selected time is past to current server time. Current server time is: " . date('h:i:s A');
        }
    }

}

//now make sure that, this time frame is not used by another patient in schedule table
if ($error == false) {
    try {
        $new_time = strtotime("$years-$months-$date $start_time_string");

        $query = "SELECT id FROM `" . DB_PREFIX . "schedule` WHERE `startTime` = ? AND `doctorId` = ?";
        $st = $db->prepare($query);
        $st->execute(array($new_time, $doctorId));

        if ($st->rowCount()) {
            $error = true;
            $ret['error'][] = "Selected time frame is already assigned to another user. Please select another timestamp.";
        }

    } catch (Exception $ex) {
        $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
    }
}

//now check securevideo user id for both doctor and patient
if ( $_SESSION['mvdoctorVisitorSecureVideoId'] == 0 ) {
    $ret['error'][] = "You need to complete your profile settings first before a consultation. <a href='" . HTTP_SERVER . 'patient.html">Click here</a> to update your profile.';
    $error = true;
}
//now check for doctor account
if ( intval( $doctor_data['secureVideoId'] ) == 0 ) {
    $ret['error'][] = 'Doctor profile settings is not completed yet. Please contact with doctor. ';
    $error = true;
}

if ( $error == false ) {
    if ( $task == 'pay_now' ) {
        //charge patient via stripe
        require_once SITE_ROOT . 'external/stripe-php/init.php';

        //below is just to check stripe api is working or not
        try {

            $ip =   getenv( 'HTTP_CLIENT_IP' )      ?: getenv( 'HTTP_X_FORWARDED_FOR' ) ?:
                getenv( 'HTTP_X_FORWARDED' )    ?: getenv( 'HTTP_FORWARDED_FOR' ) ?:
                    getenv( 'HTTP_FORWARDED' )      ?: getenv( 'REMOTE_ADDR' );

            $description_text = "My Virtual Doctor charge patient: $full_name, for plan id: {$plan_data['id']}, " . number2time( $plan_data['duration'] );

            \Stripe\Stripe::setApiKey($doctor_data['stripeSecretKey']);

            $charge = \Stripe\Charge::create(array(
                "amount" => $plan_data['price'] * 100,
                "currency" => "usd",
                "source" => $data['stripeToken'], // obtained with Stripe.js
                "description" => $description_text,
                "metadata" => array(
                    'ip_address' => $ip,
                    'full_name' => $full_name,
                    'address_line_1' => $address_1,
                    'address_line_2' => $address_2,
                    'country' => $userCountry,
                    'city' => $userCity,
                    'zip' => $userZip,
                    'state' => $userState,
                    'patient_id' => $_SESSION["mvdoctorVisitornUserId"],
                    'plan_id' => $plan_id
                )
            ));

            $charge_obj = $charge->__toArray( true );

            $charge_id = $charge_obj['id'];

            //get charge via api to check payment is completed or not
            $retrive_charge = \Stripe\Charge::retrieve( $charge_id );
            $retrive_charge_obj = $retrive_charge->__toArray( true );

            if ( $retrive_charge_obj['status'] == 'succeeded' ) {
                //put all data to database
                $query = "INSERT INTO `" . DB_PREFIX . "schedule` (`patientId`, `doctorId`, `patient_concern`, `scheduleDate`, `startTime`,
                `endTime`, `scheduleYear`, `scheduleMonth`, `scheduleDay`, `startH`, `startM`, `planId`,
                `transactionId`, `added`) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                $st = $db->prepare($query);
                $st->execute(array(
                    $_SESSION["mvdoctorVisitornUserId"],
                    $doctorId,
                    $patient_concern,
                    $schedule_date,
                    $patient_plan_start,
                    $patient_plan_ends,
                    $years,
                    $months,
                    $date,
                    $hours,
                    $minutes,
                    $plan_id,
                    $charge_id,
                    time()
                ));

                $insert_id = $db->lastInsertId();

                //add securevideo appointment
                require_once $siteRoot .'securevideo/securevideo_lib.php';
                $secure_lib = new Securevideo_Lib();

                /*
                {
                 "ScheduleTs": "2014-04-16T15:00:00",
                 "IsRecorded": true,
                 "Participants": [
                 {
                 "SecureVideoUserId": "12"
                 }
                 ]
                }
                 */
                $scheduleTs = date('Y-m-d', $patient_plan_start) . 'T' . date('H:i:s', $patient_plan_start);
                $session_data = array(
                    'ScheduleTs' => $scheduleTs,
                    'IsRecorded' => false,
                    'Participants' => array(
                        array(
                            'SecureVideoUserId' => $_SESSION['mvdoctorVisitorSecureVideoId']
                        )
                    )
                );

                $appointment_ret = $secure_lib->createNewSession($doctor_data['secureVideoId'], $session_data);

                //for debug purpose
                mail('zikubd@gmail.com', 'Secure Video Appointment response', print_r($appointment_ret,true));


                if ( $appointment_ret['status'] == '1' ) {
                    $appointmentId = $appointment_ret['data']->SessionId;
                    //update consultation table

                    $query = "UPDATE `" . DB_PREFIX . "schedule` SET appointmentId = ? WHERE id = ? LIMIT 1";
                    $st = $db->prepare($query);
                    $st->execute( array($appointmentId, $insert_id) );

                    $ret['success'] = 'yes';
                    $ret['message'][] = "<strong>Congratulations!</strong> Schedule is successfully placed.";

                } else {
                    $ret['error'][] = $appointment_ret['message'];
                    $ret['error'][] = "Please contact with doctor";
                }
            }

        } catch (Exception $e) {
            $error = true;
            $fail_message = $e->getMessage();
            $ret['error'][] = $fail_message;
        }

    }
    else {
        //nothing is here
    }

}



header('Content-Type: application/json');
echo json_encode($ret);
exit;