<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

$ret = array();

//check user is logged in or not
if( !isset($_SESSION["mvdoctorVisitornUserId"]) ) {
    $ret['error'] = 'Please login to add comment.';
} else {
    //check required variable
    $post_id = isset( $_POST['post_id'] ) ? intval( $_POST['post_id'] ) : 0;
    $comment = isset( $_POST['blog_comments'] ) ? stripslashes(strip_tags($_POST['blog_comments'])) : '';
    $time = time();
    $error = false;
    //check article
    if ( $post_id > 0 ) {
        //check this post exits or not
        $sql = 'SELECT id from `' . DB_PREFIX . 'health_tips` WHERE id = ?';
        $st = $db->prepare($sql);
        $st->execute(array($post_id));
        if ( !$st->rowCount() ) {
            $error = true;
            $ret['error'] = 'Artical doesn\'t exist on database.';
        }
    } else {
        $error = true;
        $ret['error'] = 'Invalid Article ID';
    }

    //check for comment
    if ( $error == false && $comment == '' ) {
        $error = true;
        $ret['error'] = 'Please write a comment first.';
    }

    if ( $error == false ) {
        $ip = getenv('HTTP_CLIENT_IP') ?: getenv('HTTP_X_FORWARDED_FOR') ?:
            getenv('HTTP_X_FORWARDED') ?: getenv('HTTP_FORWARDED_FOR')?:
                getenv('HTTP_FORWARDED') ?: getenv('REMOTE_ADDR');

        $sql = 'INSERT INTO `' . DB_PREFIX . 'comments` (userId,postId,comment,ip,added) values(?, ?, ?, ?, ?)';

        try {
            $st = $db->prepare($sql);
            $st->execute(array($_SESSION["mvdoctorVisitornUserId"], $post_id, $comment, $ip, $time));

            if ( isset($_SESSION["mvdoctorVisitoruserPicture"]) && $_SESSION["mvdoctorVisitoruserPicture"] != '' ) {
                $user_image = HTTP_SERVER . 'images/user/thumb/' . $_SESSION["mvdoctorVisitoruserPicture"];
            } else {
                $user_image = get_gravatar($_SESSION["mvdoctorVisitorUserEmail"],87);
            }

            $user_name = $_SESSION["mvdoctorVisitorUserFname"] . ' ' . $_SESSION["mvdoctorVisitorUserLname"];
            $time_ago = timeAgo( $time );
            $comment = htmlentities($comment);
            $str = <<<EOD
                <div class="post-heading">
                    <div class="pull-left image">
                        <img src="$user_image" class="img-circle avatar" alt="user profile image">
                    </div>
                    <div class="pull-left meta">
                        <div class="title h5">
                            <b>$user_name</b> <h6 class="time">$time_ago</h6>
                        </div>
                        <p class="des">$comment</p>
                    </div>
                </div>
EOD;
            $ret['success'] = 'yes';
            $ret['comment_html'] = $str;
        } catch (Exception  $Exception) {
            exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
        }

    }
}

header('Content-Type: application/json');
echo json_encode($ret);
exit;