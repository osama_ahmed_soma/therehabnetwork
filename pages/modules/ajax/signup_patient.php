<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

//check call is from $do=ajax
if ( $do !== 'ajax' ) {
    die('You are not authorized to view this page');
}


/*require_once $siteRoot .'securevideo/securevideo_lib.php';
$secure_lib = new Securevideo_Lib();*/

try {
    $newDB = new PDO('mysql:host=localhost;dbname=myvirtua_jul15', 'myvirtua_jul15', 'droitlab1#');
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}
$newDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

$ret = array();

if( isset($_POST['userEmail']) ) {
    $ret['post'] = $_POST;
    $userFname      = isset($_POST['userFname']) ? sanitize($_POST['userFname'],4) : '';
    $userLname      = isset($_POST['userLname']) ? sanitize($_POST['userLname'],4) : '';

    $userEmail      = isset($_POST['userEmail']) ? sanitize($_POST["userEmail"],4) : '';
//    $password       = isset($_POST['password']) ? sanitize($_POST['password'], 4) : '';

//    $userTimeZone   = isset($_POST['userTimeZone']) ? sanitize($_POST['userTimeZone'], 4) : '';
//    $consent        = isset($_POST['consent']) ? sanitize($_POST['consent'], 4) : '';

    if ( $userFname == '' ) {
        $ret['error'][] = '<p><strong>First Name</strong> field is required.<p>';
    }

    if ( $userLname == '') {
        $ret['error'][] = '<p><strong>Last Name</strong> field is required.<p>';
    }

    if( $userEmail == "" ) {

        $ret['error'][] = '<p><strong>Email address</strong> field is required.<p>';

    } elseif( $userEmail != "" && !filter_var($userEmail, FILTER_VALIDATE_EMAIL) ) {

        $ret['error'][] = "Please enter a valid <strong>Email Address</strong>.";

    } else {
        //check email exist on database or not
//        $sqlEmail = "SELECT * FROM " . DB_PREFIX . "users WHERE userEmail = ? or userLogin = ? AND userLevel = 4 LIMIT 1";
        $sqlEmail = "SELECT * FROM user WHERE user_email = ? LIMIT 1";

        /*$result = $db->prepare($sqlEmail);
        $result->execute( array($userEmail, $userEmail) );*/
        $result = $newDB->prepare($sqlEmail);
        $result->execute( array($userEmail) );

        if ( $result->rowCount() > 0 ) {
            $ret['error'][] = "<p><strong>Email address</strong> already exist in database. Please enter another email.</p>";
        }
    }

    /*if( $password == "" ) {
        $ret['error'][] = '<p><strong>Password</strong> field is required.</p>';

    } elseif( strlen($password) < 8 ) {
        $ret['error'][] = "<p><strong>Password</strong> must be 8 characters long!</p>";

    } else if(!preg_match('/[A-Z]+/', $password) && !preg_match('/[0-9]+/', $password) ) {
        $ret['error'][]  = "<p><strong>Password</strong> must have one capital letter, one number!</p>";

    }

    if ( $userTimeZone == '' ) {
        $ret['error'][] = '<p><strong>Time Zone</strong> field is required.</p>';
    }

    if ( $consent == '' ) {
        $ret['error'][] = '<p>Please agree with <strong>Terms of Use</strong></p>';
    }*/

    //create patient
    if ( !array_key_exists('error', $ret) ) {
        /*$patient_data = array(
            "FullName" => $userFname . ' ' . $userLname,
            "EmailAddress" => $userEmail,
            "TimeZoneWindowsId" => isset($userTimeZone) && $userTimeZone != '' ? $userTimeZone : 'UTC',
            //"SmsNumber"     => $data['userPhoneHome'],
            'HelpNumber' => '212-555-5555',
            'SystemRoles' => 'P',
            'ShouldAutoDeliverCode' => 'E' //send emails
        );

        $patient_create_ret = $secure_lib->createNewUser($patient_data);

        if ($patient_create_ret['status'] == '1') {
            $secureVideoId = $patient_create_ret['data']->SystemUserId;
        } else {
            //display error message
            $ret['error'][] = $patient_create_ret['message'];
        }*/
    }


    if ( !array_key_exists('error', $ret) ) {

        /*$options = [
            'cost' => 11,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        ];

        $user_key = getRandomString(20);
        $new_pass = password_hash(SITE_KEY . $password . $user_key, PASSWORD_DEFAULT, $options);*/

        function generateRandomString($length = 8)
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        $pass = generateRandomString();

            $query = "INSERT INTO `" . DB_PREFIX . "users`  (`secureVideoId`, `userFname`, `userLname`, `userTimeZone`, `userLogin`, `userKey`, `userPassword`, `userEmail`, `userLevel`, `userStatus`, `added`)
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, 4, 1, ?)";
            $query = "INSERT INTO `user`  (`user_name`, `user_email`, `user_password`, `type`)
                            VALUES (?, ?, ?, 'patient')";
            $result = $newDB->prepare($query);
            $result->execute(array(
                $userFname,
                $userEmail,
                $pass
            ));
            $lastInsertedID = $newDB->lastInsertId();
            $query2 = 'INSERT INTO `patient`  (`user_id`, `firstname`, `lastname`, `email`)
                            VALUES (?, ?, ?, ?)';
            $result2 = $newDB->prepare($query2);
            $result2->execute(array(
                $lastInsertedID,
                $userFname,
                $userLname,
                $userEmail
            ));

            //start new session

//            $_SESSION["mvdoctorVisitorUserFname"] = $userFname;
//            $_SESSION["mvdoctorVisitorUserLname"] = $userLname;
//            $_SESSION["mvdoctorVisitorUserLevel"] = '4';
//            $_SESSION["mvdoctorVisitorUserTitle"] = '';
//            $_SESSION["mvdoctorVisitornUserId"] = $db->lastInsertId();
//            $_SESSION["mvdoctorVisitorUserEmail"] = $userEmail;
//            $_SESSION['mvdoctorVisitorSecureVideoId'] = intval( $secureVideoId );

		// calling email service
		
		$url = HTTP_SERVER . 'app/email_api?userEmail=' . $userEmail . '&userFname=' . $userFname . '&userLname=' . $userLname . '&userPass=' . $pass;
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $url,
			CURLOPT_USERAGENT => 'Core PHP'
		));
		$resp = curl_exec($curl);

		if(!$curl){
			$ret['error'][] = 'Email Not Sent';
		}
        $ret['success'] = "Thank you for signing up! We just sent you an email with your new password for logging into My Virtual Doctor. Once you receive the email, you may login by clicking this link <a href=\"http://myvirtualdoctor.com/app/login\">Here</a>";
		curl_close($curl);

		$json = json_decode($resp);

		//var_dump($json);

		if (is_object($json) && isset($json->status) && $json->status !== 'sent') {
			$ret['error'][] = 'Email Not Sent';
		}
//		echo $resp;

		
    }

} else {
    $ret['error'][] = "Please enter a valid Email Address and Password.";
}
header('Content-Type: application/json');
echo json_encode($ret);
exit;
?>