<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

$ret = array();
$error = false;

//check user is logged in or not
if( !isset($_SESSION["mvdoctorVisitornUserId"]) ) {
    $ret['error'][] = 'Please login to first to view this resource.';

    header('Content-Type: application/json');
    echo json_encode($ret);
    exit;
}

$task = isset($_REQUEST['task']) ? strip_tags($_REQUEST['task']) : '';

switch ( $task ) {
    case 'add':
        $doctorId = $_SESSION["mvdoctorID"]; //doctorId
        $time = isset($_POST['time']) ? intval($_POST['time']) : '';
        $price = isset($_POST['price']) ? floatval($_POST['price']) : '';

        //validate user inputes
        if ( $doctorId == '' ) {
            $ret['error'][] = 'Please login to add timing.';
        }
        if ( $time == '' ) {
            $ret['error'][] = "Please select consultation time.";
        } elseif ( !in_array($time, array(1,2)) ) {
            $ret['error'][] = "Invalid time selected.";
        }

        if ( $price == '' ) {
            $ret['error'][] = 'Please enter a price.';
        } elseif( $price <= 1 ) {
            $ret['error'][] = 'Price value is invalid';
        }

        //check plan already exist or not
        try {
            $query = "SELECT * FROM `" . DB_PREFIX . "plans` WHERE `time` = ? AND doctorId = ?";
            $st = $db->prepare($query);
            $st->execute( array($time, $doctorId) );

            if ( $st->rowCount() ) {
                $ret['error'][] = "Consultation Duration already exists on database. Please select another consultaion duration.";
            }

        } catch (Exception $ex) {
            $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
        }

        if ( !array_key_exists('error', $ret) ) {
            //now insert into table
            try {
                $query = "INSERT INTO `" . DB_PREFIX . "plans` (`doctorId`, `time`, `price`, `status`, `added`)
                VALUES (?, ?, ?, ?, ?)";

                $st = $db->prepare($query);
                $st->execute( array($doctorId, $time, $price, '1', time()) );

                $ret['success'] = 'success';
                $ret['message'][] = 'Consultation plan added successfully.';

                $id = $db->lastInsertId();
                $time_text = number2time($time);
                $token = getToken();

                $select_options = '';
                foreach(array(1,2) as $key => $value) {
                    $sel = '';
                    if ( $time == $value ) {
                        $sel = 'selected="selected"';
                    }
                    $text = number2time($value);
                    $select_options .= "<option value='$value' $sel>$text</option>";
                }


                //now prepare the html
                $str = <<<EOD
            <tr class="row_{$id}_1">
                <td>{$time_text}</td>
                <td>\${$price}</td>
                <td class="text-center">
                    <a class='btn btn-info btn-xs' data-toggle="collapse" data-target="#consultations{$id}"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                    <a class='btn btn-default dark-pink'><span class="fa fa-minus-circle"></span> Delete</a>
                </td>
            </tr>
            <tr class="row_{$id}_2">
                <td colspan="4">
                    <!-- Edit Consultations Start here -->
                    <div class="collapse"  id="consultations{$id}">
                        <form method="post" action="index.php?do=ajax&action=add_consultation_plan" class="consultaionForm">
                            <input type="hidden" name="token" value="{$token}" />
                            <input type="hidden" name="task" value="edit" />
                            <input type="hidden" name="id" value="{$id}" />
                            <div class='form-row'>
                                <div class='col-xs-4 form-group required'>
                                    <label class='control-label'>Consultation Duration</label>
                                    <select name="time" class='form-control' data-live-search="true">
                                        {$select_options}
                                    </select>
                                </div>

                                <div class='col-xs-6 form-group required'>
                                    <div class='col-xs-12 row'><label class='control-label'>Add Price</label></div>
                                    <div class='col-xs-5 row'>
                                        <input class='form-control' name="price" value="{$price}" type='text' placeholder="Add Price">
                                    </div>
                                    <div class='col-xs-7 pull-right'>
                                        <button type="submit" class="btn btn-default dark-pink submit update"><i class="fa fa-pencil"></i> Update</button>
                                        <a class='btn btn-default dark-pink' data-toggle="collapse" data-target="#consultations{$id}"><span class="fa fa-ban"></span> Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Edit Consultations Ends here -->
                </td>
            </tr>
EOD;
            $ret['html'] = $str;

            } catch (Exception $Exception) {
                $ret['error'][] = "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( );
            }
        }

        break;

    case 'edit':
        $doctorId = $_SESSION["mvdoctorID"]; //doctorId
        $id = isset($_POST['id']) ? intval($_POST['id']) : '';
        $time = isset($_POST['time']) ? intval($_POST['time']) : '';
        $price = isset($_POST['price']) ? floatval($_POST['price']) : '';

        //validate user inputes
        if ( $doctorId == '' ) {
            $ret['error'][] = 'Please login to add timing.';
        }
        if ( $id == '' || $id <= 0 ) {
            $ret['error'][] = 'Invalid plan id.';
        }
        if ( $time == '' ) {
            $ret['error'][] = "Please select consultation time.";
        } elseif ( !in_array($time, array(1,2)) ) {
            $ret['error'][] = "Invalid time selected.";
        }

        if ( $price == '' ) {
            $ret['error'][] = 'Please enter a price.';
        } elseif( $price <= 1 ) {
            $ret['error'][] = 'Price value is invalid';
        }

        //check plan already exist or not
        try {
            $query = "SELECT * FROM `" . DB_PREFIX . "plans` WHERE `time` = ? AND doctorId = ? AND id != ?";
            $st = $db->prepare($query);
            $st->execute( array($time, $doctorId, $id) );

            if ( $st->rowCount() ) {
                $ret['error'][] = "Consultation Duration already exists on database. Please select another consultaion duration.";
            }

        } catch (Exception $ex) {
            $ret['error'][] = 'Invalid Query! File: ' . $ex->getFile() . ', Line: ' . $ex->getLine() . '<br>Error: ' . $ex->getMessage();
        }

        if ( !array_key_exists('error', $ret) ) {
            //now insert into table
            try {
                $query = "UPDATE `" . DB_PREFIX . "plans`  SET `time` = ?, `price` = ?
                WHERE `doctorId` = ? and `id` = ?";

                $st = $db->prepare($query);
                $st->execute( array( $time, $price, $doctorId, $id) );

                $ret['success'] = 'success';
                $ret['message'][] = 'Consultation plan updated successfully.';

                $time_text = number2time($time);
                $token = getToken();

                $select_options = '';
                foreach(array(1,2) as $key => $value) {
                    $sel = '';
                    if ( $time == $value ) {
                        $sel = 'selected="selected"';
                    }
                    $text = number2time($value);
                    $select_options .= "<option value='$value' $sel>$text</option>";
                }


                //now prepare the html
                $str1 = <<<EOD
                <td>{$time_text}</td>
                <td>\${$price}</td>
                <td class="text-center">
                    <a class='btn btn-info btn-xs' data-toggle="collapse" data-target="#consultations{$id}"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                    <a class='btn btn-default dark-pink'><span class="fa fa-minus-circle"></span> Delete</a>
                </td>
EOD;
                $str2 = <<<EOD
                <td colspan="4">
                    <!-- Edit Consultations Start here -->
                    <div class="collapse"  id="consultations{$id}">
                        <form method="post" action="index.php?do=ajax&action=add_consultation_plan" class="consultaionForm">
                            <input type="hidden" name="token" value="{$token}" />
                            <input type="hidden" name="task" value="edit" />
                            <input type="hidden" name="id" value="{$id}" />
                            <div class='form-row'>
                                <div class='col-xs-4 form-group required'>
                                    <label class='control-label'>Consultation Duration</label>
                                    <select name="time" class='form-control' data-live-search="true">
                                        {$select_options}
                                    </select>
                                </div>

                                <div class='col-xs-6 form-group required'>
                                    <div class='col-xs-12 row'><label class='control-label'>Add Price</label></div>
                                    <div class='col-xs-5 row'>
                                        <input class='form-control' name="price" value="{$price}" type='text' placeholder="Add Price">
                                    </div>
                                    <div class='col-xs-7 pull-right'>
                                        <button type="submit" class="btn btn-default dark-pink submit update"><i class="fa fa-pencil"></i> Update</button>
                                        <a class='btn btn-default dark-pink' data-toggle="collapse" data-target="#consultations{$id}"><span class="fa fa-ban"></span> Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Edit Consultations Ends here -->
                </td>
EOD;
                $ret['row1'] = $str1;
                $ret['row2'] = $str2;

            } catch (Exception $Exception) {
                $ret['error'][] = "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( );
            }
        }

        break;

    case 'delete':

        $doctorId = $_SESSION["mvdoctorID"]; //doctorId
        $id = isset($_POST['id']) ? intval($_POST['id']) : '';

        if ( $doctorId == '' ) {
            $ret['error'][] = 'Please login to add timing.';
        }
        if ( $id == '' || $id <= 0 ) {
            $ret['error'][] = 'Invalid plan id.';
        }

        if ( !array_key_exists('error', $ret) ) {
            //now insert into table
            try {
                $query = "DELETE FROM `" . DB_PREFIX . "plans` WHERE `doctorId` = ? and `id` = ? LIMIT 1";

                $st = $db->prepare($query);
                $st->execute( array($doctorId, $id) );

                $ret['success'] = 'success';
                $ret['message'][] = 'Consultation plan deleted successfully.';

            } catch (Exception $Exception) {
                $ret['error'][] = "DataBase Error {$Exception->getCode()}:" . $Exception->getMessage();
            }
        }

            break;
    default:


        break;
}



header('Content-Type: application/json');
echo json_encode($ret);
exit;