<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}?>
<div class="col-md-9  col-sm-12">
    <ul class="nav nav-tabs">
        <li class="<?php if ( $tab == 'overview' ) { echo 'active'; } ?>"><a data-toggle="tab" href="#home">Overview </a></li>
        <li class="<?php if ( $tab == 'experience' ) { echo 'active'; } ?>"><a data-toggle="tab" href="#experience">Experience</a></li>
        <li class="<?php if ( $tab == 'ratings' ) { echo 'active'; } ?>"><a data-toggle="tab" href="#ratings">Reviews</a></li>
       <!-- <li class="<?php if ( $tab == 'contact' ) { echo 'active'; } ?>"><a data-toggle="tab" href="#contact">Contact</a></li>-->
    </ul>

    <div class="tab-content">

        <!-- start home tab -->
        <div id="home" class="tab-pane fade <?php if ( $tab == 'overview' ) { echo 'active in'; } ?>">
            <div class="col-md-12">
                <h3>About</h3>
                <?php echo html_entity_decode($doctor_data['overview']); ?>
            </div>
            <br>

            <?php 
			if ( isset( $doctor_data['work_places']['chamber'] ) && !empty( $doctor_data['work_places']['chamber'] ) ){   ?>
                            <?php 
								$award_cnt  = count( $doctor_data['work_places']['chamber'] );  ?>
								<div class="col-md-12 col-sm-12 col-xs-12">
								  <?php 
								for ( $idx = 0; $idx < $award_cnt; $idx++ )  { 
							?>
                    <div class="Practicing-left">
                        <?php  if ( $idx == 0 ) {  ?><h2>Practicing Workplaces</h2><?php } ?>
                        <i class="fa fa-map-marker"></i> <p> <?php echo   $doctor_data['work_places']['chamber'][$idx]; ?></p>
                        <p class="Address-about" > <?php echo   $doctor_data['work_places']['address1'][$idx]; ?><br/>
                            <?php echo $doctor_data['work_places']['city'][$idx]; ?>, <?php echo $doctor_data['work_places']['state'][$idx]; ?>
                        </p>
                    </div>

                    <div class="Practicing-right">
                        
                               <h6><?php echo $doctor_data['work_places']['days'][$idx]; ?></h6>
                                <p><?php echo $doctor_data['work_places']['time'][$idx]; ?></p>
                           
                    </div>
                    <?php if ( $idx < $award_cnt-1 ) { echo "<hr style=\"clear: both;\">"; }  $idx++; ?>
                <?php } ?>
            </div>
            <?php } ?>

            <?php if ( isset($speciality_data) && is_array($speciality_data) && !empty($speciality_data) ): ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="Practicing-left">
                    <h2>Specializations</h2>
                </div>
                <br style="clear: both;">

                <?php foreach ( $speciality_data as $cat_data) { ?>
                    <div class="col-md-4" style="margin-bottom: 10px;">- <?php echo $cat_data['categoryName']; ?></div>
                <?php } ?>
            </div>
            <?php endif; ?>

        </div>
        <!-- end home tab -->

        <!-- start experience tab -->
        <div id="experience" class="tab-pane fade <?php if ( $tab == 'experience' ) { echo 'active in'; } ?>">
            <li class="Write-Review-heading"><i class="Patient-Reviews-icon fa fa-list-alt"></i>Practice Biodata</li>
            <div class="col-md-12 Experience-pra">
                <?php echo html_entity_decode($doctor_data['experience_details']); ?>
            </div>

            <?php if ( isset($language_data) && is_array($language_data) && !empty($language_data) ): ?>
                <li class="Write-Review-heading"><i class="Patient-Reviews-icon fa fa-list-alt"></i>Language ( spoken in Practice )</li>
                <div class="col-md-12">
                    <?php foreach ( $language_data as $cat_data) { ?>
                        <div class="col-md-4" style="margin-bottom: 10px;">- <?php echo $cat_data['categoryName']; ?></div>
                    <?php } ?>
                </div>
            <?php endif; ?>

            <?php if ( isset( $doctor_data['procedure_attempted']['procedure_name'] ) && !empty( $doctor_data['procedure_attempted']['procedure_name'] ) ): $procedure_attempted_cnt = count( $doctor_data['procedure_attempted']['procedure_name'] )  ?>
                <li class="Write-Review-heading"><i class="Patient-Reviews-icon fa fa-list-alt"></i>Procedures Attempted</li>
                <?php  for ( $idx = 0; $idx < $procedure_attempted_cnt; $idx++ ) {  ?>
                    <div class="col-md-12">
                        <h4>- <?php echo isset($doctor_data['procedure_attempted']['procedure_name'][ $idx ]) ? $doctor_data['procedure_attempted']['procedure_name'][ $idx ] : '';  ?></h4>
                        <p><?php echo isset($doctor_data['procedure_attempted']['procedure_description'][ $idx ]) ? $doctor_data['procedure_attempted']['procedure_description'][ $idx ] : '';  ?></p>
                    </div>
                <?php } ?>
            <?php endif; ?>

            <?php if ( isset( $doctor_data['conditions_treated']['condition_name'] ) && !empty( $doctor_data['conditions_treated']['condition_name'] ) ): $procedure_attempted_cnt = count( $doctor_data['conditions_treated']['condition_name'] )  ?>
                <li class="Write-Review-heading"><i class="Patient-Reviews-icon fa fa-list-alt"></i>Conditions Treated</li>
                <?php  for ( $idx = 0; $idx < $procedure_attempted_cnt; $idx++ ) {  ?>
                    <div class="col-md-12">
                        <h4>- <?php echo isset($doctor_data['conditions_treated']['condition_name'][ $idx ]) ? $doctor_data['conditions_treated']['condition_name'][ $idx ] : '';  ?></h4>
                        <p><?php echo isset($doctor_data['conditions_treated']['condition_description'][ $idx ]) ? $doctor_data['conditions_treated']['condition_description'][ $idx ] : '';  ?></p>
                    </div>
                <?php } ?>
            <?php endif; ?>

			   <?php if ( isset( $doctor_data['education']) && !empty( $doctor_data['education'] ) ): $edu_cnt = count( $doctor_data['education']['name'] );  ?>
                <li class="Write-Review-heading"><i class="Patient-Reviews-icon fa fa-list-alt"></i>Education & Training</li>
                <?php  for ( $idx = 0; $idx < $edu_cnt; $idx++ ) {  ?>
                    <div class="col-md-12">
                        <h4>- <?php echo isset($doctor_data['education']['name'][ $idx ]) ? $doctor_data['education']['name'][ $idx ] : '';  ?> (<?php echo isset($doctor_data['education']['institute'][ $idx ]) ? $doctor_data['education']['institute'][ $idx ]: '';  ?>) (<?php echo isset($doctor_data['education']['year'][ $idx ]) ? $doctor_data['education']['year'][ $idx ]: '';  ?>)</h4>
                       
                    </div>
                <?php } ?>
            <?php endif; ?>


            <?php if ( isset( $doctor_data['awards']['name'] ) && !empty( $doctor_data['awards']['name'] ) ){ $awards_cnt = count( $doctor_data['awards']['name'] ); ?>
                <li class="Write-Review-heading"><i class="Patient-Reviews-icon fa fa-list-alt"></i>Awards & Distinctions</li>
                <?php  for ( $idx = 0; $idx < $awards_cnt; $idx++ ) {  ?>
                    <div class="col-md-12">
                        <h4>- <?php echo isset($doctor_data['awards']['name'][ $idx ]) ? $doctor_data['awards']['name'][ $idx ] : '';  ?> (<?php echo isset($doctor_data['awards']['venue'][ $idx ]) ? $doctor_data['awards']['venue'][ $idx ]: '';  ?>) (<?php echo isset($doctor_data['awards']['year'][ $idx ]) ? $doctor_data['awards']['year'][ $idx ]: '';  ?>)</h4>

                    </div>
                <?php } ?>

            <?php } elseif ( isset( $doctor_data['awards'] ) && !empty( $doctor_data['awards'] ) ){ ?>
                <li class="Write-Review-heading"><i class="Patient-Reviews-icon fa fa-list-alt"></i>Awards & Distinctions</li>
                <div class="col-md-12">
                    <?php  foreach ( $doctor_data['awards'] as $key => $awards_name ) {  ?>
                        <h4>- <?php echo $awards_name;  ?></h4>
                    <?php } ?>
                </div>
            <?php } ?>

        </div>
        <!-- end experience tab -->

        <!-- start ratings tab -->
        <div id="ratings" class="tab-pane fade <?php if ( $tab == 'ratings' ) { echo 'active in'; } ?>">
            <?php if($error_msg != "" && $tab == "ratings"): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo $error_msg; ?>
                </div>
            <?php endif; ?>

            <?php if($success_msg != "" && $tab == "ratings"): ?>
                <div class="alert alert-success alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    <?php echo $success_msg; ?>
                </div>
            <?php endif; ?>

            <div class="col-sm-12 col-md-12 col-xs-12  Some-background">
                <div class="col-sm-3 col-md-3 col-xs-12 some-left">
                    <h1 class="Some-3"><?php echo $doctor_data['ratings']; ?> / <?php echo $doctor_data['ratingCount']; ?></h1>
                    <div class="text-center rating-big">
                        <input type="hidden" class="rating" value="<?php echo $doctor_data['ratings']; ?>" data-readonly data-fractions="2"/>
                    </div>
                    <button type="submit" class="btn btn-default btn-block  dark-pink some-but dp-center-md dp-center-sm dp-center-xs"><i class="fa fa-play"></i> Consult Now</button>
                </div>

                <div class="col-sm-9 col-md-9 col-xs-12 some-right row">
                    <?php
                    if ( !empty($individual_rating_fields) ) {
                        foreach( $individual_rating_fields as $individual_rating_field ) {
                            if ( $individual_rating_field['f2'] > 0 ) {
                                $avg_rat = $individual_rating_field['f1'] / $individual_rating_field['f2'];
                            } else {
                                $avg_rat = 0;
                            }
                            ?>
                            <div class="col-sm-12 col-md-6 col-xs-12">
                                <div class="SomeQuality-boder">
                                    <p><?php echo $individual_rating_field['f3']; ?></p>
                                    <input type="hidden" class="rating" value="<?php echo $avg_rat; ?>" data-readonly data-fractions="2"/>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        foreach( $individual_rating_fields2 as $individual_rating_field ) {
                            ?>
                            <div class="col-sm-12 col-md-6 col-xs-12">
                                <div class="SomeQuality-boder">
                                    <p><?php echo $individual_rating_field['fieldName']; ?></p>
                                    <input type="hidden" class="rating" value="0" data-readonly data-fractions="2"/>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>


            <li class="Patient-Reviews-heading"><i class="Patient-Reviews-icon fa fa-outdent"></i>Patient Reviews</li>
            <?php
            if ( !empty($user_reviews) ) {
                foreach ( $user_reviews as $user_review) {
                    ?>
                    <div class="col-sm-12 col-md-12 col-xs-12 Reviews-box">
                        <div class="col-sm-4 col-md-2 col-xs-4">
                            <img src="<?php echo get_gravatar($user_review['userEmail'], 400); ?>" alt="" class="img-rounded img-responsive">
                            <div class="experince"><?php echo sanitize($user_review['userFname']) . ' ' . sanitize($user_review['userLname']) ?> <br> <strong><?php echo date('M, d, Y', $user_review['added']) ?></strong></div>
                        </div>
                        <div class="col-sm-8 col-md-10 col-xs-8 row">
                            <div class="doctor-rating-name-section clearfix">
                                <h4 class="pull-left"><?php echo sanitize($user_review['title']) ?> </h4>
                                <div class="pull-right">
                                    <input type="hidden" class="rating" value="<?php echo floatval($user_review['rating']); ?>" data-readonly data-fractions="2"/>
                                </div>
                            </div>
                            <p><?php echo sanitize($user_review['description']); ?></p>
                        </div>
                    </div>
                    <?php
                }
            } else { ?>
                <div class="row">
                    <div class="col-md-12">
                        <p class="Write-Review-heading">No review given yet.</p>
                    </div>
                </div>
            <?php }

            //check user gave review or not
            if ( $user_gave_review == false ) {
            ?>

            <div class="col-sm-12 col-md-12 col-xs-12 row" style="margin-top: 24px;    margin-bottom: 24px;">

                <li class="Write-Review-heading"><i class="Patient-Reviews-icon fa fa-list-alt"></i>Write a Review</li>
                <div id="menu3" class="tab-pane fade active in">
                    <?php if ( isset($_SESSION["mvdoctorVisitornUserId"]) ) { ?>
                        <div id="user_rating_error"></div>
                    <form method="post" id="user_rating_form">
                        <input type="hidden" name="patient_review" value="1">
                        <?php
                        $sq = "SELECT * FROM `" . DB_PREFIX . "rating_fields` WHERE fieldStatus = 1";
                        $st = $db->prepare( $sq );
                        $st->execute();
                        if ( $st->rowCount() > 0 ) {
                            echo "<div class='row'>";
                            $rating_fields = $st->fetchAll();
                            foreach ( $rating_fields as $rating_field ) {
                                ?>
                                <div class="col-sm-6 col-md-6 col-xs-12 ">
                                    <div class="SomeQuality-boder">
                                        <p><?php echo sanitize( $rating_field['fieldName'] ); ?></p>
                                        <input type="hidden" name="rating[<?php echo $rating_field['id']  ?>]" class="user_rating" data-fractions="2"/>
                                    </div>
                                </div>
                                <?php
                            }
                            echo "</div>";
                        }
                        ?>
                        <input class="form-control Reviews-form-control" type="text" name="title" id="review_title" placeholder="Enter a review title">
                        <textarea class="form-control Reviews-form-textarea" rows="3" name="description" id="review_desc" placeholder="Enter a review decribtion"></textarea>
                        <button type="submit" class="btn btn-default dark-pink Reviews-form-butten"><i class="fa fa-check"></i> Post Review</button>
                    </form>
                    <?php } else { ?>
                        <div class="row">
                            <div class="col-md-12">
                               <p class="Write-Review-heading">Please Login To Give Review</p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <?php } ?>


        </div>
        <!-- end ratings tab -->

        <!-- start experience tab -->
        <?php /*?><div id="contact" class="tab-pane fade <?php if ( $tab == 'contact' ) { echo 'active in'; } ?>">

            <input class="form-control Reviews-form-control" type="text" placeholder="Enter Subject">
            <textarea class="form-control Reviews-form-textarea" rows="3"placeholder="Type in your message"></textarea>
            <button type="submit" class="btn btn-default dark-pink Reviews-form-butten"><i class="fa fa-check"></i> Send Message</button>

        </div><?php */?>
        <!-- end contact tab -->

    </div>
</div>

<script type="text/javascript">

jQuery(function ($) {

    Stripe.setPublishableKey('<?php echo $doctor_stripe['stripePubKey']; ?>');

    $('#doctorTimings').datetimepicker({
        inline: true,
        sideBySide: true,
        daysOfWeekDisabled: [<?php echo implode(',', $disabled_days_for_doctor); ?>],
        useCurrent: false,
        minDate: '<?php echo date('m/d/Y', time()); ?>',
        format: 'YYYY-MM-DD'
    });


    var patient_concern = '';
    var plan = null;
    var plans = {};
    var date_time = null;
    var next_step_ok = false;
    var step_1_ok = false;
    var step_2_ok = false;
    var step_3_ok = false;
    var step_4_ok = false;
    var step_5_ok = false;

    //click step one next
    //check required field entered or not
    $('.step1_next').click(function(){

        patient_concern = $('#patient_condition_block :input').serializeArray();

        next_step_ok = true;
        step_1_ok = true;
        $('#step1_error').html('').hide();

    });


    //get date change event
    $("#doctorTimings").on("dp.change", function (e) {
        date_time = e.date.toObject();
        next_step_ok = false;
        step_2_ok = false;
        step_4_ok = false;
        step_5_ok = false;
        plan = null;
        //console.log( date_time );

        /*
         {
             years: 2015
             months: 6
             date: 26,
             hours: 1,
             minutes: 53,
             seconds: 14,
             milliseconds: 600
         }
        */

        //now grab all plans for this date

        //1. disable next and previous button
        $('.step_2_next, .step_1_back').prop('disabled', true);

        //2. show loading state
        var str = '<div class="alert alert-info" role="alert"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i> \
                <span class="sr-only">Loading...</span> Checking database for available timing. Please wait...</div>';
        $('#doctorPlans').html(str);

        //3. set required parameter
        var data = {
            'years': date_time.years,
            'months': date_time.months,
            'date': date_time.date,
            'token': '<?php echo getToken(); ?>',
            'doctorId': '<?php echo $id; ?>'
        };

        //ajax call
        $.ajax({
            url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=check_available_timing",
            data: data,
            method: "POST",
            dataType: "json"
        }).error(function (err) {
            //enable buttons

            var str = '<div class="alert alert-danger" role="alert"><strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error</strong> Couldn\'t completed ajax request. Please contact site administrator.</div>';
            $('#doctorPlans').html(str + '<p><strong>Response Text: </strong>' + err.responseText).show();

            $('.step_2_next, .step_1_back').prop('disabled', false);


        }).done(function () {
            $('.step_2_next, .step_1_back').prop('disabled', false);

        }).success(function (data) {
            if ( data.success ) {

                if ( data.html ) {
                    $('#doctorPlans').html(data.html);
                }

                if( data.message ) {
                    var str = '';
                    $.each(data.message, function(index, msg){
                        str += '<div class="alert alert-success alert-dismissible" role="alert">\
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                             ' + msg + ' \
                            </div>';
                    });
                    $('#doctorPlans').html( str).show();
                }

                if ( data.plans ) {
                    plans = data.plans;
                } else {
                    plans = {};
                }

            } else if ( data.error ) {
                var str = '';
                $.each(data.error, function(index, msg) {
                    str += '<div class="alert alert-danger alert-dismissible" role="alert">\
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                            <strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error!</strong> ' + msg + ' \
                            </div>';
                });
                $('#doctorPlans').html( str).show();
            }


        }, 'json');

    });



    //select plan
    $('#consultNowModal').on('click', '.selectTiming', function () {
        plan = $(this).data('plan_id');
        step_4_ok = false;
        step_2_ok = false;

        //check plan information and populate payment value
        if ( plan != null ) {

            //console.log(plans[plan]);
            //1. select plans
            $('#consultation_plan_table tr').removeClass('danger');
            if ( plans[plan].duration == '1' ) {
                $('#consultation_plan_table tr.plan_15').addClass('danger');
            } else if ( plans[plan].duration == '2' ) {
                $('#consultation_plan_table tr.plan_30').addClass('danger');
            }

            //2. update step 5 fields
            $('#consultationDurationText').html(  plans[plan].consultationDuration  );
            $('#consultationDateText').html(plans[plan].consultationDate);
            $('#consultationFeeText').html('$' + plans[plan].consultationFee);
            $('#consultationTimeText').html(plans[plan].consultationTime);

            $('#step2_error').html('').hide();
        }
    });

    //
    $('#consultNowModal').on('click', '.timingNotAvailable', function () {
        var str = '<div class="alert alert-danger" role="alert"><strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error</strong> Time slot is not available.</div>';
        $('#step2_error').html(str).show();
        next_step_ok = false;
    });


    //$('.step_2_next').click(function(e) {
    $('.step_2_next').click( function () {
        console.log('here on step_2_next');
        if ( plan == null ) {
            var str = '<div class="alert alert-danger" role="alert"><strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error</strong> Please select a appointment date first to proceed next step.</div>';
            $('#step2_error').html(str).show();
            next_step_ok = false;
        } else {
            next_step_ok = true;
            step_2_ok = true;
            $('#step2_error').html('').hide();
        }
    });


    $('.step_3_next').click( function() {
        //do nothing, step 3 is just for display selected plan with price
    });

    //acknowledge check box
    $('.step_4_next').click( function () {
        //check that checkbox is selected or not
        if( $('#acknowledge_check').prop("checked") == false ) {
            var str = '<div class="alert alert-danger" role="alert"><strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error</strong> Please acknowledge and agree to the Terms and Conditions and Consent to Treatment stated below.</div>';
            $('#step4_error').html(str).show();
            next_step_ok = false;
            step_4_ok = false;

        } else {
            next_step_ok = true;
            step_4_ok = true;
            $('#step4_error').html('').hide();
        }
    });


    $('.step_5_next').click(function() {
        //check payment is already completed or not
        if ( step_5_ok === true ) {
            next_step_ok = true;
            //console.log(step_4_ok);
            //console.log(next_step_ok);
            return;
        }
        else if ( step_2_ok === true && step_4_ok === true ) {
            //now if step_3 is completed, we can process step_4
            next_step_ok = false;
            step_5_ok = false;
            //disable buttons
            $('.step_4_back, .step_5_next').prop('disabled', true);

            //validate for required fields, I guess this is not required

            //show modal with wait message
            var str = '<div class="alert alert-info" role="alert"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i> \
                <span class="sr-only">Loading...</span> Please wait...</div>';
            $('#step5_error').html(str).show();

            ////now get stripe token
            var $form = $('#payment-form');
            Stripe.card.createToken($form, stripeResponseHandler);

        } else {
            step_5_ok = false;
            console.log('Step 3 or Step 4 is not completed.');
            //show error message
            //PaymentDisplay
            //PaymentDisplayMsg
        }
    });


    function stripeResponseHandler(status, response) {
        // Grab the form:
        var $form = $('#payment-form');

        if (response.error) { // Problem!

            // Show the errors on the form:
            $('#step5_error').html('<div class="alert alert-danger" role="alert">' + response.error.message + '</div>').show();
            //enable buttons
            $('.step_4_back, .step_5_next').prop('disabled', false); // Re-enable submission

        } else { // Token was created!

            // Get the token ID:
            var token = response.id;

            //call ajax
            var data = {
                'task': 'pay_now',
                'years': date_time.years,
                'months': date_time.months,
                'date': date_time.date,
                'plan_id': plan,
                'token': '<?php echo getToken(); ?>',
                'doctorId': '<?php echo $id; ?>',
                'patient_concern': patient_concern,
                'stripeToken': token,
                'name_on_card': $('#name_on_card').val(),
                'address_1': $('#address_1').val(),
                'address_2': $('#address_2').val(),
                'userCity': $('#userCity').val(),
                'userCountry': $('#userCountry').val(),
                'userState': $('#userState').val(),
                'userZip': $('#userZip').val(),
                'userPhone': $('#userPhone').val()
            };

            //show modal with processing message
            var str = '<div class="alert alert-info" role="alert"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i> \
                <span class="sr-only">Loading...</span> Please wait, while we are processing your request...</div>';
            $('#step5_error').html(str).show();


            //ajax call
            $.ajax({
                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=check_appointment",
                data: data,
                method: "POST",
                dataType: "json"
            }).error(function (err) {
                //enable buttons

                var str = '<div class="alert alert-danger" role="alert"><strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error</strong> Couldn\'t completed ajax request. Please contact site administrator.</div>';
                $('#step5_error').html(str + '<p><strong>Response Text: </strong>' + err.responseText).show();

                $('.step_4_back, .step_5_next').prop('disabled', false);
                console.log('Error: ', err);

            }).done(function () {
                $('.step_4_back, .step_5_next').prop('disabled', false);

            }).success(function (data) {
                if ( data.success ) {
                    //clear all text fields, and data

                    if( data.message ) {
                        var str = '';
                        $.each(data.message, function(index, msg){
                            str += '<div class="alert alert-success alert-dismissible" role="alert">\
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                             ' + msg + ' \
                            </div>';
                        });
                        $('#step5_error').html( str).show();
                    }

                    step_5_ok = true;
                    next_step_ok = true;
                    $('#step5_error').html('').hide();
                    $('.step_5_next').trigger('click');
                    //reset form
                    resetForm($('#payment-form'));

                    //disable step button
                    $('a').find('.btn-circle').prop('disabled', true).addClass('disable_step_button');

                    //reset variable to default
                    next_step_ok = false;
                    step_2_ok = false;
                    step_4_ok = false;
                    step_5_ok = false;
                    plan = null;
                    plans = {};

                } else if ( data.error ) {
                    var str = '';
                    $.each(data.error, function(index, msg) {
                        str += '<div class="alert alert-danger alert-dismissible" role="alert">\
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                            <strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error!</strong> ' + msg + ' \
                            </div>';
                    });
                    $('#step5_error').html( str).show();
                }
            }, 'json');
        }
    }

    $('.disable_step_button').click( function(e) {
        e.preventDefault();
        next_step_ok = false;
        return false;
    });

    function resetForm($form) {
        $form.find('input:text, input:password, input:file, select, textarea').val('');
        $form.find('input:radio, input:checkbox')
            .removeAttr('checked').removeAttr('selected');
    }


    // nice form step wizard start here
    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function() {
        if ( next_step_ok === false ) {
            //console.log('here at allNextBtn', next_step_ok);
            return false;
        }

        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='email'],select[id='industry']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

	allPrevBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        $(".form-group").removeClass("has-error");
        prevStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    // nice form step wizard ends here

    //rating code starts here
    $('input.user_rating').rating();

    $('#user_rating_form').submit(function(){
        var str = "";
        var flag = false;
        var review_title = $('#review_title').val();
        var review_desc = $('#review_desc').val();

        if ( review_title == "" ) {
            str += '<div class="alert alert-danger" role="alert">Please enter a review title.</div>';
            flag = true;
        }

        if ( review_desc == "" ) {
            str += '<div class="alert alert-danger" role="alert">Please enter a review description.</div>';
            flag = true;
        }


        //check rating fields
        var rating_field = $('.user_rating');
        $.each(rating_field, function(idx, field) {
            if ( field.value == "" ) {
                str += '<div class="alert alert-danger" role="alert">Please enter rating for all fields.</div>';
                flag = true;
                return false;
            }
        });

        if ( flag === false ) {
            return true;
        } else {
            $('#user_rating_error').html(str);
        }
        return false;
    });
    //rating code ends here
});

</script>