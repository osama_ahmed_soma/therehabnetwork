<?php

//deny direct access

if ( !defined('MVD_SITE') ) {

    die('You are not authorized to view this page');

}?>

<section class="clearfix doctor-inner-page">

    <div class="container">

        <div class="row">

            <div class="col-md-12 col-sm-12">

                <div class="doctor-list clearfix">

                    <div class="col-sm-4 col-md-2 col-xs-12">

                        <img src="<?php echo HTTP_SERVER . 'images/doctors/full/' . $doctor_data['featured_image']; ?>"

                             alt="" class="img-rounded img-responsive" />

                        <div class="experince">EXPERIENCE <br> <strong><?php echo $doctor_data['experience']; ?> YEARS</strong></div>

                    </div>

                    <div class="col-sm-8 col-md-5 col-xs-12 row">

                        <div class="doctor-rating-name-section">

                            <h4><?php echo $doctor_data['doctor_name']; ?> <span class="online"><i class="fa fa-dot-circle-o"></i> Online Now</span></h4>

                            <div>

                                <input type="hidden" class="rating" value="<?php echo $doctor_data['ratings']; ?>" data-readonly data-fractions="2"/>

                                <br>

                                <small><?php echo $doctor_data['ratings']; ?> Star based on <?php echo $doctor_data['ratingCount']; ?> Reviews</small>

                            </div>

                        </div>

                        <ul class="medial-info">

                            <li><i class=" fa fa-stethoscope"></i> <?php echo $doctor_data['specialization']; ?></li>

                            <li><i class=" fa fa-graduation-cap"></i><?php echo $doctor_data['degree']; ?></li>

                            <li> <i class=" fa fa-dollar"></i><?php echo $doctor_data['fees']; ?></li>

                            <li> <i class=" fa fa-hospital-o"></i><?php echo $doctor_data['location']; ?></li>

                        </ul>

                    </div>

                    <div class="col-sm-12 col-md-5 col-xs-12 pull-right">

                        <!--

                        <div class="col-md-10 col-sm-12 col-xs-12 text-center doctor-mini-rating-col pull-right">

                            <?php if ($doctor_data['categoryImage']) { ?>

                            <div class="name">This Doctor Is Powered By:</div>

                            <div class="company"><img src="<?php echo HTTP_SERVER . 'images/category/full/' . $doctor_data['categoryImage']; ?>" /></div>

                            <?php } ?>

                        </div>

                        -->

                    </div>

                    <div class="doctor-mini-btn-col doc-profile-detail">

                        <button type="submit" class="btn btn-default dark-pink <?php if ($is_doctor_favorite == 2) { echo "removeFavorite"; } else { echo "addToFavorite"; } ?>" data-login="<?php if ($is_doctor_favorite=='0') { echo 'no'; } else { echo 'yes'; } ?>" data-id="<?php echo $id; ?>" ><?php if ($is_doctor_favorite == 2) { echo '<i class="fa fa-heartbeat"></i> Remove From Favorite'; } else { echo '<i class="fa fa-heart"></i> Favorite'; } ?></button>

                        <button type="submit" class="btn btn-default dark-pink" data-toggle="modal" data-target="#consultNowModal"><i class="fa fa-play"></i>

                            Schedule Appointment

                        </button>

                        <!--<button type="submit" class="btn btn-default dark-pink"><i class="fa fa-calendar"></i> Book Appointment</button>-->

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>



<!-- Modal -->

<div class="modal fade" id="consultNowModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content">

            <div class="modal-body">

                <?php require_once $siteRoot . 'pages/modules/doctor/consult_now.php'; ?>

            </div>

        </div>

    </div>

</div>



<!-- Modal 2-->

<div class="modal fade" id="myModalFav" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-body" style="text-align: center;" id="favoriteModal"></div>

        </div>

    </div>

</div>



<script type="text/javascript">

    jQuery(function($){



        $('body').on('click', '.addToFavorite', function(){

            var th = $(this);

            var login = $(this).data('login');

            var doctor_id = $(this).data('id');



            if ( login == 'yes' && doctor_id > 0 ) {

                //call ajax

                th.attr('readonly', 'readonly');

                $.ajax({

                    url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=doctor_favorite",

                    data: {doctor_id: doctor_id, token: '<?php echo getToken(); ?>'},

                    method: "POST",

                    dataType: "json"

                }).error(function (err) {

                    th.removeAttr('readonly');

                    console.log(err);



                }).done(function () {

                    //



                }).success(function (data) {



                    if (data.success) {

                        th.removeClass('addToFavorite').addClass('removeFavorite');

                        th.html('<i class="fa fa-heartbeat"></i> Remove From Favorite');

                        var fav_link = '<?php echo HTTP_SERVER ?>patient/providers.html';

                        var str = '<div class="alert alert-success" role="alert">This Doctor has now been added to your favorites!<br>\You can view all your favorite Doctors <a href="'+fav_link+'" title="Favorite Doctors">here</div>';

                        $('#favoriteModal').html(str);

                        $('#myModalFav').modal();

                    }

                    if ( data.error ) {

                        $('#favoriteModal').html('<div class="alert alert-danger" role="alert">'+data.error+'</div>');

                        th.removeAttr('readonly');

                        $('#myModalFav').modal();

                    }



                }, 'json');

            } else {

                $('#favoriteModal').html('<div class="alert alert-danger" role="alert">Please login to add to favorite.</div>');

                $('#myModalFav').modal();

            }

        });



        $('body').on('click', '.removeFavorite', function(){

            var th = $(this);

            var login = $(this).data('login');

            var doctor_id = $(this).data('id');



            if ( login == 'yes' && doctor_id > 0 ) {

                //call ajax

                th.attr('readonly', 'readonly');

                $.ajax({

                    url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=remove_doctor_favorite",

                    data: {doctor_id: doctor_id, token: '<?php echo getToken(); ?>'},

                    method: "POST",

                    dataType: "json"

                }).error(function (err) {

                    th.removeAttr('readonly');

                    console.log(err);



                }).done(function () {

                    //



                }).success(function (data) {



                    if (data.success) {

                        th.removeClass('removeFavorite').addClass('addToFavorite');

                        th.html('<i class="fa fa-heart"></i> Favorite');

                        $('#favoriteModal').html('<div class="alert alert-success" role="alert">Doctor removed from favorite.</div>');

                        $('#myModalFav').modal();

                    }

                    if ( data.error ) {

                        $('#favoriteModal').html('<div class="alert alert-danger" role="alert">'+data.error+'</div>');

                        th.removeAttr('readonly');

                        $('#myModalFav').modal();

                    }



                }, 'json');

            } else {

                $('#favoriteModal').html('<div class="alert alert-danger" role="alert">Please login to add to favorite.</div>');

                $('#myModalFav').modal();

            }

        });









    });

</script>