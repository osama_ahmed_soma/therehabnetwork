<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

//get all plans for this doctor
try {
    $query = "SELECT `id`, `time`, `price` FROM `" . DB_PREFIX . "plans` WHERE `doctorId` = ? LIMIT 2";
    $st = $db->prepare($query);
    $st->execute(array($id));

    if ( $st->rowCount() ) {
        $plans = $st->fetchAll();
    } else {
        $plans = array();
    }

} catch (Exception $Exception) {
    die("DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}

//get doctor stripe information
try {
    $query = "SELECT stripeSecretKey, stripePubKey FROM `" . DB_PREFIX . "doctors` WHERE id = ?";
    $st = $db->prepare($query);
    $st->execute(array($id));

    if ( $st->rowCount() ) {
        $doctor_stripe = $st->fetch();
    } else {
        $doctor_stripe = array();
    }

} catch (Exception $Exception) {
    die("DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}

//get all timing days for doctor
try {
    $query = "SELECT DISTINCT `dayId` FROM `" . DB_PREFIX . "doctor_timing` WHERE doctorId = ? ORDER BY `dayId` ASC";
    $st = $db->prepare($query);
    $st->execute(array($id));

    if ( $st->rowCount() ) {
        foreach ( $st->fetchAll() as $row ) {
            $doctor_available_days[] = $row['dayId'];
        }

        //get disabled days for this doctor
        $disabled_days_for_doctor = array();
        foreach ( array(0,1,2,3,4,5,6) as $days ) {
            if ( !in_array($days, $doctor_available_days) ) {
                $disabled_days_for_doctor[] = $days;
            }
        }

    } else {
        $doctor_available_days = array();
        $disabled_days_for_doctor = array();
    }

} catch (Exception $Exception) {
    die("DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}
?>

    <!-- check user logged in or not -->
    <?php if ( !isset($_SESSION['mvdoctorVisitornUserId'])) {
        //<button type="button" class="btn btn-info sing" data-toggle="modal" data-target="#myModal">Sign Up / Log In</button>
        echo '<div class="alert alert-danger" role="alert">Please <a href="#" data-toggle="modal" data-target="#myModal">Login</a> as a Patient to view this resource.</div>';
    } else {?>
    <!-- Step Wizard -->
    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
                <a href="#step-1" type="button" class="btn btn-primary btn-circle step_1_back">1</a>
                <p>Concern</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-2" type="button" class="btn btn-default btn-circle step_2_back" disabled="disabled">2</a>
                <p>Date & Time</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-default btn-circle step_3_back" disabled="disabled">3</a>
                <p>Consultation</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-4" type="button" class="btn btn-default btn-circle step_4_back" disabled="disabled">4</a>
                <p>Consent To Treat</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-5" type="button" class="btn btn-default btn-circle step_5_back" disabled="disabled">5</a>
                <p>Payment</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-6" type="button" class="btn btn-default btn-circle step_6_back" disabled="disabled">6</a>
                <p>Finish</p>
            </div>
        </div>
    </div>
    <!-- Step Wizard END -->
    <!-- quote form -->
    <form class="form-horizontal" role="form" method="post" autocomplete="on" spellcheck="true" id="payment-form">

        <fieldset><!-- form contents -->

            <!-- Wizard STEP 1 -->
            <div class="setup-content" id="step-1"> <br  />
                <?php
                //check required fields exist or not, 1. doctor plans, 2. doctor schedules, 3. doctor stripe key
                ?>
                <?php if ( $_SESSION['mvdoctorVisitorSecureVideoId'] == 0 ) { ?>
                    <div class="alert alert-danger" role="alert">You need to complete your profile settings first before a consultation. <a href="<?php echo HTTP_SERVER . 'patient.html'; ?>">Click here</a> to update your profile.</div>
                <?php  } elseif ( $doctor_stripe['stripeSecretKey'] == '' || $doctor_stripe['stripePubKey'] == "" ) { ?>
                    <div class="alert alert-danger" role="alert">Doctor's payment method is not configured yet. Please contact doctor via contact form.</div>
                <?php  } elseif ( empty($plans) ) { ?>
                    <div class="alert alert-danger" role="alert">Doctor didn't added any consultation plans yet, please contact with doctor via contact form.</div>
                <?php } elseif ( empty($doctor_available_days) ) {?>
                    <div class="alert alert-danger" role="alert">Doctor didn't added any schedule time yet, please contact with doctor via contact form.</div>
                <?php } else { ?>
                <div class="col-md-12"><h4 class="consult-step-heading">Medical Conditions</h4><br /></div>

                <div id="step1_error" class="col-md-12"></div>

                <div class="col-md-12  custyle">
                    <div class="col-md-11 custab" style="margin-left:2em;" id="patient_condition_block">
                        <div class="form-group">
                            <label for="exampleInputEmail1">What is the purpose of your visit ?</label>
                            <textarea class="form-control" rows="5" name="data[main_purpose]" id="main_purpose" placeholder="Please describe briefly"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">When did you start feeling this way?</label>
                            <input type="text" name="data[feeling_this_way]" class="form-control" id="feeling_this_way" placeholder="" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Please tell us if you are currently taking any medications and, if so, for how long?</label>
                            <input type="text" style="width: 49%; margin-right: 1%; margin-bottom: 5px;" name="data[taking_any_medications][name1]" class="form-control col-md-6" id="taking_any_medications1" placeholder="Medication Name" /> <input type="text" style="width: 50%; margin-bottom: 5px;" name="data[taking_any_medications][duration1]" class="form-control col-md-6" id="medications_duration_1" placeholder="How Long?" />
                            <input type="text" style="width: 49%; margin-right: 1%;" name="data[taking_any_medications][name2]" class="form-control col-md-6" id="taking_any_medications1" placeholder="Medication Name" /> <input type="text" style="width: 50%; " name="data[taking_any_medications][duration2]" class="form-control col-md-6" id="medications_duration_2" placeholder="How Long?" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Please tess us any allergies you have:</label>
                            <input type="text" name="data[allergies_1]" class="form-control" id="allergies_1" placeholder="Allergy description" />
                            <input type="text" style="margin-top: 5px;" name="data[allergies_2]" class="form-control" id="allergies_2" placeholder="Allergy description" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Do you have any of these symptoms?</label>
                            <p class="help-block">Check all that apply.</p>
                            <div class="row" style="margin-bottom: 1em;">
                                <div class="col-md-6">

                                    <h5>General Symptoms</h5>
                                    <div class="">
                                        <label><input type="checkbox" name="data[allergy_general][]" value="Fever"> Fever</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[allergy_general][]" value="Weight loss / gain"> Weight loss / gain</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[allergy_general][]" value="Difficulty sleeping"> Difficulty sleeping</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[allergy_general][]" value="Loss of appetite"> Loss of appetite</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[allergy_general][]" value="Mood changes"> Mood changes</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[allergy_general][]" value="Fatigue / weakness"> Fatigue / weakness</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[allergy_general][]" value="Foreign travel (past month)"> Foreign travel (past month)</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[allergy_general][]" value="Hospitalized (past six months)"> Hospitalized (past six months)</label>
                                    </div>


                                    <h5>Chest</h5>
                                    <div class="">
                                        <label><input type="checkbox" name="data[chest][]" value="Chest pressure / pain"> Chest pressure / pain</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[chest][]" value="Palpitations"> Palpitations</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[chest][]" value="Cough"> Cough</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[chest][]" value="Sputum"> Sputum</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[chest][]" value="Shortness of breath"> Shortness of breath</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[chest][]" value="Decreased exercise tolerance"> Decreased exercise tolerance</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[chest][]" value="History of smoking"> History of smoking</label>
                                    </div>


                                    <h5>Skin</h5>
                                    <div class="">
                                        <label><input type="checkbox" name="data[skin][]" value="Bleeding"> Bleeding</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[skin][]" value="Itching"> Itching</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[skin][]" value="Swelling"> Swelling</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[skin][]" value="Skin rashes / bumps"> Skin rashes / bumps</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[skin][]" value="Bruising / discoloration"> Bruising / discoloration</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[skin][]" value="Sores"> Sores</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[skin][]" value="Bites"> Bites</label>
                                    </div>


                                    <h5>Muscles and Joints</h5>
                                    <div class="">
                                        <label><input type="checkbox" name="data[muscles_and_joints][]" value="Muscle pain"> Muscle pain</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[muscles_and_joints][]" value="Limited motion / mobility"> Limited motion / mobility</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[muscles_and_joints][]" value="Muscle weakness"> Muscle weakness</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[muscles_and_joints][]" value="Back pain"> Back pain</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[muscles_and_joints][]" value="Swelling"> Swelling</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[muscles_and_joints][]" value="Joint replacements"> Joint replacements</label>
                                    </div>

                                </div>



                                <div class="col-md-6">

                                    <h5>Head / Neck Symptoms</h5>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="Headache"> Headache</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="Dizzy/Lightheaded"> Dizzy/Lightheaded</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="Vision Changes"> Vision Changes</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="Hearing Loss/Ringing"> Hearing Loss/Ringing</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="Ear Drainage"> Ear Drainage</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="Nasal Discharge"> Nasal Discharge</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="Congestion/Sinus Problem"> Congestion/Sinus Problem</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="Sore Throat"> Sore Throat</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="Allergies"> Allergies</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="Numbness/Tingling"> Numbness/Tingling</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="History of fainting/seizure"> History of fainting/seizure</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="Memory Loss"> Memory Loss</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="History of Stroke"> History of Stroke</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[head_neck][]" value="History of falls"> History of falls</label>
                                    </div>


                                    <h5>Pelvis</h5>
                                    <div class="">
                                        <label><input type="checkbox" name="data[pelvis][]" value="Flank pain"> Flank pain</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[pelvis][]" value="Discomfort / burning with urination"> Discomfort / burning with urination</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[pelvis][]" value="Blood in urine"> Blood in urine</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[pelvis][]" value="Frequent urination"> Frequent urination</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[pelvis][]" value="History of Sexually Transmitted Infections"> History of Sexually Transmitted Infections</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[pelvis][]" value="Irregular periods"> Irregular periods (Female only)</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[pelvis][]" value="Vaginal bleeding"> Vaginal bleeding (Female only)</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[pelvis][]" value="Vaginal discharge"> Vaginal discharge (Female only)</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[pelvis][]" value="Penile Discharge"> Penile Discharge (Male only)</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[pelvis][]" value="Testicular swelling"> Testicular swelling (Male only)</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[pelvis][]" value="Testicular pain"> testicular pain (Male only)</label>
                                    </div>

                                    <h5>Digestive Track</h5>
                                    <div class="">
                                        <label><input type="checkbox" name="data[digestive_track][]" value="Sore throat"> Sore throat</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[digestive_track][]" value="Nausea / vomiting"> Nausea / vomiting</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[digestive_track][]" value="Difficulty / pain swallowing"> Difficulty / pain swallowing</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[digestive_track][]" value="Heartburn / reflux"> Heartburn / reflux</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[digestive_track][]" value="Diarrhea"> Diarrhea</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[digestive_track][]" value="Constipation"> Constipation</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[digestive_track][]" value="Abdominal pain / discomfort"> Abdominal pain / discomfort</label>
                                    </div>

                                </div>


                            </div>

                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Please let us know if you have any medical conditions:</label>
                            <p class="help-block">Check all that apply.</p>

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="">
                                        <label><input type="checkbox" name="data[medical_conditions][]" value="Cancer"> Cancer</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[medical_conditions][]" value="Diabetes"> Diabetes</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[medical_conditions][]" value="Heart Disease (CHF, MI)"> Heart Disease (CHF, MI)</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[medical_conditions][]" value="Stroke"> Stroke</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[medical_conditions][]" value="High blood pressure"> High blood pressure</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[medical_conditions][]" value="High cholesterol"> High cholesterol</label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="">
                                        <label><input type="checkbox" name="data[medical_conditions][]" value="Asthma / COPD"> Asthma / COPD</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[medical_conditions][]" value="Depression"> Depression</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[medical_conditions][]" value="Arthritis"> Arthritis</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[medical_conditions][]" value="Abnormal Thyroid"> Abnormal Thyroid</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[medical_conditions][]" value="Pregnant"> Pregnant</label>
                                    </div>
                                    <div class="">
                                        <label><input type="checkbox" name="data[medical_conditions][]" value="Other"> Other</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <table class="table table-striped custab">
                        <tr>
                            <td><button class="btn btn-default nextBtn dark-pink pull-right step1_next" type="button" >Proceed To Step 2 <i class="fa fa-angle-double-right"></i> </button></td>
                        </tr>
                    </table>

                </div>
                <?php } ?>

            </div>
            <!-- Wizard STEP 1 END -->

            <!-- Wizard STEP 2 -->
            <div class="setup-content" id="step-2">

                <div class="col-md-12"><h4 class="consult-step-heading"><span><?php echo date('Y-m-d'); ?> </span>Date & Time <span id="serverClock1"><?php echo date('h:i:s A'); ?></span></h4><br  /></div>

                <div id="step2_error" class="col-md-12"></div>

                <div class="col-md-12 col-sm-12">

                    <div class="" style="margin-left: 0px !important;">
                        <div class="row">
                            <div class="col-md-6">
                                <div id="doctorTimings"></div>
                            </div>
                            <div class="col-md-6">
                                <div id="doctorPlans"></div>
                            </div>
                        </div>
                    </div>

                </div>

                    <div class="col-md-12"><table class="table table-striped custab">
                            <tr>
                                <td>
                                    <button class="btn btn-default prevBtn btn-info pull-left step_1_back" type="button" >
                                        <i class="fa fa-angle-double-left"></i> Back To Step 2
                                    </button>
                                    <button class="btn btn-default nextBtn dark-pink pull-right step_2_next" type="button" >
                                        Proceed To Step 4 <i class="fa fa-angle-double-right"></i>
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
            </div>
            <!-- Wizard STEP 2 END -->

            <!-- Wizard STEP 3 Start -->
            <div class="setup-content" id="step-3"> <br  />
                <div class="col-md-12"><h4 class="consult-step-heading">Consultation Plan</h4><br  /></div>

                <div id="step3_error" class="col-md-12"></div>

                <div class="col-md-12  custyle">
                    <table class="table table-striped custab" id="consultation_plan_table">
                        <thead>
                        <tr>
                            <th>Consultation Duration</th>
                            <th>Price</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php if ( !empty($plans) ):
                        foreach ( $plans as $plan ): ?>
                            <tr class="<?php if ($plan['time'] == '1') { echo 'plan_15'; } elseif ($plan['time'] == '1') { echo 'plan_30'; } ?>">
                                <td><?php echo number2time($plan['time']) ?></td>
                                <td>$<?php echo $plan['price'] ?></td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>

                    <table class="table table-striped custab">
                        <tr>
                            <td>
                                <button class="btn btn-default prevBtn btn-info pull-left" type="button" >
                                    <i class="fa fa-angle-double-left"></i> Back To Step 2
                                </button>
                                <button class="btn btn-default nextBtn dark-pink pull-right step_3_next" type="button" >
                                    Proceed To Step 4 <i class="fa fa-angle-double-right"></i>
                                </button>
                            </td>
                        </tr>
                    </table>

                    <?php else: ?>
                            <tr class="danger">
                                <td colspan="4">No plan found. Please contact with doctor via contact form.</td>
                            </tr>
                        </tbody>
                    </table>
                        <?php endif; ?>
                </div>
            </div>
            <!-- Wizard STEP 3 END -->
            
            
            <!-- Wizard STEP 4 -->
            <div class="setup-content" id="step-4">
                <div class="col-sm-12"><br  />
                    <h4 class="consult-step-heading" style="border-bottom:0px">Consent To Treat</h4><br  />

                    <div id="step4_error" class="col-md-12"></div>

                    <table class="table table-striped custab">
                        <tr>
                            <td class="pull-left" width="50%"> Patient Name: <strong > <?php echo $_SESSION["mvdoctorVisitorUserFname"] . ' ' . $_SESSION["mvdoctorVisitorUserLname"]; ?></strong></td>
                            <td class="pull-right text-right" width="50%"> Guardian Name: <strong >Self</strong></td>
                        </tr>
                        <tr>
                            <td class="pull-left" width="100%">I, <?php echo $_SESSION["mvdoctorVisitorUserFname"] . ' ' . $_SESSION["mvdoctorVisitorUserLname"]; ?> Participant or guardian, give/s permission to My Virtual Doctor, LLC to perform the medical services that the physician and other non-physician providers and assistants may deem to be necessary.  In addition, I, or guardian, authorize/s My Virtual Doctor, LLC to release any informatino obtained during the course of my examination and/or treatment to my healthcare insurer or other payer.</td	>
                        </tr>
                        <tr>
                            <td class="pull-left terms-conditions-sec" >
                            	<p><strong>Terms and Conditions</strong></p>
<p>These Terms and Conditions define the obligations of My Virtual Doctor, LLC, its authorized agents and me, the service subscriber, and they establish the basic rules of safe and fair use of My Virtual Doctor, LLC services.  My Virtual Doctor, LLC and its authorized agents reserve the right to immediately and without advance notice terminate the service and deny access to individuals who do not abide by the Terms and Conditions</p>
<p>By using the My Virtual Doctor, LLC public Website, the My Virtual Doctor, LLC secure Website and the My Virtual Doctor, LLC telemedicine service, I signify my acceptance of the My Virtual Doctor, LLC services Terms and Conditions and software provider End User Agreement.  If I do not accept the My Virtual Doctor, LLC services Terms and Conditions and software provider End User Agreement I should not use this service.  If My Virtual Doctor, LLC or software provider changes the Terms and Conditions or End User Agreement, they will post those changes prominently.  My continued use of the services and Websites following the posting of changes to these terms will mean I accept those changes.  Changes to the Terms and Conditions and End User Agreement will become effective immediately upon posting on teh My Virtual Doctor, LLC Websites and shall supersede all prior versions of the Terms and Conditions and End User Agreement unless otherwise noted.</p>
<p><strong>Privacy and Security</strong></p>
<p>My Virtual Doctor, LLC considers the privacy of my health information to be one of the most important elements in our relationship with me.  My Virtual Doctor, LLC's responsibility to maintain the confidentiality of my health information is one that they take very seriously.  I accept My Virtual Doctor, LLC Privacy and Security Policy.</p>
<p>I understand that it is extremely important that I keep my password to access My Virtual Doctor, LLC completely confidential.  If at any time I feel that the confidentiality of my password has been compromised, I will change it by going to the Password link on the My Virtual Doctor, LLC website.  I understand that My Virtual Doctor, LLC or their authorized vendors and agents take no responsibility for and disclaim any and all liability or consequential damages arising from a breach of health record confidentiality resulting from my sharing or losing my password.  If My Virtual Doctor, LLC or their authorized vendors and agents discovers that I have inappropriately shared my password with another person, or that I have misused or abused my online access privileges in any way, my participation may be discontinued without prior notice.</p>
<p><strong>Use of My Virtual Doctor, LLC for Health Care Services</strong></p>
<p>My registration authorizes me to use My Virtual Doctor, LLC services as provided in this agreement.  It is my duty to be truthful and accurate with all the information I enter into or upload to the system.  I acknowledge that I understand that any misrepresentations about the patient's condition may result in serious harm to me or others.  The law requires that every medical diagnostic or treatment encounter be documented.  The documentation of consultation encounters with My Virtual Doctor, LLC is maintained in an electronic health record (EHR).  I may also use my EHR to store other important medical information pertaining to my current health, medical condition and my health history.  While my account with My Virtual Doctor, LLC is active and in good standing, I will have unlimited access to my medical information stored in my EHR.</p>
<p>By accepting this Agreement, I am granted a non-transferable license subject to the terms of this Agreement to use My Virtual Doctor, LLC services.  In order to be valid, my account myst contain certain required true, correct and verifiable information about my identity and medical history.  In order to maintain access to My Virtual Doctor, LLC services, and in order for My Virtual Doctor, LLC to provide me important information regarding my medical treatment and my health, it is my responsibility to update my personal account information and to notify My Virtual Doctor, LLC of any changes in my home address, e-mail address, telephone number, or guardian or emergency contact.  My failure to do so may result in interruption of service or My Virtual Doctor, LLC's inability to deliver to me important time sensitive information about my medical condition, medications, laboratory and diagnostic test results.  I may update my personal information by accessing my registration information through the My Virtual Doctor, LLC Website by loggin in with my username and password.</p>
<p>My Virtual Doctor, LLC's telemedicine consults are provided by clinicians dedicated to the safe and effective, evidence-based practice of telemedicine.  I choose to enter into a clinician-patient relationship with My Virtual Doctor, LLC's clinicians.  I agree to have my medical history and other diagnostic and medical documentation review by one of My Virtual Doctor, LLC clinicians.  I acknowledge that My Virtual Doct, LLC clinicians do not prescribe DEA controlled substances, non-therapeutic drugs and certain other drugs which may be harmful because of their potential for abuse.  My Virtual Doctor, LLC's clinicians reserve the right to deny care for potential misuse of services.  My Virtual Doctor, LLC operates subject to state regulation and may not be available in certain states.</p>
<p>For individuals who are under age 18, a parent or legal guardian must accept this Agreement on his or her behalf.  I agree at all times not to falsify or misrepresent my identity or my authority to act on behalf on another person.  I also agree to not attempt or facilitate to attack or undermine the security of the integrity of the systems or networks of My Virtual Doctor, LLC, the software provider or any of its authorized agents or affiliates.</p>
<p>I understand that My Virtual Doctor, LLC should never be used for urgent matters.  Therefore, for all urgent matters that I believe may immediately affect my health or well-being, I will, without delay, go to the emergency department of a local hospital, and/or dial 911.</p>
<p>My Virtual Doctor, LLC makes the consultation report available for you to review to ensure that relevant signs and symptoms of patients presenting complaint are accurately documented and that you understand the treatment decision and instructions issued by the My Virtual Doctor, LLC health care provider.  I am advised to immediately contact My Virtual Doctor, LLC if I disagree with or do not understand the contents of the consultation report or the instructions issued by the treating My Virtual Doctor, LLC health care provider.</p>
<p>I understand that My Virtual Doctor, LLC clinicians or staff may send me messages.  These messages may contain information that is important to my health and medical care.  It is my responsibility to monitor these messages.  By entering my valid and functional e-mail address and mobile phone number, I have enabled My Virtual Doctor, LLC to notify me of messages sent to my My Virtual Doctor, LLC inbox.  I will update my e-mail address on My Virtual Doctor, LLC as needed.  I agree not to hold My Virtual Doctor, LLC or its authorized vendors and agents liable for any loss, injury or claims of any kind resulting from My Virtual Doctor, LLC messages that I fail to read in a timely manner.  I understand that contents of any message may be stored in my own permanent health record.  I agree that all communication will be in regard to my own health condition(s).  I understand that asking for advice on behalf of another person could potentially be harmful and is a violation of the My Virtual Doctor, LLC Terms and Conditions.  My Virtual Doctor, LLC and its clinicians do not assume any responsibility for health information or services used by persons other than the primary account holder.</p>
<p><strong>Deactivation of Account</strong></p>
<p>I understand that my account may be deactivated upon my request or at the discretion of My Virtual Doctor, LLC for failure to meet these Terms and Conditions</p>
<p><strong>Disclaimer</strong></p>
<p>I understand that my account may not be available to me at all times due to unanticipated system failures, back-up procedures, maintenance, or other causes beyond the control of the My Virtual Doctor, LLC or its authorized vendors and agents.  Access is provided on an "as-is as-is available" basis and My Virtual Doctor, LLC or its authorized vendors and agents do not guarantee that I will be able to access my account at all times</p>
<p>I understand that My Virtual Doctor, LLC or its authorized vendors and agents take no responsibility for and disclaim any and all liability arising from any inaccuracies or defects in software, communication lines, the virtual private network, the Internet or my Internet Service Provider (ISP), access system, computer hardware or software, or any other service or device that I use to access my account.</p>
<p>I understand taht the health care services rendered by My Virtual Doctor, LLC's clinicians are subject to their discretion and professional judgement.  I understand that My Virtual Doctor, LLC operates subject to state regulation and may not be available in certain states.</p>
<p><strong>Surveys</strong></p>
<p>I understand that from time to time I may be asked to complete patient satisfaction surveys.  My Virtual Doctor, LLC or its software provider, vendors, and agents may analyze information submitted via these surveys as part of descriptive (demographic) studies and reports.  In such cases all of my personal indentifying information will be removed.</p>
<p>If any provision or provisions of this Agreement shall be held to be invalid, illegal or unenforceable, the validity, legality and enforceability of the remaining provisions shall not be affected thereby.</p>
<p>It is understood that no delay or omission in exercising any right or rememdy identified herin shall constitute a waiver of such right or remedy, and shall not be construed as a bar to or a waiver of any such right or remedy on any other occasion.</p>
<p>My Virtual Doctor, LLC and its authorized agents and I agree to comply with all applicable laws and regulations of governmental bodies or agencies in performance of our respective obligations under this Agreement.</p>
<p> </p>
<p><strong>Consent for Treatment, Statement of Financial Responsibility/Assignment of Benefits</strong></p>
<p><strong>Patient Consent for Treatment</strong></p>
<p>As the patient and primary user for this telemedicine virtual consultation, I voluntarily give my permission to the health care providers of My Virtual Doctor, LLC and such assistants and other health care providers as they may deem necessary to provide medical services to me.  I understand by signing this form, I am authorizing them to treat me for as long as I seek care from My Virtual Doctor, LLC providers, or until I withdraw my consent in writing</p>
<p>Guardian of Patient Consent for Treatment</p>
<p>As the legal guardin or healthcare conservator of the patient for which this telemedicine consultations is being scheduled, I give my permission to the health care providers of My Virtual Doctor, LLC and such assistants and other health care providers as they may deem necessary to provide medical services to me.  I understand by signing this form, I am authorizing them to treat me for as long as I seek care from My Virtual Doctor, LLC providers, or until I withdraw my consent in writing.</p>
<p><strong>Statement of Financial Responsibility/Assignment of Benefits</strong></p>
<p>In consideration of our organization advancing credit to me for my health care and services, I hereby irrevocably assign and transfer to My Virtual Doctor, LLC and treating Clinicians all benefits and payments now due and payable or to become due and payable to me under and self-insurance program, under and third-party actions against any other person or entity, or under any other benefit plan or program (hereafter referred to as Benefits) for this or any other period of care.</p>
<p>I understand and acknowledge that this assignment does not relieve me of my financial responsibility for all My Virtual Doctor, LLC charges and treating Clinician charges incurred by me or anyone on my behalf, and I hereby accept such responsibility, including but not limited to payment of those fees and charges not directly reimbursed to My Virtual Doctor, LLC and treating Clinicians by any Benefit plan or program.  Furthermore, I agree to pay all costs of collection, reasonable attorney's fees and court costs incurred in enforcing this payment obligation.</p>
<p><strong>Authorization to Process Claims and Release Information</strong></p>
<p>I authorize My Virtual Doctor, LLc and the Clinician, caregivers and/or professional corporations that render services to me to process claims for payment by my insurance carrier on my behalf for covered services provided to me by My Virtual Doctor, LLC.  I authorize the release of necessary information, including medical information, regarding medical services rendered during this consultation and treatment or any related services or claim, to my insurance carrier(s), including any managed care plan or other payor, past and/or present employer(s), Medicare, CHAMPUS/TRICARE, authorized private review entities and/or utilization review entitles acting on behalf of such insurance carrier(s), payers, managed care plans and/or employer(s), the billing agents and collection agents or attorneys of My Virtual Doctor, LLC and/or the physicians, caregivers and/or professional corporations, my employers Workers Compensation carrier, and, as applicable, the Social Security Administration, the Health Care Financing Administration, the Peer Review Organization acting on behalf of the federal government and/or any other federal or state agency for the purpose(s) of satisfying charges billed and/or facilitating utilization review and/or otherwise complying with the obligations of state or federal law.  Authorization is hereby granted to release health record data and/or copies to my attending and/or admitting healthcare professional and/or any consulting healthcare professional and/or any healthcare professional I may be referred to for follow up care.  I further authorize My Virtual Doctor, LLC and any other healthcare provider or professional rendering services to me to obtain from any source medical history, examinations, diagnoses, treatments and other health or insurance authorization information for the purpose(s) of satisfying charges billed and/or facilitating utilization review, provideing medical treatment and/or the evaluation of such treatment, and/or otherwise complying with the obligations of federal law.  A photocopy of this Authorization may be honored.</p>
<p><strong>Medicare Patients Certification, Authorization to Release Information, Request</strong></p>
<p>I certify that the information given by my in applying for payment under Title XVIII of the Social Security Act is correct.  I authorize any holder of medical or other information about me to release to the Social Security Administration or its intermediaries or carriers any information needed for this or a RELATED Medicare claim.  I request that payment of authorized benefits be made on my behalf.</p>

                            
                            
                            </td>
                        </tr>

                    </table>


					<table class="table table-striped custab">
                        <tr>
                            <td>
                                <input type="checkbox" value="" id="acknowledge_check" /> <label for="box8">I acknowledge and agree to the Terms and Conditions and Consent to Treatment stated	above.</label>
							</td>
                        </tr>
                    </table>
                    <table class="table table-striped custab">
                        <tr>
                            <td>
                                <button class="btn btn-default prevBtn btn-info pull-left step_3_back" type="button" >
                                    <i class="fa fa-angle-double-left"></i> Back To Step 3
                                </button>
                                <button class="btn btn-default nextBtn dark-pink pull-right step_4_next" type="button" >
                                    Proceed To Step 5 <i class="fa fa-angle-double-right"></i>
                                </button>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <!-- Wizard STEP 4 END -->
            
            

            <!-- Wizard STEP 5 -->
            <div class="setup-content" id="step-5">
                <div class="col-sm-12"><br  />
                    <h4 class="consult-step-heading" style="border-bottom:0px">Enter Payment Information <br  /> <span class="text-center" style="width: 100%;margin-bottom: 23px;">Show what the patient is paying for (Appointment time and price and total)</span></h4><br  />

                    <div id="step5_error" class="col-md-12"></div>

                    <table class="table table-striped custab">
                        <tr>
                            <td class="pull-left" width="50%"> Consultation Duration |<strong id="consultationDurationText"> Phone / Skype Consultation (30 Minutes)</strong></td	>
                            <td class="pull-right" width="50%"> Appointment Date |  <strong id="consultationDateText">Wednesday March 23, 2016</strong></td	>
                        </tr>	
                        <tr>
                            <td class="pull-left"  width="50%"> Consultation Fee | <strong id="consultationFeeText">$100</strong></td>
                            <td class="pull-right"  width="50%"> Appointment Time | <strong id="consultationTimeText">112:00 PM CST</strong></td>
                        </tr>

                    </table>

                    <?php  if ( $doctor_stripe['stripeSecretKey'] == '' || $doctor_stripe['stripePubKey'] == "" ) { ?>
                        <div class="alert alert-danger" role="alert">Doctor's payment method is not configured yet. Please contact doctor via contact form.</div>
                        <table class="table table-striped custab">
                            <tr>
                                <td>
                                    <button class="btn btn-default prevBtn btn-info pull-left step_4_back" type="button" > <i class="fa fa-angle-double-left"></i> Back To Step 4 </button>
                                </td>
                            </tr>
                        </table>
                    <?php } else { ?>

                    <table class="table table-striped custab">
                        <tbody>
                        <tr>
                            <td width="25%">Name on Card</td>
                            <td><input type="text" value="<?php echo $_SESSION["mvdoctorVisitorUserFname"] . ' ' . $_SESSION["mvdoctorVisitorUserLname"]; ?>" class="form-control" id="name_on_card" name="card-holder-name" id="card-holder-name" placeholder="Card Holder's Name"></td>
                        </tr>
                        <tr>
                            <td width="25%">Card Number</td>
                            <td> <input type="text" size="20" data-stripe="number" class="form-control" placeholder="Debit/Credit Card Number"></td>
                        </tr>
                        <tr>
                            <td width="25%">Expiration Date</td>
                            <td><div class="col-xs-6 row">
                                    <select class="form-control col-sm-2" data-stripe="exp_month">
                                        <option value="01">Jan (01)</option>
                                        <option value="02">Feb (02)</option>
                                        <option value="03">Mar (03)</option>
                                        <option value="04">Apr (04)</option>
                                        <option value="05">May (05)</option>
                                        <option value="06">June (06)</option>
                                        <option value="07">July (07)</option>
                                        <option value="08">Aug (08)</option>
                                        <option value="09">Sep (09)</option>
                                        <option value="10">Oct (10)</option>
                                        <option value="11">Nov (11)</option>
                                        <option value="12">Dec (12)</option>
                                    </select>
                                </div>
                                <div class="col-xs-6">
                                    <select class="form-control" data-stripe="exp_year">
                                        <?php
                                        $year1 = date('y');
                                        $year2 = date('Y');
                                        for( $i = 0 ; $i < 25; $i++ ) {
                                            echo "<option value='$year1'>$year2</option>";
                                            $year1++;
                                            $year2++;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">Card CVV</td>
                            <td><input type="text" class="form-control" size="4" data-stripe="cvc" placeholder="Security Code"></td>
                        </tr>

                        </tbody>
                    </table>

                    <table class="table table-striped custab">
                        <tbody>
                        <tr>
                            <td width="25%"><strong>Billing Information</strong></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td width="25%">Address 1</td>
                            <td><input type="text" class="form-control" id="address_1" value="<?php echo $_SESSION["mvdoctorVisitorUserAddress"]; ?>" placeholder="Address 1"></td>
                        </tr>
                        <tr>
                            <td width="25%">Address 2</td>
                            <td> <input type="text" class="form-control" id="address_2" placeholder="Address 2"></td>
                        </tr>
                        <tr>
                            <td width="25%">City</td>
                            <td><input type="text" class="form-control" value="<?php echo $_SESSION["mvdoctorVisitorUserCity"]; ?>" id="userCity" placeholder="City"></td>
                        </tr>
                        <tr>
                            <td width="25%">Country</td>
                            <td><select class="form-control bfh-countries" data-country="<?php echo $_SESSION["mvdoctorVisitorUserCountry"]; ?>" id="userCountry"></select></td>
                        </tr>
                        <tr>
                            <td width="25%">State/Province</td>
                            <td><select class="form-control bfh-states" data-state="<?php echo $_SESSION["mvdoctorVisitorUserState"]; ?>" data-country="userCountry" id="userState"></select></td>
                        </tr>
                        <tr>
                            <td width="25%">Zip/Postal Code</td>
                            <td><input type="text" class="form-control" id="userZip" value="<?php echo $_SESSION["mvdoctorVisitorUserZip"]; ?>" placeholder="Zip/Postal Code"></td>
                        </tr>
                        <tr>
                            <td width="25%">Phone</td>
                            <td><input type="text" class="form-control" id="userPhone" placeholder="Phone"></td>
                        </tr>

                        </tbody>
                    </table>

                    <table class="table table-striped custab">
                        <tr>
                            <td>
                                <button class="btn btn-default prevBtn btn-info pull-left step_4_back" type="button" > <i class="fa fa-angle-double-left"></i> Back To Step 4 </button>
                                <button class="btn btn-default nextBtn dark-pink pull-right step_5_next" type="button"> Pay Now</button>
                            </td>
                        </tr>
                    </table>
                    <?php } ?>
                </div>
            </div>
            <!-- Wizard STEP 5 END -->
            <!-- Wizard STEP 6 -->
            <div class="setup-content" id="step-6">
                <div class="col-sm-12 "><br  />
                    <h4 class="consult-step-heading">Thank you! Your appointment has been scheduled.</h4><br  />
                    <p class="text-center  consult-calender">The doctor will confirm the appointment date once the payment has been processed. Once the appointment is confirmed, please make sure to show up at least 10 minutes before the selected date/time. The appointment will also show up under your Dashboard > Confirmed Appointments page for future reference.</p>

                    <table class="table table-striped custab">
                        <tr>
                            <td>
                                <a class="btn btn-default dark-pink pull-right finished_all_step" type="button" href="<?php echo HTTP_SERVER . 'patient.html' ?>"> Go to dashboard</a>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
            <!-- Wizard STEP 6 END -->
        </fieldset><!-- form contents END -->
    </form>
<?php } ?>