<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

try {
    $query = "SELECT doc.id, doc.url, doc.doctor_name, doc.specialization, doc.degree, doc.fees, doc.location, doc.experience,
                 doc.sex, doc.featured_image, doc.ratings, doc.ratingCount FROM `" . DB_PREFIX . "doctors` as doc WHERE `status` = 'publish' LIMIT 0,5";
    $result = $db->prepare( $query );
    $result->execute();
    $search_data = $result->fetchAll();

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}
?>

<div class="col-md-3  col-sm-12  col-xs-12 sidebar">
    <h1 class="sidebar-heading">Similar Top Doctors</h1>
    <?php
    if ( !empty($search_data) ):
        foreach ( $search_data as $data ):
    ?>
        <div class="doc-profile-detail-side">
            <div class="col-sm-4 col-md-4 col-xs-4">
                <img src="<?php echo HTTP_SERVER . 'images/doctors/full/' . $data['featured_image']; ?>"
                     alt="" class="img-rounded img-responsive" />

            </div>
            <div class="col-sm-8 col-md-8 col-xs-8 row">
                <div class="doctor-rating-name-section">
                    <h4><a href="<?php echo HTTP_SERVER . 'doctor/' . $data['url'] . '.html'; ?>"><?php echo $data['doctor_name']; ?></a></h4>
                    <div>
                        <input type="hidden" class="rating" value="<?php echo $data['ratings']; ?>" data-readonly data-fractions="2"/>
                        <br>
                        <small><li><i class=" fa fa-graduation-cap"></i> <?php echo $data['degree']; ?></li></small>
                    </div>
                </div>

            </div>
            <div class="col-sm-8 col-md-12 col-xs-8 row">
                <ul class="medial-info Obstetrics">
                    <li><i class=" fa fa-stethoscope"></i> <?php echo $data['specialization']; ?></li>
                    <li> <i class=" fa fa-hospital-o"></i><?php echo $data['location']; ?></li>
                </ul>
                <br style="clear: both;">
            </div>
        </div>

    <?php
        endforeach;
    else:
    ?>

    <?php
    endif;
    ?>
</div>