<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

$aid = isset($_GET['id']) ? intval($_GET['id']) : 0;

if ( $aid > 0 ) {

    //get post details
    try {
        $select = "SELECT tips.id, tips.view, tips.title, tips.excerpt, tips.content, tips.image, doc.doctor_name";
        $table = " FROM `" . DB_PREFIX . "health_tips` as tips, `" . DB_PREFIX . "doctors` as doc";
        $where = " WHERE doc.id = tips.doctorId AND tips.status = 'publish' AND tips.id=? ";
        $lim = " LIMIT 1";

        $sql = $select . $table. $where  . $lim;
        $st = $db->prepare( $sql );
        $st->execute( array($aid) );

        if ( $st->rowCount() > 0 ) {
            $data = $st->fetch();
            $data['image'] = unserialize(base64_decode($data['image']));

            //update view count
            if ( $data['view'] != '' && $data['view'] > 0 ) {
                $view = $data['view'] + 1;
            } else {
                $view = 1;
            }
            if ( $view ) {
                $sql = 'UPDATE ' . DB_PREFIX . 'health_tips SET view = ? WHERE id = ?';
                $st = $db->prepare($sql);
                $st->execute(array($view, $aid));
            }

        } else {
            $data = array();
        }

    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }
}

//check for favorite doctor
/*
 * 0 - user not logged in
 * 1 - user is logged in but not added to favorite
 * 2 - user added this doctor to favorite
 */
$is_doctor_favorite = 0;
if ( isset($_SESSION["mvdoctorVisitornUserId"])  ) {
    $sql = 'SELECT userId  FROM `'. DB_PREFIX .'pinned_post` WHERE `userId` = ? AND postId = ? LIMIT 1';

    try {
        $st = $db->prepare( $sql );
        $st->execute( array( $_SESSION["mvdoctorVisitornUserId"], $aid ) );

        if ( $st->rowCount() > 0 ) {
            $is_doctor_favorite = 2;
        } else {
            $is_doctor_favorite = 1;
        }

    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }
}

if ( $aid > 0 && isset($data) && is_array($data) ) {  ?>
    <div class="health-detail health-detail-innerpage clearfix">
        <div class="col-sm-12 col-md-12 col-xs-8 row ">
        		<div class="col-md-12 pull-left health-detail">
                 <h1><?php echo $data['title']; ?></h1>
               </div>
            <div class="col-md-12  pull-left health-detail">

                <div class="col-md-4 text-right pull-right row">
                    <a href="#" class="btn btn-default dark-pink <?php if ($is_doctor_favorite == 2) { echo "removeFavorite"; } else { echo "addToFavorite"; } ?>" data-login="<?php if ($is_doctor_favorite=='0') { echo 'no'; } else { echo 'yes'; } ?>" data-id="<?php echo $aid; ?>" title="Pin now, read later"><i class="fa fa-thumb-tack"></i> <?php if ($is_doctor_favorite == 2) { echo " Unpin Article"; } else { echo " Pin Article"; } ?></a>
                    <a href="<?php echo HTTP_SERVER; ?>blog.html" class="btn btn-default dark-pink"><i class="fa fa-book"></i> Back to Articles</a>
                </div>

                 <div class="col-md-5 text-left pull-left row">
                     <div class="share-name">Share This Article:
                         <span class='st__hcount' displayText=''></span>
                         <span class='st_facebook_hcount' displayText='Facebook' st_summary="<?php echo html_entity_decode($data['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $data['image']['img1']; ?>" st_title="<?php echo $data['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $data['id'] . '.html'; ?>'></span>
                         <span class='st_twitter_hcount' displayText='Tweet' st_summary="<?php echo html_entity_decode($data['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $data['image']['img1']; ?>" st_title="<?php echo $data['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $data['id'] . '.html'; ?>'></span>
                         <span class='st_email_hcount' displayText='Email' st_summary="<?php echo html_entity_decode($data['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $data['image']['img1']; ?>" st_title="<?php echo $data['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $data['id'] . '.html'; ?>'></span>
                     </div>
                </div>
            </div>
           
        </div>
        <div class="col-md-12">
            <p style="text-align: center;"><strong>By <?php echo $data['doctor_name'] ?></strong></p>
            <?php echo html_entity_decode($data['content']); ?>
        </div>
    </div>

    <div class="col-md-12  pull-left health-detail">
        <div class="col-md-4 text-right pull-right row">
            <a href="<?php echo HTTP_SERVER; ?>blog.html" class="btn btn-default dark-pink"><i class="fa fa-book"></i> Back to Articles</a>
        </div>
         <div class="col-md-5 text-left pull-left row">
             <div class="share-name">Share This Article:
                 <span class='st__hcount' displayText=''></span>
                 <span class='st_facebook_hcount' displayText='Facebook' st_summary="<?php echo html_entity_decode($data['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $data['image']['img1']; ?>" st_title="<?php echo $data['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $data['id'] . '.html'; ?>'></span>
                 <span class='st_twitter_hcount' displayText='Tweet' st_summary="<?php echo html_entity_decode($data['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $data['image']['img1']; ?>" st_title="<?php echo $data['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $data['id'] . '.html'; ?>'></span>
                 <span class='st_email_hcount' displayText='Email' st_summary="<?php echo html_entity_decode($data['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $data['image']['img1']; ?>" st_title="<?php echo $data['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $data['id'] . '.html'; ?>'></span>
             </div>
        </div>
    </div>
<?php
} else {
    echo '<h2 class="no-results">Invalid Article Link.</h2>';
}