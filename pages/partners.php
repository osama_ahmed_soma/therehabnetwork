<section class="grey clearfix" id="service"  style="background:url(img/back.png);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1 class=" Virtual-heading">My Virtual Doctor proudly partners with leaders in the telehealth industry. </h1>
            </div>
            <div class="col-md-12">
                <div class="col-md-6  col-md-offset-3 text-center">
                    <h2 class="simple pra">Want to become a My Virtual Doctor partner?<br /><br />
                        <button type="button" class="btn btn-primary">Learn More</button></h2>
                </div>

            </div>


        </div>
    </div>
    </div>
</section>

<section class="Finding clearfix">
    <div class="container">
        <div class="row">

            <div class="col-md-4 col-sm-4 col-sx-4">
                <div class="col-md-12 text-center">
                    <img src="<?php echo HTTP_SERVER; ?>img/secondopinions.png" class="dp-center-xs ">
                </div>
                <div class="col-md-12 text-center">
                    <h1 class="Finding-heading"><a href="https://SecondOpinions.com" target="new">SecondOpinions.com</a></h1>
                    <p class="Finding-pra">SecondOpinions.com is a Medical Consultation and Second Opinion leader providing second opinion consultation service in all areas of medicine, including radiology.Utilizing the power of the internet, we are connecting patients around the globe with some of the world's best doctors.</div>
            </div>
            <div class="col-md-4 col-sm-4 col-sx-4">
                <div class="col-md-12 text-center">
                    <img src="<?php echo HTTP_SERVER; ?>img/evisit.png" class="dp-center-xs">
                </div>
                <div class="col-md-12 text-center">
                    <h1 class="Finding-heading"><a href="https://eVisit.com" target="new">eVisit.com</a></h1>
                    <p class="Finding-pra">eVisit makes telehealth simple for healthcare providers. Collect co-pays, diagnose, eprescribe and submit patient charts for telemedicine reimbursement.</div>
            </div>
            <div class="col-md-4 col-sm-4 col-sx-4">
                <div class="col-md-12 text-center">
                    <img src="<?php echo HTTP_SERVER; ?>img/wecounsel.png" class="dp-center-xs">
                </div>
                <div class="col-md-12 text-center">
                    <h1 class="Finding-heading"><a href="https://WeCounsel.com" target="new">WeCounsel.com</a></h1>
                    <p class="Finding-pra">WeCounsel is a HIPAA-compliant, telehealth engagement platform with an emphasis in behavioral health. Our comprehensive software serves as a turnkey virtual office for healthcare providers and businesses, complete with a custom video conferencing experience.</div>
            </div>


        </div>
    </div>
</section>