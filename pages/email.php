<?php

function sanitize ($input, $condition = 1){
 if($condition == 1){
  return htmlentities($input, ENT_QUOTES, 'UTF-8');
 }elseif ($condition == 2){
  //Allow quotes for database entry
  $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
     '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
     '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
     '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
  ); 
  $input = preg_replace($search, '', $input); 
  return htmlentities(strip_tags($input));
 }
}
	$name = sanitize($_POST['InputNameFirst']);
	$namelast = sanitize($_POST['InputNameLast']);
	$email = sanitize($_POST['InputEmail']);
	$phone = sanitize($_POST['InputPhone']);
	$clinic = sanitize($_POST['InputClinic']);
	 $to = 'sales@myvirtualdoctor.com';
	 $subject = 'MyVirtualDoctor - Demo Request Form';
 $message = '
 <html>
 <head>
  <title>MyVirtualDoctor - Demo Request <Form></Form></title>
 </head>
 <body>
 <h3>Someone has filled out the demo request form and here are the details</h3>
  <p>First Name:  '.$name.',</p>
  <p>Last Name:  '.$namelast.',</p>
  <p>Email Address: '.$email.',</p>
  <p>Phone: '.$phone.'</p>
  <p>Clinic: '.$clinic.'</p>
 </body> 
 </html>
 ';
 
 // ------------------------------------------------------------
 // SET HEADERS FOR HTML MAIL
 // ------------------------------------------------------------
 $headers  = 'MIME-Version: 1.0' . "\r\n";
 $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
 //$headers .= 'To: <'.$txbEmail.'>' . "\r\n";
 $headers .= 'From: MyVirtualDoctor - Demo Request Form <'.$email.'>' . "\r\n";
 //$headers .= 'Cc: anothermail@foo.org' . "\r\n";
 //$headers .= 'Bcc: '.$from_address.'' . "\r\n";
 
 // ------------------------------------------------------------
 // SEND E-MAIL
 // ------------------------------------------------------------
 $email_form = mail($to, $subject, $message, $headers);
if ($email_form){
	header('Location:http://excesssol.com/projects/arsalan/myvirtualdoctor/php/request-a-demo.html?status=success');
}
else
{
 $msg = "Not send";
}
?>