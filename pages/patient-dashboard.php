<section class="dashboard-new-section patient-new-section clearfix">
    <div class="container">
        <div class="row">
        	<div class="nav-pills">
            	<ul>
                    <li class="col-md-2 col-sm-2 col-xs-4 dns-icon active"><a href="#dasboard" data-toggle="tab"> <i class="fa fa-calendar"></i> Dashboard</a></li>
                    <li class="col-md-2 col-sm-2 col-xs-4 dns-icon "><a href="#health" data-toggle="tab"> <i class="fa fa-heart"></i> My Health</a></li>
                    <li class="col-md-2 col-sm-2 col-xs-4 dns-icon "><a href="#provider" data-toggle="tab"> <i class="fa fa-star"></i>  My Providers</a>
                    	
                    </li>
                    <li class="col-md-2 col-sm-2 col-xs-4 dns-icon "><a href="#Pinned" data-toggle="tab"> <i class="fa fa-paperclip"></i>  Pinned Blogs</a></li>
                    <li class="col-md-2 col-sm-2 col-xs-4 dns-icon "><a href="#vtab1" data-toggle="tab"> <i class="fa fa-gears"></i> Account Settings</a>
                    	<ul>
                        	<li><a href="#provider" data-toggle="tab"> <i class="fa fa-gears"></i>  General Information</a></li>
                        	<li><a href="#provider" data-toggle="tab"> <i class="fa fa-gears"></i> Profile</a></li>
                        	<li><a href="#provider" data-toggle="tab"> <i class="fa fa-gears"></i>  Workplaces</a></li>
                        	<li><a href="#provider" data-toggle="tab"> <i class="fa fa-gears"></i>  Specializations</a></li>
                        	<li><a href="#provider" data-toggle="tab"> <i class="fa fa-gears"></i>  Practice Biodata</a></li>
                        	<li><a href="#provider" data-toggle="tab"> <i class="fa fa-gears"></i>  Languages</a></li>
                        	<li><a href="#provider" data-toggle="tab"> <i class="fa fa-gears"></i>  Procedures Attempted</a></li>
                        	<li><a href="#provider" data-toggle="tab"> <i class="fa fa-gears"></i>  Conditions Treated</a></li>
                        	<li><a href="#provider" data-toggle="tab"> <i class="fa fa-gears"></i>  Awards & Distinctions</a></li>
                            <li><a href="#provider" data-toggle="tab"> <i class="fa fa-gears"></i>  Payment Method</a></li>
                            
                        </ul>
                    
                    </li>
                    <li class="col-md-2 col-sm-2 col-xs-4 dns-icon "><a href="#vtab1" data-toggle="tab"> <i class="fa fa-users"></i> Help and Support</a></li>
                </ul>
          	</div>
        </div>
    </div>
</section>
<div class="tab-content">

<section class="dashboard-new-section-main patient-blog-section clearfix tab-pane fade" role="tabpanel" id="Pinned">
	<div class="container">
    <div class="dnsm-list clearfix">
    <div id="bloglisting">
    <div class="dnsm-list-col">
        <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/health_tips/full/stock_footage_senior_female_patient_in_hospital_bed_being_treated_by_doctor_and_nurse.jpg);">
        </div>
        <div class="dnsm-lc-txt-area">
            <ul>
                <li> <i class="fa fa-clipboard"></i>Difference Between Telemedicine and Telehealth</li>
                <ul class="medial-info article-media-info">
                    <li> by <strong> Dr. Sherri L. Dehaas</strong></li>
                    <li><i class=" fa fa-stethoscope"></i> Family Medicine</li>
                    <li><i class=" fa fa-graduation-cap"></i>  MD</li>
                </ul>
            </ul>
            <p>The healthcare system in particular has seen much of this advancement, as new technologies each day are aimed at placing healthcare in the hands of more people than ever before.</p>
        </div>
       <div class=" health-social">
            <span class='st_facebook_large' displayText='Facebook' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_twitter_large' displayText='Tweet' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_email_large' displayText='Email' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
        </div>
        <div class="dnsm-lc-btn unpin"><a href="#" class="btn-block"><i class="fa fa-remove"></i> Unpin Article</a></div>
    </div>
    
    <div class="dnsm-list-col">
        <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/health_tips/full/stock_footage_senior_female_patient_in_hospital_bed_being_treated_by_doctor_and_nurse.jpg);">
        </div>
        <div class="dnsm-lc-txt-area">
            <ul>
                <li> <i class="fa fa-clipboard"></i>Difference Between Telemedicine and Telehealth</li>
                <ul class="medial-info article-media-info">
                    <li> by <strong> Dr. Sherri L. Dehaas</strong></li>
                    <li><i class=" fa fa-stethoscope"></i> Family Medicine</li>
                    <li><i class=" fa fa-graduation-cap"></i>  MD</li>
                </ul>
            </ul>
            <p>The healthcare system in particular has seen much of this advancement, as new technologies each day are aimed at placing healthcare in the hands of more people than ever before.</p>
        </div>
       <div class=" health-social">
            <span class='st_facebook_large' displayText='Facebook' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_twitter_large' displayText='Tweet' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_email_large' displayText='Email' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
        </div>
        <div class="dnsm-lc-btn unpin"><a href="#" class="btn-block"><i class="fa fa-remove"></i> Unpin Article</a></div>
    </div>
    <div class="dnsm-list-col">
        <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/health_tips/full/stock_footage_senior_female_patient_in_hospital_bed_being_treated_by_doctor_and_nurse.jpg);">
        </div>
        <div class="dnsm-lc-txt-area">
            <ul>
                <li> <i class="fa fa-clipboard"></i>Difference Between Telemedicine and Telehealth</li>
                <ul class="medial-info article-media-info">
                    <li> by <strong> Dr. Sherri L. Dehaas</strong></li>
                    <li><i class=" fa fa-stethoscope"></i> Family Medicine</li>
                    <li><i class=" fa fa-graduation-cap"></i>  MD</li>
                </ul>
            </ul>
            <p>The healthcare system in particular has seen much of this advancement, as new technologies each day are aimed at placing healthcare in the hands of more people than ever before.</p>
        </div>
       <div class=" health-social">
            <span class='st_facebook_large' displayText='Facebook' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_twitter_large' displayText='Tweet' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_email_large' displayText='Email' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
        </div>
        <div class="dnsm-lc-btn unpin"><a href="#" class="btn-block"><i class="fa fa-remove"></i> Unpin Article</a></div>
    </div>
    <div class="dnsm-list-col">
        <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/health_tips/full/stock_footage_senior_female_patient_in_hospital_bed_being_treated_by_doctor_and_nurse.jpg);">
        </div>
        <div class="dnsm-lc-txt-area">
            <ul>
                <li> <i class="fa fa-clipboard"></i>Difference Between Telemedicine and Telehealth</li>
                <ul class="medial-info article-media-info">
                    <li> by <strong> Dr. Sherri L. Dehaas</strong></li>
                    <li><i class=" fa fa-stethoscope"></i> Family Medicine</li>
                    <li><i class=" fa fa-graduation-cap"></i>  MD</li>
                </ul>
            </ul>
            <p>The healthcare system in particular has seen much of this advancement, as new technologies each day are aimed at placing healthcare in the hands of more people than ever before.</p>
        </div>
       <div class=" health-social">
            <span class='st_facebook_large' displayText='Facebook' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_twitter_large' displayText='Tweet' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_email_large' displayText='Email' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
        </div>
        <div class="dnsm-lc-btn unpin"><a href="#" class="btn-block"><i class="fa fa-remove"></i> Unpin Article</a></div>
    </div>
    <div class="dnsm-list-col">
        <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/health_tips/full/stock_footage_senior_female_patient_in_hospital_bed_being_treated_by_doctor_and_nurse.jpg);">
        </div>
        <div class="dnsm-lc-txt-area">
            <ul>
                <li> <i class="fa fa-clipboard"></i>Difference Between Telemedicine and Telehealth</li>
                <ul class="medial-info article-media-info">
                    <li> by <strong> Dr. Sherri L. Dehaas</strong></li>
                    <li><i class=" fa fa-stethoscope"></i> Family Medicine</li>
                    <li><i class=" fa fa-graduation-cap"></i>  MD</li>
                </ul>
            </ul>
            <p>The healthcare system in particular has seen much of this advancement, as new technologies each day are aimed at placing healthcare in the hands of more people than ever before.</p>
        </div>
       <div class=" health-social">
            <span class='st_facebook_large' displayText='Facebook' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_twitter_large' displayText='Tweet' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_email_large' displayText='Email' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
        </div>
        <div class="dnsm-lc-btn unpin"><a href="#" class="btn-block"><i class="fa fa-remove"></i> Unpin Article</a></div>
    </div>
    <div class="dnsm-list-col">
        <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/health_tips/full/stock_footage_senior_female_patient_in_hospital_bed_being_treated_by_doctor_and_nurse.jpg);">
        </div>
        <div class="dnsm-lc-txt-area">
            <ul>
                <li> <i class="fa fa-clipboard"></i>Difference Between Telemedicine and Telehealth</li>
                <ul class="medial-info article-media-info">
                    <li> by <strong> Dr. Sherri L. Dehaas</strong></li>
                    <li><i class=" fa fa-stethoscope"></i> Family Medicine</li>
                    <li><i class=" fa fa-graduation-cap"></i>  MD</li>
                </ul>
            </ul>
            <p>The healthcare system in particular has seen much of this advancement, as new technologies each day are aimed at placing healthcare in the hands of more people than ever before.</p>
        </div>
       <div class=" health-social">
            <span class='st_facebook_large' displayText='Facebook' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_twitter_large' displayText='Tweet' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_email_large' displayText='Email' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
        </div>
        <div class="dnsm-lc-btn unpin"><a href="#" class="btn-block"><i class="fa fa-remove"></i> Unpin Article</a></div>
    </div>
    <div class="dnsm-list-col">
        <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/health_tips/full/stock_footage_senior_female_patient_in_hospital_bed_being_treated_by_doctor_and_nurse.jpg);">
        </div>
        <div class="dnsm-lc-txt-area">
            <ul>
                <li> <i class="fa fa-clipboard"></i>Difference Between Telemedicine and Telehealth</li>
                <ul class="medial-info article-media-info">
                    <li> by <strong> Dr. Sherri L. Dehaas</strong></li>
                    <li><i class=" fa fa-stethoscope"></i> Family Medicine</li>
                    <li><i class=" fa fa-graduation-cap"></i>  MD</li>
                </ul>
            </ul>
            <p>The healthcare system in particular has seen much of this advancement, as new technologies each day are aimed at placing healthcare in the hands of more people than ever before.</p>
        </div>
       <div class=" health-social">
            <span class='st_facebook_large' displayText='Facebook' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_twitter_large' displayText='Tweet' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
            <span class='st_email_large' displayText='Email' st_summary="<?php echo html_entity_decode($item['excerpt']); ?>" st_image="<?php echo HTTP_SERVER . 'images/health_tips/full/' . $item['image']['img1']; ?>" st_title="<?php echo $item['title']; ?>" st_url='<?php echo HTTP_SERVER . 'blog/' . $item['id'] . '.html'; ?>'></span>
        </div>
        <div class="dnsm-lc-btn unpin"><a href="#" class="btn-block"><i class="fa fa-remove"></i> Unpin Article</a></div>
    </div>  
    </div>
     </div> </div>
</section>
<section class="dashboard-new-section-main patient-dashbaord-section-main clearfix tab-pane fade in active" role="tabpanel" id="dasboard">
    <div class="container">
    	<div class="admin-pro clearfix">
        		<div class="col-sm-2 col-md-2 text-center">
                   
                      <div  id="upload_photo_div" class="user-display-picture">
                        <img alt="1" class="avatar" src="<?php echo HTTP_SERVER; ?>images/user/full/ziku.jpeg">
            
                      </div>
                      <div class="change-photo">
                        
                        <a href="#" class="btn btn-info">Edit Profile</a>
                      </div>
           			
				</div>
                
                <div class="user-info col-sm-10 col-md-10">
                        <div class="last-updated-info pull-right">
                            <span id="last_updated_at">Last Updated On 05/28/2016</span>
                        </div>
                        <div>
                          <span class="title">Adam Nadler</span>
                        </div>
                        <div>
                          <span id="profile_percentage">Your profile is <strong>92%</strong> complete</span>
                        </div>
                        <div class="progress">
                          <div id="profile_progress_bar" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 92%;">
                            <span class="sr-only"></span>
                          </div>
                        </div>
                        <div>
                          <span class="notification" id="pc_condition">Please complete <strong><a data-remote="true" href="/patients/life_style/info">My Health</a></strong> questions</span>
                        </div>
                </div>
           
                
        </div>
    	<div class="admin-home-pro-banner clearfix" style="background: linear-gradient(rgba(187, 178, 69, 0.07), rgba(103, 103, 103, 0.06)), url(<?php echo HTTP_SERVER; ?>img/34601926.jpg) center bottom;background-size: cover;    background-position: 0px -80px;">
                	<div class="dr-see">
                    	<div class="dr-icon"><i class="fa fa-user-md"></i></div>
                        <div class="dr-text">
                        	<div class="dr-txt-head">See a Doctor</div>
                            <div class="dr-txt-pra">Find a Provider and make an appointment</div>
                        </div>
                    </div>
                
                <div class="user-search-section ">
                	
                    <div class="dr-search">
                    	<!-- Search Section --->
                        <form method="get" action="<?php echo HTTP_SERVER ?>search.html">
                        <div class="input-group hm-search">
                            <span class="input-group-addon">
                            <i class="fa  fa-user-md"></i></span>
                                <select name="location" data-placeholder="Select a state" class="chosen-select form-control paitent-dropdown" tabindex="-1" style="display: none;">
									<option value=""></option>
									<option value="8">Alabama</option><option value="9">Alaska</option><option value="20">Arizona</option><option value="21">Arkansas</option><option value="22">California</option><option value="26">Colorado </option><option value="27">Connecticut </option><option value="28">Delaware </option><option value="29">Florida </option><option value="30">Georgia</option><option value="31">Hawaii </option><option value="32">Idaho</option><option value="33">Illinois</option><option value="34">Indiana</option><option value="35">Iowa</option><option value="36">Kansas</option><option value="37">Kentucky</option><option value="38">Louisiana</option><option value="39">Maine </option><option value="40">Maryland </option><option value="41">Massachusetts</option><option value="42">Michigan</option><option value="43">Minnesota</option><option value="44">Mississippi</option><option value="45">Missouri</option><option value="46">Montana</option><option value="47">Nebraska </option><option value="48">Nevada </option><option value="49">New Hampshire</option><option value="50">New Jersey</option><option value="51">New Mexico </option><option value="52">New York </option><option value="53">North Carolina </option><option value="54">North Dakota</option><option value="55">Ohio </option><option value="56">Oklahoma</option><option value="57">Oregon </option><option value="58">Pennsylvania</option><option value="59">Rhode Island</option><option value="60">South Carolina</option><option value="61">South Dakota </option><option value="62">Tennessee </option><option value="63">Texas </option><option value="64">Utah </option><option value="65">Vermont </option><option value="66">Virginia </option><option value="67">Washington </option><option value="68">West Virginia </option><option value="69">Wisconsin </option><option value="70">Wyoming</option>																	</select>
                            <div class="chosen-container chosen-container-single" style="width: 264px;" title="">
                               
                                <div class="chosen-drop">
                                    <div class="chosen-search">
                                        <input type="text" autocomplete="off" tabindex="5">
                                    </div>
                                    <ul class="chosen-results"></ul>
                                </div>
                            </div>
                        </div>
                   		
                        
                        <div class="input-group hm-search">
                            <span class="input-group-addon">
                                <i class="fa fa-user-md"></i>
                            </span>
                            
                            <div class="chosen-container chosen-container-single" style="width: 270px;" title="">
                               <input type="text" class="chosen-single chosen-default wth-100" autocomplete="off" placeholder="Physicians Name">
                               <div class="chosen-drop">
                                   <div class="chosen-search">
                                        <input type="text" class="chosen-single chosen-default" autocomplete="off" tabindex="5" placeholder="Physicians Name">
                                   </div>
                                    <ul class="chosen-results"></ul>
                               </div>
                           </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Search</button>
                        </form>
                        <!--  End Search Section --->
                        
                        
                    </div>
                </div>
         </div>
    
    
    
        <div class="row">
            <div class="col-md-12  clearfix"><br>
                <div class="dnsm-highlights">Upcoming <strong>Appointments</strong>
                   <div class="left-right-main">
                     <!-- Left and right controls -->
                    <a class="left" href="#doctorlisting" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right" href="#doctorlisting" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    </div>
                </div>
            </div>
            <div class="dnsm-list clearfix">
            
            	<div id="doctorlisting" class="carousel slide" data-ride="carousel"  data-interval="false">
                 <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                	<div class="item active">
                        <div class="dnsm-list-col">
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                            </div>
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>
                                    <li class="date"><i class="fa fa-calendar"></i> 12th, May <i class="fa fa-clock-o"></i>10:30 AM</li>
                                    <li><i class="fa fa-stethoscope"></i> Family Medicine</li>
                                    <li><i class="fa fa-graduation-cap"></i> MD</li>
                                </ul>
                                <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block">Start Appoitment</a></div>
                        </div>
                        <div class="dnsm-list-col">
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                            </div>
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>
                                    <li class="date"><i class="fa fa-calendar"></i> 12th, May <i class="fa fa-clock-o"></i>10:30 AM</li>
                                    <li><i class="fa fa-stethoscope"></i> Family Medicine</li>
                                    <li><i class="fa fa-graduation-cap"></i> MD</li>
                                </ul>
                                <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block">Start Appoitment</a></div>
                        </div>
                        <div class="dnsm-list-col">
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                            </div>
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>
                                    <li class="date"><i class="fa fa-calendar"></i> 12th, May <i class="fa fa-clock-o"></i>10:30 AM</li>
                                    <li><i class="fa fa-stethoscope"></i> Family Medicine</li>
                                    <li><i class="fa fa-graduation-cap"></i> MD</li>
                                </ul>
                                <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block">Start Appoitment</a></div>
                        </div>
                        
              		</div>
                    <div class="item ">
                        <div class="dnsm-list-col">
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                            </div>
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>
                                    <li class="date"><i class="fa fa-calendar"></i> 12th, May <i class="fa fa-clock-o"></i>10:30 AM</li>
                                    <li><i class="fa fa-stethoscope"></i> Family Medicine</li>
                                    <li><i class="fa fa-graduation-cap"></i> MD</li>
                                </ul>
                                <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block">Start Appoitment</a></div>
                        </div>
                        <div class="dnsm-list-col">
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                            </div>
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>
                                    <li class="date"><i class="fa fa-calendar"></i> 12th, May <i class="fa fa-clock-o"></i>10:30 AM</li>
                                    <li><i class="fa fa-stethoscope"></i> Family Medicine</li>
                                    <li><i class="fa fa-graduation-cap"></i> MD</li>
                                </ul>
                                <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block">Start Appoitment</a></div>
                        </div>
                        <div class="dnsm-list-col">
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                            </div>
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>
                                    <li class="date"><i class="fa fa-calendar"></i> 12th, May <i class="fa fa-clock-o"></i>10:30 AM</li>
                                    <li><i class="fa fa-stethoscope"></i> Family Medicine</li>
                                    <li><i class="fa fa-graduation-cap"></i> MD</li>
                                </ul>
                                <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block">Start Appoitment</a></div>
                        </div>
                        
              		</div>
               </div>
               
            </div>
        </div>
    </div>
</section>

<section class="dashboard-new-section-main patient-blog-section clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <div class="col-md-12 clearfix"><div class="dnsm-highlights">Recent  <strong>Activity</strong>
            
             		<div class="left-right-main">
                         <!-- Left and right controls -->
                        <a class="left" href="#bloglisting" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right" href="#bloglisting" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
            
            </div></div>
            
           
            
            <div class="dnsm-list clearfix">
            	<div id="bloglisting" class="carousel slide" data-ride="carousel"  data-interval="false">
                 <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="dnsm-list-col">
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/health_tips/full/stock_footage_senior_female_patient_in_hospital_bed_being_treated_by_doctor_and_nurse.jpg);">
                            </div>
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li> <i class="fa fa-clipboard"></i>Difference Between Telemedicine and Telehealth</li>
                                </ul>
                                <p>The healthcare system in particular has seen much of this advancement.. <a href="#">Read More</a></p>
                            </div>
                           
                        </div>
                        
                       <div class="dnsm-list-col">
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/health_tips/full/stock_footage_senior_female_patient_in_hospital_bed_being_treated_by_doctor_and_nurse.jpg);">
                            </div>
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li> <i class="fa fa-clipboard"></i>Difference Between Telemedicine and Telehealth</li>
                                </ul>
                                <p>The healthcare system in particular has seen much of this advancement.. <a href="#">Read More</a></p>
                            </div>
                          
                        </div>
                        <div class="dnsm-list-col">
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/health_tips/full/stock_footage_senior_female_patient_in_hospital_bed_being_treated_by_doctor_and_nurse.jpg);">
                            </div>
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li> <i class="fa fa-clipboard"></i>Difference Between Telemedicine and Telehealth</li>
                                </ul>
                                <p>The healthcare system in particular has seen much of this advancement.. <a href="#">Read More</a></p>
                            </div>
                            
                        </div>
                       
                    </div>  
                    <div class="item ">
                        <div class="dnsm-list-col">
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/health_tips/full/stock_footage_senior_female_patient_in_hospital_bed_being_treated_by_doctor_and_nurse.jpg);">
                            </div>
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li> <i class="fa fa-clipboard"></i>Difference Between Telemedicine and Telehealth</li>
                                </ul>
                                <p>The healthcare system in particular has seen much of this advancement.. <a href="#">Read More</a></p>
                            </div>
                            
                        </div>
                        
                       <div class="dnsm-list-col">
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/health_tips/full/stock_footage_senior_female_patient_in_hospital_bed_being_treated_by_doctor_and_nurse.jpg);">
                            </div>
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li> <i class="fa fa-clipboard"></i>Difference Between Telemedicine and Telehealth</li>
                                </ul>
                                <p>The healthcare system in particular has seen much of this advancement.. <a href="#">Read More</a></p>
                            </div>
                           
                        </div>
                        <div class="dnsm-list-col">
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/health_tips/full/stock_footage_senior_female_patient_in_hospital_bed_being_treated_by_doctor_and_nurse.jpg);">
                            </div>
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                    <li> <i class="fa fa-clipboard"></i>Difference Between Telemedicine and Telehealth</li>
                                </ul>
                                <p>The healthcare system in particular has seen much of this advancement.. <a href="#">Read More</a></p>
                            </div>
                            
                        </div>
                        
                    </div>  
               </div>
            </div>
        </div>
    </div>
</section>

<section class="dashboard-new-section-main  pad-top-0 clearfix tab-pane fade in active hidden" role="tabpanel" id="dasboard">
    <div class="container">
        <div class="row">
        	<div class="col-md-7">
            	<div class="dnsm-highlights">Appointment<strong> Statistics </strong></div>
            </div>
            
            <div class="col-md-5">
            	<div class="dnsm-highlights">Account<strong> Summary</strong></div>
                <div class="dnsm-revenue-section">
                	<div class="dnsm-rv-sec-list">
                    	<div class="dnsm-rv-sec-lst-icon"> 
                        	<i class="fa fa-bar-chart"></i> <span><i class="fa fa-usd"></i></span>
                        </div>
                        <div class="dnsm-rv-sec-lst-txt">
                        	$200 Spent Revenue Generated 
                        </div>
                     </div>
                     <div class="dnsm-rv-sec-list">
                    	<div class="dnsm-rv-sec-lst-icon">
                        	<i class="fa fa-clock-o"></i>
                        </div>
                        <div class="dnsm-rv-sec-lst-txt">
                        	24 Appointments This Month
                        </div>
                     </div>
                     <div class="dnsm-rv-sec-list">
                    	<div class="dnsm-rv-sec-lst-icon">
                        	<i class="fa fa-refresh"></i>
                        </div>
                        <div class="dnsm-rv-sec-lst-txt">
                        	15 Doctors Consulted
                        </div>
                     </div>
                     <div class="dnsm-rv-sec-list">
                    	<div class="dnsm-rv-sec-lst-icon">
                        	<i class="fa fa-user"></i> <span><i class="fa fa-check"></i></span>
                        </div>
                        <div class="dnsm-rv-sec-lst-txt">
                        	9 New Doctors
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dashboard-new-section-main patient-dashbaord-section-main clearfix tab-pane fade" role="tabpanel" id="health">
    <div class="container">
        <div class="row admin-pro">
        		<div class="col-sm-2 col-md-2 text-center">
                    
                      <div id="upload_photo_div" class="user-display-picture">
                        <img alt="1" class="avatar" src="<?php echo HTTP_SERVER; ?>/images/user/full/ziku.jpeg">
            
                      </div>
                      <div class="change-photo">
                        
                        <a href="#" class="btn btn-info">Edit Profile</a>
                      </div>
           			</form>      
				</div>
                
                <div class="user-info col-sm-10 col-md-10">
                        <div class="last-updated-info pull-right">
                            <span id="last_updated_at">Last Updated On 05/28/2016</span>
                        </div>
                        <div>
                          <span class="title">Adam Nadler</span>
                        </div>
                        <div>
                          <span id="profile_percentage">Your profile is <strong>92%</strong> complete</span>
                        </div>
                        <div class="progress">
                          <div id="profile_progress_bar" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 92%;">
                            <span class="sr-only"></span>
                          </div>
                        </div>
                        <div>
                          <span class="notification" id="pc_condition">Please complete <strong><a data-remote="true" href="/patients/life_style/info">My Health</a></strong> questions</span>
                        </div>
                </div>
                
                
        </div>
        
        
        <section class="health-madication-tabs">
           
            <div class="row">
                <div class="nav-pills">
                 <div class="col-md-2 dns-icon active"><a href="#health-history" data-toggle="tab"><i class="fa fa-heart"></i> My Health History</a></div>
                    <div class="col-md-2 dns-icon"><a href="#providers" data-toggle="tab"><i class="fa fa-user-md"></i> My Providers</a></div>
                    <div class="col-md-2 dns-icon"><a href="#history" data-toggle="tab"><i class="fa fa-calendar"></i> Consultation History</a></div>
                    <div class="col-md-2 dns-icon"><a href="#records" data-toggle="tab"><i class="fa fa-calendar-plus-o"></i> My Records</a></div>
                </div>
            </div>
          
        </section>
        <div class="tab-content">
         <div class="row haelth-section clearfix tab-pane fade in active" role="tabpanel" id="health-history">
            <div class="hs-list myhealth">
            	<div class="hs-l-main">
                	<div class="hs-l-icon"><i class="fa fa-heartbeat"></i></div>
                    <div class="hs-l-heading">My Health Conditions</div>
                    <div class="hs-l-questions">Do you have any health conditions?</div>
                    <div class="hs-l-questions-checkbox">
                    	<div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default " id="healthconditions-yes">Yes
                                <input type="radio" name="option" id="Yes" autocomplete="off" chacked>
                            </label>
                    
                            <label class="btn btn-default active" id="healthconditions-no">No
                                <input type="radio" name="option" id="No" autocomplete="off">
                            </label>
                    	</div>
                    </div>
                 </div>   
                <div class="hs-l-questions-txt desc" id="healthconditions-div"  style="display: none;">
                	<div class="more-info select-yes" >
                      <div class="row table-container my-conditions my">
                        <div class="table" id="health_conditions_table">
                          <div class="table-row tbl-header">
                            <div class="table-cell">My Conditions</div>
                            <div class="table-cell">Reported Date</div>
                            <div class="table-cell">Source</div>
                            <div class="table-cell"></div>
                          </div>
                            <div class="table-row no-records">
                                <div class="table-cell">No medical conditions reported</div>
                                <div class="table-cell"></div>
                                <div class="table-cell"></div>
                                <div class="table-cell delete"></div>
                            </div>
                
                        </div>
                      </div>
                      <div class="add-condition">
                        <div class="clearfix">
                         <div class="col-md-10 row"> <input class="form-control input" id="healthCondition" placeholder="Start typing to search for health conditions (Example: Diabetes)" type="text"></div>
                         <div class="col-md-2"> <button type="button" id="add_health_condition" class="btn btn-info btn-block pull-right">Add Condition</button></div>
                          <br>
                        </div>
                        <div class="message health_message">Condition name not found. Check spelling or click Add.</div>
                      </div>
                    </div>
                
                
                </div>
                
            </div>
            
            
            
            <div class="hs-list myhealth">
            	<div class="hs-l-main">
                	<div class="hs-l-icon"><i class="fa fa-medkit"></i></div>
                    <div class="hs-l-heading">My Medications</div>
                    <div class="hs-l-questions">Are you currently taking any medication?</div>
                    <div class="hs-l-questions-checkbox">
                    	<div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default " id="medications-yes">Yes
                                <input type="radio" name="options" id="Yes" autocomplete="off" chacked>
                            </label>
                    
                            <label class="btn btn-default active" id="medications-no">No
                                <input type="radio" name="options" id="No" autocomplete="off">
                            </label>
                    	</div>
                    </div>
                 </div>   
                <div class="hs-l-questions-txt"  id="medications-div" style="display: none;">
                	<div class="more-info select-yes">
                      <div class="row table-container my-conditions my">
                        <div class="table" id="health_conditions_table">
                          <div class="table-row tbl-header">
                            <div class="table-cell">My Medications</div>
                            <div class="table-cell">Frequency</div>
                            <div class="table-cell">	Reported Date</div>
                            <div class="table-cell">Source</div>
                          </div>
                            <div class="table-row no-records">
                                <div class="table-cell">No medications reported</div>
                                <div class="table-cell"></div>
                                <div class="table-cell"></div>
                                <div class="table-cell delete"></div>
                           	</div>
                
                        </div>
                      </div>
                      <div class="add-condition">
                        <div class="clearfix">
                         <div class="col-md-10 row"> <input class="form-control input" id="healthCondition" placeholder="Start typing to search for health conditions (Example: Diabetes)" type="text"></div>
                         <div class="col-md-2"> <button type="button" id="add_health_condition" class="btn btn-info btn-block pull-right">Add Condition</button></div>
                          <br>
                        </div>
                        <div class="message health_message">No medications reported</div>
                      </div>
                    </div>
                
                
                </div>
                
            </div>
            
            
            
            <div class="hs-list myhealth">
            	<div class="hs-l-main">
                	<div class="hs-l-icon"><i class="fa fa-stethoscope"></i></div>
                    <div class="hs-l-heading">My Allergies</div>
                    <div class="hs-l-questions">Do you have any Allergies or Drug Sensitivities?</div>
                    <div class="hs-l-questions-checkbox">
                    	<div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default " id="allergies-yes">Yes
                                <input type="radio" name="options" id="Yes" autocomplete="off" chacked>
                            </label>
                    
                            <label class="btn btn-default active" id="allergies-no">No
                                <input type="radio" name="options" id="No" autocomplete="off">
                            </label>
                    	</div>
                    </div>
                 </div>   
                <div class="hs-l-questions-txt" id="allergies-div" style="display: none;">
                	<div class="more-info select-yes">
                      <div class="row table-container my-conditions my">
                        <div class="table" id="health_conditions_table">
                          <div class="table-row tbl-header">
                            <div class="table-cell">
My Allergies</div>
                            <div class="table-cell">Severity</div>
                            <div class="table-cell">Reaction</div>
                            <div class="table-cell">Source</div>
                          </div>
                            <div class="table-row no-records">
                                <div class="table-cell">No allergies reported</div>
                                <div class="table-cell"></div>
                                <div class="table-cell"></div>
                                <div class="table-cell delete"></div>
                            </div>
                
                        </div>
                      </div>
                      <div class="add-condition">
                        <div class="clearfix">
                         <div class="col-md-10 row"> <input class="form-control input" id="healthCondition" placeholder="Start typing to search for health conditions (Example: Diabetes)" type="text"></div>
                         <div class="col-md-2"> <button type="button" id="add_health_condition" class="btn btn-info btn-block pull-right">Add Condition</button></div>
                          <br>
                        </div>
                        <div class="message health_message">Have no allergies or drug sensitivities</div>
                      </div>
                    </div>
                
                
                </div>
                
            </div>
            
            
            
            <div class="hs-list myhealth">
            	<div class="hs-l-main">
                	<div class="hs-l-icon"><i class="fa fa-user-md"></i></div>
                    <div class="hs-l-heading">My Surgeries and Procedures</div>
                    <div class="hs-l-questions">Have you ever had any surgeries or medical procedures?</div>
                    <div class="hs-l-questions-checkbox">
                    	<div class="btn-group health-check" data-toggle="buttons">
                            <label class="btn btn-default " id="surgeries-yes">Yes
                                <input type="radio" name="medical" id="option2" autocomplete="off" value="" >
                            </label>
                    
                            <label class="btn btn-default active" id="surgeries-yes">No
                                <input type="radio" name="medical" id="option2_2" autocomplete="off" value="">
                            </label>
                    	</div>
                    </div>
                 </div>   
                <div class="hs-l-questions-txt"  id="surgeries-div" style="display: none;">
                	
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                        <div class="checkbox">
                        	<label class="surgery-name">
                            <input type="checkbox" name="Ankle" id="Ankle" value="Ankle" class="surgery-part" autocomplete="off">
                            	Ankle
                            </label>
                        </div>
                
                </div>
                
            </div>
            
         </div>
         
         
         <div class="row haelth-section clearfix tab-pane fade" role="tabpanel" id="providers">
         		<div class="dnsm-list-col">
                        	<div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="187" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong>13YEARS</strong></div>
                            </div>
                            
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                	<li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>	
                                    <li>
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div></span>
                                    <br /><small>0.00 Star based on 0 Reviews</small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> Psychology</li>
                                    <li><i class=" fa fa-graduation-cap"></i>Lake Erie College of Osteopathy, 2002</li>
                                    <li><i class=" fa fa-dollar"></i>$40.00</li>
                                    <li><i class=" fa fa-hospital-o"></i>2234 Jackson Ave Ste 2 Seaford, NY 11783</li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
                        <div class="dnsm-list-col">
                        	<div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="187" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong>13YEARS</strong></div>
                            </div>
                            
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                	<li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>	
                                    <li>
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div></span>
                                    <br /><small>0.00 Star based on 0 Reviews</small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> Psychology</li>
                                    <li><i class=" fa fa-graduation-cap"></i>Lake Erie College of Osteopathy, 2002</li>
                                    <li><i class=" fa fa-dollar"></i>$40.00</li>
                                    <li><i class=" fa fa-hospital-o"></i>2234 Jackson Ave Ste 2 Seaford, NY 11783</li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
                        <div class="dnsm-list-col">
                        	<div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="187" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong>13YEARS</strong></div>
                            </div>
                            
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                	<li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>	
                                    <li>
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div></span>
                                    <br /><small>0.00 Star based on 0 Reviews</small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> Psychology</li>
                                    <li><i class=" fa fa-graduation-cap"></i>Lake Erie College of Osteopathy, 2002</li>
                                    <li><i class=" fa fa-dollar"></i>$40.00</li>
                                    <li><i class=" fa fa-hospital-o"></i>2234 Jackson Ave Ste 2 Seaford, NY 11783</li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
                        <div class="dnsm-list-col">
                        	<div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="187" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong>13YEARS</strong></div>
                            </div>
                            
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                	<li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>	
                                    <li>
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div></span>
                                    <br /><small>0.00 Star based on 0 Reviews</small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> Psychology</li>
                                    <li><i class=" fa fa-graduation-cap"></i>Lake Erie College of Osteopathy, 2002</li>
                                    <li><i class=" fa fa-dollar"></i>$40.00</li>
                                    <li><i class=" fa fa-hospital-o"></i>2234 Jackson Ave Ste 2 Seaford, NY 11783</li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
                        <div class="dnsm-list-col">
                        	<div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="187" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong>13YEARS</strong></div>
                            </div>
                            
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                	<li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>	
                                    <li>
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div></span>
                                    <br /><small>0.00 Star based on 0 Reviews</small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> Psychology</li>
                                    <li><i class=" fa fa-graduation-cap"></i>Lake Erie College of Osteopathy, 2002</li>
                                    <li><i class=" fa fa-dollar"></i>$40.00</li>
                                    <li><i class=" fa fa-hospital-o"></i>2234 Jackson Ave Ste 2 Seaford, NY 11783</li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
                        <div class="dnsm-list-col">
                        	<div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="187" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong>13YEARS</strong></div>
                            </div>
                            
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                	<li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>	
                                    <li>
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div></span>
                                    <br /><small>0.00 Star based on 0 Reviews</small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> Psychology</li>
                                    <li><i class=" fa fa-graduation-cap"></i>Lake Erie College of Osteopathy, 2002</li>
                                    <li><i class=" fa fa-dollar"></i>$40.00</li>
                                    <li><i class=" fa fa-hospital-o"></i>2234 Jackson Ave Ste 2 Seaford, NY 11783</li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
         
         
                <div class="hs-list myhealth hidden">
                    <div class="hs-l-main-no-pad">
                        <div class="hs-l-heading">Primary Care Provider</div>
                        <div class="hs-l-questions">MDLIVE can send a report of your consultation to primary care provider for continuity of care.</div>
                        <div class="hs-l-questions-checkbox">
                          	<div class="btn btn-info pull-right" id="provider-btn">Add Provider</div>
                        </div>
                     </div>   
                 </div>
                 <div class="hs-l-questions-txt provider-btn-div hidden" style="display: none;">
                	<div class="more-info select-yes">
                      
                      <div class="add-condition">
                        <div class="clearfix">
                             <div class="col-md-4"> 
                                 <label>First Name:</label>
                                 <input class="form-control input" placeholder="First Name" type="text">
                             </div>
                             <div class="col-md-4"> 
                                 <label>Middle Name:</label>
                                 <input class="form-control input" placeholder="Middle Name" type="text">
                             </div>
                             <div class="col-md-4"> 
                                 <label>Last Name:</label>
                                 <input class="form-control input" placeholder="Last Name" type="text">
                             </div>
                              <div class="col-md-6"> 
                                 <label>Email:</label>
                                 <input class="form-control input" placeholder="Email" type="text">
                             </div>
                              <div class="col-md-6"> 
                                 <label>Practice Name:</label>
                                 <input class="form-control input" placeholder="Practice Name" type="text">
                             </div>
                             
                              <div class="col-md-6"> 
                                 <label>Address:</label>
                                 <input class="form-control input" placeholder="Address" type="text">
                             </div>
                             <div class="col-md-6"> 
                                 <label>Address (Line 2):</label>
                                 <input class="form-control input" placeholder="Address (Line 2)" type="text">
                             </div>
                              <div class="col-md-4"> 
                                 <label>City:</label>
                                 <input class="form-control input" placeholder="City" type="text">
                             </div>
                             <div class="col-md-4"> 
                                 <label>State:</label>
                                 <input class="form-control input" placeholder="State" type="text">
                             </div>
                             <div class="col-md-4"> 
                                 <label>Country:</label>
                                 <input class="form-control input" placeholder="Country" type="text">
                             </div>
                             
                              <div class="col-md-4"> 
                                 <label>Cell:</label>
                                 <input class="form-control input" placeholder="Cell" type="text">
                             </div>
                             <div class="col-md-4"> 
                                 <label>Preferred Phone:</label>
                                 <input class="form-control input" placeholder="Preferred Phone" type="text">
                             </div>
                             <div class="col-md-4"> 
                                 <label>Fax:</label>
                                 <input class="form-control input" placeholder="Fax" type="text">
                             </div>
                             
                             <div class="col-md-2 "> <button type="button" id="add_health_condition" class="btn btn-info btn-block pull-right">Add</button></div>
                         
                        </div>
                       
                      </div>
                    </div>
                
                
                </div>
                 
         </div>
          <div class="row haelth-section clearfix tab-pane fade" role="tabpanel" id="history">
          		<div class="">
                	 <div class="col-md-12 text-center"><i class="fa fa-calendar"></i></div>
                     <div class="col-md-12 text-center">No Previous visit</div>
                    <div class="col-md-12 text-center">You will see your visit history here</div>
                </div>	
          
          </div>
          
          <div class="row haelth-section clearfix tab-pane fade" role="tabpanel" id="records">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Upload files</strong> <small> </small></div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="document-name">Name of Document:</label>
                                <input type="text" class="form-control" id="document-name" placeholder="">
                              </div>
                            <div class="input-group image-preview">
                                <input placeholder="" type="text" class="form-control image-preview-filename" disabled="disabled">
                                <!-- don't give a name === doesn't send on POST/GET --> 
                                <span class="input-group-btn"> 
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;"> <span class="glyphicon glyphicon-remove"></span> Clear </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input"> <span class="glyphicon glyphicon-folder-open"></span> <span class="image-preview-input-title">Browse</span>
                                    <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/>
                                    <!-- rename it --> 
                                </div>
                                <button type="button" class="btn btn-labeled btn-default"> <span class="btn-label"><i class="glyphicon glyphicon-upload"></i> </span>Upload</button>
                                </span> </div>
                            <!-- /input-group image-preview [TO HERE]--> 
                            
                            <br />
                            
                            <!-- Drop Zone -->
                            <div class="upload-drop-zone" id="drop-zone"> Or drag and drop files here </div>
                            <br />
                            <!-- Progress Bar -->
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"> <span class="sr-only">60% Complete</span> </div>
                            </div>
                            <br />
                            <!-- Upload Finished -->
                            <div class="js-upload-finished">
                                <h4>Upload history</h4>
                                <div class="list-group"> <a href="#" class="list-group-item list-group-item-danger"><span class="badge alert-danger pull-right">23-11-2014</span>amended-catalogue-01.xls</a> <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">23-11-2014</span>amended-catalogue-01.xls</a> <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">23-11-2014</span>amended-catalogue-01.xls</a> <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">23-11-2014</span>amended-catalogue.xls</a> </div>
                            </div>
                        </div>
                    </div>
                </div>
          
          </div>
          
        </div> 
          
        
    </div>
</section>

<section class="dashboard-new-section-main patient-dashbaord-section-main clearfix tab-pane fade" role="tabpanel" id="provider">
    <div class="container">
    	
    	<div class="row">
            <div class="col-md-12  clearfix"><br>
                <div class="dnsm-highlights">My  <strong> Providers</strong>
                   
                </div>
            </div>
            <div class="dnsm-list clearfix">
            
            
                        <div class="dnsm-list-col">
                        	<div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="187" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong>13YEARS</strong></div>
                            </div>
                            
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                	<li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>	
                                    <li>
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div></span>
                                    <br /><small>0.00 Star based on 0 Reviews</small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> Psychology</li>
                                    <li><i class=" fa fa-graduation-cap"></i>Lake Erie College of Osteopathy, 2002</li>
                                    <li><i class=" fa fa-dollar"></i>$40.00</li>
                                    <li><i class=" fa fa-hospital-o"></i>2234 Jackson Ave Ste 2 Seaford, NY 11783</li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
                      <div class="dnsm-list-col">
                        	<div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="187" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong>13YEARS</strong></div>
                            </div>
                            
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                	<li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>	
                                    <li>
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div></span>
                                    <br /><small>0.00 Star based on 0 Reviews</small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> Psychology</li>
                                    <li><i class=" fa fa-graduation-cap"></i>Lake Erie College of Osteopathy, 2002</li>
                                    <li><i class=" fa fa-dollar"></i>$40.00</li>
                                    <li><i class=" fa fa-hospital-o"></i>2234 Jackson Ave Ste 2 Seaford, NY 11783</li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
                        <div class="dnsm-list-col">
                        	<div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="187" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong>13YEARS</strong></div>
                            </div>
                            
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                	<li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>	
                                    <li>
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div></span>
                                    <br /><small>0.00 Star based on 0 Reviews</small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> Psychology</li>
                                    <li><i class=" fa fa-graduation-cap"></i>Lake Erie College of Osteopathy, 2002</li>
                                    <li><i class=" fa fa-dollar"></i>$40.00</li>
                                    <li><i class=" fa fa-hospital-o"></i>2234 Jackson Ave Ste 2 Seaford, NY 11783</li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
                        <div class="dnsm-list-col">
                        	<div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="187" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong>13YEARS</strong></div>
                            </div>
                            
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                	<li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>	
                                    <li>
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div></span>
                                    <br /><small>0.00 Star based on 0 Reviews</small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> Psychology</li>
                                    <li><i class=" fa fa-graduation-cap"></i>Lake Erie College of Osteopathy, 2002</li>
                                    <li><i class=" fa fa-dollar"></i>$40.00</li>
                                    <li><i class=" fa fa-hospital-o"></i>2234 Jackson Ave Ste 2 Seaford, NY 11783</li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
                        <div class="dnsm-list-col">
                        	<div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="187" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong>13YEARS</strong></div>
                            </div>
                            
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                	<li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>	
                                    <li>
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div></span>
                                    <br /><small>0.00 Star based on 0 Reviews</small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> Psychology</li>
                                    <li><i class=" fa fa-graduation-cap"></i>Lake Erie College of Osteopathy, 2002</li>
                                    <li><i class=" fa fa-dollar"></i>$40.00</li>
                                    <li><i class=" fa fa-hospital-o"></i>2234 Jackson Ave Ste 2 Seaford, NY 11783</li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
                        <div class="dnsm-list-col">
                        	<div class="cencal-section"><button type="submit" class="btn btn-default dark-pink removeFavoriteModal" data-login="yes" data-id="187" alt="Remove From Favorites" title="Remove From Favorites"><i class="fa fa-remove"></i></button></div>
                            <div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>images/doctors/full/getProviderImage.png);">
                                <span><i class="fa fa-user"></i> Online</span>
                                <div class="experince">EXPERIENCE <br> <strong>13YEARS</strong></div>
                            </div>
                            
                            <div class="dnsm-lc-txt-area">
                                <ul>
                                	<li class="name"><a href="#"><i class="fa fa-user"></i> Dr. Sherri L. Dehaas</a></li>	
                                    <li>
                                        <span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background glyphicon glyphicon-star-empty"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0px;"><span></span></div></div></span>
                                    <br /><small>0.00 Star based on 0 Reviews</small>
                                    </li>
                                    <li><i class=" fa fa-stethoscope"></i> Psychology</li>
                                    <li><i class=" fa fa-graduation-cap"></i>Lake Erie College of Osteopathy, 2002</li>
                                    <li><i class=" fa fa-dollar"></i>$40.00</li>
                                    <li><i class=" fa fa-hospital-o"></i>2234 Jackson Ave Ste 2 Seaford, NY 11783</li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-play"></i> Consult Now</a></div>
                            <div class="dnsm-lc-btn"> <a href="#" class="btn-block"><i class="fa fa-calendar"></i> Book Appointment</a></div>
                        </div>
              		
               </div>
               
            </div>
        </div>
    </div>
</section>

</div>

<script>
$('.nav-pills div a').click(function (e) {
  $('.nav-pills div.active').removeClass('active')
  $(this).parent('div').addClass('active')
});

	
$('#provider-btn').click(function() {
	$('.provider-btn-div').css('display','block');	
});

$('#healthconditions-yes').click(function() {
	$('#healthconditions-div').css('display','block');	
});
$('#healthconditions-no').click(function() {
	$('#healthconditions-div').css('display','none');	
});

$('#medications-yes').click(function() {
	$('#medications-div').css('display','block');	
});
$('#healthconditions-no').click(function() {
	$('#medications-div').css('display','none');	
});

$('#allergies-yes').click(function() {
	$('#allergies-div').css('display','block');	
});
$('#allergies-no').click(function() {
	$('#allergies-div').css('display','none');	
});

$('#surgeries-yes').click(function() {
	$('#surgeries-div').css('display','block');	
});
$('#surgeries-no').click(function() {
	$('#surgeries-div').css('display','none');	
});


$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'top'
    });   
/* end dot nav */
});

</script><script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "102755ae-bbdb-48be-8649-21943945ea02", doNotHash: true, doNotCopy: false, hashAddressBar: false});</script>