<section class="dashboard-new-section clearfix">
    <div class="container">
        <div class="row">
        	<div class="nav-pills">
        	 <div class="col-md-2 dns-icon active"><a href="#dasboard" data-toggle="tab"> <i class="fa fa-list-alt"></i> Dashboard</a></div>
                <div class="col-md-2 dns-icon"><a href="#appointment" data-toggle="tab"> <i class="fa fa-clock-o"></i> Appoitment <span>7</span></a></div>
                <div class="col-md-2 dns-icon"><a href="#vtab1" data-toggle="tab"> <i class="fa fa-comments"></i> Message <span>10</span></a></div>
                <div class="col-md-2 dns-icon"><a href="#vtab1" data-toggle="tab"> <i class="fa fa-gears"></i> Account Settings</a></div>
                <div class="col-md-2 dns-icon"><a href="#vtab1" data-toggle="tab"> <i class="fa fa-clipboard"></i> Blog</a></div>
          	</div>
        </div>
    </div>
</section>
<div class="tab-content">
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="dasboard">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><div class="dnsm-highlights">You have <strong>7</strong> patients in the <strong>WAITING ROOM</strong></div></div>
            <div class="dnsm-list">
            	<div class="dnsm-list-col">
                	<div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>/img/admin-img.jpg);">
                    	<span><i class="fa fa-user"></i> Online</span>
                    </div>
                    <div class="dnsm-lc-txt-area">
                    	<ul>
                        	<li class="name"><i class="fa fa-user"></i> Johnny Depp</li>
                        	<li class="date"><i class="fa fa-calendar"></i> 12th May, 2016  <i class="fa fa-clock-o"></i> 10:30 PM</li>
                            <li class="prsm"><i class="fa fa-clipboard"></i> Headache</li>
                        </ul>
                        <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                    </div>
                    <div class="dnsm-lc-btn"> <a href="#">Start Appoitment</a></div>
                </div>
                <div class="dnsm-list-col">
                	<div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>/img/admin-img.jpg);">
                    	<span><i class="fa fa-user"></i> Online</span>
                    </div>
                    <div class="dnsm-lc-txt-area">
                    	<ul>
                        	<li class="name"><i class="fa fa-user"></i> Johnny Depp</li>
                        	<li class="date"><i class="fa fa-calendar"></i> 12th May, 2016  <i class="fa fa-clock-o"></i> 10:30 PM</li>
                            <li class="prsm"><i class="fa fa-clipboard"></i> Headache</li>
                        </ul>
                        <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                    </div>
                    <div class="dnsm-lc-btn"> <a href="#">Start Appoitment</a></div>
                </div>
                <div class="dnsm-list-col">
                	<div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>/img/admin-img.jpg);">
                    	<span><i class="fa fa-user"></i> Online</span>
                    </div>
                    <div class="dnsm-lc-txt-area">
                    	<ul>
                        	<li class="name"><i class="fa fa-user"></i> Johnny Depp</li>
                        	<li class="date"><i class="fa fa-calendar"></i> 12th May, 2016  <i class="fa fa-clock-o"></i> 10:30 PM</li>
                            <li class="prsm"><i class="fa fa-clipboard"></i> Headache</li>
                        </ul>
                        <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                    </div>
                    <div class="dnsm-lc-btn"> <a href="#">Start Appoitment</a></div>
                </div>
                <div class="dnsm-list-col">
                	<div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>/img/admin-img.jpg);">
                    	<span><i class="fa fa-user"></i> Online</span>
                    </div>
                    <div class="dnsm-lc-txt-area">
                    	<ul>
                        	<li class="name"><i class="fa fa-user"></i> Johnny Depp</li>
                        	<li class="date"><i class="fa fa-calendar"></i> 12th May, 2016  <i class="fa fa-clock-o"></i> 10:30 PM</li>
                            <li class="prsm"><i class="fa fa-clipboard"></i> Headache</li>
                        </ul>
                        <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                    </div>
                    <div class="dnsm-lc-btn"> <a href="#">Start Appoitment</a></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="dashboard-new-section-main clearfix tab-pane fade in active" role="tabpanel" id="appointment">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><div class="dnsm-highlights">Upcoming  <strong>Appointments</strong></div></div>
            <div class="dnsm-list">
            	<div class="dnsm-list-col">
                	<div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>/img/admin-img.jpg);">
                    	<span><i class="fa fa-user"></i> Online</span>
                    </div>
                    <div class="dnsm-lc-txt-area">
                    	<ul>
                        	<li class="name"><i class="fa fa-user"></i> Johnny Depp</li>
                        	<li class="date"><i class="fa fa-calendar"></i> 12th May, 2016  <i class="fa fa-clock-o"></i> 10:30 PM</li>
                            <li class="prsm"><i class="fa fa-clipboard"></i> Headache</li>
                        </ul>
                        <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                    </div>
                    <div class="dnsm-lc-btn"> <a href="#">Start Appoitment</a></div>
                </div>
                <div class="dnsm-list-col">
                	<div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>/img/admin-img.jpg);">
                    	<span><i class="fa fa-user"></i> Online</span>
                    </div>
                    <div class="dnsm-lc-txt-area">
                    	<ul>
                        	<li class="name"><i class="fa fa-user"></i> Johnny Depp</li>
                        	<li class="date"><i class="fa fa-calendar"></i> 12th May, 2016  <i class="fa fa-clock-o"></i> 10:30 PM</li>
                            <li class="prsm"><i class="fa fa-clipboard"></i> Headache</li>
                        </ul>
                        <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                    </div>
                    <div class="dnsm-lc-btn"> <a href="#">Start Appoitment</a></div>
                </div>
                <div class="dnsm-list-col">
                	<div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>/img/admin-img.jpg);">
                    	<span><i class="fa fa-user"></i> Online</span>
                    </div>
                    <div class="dnsm-lc-txt-area">
                    	<ul>
                        	<li class="name"><i class="fa fa-user"></i> Johnny Depp</li>
                        	<li class="date"><i class="fa fa-calendar"></i> 12th May, 2016  <i class="fa fa-clock-o"></i> 10:30 PM</li>
                            <li class="prsm"><i class="fa fa-clipboard"></i> Headache</li>
                        </ul>
                        <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                    </div>
                    <div class="dnsm-lc-btn"> <a href="#">Start Appoitment</a></div>
                </div>
                <div class="dnsm-list-col">
                	<div class="dnsm-lc-image" style="background:url(<?php echo HTTP_SERVER; ?>/img/admin-img.jpg);">
                    	<span><i class="fa fa-user"></i> Online</span>
                    </div>
                    <div class="dnsm-lc-txt-area">
                    	<ul>
                        	<li class="name"><i class="fa fa-user"></i> Johnny Depp</li>
                        	<li class="date"><i class="fa fa-calendar"></i> 12th May, 2016  <i class="fa fa-clock-o"></i> 10:30 PM</li>
                            <li class="prsm"><i class="fa fa-clipboard"></i> Headache</li>
                        </ul>
                        <p>My name is Dr. DeHaas, and I'm here to provide you with great medical care.. <a href="#">Read More</a></p>
                    </div>
                    <div class="dnsm-lc-btn"> <a href="#">Start Appoitment</a></div>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="dashboard-new-section-main  pad-top-0 clearfix">
    <div class="container">
        <div class="row">
        	<div class="col-md-7">
            	<div class="dnsm-highlights">Appointment<strong> Statistics </strong></div>
            </div>
            
            <div class="col-md-5">
            	<div class="dnsm-highlights">Dr.’s Myvirtualdoctor for<strong> This Week</strong></div>
                <div class="dnsm-revenue-section">
                	<div class="dnsm-rv-sec-list">
                    	<div class="dnsm-rv-sec-lst-icon"> 
                        	<i class="fa fa-bar-chart"></i> <span><i class="fa fa-usd"></i></span>
                        </div>
                        <div class="dnsm-rv-sec-lst-txt">
                        	$1000 Revenue Generated 
                        </div>
                     </div>
                     <div class="dnsm-rv-sec-list">
                    	<div class="dnsm-rv-sec-lst-icon">
                        	<i class="fa fa-clock-o"></i>
                        </div>
                        <div class="dnsm-rv-sec-lst-txt">
                        	24 Appointments This Month
                        </div>
                     </div>
                     <div class="dnsm-rv-sec-list">
                    	<div class="dnsm-rv-sec-lst-icon">
                        	<i class="fa fa-refresh"></i>
                        </div>
                        <div class="dnsm-rv-sec-lst-txt">
                        	15 Returning Patients
                        </div>
                     </div>
                     <div class="dnsm-rv-sec-list">
                    	<div class="dnsm-rv-sec-lst-icon">
                        	<i class="fa fa-user"></i> <span><i class="fa fa-check"></i></span>
                        </div>
                        <div class="dnsm-rv-sec-lst-txt">
                        	9 New Patients
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

<script>
$('.nav-pills div a').click(function (e) {
  $('.nav-pills div.active').removeClass('active')
  $(this).parent('div').addClass('active')
})
</script>