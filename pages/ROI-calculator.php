<script type="text/javascript">
      	function calculateROI(){
	        var numofproviders = $('#numofproviders').val();
	        var expectedvisits = $('#expectedvisits').val();
			var averagecharge = $('#averagecharge').val();
			
			if(numofproviders == "" || expectedvisits == "" || averagecharge == ""){
				
				if(numofproviders == ""){$('#numofproviders').focus();return false;}
				if(expectedvisits == ""){$('#expectedvisits').focus();return false;}
				if(averagecharge == ""){$('#averagecharge').focus();return false;}
				
				return false;
			}else{
				
				var numofproviders = parseFloat($('#numofproviders').val());
	        	var expectedvisits = parseFloat($('#expectedvisits').val());
				var averagecharge = parseFloat($('#averagecharge').val());
				
				var result = numofproviders * expectedvisits * averagecharge * 260;	
				$('#result').text("$"+result.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
				//toogle element
				$("#collapseExample").hide();
				$("#collapseExample").toggle('slow');
			}
			
	        
	        //$('#result').text(result.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
		}
      	
		function resetField(fieldId){
			var fieldValue = $('#'+fieldId).val();
			if(isNaN(fieldValue)){
				$('#'+fieldId).val("");
				$('#'+fieldId).focus();
			}
		}
		
      	
      </script>


 <div class="calculator-main" style="    background: #ffffff !important;">
         <div class="container">
            <h1>My Virtual Doctor is simplifying telemedicine </h1>
            <p>Find out how much you can increase the revenue of your practice by implementing our turnkey telemedicine solution.</p>
            <div class="calculator-box-main">
               <div class="calculator-box">
                  <h6>Number of Providers</h6>
                  <p><input class="form-control ng-pristine ng-valid" id="numofproviders" type="text" name="numofproviders" required onkeyup="resetField('numofproviders')"></p>
               </div>
               <div class="calculator-box">
                  <h6>Expected number of telemedicine visits per day, per provider</h6>
                  <p><input class="form-control ng-pristine ng-valid" id="expectedvisits" type="text" name="expectedvisits" required onkeyup="resetField('expectedvisits')"></p>
               </div>
               <div class="calculator-box">
                  <h6>Average charge for telemedicine visits<br><span>If you're not sure, use $75 </span></h6>
                  <p><input  class="form-control ng-pristine ng-valid" id="averagecharge" type="text" name="averagecharge" required onkeyup="resetField('averagecharge')" value="75"></p>
                 
               </div>
               <p>
                  <button id="my_button" class="btn btn-primary" type="button" onclick="return calculateROI();">
                  Calculate Revenue
                  </button>
               </p>
               <div class="collapse" id="collapseExample">
                  <div class="card card-block">
                     <p>By adding My Virtual Doctor to your practice, you'd increase revenue by </p>
                     <h1><span id="result"></span><span>/year</span></h1>
                     <a class="green-outline-button text-center" href="<?php echo HTTP_SERVER; ?>app/register/doctor">Get Started</a>
                  </div>
               </div>
               <div id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                  </div>
               </div>
            </div>
         </div>
      </div>
