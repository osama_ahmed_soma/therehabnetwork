<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

$id = intval($_GET['id']);
$tab = 'overview';
$error_msg = "";
$success_msg = "";

if ( isset( $_POST['patient_review'], $_SESSION["mvdoctorVisitornUserId"] ) && $_POST['patient_review'] === '1' ) {
    $tab = 'ratings';
    $user_id = $_SESSION["mvdoctorVisitornUserId"];

    //insert into
    $sql = "INSERT INTO `" . DB_PREFIX . 'ratings` (ratingFieldsId, userId, doctorId, ratings, added) VALUES ';

    $cnt = count($_POST['rating']);
    $total_rating = 0;
    $idx = 1;
    $added = time();
    $arr = array();

    foreach ( $_POST['rating'] as $ratingFieldsId => $ratings ) {
        $total_rating += $ratings;
        $sql .= '(?, ?, ?, ?, ?)';
        if ( $idx < $cnt ) {
            $sql .= ', ';
        }
        $idx++;
        $arr[] = $ratingFieldsId;
        $arr[] = $user_id;
        $arr[] = $id;
        $arr[] = $ratings;
        $arr[] = $added;
    }

    try {
        $st1 = $db->prepare($sql);
        $st1->execute($arr);
    } catch (Exception $Exception) {
        die("DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }

    //now insert real review
    $title = isset($_POST['title']) ? stripslashes($_POST['title']) : '';
    $description = isset($_POST['description']) ? stripslashes($_POST['description']) : '';
    $avg_rating = $total_rating / $cnt;

    $sql = "INSERT INTO `" . DB_PREFIX . 'reviews` (`userId`, `doctorId`, `title`, `description`, `rating`, `added`)
    VALUES (?, ?, ?, ?, ?, ?)';

    try {
        $st2 = $db->prepare($sql);
        $st2->execute( array( $user_id, $id, $title, $description, $avg_rating, $added ) );
    } catch (Exception $Exception) {
        die("DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ));
    }

    //recalculate global rating index for this user, this will fasten later queries
    //get average rating
    $sql = 'SELECT count(*) as cnt, sum(`rating`) as tot FROM `' . DB_PREFIX .  'reviews` WHERE `doctorId` = ? AND `status` = 1';

    try {
        $st = $db->prepare($sql);
        $st->execute(array($id));

        if ( $st->rowCount() > 0 ) {
            $avg_rat_data = $st->fetch();
            if ( $avg_rat_data['cnt'] > 0 ) {
                $rating_data['ratings'] = $avg_rat_data['tot'] / $avg_rat_data['cnt'];
                $rating_data['ratingCount'] = $avg_rat_data['cnt'];
            } else {
                $rating_data['ratings'] = 0;
                $rating_data['ratingCount'] = 0;
            }

            //now update table
            $result = fastInsertUpdate( 'update', DB_PREFIX . 'doctors', $rating_data, array(), array('id' => $id) );

            if ( isset($result['status']) && $result['status'] == 'success' ) {
                $success_msg = "Review Added Successfully.";
            } else {
                echoPre($result);
                die;
            }
        }

    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }
}

//get doctor data
try {
    $query = "SELECT doc.*, cat.categoryImage, cat.categoryName, cat.categoryDesc FROM `" . DB_PREFIX . "doctors` as doc, `" . DB_PREFIX . "category` as cat, `" . DB_PREFIX . "category_data` as cat_data
    WHERE cat.id = cat_data.categoryId AND doc.id = cat_data.postId AND doc.id = ? AND cat.categoryType='company' LIMIT 1";
    $result = $db->prepare($query);
    $result->execute( array($id) );

    if ( $result->rowCount() > 0 ) {
        $doctor_data = $result->fetch();
        $doctor_data['work_places'] = unserialize(base64_decode($doctor_data['work_places']));
        $doctor_data['procedure_attempted'] = unserialize(base64_decode($doctor_data['procedure_attempted']));
        $doctor_data['conditions_treated'] = unserialize(base64_decode($doctor_data['conditions_treated']));
        $doctor_data['awards'] = unserialize(base64_decode($doctor_data['awards']));
        $doctor_data['education'] = unserialize(base64_decode($doctor_data['education']));

        if ( $doctor_data['ratings'] < 1 ) {
            $doctor_data['ratings'] = 0;
        }

        //$doctor_data['ratingCount'] = $doctor_data['ratingCount'] . '.00';
    }

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}

//get category data
try {
    $query = "SELECT ct.categoryName, ct.categoryType FROM `" . DB_PREFIX . "category` as ct, `" . DB_PREFIX . "category_data` as ct_data  WHERE ct.categoryType in ('speciality', 'location', 'company', 'education', 'language') AND ct.categoryStatus = 1 AND ct_data.postId = ? AND ct_data.postType='doctors' AND ct.id = ct_data.categoryId order by ct.categoryOrder asc";
    $result = $db->prepare($query);
    $result->execute( array($id) );

    if ( $result->rowCount() > 0 ) {
        foreach ( $result->fetchAll() as $category_data ) {
            if ( $category_data['categoryType'] === 'speciality' ) {
                $speciality_data[] = $category_data;
            } elseif ( $category_data['categoryType'] === 'location' ) {
                $location_data[] = $category_data;
            } elseif ( $category_data['categoryType'] === 'company' ) {
                $company_data[] = $category_data;
            } elseif ( $category_data['categoryType'] === 'education' ) {
                $education_data[] = $category_data;
            }
            elseif ( $category_data['categoryType'] === 'language' ) {
                $language_data[] = $category_data;
            }
        }
    }

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}

//get individual fields ratings
$sql = 'SELECT sum(rat.ratings) as f1, count(rat.`ratingFieldsId`) as f2, fld.fieldName as f3 FROM `' .DB_PREFIX . 'ratings` as rat, `' . DB_PREFIX . 'rating_fields` as fld WHERE fld.id = rat.`ratingFieldsId` AND rat.`doctorId` = ? group by rat.`ratingFieldsId` ';
$individual_rating_fields = array();
$individual_rating_fields2 = array();
try {
    $st = $db->prepare($sql);
    $st->execute(array($id));

    if ( $st->rowCount() > 0 ) {
        $individual_rating_fields = $st->fetchAll();
    }

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}

if ( empty($individual_rating_fields) ) {
    $sql = "SELECT `fieldName` FROM `" .DB_PREFIX . "rating_fields` WHERE `fieldStatus`=1 ORDER BY fieldOrder asc";
    try {
        $st = $db->prepare($sql);
        $st->execute();
        if ( $st->rowCount() > 0 ) {
            $individual_rating_fields2 = $st->fetchAll();
        }
    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }
}

//get user reviews
$sql = 'SELECT rv.*, usr.`userFname`, usr.`userLname`, usr.`userEmail` FROM `'. DB_PREFIX .'reviews` as rv, `'. DB_PREFIX .'users` as usr  WHERE usr.`userId` = rv.userId AND rv.doctorId = ? ORDER BY rv.rating desc LIMIT 10';

try {
    $st = $db->prepare( $sql );
    $st->execute(array( $id ));
    if ( $st->rowCount() > 0 ) {
        $user_reviews = $st->fetchAll();
    } else {
        $user_reviews = array();
    }



} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}

//check user gave review or not
$user_gave_review = false;
if ( isset($_SESSION["mvdoctorVisitornUserId"])  ) {
    $sql = 'SELECT userId  FROM `'. DB_PREFIX .'reviews` WHERE `userId` = ? AND doctorId = ? LIMIT 1';

    try {
        $st = $db->prepare( $sql );
        $st->execute( array( $_SESSION["mvdoctorVisitornUserId"], $id ) );

        if ( $st->rowCount() > 0 ) {
            $user_gave_review = true;
        }

    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }
}

//check for favorite doctor
/*
 * 0 - user not logged in
 * 1 - user is logged in but not added to favorite
 * 2 - user added this doctor to favorite
 */
$is_doctor_favorite = 0;
if ( isset($_SESSION["mvdoctorVisitornUserId"])  ) {
    $sql = 'SELECT userId  FROM `'. DB_PREFIX .'favorite_doctor` WHERE `userId` = ? AND doctorId = ? LIMIT 1';

    try {
        $st = $db->prepare( $sql );
        $st->execute( array( $_SESSION["mvdoctorVisitornUserId"], $id ) );

        if ( $st->rowCount() > 0 ) {
            $is_doctor_favorite = 2;
        } else {
            $is_doctor_favorite = 1;
        }

    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }
}

?>
<?php require(SITE_ROOT.'pages/modules/doctor/head.php'); //get head section ?>

<section class="clearfix profile-detail">
    <div class="container">
        <div class="row">
            <?php require(SITE_ROOT.'pages/modules/doctor/content.php'); //get content section ?>
            <?php require(SITE_ROOT.'pages/modules/doctor/sidebar.php'); //get sidebar section ?>
        </div>
    </div>
</section>