<?php

function sanitize ($input, $condition = 1){
 if($condition == 1){
  return htmlentities($input, ENT_QUOTES, 'UTF-8');
 }elseif ($condition == 2){
  //Allow quotes for database entry
  $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
     '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
     '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
     '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
  ); 
  $input = preg_replace($search, '', $input); 
  return htmlentities(strip_tags($input));
 }
}

	$name = sanitize($_POST['InputName']);
	$email = sanitize($_POST['InputEmail']);
	$message = sanitize($_POST['InputMessage']);
	$real = sanitize($_POST['InputReal']);
	 $ip_address = $_SERVER['REMOTE_ADDR'];
	 $to = 'info@myvirtualdoctor.com';
	 $subject = 'My Virtual Doctor Contact Form';
 $message = '
 <html>
 <head>
  <title>My Virtual Doctor Contact Form</title>
 </head>
 <body>
  <p>Your Name: '.$name.',</p>
  <p>Email Address: '.$email.',</p>
  <p>Massage: '.$message.',</p>
  <p>IP Address: '.$ip_address.'</p>
 </body> 
 </html>
 ';
 
 // ------------------------------------------------------------
 // SET HEADERS FOR HTML MAIL
 // ------------------------------------------------------------
 $headers  = 'MIME-Version: 1.0' . "\r\n";
 $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
 //$headers .= 'To: <'.$txbEmail.'>' . "\r\n";
 $headers .= 'From: My Virtual Doctor Contact Form <'.$email.'>' . "\r\n";
 //$headers .= 'Cc: anothermail@foo.org' . "\r\n";
 //$headers .= 'Bcc: '.$from_address.'' . "\r\n";
 
 // ------------------------------------------------------------
 // SEND E-MAIL
 // ------------------------------------------------------------
 $email_form = mail($to, $subject, $message, $headers);
if ($email_form){
	header('Location:contact-us.php?status=success');
}
else
{
 $msg = "Not send";
}
?>