<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}

//get category data
$location_data = array();

try {
	$query = "SELECT id, categoryName, categoryType, categoryFeatured FROM " . DB_PREFIX . "category WHERE `categoryType` = 'location' AND categoryStatus = 1 order by `categoryOrder` asc";
	$result = $db->prepare($query);
	$result->execute();

	if ( $result->rowCount() > 0 ) {
		$location_data = $result->fetchAll();
	}

} catch (Exception $Exception) {
	exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}
?>

<section id="banner" style="background:url(<?php echo HTTP_SERVER; ?>/img/banner.png) center center;">
	<div class="container">
		<div class="row">
			<div class="col-md-10 text-center col-md-offset-1">
				<div class="paragrph f20 regular">
                	<h1>Get The Best Personalized Care</h1>
					<?php /*?>We match patient needs and preferences to physician expertise and experience, helping you find and choose the best virtual doctors in America. It’s personalized online healthcare the way it should be.<?php */?>
                    Find highly educated and experienced online doctors based on your preferences

				</div>
				<div class="col-md-12  surch-main">
					<form method="get" action="<?php echo HTTP_SERVER ?>search.html">
						<div class="col-md-5 col-sm-5 col-xs-12 row surch-right">
							<div class="input-group hm-search"><span class="input-group-addon"><i class="fa  fa-user-md"></i></span>
								<select name="location" data-placeholder="Select a state" class="chosen-select form-control paitent-dropdown" tabindex="5">
									<option value=""></option>
									<?php if ( !empty( $location_data )): ?>
										<?php
										foreach ( $location_data as $item ) {
											echo "<option value='{$item['id']}'>{$item['categoryName']}</option>";
										}
										?>
									<?php endif; ?>
								</select>
							</div>
						</div>

						<div class="col-md-5 col-sm-5 col-xs-12 row surch-right">
							<div class="input-group hm-search"><span class="input-group-addon"><i class="fa fa-user-md"></i></span>
								<input type="text" class="chosen-single chosen-default form-control" name="searchName" autocomplete="off" tabindex="5" placeholder="Physicians Name">
							</div>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12 row hm-search">
							<button type="submit" class="btn btn-primary">Search</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>


<section  class="step-choose-section">
	<div class="container">
		<div class="row">
        	<div class="col-md-12 text-center practice-heading clearfix">
            	<h1>3 Key Ways We Help Patients</h1>
            </div>
        	<div class="col-md-4 col-sm-4 col-xs-12 step-main">
            	<div class="col-md-12 text-center">
                	<div class="step-icon"><i  class="fa fa-user-plus"></i></div>
                    <div class="step-heading">Free Signup</div>
                </div>
                <div class="col-md-12 step-detail">
                	<div class="col-lg-1  col-md-2"><i class="fa fa-check-circle hidden-xs"></i></div>
                	<div class="col-lg-11">Create a free patient account to access your personal telehealth hub where receiving care has never been simpler or more convenient.</div>
                </div>          
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 step-main">
            	<div class="col-md-12 text-center">
                	<div class="step-icon"><i  class="fa fa-users"></i></div>
                    <div class="step-heading">User Friendly Interface</div>
                </div>
                <div class="col-md-12 step-detail">
                	<div class="col-lg-1 col-md-2"><i class="fa fa-check-circle hidden-xs"></i></div>
                	<div class="col-lg-11">Manage your healthcare with user-friendly, intuitive features that let you schedule appointments, save your favorite doctors, pin your favorite blog posts, and more.</div>
                </div>          
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 step-main">
            	<div class="col-md-12 text-center">
                	<div class="step-icon"><i  class="fa fa-bell"></i></div>
                    <div class="step-heading">Health Reminders</div>
                </div>
                <div class="col-md-12 step-detail">
                	<div class="col-lg-1 col-md-2"><i class="fa fa-check-circle hidden-xs"></i></div>
                	<div class="col-lg-11">Receive real-time alerts and healthcare notifications to ensure you don’t forget important doctor appointments during life’s inevitable disruptions. </div>
                </div>          
            </div>
            
        </div>
        <div class="col-md-12 signup-btn">
            	<button type="button" class="btn btn-info sing" data-toggle="modal" data-target="#myModal">Sign Up / Log In</button>
            </div>
    </div>    
</section>


<section  class="step-practice-section">
	<div class="container">
		<div class="row">
        	<div class="col-md-12 text-center practice-heading clearfix">
            	<h1>A Comprehensive Telehealth Platform That Will Improve Your Practice's Financial & Operational Efficiency</h1>
            </div>
            <div class="col-md-11 practice-sec clearfix">
                <div class="col-lg-5 col-md-6 col-sm-6 practice-moniter">
                	<img src="<?php echo HTTP_SERVER; ?>/img/moniter.png" alt="" style="margin-top: 18px;"  />
                </div>
                <div class="col-lg-1 arrows-connect hidden visible-lg">
                	<img src="<?php echo HTTP_SERVER; ?>/img/arrows-connect.png" alt="" />
                </div>
                <div class="col-lg-6 pull-right  col-md-6 col-sm-12  col-xs-12">	
                    <div class="col-md-12 col-sm-4 steps">
                        <div class="col-md-3 text-center">
                            <div class="step-icon"><i  class="fa fa-thumbs-o-up"></i></div>
                            
                        </div>
                        <div class="col-md-9 step-detail">
                            <div class="col-md-12 step-heading">Easy to Use</div>
                            <div class="col-md-12 step-list"><li><i class="fa fa-check-circle"></i> Systems agnostic-PC, laptop, tablet, or smartphone</li> <li><i class="fa fa-check-circle"></i>  One Step EMR data integration</div>
                        </div>         
                    </div>
                    <div class="col-md-12 col-sm-4 steps">
                        <div class="col-md-3 text-center">
                            <div class="step-icon"><i  class="fa fa-line-chart"></i></div>
                           
                        </div>
                        <div class="col-md-9 step-detail">
                             <div class="col-md-12 step-heading">Efficient</div>
                            <div class="col-md-12 step-list"><li><i class="fa fa-check-circle"></i>  Offers scheduled appointments and on demand consults </li> <li><i class="fa fa-check-circle"></i>  E-Prescribing with single sign on</div>
                        </div>        
                    </div>
                    <div class="col-md-12 col-sm-4 steps">
                        <div class="col-md-3 text-center">
                            <div class="step-icon"><i  class="fa fa-shield"></i></div>
                        </div>
                        <div class="col-md-9 step-detail">
                            <div class="col-md-12 step-heading">Secure</div>
                            <div class="col-md-12 step-list"><li><i class="fa fa-check-circle"></i>  HIPAA and HITECH Compliant </li><li><i class="fa fa-check-circle"></i>  Encrypted Communications 24/7</li></div>
                        </div>        
                    </div>
                 </div>    
                
            </div>
            <div class="col-md-12 signup-btn clearfix">
                    <a href="<?php echo HTTP_SERVER; ?>our-platform.html" class="btn btn-info sing">Learn More</a>
                    <a href="<?php echo HTTP_SERVER; ?>request-a-demo.html" class="btn btn-info sing" style="left: 10px;position: relative;">Request a demo</a><br/><br/>
                  <div class="col-md-12 text-center"> Already have an account?  <a href="<?php echo HTTP_SERVER; ?>/doctor-login.html" >Sign In Here</a></div>
                </div>
                 
        </div>
    </div>    
</section>

