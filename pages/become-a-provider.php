<script type="text/javascript">
      	function calculateROI(){
	        var numofproviders = $('#numofproviders').val();
	        var expectedvisits = $('#expectedvisits').val();
			var averagecharge = $('#averagecharge').val();
			
			if(numofproviders == "" || expectedvisits == "" || averagecharge == ""){
				
				if(numofproviders == ""){$('#numofproviders').focus();return false;}
				if(expectedvisits == ""){$('#expectedvisits').focus();return false;}
				if(averagecharge == ""){$('#averagecharge').focus();return false;}
				
				return false;
			}else{
				
				var numofproviders = parseFloat($('#numofproviders').val());
	        	var expectedvisits = parseFloat($('#expectedvisits').val());
				var averagecharge = parseFloat($('#averagecharge').val());
				
				var result = numofproviders * expectedvisits * averagecharge * 260;	
				$('#result').text("$"+result.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
				//toogle element
				$("#collapseExample").hide();
				$("#collapseExample").toggle('slow');
			}
			
	        
	        //$('#result').text(result.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
		}
      	
		function resetField(fieldId){
			var fieldValue = $('#'+fieldId).val();
			if(isNaN(fieldValue)){
				$('#'+fieldId).val("");
				$('#'+fieldId).focus();
			}
		}
		
      	
      </script>


<section class="grey clearfix" id="service" style="background: #f8f8f8;">

    <div class="container">

        <div class="row">

            <div class="col-md-12 text-center">

                <h1 class=" Virtual-heading">Become a Provider</h1>

            </div>

            <div class="col-md-12">

                <div class="col-md-6  col-md-offset-3 text-center">

                    <h2 class="simple pra">Discover just how simple the switch to telemedicine can be.  </h2>

                </div>



            </div>





        </div>

    </div>

</section>





<section class="become-provider-main " >

    <div class="container">

        <h1>Get Started with a Risk-Free 90-Day Trial</h1>

       <div class="become-provider clearfix">
	   
	   
        <div class="become-provider-left col-md-8 row clearfix"  >
		 
		  <div class="col-md-6">
		<input type="text" class="form-control" name="InputName" id="InputName" placeholder="First Name" required="">
		</div>
		 <div class="col-md-6">
		<input type="text" class="form-control" name="InputName" id="InputName" placeholder="Last Name" required="">
		</div>
		
		
		<div class="col-md-6">
		<div class="dropdown">
		<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Professional Title
		<span class="caret"></span></button>
		<ul class="dropdown-menu">
		<li><a href="#">Professional Title</a></li>
		<li><a href="#">Professional Title</a></li>
		<li><a href="#">Professional Title</a></li>
		</ul>
		</div>
		</div>
  
		<div class="col-md-6">
		<div class="dropdown">
		<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Primary Specialty
		<span class="caret"></span></button>
		<ul class="dropdown-menu">
		<li><a href="#">Primary Specialty</a></li>
		<li><a href="#">Primary Specialty</a></li>
		<li><a href="#">Primary Specialty</a></li>
		</ul>
		</div>
		</div>
		

		<div class="col-md-12 Board-Certified-main">
		<div class="col-md-6 pull-left  row"><h4>Board-Certified: </h4></div>
		
		
		<div class="col-md-6 Board-Certified-but pull-right">
		<button type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">No</button>
		<button type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">Yes</button>
		</div>
		</div>
		<hr>
		
		 <div class="col-md-12" >
		<input type="text" class="form-control" name="InputName" id="InputName" placeholder="Years in Practice" required="" style="    width: 689px;">
		</div>
		
		<div class="col-md-6">
		<input type="text" class="form-control" name="InputName" id="InputName" placeholder="Email" required="">
		</div>
		
		<div class="col-md-6">
		<input type="text" class="form-control" name="InputName" id="InputName" placeholder="Preferred Phone Number" required="">
		</div>
		
		
		<div class="col-md-6">
		<div class="dropdown">
		<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Primary State License
		<span class="caret"></span></button>
		<ul class="dropdown-menu">
		<li><a href="#">Primary Specialty</a></li>
		<li><a href="#">Primary Specialty</a></li>
		<li><a href="#">Primary Specialty</a></li>
		</ul>
		</div>
		</div>
		
		
		<div class="col-md-6">
		<input type="text" class="form-control" name="InputName" id="InputName" placeholder="Other Licenses" required="">
		</div>
		
		<div class="col-md-6">
		<div class="dropdown">
		<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Languages Spoken
		<span class="caret"></span></button>
		<ul class="dropdown-menu">
		<li><a href="#">Primary Specialty</a></li>
		<li><a href="#">Primary Specialty</a></li>
		<li><a href="#">Primary Specialty</a></li>
		</ul>
		</div>
		</div>
		
		
		<div class="col-md-6">
		<input type="text" class="form-control" name="InputName" id="InputName" placeholder="Other Languages" required="">
		</div>
		
		<hr>
		
		<div class="col-md-6 Submit">
           <button type="submit" name="submit" id="submit" value="Submit" class="btn btn-info">Submit</button>
		</div>
		
		
		
		
        </div>
		
		
		
		
		
		
		

        <div class="become-provider-right col-md-4" >
		
		
		<div class="inquire-phone col-md-12" >
		<i class="fa fa-phone"></i>
		<h5>Inquire by Phone</h6>
		<h2>1-855-80- DOCTOR</h2>
		<hr>
		<img src="img/inquire-phone.png" class="inqurie-img">
		
		
		 </div>
		

        </div>



          </div>
  
  
         </div>
  
</section>









