<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}

//get category data
$location_data = array();

try {
	$query = "SELECT id, categoryName, categoryType, categoryFeatured FROM " . DB_PREFIX . "category WHERE `categoryType` = 'location' AND categoryStatus = 1 order by `categoryOrder` asc";
	$result = $db->prepare($query);
	$result->execute();

	if ( $result->rowCount() > 0 ) {
		$location_data = $result->fetchAll();
	}

} catch (Exception $Exception) {
	exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}
?>


<style>
.row{
    margin-top:0px;
    padding: 0 0px;
}

.clickable{
    cursor: pointer;   
}

.panel-heading span {
	margin-top: -20px;
	font-size: 15px;
}
.faqs-hea-top {
              margin-left: 29px;
    margin-bottom: 0;
    margin-top: 19px;
    height: 2px;
}
.faqs-hea-top h1 {
        color: #c11d5f;
    padding: 9px 10px 20px;
    text-align: center;
}
.panel-primary {
    border-color: #e7f4f7;
}
.panel-primary > .panel-heading {
    color: #107184;
        background-color: rgba(54, 165, 188, 0.09);
    border-color: #e7f4f7;
}
.panel-title {
    margin-top: 0;
    margin-bottom: 0;
    font-size: 20px;
    color: inherit;
}
.panel-body {
    padding: 15px;
    background: #ffffff;
    font-size: 16px;
}



.panel-body li {
    float: left;
    width: 34%;
}

.panel-body p {
    clear: both;
    
}
.panel-body ul {
    clear: both;
    padding-top: 16px;
}
.panel-body ul:after {
    content: "";
    display: block;
    clear: both;
}
.panel-primary > .panel-heading {
   
    border-color: #36a5bc;
}
.panel {
    margin-bottom: 5px;
    
}
.panel-heading {
    padding: 18px 30px;
   
}

.faqs {
    margin-bottom: 30px;
}
</style>





<section class="grey clearfix" id="service" style="background: #f8f8f8;">

    <div class="container">

        <div class="row">

            <div class="col-md-12 text-center">

                <h1 class=" Virtual-heading">Frequently Asked Questions</h1>

            </div>

            <div class="col-md-12">

                <div class="col-md-6  col-md-offset-3 text-center">

                    <h2 class="simple pra">For any questions not answered below, please feel free to contact us.  </h2>

                </div>



            </div>





        </div>

    </div>

</section>



<div class="faqs">
<div class="container">
      <div class="faqs-hea-top">
      
	  
	  
	  </div>
<div class="container">
    <div class="">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">What is My Virtual Doctor? </h3>
					<span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
				<div class="panel-body" style="display: none;">My Virtual Doctor is a telehealth platform that gives you access to board-certified medical professionals via live video, chat, and phone. Our doctors are able to diagnose your symptoms, prescribe treatment, and electronically send prescriptions directly to your preferred pharmacy. Obtaining care from the comfort of your own home, at work, or on the go has never been easier or more convenient. </div>
			</div>
		</div>
		
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">When are doctors available?  </h3>
					<span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
				<div class="panel-body" style="display: none;">  Medical assistance is available 24/7, 365 days of the year. Whether you’re unable to wait for an in-person consultation with your primary care provider, prefer the convenience of online consults, or require a cost-effective alternative to urgent care and emergency room visits for non-emergency issues, one of our on-call doctors can help. Alternatively, you can schedule an appointment with your preferred provider.  </div>
			</div>
		</div>

		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Where is My Virtual Doctor available?  </h3>
					<span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
				<div class="panel-body" style="display: none;"> My Virtual Doctor is available across the United States, so you can easily find a licensed physician in your area. Simply use the search functionality on our homepage or on your personalized patient dashboard.   </div>
			</div>
		</div>
		
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">When can I start using My Virtual Doctor?  </h3>
					<span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
				<div class="panel-body" style="display: none;">  As soon as you sign up for a free account, you can access your patient dashboard, fill in your details, and start browsing physician profiles and their available appointment times. When you find a healthcare provider who meets your needs, simply schedule an appointment for a time that suits you.  </div>
			</div>
		</div>
		
		
		
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Can I use My Virtual Doctor for medical emergencies? </h3>
					<span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
				<div class="panel-body" style="display: none;">No. My Virtual Doctor is designed to accommodate the diagnosis and treatment of non-emergency medical conditions. In cases where symptoms are life threatening or require advanced medical attention, you should dial 911 or visit your nearest emergency room. Syncope (fainting), physical trauma, bleeding, chest pain, stroke symptoms, unstable vitals, and certain mental illnesses are just a few instances that require urgent care.
                 <p>With that said, our primary healthcare practitioners and specialists can diagnose and treat a massive range of conditions via remote video-chat. These conditions include (but are not limited to):</p>
                  <ul>
				    <li>	Abrasions</li>
					<li>	Acne</li>
					<li>	Allergies</li>
					<li>	Arthritic pain</li>
					<li>	Asthma</li>
					<li>	Bacterial vaginosis</li>
					<li>	Bladder infections</li>
					<li>	Bronchitis</li>
					<li>	Bruises</li>
					<li>	Cellulitis</li>
					<li>	Colds and flu</li>
					<li>	Conjunctivitis</li>
					<li>	Coughs</li>
					<li>	Dehydration</li>
					<li>	Diarrhea</li>
					<li>	Earache</li>
					<li>	Fever</li>
					<li>	Frostbite</li>
					<li>	Headaches and migraines</li>
					<li>	Hives</li>
					<li>	Infections</li>
					<li>	Insect bites and stings</li>
					<li>	Insomnia</li>
					<li>	Lice</li>
					<li>	Mild lacerations or burns</li>
					<li>	Nausea</li>
					<li>	Pharyngitis</li>
					<li>	Pink eye</li>
					<li>	Rashes</li>
					<li>	Respiratory infections</li>
					<li>	Sexually transmitted infections (STIs)</li>
					<li>	Sinus symptoms</li>
					<li>	Skin inflammations</li>
					<li>	Sore throats</li>
					<li>	Sports injuries</li>
					<li>	Sprains</li>
					<li>	Urinary tract infections (UTIs)</li>
					<li>	Vomiting</li>
					<li>	Yeast infections</li>
					<li></li>
					
				  
				  </ul>

				</div>
			</div>
		</div>
		
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">What can I expect during an online visit?  </h3>
					<span class="pull-right clickable  panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
				<div class="panel-body" style="display: none;"> Just like an in-person visit, most virtual doctor visits last around 15 minutes. While you may be in a different location during a virtual consult, your provider will still be able to review your medical history, discuss your symptoms, perform certain tests, provide a diagnosis, recommend a treatment plan, and prescribe medications. 

  </div>
			</div>
		</div>
		
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">How much does an online visit cost? </h3>
					<span class="pull-right clickable  panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
				<div class="panel-body" style="display: none;">Fees differ from provider to provider, but most visits cost about the same as a copay for in-person visits. You can view applicable prices when viewing a physician’s or specialist’s profile.    </div>
			</div>
		</div>
		
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">How will I be billed and is my visit covered by health insurance?  </h3>
					<span class="pull-right clickable  panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
				<div class="panel-body" style="display: none;"> The billing department of your provider’s office will handle all invoicing. Many employer and health plans do cover telehealth, but you’ll need to check with your provider before booking an appointment. If your telehealth consult is covered, your doctor’s office will adjust the bill accordingly.   </div>
			</div>
		</div>
		
		
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">What payment methods do you accept?  </h3>
					<span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
				<div class="panel-body"style="display: none;"> We accept most major credit cards and debit cards, including VISA, American express, MasterCard, and Discover.   </div>
			</div>
		</div>
		
		
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Is my health information safe and secure?  </h3>
					<span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
				<div class="panel-body" style="display: none;">Yes. Protecting your privacy is of the utmost importance to us. My Virtual Doctor’s platform is HIPAA compliant and data encrypted to ensure the secure storage of your personal health information at all times. We employ stringent security policies and protocols so that you can use our platform with complete confidence and peace of mind.    </div>
			</div>
		</div>
		
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">What are your equipment and software requirements?  </h3>
					<span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
				<div class="panel-body" style="display: none;">To use My Virtual Doctor, you’ll need:  

                 <ul> 
				    <li>	Windows 7, XP, or Vista</li>
					 <li>	A Mac running OS X 10.6 or a later version</li>
					  <li>	A webcam (1.3 megapixels or higher)</li>
					   <li>	A microphone</li>
					    <li>	High-speed Internet connectivity</li>
					
				 </ul>
                 <p>Once your account is set up, you can use our online simulation to test and configure your settings for virtual consults. </p>
                    



				</div>
			</div>
		</div>
		
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Do I need to download an app for my mobile device?  </h3>
					<span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
				<div class="panel-body" style="display: none;"> No. Although we will launch an app in the future, our website and services are optimized for mobile so that you can consult with a doctor on a device that suits you.   </div>
			</div>
		</div>
		
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">What if I need help or have a question? </h3>
					<span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
				<div class="panel-body" style="display: none;">You can email our friendly support staff at <a href="#"/>support@myvirtualdoctor.com</a>. We endeavor to respond to all queries and concerns promptly.    </div>
			</div>
		</div>
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		
	</div>
	
</div>
		

	</div>
</div>		




<script>



$(document).on('click', '.panel-heading span.clickable', function(e){
    var $this = $(this);
	if(!$this.hasClass('panel-collapsed')) {
		$this.parents('.panel').find('.panel-body').slideUp();
		$this.addClass('panel-collapsed');
		$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
	} else {
		$this.parents('.panel').find('.panel-body').slideDown();
		$this.removeClass('panel-collapsed');
		$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
	}
})



</script>






