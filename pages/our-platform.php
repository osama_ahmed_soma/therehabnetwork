<section class="grey clearfix" id="service"  style="background:url(img/back.png);">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class=" Virtual-heading">Connecting Patients to Better Quality Healthcare</h1>
            </div>
            <div class="col-md-12">
                <div class="col-md-6  col-md-offset-3 text-center">
                    <h2 class="simple pra">Through Accessibility. Through Freedom of Choice. Through Empathy.  </h2>
                </div>

            </div>


        </div>
    </div>
</section>

<section class="Finding our-platform clearfix">
    <div class="container">
        <div class="row">
        	<div class="col-md-8">
                <h2>Interactive 2-Way Consults</h2>
                <p>My Virtual Doctor’s powerful turnkey telemedicine solution lets you find operational and financial efficiencies while extending your ability to reach patients through secure one-on-one HD video streaming, audio, and text message consultations. Our cutting-edge technology is not only fast, convenient, and easy to use, but it also offers you and your patients a seamless healthcare experience and optimal flexibility. It’s ideal for creating valuable patient engagement online and delivering high-quality healthcare to patients who need it most. The software is integration-ready and designed to scale with a growing practice.</p>
			</div>
            <div class="col-md-4 text-center">
           		<img class="img-with-animation" data-delay="0" height="100%" data-animation="fade-in" src="<?php echo HTTP_SERVER; ?>/img/image_01.jpg" alt="" style="opacity: 1;">
       		 </div>
        </div>  
    </div>
</section>
<section class="Finding our-platform grey clearfix">
    <div class="container">
        <div class="row">
        	 <div class="col-md-4 text-center">
           		<img class="img-with-animation" data-delay="0" height="100%"data-animation="fade-in" src="<?php echo HTTP_SERVER; ?>/img/image_03.png" alt="" style="opacity: 1;">
       		 </div>
        	<div class="col-md-8">
                <h2>Highest Level of HIPAA Compliance and Data Encryption</h2>
                <p>Protecting a patient’s privacy and limiting liability are top priorities for any physician. Consultations via My Virtual Doctor’s platform are fully HIPAA and HITECH compliant while using a quarantined data encrypted to ensure the highest level of security at all times. We employ comprehensive and stringent security policies, procedures, and protocols to keep your information safe so that you can confidently use our platform with utmost peace of mind. </p>

			</div>
           
        </div>  
    </div>
</section>
<section class="Finding our-platform clearfix">
    <div class="container">
        <div class="row">
        	<div class="col-md-8">
                <h2>Valuable Telemedicine Directory Listings</h2>
                <p>Our software enables you to build a detailed physician profile that showcases both your education and experience while enhancing your online presence and creating brand awareness. My Virtual Doctor’s directory listing casts a wider net in search results, effectively attracting targeted traffic to your profile and generating new business for you and your practice. In addition, the platform allows for review submissions by verified patients only, thereby boosting your credibility and protecting your reputation.</p>
			</div>
            <div class="col-md-4 text-center">
           		<img class="img-with-animation" data-delay="0" data-animation="fade-in" src="<?php echo HTTP_SERVER; ?>/img/image_04.png" alt="" style="opacity: 1;">
       		 </div>
        </div>  
    </div>
</section>
<section class="Finding our-platform grey clearfix">
    <div class="container">
        <div class="row">
        	 <div class="col-md-4 text-center">
           		<img class="img-with-animation" data-delay="0" height="100%" data-animation="fade-in" src="<?php echo HTTP_SERVER; ?>/img/image_02.png" alt="" style="opacity: 1;">
       		 </div>
        	<div class="col-md-8">
                <h2>Modern and Dynamic Healthcare Delivery</h2>
                <p>My Virtual Doctor’s software helps you achieve and maintain a competitive advantage by helping you reach and engage increasingly tech-savvy patients, while still accommodating patients not up to date on the latest technologies. Our easy to use platform can be accessed from anywhere and at any time using a multitude of Internet-enabled devices including smartphones, laptops, tablets, and desktop computers. This effectively reduces cancellations and no-shows while increasing retention rates. We do all the work so you can focus on what you do best: providing the best healthcare possible to your patient population.</p>

			</div>
           
        </div>  
    </div>
</section>
<section class="Finding our-platform clearfix">
    <div class="container">
        <div class="row">
        	<div class="col-md-8">
                <h2>Advanced Administrative Backend</h2>
                <p>Specifically designed for healthcare providers, My Virtual Doctor’s administrative application strategically integrates with standard medical administrative practices and the systems you already have in place. From consultation scheduling and payment processing to reporting and analytics, our telemedicine platform allows you and your administrative personnel to manage your responsibilities easily and proficiently.</p>
			</div>
            <div class="col-md-4 text-center">
           		<img class="img-with-animation" data-delay="0" height="100%"  data-animation="fade-in" src="<?php echo HTTP_SERVER; ?>/img/image_05.png" alt="" style="opacity: 1;">
       		 </div>
        </div>  
    </div>
</section>
<section class="Finding text-center grey clearfix">
    <div class="container">
        <div class="row">
        	 <div class="col-md-12">
                <h1 class="text-center">MORE CORE FEATURES</h1>
                <div class="col-md-3">
                	<div class="icon-of-ourplatform">
                    	<i class="fa fa-check"></i>
                    </div>
                	<h3>Claims System</h3>
                    <p>An integrated claims system allows for digital adjudication to support payers.</p>
                </div>
                 <div class="col-md-3">
                 	<div class="icon-of-ourplatform">
                    	<i class="fa fa-check"></i>
                    </div>
                	<h3>Medical History</h3>
                    <p>Direct EMR system integration via API </p>
                </div>
                 <div class="col-md-3">
                 	<div class="icon-of-ourplatform">
                    	<i class="fa fa-check"></i>
                    </div>
                	<h3>Consent to Treat</h3>
                    <p>A HIPAA compliant authorization process giving a physician the permission to provide care.</p>
                </div>
                 <div class="col-md-3">
                 	<div class="icon-of-ourplatform">
                    	<i class="fa fa-check"></i>
                    </div>
                	<h3>Pharmacy Data</h3>
                    <p>Import of PBM data allows providers to view up-to-date medication history.</p>
                </div>

			</div>
           
        </div>  
    </div>
</section>

<section class="grey clearfix" id="footer" style="background:url(http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/img/footer.png);">
    <div class="container">
		<div class="col-md-12">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h1 class=" Virtual-heading">Are you ready to market your practice more effectively?</h1>
                </div>
                <div class="col-md-12">
                   <div class="col-md-8  col-md-offset-2 text-center request-form">
                   		<h2 class="simple pra">Sign Up For  A Free Demo Today </h2> <br />
                    	<form role="form" id="demo-form" action="#" method="post">
                         <div id="form_message"></div>
                            <input type="hidden" name="token" value="373353344136304f3341345049334249304458324e50514a574443554f4e4b">
                            <input type="text" placeholder="First Name" id="InputNameFirst" name="InputNameFirst" value="" class="form-control" required="">
                            <input type="text" placeholder="Last Name" id="InputNameLast" name="InputNameLast" value="" class="form-control" required="">
                            <input type="text" placeholder="Email" id="InputEmail" name="InputEmail" value="" class="form-control" required="">
                            <input type="text" placeholder="Phone" id="InputPhone" name="InputPhone" value="" class="form-control" required="">
                             <!--<input type="text" placeholder="Clinic" id="InputClinic" name="InputClinic" value="" class="form-control" required=""> -->
                           <!-- <input type="submit" value="REQUEST DEMO"  name="submit" id="submit" class="btn btn-info request-btn" /> -->
                             <button type="submit" name="submit" id="submit" value="REQUEST DEMO" class="btn btn-info pull-right request-btn">REQUEST DEMO</button>
                            
                        </form>
                    </div>
                
                    
    
                </div>
            </div>
		</div>
	</div>
</section>	