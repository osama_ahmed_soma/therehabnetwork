<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}

?>

<section  class="privacypolicy-page">
	<div class="container">
		<div class="row">
          <div class="privacypolicy-main">
		  
		    <h1>My Virtual Doctor Terms of Use</h1>
		  
           <p><b>1</b>.	Use of the Site. My Virtual Doctor, LLC. (“My Virtual Doctor”, “we”, “us”, or “our”) operates the website located at<a href="<?php echo HTTP_SERVER; ?>"> <?php echo HTTP_SERVER; ?></a> and other related websites and mobile applications with links to these Terms of Use (collectively, the “Site”). We offer online telehealth services (the “Services”) enabling our members (“Members”) to report their health history and engage healthcare professionals (“Treating Providers”) to obtain medical and healthcare services (“Services”). By accessing and using the Site, you agree to be bound by these Terms of Use and all other terms and policies that appear on the Site. If you do not wish to be bound by any of these Terms of Use, you may not use the Site or the Services.</p>
		   <p><b>2</b>.	Healthcare Services. All of the Treating Providers who deliver Services through My Virtual Doctor are independent professionals solely responsible for the services each provides to you. My Virtual Doctor does not practice medicine or any other licensed profession, and does not interfere with the practice of medicine or any other licensed profession by Treating Providers, each of whom is responsible for his or her services and compliance with the requirements applicable to his or her profession and license. Neither My Virtual Doctor nor any third parties who promote the Services or provide you with a link to the Service shall be liable for any professional advice you obtain from a Treating Provider via the Services.</p>
		   <p><b>3</b>.	Site Content. None of the Site content (other than information you receive from Treating Providers) should be considered medical advice or an endorsement, representation or warranty that any particular medication or treatment is safe, appropriate, or effective for you.</p>
		   <p><b>4</b>.	Informed Consent.</p>
		   <p><b>A</b>. Telehealth is the delivery of health care services using interactive audio and video technology, where the patient and the health care professional are not in the same physical location. During your telehealth consultation with a Treating Provider, details of your health history and personal health information may be discussed with you through the use of interactive video, audio and other telecommunications technology, and your Treating Provider may perform a physical examination through these technologies.</p>
		   <p><b>B</b>. The telehealth Services you receive from Treating Providers are not intended to replace a primary care physician relationship or be your permanent medical home. You may form an ongoing treatment relationship with some Treating Providers. However, your initial visit with a Treating Provider will begin as a consultation (e.g. to determine the most appropriate treatment setting for you to receive care) and will not necessarily give rise to an ongoing treatment relationship. You should seek emergency help or follow-up care when recommended by a Treating Provider or when otherwise needed, and continue to consult with your primary care physician and other healthcare professionals as recommended. We may make arrangements for follow up care either through My Virtual Doctor or other healthcare providers. You will have direct access to customer support services to follow up on medication reactions, side effects or other adverse events. Among the benefits of our Services are improved access to healthcare professionals and convenience. However, as with any health service, there are potential risks associated with the use of telehealth. These risks include, but may not be limited to:</p>
		   
			<ul>
				<li>.	In rare cases, information transmitted may not be sufficient (e.g. poor resolution of images) to allow for appropriate health care decision making by the Treating Provider;</li>
				<li>.	Delays in evaluation or treatment could occur due to failures of the electronic equipment. If this happens, you may be contacted by phone or other means of communication.</li>
				<li>.	In rare cases, a lack of access to all of your health records may result in adverse drug interactions or allergic reactions or other judgment errors;</li>
				<li>.	Although the electronic systems we use will incorporate network and software security protocols to protect the privacy and security of health information, in rare instances, security protocols could fail, causing a breach of privacy of personal health information</li>
				
			</ul>
			
		   <p><b>C</b>. By accepting these Terms of Use, you acknowledge that you understand and agree with the following:</p>
		   
<ul>
	<li>.	You understand that you may expect the anticipated benefits from the use of telehealth in your care, but that no results can be guaranteed or assured.</li>
	<li>.	You understand that the laws that protect the privacy and security of health information apply to telehealth, and have received My Virtual Doctor's Notice of Privacy Practices, which describes these protections in more detail. Electronic communications are directed to your Treating Provider(s) and their supervisees through a secure, encrypted video interface and electronic health record.</li>
	<li>.	If your health insurance coverage does not include or is not sufficient to satisfy the Services charges in full, you may be fully or partially responsible for payment.</li>
	<li>.	Your Treating Provider may determine that the Services are not appropriate for some or all of your treatment needs, and accordingly may elect not to provide telehealth services to you through the Site.</li>
	<li>.	With respect to psychotherapy, you are entitled to receive information from your Treating Provider about the methods of therapy, the techniques used, the duration of your therapy (if known), and the fee structure. You can seek a second opinion from another therapist or terminate therapy at any time.</li>
</ul>
		   
		   
		   <p><b>D</b>. With respect to psychotherapy, if you and your Treating Provider decide to engage in group or couples therapy (collectively “Group Therapy”), you understand that information discussed in Group Therapy is for therapeutic purposes and is not intended for use in any legal proceedings involving Group Therapy participants. You agree not to subpoena the Treating Provider to testify for or against other Group Therapy participants or provide records in court actions against other Group Therapy participants. You understand that anything any Group Therapy participant tells the Treating Provider individually, whether on the phone or otherwise, may at the therapist’s discretion be shared with the other Group Therapy participants. You agree to share responsibility with the Treating Provider for the therapy process, including goal setting and termination.</p>
		   <p><b>E</b>.. You can send messages to your Treating Provider by contacting My Virtual Doctor Customer Support at 1-855-80-DOCTOR. Emails or electronic messages to Customer Support or your Treating Provider may not be returned immediately. If you are experiencing a medical emergency, you should call 911 or go to the nearest emergency room.</p>
		   <p><b>F</b>.. You may also be able to send written notes to therapists or other Treating Providers or to Customer Support using a secure messaging portal within the Site (the “Messaging Portal”). Messages you send over the Messaging Portal may be viewed by more than one Treating Provider or Customer Support agent and will typically be answered within 48 hours. You should not rely on the Messaging Portal if you need immediate attention from a Treating Provider or other health care professional. If you are experiencing a medical emergency, you should call 911 or go to the nearest emergency room. You understand and agree that messaging with Treating Providers over the Messaging Portal does not give rise to a provider-patient relationship, and does not by itself constitute treatment, diagnosis, therapy, or medical advice.</p>
		   <p><b>5</b>..	Privacy. My Virtual Doctor is required to comply with the federal health care privacy and security laws and maintain safeguards to protect the security of your health information. Additionally, the information you provide to your Treating Provider during a medical consultation or therapy session is legally confidential, except for certain legal exceptions as more fully described in our Notice of Privacy Practices. We devote considerable effort toward ensuring that your personal information is secure. Information regarding our use of health and other personal information is provided in our Site Privacy Policy and health information Notice of Privacy Practices. As part of providing you the Services, we may need to provide you with certain communications, such as appointment reminders, service announcements and administrative messages. These communications are considered part of the Services and your Account. While secure electronic messaging is always preferred to insecure email, under certain circumstances, insecure email communication containing personal health information may take place between you and  My Virtual Doctor.  My Virtual Doctor cannot ensure the security or confidentiality of messages sent by email. Information relating to your care, including clinical notes and medical records, are stored on secure, encrypted servers maintained by My Virtual Doctor.</p>
		   <p><b>6</b>..	User Accounts. When you register on the Site, you are required to create an account (“Account”) by entering your name, email address, password and certain other information collected by My Virtual Doctor (collectively “Account Information”). To create an Account, you must be of legal age to form a binding contract. If you are not of legal age to form a binding contract, you may not register to use our Services. You agree that the Account Information that you provide to us at all times, including during registration and in any information you upload to the Site will be true, accurate, current, and complete. You may not transfer or share your Account password with anyone, or create more than one Account (with the exception of subaccounts established for children of whom you are the parent or legal guardian). You are responsible for maintaining the confidentiality of your Account password and for all activities that occur under your Account. My Virtual Doctor reserves the right to take any and all action, as it deems necessary or reasonable, regarding the security of the Site and your Account Information. In no event and under no circumstances shall My Virtual Doctor be held liable to you for any liabilities or damages resulting from or arising out of your use of the Site, your use of the Account Information or your release of the Account Information to a third party. You may not use anyone else's account at any time.</p>
		   <p><b>7</b>..	Use of the Services by Children. The Services are available for use by children, but the Member for all patients under the age of 18 must be the patient’s parent or legal guardian. If you register as the parent or legal guardian on behalf of a minor, you will be fully responsible for complying with these Terms of Use.</p>
		   <p><b>8</b>..	Access Rights. We hereby grant to you a limited, non-exclusive, nontransferable right to access the Site and use the Services solely for your personal non-commercial use and only as permitted under these Terms of Use and any separate agreements you may have entered into with us (“Access Rights”). We reserve the right, in our sole discretion, to deny or suspend use of the Site or Services to anyone for any reason. You agree that you will not, and will not attempt to: (a) impersonate any person or entity, or otherwise misrepresent your affiliation with a person or entity; (b) use the Site or Services to violate any local, state, national or international law; (c) reverse engineer, disassemble, decompile, or translate any software or other components of the Site or Services; (d) distribute viruses or other harmful computer code through the Site or (e) otherwise use the Services or Site in any manner that exceeds the scope of use granted above. In addition, you agree to refrain from abusive language and behavior which could be regarded as inappropriate, or conduct that is unlawful or illegal, when communicating with Treating Providers through the Site and to refrain from contacting Treating Providers for telehealth services outside of the Site. My Virtual Doctor is not responsible for any interactions with Treating Providers that are not conducted through the Site. We strongly recommend that you do not use the Services on public computers. We also recommend that you do not store your Account password through your web browser or other software.</p>
		   <p><b>9</b>..	Fees and Purchase Terms. You agree to pay all fees or charges to your Account in accordance with the fees, charges, and billing terms in effect at the time a fee or charge is due and payable. By providing My Virtual Doctor with your credit card number and associated payment information, you agree that My Virtual Doctor is authorized to immediately invoice your account for all fees and charges due and payable to My Virtual Doctor hereunder and that no additional notice or consent is required. If your health plan, employer or agency has arranged with My Virtual Doctor to pay the fee or any portion of the fee, or if the fee is pursuant to some other arrangement with My Virtual Doctor, that fee adjustment will be reflected in the fee that you are ultimately charged. Please check with your employer, health plan or agency to determine if any Services will be reimbursed.</p>
		   <p>If you do not have insurance coverage for Services, or if your coverage is denied, you acknowledge and agree that you shall be personally responsible for all incurred expenses. My Virtual Doctor offers no guarantee that you shall receive any such reimbursement. My Virtual Doctor reserves the right to modify or implement a new pricing structure at any time prior to billing you for your initial payment or for future payments due pursuant to these Terms of Use. You understand and agree that for Services provided on an appointment basis, you will be responsible for a missed appointment fee equal to the fees you and your insurer or other payor would have paid for the scheduled services if you do not cancel a scheduled appointment at least 24 hours in advance.</p>
		   <p><b>10</b>..	Website Links. WE WILL NOT BE LIABLE FOR ANY INFORMATION, SOFTWARE, OR LINKS FOUND AT ANY OTHER WEBSITE, INTERNET LOCATION, OR SOURCE OF INFORMATION, NOR FOR YOUR USE OF SUCH INFORMATION, SOFTWARE OR LINKS, NOR FOR THE ACTS OR OMISSIONS OF ANY SUCH WEBSITES OR THEIR RESPECTIVE OPERATORS.</p>
		   <p><b>11</b>..	Ownership. The Site and its entire contents, features and functionality (including but not limited to all information, software, text, displays, images, video and audio, and the design, selection and arrangement thereof), are owned by the Company, its licensors or other providers of such material and are protected by United States and international copyright,trademark, patent, trade secret and other intellectual property or proprietary rights laws. These Terms of Use permit you to use the Site for your personal, non-commercial use only. You must not reproduce, distribute, modify, create derivative works of, publicly display, publicly perform, republish, download, store or transmit any of the material on our Site except as generally and ordinarily permitted through the Site according to these Terms of Use. You must not access or use for any commercial purposes any part of the Site or any services or materials available through the Site. </p>
		   <p><b>12</b>..	Trademarks. Certain of the names, logos, and other materials displayed on the Site or in the Services may constitute trademarks, trade names, service marks or logos ("Marks") of My Virtual Doctor or other entities. You are not authorized to use any such Marks without the express written permission of My Virtual Doctor. Ownership of all such Marks and the goodwill associated therewith remains with us or those other entities.</p>
		   <p><b>13</b>..	Termination. You may deactivate your Account and end your registration at any time, for any reason by sending an email to <a href="#">  info@myvirtualdoctor.com </a>.  My Virtual Doctor may suspend or terminate your use of the Site, your Account and/or registration for any reason at any time. Subject to applicable law, My Virtual Doctor reserves the right to maintain, delete or destroy all communications and materials posted or uploaded to the Site pursuant to its internal record retention and/or content destruction policies. After such termination, My Virtual Doctor will have no further obligation to provide the Services, except to the extent we are obligated to provide you access to your health records or Treating Providers are required to provide you with continuing care under their applicable legal, ethical and professional obligations to you.</p>
		   <p><b>14</b>..	Right to modify. We may at our sole discretion change, add, or delete portions of these Terms of Use at any time on a going-forward basis. Continued use of the Site and/or Services following notice of any such changes will indicate your acknowledgement of such changes and agreement to be bound by the revised Terms of Use, inclusive of such changes</p>
		   <p><b>15</b>..	DISCLAIMER OF WARRANTIES. YOU EXPRESSLY AGREE THAT USE OF THE SITE OR SERVICES IS AT YOUR SOLE RISK. BOTH THE SITE AND SERVICES ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS. MY VIRTUAL DOCTOR EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE, NON-INFRINGEMENT, TITLE, OPERABILITY, CONDITION, QUIET ENJOYMENT, VALUE, ACCURACY OF DATA AND SYSTEM INTEGRATION.</p>
		   <p>You acknowledge and agree that My Virtual Doctor does not provide medical advice, diagnosis, or treatment, and is strictly a technology platform and infrastructure for connecting patients with independent third party Treating Providers, including physicians and other Treating Providers in the My Virtual Doctor network. You acknowledges and agree that the Treating Providers using the Site are solely responsible for and will have complete authority, responsibility, supervision, and control over the provision of all medical services, advice, instructions, treatment decisions, and other professional health care services performed, and that all diagnoses, treatments, procedures, and other professional health care services will be provided and performed exclusively by or under the supervision of Treating Providers, in their sole discretion, as they deem appropriate.</p>
		   <p><b>16</b>..	LIMITATION OF LIABILITY. YOU UNDERSTAND THAT TO THE EXTENT PERMITTED UNDER APPLICABLE LAW, IN NO EVENT WILL MY VIRTUAL DOCTOR OR ITS OFFICERS, EMPLOYEES, DIRECTORS, PARENTS, SUBSIDIARIES, AFFILIATES, AGENTS OR LICENSORS BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF REVENUES, PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES ARISING OUT OF OR RELATED TO YOUR USE OF THE SITE OR THE SERVICES, REGARDLESS OF WHETHER SUCH DAMAGES ARE BASED ON CONTRACT, TORT (INCLUDING NEGLIGENCE AND STRICT LIABILITY), WARRANTY, STATUTE OR OTHERWISE. To the extent that we may not, as a matter of applicable law, disclaim any implied warranty or limit its liabilities, the scope and duration of such warranty and the extent of our liability will be the minimum permitted under such applicable law.</p>
		   <p><b>17</b>..	Indemnification. You agree to indemnify, defend and hold harmless My Virtual Doctor, its officers, directors, employees, agents, subsidiaries, affiliates, licensors, and suppliers, harmless from and against any claim, actions, demands, liabilities and settlements, including without limitation reasonable legal and accounting fees (“Claims”), resulting from, or alleged to result from, your violation of these terms and conditions. In addition, you agree to indemnify, defend and hold harmless your Treating Provider(s) from and against any third party Claims resulting from your lack of adherence with the advice or recommendation(s) of such Treating Provider.</p>
		   <p><b>18</b>..	Geographical Restrictions. My Virtual Doctor makes no representation that all products, services and/or material described on the Site, or the Services available through the Site, are appropriate or available for use in locations outside the United States or all territories within the United States.</p>
		   <p><b>19</b>..	Disclosures.</p>
		   <p>All physicians and psychologists on the Site hold professional licenses issued by the professional licensing boards in the states where they practice, hold doctoral degrees in either medicine or psychology and have undergone post-doctoral training. You can report a complaint relating to services provided by a Treating Provider by contacting the professional licensing board in the state where the services were received. In a professional relationship, sexual intimacy is never appropriate and should be reported to the board that licenses, registers, or certifies the licensee.</p>
		   <p>You can find the contact information for each of the state professional licensing boards governing medicine on the Federation of State Medical Boards website at</p>
		   <p>
<a href="#">http://www.fsmb.org/state-medical-boards/contacts </a>and governing psychology on the Association of State and Provincial Psychology Boards website at<a href="#"> http://www.asppb.net/?page=BdContactNewPG.</a></p>
		   <p>Any clinical records created as a result of your use of the Site will be securely maintained by My Virtual Doctor on behalf of your Treating Provider(s) for a period that is no less than the minimum number of years such records are required to be maintained under state and federal law, and which is typically at least six years.</p>
		   <p><b>20</b>.	Miscellaneous.</p>
		   <p>These Terms of Use and your use of the Site shall be governed by the laws of the State of Delaware, without giving effect to the principles of conflict of laws. Any dispute arising under or relating in any way to these Terms of Use will be resolved exclusively by final and binding arbitration in Broward County, Florida under the rules of the American Arbitration Association, except that either party may bring a claim related to intellectual property rights, or seek temporary and preliminary specific performance and injunctive relief, in any court of competent jurisdiction, without the posting of bond or other security. The parties agree to the personal and subject matter jurisdiction and venue of the courts located in Broward County, Florida for any action related to these Terms of Use.</p>
		   <p>You understand that by checking the “agree” box for these Terms of Use and/or any other forms presented to you on the Site you are agreeing to these Terms of Use and that such action constitutes a legal signature. You agree that we may send to you any privacy or other notices, disclosures, or communications regarding the Services (collectively, "Communications") through electronic means including but not limited to: (1) by e-mail, using the address that you provided to us during registration, (2) short messaging service ("SMS") text message to the mobile number you provided us during registration, (3) push notifications on your mobile device or (4) by posting the Communications on the Site. The delivery of any Communications from us is effective when sent by us, regardless of whether you read the Communication when you receive it or whether you actually receive the delivery. You can withdraw your consent to receive Communications by email by canceling or discontinuing your use of the Service. You can opt-out of future Communications through SMS text message by replying “STOP” or by calling My Virtual Doctor customer support.</p>
		   <p>No waiver by the Company of any term or condition set forth in these Terms of Use shall be deemed a further or continuing waiver of such term or condition or a waiver of any other term or condition, and any failure of the Company to assert a right or provision under these Terms of Use shall not constitute a waiver of such right or provision. If any provision of these Terms of Use is held by a court or other tribunal of competent jurisdiction to be invalid, illegal or unenforceable for any reason, such provision shall be eliminated or limited to the minimum extent such that the remaining provisions of the Terms of Use will continue in full force and effect.</p>
		   <p>My Virtual Doctor devotes considerable effort to optimizing signal strength and diagnosis deficiencies but is not responsible for the internet or data bandwidth and signal of your mobile device.</p>
		   <p>Please report any violations of these Terms of Use to <a href="#">info@myvirtualdoctor.com</a>.</p>
		   <p>The Digital Millennium Copyright Act of 1998 (the "DMCA") provides recourse for copyright owners who believe that material appearing on the Internet infringes their rights under U.S. copyright law. If you believe in good faith that materials appearing on this Web site infringe your copyright, you (or your agent) may send us a notice requesting that the material be removed, or access to it blocked. In addition, if you believe in good faith that a notice of copyright infringement has been wrongly filed against you, the DMCA permits you to send us a counter-notice. Notices and counter-notices must meet statutory requirements imposed by the DMCA. One place to find more information is the U.S. Copyright Office Web site, currently located at <a href="#"> http://www.loc.gov/copyright</a>. In accordance with the DMCA, My Virtual Doctor has designated an agent to receive notification of alleged copyright infringement in accordance with the DMCA. Any written Notification of Claimed infringement should comply with Title 17, United States Code, Section 512(c)(3)(A) and should be provided in writing to My Virtual Doctor, LLC  7451 Wiles Road, Coral Springs, FL 33067</p>
		   <h1 style="    text-align: center;">Notice of<br>
Health Information Privacy Practices
</h1>
		    <p style="    text-align: center;"><em>Effective:  October 3, 2013</em></p>
		   <p style="    text-align: center;">This notice describes how medical information about you may be used and </p>
		   <p style="    text-align: center;">disclosed and how you can get access to this information.  </p>
		   <p style="    text-align: center;">Please review it carefully.</p>
		   
		   <h2>How is Patient Privacy Protected?</h2>	   
		   
		    <p>As the healthcare providers providing online medical services through My Virtual Doctor (the “Healthcare Providers”, “us”, “we”, “our”), we understand that information about you and your health is personal.  Because of this, we strive to maintain the confidentiality of your health information.  We continuously seek to safeguard that information through administrative, physical and technical means, and otherwise abide by applicable federal and state guidelines.</p>
			<h2>How do we use and disclose health information?</h2>
			
		   <p>We use and disclose your health information for the normal business activities that the law sees as falling in the categories of treatment, payment and health care operations.  Below we provide examples of those activities, although not every use or disclosure falling within each category is listed:</p>
		   <p><b>Treatment</b> – We keep a record of the health information you provide us.  This record may include your test results, diagnoses, medications, your response to medications or other therapies, and information we learn about your medical condition through the online services.  We may disclose this information so that other doctors, nurses, and entities such as laboratories can meet your healthcare needs</p>
		   <p><b>Payment </b>– We document the services and supplies you receive when we are providing care to you so that you, your insurance company or another third party can pay us.  We may tell your health plan about upcoming treatment or services that require prior approval by your health plan.  </p>
		    <p><b>Health Care</b> Operations – Health information is used to improve the services we provide, to train staff and students, for business management, quality improvement, and for customer service.  For example, we may use your health information to review our treatment and services and to evaluate the performance of our staff in caring for you.</p>
		   <p>We may also use your health information to:</p>
			<ul>
				<li>•	Comply with federal, state or local laws that require disclosure.</li>
				<li>•	Assist in public health activities such as tracking diseases or medical devices.</li>
				<li>•	Inform authorities to protect victims of abuse or neglect.</li>
				<li>•	Comply with Federal and state health oversight activities such as fraud investigations.</li>
				<li>•	Respond to law enforcement officials or to judicial orders, subpoenas or other process.  </li>
				<li>•	Inform coroners, medical examiners and funeral directors of information necessary for them to fulfill their duties.</li>
				<li>•	Facilitate organ and tissue donation or procurement.</li>
				<li>•	Conduct research following internal review protocols to ensure the balancing of privacy and research needs.</li>
				<li>•	Avert a serious threat to health or safety.</li>
				<li>•	Assist in specialized government functions such as national security, intelligence and protective services.</li>
				<li>•	Inform military and veteran authorities if you are an armed forces member (active or reserve).</li>
				<li>•	Inform a correctional institution if you are an inmate.</li>
				<li>•	Inform workers’ compensation carriers or your employer if you are injured at work.</li>
				<li>•	Recommend treatment alternatives.</li>
				<li>•	Tell you about health-related products and services.</li>
				<li>•	Communicate within our organization for treatment, payment, or health care operations.</li>
				<li>•	Communicate with other providers, health plans, or their related entities for their treatment or payment activities, or health care operations activities relating to quality assessment or licensing.</li>
				<li>•	Provide information to other third parties with whom we do business, such as a record storage provider.  However, you should know that in these situations, we require third parties to provide us with assurances that they will safeguard your information.</li>
				<p>We may also use or disclose your personal or health information for the following operational purposes. For example, we may:</p>
				<li>•	Communicate with individuals involved in your care or payment for that care, such as friends and family.</li>
				<li>•	Send appointment reminders.</li>
			</ul>
		   
		   
		   
		   
		   
		   <p>You may tell us that you do not want us to use or disclose your information for these two activities.
All other uses and disclosures, not previously described, may only be done with your written authorization.  For example, we need your authorization before we disclose your psychotherapy notes.  We will also obtain your authorization before we use or disclose your health information for marketing purposes or before we would sell your information.  You may revoke your authorization at any time; however, this will not affect prior uses and disclosures.
In some cases state law may require that we apply extra protections to some of your health information.  
</p>

<h2>What are the Healthcare Provider’s Responsibilities?</h2>

		   <p>We are required by law to:</p>
		   
		   
		   <ul>
	<li>•	Maintain the privacy of your health information</li>
	<li>•	Provide this notice of our duties and privacy practices</li>
	<li>•	Abide by the terms of the notice currently in effect.</li>
	<li>•	Tell you if there has been a breach that compromises your health information.</li>
</ul>

		    <p>We reserve the right to change privacy practices, and make the new practices effective for all the information we maintain.  Revised notices will be posted on the My Virtual Doctor website.</p>
			<h2>Do you have any Federal Rights?</h2>
		   <p>The law entitles you to:</p>
		   
			<ul>
				<li>•	Inspect and copy certain portions of your health information.  In most cases this will not include psychotherapy notes and we may deny your request under limited circumstances. (Fees may apply to this request).  If we keep records electronically, you may request that we provide them to you in an electronic format.</li>
				<li>•	Request amendment of your health information if you feel the health information is incorrect or incomplete.  (However, under certain circumstances we may deny your request.)</li>
				<li>•	Request amendment of your health information if you feel the health information is incorrect or incomplete.  (However, under certain circumstances we may deny your request.)</li>
				<li>•	Request that we restrict how we use or disclose your health information.  (However, we are not required to agree with your requests, unless you request that we restrict information provided to a payor, the disclosure would be for the payor’s payment or health care operations, and you have paid for the health care services completely out of pocket).</li>
				<li>•	Request that we communicate with you at a specific telephone number or address.</li>
				<li>•	Obtain a paper copy of this notice even if you receive it electronically.</li>
				
			</ul>
				   
		   
		   
		   
		   
		   <p><em>We may ask that you make some of these requests in writing. </em> </p>
		    
		   <h2>What if I have a Complaint?</h2>
		   <p>If you believe that your privacy has been violated, you may file a complaint with us or with the Secretary of Health and Human Services in Washington, D.C. We will not retaliate or penalize you for filing a complaint with the facility or the Secretary.  </p>
		     
		   
		   <ul>
	<li>To file a complaint with us or receive more information contact:</li>
	<li>Phone:          1-855-80-DOCTOR</li>
	<li>Email:           
<a href="#"> info@myvirtualdoctor.com  </a>       </li>
	<li>Address:       7451 Wiles Road, Suite 205, Coral Springs, FL 33067</li>
	
</ul>
		   
		   
		    <p>To file a complaint with the Secretary of Health and Human Services write to 200 Independence Ave., S.E., Washington, D.C. 20201 or call <a href="#">  1-877-696-6775</a>.</p>
			
			 <h2>Who Will Follow This Notice?</h2>
			
		   <p>This Notice describes the healthcare practices of:</p>
		   
		   
<ul>
	<li>•	Any physician or other health care professional authorized by us to access and/or enter information into your medical record,</li>
	<li>•	All departments and units through which My Virtual Doctor's online services are provided; and</li>
	<li>•	All affiliates and volunteers.</li>
	
</ul>
		   
		   <p>Your personal care providers may have different policies or Notices regarding their use and disclosure of your health information created in their offices.</p>
		   <h2>Need more information?</h2>
		   


<ul>
	<li>•	Visit our website at <a href="<?php echo HTTP_SERVER; ?>"><?php echo HTTP_SERVER; ?></a> </li>
	<li>•	Call or write  <a href="tel:1-855-80"> (1-855) 80-DOCTOR</a> or  <a href="#">info@myvirtualdoctor.com</a></li>
	
</ul>








 </div>
  </div>    
    </div>    
</section>





		  
		  