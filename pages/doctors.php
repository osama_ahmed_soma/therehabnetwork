<?php

//deny direct access

if ( !defined('MVD_SITE') ) {

    die('You are not authorized to view this page');

}

$settings_pages = array(

    'general-information' => 'General Information',

    'profile-settings' => 'Profile',

    'workplaces' => 'Workplaces',

    'specializations' => 'Specializations',

    'biodata' => 'Biodata',

    'languages' => 'Languages',

    'procedures-attempted' => 'Procedures Attempted',

    'conditions-treated' => 'Conditions Treated',

    'education-and-training' => 'Education and Training',

    'awards-and-distinctions' => 'Awards & Distinctions',

    'payment-method' => 'Billing & Payments',

    'consultation-plans' => 'Consultation Plans',

    'availability-timings' => 'Availability Timings'

);







$today = strtotime("Today");

//$tomorrow = strtotime('+1 day');



try {

    $query = "SELECT count(sc.id) Appointments FROM `" . DB_PREFIX . "schedule` as sc, `" . DB_PREFIX . "users` as us WHERE sc.doctorId = ? AND sc.startTime > ? AND sc.patientId = us.userId ORDER BY sc.startTime asc";

    $st = $db->prepare($query);

    $st->execute( array($_SESSION["mvdoctorID"], $today) );



    if ( $st->rowCount() )  {

        $data = $st->fetchAll();

    } else {

        $data = array();

    }

} catch (Exception $Exception) {

    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );

}



?>

<section class="dashboard-new-section clearfix">

    <div class="container">

        <div class="row">

            <div class="nav-pills">

                <div class="col-md-2 col-sm-2 col-xs-4 dns-icon  <?php echo $sub_page == '' || $sub_page == 'dashboard' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'doctors.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-list-alt"></i> Dashboard</a></div>

                <div class="col-md-2 col-sm-2 col-xs-4 dns-icon <?php echo $sub_page == 'appointments' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'doctors/appointments.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-clock-o"></i> My Calendar <span><?php if(count($data) > 0){ ?><?php echo $data[0]['Appointments']; ?><?php }else{ ?>0<?php } ?></span></a></div>

                <div class="col-md-2 col-sm-2 col-xs-4 dns-icon <?php echo $sub_page == 'messages' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'doctors/messages.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-comments"></i> Messages <span>10</span></a></div>

                <li class="col-md-2 col-sm-2 col-xs-4 dns-icon <?php echo array_key_exists($sub_page, $settings_pages) ? 'active' : ''; ?>"><a href="#vtab1" data-toggle="tab"> <i class="fa fa-gears"></i> Account Settings</a>

                    <ul>

                        <?php foreach ( $settings_pages as $page_key => $page_title ): ?>

                        

                        	<?php if($page_key=='payment-method'): ?>

                             <li class="<?php echo $sub_page == $page_key ? 'active' : ''; ?>"><a href="#vtab1" data-toggle="tab"> <i class="fa fa-gears"></i> <?php echo $page_title; ?></a>
                             
                                 <ul>
                                     <li><a href="<?php echo HTTP_SERVER . "doctors/$page_key.html" ?>">Payment Method</a></li>
                                     <li><a href="<?php echo HTTP_SERVER . "doctors/stripe-account.html" ?>">Stripe Account</a></li>
                                 </ul>
                             
                             </li>

                             

                            <?php else: ?>

                            <li class="<?php echo $sub_page == $page_key ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . "doctors/$page_key.html" ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-gears"></i> <?php echo $page_title; ?></a></li>

                            <?php endif; ?>

                        <?php endforeach; ?>

                        

                    </ul>



                </li>

               <?php /*?> <div class="col-md-2 col-sm-2 col-xs-4 dns-icon <?php echo $sub_page == 'blog' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . "doctors/blog.html" ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-clipboard"></i> Blog</a></div><?php */?>

            </div>

        </div>

    </div>

</section>

<div class="tab-content">

    <?php

    if ( $sub_page != '' ) {



        if ( array_key_exists($sub_page, $settings_pages) && !in_array($sub_page, array('payment-method', 'consultation-plans', 'availability-timings')) ) {

            if(!isset($_SESSION['success_result']['error_msg']) || empty($_SESSION['success_result']['error_msg'])) {

                $_SESSION['success_result']['error_msg'] = "";

            }



            if(!isset($_SESSION['success_result']['success_msg']) || empty($_SESSION['success_result']['success_msg'])) {

                $_SESSION['success_result']['success_msg'] = "";

            }



            if(!isset($_SESSION['success_result']['tab']) || empty($_SESSION['success_result']['tab'])) {

                $_SESSION['success_result']['tab'] = "settings";

            }



            $doctor_data = array();

            $doctor_category = array();





//$sql = "SELECT * FROM ". DB_PREFIX . "users WHERE userId = ? AND userLevel = 4 AND userStatus=1 LIMIT 1";

            $sql = "SELECT * FROM ". DB_PREFIX . "users WHERE userId = ?  AND userStatus=1 LIMIT 1";

            try {

                $result = $db->prepare( $sql );

                $result->execute( array( $_SESSION["mvdoctorVisitornUserId"] ) );



                if ( $result->rowCount() > 0 ) {

                    $row = $result->fetch();

                    $docemail = $row['userEmail'];

                    if ( $row['userPicture'] != '' ) {

                        $row['userPicture'] = unserialize(base64_decode($row['userPicture']));

                    }

                }



            } catch (Exception $Exception) {

                exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );

            }



            try {

                $query = "SELECT * FROM " . DB_PREFIX . "doctors WHERE userEmail = ? LIMIT 1";

                $result = $db->prepare($query);

                $result->execute( array($_SESSION["mvdoctorVisitorUserEmail"]) );



                if ( $result->rowCount() > 0 ) {



                    $doctor_data = $result->fetch();

                    $docid=$doctor_data['id'];

                    $_SESSION['visitorDocId'] = $docid;

                    $doctor_data['work_places'] = unserialize(base64_decode($doctor_data['work_places']));

                    $doctor_data['education'] = unserialize(base64_decode($doctor_data['education']));

                    $doctor_data['procedure_attempted'] = unserialize(base64_decode($doctor_data['procedure_attempted']));

                    $doctor_data['conditions_treated'] = unserialize(base64_decode($doctor_data['conditions_treated']));

                    $doctor_data['awards'] = unserialize(base64_decode($doctor_data['awards']));

                }



            } catch (Exception $Exception) {

                exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );

            }



            //get category data

            try {

                $query = "SELECT categoryId FROM " . DB_PREFIX . "category_data WHERE postId = ? AND postType = ?";

                $result = $db->prepare($query);

                $result->setFetchMode(PDO::FETCH_COLUMN, 0);

                $result->execute( array($docid, 'doctors') );



                if ( $result->rowCount() > 0 ) {

                    $doctor_category = $result->fetchAll( );

                }



            } catch (Exception $Exception) {

                exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );

            }





            //get category data

            try {

                $query = "SELECT id, categoryName, categoryType FROM " . DB_PREFIX . "category WHERE `categoryType` in ('speciality', 'location', 'company', 'education', 'language') AND categoryStatus = 1 order by `categoryOrder` asc";



                $result = $db->prepare($query);

                $result->execute();



                if ( $result->rowCount() > 0 ) {

                    foreach ( $result->fetchAll() as $category_data ) {

                        if ( $category_data['categoryType'] === 'speciality' ) {

                            $speciality_data[] = $category_data;

                        } elseif ( $category_data['categoryType'] === 'location' ) {

                            $location_data[] = $category_data;

                        } elseif ( $category_data['categoryType'] === 'company' ) {

                            $company_data[] = $category_data;

                        } elseif ( $category_data['categoryType'] === 'education' ) {

                            $education_data[] = $category_data;

                        }

                        elseif ( $category_data['categoryType'] === 'language' ) {

                            $language_data[] = $category_data;

                        }

                    }

                }



            } catch (Exception $Exception) {

                exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );

            }

        }



        require_once SITE_ROOT . 'pages/modules/' . $do .'/'. $sub_page .'.php';



        if ( isset($_SESSION['success_result']) ) {

            unset($_SESSION['success_result']);

        }







    } else {

        require_once SITE_ROOT . 'pages/modules/' . $do .'/dashboard.php';

    }

    ?>

</div>

<script type="text/javascript">



    jQuery(function ($){

        $('a.main_tab').on('click', function (e) {

            window.location = $(this).attr('href'); // newly activated tab

            //e.relatedTarget // previous active tab

            return false;

        });



        $('.nav-pills div a').click(function (e) {

            $('.nav-pills div.active').removeClass('active')

            $(this).parent('div').addClass('active')

        });



        //settings page

        $('.eduYear').datetimepicker({

            format: 'YYYY-MM-DD'

        });



        $('.awardYear').datetimepicker({

            format: 'YYYY-MM-DD'

        });



        $('#userDob').datetimepicker({

            format: 'YYYY-MM-DD'

        });



        function changeStateField() {

            if ($('select.userCountry').val() != 'US') {

                $('.userStateDiv').css( "display", "none" );

                $('#userStateInDr').attr('name', 'userStateBK');

                $('#userStateInDr').removeAttr('required');



                $('#userStateIn').attr('name', 'userState');

                $('#userStateIn').attr('required', 'required');

                $('.userStateInput').css( "display", "block" );

                $('#userStateIn').val( "" );



            }

            else {

                $('.userStateInput').css( "display", "none" );

                $('#userStateInDr').attr('name', 'userState');

                $('#userStateInDr').attr('required', 'required');



                $('#userStateIn').attr('name', 'userStateBK');

                $('#userStateIn').removeAttr('required');

                $('.userStateDiv').css( "display", "block" );

            }

        }



        $('select.userCountry').change(function() {

            changeStateField();

        });



    });

</script>