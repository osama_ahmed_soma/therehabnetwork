<?php

if (isset($_POST['submitDemo'])) {
   $form_name = "Providers Request a Demo";
} else if (isset($_POST['submitTrial'])) {
    $form_name = "Providers Free Trial";
} 


function sanitize ($input, $condition = 1){
 if($condition == 1){
  return htmlentities($input, ENT_QUOTES, 'UTF-8');
 }elseif ($condition == 2){
  //Allow quotes for database entry
  $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
     '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
     '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
     '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
  ); 
  $input = preg_replace($search, '', $input); 
  return htmlentities(strip_tags($input));
 }
}

	$InputNameFirst = sanitize($_POST['InputNameFirst']);
	$InputNameLast = sanitize($_POST['InputNameLast']);
	$InputEmail = sanitize($_POST['InputEmail']);
	$InputPhone = sanitize($_POST['InputPhone']);
	 $ip_address = $_SERVER['REMOTE_ADDR'];
	 $to = 'info@myvirtualdoctor.com';
	 //$to = "soulreava@gmail.com";
	 $subject = $form_name." : ".$InputNameFirst;
 $message = '
 <html>
 <head>
  <title>'.$form_name.' '.$InputNameFirst.'</title>
 </head>
 <body>
 <p><strong>Dear Administrator,</strong></p>
  <p>Here are the details of the form submission for the '.$form_name.'</p>
  <p>First Name: '.$InputNameFirst.'<br />
  Last Name: '.$InputNameLast.',<br />
  Email address: '.$InputEmail.',<br />
  Phone Number: '.$InputPhone.'<br />
  IP Address: '.$ip_address.'</p>
   <p><strong>Thank you,<br/>My Virtual Doctor</strong></p>
 </body> 
 </html>
 ';
 
 // ------------------------------------------------------------
 // SET HEADERS FOR HTML MAIL
 // ------------------------------------------------------------
 $headers  = 'MIME-Version: 1.0' . "\r\n";
 $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
 //$headers .= 'To: <'.$txbEmail.'>' . "\r\n";
 $headers .= 'From: My Virtual Doctor <info@myvirtualdoctor.com>' . "\r\n";
 //$headers .= 'Cc: anothermail@foo.org' . "\r\n";
 //$headers .= 'Bcc: '.$from_address.'' . "\r\n";
 
 // ------------------------------------------------------------
 // SEND E-MAIL
 // ------------------------------------------------------------
 $email_form = mail($to, $subject, $message, $headers);
if ($email_form){
	header('Location:providers.php?status=success');
}
else
{
 $msg = "Not send";
}
?>