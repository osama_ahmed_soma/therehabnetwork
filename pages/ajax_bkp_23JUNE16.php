<?php

//deny direct access
if ( !defined('MVD_SITE') ) {

    die('You are not authorized to view this page');

}

//csrf security check

if ( !isset($_SESSION['ls_session']['token']) || !isset( $_REQUEST['token'] ) ) {

    $result['error'] = 'You are not authorized to view this resource';

    header('Content-Type: application/json');

    echo json_encode($result);

    exit;

} elseif ( isset($_SESSION['ls_session']['token']) && $_REQUEST['token'] != $_SESSION['ls_session']['token'] ) {

    $result['error'] = 'You are not authorized to view this resource';

    header('Content-Type: application/json');

    echo json_encode($result);

    exit;

}





if ( $sub_page == 'search_form' ) {

    require_once 'modules/ajax/search_form.php';

} elseif ( $sub_page == 'login' ) {

    require_once 'modules/ajax/login.php';

} elseif ( $sub_page == 'forgot_password' ) {

    require_once 'modules/ajax/forgot_password.php';
	
} elseif ( $sub_page == 'doctor_forgot_password' ) {

    require_once 'modules/ajax/doctor_forgot_password.php';


} elseif ( $sub_page == 'register_with_email' ) {

    require_once 'modules/ajax/register_with_email.php';

} elseif ( $sub_page == 'doctor_favorite' ) {

    require_once 'modules/ajax/doctor_favorite.php';

} elseif ( $sub_page == 'remove_doctor_favorite' ) {

    require_once 'modules/ajax/remove_doctor_favorite.php';

} elseif ( $sub_page == 'blog_comment' ) {

    require_once 'modules/ajax/blog_comment.php';

} elseif ( $sub_page == 'doctor_signup_form' ) {

	require_once 'modules/ajax/signup_doctor.php';

} elseif ( $sub_page == 'doctorlogin' ) {

	require_once 'modules/ajax/doctorlogin.php';

} elseif ( $sub_page == 'contact-us' ) {

	require_once 'modules/ajax/contact-us.php';

} elseif ( $sub_page == 'request_demo' ) {

	require_once 'modules/ajax/request_demo.php';

} elseif ( $sub_page == 'settings_post' ) {

	require_once 'modules/ajax/settings_post.php';

} elseif ( $sub_page == 'add_consultation_plan' ) {

    require_once 'modules/ajax/add_consultation_plan.php';

} elseif ( $sub_page == 'check_appointment' ) {

    require_once 'modules/ajax/check_appointment.php';

} elseif ( $sub_page == 'add_doctors_timing' ) {

    require_once 'modules/ajax/add_doctors_timing.php';

} elseif ( $sub_page == 'check_available_timing' ) {

    require_once 'modules/ajax/check_available_timing.php';

} elseif ( $sub_page == 'get_login_url' ) {

    require_once 'modules/ajax/get_login_url.php';

} elseif ( $sub_page == 'signup_patient' ) {

    require_once 'modules/ajax/signup_patient.php';

} elseif ( $sub_page == 'patient_login' ) {

    require_once 'modules/ajax/patient_login.php';

} elseif ( $sub_page == 'patient_forgot_password' ) {
    require_once 'modules/ajax/patient_forgot_password.php';
}



?>