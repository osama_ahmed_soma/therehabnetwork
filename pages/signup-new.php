<div class="log-back">
         <div class="container">
            <div class="logfrom-main">
               <h1>
               See a Doctor Within Minutes</h1>
               <h3>Find highly educated and experienced online</br> doctors based on your preferences</h3>
               <div class="logfrom-left">
                  <div class="logfrom-left-back"></div>
                  <ul class="list-unstyled">
                     <li>
                        <img src="<?php echo HTTP_SERVER; ?>img/PhysiciansIcon.png"  class="logfrom-icon"/>
                        <p>
                           <span>Physicians can treat most common non-emergency medical conditions over video.</span>
                        </p>
                     </li>
                     <li>
                        <img src="<?php echo HTTP_SERVER; ?>img/PsychologistsIcon.png" class="logfrom-icon"/>
                        <p>
                           <span>Psychologists and psychiatrists can address non-emergency emotional or mental health issues.</span>
                        </p>
                     </li>
                     <li>
                        <img src="<?php echo HTTP_SERVER; ?>img/dolar.png" class="logfrom-icon"/>
                        <p>
                           <span>Risk free. If we're unable to treat you through video, we offer a money-back guarantee.</span>
                        </p>
                     </li>
                  </ul>
               </div>
               <div class="signup-right">
                  <h2 class="text-center">Get Started</h2>
                  <p class="text-center" style="    font-size: 16px;">Tell us a little more and we'll get you in to see our providers. Creating an account is free!</p>
                  <div>
                     <form data-role="form" name="registrationForm" data-novalidate="novalidate" class="ng-invalid ng-invalid-required ng-dirty">
                        <div id="fullname-wrapper" class="form-group has-feedback margin-bottom-sm">
                           <input class="form-control ng-pristine ng-valid" type="text" name="name" data-key-trigger="register({'event': event})" data-ng-model="my.name" placeholder="Full Name">
                        </div>
                        <div id="username-wrapper" class="form-group has-feedback margin-bottom-sm" data-ng-class="{ 'has-error' :  registrationForm.username.$invalid  &amp;&amp; my.submitted}">
                           <input class="form-control ng-isolate-scope ng-pristine ng-invalid ng-invalid-required ng-valid-email" autocomplete="off" type="email" name="username" data-key-trigger="register({'event': event})" data-ng-model="my.username" data-ng-change="my.auth_error = ''" placeholder="Email address" data-as-popover="my.submitted &amp;&amp; registrationForm.username.$invalid" data-as-type="emailreg" required="required">
                        </div>
                        <div id="password-wrapper" class="form-group has-feedback" data-ng-class="{ 'has-error' :  (!my.password || registrationForm.password.$invalid)  &amp;&amp; my.submitted}">
                           <input name="password" type="password" class="form-control ng-isolate-scope ng-pristine ng-valid ng-valid-minlength" autocomplete="off" placeholder="Password" data-key-trigger="register({'event': event})" data-password-policy="" data-policy-error-count="my.policy_error_count" data-ng-minlength="8" data-ng-change="my.auth_error = ''" data-ng-model="my.password">
                           <ul class="error-list"></ul>
                           <input type="password" name="password_hidden" autocomplete="off" style="display:none">
                        </div>
                        <div id="dob-wrapper" class="form-group has-feedback" data-ng-class="{ 'has-error' :  (!my.dob || registrationForm.dob.$invalid) &amp;&amp; my.submitted }">
                           <label>Date of Birth</label><br>
                           <input class="form-control ng-isolate-scope ng-invalid ng-invalid-required ng-dirty" name="dob" data-key-trigger="register({'event': event})" type="text" placeholder="mm/dd/yyyy" data-ng-model="my.dob" data-dod-date-mask="" required="required" data-as-popover="my.submitted &amp;&amp; registrationForm.dob.$invalid" data-as-type="dob">
                        </div>
                        <div id="consent" class="form-group has-feedback" data-ng-class="{ 'has-error' :  (!my.consent || registrationForm.consent.$invalid) &amp;&amp; my.submitted }">
                           <span class="required-checkbox consent-needed" data-ng-class="{ 'consent-needed' : !my.consent }">
                           <input type="checkbox" name="consent" data-key-trigger="register({'event': event})" class="cursor ng-isolate-scope ng-pristine ng-invalid ng-invalid-required" data-ng-class="{'checkbox-error' : !my.consent &amp;&amp; my.submitted }" data-ng-change="my.auth_error = ''" data-ng-model="my.consent" required="required" data-as-popover="my.submitted &amp;&amp; (!my.consent || registrationForm.consent.$invalid)" data-as-type="consent"> 
                           </span>
                           <div>
                              <p class="tou-wrapper">
                                 <label class="cursor" data-ng-click="my.consent = true">I agree to Doctor On Demand's</label>
                                 </br>
                              </p>
                              <p class="tou-Terms"><span>Terms of Use and Informed Consent.	</span>	
                           </div>
                        </div>
                  </div>
                  <a id="btn-login" href="#" class="btn btn-info sing login">Create Account </a>
				   <h5> Already a member?<a href="#"> Click here to login</a></h5>
               </div>
               </form>
			   
			   
			   
			   
            </div>
         </div>
      </div>