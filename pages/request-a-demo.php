<section class="grey clearfix" id="service"  style="background:url(img/back.png);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1 class=" Virtual-heading">Don’t Just Take Your Practice Online </h1>
            </div>
            <div class="col-md-12">
                <div class="col-md-6  col-md-offset-3 text-center">
                    <h2 class="simple pra">Take It To The Next Level. </h2>
                </div>

            </div>


        </div>
    </div>
</section>

<section class="Finding clearfix">
    <div class="container">
        <div class="row">
           <div class="col-md-6 request-detail">
           		<h4><?php /*?>See why myvirtualdoctor is the fastest growing telemedicine software for providers. Your demo will include<?php */?>
                We’ll personally walk you through our revolutionary software to show you how we can help you boost your practice’s profitability while improving operational efficiency. </h4>
                <ul>
                	<li><i class="fa fa-check-circle"></i> Increase your profitability</li>
                	<li><i class="fa fa-check-circle"></i> Treat patients at your convenience</li>
                	<li><i class="fa fa-check-circle"></i> Optimize your schedule</li>
                	<li><i class="fa fa-check-circle"></i> Increase patient satisfaction and loyalty</li>
                </ul>
           </div>
           <div class="col-md-6  request-form">
           
           	 <?php 
				if (isset($_GET['status']) && $_GET['status'] === "success"){
					echo "<div class='col-lg-12'><div class='thank_msg'>Your message has been sent!</div></div>";
				}
				else{?>
           		<h3>Request a Demo Now</h3>
          	
           		<form role="form" id="demo-form" action="#" method="post" >
				 <div id="form_message"></div>
				    <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                	<input type="text" placeholder="First Name" id="InputNameFirst" name="InputNameFirst" value="" class="form-control" required/>
                	<input type="text" placeholder="Last Name" id="InputNameLast" name="InputNameLast" value="" class="form-control" required/>
                	<input type="text" placeholder="Email" id="InputEmail" name="InputEmail" value="" class="form-control" required/>
                	<input type="text" placeholder="Phone" id="InputPhone" name="InputPhone" value="" class="form-control" required/>
                	<input type="text" placeholder="Clinic" id="InputClinic" name="InputClinic" value="" class="form-control" required/>
                   <!-- <input type="submit" value="REQUEST DEMO"  name="submit" id="submit" class="btn btn-info request-btn" /> -->
					 <button type="submit" name="submit" id="submit" id="submit" value="REQUEST DEMO" class="btn btn-info request-btn">REQUEST DEMO</button>
					
                </form>
                <?php	}
				?>
           </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    jQuery(function($){
        //reigster_form
		
        $('#demo-form').submit(function(){
            var th = $(this);
            var data = $( "#demo-form" ).serializeArray();

            th.find('button').attr('disabled', true);
			console.log(data);
            $.ajax({

                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=request_demo",
                data: data,
                method:"POST",
                dataType:"json"

            }).error(function (e) {

                th.find('button').removeAttr('disabled');
				console.log(e);


            }).done(function () {

                th.find('button').removeAttr('disabled');
				console.log('done');

            }).success(function (data) {
                //data = JQuery.parseJSON(data);
				console.log('success');	
				console.log(data);	
                if ( data.success ) {
				console.log('success2');	
						var str = "";
						str += '<div class="alert alert-success alert-dismissible" role="alert">\
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
							<strong>Success!</strong> ' + data.success + '</div>';
						$('#form_message').html(str);
                 setTimeout(function(){
                        window.location = '<?php echo HTTP_SERVER; ?>contact-us.html';
                    }, 10000); 
                }

                if ( data.error ) {
				console.log('error2');
                    var str = "";
                    $.each(data.error, function(idx, error){
                        str += '<div class="alert alert-danger alert-dismissible" role="alert">\
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                        <strong>Error!</strong> ' + error + '</div>';
                    });
                    $('#form_message').html(str);
                }

            }, 'json');
		
            return false;
		
		
		
        });
    });
</script>