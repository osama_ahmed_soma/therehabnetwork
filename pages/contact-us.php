<section class="grey clearfix">

    <div class="heading-center-map col-md-8">

        <h1>We’d love to hear from you!</h1>

        <h4>Have questions or feedback about My Virtual Doctor? Please fill out the form below. </h4>


    </div>

    <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3577.1979883878494!2d-80.227561284517!3d26.287680893043927!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88d91b25c6d04009%3A0xb973c7645270e160!2s7451+Wiles+Rd+%23105%2C+Coral+Springs%2C+FL+33067!5e0!3m2!1sen!2s!4v1462542461314"
        width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>

</section>


<section class="contact clearfix">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

            </div>

            <?php

            $min_number = 1;

            $max_number = 15;

            $random_number1 = mt_rand($min_number, $max_number);

            $random_number2 = mt_rand($min_number, $max_number);

            ?>

            <script>

                function validate() {


                    var flag = true;

                    if (jQuery('#InputReal').val() == '') {

                        jQuery('#InputReal').css("border", "1px solid #ff0000");

                        jQuery('#eror').html("Incorrect validation code");

                        flag = false;

                    }

                    else if (jQuery('#InputReal').val() != '') {

                        var result = jQuery('#resultNumber').val();

                        var userresult = jQuery('#InputReal').val()

                        if (result != userresult) {

                            jQuery('#InputReal').css("border", "1px solid #ff0000");

                            jQuery('#eror').html("Incorrect validation code");

                            flag = false;

                        }

                        else {

                            jQuery('#InputReal').css("border", "");

                            jQuery('#eror').html("");

                        }

                    }

                    else {

                        jQuery('#InputReal').css("border", "");

                        jQuery('#eror').html("");

                    }


                    return flag;


                }

            </script>

            <?php

            if (isset($_GET['status']) && $_GET['status'] === "success") {

                echo "<div class='col-lg-6'><div class='thank_msg'>Your message has been sent!</div></div>";

            } else {
                ?>


                <form role="form" id="contact-form" method="post" onsubmit="contactFormSubmit(); return false;">

                    <div id="reigster_form_error"></div>

                    <input type="hidden" name="token" value="<?php echo getToken(); ?>">

                    <div class="col-lg-6 col-md-6">

                        <div class="well well-sm"><strong><i class="glyphicon glyphicon-ok"></i> Required Field</strong>
                        </div>

                        <div class="form-group">

                            <label for="InputName">Your Name</label>

                            <div class="input-group">

                                <input type="text" class="form-control" name="InputName" id="InputName"
                                       placeholder="Enter Name" required>

                                <span class="input-group-addon"><i class="glyphicon glyphicon-ok"></i></span></div>

                        </div>

                        <div class="form-group">

                            <label for="InputEmail">Your Email</label>

                            <div class="input-group">

                                <input type="email" class="form-control" id="InputEmail" name="InputEmail"
                                       placeholder="Enter Email" required>

                                <span class="input-group-addon"><i class="glyphicon glyphicon-ok"></i></span></div>

                        </div>

                        <div class="form-group">

                            <label for="InputMessage">Message</label>

                            <div class="input-group"

                            >

                                <textarea name="InputMessage" id="InputMessage" class="form-control" rows="5"
                                          required></textarea>

                                <span class="input-group-addon"><i class="glyphicon glyphicon-ok"></i></span></div>

                        </div>

                        <div class="form-group">

                            <label for="InputReal">What is <?php echo $random_number1 . ' + ' . $random_number2 . ''; ?>
                                ? (Simple Spam Checker) </label>

                            <div class="input-group">

                                <input type="text" class="form-control" name="InputReal" id="InputReal" required>

                                <input name="firstNumber" type="hidden" value="<?php echo $random_number1; ?>"/>

                                <input name="secondNumber" type="hidden" value="<?php echo $random_number2; ?>"/>

                                <input name="resultNumber" id="resultNumber" type="hidden"
                                       value="<?php echo($random_number1 + $random_number2); ?>"/>

                                <span class="input-group-addon"><i class="glyphicon glyphicon-ok "></i></span></div>

                            <div id="eror" class=""></div>

                        </div>


                        <button type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
                            Submit
                        </button>

                    </div>

                </form>

            <?php } ?>

            <div class="col-lg-5  col-md-6  col-sm-12 col-xs-12  pull-right">

                <address class="text-right">
                    <div class="addres-cont">
                        <strong>Company Headquarters</strong><br/>
                        <a href="https://www.google.com/maps/place/7451+Wiles+Rd+%23105,+Coral+Springs,+FL+33067/@26.2876809,-80.2275613,17z/data=!3m1!4b1!4m5!3m4!1s0x88d91b25c6d04009:0xb973c7645270e160!8m2!3d26.2876761!4d-80.2253726">7451
                            Wiles Road, Suite 105
                            <br/> Coral Springs, FL 33067</a>
                    </div>

                    <div class="addres-cont">
                        <strong>Phone:</strong> <a href="tel:1-855-80">1-855-80- DOCTOR</a>
                    </div>

                    <div class="addres-cont">
                        <strong>Email:</strong> <a href="mailto:info@myvirtualdoctor.com">info@myvirtualdoctor.com</a>
                    </div>

                </address>

            </div>

        </div>


    </div>

</section>

<!-- Modal -->

<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">

        <div class="account-wall">


            <div class="">

                <img class="profile-img" src="img/logo.png" alt="">

                <form class="form-signin">

                    <div class="input-group"><span class="input-group-addon"><i class="fa-lock"></i></span>

                        <input type="text" class="form-control" placeholder="Username / Email Address">

                    </div>


                    <div class="input-group"><span class="input-group-addon"><i class="fa-lock"></i></span>

                        <input type="text" class="form-control" placeholder=" Password">

                    </div>


                    <button class="btn btn-lg btn-primary btn-block" type="submit">

                        Sign in
                    </button>

                    <label class="checkbox pull-left">

                        <input type="checkbox" value="remember-me">

                        Remember me

                    </label>

                    <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>

                </form>

            </div>

            <a href="#" class="text-center new-account">Create an account </a>

        </div>

    </div>


</div>

</div>

<script type="text/javascript">

    function contactFormSubmit(){
        if (validate()) {


            var th = $(this);

            var data = $("#contact-form").serializeArray();


            th.find('button').attr('disabled', true);

            console.log(data);

            $.ajax({


                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=contact-us",

                data: data,

                method: "POST",

                dataType: "json"


            }).error(function (e) {


                th.find('button').removeAttr('disabled');

                console.log(e);


            }).done(function () {


                th.find('button').removeAttr('disabled');

                console.log('done');


            }).success(function (data) {

                //data = JQuery.parseJSON(data);

                console.log('success');

                console.log(data);

                if (data.success) {

                    console.log('success2');

                    var str = "";

                    str += '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" ><span aria-hidden="true" >&times;</span></button><strong>Success!</strong> ' + data.success + ' </div> ';

                    $('#reigster_form_error').html(str);

                    /*setTimeout(function () {

                        window.location = '<?php echo HTTP_SERVER; ?>contact-us.html';

                    }, 10000);*/

                }


                if (data.error) {

                    console.log('error2');

                    var str = "";

                    $.each(data.error, function (idx, error) {

                        str += '<div class="alert alert-danger alert-dismissible" role="alert"><button type = "button" class= "close" data-dismiss="alert" aria-label="Close" ><span aria-hidden="true" >&times;</span></button><strong>Error!</strong> ' + error + ' </div> ';

                    });

                    $('#reigster_form_error').html(str);

                }


            }, 'json');

        }

        return false;
    }

    jQuery(function ($) {

        //reigster_form


        /*$('#contact-form').submit(function () {




        });*/

    });

</script>