<?php
class PatientAjax extends Auth_Controller
{
    public function __construct()
    {
        parent::__construct();


        //$this->load->model('appointment_model');
        $this->load->model('patient_model');
        //$this->load->model('timezone_model');
        //$this->load->model('reason_model');
        $this->load->model('doctor_model');
        $this->load->model('consultationtype_model');
    }

    public function ajax_get_doctor_patients() {
        if ( $this->input->is_ajax_request() ) {
            $id = $this->input->post('id', true);
            $limit = $this->input->post('limit', true);

            $patients = $this->doctor_model->get_doctor_patients( $id, $limit );

            if ( is_array($patients) ) {
                foreach ($patients as $patient) { ?>
                    <div class="doc-img">
                        <div class="di-user"><img src="<?php echo default_image($patient->image_url); ?>" /></div>
                        <div class="di-detail select_patient"><a href="#" data-image_url="<?php echo default_image($patient->image_url); ?>" data-patient_id="<?php echo $patient->user_id; ?>" data-patient_name="<?php echo $patient->firstname . ' ' . $patient->lastname; ?>" data-patient_details="<?php echo $patient->city; echo isset($patient->code) ? ', ' . $patient->code : ''; ?>"><?php echo $patient->firstname . ' ' . $patient->lastname; ?></a> <span><?php echo $patient->city; echo isset($patient->code) ? ', ' . $patient->code : ''; ?></span></div>
                    </div>

                    <?php

                }
            }
        } else {
            echo "Invalid Request";
        }
    }

    public function ajax_search_doctor_patients() {
        if ( $this->input->is_ajax_request() ) {
            $id = $this->input->post('id', true);
            $limit = $this->input->post('limit', true);
            $text = $this->input->post('search', true);

            $patients = $this->doctor_model->search_doctor_patients( $text, $id, $limit );

            if ( is_array($patients) ) {
                foreach ($patients as $patient) { ?>
                    <div class="doc-img">
                        <div class="di-user"><img src="<?php echo default_image($patient->image_url); ?>" /></div>
                        <div class="di-detail select_patient"><a href="#" data-image_url="<?php echo default_image($patient->image_url); ?>" data-patient_id="<?php echo $patient->user_id; ?>" data-patient_name="<?php echo $patient->firstname . ' ' . $patient->lastname; ?>" data-patient_details="<?php echo $patient->city; echo isset($patient->code) ? ', ' . $patient->code : ''; ?>"><?php echo $patient->firstname . ' ' . $patient->lastname; ?></a> <span><?php echo $patient->city; echo isset($patient->code) ? ', ' . $patient->code : ''; ?></span></div>
                    </div>

                    <?php

                }
            }
        } else {
            echo "Invalid Request";
        }
    }

    public function ajax_get_patient_doctors() {
        if ( $this->input->is_ajax_request() ) {
            $id = $this->input->post('id', true);
            $limit = $this->input->post('limit', true);

            $doctors = $this->patient_model->get_patient_doctors( $id, $limit );

            if ( is_array($doctors) ) {
                foreach ($doctors as $doctor) { ?>
                    <div class="doc-img">
                        <div class="di-user"><img src="<?php echo default_image($doctor->image_url); ?>" /></div>
                        <div class="di-detail select_patient"><a href="#" data-image_url="<?php echo default_image($doctor->image_url); ?>" data-patient_id="<?php echo $doctor->userid; ?>" data-did_stripe="<?php echo $doctor->stripe_two; ?>" data-patient_name="<?php echo $doctor->firstname . ' ' . $doctor->lastname; ?>" data-patient_details="<?php echo $doctor->city; echo isset($doctor->code) ? ', ' . $doctor->code : ''; ?>"><?php echo $doctor->firstname . ' ' . $doctor->lastname; ?></a> <span><?php echo $doctor->city; echo isset($doctor->code) ? ', ' . $doctor->code : ''; ?></span></div>
                    </div>

                    <?php

                }
            }
        } else {
            echo "Invalid Request";
        }
    }

    public function ajax_search_patient_doctors() {
        if ( $this->input->is_ajax_request() ) {
            $id = $this->input->post('id', true);
            $limit = $this->input->post('limit', true);
            $text = $this->input->post('search', true);

            $doctors = $this->patient_model->search_patient_doctors( $text, $id, $limit );

            if ( is_array($doctors) ) {
                foreach ($doctors as $doctor) { ?>
                    <div class="doc-img">
                        <div class="di-user"><img src="<?php echo default_image($doctor->image_url); ?>" /></div>
                        <div class="di-detail select_patient"><a href="#" data-image_url="<?php echo default_image($doctor->image_url); ?>" data-patient_id="<?php echo $doctor->userid; ?>"  data-did_stripe="<?php echo $doctor->stripe_two; ?>" data-patient_name="<?php echo $doctor->firstname . ' ' . $doctor->lastname; ?>" data-patient_details="<?php echo $doctor->city; echo isset($doctor->code) ? ', ' . $doctor->code : ''; ?>"><?php echo $doctor->firstname . ' ' . $doctor->lastname; ?></a> <span><?php echo $doctor->city; echo isset($doctor->code) ? ', ' . $doctor->code : ''; ?></span></div>
                    </div>

                    <?php

                }
            }
        } else {
            echo "Invalid Request";
        }
    }

    public function ajax_get_doctor_available_times( $doctor_id ) {
        if ( $this->input->is_ajax_request() ) {
            $ret = $this->doctor_model->getDoctorAvailableDate($doctor_id);
            //header('Content-Type: application/json');
            echo json_encode($ret);
            exit;
        }
        else {
            show_error('Invalid Request');
        }
    }

    public function ajax_check_patient_waived( $doctor_id ) {
        if ( $this->input->is_ajax_request() ) {
            $ret['waived'] = $this->patient_model->patient_check_waive($doctor_id);
            //header('Content-Type: application/json');
            echo json_encode($ret);
            exit;
        }
        else {
            show_error('Invalid Request');
        }
    }

    public function ajax_check_doctor_waived( $patient_id ) {
        if ( $this->input->is_ajax_request() ) {
            $ret['waived'] = $this->doctor_model->doctor_check_waive($patient_id);
            //header('Content-Type: application/json');
            echo json_encode($ret);
            exit;
        }
        else {
            show_error('Invalid Request');
        }
    }

    public function ajax_get_doctor_consultation_types( $doctor_id ) {
        $ret['error'] = 'yes';
        if ( $this->input->is_ajax_request() ) {
            $consultation_types = $this->consultationtype_model->getByDoctorList($doctor_id);

            if ( is_array($consultation_types) && !empty($consultation_types) ) {
                $ret['success'] = 'ok';
                $ret['data'] = $consultation_types;
            } else {
                $ret['error'] = "No data found.";
            }
        }
        else {
            $ret['error'] = 'Invalid Request.';
        }
        echo json_encode($ret);
    }

}