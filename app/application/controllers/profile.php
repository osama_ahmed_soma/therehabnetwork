<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends Auth_Controller
{

    function __construct()
    {
        parent::__construct();
        //$this->is_logged_in();
        //$this->load->model('login_model');
    }

    public function index()
    {
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("profile/index");
        $this->load->view("footer");
    }


    public function delete_health($c_id, $uid)
    {
        $res = $this->login_model->delete_condition($c_id, $uid);
        if ($res)
            redirect("myhealth");
    }

    public function update_health($c_id, $uid)
    {
        $id = $this->session->userdata("userid");
        $data['condition'] = $this->login_model->get_health($c_id);
        if ($uid == $id) {
            $this->load->view('header');
            $this->load->view('header_top');
            $this->load->view('nav');
            $this->load->view("health/update", $data);
            $this->load->view("footer");
        }
    }

    public function update_condition()
    {
        $condition = $this->input->post('condition');
        //$uid = $this->input->post('uid');
        $date = $this->input->post('date');
        $source = $this->input->post('source');
        $id = $this->input->post('id');
        $data = array('condition_term' => $condition,
            'date' => $date,
            'source' => $source
        );
        $this->db->where('id', $id);
        $this->db->update('health', $data);
        $this->session->set_flashdata('message', 'Health updated Successfully..');
        $data['condition'] = $this->login_model->get_health($id);
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("health/update", $data);
        $this->load->view("footer");
    }

}

?>