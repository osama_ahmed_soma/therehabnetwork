<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Invite_patients extends Auth_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->admin_access_check();
	}

	function index()
	{
		$data = [];
		$this->load->view('admin/header');
		$this->load->view('admin/invite_patients/index_view', $data);
		$this->load->view('admin/footer');
	}

	public function import()
	{
		$config['upload_path'] = SECURE_UPLOAD . '/temp/';
		$config['allowed_types'] = 'csv';
		$config['max_size'] = 1024 * 10;

//		dd($config);

		$this->load->library('upload', $config);

		if (!is_dir($config['upload_path'])) {
			if (!mkdir($config['upload_path'], 0777, true)) {
//				echo json_encode(array('errors' => 'Failed to create directory'));
				die('Failed to create directory');
			}
		}
		
		ini_set('memory_limit', '128M');

		if (!$this->upload->do_upload('file')) {
			$this->output->set_status_header(400);
			echo $this->upload->display_errors('', '');
		} else {
			$data = $this->upload->data();
//			var_debug($data);

			$array = array_map('str_getcsv', file($data['full_path']));

//			var_debug($array);

			$already_exist = [];
			$not_exist = [];

			foreach ($array as $key => $value) {
				if ($key > 0) {
//					var_debug($value);
					
					$exist_check = $this->db->get_where('user', [ 'user_email' => $value[5]]);
					$exist_check = $exist_check->row();
					
//					var_dump($exist_check);
//					
					if ($exist_check) {
						$already_exist[] = $exist_check;
					} else {
//						
//						die;
						
						$email = $value[5];
						$fname = $value[0];
						$lname = $value[1];
						$phone = $value[4];
						$state_code = $value[3];
						$city = $value[2];
						
						$stateId = null;
						$row = $this->db->get('state', ['code' => $state_code ])->row();
						if ($row) {
							$stateId = $row->id;
						}
						
						$password = random_string();
						
						$this->load->library('email');
						$this->email->clear();
						$this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
						$this->email->to($email);
						$this->email->subject('A doctor is looking to connect with you via a virtual consultation');
						$this->email->message('<html>
<head>
<title>A doctor is looking to connect with you via a virtual consultation</title>
</head>
<body>
<h4>Hi ' . $fname . ' ' . $lname . '</h4>
<p></p>
<p>Now that you have access to a scalable, integration-ready telemedicine solution, it’s time to start reducing costs and broadening the availability of healthcare to your patients.</p>
<p></p>
<strong>Here’s what you can expect as a My Virtual Doctor member:</strong>
<ul>
<li>HIPAA-compliant 2-way HD video streaming </li>
<li>Secure file sharing and patient histories</li>
<li>Secure in-consultation chat</li>
<li>Scheduled and on-demand visit capabilities</li>
<li>Billing with direct deposit</li>
<li>Customer support Mon-Fri between 9am-5pm EST</li>
<li>And more!</li>
</ul>
<p>To start improving your patient flow and revenue, just click on the button below to set up your online practice now.</p>
<p>Your login details are as follows: </p>
<p></p>
<p>Email: <strong>' . $email . '</strong></p>
<p>Password: <strong>' . $password . '</strong></p>
<p></p>
<p><a href="http://www.myvirtualdoctor.com/app/login"><strong>Let\'s Get Started</strong></a></p>
<p></p>
<p>We hope you enjoy using our platform and growing your practice.</p>
<p></p>
<p>Have a great day!, <br/>The My Virtual Doctor Team</p>
</body>
</html>');
						
						// auto_clear = false, second param true indicates we DON'T want to queue it
//						if (!$this->email->send(false, true)) {

						// auto_clear = false, second false indicates we want to queue it
//						if (!$this->email->send(false, false)) {
						if (!$this->email->send()) { // same as above
							if (DEBUG) {
								echo $this->email->print_debugger(array('headers'));
								die;
							} else {
								$this->session->set_flashdata('csv_import_error', "There is some problem while mailing to you. Please try again.");
								@unlink($data['full_path']);
								redirect('admin/invite_doctors');
							}
						} else {
							$this->db->trans_start();
							
							$this->db->insert('user', [
								'user_email' => $email,
								'user_name' => $email,
								'user_password' => password($password),
								'type' => "patient"
							]);
							
							$userId = $this->db->insert_id();

							$this->db->insert('patient', [
								'firstname' => $fname,
								'lastname' => $lname,
								'email' => $email,
								'user_id' => $userId,
								'state_id' => $stateId,
								'city' => $city,
								'phone_home' => $phone,
								'phone_mobile' => $phone,
								'phone_business' => $phone,
							]);
							
							$patient_id = $this->db->insert_id();
							
							$this->db->trans_complete();
							
							if (!$this->db->trans_status()) {
								$this->session->set_flashdata('csv_import_error', "There is problem in creating new provider.");
								@unlink($data['full_path']);
								redirect('admin/invite_patients');
							}
							$getting_patient = $this->db->get_where('patient', ['id' => $patient_id]);
							$getting_patient = $getting_patient->row();
							$not_exist[] = $getting_patient;
						}
					}
				}
			}
		}

//		var_debug($already_exist, '$already_exist');
//		var_debug($not_exist, '$not_exist');

//		d();
		
		@unlink($data['full_path']);
		
		$this->session->set_flashdata('csv_import_success', count($already_exist) . ' patient(s) aleady exist. <br>' . count($not_exist) . ' patient(s) has been created');
		redirect('admin/invite_patients');
		
	}

}
