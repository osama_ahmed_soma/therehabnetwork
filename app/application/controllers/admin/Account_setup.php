<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account_setup extends Auth_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->admin_access_check();

        $this->load->model('admin_model');
        $this->load->model('state_model');
        $this->load->model('doctor_model');
        $this->load->model('language_model');
        //$this->load->model('admin_model');
    }

    public function ajax_get_provider_list() {

    }

    private function pagination_setup($setup_data = []){
        $this->load->library('pagination');
        $limit = $setup_data['limit'];
        $config = [];
        $config['reuse_query_string'] = FALSE;
        $config['total_rows'] = $setup_data['count'];

        //bootstrap changes
        $config['full_tag_open'] = '<nav aria-label="Page navigation" style="text-align: center;"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class=" page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class=" page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class=" page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');

        //pagination value
        $config['base_url'] = base_url() . $setup_data['url'];
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    public function create_provider()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }

        $data = [];
        $data['message'] = '';
        $data['result'] = '';
        $data['bool'] = false;

        $email = $this->input->post('email');
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        
		if($this->user_model->email_exists($email)){
            echo json_encode(['message' => 'This email address is already in use, please use another email address to signup']);
            return;
        }
		
//        $p_title = $this->input->post('p_title');
//        list($specialtyId, $specialty) = explode(":", $this->input->post('specialty'));
//        $otherSpecialty = $this->input->post('otherSpecialty');
//        if ($otherSpecialty != "") {
//            $specialtyId = $this->specialty_model->create($otherSpecialty);
//            $specialty = $otherSpecialty;
//        }
//        list($stateId, $state) = explode(":", $this->input->post('state'));
        $password = random_string();
        $this->load->library('email');

        $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
        $this->email->to($email);

        $this->email->subject('Welcome to Myvirtualdoctor.com');
        $this->email->message('
<html>
<head>
    <title></title>
</head>
<body>
    <h4>Dear ' . $fname . ' ' . $lname . '</h4>
    <p></p>
    <p>Your account has been successfully created and you can now login to our doctor portal using the following information</p>
    <p></p>
    <p>Email: <strong>' . $email . '</strong></p>
    <p>Password: <strong>' . $password . '</strong></p>
    <p></p>
    <p>Click this link to login <a href="http://www.myvirtualdoctor.com/app/login">http://www.myvirtualdoctor.com/app/login</a></p>
    <p></p>
    <p>Please don\'t hesitate to contact us at  1-855-80-DOCTOR or at info@myvirtualdoctor.com for further assistance.</p>
    <p></p>
    <p>Sincerely,<br />
My Virtual Doctor</p>
</body>
</html>
            ');

        if (!$this->email->send()) {
            $data['message'] = "There is some problem while mailing to you. Please try again.";
            echo json_encode($data);
            return;
        }

        $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
        $this->email->to('info@myvirtualdoctor.com');

        $this->email->subject('New Doctor Signup (' . $fname . ' ' . $lname);
        $this->email->message('Dear Administrator,
            <br />
            <br />
            A doctor just signed up to the system and here is the information:
            <br />
            <br />
            First Name: ' . $fname . '
            <br />
            Last Name: ' . $lname . '
            <br />
            Email: ' . $email . '
            <br />
            <br />
            Thank You.');

        if (!$this->email->send()) {
            $data['message'] = "There is some problem while mailing to admin. Please try again.";
            echo json_encode($data);
            return;
        }

        $result = $this->doctor_model->createDoctor(
            $fname,
            $lname,
            $email,
            password($password),
            0
        );

        if (!$result) {
            $data['message'] = 'There is problem in creating new provider.';
            echo json_encode($data);
            return;
        }

        $data['message'] = 'An email has been sent to the doctor with the new generated password. Once he/she receives the email they can login to the system';
        $data['result'] = $result;
        $data['bool'] = true;
        echo json_encode($data);
    }

    public function create_patient()
    {

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }

        $result = [];
        $result['message'] = '';
        $result['result'] = '';
        $result['bool'] = false;

        $this->load->model('patient_model');

        $firstname = $this->input->post('fname', true);
        $lastname = $this->input->post('lname', true);
        $email = $this->input->post('email', true);
		
		if($this->user_model->email_exists($email)){
            echo json_encode(['message' => 'This email address is already in use, please use another email address to signup']);
            return;
        }
		
        $password = random_string();
        $user_data = array(
            'user_email' => $email,
            'user_password' => password($password),
            'type' => "patient"
        );

        $this->db->insert('user', $user_data);
        $insert_id = $this->db->insert_id();

        if ($insert_id > 0) {
            $result['message'] = "Patient Created Successfully.";
            $result['bool'] = true;
            $data = array(
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'user_id' => $insert_id
            );
            $this->db->insert('patient', $data);

            $this->load->library('email');
            $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
            $this->email->to($email, $firstname . ' ' . $lastname);
            $this->email->subject('Welcome to Myvirtualdoctor.com');
            $this->email->message('Dear ' . $firstname . ' ' . $lastname . ',
<br /><br />
Thank you for signing up on My Virtual Doctor.
<br /><br />
Your login is <strong>' . $email . '</strong> and password is <strong>' . $password . '</strong>
<br /><br />
Please use the following link in order to login to the patient portal
<br /><br />
<a href="' . base_url() . 'login">Login Here</a>
<br /><br />
            Thank you,');
            $this->email->send();

        } else {
            $result['message'] = "Database Error. Patient creation failed.";
        }
        echo json_encode($result);
    }

    public function provider_list($page = 0){
        $data = [];
        $this->load->model('company_model');
        if(is_numeric($page)){
            $config = [
                'limit' => 20,
                'count' => $this->doctor_model->get_all_doctors_count(),
                'url' => 'admin/account_setup/provider_list'
            ];
            $data['doctors'] = $this->doctor_model->get_all_doctors($config['limit'], $page);
            $data['pagination_link'] = $this->pagination_setup($config);
        } else {
            $company= $this->company_model->getByField('code', $page);
            if(!empty($company)){
                $data['doctors'] = $this->doctor_model->get_all_doctors(null, null, $company->id);
            } else {
                $data['doctors'] = [];
            }
            $data['pagination_link'] = '';
        }
        if(!empty($data['doctors'])) {
            foreach ($data['doctors'] as $key => $doctor) {
                $specialties_string = '';
                $specialties = $this->doctor_model->get_doctors_specialization($doctor->userid);
                if (is_array($specialties)) {
                    $i = 0;
                    foreach ($specialties as $specialty) {
                        $specialties_string .= $specialty->speciality;
                        if ($i < (count($specialties) - 1)) {
                            $specialties_string .= ', ';
                        }
                        $i++;
                    }
                } else {
                    $specialties_string .= 'N/A';
                }
                $data['doctors'][$key]->doctor_specialty = $specialties_string;
                $data['doctors'][$key]->company_name = $this->company_model->getById($doctor->company_id);
            }
        }
        $this->load->view('admin/header');
        $data['specialties'] = $this->doctor_model->getSpecialtyList();
        $data['states'] = $this->state_model->getList();
        $data['companies'] = $this->company_model->getList();
        $this->load->view('admin/account_setup/provider_list', $data);
        $this->load->view('admin/footer');
    }

    public function search_provider(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $this->load->model('company_model');
        $search = trim($this->input->post('search'));
        $comapny_id_selected = $this->input->post('company');
        $data = [];
        $data['bool'] = false;
        $data['doctors'] = $this->doctor_model->get_list_by_name_email_phone($search, $comapny_id_selected);
        if(count($data['doctors']) == 0){
            echo json_encode($data);
            return;
        }
        $data['bool'] = true;
        $x = 0;
        foreach ($data['doctors'] as $key => $doctor){
            $data['doctors'][$x]->created_at = date('m/d/Y', strtotime($doctor->created_at));
            $data['doctors'][$x]->phone_mobile = ($doctor->phone_mobile) ? $doctor->phone_mobile : 'N/A';
            $data['doctors'][$x]->login_date_time = ($doctor->is_joined == 1) ? date('m/d/Y h:i A', strtotime($doctor->login_date_time)) : 'Never';
            $company = $this->company_model->getById($doctor->company_id);
            $data['doctors'][$x]->company_name = ($company) ? $company->name : 'N/A';
            $specialties_string = '';
            $specialties = $this->doctor_model->get_doctors_specialization($doctor->userid);
            if(is_array($specialties)){
                $i = 0;
                foreach ($specialties as $specialty){
                    $specialties_string .= $specialty->speciality;
                    if($i < (count($specialties) - 1)){
                        $specialties_string .= ', ';
                    }
                    $i++;
                }
            } else {
                $specialties_string .= 'N/A';
            }
            $x++;
            $data['doctors'][$key]->doctor_specialty = $specialties_string;
        }
        echo json_encode($data);
    }

    public function patient_list($page = 0) {
        $data = [];
        $this->load->model('patient_model');
        $config = [
            'limit' => 20,
            'count' => $this->patient_model->get_all_patient_count(),
            'url' => 'admin/account_setup/patient_list'
        ];
        $data['patients'] = $this->patient_model->get_all_patient($config['limit'], $page);
        $i = 0;
        foreach ($data['patients'] as $patient){
            $data['patients'][$i]->primary_physician = $this->doctor_model->get_doctor_by_patient_id($patient->user_id);
            if($data['patients'][$i]->primary_physician){
                if($data['patients'][$i]->primary_physician->state_id){
                    $data['patients'][$i]->primary_physician->state_name = $this->state_model->get_by_id($data['patients'][$i]->primary_physician->state_id)->name;
                } else {
                    $data['patients'][$i]->primary_physician->state_name = '';
                }
            }
            $i++;
        }
        $data['pagination_link'] = $this->pagination_setup($config);
        $this->load->view('admin/header');
        $this->load->view('admin/account_setup/patient_list', $data);
        $this->load->view('admin/footer');
    }

    public function search_patient(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $search = trim($this->input->post('search'));
        $data = [];
        $data['bool'] = false;
        $this->load->model('patient_model');
        $data['patients'] = $this->patient_model->get_list_by_name_email_phone($search);
        if(count($data['patients']) == 0){
            echo json_encode($data);
            return;
        }
        $data['bool'] = true;
        $i = 0;
        foreach ($data['patients'] as $key => $patient){
            $primary_physician = $this->doctor_model->get_doctor_by_patient_id($patient->user_id);
            if(!$primary_physician){
                $primary_physician = new stdClass();
                $primary_physician->full_name = 'None';
            } else {
                if($primary_physician->state_id){
                    $primary_physician->state_name = $this->state_model->get_by_id($primary_physician->state_id)->name;
                } else {
                    $primary_physician->state_name = '';
                }
                $primary_physician->full_name = '<a href="" data-toggle="modal" data-target="#primary_physician_detail" data-first_name="'.$primary_physician->firstname.'" data-last_name="'.$primary_physician->lastname.'" data-email="'.$primary_physician->user_email.'" data-phone_number="'.$primary_physician->phone_mobile.'" data-city="'.$primary_physician->city.'" data-state="'.$primary_physician->state_name.'" data-zip="'.$primary_physician->zip.'" data-is_joined="'.(($primary_physician->is_joined == 1) ? 'Yes' : 'No').'" class="primary_physician_modal">'.$primary_physician->firstname.' '.$primary_physician->lastname.'</a>';
            }
            $data['patients'][$i]->primary_physician = $primary_physician;
            $data['patients'][$i]->created_at = date('m/d/Y', strtotime($patient->created_at));
            $data['patients'][$i]->login_date_time = ($patient->is_joined == 1) ? date('m/d/Y h:i A', strtotime($patient->login_date_time)) : 'Never';
            $data['patients'][$i]->full_name = ($patient->firstname || $patient->lastname) ? $patient->firstname.' '.$patient->lastname : 'N/A';
            $data['patients'][$i]->phone_mobile = ($patient->phone_mobile) ? $patient->phone_mobile : 'N/A';
            $i++;
        }
        echo json_encode($data);
    }

    public function remove_provider(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $userid = $this->input->post('userid');
        $this->doctor_model->remove_doctor($userid);
        return true;
    }

    public function remove_patient(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $userid = $this->input->post('userid');
        $this->load->model('patient_model');
        $this->patient_model->remove_patient($userid);
        return true;
    }

    public function deactivate_provider(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $userid = $this->input->post('userid');
        $this->user_model->update([
            'is_active' => 0
        ], 'id', $userid);
        return true;
    }

    public function activate_provider(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $userid = $this->input->post('userid');
        $this->user_model->update([
            'is_active' => 1
        ], 'id', $userid);
        return true;
    }

    public function deactivate_patient(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $userid = $this->input->post('userid');
        $this->user_model->update([
            'is_active' => 0
        ], 'id', $userid);
        return true;
    }

    public function activate_patient(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $userid = $this->input->post('userid');
        $this->user_model->update([
            'is_active' => 1
        ], 'id', $userid);
        return true;
    }

    public function ajax_get_patient_list() {

    }

    public function index() {
        echo "this is index test";
    }

}