<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Timezones extends Auth_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('timezones_model');

        $this->admin_access_check();
    }

    public function index()
    {
        $data = [];
        $data['timezones'] = $this->timezones_model->get([
            '*' => ''
        ]);
        $is_request = $this->input->post('is_request');
        if ($is_request && $is_request == 'yes') {
            echo json_encode($data['timezones']);
            return;
        }
        $this->load->view('admin/header');
        $this->load->view('admin/timezones/index', $data);
        $this->load->view('admin/footer');
    }

    public function add()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $return = [];
        $return['message'] = '';
        $return['bool'] = false;
        $var = trim($this->input->post('var'));
        $name = trim($this->input->post('name'));
        $order_by = trim($this->input->post('order_by'));
        if ($this->timezones_model->get([
                'var' => $var,
                '*' => ''
            ]) != null
        ) {
            $return['message'] = 'This timezone is already exists. Try different and new.';
            echo json_encode($return);
            return;
        }
        $inserted_id = $this->timezones_model->add([
            'var' => $var,
            'name' => $name,
            'order_by' => $order_by
        ]);
        if ($inserted_id) {
            $return['message'] = 'Successfully Added.';
            $return['bool'] = true;
        } else {
            $return['message'] = 'There is an error while adding new timezone. Please try again.';
        }
        echo json_encode($return);
    }

    public function update()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $return = [];
        $return['message'] = 'Successfully Updated.';
        $return['bool'] = false;
        $id = trim($this->input->post('id'));
        $var = trim($this->input->post('var'));
        $name = trim($this->input->post('name'));
        $order_by = trim($this->input->post('order_by'));
        if ($this->timezones_model->get([
                'var' => $var,
                'id !=' => $id,
                '*' => ''
            ]) != null
        ) {
            $return['message'] = 'This timezone is already exists. Try different and new.';
            echo json_encode($return);
            return;
        }
        $this->timezones_model->update([
            'id' => $id
        ], [
            'var' => $var,
            'name' => $name,
            'order_by' => $order_by
        ]);
        $return['bool'] = true;
        echo json_encode($return);
    }

    public function remove()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $return = [];
        $return['message'] = 'Successfully Removed.';
        $return['bool'] = true;
        $id = trim($this->input->post('id'));
        $this->timezones_model->remove($id);
        echo json_encode($return);
    }

    public function search()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $return = [];
        $return['message'] = '';
        $return['bool'] = false;
        $search = trim($this->input->post('search'));
        $data = $this->timezones_model->search($search);
        if ($data == null) {
            $return['message'] = 'No timezone exists with this (' . $search . ') variable or name.';
        } else {
            $return['message'] = (count($data) > 1) ? count($data) . ' timezones found.' : count($data) . ' timezone found.';
            $return['result'] = $data;
            $return['bool'] = true;
        }
        echo json_encode($return);
    }

    public function test()
    {
        $data = $this->timezones_model->search('Newasdad');
        echo "<pre>";
        var_dump($data);
        var_dump(($data != null));
    }

}