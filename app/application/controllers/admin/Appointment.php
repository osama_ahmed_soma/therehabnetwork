<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Appointment extends Auth_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_access_check();

        $this->load->model('appointment_model');
    }

    private function pagination_setup($setup_data = []){
        $this->load->library('pagination');
        $limit = $setup_data['limit'];
        $config = [];
        $config['reuse_query_string'] = FALSE;
        $config['total_rows'] = $setup_data['count'];

        //bootstrap changes
        $config['full_tag_open'] = '<nav aria-label="Page navigation" style="text-align: center;"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class=" page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class=" page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class=" page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');

        //pagination value
        $config['base_url'] = base_url() . $setup_data['url'];
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    public function all_appointment($page = 0){
        $data = [];
        $config = [
            'limit' => 20,
            'count' => $this->appointment_model->getAll_count(),
            'url' => 'admin/appointment/all_appointment'
        ];
        $data['all_appointments'] = $this->appointment_model->getAll($config['limit'], $page);
        $data['all_appointments_count'] = $config['count'];
        $data['pagination_link'] = $this->pagination_setup($config);
        $this->load->view('admin/header');
        $this->load->view('admin/appointments/appointment_all', $data);
        $this->load->view('admin/footer');
    }

    public function search_all_appointments(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $provider_search = trim($this->input->post('provider_search'));
        $patient_search = trim($this->input->post('patient_search'));
        $data = [];
        $data['bool'] = false;
        $data['all_appointments'] = $this->appointment_model->appointment_search(
            [],
            $provider_search,
            $patient_search
        );
        if(count($data['all_appointments']) == 0){
            echo json_encode($data);
            return;
        }
        $data['bool'] = true;
        $i = 0;
        foreach ($data['all_appointments'] as $appointment){
            $data['all_appointments'][$i]->start_date_stamp_date = date("jS F Y", $appointment->start_date_stamp);
            $data['all_appointments'][$i]->start_date_stamp_time = date('g:i A', $appointment->start_date_stamp);
            $data['all_appointments'][$i]->end_date_stamp_time = date('g:i A', $appointment->end_date_stamp);
            $i++;
        }
        echo json_encode($data);
    }

    public function remove_all_appointment(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $appointmentid = $this->input->post('appointmentid');
        return $this->remove_appointment($appointmentid);
    }

    public function appointment_today($page = 0){
        $data = [];
        $config = [
            'limit' => 10,
            'count' => $this->appointment_model->appointment_today_count(),
            'url' => 'admin/appointment/appointment_today'
        ];
        $data['today_appointments'] = $this->appointment_model->appointment_today($config['limit'], $page);
        $data['today_appointments_count'] = $config['count'];
        $data['pagination_link'] = $this->pagination_setup($config);
        $this->load->view('admin/header');
        $this->load->view('admin/appointments/appointment_today', $data);
        $this->load->view('admin/footer');
    }

    public function search_today_appointments(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $date_time_array = [];
        $date_time_array['date_search'] = (trim($this->input->post('date_search'))) ? strtotime(trim($this->input->post('date_search'))) : '' ;
        if($date_time_array['date_search'] != ''){
            $date_time_array['date_search_start'] = $date_time_array['date_search'];
            $date_time_array['date_search_end'] = strtotime('+24 hours', date('m/d/Y', $date_time_array['date_search_start']));
        }
        $provider_search = trim($this->input->post('provider_search'));
        $patient_search = trim($this->input->post('patient_search'));
        $data = [];
        $data['bool'] = false;
        $data['today_appointments'] = $this->appointment_model->appointment_today_search(
            $date_time_array,
            $provider_search,
            $patient_search
        );
        if(count($data['today_appointments']) == 0){
            echo json_encode($data);
            return;
        }
        $data['bool'] = true;
        $i = 0;
        foreach ($data['today_appointments'] as $appointment){
            $data['today_appointments'][$i]->start_date_stamp_date = date("jS F Y", $appointment->start_date_stamp);
            $data['today_appointments'][$i]->start_date_stamp_time = date('g:i A', $appointment->start_date_stamp);
            $data['today_appointments'][$i]->end_date_stamp_time = date('g:i A', $appointment->end_date_stamp);
            $i++;
        }
        echo json_encode($data);
    }

    public function remove_today_appointment(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $appointmentid = $this->input->post('appointmentid');
        return $this->remove_appointment($appointmentid);
    }

    public function pending_appointment($page = 0){
        $data = [];
        $config = [
            'limit' => 10,
            'count' => $this->appointment_model->appointment_pending_count(),
            'url' => 'admin/appointment/pending_appointment'
        ];
        $data['pending_appointments'] = $this->appointment_model->appointment_pending($config['limit'], $page);
        $data['pending_appointments_count'] = $config['count'];
        $data['pagination_link'] = $this->pagination_setup($config);
        $this->load->view('admin/header');
        $this->load->view('admin/appointments/pending_appointment', $data);
        $this->load->view('admin/footer');
    }

    public function search_pending_appointments(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $date_time_array = [];
        $date_time_array['date_search'] = (trim($this->input->post('date_search'))) ? strtotime(trim($this->input->post('date_search'))) : '' ;
        if($date_time_array['date_search'] != ''){
            $date_time_array['date_search_start'] = $date_time_array['date_search'];
            $date_time_array['date_search_end'] = strtotime('+1 day', $date_time_array['date_search_start']);
        }
        $provider_search = trim($this->input->post('provider_search'));
        $patient_search = trim($this->input->post('patient_search'));
        $data = [];
        $data['bool'] = false;
        $data['pending_appointments'] = $this->appointment_model->appointment_pending_search(
            $date_time_array,
            $provider_search,
            $patient_search
        );
        if(count($data['pending_appointments']) == 0){
            echo json_encode($data);
            return;
        }
        $data['bool'] = true;
        $i = 0;
        foreach ($data['pending_appointments'] as $appointment){
            $data['pending_appointments'][$i]->start_date_stamp_date = date("jS F Y", $appointment->start_date_stamp);
            $data['pending_appointments'][$i]->start_date_stamp_time = date('g:i A', $appointment->start_date_stamp);
            $data['pending_appointments'][$i]->end_date_stamp_time = date('g:i A', $appointment->end_date_stamp);
            $i++;
        }
        echo json_encode($data);
    }

    public function remove_pending_appointment(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $appointmentid = $this->input->post('appointmentid');
        return $this->remove_appointment($appointmentid);
    }

    public function cancelled_appointment($page = 0){
        $data = [];
        $config = [
            'limit' => 10,
            'count' => $this->appointment_model->appointment_cancelled_count(),
            'url' => 'admin/appointment/cancelled_appointment'
        ];
        $data['cancelled_appointments'] = $this->appointment_model->appointment_cancelled($config['limit'], $page);
        $data['cancelled_appointments_count'] = $config['count'];
        $data['pagination_link'] = $this->pagination_setup($config);
        $this->load->view('admin/header');
        $this->load->view('admin/appointments/cancelled_appointment', $data);
        $this->load->view('admin/footer');
    }

    public function search_cancelled_appointments(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $date_time_array = [];
        $date_time_array['date_search'] = (trim($this->input->post('date_search'))) ? strtotime(trim($this->input->post('date_search'))) : '' ;
        if($date_time_array['date_search'] != ''){
            $date_time_array['date_search_start'] = $date_time_array['date_search'];
            $date_time_array['date_search_end'] = strtotime('+1 day', $date_time_array['date_search_start']);
        }
        $provider_search = trim($this->input->post('provider_search'));
        $patient_search = trim($this->input->post('patient_search'));
        $data = [];
        $data['bool'] = false;
        $data['cancelled_appointments'] = $this->appointment_model->appointment_cancelled_search(
            $date_time_array,
            $provider_search,
            $patient_search
        );
        if(count($data['cancelled_appointments']) == 0){
            echo json_encode($data);
            return;
        }
        $data['bool'] = true;
        $i = 0;
        foreach ($data['cancelled_appointments'] as $appointment){
            $data['cancelled_appointments'][$i]->start_date_stamp_date = date("jS F Y", $appointment->start_date_stamp);
            $data['cancelled_appointments'][$i]->start_date_stamp_time = date('g:i A', $appointment->start_date_stamp);
            $data['cancelled_appointments'][$i]->end_date_stamp_time = date('g:i A', $appointment->end_date_stamp);
            $i++;
        }
        echo json_encode($data);
    }

    public function remove_cancelled_appointment(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $appointmentid = $this->input->post('appointmentid');
        return $this->remove_appointment($appointmentid);
    }

    private function remove_appointment($appointmentid){
        $this->appointment_model->remove_appointment($appointmentid);
        return true;
    }

}