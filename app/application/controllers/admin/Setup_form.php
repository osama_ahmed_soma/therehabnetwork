<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Setup_form extends Auth_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_access_check();
    }

    private function pagination_setup($setup_data = []){
        $this->load->library('pagination');
        $limit = $setup_data['limit'];
        $config = [];
        $config['reuse_query_string'] = FALSE;
        $config['total_rows'] = $setup_data['count'];

        //bootstrap changes
        $config['full_tag_open'] = '<nav aria-label="Page navigation" style="text-align: center;"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class=" page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class=" page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class=" page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');

        //pagination value
        $config['base_url'] = base_url() . $setup_data['url'];
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    public function add_app_reason($page = 0){
        $this->load->model('reason_model');
        $data = [];
        $config = [
            'limit' => 20,
            'count' => $this->reason_model->getList_count(),
            'url' => 'admin/setup_form/add_app_reason'
        ];
        $data['reasons'] = $this->reason_model->getList($config['limit'], $page);
        $data['pagination_link'] = $this->pagination_setup($config);
        $this->load->view('admin/header');
        $this->load->view('admin/setup_form/add_app_reason', $data);
        $this->load->view('admin/footer');
    }

    public function add_app_reason_ajax(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $reason_name = $this->input->post('reason_name');
        $this->load->model('reason_model');
        if($this->reason_model->check_exists($reason_name)){
            echo json_encode([
                'message' => 'This reason is already exists.',
                'bool' => false
            ]);
            return;
        }
        $reason_id = $this->reason_model->add($reason_name);
        echo json_encode([
            'message' => 'New Reason Added',
            'reason_id' => $reason_id,
            'bool' => true
        ]);
        return;
    }

    public function edit_app_reason_ajax(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $reason_id = $this->input->post('reason_id');
        $reason_name = $this->input->post('reason_name');
        $this->load->model('reason_model');
        if($this->reason_model->check_exists($reason_name)){
            echo json_encode([
                'message' => 'This reason is already exists.',
                'bool' => false
            ]);
            return;
        }
        $this->reason_model->update($reason_id, $reason_name);
        echo json_encode([
            'message' => 'Reason Edited',
            'reason_id' => $reason_id,
            'reason_name' => $reason_name,
            'bool' => true
        ]);
        return;
    }

    public function remove_reason(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $reasonid = $this->input->post('reasonid');
        $this->load->model('reason_model');
        $this->reason_model->remove($reasonid);
        return true;
    }

    public function add_medicines($page = 0){
        $this->load->model('drug_model');
        $data = [];
        $config = [
            'limit' => 20,
            'count' => $this->drug_model->getAll_count(),
            'url' => 'admin/setup_form/add_medicines'
        ];
        $data['medicines'] = $this->drug_model->getAll($config['limit'], $page);
        $data['pagination_link'] = $this->pagination_setup($config);
        $this->load->view('admin/header');
        $this->load->view('admin/setup_form/add_medicines', $data);
        $this->load->view('admin/footer');
    }

    public function add_medicines_ajax(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $medicine_name = $this->input->post('medicine_name');
        $this->load->model('drug_model');
        if($this->drug_model->check_exists($medicine_name)){
            echo json_encode([
                'message' => 'This medicine is already exists.',
                'bool' => false
            ]);
            return;
        }
        $medicine_id = $this->drug_model->add($medicine_name);
        echo json_encode([
            'message' => 'New Medicine Added',
            'medicine_id' => $medicine_id,
            'bool' => true
        ]);
        return;
    }

    public function edit_medicine_ajax(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $medicine_id = $this->input->post('medicine_id');
        $medicine_name = $this->input->post('medicine_name');
        $this->load->model('drug_model');
        if($this->drug_model->check_exists($medicine_name)){
            echo json_encode([
                'message' => 'This medicine is already exists.',
                'bool' => false
            ]);
            return;
        }
        $this->drug_model->update($medicine_id, [
            'BrandName' => $medicine_name
        ]);
        echo json_encode([
            'message' => 'Medicine Edited',
            'medicine_id' => $medicine_id,
            'medicine_name' => $medicine_name,
            'bool' => true
        ]);
        return;
    }

    public function remove_medicine(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $medicineid = $this->input->post('medicineid');
        $this->load->model('drug_model');
        $this->drug_model->remove($medicineid);
        return true;
    }

    public function add_procedure($page = 0){
        $this->load->model('procedure_model');
        $data = [];
        $config = [
            'limit' => 20,
            'count' => $this->procedure_model->getAll_count(),
            'url' => 'admin/setup_form/add_procedure'
        ];
        $data['procedures'] = $this->procedure_model->getAll($config['limit'], $page);
        $data['pagination_link'] = $this->pagination_setup($config);

        $this->load->view('admin/header');
        $this->load->view('admin/setup_form/add_procedure', $data);
        $this->load->view('admin/footer');
    }

    public function add_procedure_ajax(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $procedure_name = $this->input->post('procedure_name');
        $procedure_code = $this->input->post('procedure_code');
        $this->load->model('procedure_model');
        if($this->procedure_model->check_exists($procedure_name, $procedure_code)){
            echo json_encode([
                'message' => 'This procedure is already exists.',
                'bool' => false
            ]);
            return;
        }
        $procedure_id = $this->procedure_model->add([
            'name' => $procedure_name,
            'code' => $procedure_code
        ]);
        echo json_encode([
            'message' => 'New Procedure Added',
            'procedure_id' => $procedure_id,
            'bool' => true
        ]);
        return;
    }

    public function edit_procedure_ajax(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $procedure_id = $this->input->post('procedure_id');
        $procedure_name = $this->input->post('procedure_name');
        $procedure_code = $this->input->post('procedure_code');
        $this->load->model('procedure_model');
        if($this->procedure_model->check_exists($procedure_name, $procedure_code)){
            echo json_encode([
                'message' => 'This procedure is already exists.',
                'bool' => false
            ]);
            return;
        }
        $this->procedure_model->update($procedure_id, [
            'name' => $procedure_name,
            'code' => $procedure_code
        ]);
        echo json_encode([
            'message' => 'Procedure Edited',
            'procedure_id' => $procedure_id,
            'procedure_name' => $procedure_name,
            'procedure_code' => $procedure_code,
            'bool' => true
        ]);
        return;
    }

    public function remove_procedure(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $procedureid = $this->input->post('procedureid');
        $this->load->model('procedure_model');
        $this->procedure_model->remove($procedureid);
        return true;
    }

    public function add_diagnosis($page = 0){
        $this->load->model('diagnosis_model');
        $data = [];
        $config = [
            'limit' => 20,
            'count' => $this->diagnosis_model->getAll_count(),
            'url' => 'admin/setup_form/add_diagnosis'
        ];
        $data['diagnoses'] = $this->diagnosis_model->getAll($config['limit'], $page);
        $data['pagination_link'] = $this->pagination_setup($config);
        $this->load->view('admin/header');
        $this->load->view('admin/setup_form/add_diagnosis', $data);
        $this->load->view('admin/footer');
    }

    public function add_diagnosis_ajax(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $diagnosis_name = $this->input->post('diagnosis_name');
        $diagnosis_code = $this->input->post('diagnosis_code');
        $this->load->model('diagnosis_model');
        if($this->diagnosis_model->check_exists($diagnosis_name, $diagnosis_code)){
            echo json_encode([
                'message' => 'This diagnosis is already exists.',
                'bool' => false
            ]);
            return;
        }
        $diagnosis_id = $this->diagnosis_model->add([
            'name' => $diagnosis_name,
            'code' => $diagnosis_code
        ]);
        echo json_encode([
            'message' => 'New Diagnosis Added',
            'diagnosis_id' => $diagnosis_id,
            'bool' => true
        ]);
        return;
    }

    public function edit_diagnosis_ajax(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $diagnosis_id = $this->input->post('diagnosis_id');
        $diagnosis_name = $this->input->post('diagnosis_name');
        $diagnosis_code = $this->input->post('diagnosis_code');
        $this->load->model('diagnosis_model');
        if($this->diagnosis_model->check_exists($diagnosis_name, $diagnosis_code)){
            echo json_encode([
                'message' => 'This diagnosis is already exists.',
                'bool' => false
            ]);
            return;
        }
        $this->diagnosis_model->update($diagnosis_id, [
            'name' => $diagnosis_name,
            'code' => $diagnosis_code
        ]);
        echo json_encode([
            'message' => 'Diagnosis Edited',
            'diagnosis_id' => $diagnosis_id,
            'diagnosis_name' => $diagnosis_name,
            'diagnosis_code' => $diagnosis_code,
            'bool' => true
        ]);
        return;
    }

    public function remove_diagnosis(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $diagnosisid = $this->input->post('diagnosisid');
        $this->load->model('diagnosis_model');
        $this->diagnosis_model->remove($diagnosisid);
        return true;
    }

    public function add_specialty($page = 0){
        $this->load->model('specialty_model');
        $data = [];
        $config = [
            'limit' => 20,
            'count' => $this->specialty_model->getList_count(),
            'url' => 'admin/setup_form/add_specialty'
        ];
        $data['specialties'] = $this->specialty_model->getList($config['limit'], $page);
        $data['pagination_link'] = $this->pagination_setup($config);
        $this->load->view('admin/header');
        $this->load->view('admin/setup_form/add_specialty', $data);
        $this->load->view('admin/footer');
    }

    public function add_specialty_ajax(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $specialty_name = $this->input->post('specialty_name');
        $this->load->model('specialty_model');
        if($this->specialty_model->check_exists($specialty_name)){
            echo json_encode([
                'message' => 'This specialty is already exists.',
                'bool' => false
            ]);
            return;
        }
        $specialty_id = $this->specialty_model->create($specialty_name);
        echo json_encode([
            'message' => 'New Specialty Added',
            'specialty_id' => $specialty_id,
            'bool' => true
        ]);
        return;
    }

    public function edit_specialty_ajax(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $specialty_id = $this->input->post('specialty_id');
        $specialty_name = $this->input->post('specialty_name');
        $this->load->model('specialty_model');
        if($this->specialty_model->check_exists($specialty_name)){
            echo json_encode([
                'message' => 'This specialty is already exists.',
                'bool' => false
            ]);
            return;
        }
        $this->specialty_model->update($specialty_id, [
            'name' => $specialty_name
        ]);
        echo json_encode([
            'message' => 'Specialty Edited',
            'specialty_id' => $specialty_id,
            'specialty_name' => $specialty_name,
            'bool' => true
        ]);
        return;
    }

    public function remove_specialty(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $specialtyid = $this->input->post('specialtyid');
        $this->load->model('specialty_model');
        $this->specialty_model->remove($specialtyid);
        return true;
    }

}