<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Admin extends Auth_Controller {

	public function __construct() {
		parent::__construct();

		$this->admin_access_check();

		$this->load->model('admin_model');

		$this->load->helper('captcha');
		captcha_required();
	}

	public function login() {
		/*
		  $options = [
		  'cost' => 11,
		  'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
		  ];

		  $user_key = getRandomString(20);
		  $new_pass = password_hash(SITE_KEY . 'temp' . $user_key, PASSWORD_DEFAULT, $options);

		  echo $user_key . "<br>" . $new_pass;
		 */

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
		$data = array();

		$config = array(
			array(
				'field' => 'user_login',
				'label' => 'Username',
				'rules' => 'required'
			),
			array(
				'field' => 'user_password',
				'label' => 'Password',
				'rules' => array('required', 'min_length[4]'),
				'errors' => array(
					'required' => 'Please enter your %s.',
				),
			)
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == true && empty($this->session->flashdata('captcha_error')) == true) {
			$user_login = $this->input->post('user_login', true);
			$user_password = $this->input->post('user_password', true);
			$user = $this->admin_model->get_userdata_by_login($user_login);

			if (is_object($user)) {
				//now check password
				$hash = $user->user_password;
				$password_to_check = SITE_KEY . $user_password . $user->user_key;

				if (password_verify($password_to_check, $hash)) {
					//if password need rehash with a new algorithm
					$options = [
						'cost' => 11,
						'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
					];

					if (password_needs_rehash($hash, PASSWORD_DEFAULT, $options)) {
						$user_key = getRandomString(20);
						$new_pass = password_hash(SITE_KEY . $user_password . $user_key, PASSWORD_DEFAULT, $options);

						//store new password to database
						$user_data = array(
							'user_password' => $new_pass,
							'user_key' => $user_key
						);
						$user_where = array(
							'id' => $user->id
						);

						$this->admin_model->update_user($user_data, $user_where);
					}

					//store userdata to session
					$session_data = (array) $user;
					$session_data['logged_in'] = true;

					//remove sensitive information
					unset($session_data['user_login'], $session_data['user_password'], $session_data['user_key']);

					$this->session->set_userdata([
						'admin_sess' => $session_data
					]);

					reset_attempts();

					//redirect user to dashboard
					redirect('admin/dashboard');
				} else {
					failed_attempt();
					$data['loginError'] = "Incorrect Login Information, Please try again.";
				}
			} else {
				failed_attempt();
				$data['loginError'] = 'Invalid username or password.';
			}
		} else {
			//now load view
			if ($this->session->userdata('admin_sess')['logged_in']) {
				redirect('admin/dashboard');
			}
			failed_attempt();
		}

		$this->load->view('admin/header');
		$this->load->view('admin/login/index', $data);
		$this->load->view('admin/footer');
	}

	public function logout() {
		$this->session->unset_userdata('admin_sess');
		redirect('/admin/login');
	}

	public function dashboard() {
		$this->load->model('doctor_model');
		$this->load->model('patient_model');
		$this->load->model('company_model');
		$this->load->model('appointment_model');
//        $this->load->model('admin_model');
		$data = [];
		$data['doctors_count'] = $this->doctor_model->get_all_doctors_count();
		$data['patients_count'] = $this->patient_model->get_all_patient_count();
		$data['companies_count'] = $this->company_model->getList_count();
		$data['appointments_count'] = $this->appointment_model->getAll_count();
//        $data['admins_count'] = $this->admin_model->getAll_count();
		/* echo $doctors_count;
		  die(); */
		$this->load->view('admin/header');
		$this->load->view('admin/dashboard/index', $data);
		$this->load->view('admin/footer');
	}

	public function index() {
		$this->dashboard();
		/* $this->load->view('admin/header');
		  $this->load->view('admin/dashboard/index');
		  $this->load->view('admin/footer'); */
	}

}
