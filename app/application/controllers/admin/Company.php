<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Company extends Auth_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_access_check();
        $this->load->model('company_model');
    }
	
	function index()
	{
		var_debug(__METHOD__);
	}

    private function pagination_setup($setup_data = []){
        $this->load->library('pagination');
        $limit = $setup_data['limit'];
        $config = [];
        $config['reuse_query_string'] = FALSE;
        $config['total_rows'] = $setup_data['count'];

        //bootstrap changes
        $config['full_tag_open'] = '<nav aria-label="Page navigation" style="text-align: center;"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class=" page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class=" page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class=" page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');

        //pagination value
        $config['base_url'] = base_url() . $setup_data['url'];
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    public function companies_list($page = 0){
        $data = [];
        $config = [
            'limit' => 20,
            'count' => $this->company_model->getList_count(),
            'url' => 'admin/companies_setup/companies_list'
        ];
        $data['companies'] = $this->company_model->getList($config['limit'], $page);
        $data['pagination_link'] = $this->pagination_setup($config);
        $this->load->model('doctor_model');
        $i = 0;
        foreach ($data['companies'] as $company){
            $data['companies'][$i]->number_of_doctors = $this->doctor_model->get_all_doctors_count($company->id);
            $i++;
        }
        /*echo "<pre>";
        print_r($data);
        die();*/
        $this->load->view('admin/header');
        $this->load->view('admin/companies_setup/companies_list', $data);
        $this->load->view('admin/footer');
    }

    public function add_company_ajax(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $company_name = $this->input->post('company_name');
        $company_code = $this->input->post('company_code');
        if($this->company_model->check_exists($company_name, $company_code)){
            echo json_encode([
                'message' => 'This company is already exists.',
                'bool' => false
            ]);
            return;
        }
        $company_id = $this->company_model->add([
            'name' => $company_name,
            'code' => $company_code
        ]);
        echo json_encode([
            'message' => 'New Company Added',
            'company_id' => $company_id,
            'bool' => true
        ]);
        return;
    }

    public function edit_company_ajax(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $company_id = $this->input->post('company_id');
        $company_name = $this->input->post('company_name');
        $company_code = $this->input->post('company_code');
        if($this->company_model->check_exists($company_name, $company_code)){
            echo json_encode([
                'message' => 'This company is already exists.',
                'bool' => false
            ]);
            return;
        }
        $this->company_model->update($company_id, [
            'name' => $company_name,
            'code' => $company_code
        ]);
        echo json_encode([
            'message' => 'Company Edited',
            'company_id' => $company_id,
            'company_name' => $company_name,
            'company_code' => $company_code,
            'bool' => true
        ]);
        return;
    }

    public function remove_company(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $companyid = $this->input->post('companyid');
        $this->company_model->remove($companyid);
        return true;
    }



}