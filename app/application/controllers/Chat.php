<?php


class Chat extends Auth_Controller
{

	function __construct() {
		parent::__construct();
	}

    public function index()
    {
        if ($this->session->userdata('patient') == 'patient') {
            $data = array();
            $this->load->view('header');
            $this->load->view('header_top');
            $this->load->view('nav');
            $data['contact_list'] = $this->login_model->find_users_for_chat($this->session->userdata('userid'));
            $this->load->view("chat/index", $data);
            $this->load->view("footer");
        } else {
            $data = array();
            $this->load->view('header');
            $this->load->view('doctor_header_top');
            $this->load->view('doctor_nav');
            $data['contact_list'] = $this->login_model->find_users_for_chat($this->session->userdata('userid'));
            $this->load->view("chat/index", $data);
            $this->load->view("footer");
        }

    }

    public function chat($partner_id)
    {

        if ($this->session->userdata('patient') == 'patient') {
            $data = array();
            $this->load->view('header');
            $this->load->view('header_top');
            $this->load->view('nav');
            $data['chat_partner'] = $this->login_model->find_chat_partner($partner_id);
            //$data['chat'] = $this->login_model->find_chat($partner_id);
            //echo json_encode($data);
            $this->load->view("chat/chat", $data);
            $this->load->view("footer");
        } elseif ($this->session->userdata('doctor') == 'doctor') {
            $data = array();
            $this->load->view('header');
            $this->load->view('doctor_header_top');
            $this->load->view('doctor_nav');
            $data['chat_partner'] = $this->login_model->find_chat_partner($partner_id);
            //$data['chat'] = $this->login_model->find_chat($partner_id);
            //echo json_encode($data);
            $this->load->view("chat/chat", $data);
            $this->load->view("footer");
        }

        //$partner_id = $this->input->post('contact_id');


    }

    public function ajax_save_messages()
    {
        $sender = $this->input->post('sender', true);
        $receiver = $this->input->post('receiver', true);
        $message = $this->input->post('message_body', true);
        //echo $receiver;
        $this->login_model->ajax_message_save($receiver, $sender, $message);
        //$this->chat($partner_id);
    }

    public function ajax_get_chat_messages()
    {
        $sender_id = $this->input->post('sender_user_id', true);
        $reciever_id = $this->input->post('reciever_user_id', true);
        $chat_messages = $this->login_model->ajax_getting_chat_messages($sender_id, $reciever_id);
        //print_r($chat_messages);
        //if($chat_messages->num_rows() >0){
        if (is_array($chat_messages)) {

            $chat_messages_html = '';
            foreach ($chat_messages as $chat_message) {
                //echo $chat_message->messages;
                $user = '';
//                if($chat_message->from_userid==$this->session->userdata('userid')){
//                    $user = $this->session->userdata('userid');
//                }else{
//                    $user = $chat_message->to_userid;
//                }

                $chat_messages_html .= '<div class="chat-message">';

                $chat_messages_html .= '<div class="chat-message-photo">';
                $chat_messages_html .= '<img src="img/photo-64-1.jpg" alt="">';
                $chat_messages_html .= '</div>';
                $chat_messages_html .= '<div class="chat-message-header">';
                $chat_messages_html .= '<div class="tbl-row">';
                $chat_messages_html .= '<div class="tbl-cell tbl-cell-name">' . $chat_message->user_name;
                $chat_messages_html .= '</div>';

                $chat_messages_html .= '<div class="tbl-cell tbl-cell-date">' . $chat_message->time_sent;
                $chat_messages_html .= '</div>';
                $chat_messages_html .= '</div>';
                $chat_messages_html .= '</div>';

                $chat_messages_html .= '<div class="chat-message-content">';
                $chat_messages_html .= '<div class="chat-message-txt">' . $chat_message->messages;
                $chat_messages_html .= '</div>';
                $chat_messages_html .= '</div>';
                $chat_messages_html .= '</div>';
                //$result = array('chat_status'=>'ok', 'content'=>$chat_messages_html);
                //echo json_encode($result);

                //exit();
            }
            //echo $chat_messages_html;
            $result = array('chat_status' => 'ok', 'content' => $chat_messages_html);
            echo json_encode($result);
        } else {
            $result = array('chat_status' => 'no', 'content' => 'No message yet to be shared!');
            echo json_encode($result);
            exit();
        }
    }

    public function save_messages()
    {
        $sender = $this->input->post('sender_id', true);
        $receiver = $this->input->post('receiver_id', true);
        $message = $this->input->post('message_body', true);
        $this->login_model->message_save($receiver, $sender, $message);
        $this->chat($receiver);
        //$this->chat($partner_id);
    }


}