<?php


use Stripe\Stripe;
use Stripe\Customer;

/**
 * Class Patient
 *
 * @property CI_Loader $load
 * @property Login_model $login_model
 * @property State_Model $state_model
 * @property Patient_model $patient_model
 * @property Timezone_Model $timezone_model
 *
 */
class Patient extends Auth_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->patient_access_check();

        $this->load->model('state_model');
        $this->load->model('patient_model');
        $this->load->model('timezone_model');
        $this->load->model('reason_model');
        $this->load->model('appointment_model');
        $this->load->model('doctor_model');
        $this->load->model('consultationtype_model');

        $this->config->load('stripe');
    }

    public function index()
    {
        /*echo "yes";
        die();*/
        $patient_id = $this->session->userdata('userid');
        $result['states'] = $this->state_model->getList();
        $result['specialties'] = $this->doctor_model->getSpecialtyList();
        $result['health'] = $this->login_model->get_patient_health_condition("health", $patient_id, 10, 0);
        $result['medication'] = $this->login_model->get_patient_health_condition("medication", $patient_id, 10, 0);
        $result['allergies'] = $this->login_model->get_patient_health_condition("allergies", $patient_id, 10, 0);
        $result['surgery'] = $this->login_model->get_patient_health_condition("surgery", $patient_id, 10, 0);
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("home/index", $result);
        $this->load->view("footer");
    }

    public function records()
    {
        $this->load->model('File_model');
        $this->load->model('messages_model');
        $this->load->library('pagination');

        $data['recipient_list'] = $this->messages_model->getRecipients();

        $limit = 10;
        $page = $this->uri->segment(3) !== NULL ? $this->uri->segment(3) : 0;

        $search_text = trim($this->input->get('search', true));
        $data['search_text'] = $search_text;
        if (!empty($search_text)) {
            $data['search'] = 1;
            $data['documents'] = $this->File_model->search_files($limit, $page, $search_text);
            $config['reuse_query_string'] = TRUE;
            $config['total_rows'] = $this->File_model->search_files($limit, $page, $search_text, true);
        } else {
            $search_text = '';
            $data['search'] = 0;
            $data['documents'] = $this->File_model->files($limit, $page);
            $config['reuse_query_string'] = FALSE;
            $config['total_rows'] = $this->File_model->count_files();
        }

        //bootstrap changes
        $config['full_tag_open'] = '<nav aria-label="Page navigation" style="text-align: center;"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class=" page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class=" page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class=" page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
//         $config['display_pages'] = FALSE;
        $config['attributes'] = array('class' => 'page-link');

        //pagination value
        $config['base_url'] = base_url() . $this->uri->segment(1) . '/' . __FUNCTION__ . '/';
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['pagination_link'] = $this->pagination->create_links();

        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("records", $data);
        $this->load->view("footer");
    }

    public function providers()
    {
        $data['providers'] = $this->login_model->get_providers();
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("provider/index", $data);
        $this->load->view("footer");
    }

    public function add_to_fav()
    {
        $p_id = $this->input->post('p_id');
        $d_id = $this->input->post('d_id');
        $data = $this->login_model->add_to_fav($p_id, $d_id);
        echo $data;
    }

    public function add_to_favorite($is_loaded_from_view = true, $p_id = null, $d_id = null)
    {
        if ($is_loaded_from_view) {
            $p_id = $this->input->post('p_id');
            $d_id = $this->input->post('d_id');
        }
        $insert_id = $this->login_model->add_to_fav($p_id, $d_id);
        // echo $insert_id;
        $favorite = $this->login_model->get_added_favorite($insert_id);
        //print_r($favorite);
        $fav_html = '';
        $fav_html .= '<button class="btn btn-primary-outline remove_from_favorite" href="#"
                                               data-patientid="' . $this->session->userdata("userid") . '"
                                               data-doctorid="' . $favorite->d_id . '"
                                               data-toggle="tooltip" data-placement="top"
                                               title="Click to remove to My Physicians list." style="color: #fff !important;"><i class="font-icon fa fa-stethoscope"></i> Remove from My Physicians</button>';
        if ($is_loaded_from_view) {
            echo json_encode($fav_html);
        }
    }

    public function remove_from_favorite()
    {
        $p_id = $this->input->post('p_id');
        $d_id = $this->input->post('d_id');
        $data = $this->login_model->remove_from_favorite($p_id, $d_id);
        $fav_html = '';
        $fav_html .= '<button class="btn btn-primary-outline add_to_favorite" href="#"
                                               data-patientid="' . $this->session->userdata("userid") . '"
                                               data-doctorid="' . $d_id . '"
                                               data-toggle="tooltip" data-placement="top"
                                               title="Click to add to My Physicians list."><i class="font-icon fa fa-stethoscope"></i> Add To My Physicians</button>';
        echo json_encode($fav_html);
    }

    public function remove_from_fav()
    {
        $d_id = $this->input->post('d_id');
        $data = $this->login_model->remove_from_fav($d_id);
        if ($data) {
            $result = array(
                'text' => "Removed From Provider",
                'decision' => TRUE,
            );
            echo json_encode($result);
        } else {
            $result = array(
                'text' => "Something Wrong",
                'decision' => FALSE,
            );
            echo json_encode($result);
        }
    }

    public function ajax_payment_appointment_modal()
    {
        if ($this->input->is_ajax_request()) {

            $data = array();
            $me = $this->session->userdata('userid');

            $appointment_id = $this->input->post('appointment_id');

            //now get appointment info
            $appointment = $this->appointment_model->get_appointment_by_id($appointment_id);

            if (is_object($appointment)) {
                //now check security
                //1. check this patient appointment
                if ($appointment->patients_id != $me) {
                    $data['error'][] = 'Invalid appointment id. This appointment is not assigned to you.';
                }

                //2. check appointment is pending
                if ($appointment->status != '1') {
                    $data['error'][] = 'Invalid appointment id. Appointment status is not pending.';
                }

                //3. check for past appointment
                $current_time = time();
                if ($appointment->end_date_stamp < $current_time) {
                    $data['error'][] = 'Invalid appointment id. Selected appointment is in the past.';
                }

                //now do actual work
                if (!isset($data['error'])) {
                    $data['appointment'] = $appointment;
                    $data['patient'] = $this->patient_model->getById($appointment->patients_id);
                    $data['doctor'] = $this->doctor_model->get_doctor($appointment->doctors_id);
                    $data['reasons'] = $this->reason_model->getList();
                    $data['id'] = $appointment->doctors_id;

                    $data['consultation_types'] = $this->consultationtype_model->getByDoctorList($appointment->doctors_id);
                    //get consultation type data
                    if (is_numeric($appointment->consultation_type) && $appointment->consultation_type > 0) {
                        //get data from table
                        $consultation_type_data = $this->consultationtype_model->get_consultation_by_id($appointment->consultation_type);
                        if (is_object($consultation_type_data) && isset($consultation_type_data->id)) {
                            $data['consultation_type_data'] = $consultation_type_data;
                        }

                        //check for billing waived
                        $billing_waived = $this->patient_model->patient_check_waive($appointment->doctors_id);
                        if ($billing_waived > 0) {
                            $data['billing_waived'] = $billing_waived;
                        }
                    }

                }

            } else {
                $data['error'][] = 'Invalid Appointment.';
            }

            $data['calling_from_ajax'] = true;

            //return view
            $this->load->view('appointment/_popup_create2', $data);


        } else {
            show_error('Invalid Request.');
        }
    }

    public function ajax_appointment_for_doctor_single()
    {
        if ($this->input->is_ajax_request()) {
            $data = array();
            $doctor_id = $this->input->post('doctor_id', true);
            //now do actual work
            if (is_numeric($doctor_id) && $doctor_id > 0) {
                $data['patient'] = $this->patient_model->getById();
                $data['doctor'] = $this->doctor_model->get_doctor($doctor_id);
                $data['reasons'] = $this->reason_model->getList();
                $data['doctor_available_time'] = $this->doctor_model->getDoctorAvailableDate($doctor_id);
                $data['id'] = $doctor_id;
                $data['consultation_types'] = $this->consultationtype_model->getByDoctorList($doctor_id);
                $billing_waived = $this->patient_model->patient_check_waive($doctor_id);
                if ($billing_waived > 0) {
                    $data['billing_waived'] = $billing_waived;
                }
            }

            $data['calling_from_ajax'] = true;

            //return view
            $this->load->view('appointment/_popup_create2', $data);

        } else {
            show_error('Invalid Request.');
        }
    }

    public function ajax_payment_appointment_modal2()
    {
        if ($this->input->is_ajax_request()) {
            $data = array();

            //now do actual work
            if (!isset($data['error'])) {
                $data['reasons'] = $this->reason_model->getList();
                $data['doctors'] = $this->patient_model->get_patient_doctors();
                $data['patient'] = $this->patient_model->getById();
            }

            $data['calling_from_ajax'] = true;

            //$data['waived'] = $this->patient_model->patient_check_waive()

            //return view
            $this->load->view('appointment/_popup_create2', $data);

        } else {
            show_error('Invalid Request.');
        }
    }

    public function send_contact_message_to_doctor()
    {
        $me = $this->session->userdata('userid');
        $doctor_id = $this->input->post('receiver_id');
        $message = $this->input->post('message_text');
        $data = array(
            'from_userid' => $me,
            'to_userid' => $doctor_id,
            'subject' => 'Contact Request from ' . $this->session->userdata('full_name'),
            'message' => $message,
            'added' => time(),
            'draft' => 0
        );
        $this->load->model('messages_model');
        $message_id = $this->messages_model->addMessage($data);
        if (!$message_id) {
            echo json_encode([
                'message' => 'Your message is not sent to ' . $this->session->userdata('full_name') . '. Try again.',
                'bool' => false
            ]);
        }
        echo json_encode([
            'message' => 'Your message is sent to ' . $this->session->userdata('full_name') . ' successfully.',
            'bool' => true
        ]);
    }

    public function doctorinfo($id)
    {
        $data['patient'] = $this->patient_model->getById();
        $data['doctor'] = $this->doctor_model->get_doctor($id);
        $data['specials'] = $this->doctor_model->get_doctors_specialization($id);
        $data['degrees'] = $this->doctor_model->get_doctors_degree($id);
        $data['experiences'] = $this->doctor_model->get_doctors_experience($id);
        $data['educations'] = $this->doctor_model->get_doctors_education($id);
        $data['languages'] = $this->doctor_model->get_doctors_language($id);
        $data['awards'] = $this->doctor_model->get_doctors_awards($id);
//        $data['licenced_states'] = $this->doctor_model->get_doctors_licenced_states($id);
        $data['licenced_states'] = $this->login_model->get_doctors_other_licenced_states($id);
        $data['memberships'] = $this->doctor_model->get_doctors_professiona_membership($id);
        $data['fav'] = $this->login_model->get_favorite($id);
        $data['reasons'] = $this->reason_model->getList();
        $data['doctor_available_time'] = $this->doctor_model->getDoctorAvailableDate($id);

        $data['id'] = $id;
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("patient/doctor_profile_for_appointment", $data);
        $this->load->view("footer", [
            'customScript' => ['https://js.stripe.com/v2/']
        ]);
        /*
                $fname = $this->input->post('fname');
                $lname = $this->input->post('lname');
                $email = $this->input->post('email');
                $gender = $this->input->post('gender');
                $dob = $this->input->post('dob');
                $timezone = $this->input->post('timezone');
                $state = $this->input->post('state');
                $city = $this->input->post('city');
                $zip = $this->input->post('zip');
                $phone_home = $this->input->post('phone-office');
                $phone_mobile = $this->input->post('phone-mobile');
                $fax = $this->input->post('fax');
                $pre = $this->input->post('pre_education');
                //$intern = $this->input->post('intern');
                //$country = $this->input->post('country');
                $pratice = $this->input->post('practicing');
                $residency = $this->input->post('residency');
                $dept = $this->input->post('dept');
                $specialty = $this->input->post('specialty');
                $s_specialty = $this->input->post('s_specialty');
                $m_license = $this->input->post('m_license');
                $s_license = $this->input->post('s_license');



                $config['upload_path'] = 'uploads/doctors/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = 1024;
                $config['max_width'] = 1024;
                $config['max_height'] = 768;
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('doctor_image')) {
        //            $error = array('data' => $this->upload->display_errors());
                    //$this->load->view('profile/file', $error);
                    $image_url = $this->input->post('doctor_image_db');
                } else {
                    $fdata = $this->upload->data();
                    $image_url = $config['upload_path'] . $fdata['file_name'];
                }

                $data = array(
                    'firstname' => $fname,
                    'lastname' => $lname,
                    'email' => $email,
                    'timezone' => $timezone,
                    'gender' => $gender,
                    'dob' => $dob,
                    'state' => $state,
                    'city' => $city,
                    'zip' => $zip,
                    'phone_home' => $phone_home,
                    'phone_mobile' => $phone_mobile,
                    'fax' => $fax,
                    'pre_medical_education' => $pre,
                    'practicing_since' => $pratice,
                    'residency' => $residency,
                    'department' => $dept,
                    'specialty' => $specialty,
                    'sub_specialty' => $s_specialty,
                    'npi' => $m_license,
                    'state_license' => $s_license,
                    'image_url' => $image_url
                );


                if ($this->input->post("save") == "1") {
                    $this->db->where('userid', $id);
                    $this->db->update('doctor', $data);
                }
                $data['patients'] = $this->login_model->get_patients();
                $data['doctors'] = $this->login_model->get_doctors($id);
                $data['availabilities'] = $this->login_model->get_doctors_available($id, date("m"), date("Y-m-d"));
                $data['specials'] = $this->login_model->get_doctors_specialization($id);
                $data['degrees'] = $this->login_model->get_doctors_degree($id);
                $data['experiences'] = $this->login_model->get_doctors_experience($id);
                $data['educations'] = $this->login_model->get_doctors_education($id);
                $data['languages'] = $this->login_model->get_doctors_language($id);
                $data['languages'] = $this->login_model->get_doctors_language($id);
                $data['awards'] = $this->login_model->get_doctors_awards($id);
                $data['licenced_states'] = $this->login_model->get_doctors_licenced_states($id);
                $data['memberships'] = $this->login_model->get_doctors_professiona_membership($id);
                $data['fav'] = $this->login_model->get_favorite($id);
                $data['id'] = $id;
                $this->load->view('header');
                $this->load->view('doctor_header_top');
                $this->load->view('doctor_nav');
                $this->load->view("doctor/index", $data);
                $this->load->view("footer");
        */

        /*        $data = array();
                $data['doctors'] = $this->login_model->get_doctors($id);
                $data['specials'] = $this->login_model->get_doctors_specialization($id);
                $data['degrees'] = $this->login_model->get_doctors_degree($id);
                $data['experiences'] = $this->login_model->get_doctors_experience($id);
                $data['educations'] = $this->login_model->get_doctors_education($id);
                $data['languages'] = $this->login_model->get_doctors_language($id);
                $data['languages'] = $this->login_model->get_doctors_language($id);
                $data['awards'] = $this->login_model->get_doctors_awards($id);
                $data['memberships'] = $this->login_model->get_doctors_professiona_membership($id);
                $data['licenced_states'] = $this->login_model->get_doctors_licenced_states($id);
                $data['fav'] = $this->login_model->get_favorite($id);
                //$data['doctor'] = $this->login_model->get_doctor($doctor_id);
                $this->load->view('header');
                $this->load->view('header_top');
                $this->load->view('nav');
                $this->load->view("doctor/index",$data);
                $this->load->view("footer");*/

    }

    public function myhealth($tab = 'condition')
    {
        $styles['customStyles'] = 'template_file/css/patients_health.css';
        $this->load->view('header', $styles);
        $this->load->view('header_top');
        $this->load->view('nav');
        $patient_id = $this->session->userdata('userid');
        $conditions['get_patient'] = $this->patient_model->get([
            'user_id' => $patient_id,
            'is_health, is_allergies, is_medication, is_surgeries' => ''
        ])[0];
        $conditions['health'] = $this->login_model->get_patient_health_condition("health", $patient_id);
        $conditions['medication'] = $this->login_model->get_patient_health_condition("medication", $patient_id);
        $conditions['drugs'] = $this->login_model->get_drugs();
        $conditions['allergies'] = $this->login_model->get_patient_health_condition("allergies", $patient_id);
        $conditions['surgery'] = $this->login_model->get_patient_health_condition("surgery", $patient_id);
        $conditions['script'] = 'template_file/js/lib/typeahead/jquery.typeahead.min.js';
        $conditions['tab'] = $tab;
        $this->load->view("health/footer");
        $this->load->view("health/health", $conditions);
        $this->load->view("footer");
    }

    public function myhealth_actions()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $field = trim($this->input->post('field'));
        $value = $this->input->post('value');
        $this->patient_model->update($this->session->userdata('userid'), [
            'is_' . $field => $value
        ]);
    }

    public function update_health($c_id, $uid)
    {
        $id = $this->session->userdata("userid");
        $data['condition'] = $this->login_model->get_health($c_id);
        if ($uid == $id) {
            $this->load->view('header');
            $this->load->view('header_top');
            $this->load->view('nav');
            $this->load->view("health/update", $data);
            $this->load->view("footer");
        }
    }


    public function update_condition()
    {
        $condition_type = $this->input->post('condition_type');
        $condition_id = $this->input->post('condition_id');
        $condition_term = $this->input->post('condition_term');
        $condition_date = $this->input->post('condition_date');
        $condition_source = $this->input->post('condition_source');

        $condition_freq = $this->input->post('condition_freq');
        $freq_time = $this->input->post('freq_time');
        $dosage = $this->input->post('dosage');
        $condition_severity = $this->input->post('condition_severity');
        $reaction = $this->input->post('reaction');
        $id = $this->input->post('id');

        $data = array(
            'condition_term' => $condition_term,
            'date' => $condition_date,
            'source' => $condition_source,
            'dosage' => $dosage,
            'frequency' => $condition_freq,
            'time' => $freq_time,
            'severity' => $condition_severity,
            'reaction' => $reaction
        );
        $this->login_model->edit_health($condition_id, $condition_type, $data);
        redirect('/patient/myhealth');

        /*if ($this->db->update('health', $data)) {
            if ($this->input->post('redirect') == '1') {
                redirect('/patient/myhealth');
            }
        } else {
            $this->session->set_flashdata('message', 'Please Fill the information correctly!');
            $data['condition'] = $this->login_model->get_health($id);
            $this->update_health($id, $this->session->userdata('userid'));
            /*$this->load->view('header');
            $this->load->view('header_top');
            $this->load->view('nav');
            $this->load->view("health/update", $data);
            $this->load->view("footer");*/
        /*}*/
    }

    public function delete_health($c_id, $uid)
    {
        $res = $this->login_model->delete_condition($c_id, $uid);
        if ($res)
            redirect("/patient/myhealth");
    }

    /*public function appointments()
    {
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("patient/appointments");
        $this->load->view("footer");
    }*/

    public function appointments()
    {
        $pid = $this->session->userdata('userid');
        $data['pastAppointments'] = $this->patient_model->getPastAppointmentForPatient($pid);
        $data['futureAppointments'] = $this->patient_model->getFutureAppointmentForPatient($pid);
        $data['pendingAppointments'] = $this->patient_model->getPendingAppointmentForPatient($pid);
        $data['canceledAppointments'] = $this->patient_model->getCanceledAppointmentForPatient($pid);

        $data['reasons'] = $this->reason_model->getList();
        $data['doctors'] = $this->patient_model->get_patient_doctors();
        $data['patient'] = $this->patient_model->getById();

        $providers = $this->login_model->get_providers();
        $is_physician = false;
        if (!empty($providers)) {
            if (is_array($providers)) {
                if ($providers['general_info'] && is_array($providers['general_info'])) {
                    if (count($providers['general_info']) > 0) {
                        $is_physician = true;
                    }
                }
            }
        }
        $data['is_physician'] = $is_physician;

        $scriptData['scriptData'] = 'template_file/js/lib/pagination/pagination.min.js';
        $this->load->view('header', $scriptData);
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("patient/appointments", $data);
        $this->load->view("footer");
    }

    public function updateProfile()
    {
        $id = $this->session->userdata('userid');

        $email = $this->input->post('email');
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $gender = $this->input->post('gender');
        $dob = $this->input->post('dob');
        $timezone = $this->input->post('timezone');
//        $timezone = empty($timezone) ? null : $timezone;
        $city = $this->input->post('city');
        $address = $this->input->post('address');
        $country = $this->input->post('country');
        $optional_state = $this->input->post('optional_state');
        /*if($country != 'United States'){
            $optional_state = $this->input->post('optional_state');
        }*/


        $state = $this->input->post('state');
        $state = (!isset($state) && empty($state)) ? null : $state;
        $zip = $this->input->post('zip');
        $phone_mobile = $this->input->post('phone-mobile');
        $height_ft = $this->input->post('user_height_ft');
        $height_inch = $this->input->post('user_height_inch');
        $weight = $this->input->post('user_weight');


        $config['upload_path'] = 'uploads/patients/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $this->load->library('upload', $config);
        $result = [];
        $result['message'] = '';
        $result['src'] = '';
        $result['bool'] = false;
        $result['isByError'] = false;
//        if (!$this->upload->do_upload('patient_image')) {
        if (!$this->upload->do_upload('croppedImage')) {
            if ($this->input->post('patient_image_db')) {
                $error = $this->upload->display_errors();
                if ($error == "<p>The image you are attempting to upload doesn't fit into the allowed dimensions.</p>") {
                    $result['message'] = 'The image you are attempting to upload doesn\'t fit into the allowed dimensions. Width = 1024 and Height = 768.';
                    $result['src'] = $config['upload_path'] . $this->input->post('patient_image_db');
                } else {
                    $result['bool'] = true;
                }
            } else {
                $error = $this->upload->display_errors();
                if ($error == "<p>The image you are attempting to upload doesn't fit into the allowed dimensions.</p>") {
                    $result['message'] = 'The image you are attempting to upload doesn\'t fit into the allowed dimensions. Width = 1024 and Height = 768.';
                    $result['src'] = $config['upload_path'] . $this->input->post('patient_image_db');
                } else {
                    $result['bool'] = true;
                }
            }
            $image_url = $this->input->post('patient_image_db');

        } else {
            $fdata = $this->upload->data();
            $image_url = $config['upload_path'] . $fdata['file_name'];

//            $this->session->set_userdata('image', $image_url);
            $result['src'] = $image_url;
            $result['bool'] = true;
            $result['isByError'] = true;
        }
        $this->session->set_userdata('image', $image_url);

        $data = array(
            'email' => $email,
            'firstname' => $fname,
            'lastname' => $lname,
            'city' => $city,
            'address' => $address,
            'country' => $country,
            'optional_state' => $optional_state,
            'state_id' => $state,
            'zip' => $zip,
            'gender' => $gender,
            'dob' => $dob,
            'phone_mobile' => $phone_mobile,
            'height_ft' => $height_ft,
            'height_inch' => $height_inch,
            'weight' => $weight,
            'timezone' => $timezone,
            'image_url' => $image_url,
        );

        $this->patient_model->update($id, $data);

        if ($email != $this->session->userdata('useremail')) {
//            $this->load->model('user_model');
            $this->user_model->update([
                'user_email' => $email
            ], 'id', $id);
            $this->session->set_userdata('useremail', $email);
        }

        $this->session->set_userdata('full_name', $fname . ' ' . $lname);
        $this->session->set_userdata('username', $fname);
        $this->session->set_userdata('user_timezone', $timezone);

        if ($result['bool']) {
            $result['message'] = 'Profile updated';
        }
        echo json_encode($result);

    }

    public function profile()
    {

        $id = $this->session->userdata('userid');

        $this->load->model('country_model');

        $data = [];
        $data['user'] = $this->patient_model->getById($id);
        $data['states'] = $this->state_model->getList();
        $data['zones'] = $this->timezone_model->getList();
        $data['card'] = $this->patient_model->getLastCard($id);
        $data['familyMembers'] = $this->patient_model->getFamilyMembers($id);
        $data['countries'] = $this->country_model->find_all_countries();

        $data['stripePublishableKey'] = $this->config->item('publishable_key');

        /*echo "Working";
        die();*/

        $this->load->view('header', [
            'customStyles' => 'node_modules/cropperjs/dist/cropper.min.css',
            'scriptData' => 'node_modules/cropperjs/dist/cropper.min.js'
        ]);
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("profile/index", $data);
        $this->load->view("footer");
    }

    public function settings()
    {
//        $this->load->view('header');
//        $this->load->view('header_top');
//        $this->load->view('nav');
//        $this->load->view("patient/settings/header");
//        $this->load->view("patient/settings/main");
//        $this->load->view("patient/settings/footer");
//        $this->load->view("footer");
        redirect('/patient/change_password');
    }

    public function current_password_check($current_password)
    {
        if (!password_verify($current_password, $this->session->userdata('userpassword'))) {
            $this->form_validation->set_message('current_password_check', 'Current Password not match');
            return false;
        }
        return true;
    }

    public function change_password()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
        $config = array(
            array(
                'field' => 'current_password',
                'label' => 'Current Password',
                'rules' => 'trim|required|callback_current_password_check'
            ),
            array(
                'field' => 'password',
                'label' => 'New Password',
                'rules' => 'trim|required|min_length[8]'
            ),
            array(
                'field' => 'passconf',
                'label' => 'Password Confirmation',
                'rules' => 'trim|required|matches[password]'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            //load view
        } else {
            $password = $this->input->post('password', true);
            $update_array = array(
                'user_password' => password($password)
            );
            $where = array(
                'id' => $this->session->userdata('userid')
            );

            $updated = $this->login_model->update_password($update_array, $where);

            if ($updated) {
                $this->session->set_userdata('userpassword', $update_array['user_password']);
                $this->session->set_flashdata('success', 'Your Password Updated Successfully!');
                $this->load->library('email');
                $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
                $this->email->to($this->session->userdata('useremail'));
                $this->email->subject('Password Change');
                $this->email->message('Dear ' . $this->session->userdata('full_name') . ',
                <br />
                <br />
                Your password successfully changed. Your new password is <strong>' . $password . '</strong>
                <br />
                <br />
                <a href="http://www.myvirtualdoctor.com/app/login">Login Here</a>
                <br />
                <br />
                Thank You.');
                $this->email->send(true, true);
                redirect('login/logout?message=Your password has been changed, please login again.');
            }
        }
        if ($this->session->userdata('patient') == 'patient') {
            $this->load->view('header');
            $this->load->view('header_top');
            $this->load->view('nav');
            $this->load->view("patient/settings/header");
            $this->load->view("patient/settings/change_password");
            $this->load->view("patient/settings/footer");
            $this->load->view("footer");
        }

    }

    public function billing_information()
    {
        $id = $this->session->userdata('userid');
        $data['card'] = $this->patient_model->getLastCard($id);
        $data['stripePublishableKey'] = $this->config->item('publishable_key');
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("patient/settings/header");
        $this->load->view("patient/settings/billing_information", $data);
        $this->load->view("patient/settings/footer");
        $this->load->view("footer");
    }

    public function family_members()
    {
        $id = $this->session->userdata('userid');

        $data = [];
        /*$data['states'] = $this->state_model->getList();
        $data['zones'] = $this->timezone_model->getList();*/
        $data['familyMembers'] = $this->patient_model->getFamilyMembers($id);
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("patient/settings/header");
        $this->load->view("patient/settings/family_members", $data);
        $this->load->view("patient/settings/footer");
        $this->load->view("footer");
    }

    private function create_primary_physician($data = [])
    {
        $this->load->model('user_model');
        if ($this->user_model->email_exists($data['email'])) {
            $this->session->set_flashdata('error', 'This email is already exists.');
        } else {
            $this->load->library('email');
            $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
            $this->email->to($data['email']);

            $this->email->subject('Welcome to Myvirtualdoctor.com');
            $this->email->message('
<html>
<head>
    <title></title>
</head>
<body>
    <h4>Dear ' . $data['first_name'] . ' ' . $data['last_name'] . '</h4>
    <p></p>
    <p>Your account has been successfully created and you can now login to our doctor portal using the following information</p>
    <p></p>
    <p>Email: <strong>' . $data['email'] . '</strong></p>
    <p>Password: <strong>' . $data['password'] . '</strong></p>
    <p></p>
    <p>Click this link to login <a href="' . base_url() . 'login">' . base_url() . 'login</a></p>
    <p></p>
    <p>Please don\'t hesitate to contact us at  1-855-80-DOCTOR or at info@myvirtualdoctor.com for further assistance.</p>
    <p></p>
    <p>Sincerely,<br />
My Virtual Doctor</p>
</body>
</html>');
            if (!$this->email->send(true, true)) {
                $this->session->set_flashdata('error', 'There is some problem while mailing to you. Please try again.');
            }
            $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
            $this->email->to('info@myvirtualdoctor.com');

            $this->email->subject('New Doctor Signup (' . $data['first_name'] . ' ' . $data['last_name']);
            $this->email->message('Dear Administrator,
            <br />
            <br />
            A doctor just signed up to the system and here is the information:
            <br />
            <br />
            First Name: ' . $data['first_name'] . '
            <br />
            Last Name: ' . $data['last_name'] . '
            <br />
            Email: ' . $data['email'] . '
            <br />
            <br />
            Thank You.');

            if (!$this->email->send(true, true)) {
                $this->session->set_flashdata('error', 'There is some problem while mailing to admin. Please try again.');
            }
            $result = $this->doctor_model->createDoctor(
                $data['first_name'],
                $data['last_name'],
                $data['email'],
                $data['password'],
                0,
                $data['state'],
                $data['phone_number'],
                $data['city'],
                $data['zip'],
                $this->session->userdata('userid')
            );
            if ($result) {
                $this->session->set_flashdata('success', 'An email has been sent to provided email with physician\'s login and password. Once you receive the email please click <a href="' . site_url('login') . '">here</a> to login to the doctor portal.');
            } else {
                $this->session->set_flashdata('error', 'There is some problem while inserting physician data. Please try again.');
            }
        }
    }

    public function primary_physician()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
        $config = array(
            array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'phone_number',
                'label' => 'Phone Number',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'city',
                'label' => 'City',
                'rules' => 'trim|required'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $data['is_error'] = true;
        } else {
            $data['is_error'] = false;
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $password = random_string();
            $phone_number = $this->input->post('phone_number');
            $city = $this->input->post('city');
            $state = $this->input->post('state');
            if (!$state) {
                $state = null;
            }
            $zip = $this->input->post('zip');
            if ($this->input->post('is_update') == 1) {
//                update
                $user_id = $this->input->post('user_id');
                $this->load->model('user_model');
                if ($this->user_model->email_exists($email, $user_id)) {
                    $this->session->set_flashdata('error', 'This email is already exists.');
                } else {
                    if ($email == $this->input->post('old_email')) {
                        // update
                        $this->doctor_model->updateDoctor([
                            'firstname' => $first_name,
                            'lastname' => $last_name,
                            'phone_mobile' => $phone_number,
                            'city' => $city,
                            'state_id' => $state,
                            'zip' => $zip,
                        ], [
                            'userid' => $user_id
                        ]);
                        $this->session->set_flashdata('success', 'Primary Physician has been updated.');
                    } else {
                        // add new and remove parent_id from last one
                        $this->create_primary_physician([
                            'email' => $email,
                            'first_name' => $first_name,
                            'last_name' => $last_name,
                            'password' => password($password),
                            'state' => $state,
                            'phone_number' => $phone_number,
                            'city' => $city,
                            'zip' => $zip
                        ]);
                        $this->user_model->update([
                            'parent_id' => null
                        ], 'id', $user_id);
                        $this->session->set_flashdata('success', 'Primary Physician has been added.');
                    }
                }
            } elseif ($this->input->post('is_update') == 0) {
                $this->create_primary_physician([
                    'email' => $email,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'password' => password($password),
                    'state' => $state,
                    'phone_number' => $phone_number,
                    'city' => $city,
                    'zip' => $zip
                ]);
                $this->session->set_flashdata('success', 'Primary Physician has been added.');
            }

        }
        $data = [];
        $data['states'] = $this->state_model->getList();
        $data['primary_physician'] = $this->doctor_model->get_doctor_by_patient_id();
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("patient/settings/header");
        $this->load->view("patient/settings/primary_physician", $data);
        $this->load->view("patient/settings/footer");
        $this->load->view("footer");
    }

    public function addCard()
    {
        $userId = $this->session->userdata('userid');
        $patient = $this->patient_model->getById($userId);

        $token = $this->input->post('token');
        $cardName = $this->input->post('card_name');

        $apiKey = $this->config->item('api_key');
        Stripe::setApiKey($apiKey);

        $customer = Customer::create([
            'source' => $token,
            'description' => $patient->firstname . " " . $patient->lastname,
            'email' => $patient->email,
        ]);

        $this->patient_model->addCard($userId, $cardName, $customer->id);

        $lastCard = $this->patient_model->getLastCard($userId);

        echo $lastCard->id;
    }

    public function deleteCard()
    {
        $cardId = $this->input->post('card_id');
        $userId = $this->session->userdata('userid');

        $this->patient_model->deleteCardById($userId, $cardId);
    }

    public function addFamilyMember()
    {
        $id = $this->session->userdata('userid');

        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $email = $this->input->post('email');
//        $address = $this->input->post('address');
//        $city = $this->input->post('city');
//        $state = $this->input->post('state');
//        $state = empty($state) ? null : $state;
//        $zip = $this->input->post('zip');
//        $dob = $this->input->post('dob');
//        $gender = $this->input->post('gender');
//        $timezone = $this->input->post('timezone');
//        $timezone = empty($timezone) ? null : $timezone;
//        $phone_mobile = $this->input->post('phone-mobile');

        $data = array(
            'firstname' => $fname,
            'lastname' => $lname,
            'email' => $email
//            'address' => $address,
//            'city' => $city,
//            'state_id' => $state,
//            'zip' => $zip,
//            'dob' => $dob,
//            'gender' => $gender,
//            'timezone_id' => $timezone,
//            'phone_mobile' => $phone_mobile,
        );

        $this->load->model('user_model');
        if ($this->user_model->email_exists($email)) {
            echo json_encode([
                'message' => 'This email is already exists. Try other',
                'bool' => false
            ]);
            return;
        }

        $result = $this->patient_model->createFamilyMember($id, $data);

        if (!$result) {
            echo json_encode([
                'message' => 'Error creating',
                'bool' => false
            ]);
            return;
            $this->session->set_flashdata('success', 'Family member successfully created');
        }
        echo json_encode([
            'message' => 'Family member successfully created',
            'user_id' => $result,
            'bool' => true
        ]);
        return;

    }

    public function edit_family_member()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $user_id = $this->session->userdata('userid');

        $fname = $this->input->post('fname_edit');
        $lname = $this->input->post('lname_edit');
        $email = $this->input->post('email_edit');
        $patient_id = $this->input->post('user_id');
        $this->load->model('user_model');
        if ($this->user_model->email_exists($email, $patient_id)) {
            echo json_encode([
                'message' => 'This email is already exists. Try other',
                'bool' => false
            ]);
            return;
        }
        $this->patient_model->update($patient_id, [
            'firstname' => $fname,
            'lastname' => $lname
        ]);
        $this->user_model->update([
            'user_email' => $email
        ], 'id', $patient_id);

        echo json_encode([
            'message' => 'Family member successfully updated',
            'bool' => true
        ]);
        return;
    }

    public function removeFamilyMember()
    {
        $memberID = $this->input->post('id');
        if ($memberID) {
            $this->patient_model->removeFamilyMember($memberID);
        }
    }

    function upcoming_appointment()
    {
        $pid = $this->session->userdata('userid');

        $visitor_time = time();

        $time = date('Y-m-d H:i:s');
        $coming_time = date('Y-m-d H:i:s', strtotime("+5 minutes", $visitor_time));

        $upcoming_ppointment = $this->patient_model->getUpcomingAppointmentForPatient($pid, $coming_time, $time);
        $current_ppointment = $this->patient_model->getCurrentAppointmentForDoctor($pid, $time);
        $diff = '';
        $is_delayed = false;
        $is_action = false;
        $this->load->model('appointmentlog_model');
        if ($upcoming_ppointment) {
            if ($this->appointmentlog_model->check_exists([
                'appointment_log.appointment_id' => $upcoming_ppointment->id,
                'appointment_log.status !=' => 'cancelled'
            ])
            ) {
                $is_action = true;
                $appointmentlog_data = $this->appointmentlog_model->getByFields([
                    'appointment_log.appointment_id' => $upcoming_ppointment->id,
                    'appointment_log.status !=' => 'cancelled'
                ]);
                if ($appointmentlog_data[0]->status == 'delayed') {
                    $line = __LINE__;
                    $is_delayed = true;
                    $diff = date_diff(date_create(date('Y-m-d H:i:s', $appointmentlog_data[0]->stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));

                } else if ($appointmentlog_data[0]->status == 'accepted') {
                    $line = __LINE__;
                    $diff = date_diff(date_create(date('Y-m-d H:i:s', $upcoming_ppointment->start_date_stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));
                    $line = __LINE__;
                }
            } else {
                $line = __LINE__;
                $diff = date_diff(date_create(date('Y-m-d H:i:s', $upcoming_ppointment->start_date_stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));
            }
            $upcoming_ppointment->start_time = date('g:i A', ($upcoming_ppointment->start_date_stamp));
            $upcoming_ppointment->end_time = date('g:i A', ($upcoming_ppointment->end_date_stamp));
            $upcoming_ppointment->hover_text = 'Doctor Name: ' . $upcoming_ppointment->firstname . " " . $upcoming_ppointment->lastname . "\nAppointment Reason: " . $upcoming_ppointment->primary_symptom . "\nAppointment Time: " . $upcoming_ppointment->start_time;
        } else if ($current_ppointment) {
            if ($this->appointmentlog_model->check_exists([
                'appointment_log.appointment_id' => $current_ppointment->id,
                'appointment_log.status !=' => 'cancelled'
            ])
            ) {
                $upcoming_ppointment = $current_ppointment;
                $is_action = true;
                $appointmentlog_data = $this->appointmentlog_model->getByFields([
                    'appointment_log.appointment_id' => $upcoming_ppointment->id,
                    'appointment_log.status !=' => 'cancelled'
                ]);
                if ($appointmentlog_data[0]->status == 'delayed') {
                    $line = __LINE__;
                    $is_delayed = true;
                    if ($appointmentlog_data[0]->stamp > $visitor_time) {
                        $diff = date_diff(date_create(date('Y-m-d H:i:s', $appointmentlog_data[0]->stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));
                    } else {
                        $diff = '0';
                    }
                } else if ($appointmentlog_data[0]->status == 'accepted') {
                    $line = __LINE__;
                    $diff = date_diff(date_create(date('Y-m-d H:i:s', $upcoming_ppointment->start_date_stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));
                    $diff = '0';
                }
                $upcoming_ppointment->start_time = date('g:i A', ($upcoming_ppointment->start_date_stamp));
                $upcoming_ppointment->end_time = date('g:i A', ($upcoming_ppointment->end_date_stamp));
                $upcoming_ppointment->hover_text = 'Doctor Name: ' . $upcoming_ppointment->firstname . " " . $upcoming_ppointment->lastname . "\nAppointment Reason: " . $upcoming_ppointment->primary_symptom . "\nAppointment Time: " . $upcoming_ppointment->start_time;
            } else {
                $line = __LINE__;
                $diff = '0';
            }
        } else {
            if ($this->appointmentlog_model->check_exists([
                'appointment.patients_id' => $pid,
                "FROM_UNIXTIME(appointment_log.stamp, '%Y-%m-%d %H:%i:%s') <=" => date('Y-m-d H:i:s', strtotime("+15 minutes", $visitor_time)),
                "FROM_UNIXTIME(appointment_log.stamp, '%Y-%m-%d %H:%i:%s') >=" => $time,
                'appointment_log.status' => 'delayed',
                'appointment_log.status !=' => 'cancelled'
            ])
            ) {
                $line = __LINE__;
                $is_action = true;
                $appointmentlog_data = $this->appointmentlog_model->getByFields([
                    'appointment.patients_id' => $pid,
                    "FROM_UNIXTIME(appointment_log.stamp, '%Y-%m-%d %H:%i:%s') <=" => date('Y-m-d H:i:s', strtotime("+15 minutes", $visitor_time)),
                    "FROM_UNIXTIME(appointment_log.stamp, '%Y-%m-%d %H:%i:%s') >=" => $time,
                    'appointment_log.status' => 'delayed',
                    'appointment_log.status !=' => 'cancelled'
                ]);
                $is_delayed = true;
                $diff = date_diff(date_create(date('Y-m-d H:i:s', $appointmentlog_data[0]->stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));
                $line = __LINE__;
                $upcoming_ppointment = $this->appointment_model->get_appointment_by_id_for_consultation_notification($appointmentlog_data[0]->appointment_id, 'patient');
                $upcoming_ppointment->start_time = date('g:i A', ($upcoming_ppointment->start_date_stamp));
                $upcoming_ppointment->end_time = date('g:i A', ($upcoming_ppointment->end_date_stamp));
                $upcoming_ppointment->hover_text = 'Doctor Name: ' . $upcoming_ppointment->firstname . " " . $upcoming_ppointment->lastname . "\nAppointment Reason: " . $upcoming_ppointment->primary_symptom . "\nAppointment Time: " . $upcoming_ppointment->start_time;
            } else if ($this->appointmentlog_model->check_exists([
                'appointment.patients_id' => $pid,
                'appointment_log.status' => 'accepted',
                "FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') <=" => date('Y-m-d H:i:s', strtotime("+5 minutes", $visitor_time)),
                "FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') >=" => $time,
                'appointment_log.status !=' => 'cancelled'
            ])
            ) {
                $line = __LINE__;
                $is_action = true;
                $appointmentlog_data = $this->appointmentlog_model->getByFields([
                    'appointment.patients_id' => $pid,
                    'appointment_log.status' => 'accepted',
                    'appointment_log.status !=' => 'cancelled'
                ]);
                $upcoming_ppointment = $this->appointment_model->get_appointment_by_id_for_consultation_notification($appointmentlog_data[0]->appointment_id, 'patient');
                $diff = date_diff(date_create(date('Y-m-d H:i:s', $upcoming_ppointment->start_date_stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));

                $upcoming_ppointment->start_time = date('g:i A', ($upcoming_ppointment->start_date_stamp));
                $upcoming_ppointment->end_time = date('g:i A', ($upcoming_ppointment->end_date_stamp));
                $upcoming_ppointment->hover_text = 'Doctor Name: ' . $upcoming_ppointment->firstname . " " . $upcoming_ppointment->lastname . "\nAppointment Reason: " . $upcoming_ppointment->primary_symptom . "\nAppointment Time: " . $upcoming_ppointment->start_time;
            } else {
                $line = __LINE__;
            }
        }
        $diff_text = '';
        if (!is_string($diff)) {
            $minutes = $diff->format('%i');
            $seconds = $diff->format('%s');
            $diff_text = ($minutes * 60) + $seconds;
        } else {
            $diff_text = $diff;
        }
        $this->load->model('knocking_model');
        $knocking_data = $this->knocking_model->check_doctor_availability($pid);
        $knocking_data_bool = false;
        $knocking_data_id = 0;
        $knocking_data_appointment_id = 0;
        if($knocking_data){
            $knocking_data_bool = $knocking_data['bool'];
            $knocking_data_id = $knocking_data['id'];
            $knocking_data_appointment_id = $knocking_data['appointment_id'];

        }
        $data_send = json_encode([
            'appointment_info' => $upcoming_ppointment,
            'coming_time' => $coming_time,
            'time' => $time,
            'diff' => $diff_text,
            'is_delayed' => $is_delayed,
            'is_action' => $is_action,
            'knocking_data' => [
                'is_doctor_in_video_page' => $knocking_data_bool,
                'knocking_id' => $knocking_data_id,
                'appointment_id' => $knocking_data_appointment_id,
                'is_consultation_start' => (($knocking_data_appointment_id != 0) ? $this->is_doctor_ready_for_consultation($knocking_data_appointment_id, false) : false)
            ],
            'line' => $line
        ]);
        echo $data_send;
    }

    public function update_appointment_timing($appointment_id){
        $time = time();
        $endTime = strtotime("+30 minutes", $time);
        $this->appointment_model->updateById($appointment_id, [
            'start_date_stamp' => $time,
            'end_date_stamp' => $endTime
        ]);
    }

    public function approved_knocking_notification($knocking_id){
        if($knocking_id){
            $this->load->model('knocking_model');
            $this->knocking_model->patient_approved($knocking_id);
        }
    }

    public function is_doctor_ready_for_consultation($a_id, $is_request = true)
    {
        $pid = $this->session->userdata('userid');
        if(!$is_request){
            return $this->patient_model->upcoming_appointment_is_doctor_status($pid, $a_id, 1);
        }
        echo json_encode([
            'bool' => $this->patient_model->upcoming_appointment_is_doctor_status($pid, $a_id, 1)
        ]);
        return;
    }

    public function is_doctor_end_consultation($a_id)
    {
        $pid = $this->session->userdata('userid');
        echo json_encode([
            'bool' => $this->patient_model->upcoming_appointment_is_doctor_status($pid, $a_id, 2)
        ]);
        return;
    }

    public function add_start_session_time($al_id)
    {
        $this->load->model('appointmentlog_model');
        $current_time = time();
        $this->appointmentlog_model->update($al_id, [
            'session_start_stamp' => $current_time
        ]);
    }


    function check_appointment_log_status()
    {
        $a_id = $this->input->post('id', true);
        $stamp = $this->input->post('stamp', true);
        $this->load->model('appointmentlog_model');
        $diff = '';
        $status = '';
        $msg = '';
        $reason = '';
        if ($this->appointmentlog_model->check_exists([
            'appointment_log.appointment_id' => $a_id,
        ])
        ) {
            $appointmentlog_data = $this->appointmentlog_model->getByFields([
                'appointment_log.appointment_id' => $a_id,
            ]);
            if ($appointmentlog_data[0]->status == 'delayed') {
                $status = 'delayed';
                $diff = date_diff(date_create(date('Y-m-d H:i:s', $appointmentlog_data[0]->stamp)), date_create(date('Y-m-d H:i:s', time())));
                $msg = 'Your appointment start time has been delayed by the doctor, thank you for your patience';
                $reason = $appointmentlog_data[0]->reason;
            } else if ($appointmentlog_data[0]->status == 'cancelled') {
                $status = 'cancelled';
                $msg = 'Your appointment has been cancelled by the doctor.';
                $reason = $appointmentlog_data[0]->reason;
            } else {
                $status = 'nothing';
            }
        } else {
            $status = 'nothing';
        }
        $diff_text = '';
        if (!is_string($diff)) {
            $minutes = $diff->format('%i');
            $seconds = $diff->format('%s');
            $diff_text = ($minutes * 60) + $seconds;
        } else {
            $diff_text = '';
        }
        echo json_encode([
            'status' => $status,
            'diff' => $diff_text,
            'msg' => $msg,
            'reason' => $reason
        ]);


//		$this->db->where('appointment_id', $id);
//		$this->db->order_by('created_at', 'DESC');
//		$this->db->limit(1);
//		$check = $this->db->get('appointment_log');
//		if ($check->num_rows() > 0) {
//			$data = $check->row();
//			if ($data->status == 'cancelled') {
//				$msg = 'Your appointment has been cancelled by the doctor.';
//			} else if ($data->status == 'delayed') {
//				$msg = 'Your appointment start time has been delayed by the doctor, thank you for your patience';
//			}
//			echo json_encode(array('status' => $data->status, 'msg' => $msg, 'reason' => $data->reason ));
//			return ;
//		}
//		echo json_encode(array('status' => 'nothing'));
    }

    public function invite_doctors()
    {
        $email = $this->input->post('email');
        // check email exists or not
        if ($this->user_model->email_exists($email)) {
            // email exists show error
            $return['message'] = 'This email already exists in our system';
            echo json_encode($return);
            return;
        }
        $return = [];
        $return['message'] = '';
        $return['boolean'] = false;
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $specialty = $this->input->post('specialty');
        $state_id = $this->input->post('state_id');
        $password = random_string();
        $this->load->library('email');
        $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
        $this->email->to($email);
        $this->email->subject('Get Started With My Virtual Doctor');
        // getting template
        $body = $this->db->get_where('email_templates', ['key' => 'doctor_signup'])->row('body');
        $message = str_replace(['{FULL_NAME}', '{URL}', '{EMAIL}', '{PASSWORD}'], [$first_name . ' ' . $last_name, base_url('login'), $email, $password], $body);
        $this->email->message($message);
        if (!$this->email->send(true, true)) {
            // email is not send to the new doctor show error
            $return['message'] = 'Email could not be send to <strong>' . $email . '</strong> at this point';
            echo json_encode($return);
            return;
        }
        //send email to the admin
        $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
        $this->email->to('info@myvirtualdoctor.com');
        $this->email->subject('New Doctor Signup (' . $first_name . ' ' . $last_name);
        $this->email->message('Dear Administrator,
            <br />
            <br />
            A doctor just signed up to the system and here is the information:
            <br />
            <br />
            First Name: ' . $first_name . '
            <br />
            Last Name: ' . $last_name . '
            <br />
            Email: ' . $email . '
            <br />
            <br />
            Thank You.');

        if (!$this->email->send(true, true)) {
            // email is not send to the admin show error
            $return['message'] = 'Email could not be send to <strong>info@myvirtualdoctor.com</strong> at this point';
            echo json_encode($return);
            return;
        }

        $result = $this->doctor_model->createDoctor(
            $first_name,
            $last_name,
            $email,
            password($password),
            0,
            $state_id,
            null,
            null,
            null,
            null,
            $specialty
        );
        $doctor_id = $this->user_model->get_user_field([
            'user_email' => $email
        ])->id;
        $this->add_to_favorite(false, $this->session->userdata('userid'), $doctor_id);
        if ($result) {
            // doctor register successfully show success message
            $return['message'] = 'Your invitation has been sent to your doctor.';
            $return['boolean'] = true;
            echo json_encode($return);
            return;
        }
        // show error doctor data is not inserted properly
        $return['message'] = 'There is some issues while inserting data. Please try again.';
        echo json_encode($return);
        return;
    }

    public function edit_health_condition()
    {
        $condition_term = $this->input->post('condition_term');
        $id = $this->input->post('id');
        $this->load->model('health_model');
        $this->health_model->update($id, [
            'condition_term' => $condition_term
        ]);
        echo json_encode([
            'message' => 'Changes Saved',
            'bool' => true
        ]);
    }

    public function edit_medication()
    {
        $medication = $this->input->post('medication');
        $dosage = $this->input->post('dosage');
        $frequency = $this->input->post('frequency');
        $time = $this->input->post('time');
        $id = $this->input->post('id');
        $ndc = $this->input->post('ndc');
        $this->load->model('health_model');
        $this->health_model->update($id, [
            'condition_term' => $medication,
            'dosage' => $dosage,
            'frequency' => $frequency,
            'time' => $time,
            'ndc' => $ndc
        ]);
        echo json_encode([
            'message' => 'Changes Saved',
            'bool' => true
        ]);
    }

    public function edit_allergies()
    {
        $allergies = $this->input->post('allergies');
        $severity = $this->input->post('severity');
        $reaction = $this->input->post('reaction');
        $id = $this->input->post('id');
        $this->load->model('health_model');
        $this->health_model->update($id, [
            'condition_term' => $allergies,
            'severity' => $severity,
            'reaction' => $reaction
        ]);
        echo json_encode([
            'message' => 'Changes Saved',
            'bool' => true
        ]);
    }

    public function edit_surgery()
    {
        $surgery = $this->input->post('surgery');
        $year = $this->input->post('year');
        $id = $this->input->post('id');
        $this->load->model('health_model');
        $this->health_model->update($id, [
            'condition_term' => $surgery,
            'source' => $year
        ]);
        echo json_encode([
            'message' => 'Changes Saved',
            'bool' => true
        ]);
    }

    public function getGPI10($search, $from_url = true)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://c.qa.med.api.rxcut.com/1.1/GetGPI10s?search=' . $search . '&uid=C663A703-D98D-4D8D-875D-F85EF1B9779C');
        $result = curl_exec($ch);
        curl_close($ch);
        if($from_url){
            echo $result;
            die();
        }

        return json_decode($result, true);
    }

    public function getGPI14($GPI10, $from_url = true)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_URL, 'http://c.qa.med.api.rxcut.com/1.1/GetGPI14s?GPI10=' . $GPI10 . '&uid=C663A703-D98D-4D8D-875D-F85EF1B9779C');
        curl_setopt($ch, CURLOPT_URL, 'http://c.qa.med.api.rxcut.com/1.1/GetGPI14s?GPI10=' . $GPI10 . '&uid=C663A703-D98D-4D8D-875D-F85EF1B9779C&freqrank=true&prediction=100000');
        $result = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($result, true);
        $return = [
            'drugs' => []
        ];
//        foreach ($data['drugs'] as $key => $value) {
//            $return['drugs'][] = $value;
//        }
        if(array_key_exists('predictions', $data)){
            foreach ($data['predictions'] as $key => $value) {
                if(count($return['drugs']) > 0){
                    $inArray = false;
                    for($i = 0; $i < count($return['drugs']); $i++){
                        if($value['FreqRank'] == $return['drugs'][$i]['FreqRank']){
                            $inArray = true;
                        }
                    }
                    if(!$inArray){
                        $return['drugs'][] = $value;
                    }
                } else {
                    $return['drugs'][] = $value;
                }
            }
        }
        if($from_url){
            echo $result;
            die();
        }
//        echo "<pre>";
//        print_r($return);
//        die();
        return $return;
    }

    public function getPackages($GPI14, $from_url = true)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://c.qa.med.api.rxcut.com/1.1/GetPackagesFromGPI?GPI14=' . $GPI14 . '&flat=true&truncate=false&uid=C663A703-D98D-4D8D-875D-F85EF1B9779C');
        $result = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($result, true);
        $return = [
            'drugs' => []
        ];
        foreach ($data['drugs'] as $key => $value) {
//            if ($value["DrugType"] == 'B' && ($value['DosageForm'] == 'TABLET' || $value['DosageForm'] == 'SYRUP') && $value['Strength'] != '') {
//                $value["full_name"] = $value['LN'] . ' ' . $value['PackageSize'] . ' ' . $value['PackageUnit'] . ' ' . $value['PackageDesc'];
                $return['drugs'][] = $value;
//            }
        }
        if($from_url){
            echo $result;
            die();
        }
//        echo "<pre>";
//        print_r($return);
//        die();
        return $return;
    }

    public function getMedicine_api($search, $isrequest = true)
    {
        $result = [];
//        foreach ($this->getGPI10($search, false)['drugs'] as $key => $value) {
//            $return = [];
//            foreach ($value['GPI10s'] as $key => $val) {
//                $return[] = $val;
//            }
//            foreach ($this->getGPI14(implode(',', $return), false)['drugs'] as $key => $value) {
//                $return2 = [];
//                foreach ($value['GPI14s'] as $key => $val) {
//                    $return2[] = $val;
//                }
//                foreach ($this->getPackages(implode(',', $return2), false)['drugs'] as $key => $value) {
////                    if(($value['DosageForm'] == 'LIQUID' || $value['DosageForm'] == 'TABLET') && $value['DrugType'] == 'B' && $value['Strength'] != ''){
//                    if(($value['DosageForm'] == 'LIQUID' || $value['DosageForm'] == 'TABLET') && $value['Strength'] != ''){
//                        $result[] = $value;
//                    }
//                }
//            }
//        }
        foreach ($this->getGPI10($search, false)['drugs'] as $key => $value) {
            $return = [];
            foreach ($value['GPI10s'] as $key => $val) {
                $return[] = $val;
            }
            foreach ($this->getGPI14(implode(',', $return), false)['drugs'] as $key => $value) {
                if(($value['DosageForm'] == 'LIQUID' || $value['DosageForm'] == 'TABLET') && $value['Strength'] != ''){
                    $result[] = $value;
                }
            }
        }
//        echo "<pre>";
//        print_r($result);
//        die();
        $return = [
            'full_data' => [],
            'names' => []
        ];
        $result = array_map("unserialize", array_unique(array_map("serialize", $result)));
        foreach ($result as $index => $value) {
            $inArr = false;
            foreach ($return['names'] as $ind => $val){
                if($value['LN'] == $val){
                    $inArr = true;
                }
            }
            if(!$inArr){
                $return['full_data'][$value['LN']] = $value;
                $return['names'][] = $value['LN'];
            }
        }
        echo json_encode($return);
        die();

//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_URL, 'http://c.qa.med.api.rxcut.com/1.1/GetGPI10s?search=' . $search . '&uid=C663A703-D98D-4D8D-875D-F85EF1B9779C');
//        $result = curl_exec($ch);
//        curl_close($ch);
//        $data = json_decode($result, true);
//        $return = [];
//        if (!$isrequest) {
//            foreach ($data['drugs'] as $index => $value) {
//                foreach ($value['GPI10s'] as $key => $val) {
//                    $return[] = $val;
//                }
//            }
//            $GPI10s_str = implode(",", $return);
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_URL, 'http://c.qa.med.api.rxcut.com/1.1/GetGPI14s?GPI10=' . $GPI10s_str . '&uid=C663A703-D98D-4D8D-875D-F85EF1B9779C');
//            $result = curl_exec($ch);
//            curl_close($ch);
//            $GPIs_14_arr = json_decode($result, true);
//            $return = [];
//            foreach ($GPIs_14_arr['drugs'] as $index => $value) {
//                foreach ($value['GPI14s'] as $key => $val) {
//                    $return[] = $val;
//                }
//            }
//            $GPI14s_str = implode(",", $return);
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_URL, 'http://c.qa.med.api.rxcut.com/1.1/GetPackagesFromGPI?GPI14=' . $GPI14s_str . '&flat=true&truncate=false&uid=C663A703-D98D-4D8D-875D-F85EF1B9779C');
//            $result = curl_exec($ch);
//            curl_close($ch);
//            $NDC_arr = json_decode($result, true);
//            $return = [];
//            foreach ($NDC_arr['drugs'] as $index => $value) {
//                $return[] = [
//                    'ndc' => $value['NDC'],
//                    'qty' => 10
//                ];
//            }
//            return $return;
//        }
//        foreach ($data['drugs'] as $index => $value) {
//            $return[] = $value['BN'];
//        }
//        echo json_encode($return);
    }

    public function get_pharm_pricing($ndc = '', $qty = '', $freq = '', $time = '')
    {
        $freq = str_replace('%20', ' ', trim($freq));
        $freq_arr = [
            'Once' => 1,
            'Twice' => 2,
            'Three times' => 3,
            'Four times' => 4,
            'Five times' => 5,
            'Six times' => 6
        ];
        $time_arr = [
            'Daily' => 30,
            'Weekly' => 4,
            'Monthly' => 1
        ];
        $freq = $freq_arr[$freq];
        $time = $time_arr[$time];
        $patient_zip = $this->patient_model->get_patient()->zip;
        $ndc_arr = [
            [
                "ndc" => $ndc,
                "qty" => (($qty * $freq) * $time)
            ]
        ];
        $url_send = "http://qa.med.api.rxcut.com/1.1/GetPrices";
        $request_data = json_encode([
            "uid" => "C663A703-D98D-4D8D-875D-F85EF1B9779C",
            "location" => $patient_zip,
            "maxReturn" => 10,
            "radius" => 50.0,
            "unc" => [ "unc" ],
            "medications" => $ndc_arr,
            "mode" => 'rollup'
//            "PharmacyGroup" => 'walmart'
        ]);
        $ch = curl_init($url_send);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request_data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $pharmData = json_decode($result);
//        echo "<pre>";
//        print_r($pharmData);
//        die();
        $return = [];
        foreach (@$pharmData->pharmData as $index => $value) {
            $pharm_name = $value->name;
            $pharm_price = $value->pricing[0]->price;
            $unc = ((property_exists($value->pricing[0], 'unc')) ? $value->pricing[0]->unc : 0);
//            $pharm_price = $value->Summary->MinPrice;
//            $unc = $value->Summary->MaxPrice;
            if($pharm_price <= $unc){
                if(count($return) > 0){
                    $inArray = false;
                    foreach ($return as $ind => $val){
                        if($pharm_name == $val['name']){
                            $inArray = true;
                        }
                    }
                    if(!$inArray){
                        $return[] = [
                            'name' => $pharm_name,
                            'price' => $pharm_price,
                            'unc' => $unc,
                            'pharmData' => $value
                        ];
                    }
                } else {
                    $return[] = [
                        'name' => $pharm_name,
                        'price' => $pharm_price,
                        'unc' => $unc,
                        'pharmData' => $value
                    ];
                }
            }
        }
        echo json_encode($return);
        die();
    }

    public function get_card_discount()
    {
        $patient_data = $this->patient_model->get_patient();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://www.rxcut.com/MVD/en/Card/Send/Email?to='.$this->session->userdata('useremail').'&uid=C663A703-D98D-4D8D-875D-F85EF1B9779C');
//        curl_setopt($ch, CURLOPT_URL, 'http://www.rxcut.com/MVD/en/Card/Send/Email?to=osama.ahmed220@gmail.com&uid=C663A703-D98D-4D8D-875D-F85EF1B9779C');
        $result = curl_exec($ch);
        curl_close($ch);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://www.rxcut.com/MVD/en/Card/Send/Text?to='.$patient_data->phone_mobile.'&uid=C663A703-D98D-4D8D-875D-F85EF1B9779C');
        $result = curl_exec($ch);
        curl_close($ch);

        $this->patient_model->update($this->session->userdata('userid'), [
            'is_discount_applied' => 1
        ]);

        echo json_encode([
            'bool' => true
        ]);
        return;


//        $this->load->model('state_model');
//        $state_data = $this->state_model->get_by_id($patient_data->state_id);
//        $this->load->library('email');
//        $this->email->clear();
//        $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
//        $this->email->to('info@myvirtualdoctor.com');
//        $this->email->subject('Application for My Virtual Doctor discount drug card');
//        $this->email->message('
//        <!DOCTYPE html>
//        <html>
//        <head>
//        <title>Application for My Virtual Doctor discount drug card</title>
//        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
//        </head>
//        <body>
//        <div class="container">
//        <h3>Dear Administrator,</h3>
//        <p>The following patient has requested the My Virtual Doctor discount drug card:</p>
//        <ul class="list-group">
//        <li class="list-group-item">Patient Name: <strong>' . $user_session_data['full_name'] . '</strong></li>
//        <li class="list-group-item">Email: <strong>' . $user_session_data['useremail'] . '</strong></li>
//        <li class="list-group-item">City: <strong>' . $patient_data->city . '</strong></li>
//        <li class="list-group-item">State: <strong>' . $state_data->name . '</strong></li>
//        <li class="list-group-item">Zip: <strong>' . $patient_data->zip . '</strong></li>
//        <li class="list-group-item">Phone Mobile: <strong>' . $patient_data->phone_mobile . '</strong></li>
//        </ul>
//        <p>Thank You.</p>
//        </div>
//         </body>
//        </html>
//        ');
//        if (!$this->email->send()) {
//            echo json_encode([
//                'bool' => false
//            ]);
//            return;
//        }
//        $this->patient_model->update($this->session->userdata('userid'), [
//            'is_discount_applied' => 1
//        ]);
////        $this->load->model('health_model');
////        $this->health_model->update_by_field([
////            'id' => $this->input->post('medication_id')
////        ], [
////            'apply_c_dis' => 1
////        ]);
//        echo json_encode([
//            'bool' => true
//        ]);
//        return;
    }
}








































































