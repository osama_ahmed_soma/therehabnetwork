<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Class Doctor
 * @property CI_Input $input
 * @property CI_Form_validation $form_validation
 * @property Login_model $login_model
 * @property Specialty_Model $specialty_model
 * @property Timezone_Model $timezone_model
 * @property State_Model $state_model
 */
class Doctor extends Auth_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->doctor_access_check();

        $this->load->model('specialty_model');
        $this->load->model('timezone_model');
        $this->load->model('state_model');
        $this->load->model('appointment_model');
        $this->load->model('doctor_model');
        $this->load->model('reason_model');
        $this->load->model('patient_model');
        $this->load->model('consultationtype_model');


    }

    public function index($is_show = true, $data_for_waive_fee_page = [])
    {
        $result['is_show'] = $is_show;
        $result['doctors_patients'] = $this->doctor_model->get_doctors_patients($this->session->userdata('userid'));
        $this->load->model('waive_model');
        if (is_array($result['doctors_patients']['general_info'])){
            $i = 0;
            foreach ($result['doctors_patients']['general_info'] as $doctors_patient){
                $result['doctors_patients']['general_info'][$i]->image_url = default_image($doctors_patient->image_url);
                $result['doctors_patients']['general_info'][$i]->age = date_diff(date_create($doctors_patient->dob), date_create('today'))->y;
                if($result['doctors_patients']['general_info'][$i]->age == 0){
                    $result['doctors_patients']['general_info'][$i]->age = 'N/A';
                }
                $waive_fee_patient_check = $this->waive_model->check_exists([
                    [
                        'field' => 'doctor_id',
                        'value' => $this->session->userdata('userid')
                    ],
                    [
                        'field' => 'patient_id',
                        'value' => $doctors_patient->user_id
                    ]
                ]);
                $result['doctors_patients']['general_info'][$i]->is_waived = 'On';
                $result['doctors_patients']['general_info'][$i]->waive_fee_id = '';
                if($waive_fee_patient_check){
                    $result['doctors_patients']['general_info'][$i]->is_waived = 'Off';
                    $result['doctors_patients']['general_info'][$i]->waive_fee_id = $this->waive_model->getByFields([
                        [
                            'field' => 'doctor_id',
                            'value' => $this->session->userdata('userid')
                        ],
                        [
                            'field' => 'patient_id',
                            'value' => $doctors_patient->user_id
                        ]
                    ])[0]->id;
                }
                $i++;
            }
        }
        $scriptData['scriptData'] = 'template_file/js/lib/pagination/pagination.min.js';
        $scriptData['customStyles'] = 'template_file/css/lib/bootstrap-switch/bootstrap-switch.min.css';

        $this->load->view('header', $scriptData);
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');
        if($is_show){
            $this->load->view("doctor/patient_profiles", $result);
        } else {
            $this->load->view($data_for_waive_fee_page['header']);
            $this->load->view("doctor/patient_profiles", $result);
            $this->load->view($data_for_waive_fee_page['footer']);
        }
        $this->load->view("footer", [
            'customScript' => [
                base_url().'template_file/js/lib/bootstrap-switch/bootstrap-switch.min.js'
            ]
        ]);
    }

    public function search_doctor_patients($search_input = false){
        if(!$search_input){
            echo json_encode([
                'message' => 'You can\'t access this page',
                'bool' => false
            ]);
            return;
        }
        if($search_input == 'all'){
            $result['doctors_patients'] = $this->doctor_model->get_doctors_patients($this->session->userdata('userid'));
        } else {
            $result['doctors_patients'] = $this->doctor_model->search_doctors_patients($this->session->userdata('userid'), $search_input);
        }
        $this->load->model('waive_model');
        if (is_array($result['doctors_patients']['general_info'])){
            $i = 0;
            foreach ($result['doctors_patients']['general_info'] as $doctors_patient){
                $result['doctors_patients']['general_info'][$i]->image_url = default_image($doctors_patient->image_url);
                $result['doctors_patients']['general_info'][$i]->age = date_diff(date_create($doctors_patient->dob), date_create('today'))->y;
                $waive_fee_patient_check = $this->waive_model->check_exists([
                    [
                        'field' => 'doctor_id',
                        'value' => $this->session->userdata('userid')
                    ],
                    [
                        'field' => 'patient_id',
                        'value' => $doctors_patient->user_id
                    ]
                ]);
                $result['doctors_patients']['general_info'][$i]->is_waived = 'On';
                $result['doctors_patients']['general_info'][$i]->waive_fee_id = '';
                if($waive_fee_patient_check){
                    $result['doctors_patients']['general_info'][$i]->is_waived = 'Off';
                    $result['doctors_patients']['general_info'][$i]->waive_fee_id = $this->waive_model->getByFields([
                        [
                            'field' => 'doctor_id',
                            'value' => $this->session->userdata('userid')
                        ],
                        [
                            'field' => 'patient_id',
                            'value' => $doctors_patient->user_id
                        ]
                    ])[0]->id;
                }
                $i++;
            }
        } else {
            echo json_encode([
                'message' => 'No Patient found.',
                'bool' => false
            ]);
            return;
        }
        echo json_encode([
            'message' => count($result['doctors_patients']['general_info']).' '.((count($result['doctors_patients']['general_info']) > 1) ? 'Patients' : 'Patient').' found.',
            'doctors_patients' => $result['doctors_patients'],
            'bool' => true
        ]);
        return;
    }


    /*public function login() {
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $email = $this->input->post('email');
        $pass = $this->input->post('password');
        $type = $this->input->post('type');
        if ($this->form_validation->run()) {
            if ($this->login_model->get_login_doctor()) {
                redirect('/home/doctor');
            } else {
                $data['message'] = 'Wrong Information';
                $this->load->view('header');
                //$this->load->view('nav');
                $this->load->view("signin/doctor_singin", $data);
                $this->load->view("footer");
            }
        } else {
            $this->load->view('header');
            //$this->load->view('nav');
            //$user=$this->login_model->all_user();
            $this->load->view("signin/doctor_singin");
            $this->load->view("footer");
        }
    }*/


    public function info()
    {
        $id = $this->session->userdata('userid');
        $this->load->view('header');
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');
        $data = array();
        $data['doctor'] = $this->login_model->get_doctor($id);
        $data['specials'] = $this->login_model->get_doctors_specialization($id);
        $data['degrees'] = $this->login_model->get_doctors_degree($id);
        $data['experiences'] = $this->login_model->get_doctors_experience($id);
        $data['educations'] = $this->login_model->get_doctors_education($id);
        $data['languages'] = $this->login_model->get_doctors_language($id);
        $data['awards'] = $this->login_model->get_doctors_awards($id);
        $data['memberships'] = $this->login_model->get_doctors_professiona_membership($id);
//        $data['licenced_states'] = $this->login_model->get_doctors_licenced_states($id);
        $data['licenced_states'] = $this->login_model->get_doctors_other_licenced_states($id);
        $data['fav'] = $this->login_model->get_favorite($id);
        $data['states'] = $this->state_model->getList();
        /*echo "<pre>";
        print_r($data);
        die();*/
        $this->load->view("doctor_info/index", $data);
        $this->load->view("footer");
    }

    public function patientinfo($id)
    {
        //$id = $this->session->userdata('userid');
        $scriptData['scriptData'] = 'template_file/js/lib/pagination/pagination.min.js';
        $this->load->view('header', $scriptData);
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');
        $data = array();
        $data['patients'] = $this->login_model->get_patient($id);
        //$data['specials'] = $this->login_model->get_doctors_specialization($id);
        //$data['degrees'] = $this->login_model->get_doctors_degree($id);
        //$data['experiences'] = $this->login_model->get_doctors_experience($id);
        //$data['educations'] = $this->login_model->get_doctors_education($id);
        //$data['languages'] = $this->login_model->get_doctors_language($id);
        //$data['awards'] = $this->login_model->get_doctors_awards($id);
        //$data['memberships'] = $this->login_model->get_doctors_professiona_membership($id);
        //$data['licenced_states'] = $this->login_model->get_doctors_licenced_states($id);
        /*$data['patient_files'] = $this->login_model->get_patient_files($id);*/
		
		$this->load->model('File_model');
        $data['patient_files'] = $this->File_model->get_files_shared_by_patient($id);
		$data['patient_id'] = $id;
		
        $data['healths'] = $this->login_model->get_patient_health_condition('health', $id);
        $data['medications'] = $this->login_model->get_patient_health_condition('medication', $id);
        $data['allergies'] = $this->login_model->get_patient_health_condition('allergies', $id);
        $data['surgeries'] = $this->login_model->get_patient_health_condition('surgery', $id);
        $data['fav'] = $this->login_model->get_favorite($id);

        $this->load->model('patient_model');

        $data['pastAppointments'] = $this->patient_model->getPastAppointmentForPatient($id);
        $data['futureAppointments'] = $this->patient_model->getFutureAppointmentForPatient($id);

        /*echo "<pre>";
        print_r($data['futureAppointments']);
        die();*/

        $this->load->view("doctor/patient_profile", $data);
        $this->load->view("footer");
    }

    public function patientProfiles(){
        $this->load->view('header');
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');
        $this->load->view("doctor/patient_profiles");
        $this->load->view("footer");
    }

    public function consultations()
    {
        /*echo base_url().'doctor/consultations/';
        die();*/
        /*$this->load->library('pagination');

        $config['base_url'] = base_url().'doctor/consultations/'.$arg.'/';
        $config['total_rows'] = 200;
        $config['per_page'] = 20;

        $this->pagination->initialize($config);

        echo $this->pagination->create_links();
//        print_r($arg);
        die();*/
        $scriptData['scriptData'] = 'template_file/js/lib/pagination/pagination.min.js';
        $this->load->view('header', $scriptData);
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');

        $userId = $this->session->userdata('userid');

        $data['pastAppointments'] = $this->doctor_model->getPastAppointmentForDoctors($userId);
        $data['futureAppointments'] = $this->doctor_model->getFutureAppointmentForDoctors($userId);
        $data['pendingAppointments'] = $this->doctor_model->getPendingAppointmentForDoctors($userId);
        $data['canceledAppointments'] = $this->doctor_model->getCanceledAppointmentForDoctors($userId);

        /*echo "<pre>";
        print_r($data);
        die(); */

        //get doctor latest 4 patients
        $data['patients'] = $this->doctor_model->get_doctor_patients();
        $data['reasons'] = $this->reason_model->getList();
        $data['doctor_available_time'] = $this->doctor_model->getDoctorAvailableDate($userId);

        $this->load->view("doctor/appointments", $data);
        $this->load->view("footer");
    }

    public function ajax_patient_appointment_modal()
    {
        if ( $this->input->is_ajax_request() )
        {
            $userId = $this->session->userdata('userid');

            //get doctor latest 4 patients
            $data['patients'] = $this->doctor_model->get_doctor_patients();
            $data['reasons'] = $this->reason_model->getList();
            $data['doctor_available_time'] = $this->doctor_model->getDoctorAvailableDate($userId);
            $data['doctor_stripe'] = $this->doctor_model->get_doctor_stripe($userId);
            $data['consultation_types'] = $this->consultationtype_model->getByDoctorList($this->session->userdata('userid'));
            $data['calling_from_ajax'] = true;

            //return view
//			Header(__CLASS__ . ': ' . __FUNCTION__);
            $this->load->view('doctor/_schedule', $data);
//            return;
        }
        else {
            show_error('Invalid Request.');
        }
    }

    public function ajax_reschedule_patient_appointment()
    {
        if ( $this->input->is_ajax_request() )
        {
            $data = array();
            $me = $this->session->userdata('userid');

            $appointment_id = $this->input->post('appointment_id', true);

            //now get appointment info
            $appointment = $this->appointment_model->get_appointment_by_id( $appointment_id );

            if ( is_object($appointment) ) {
                //now check security
                //1. check this patient appointment
                if ( $appointment->doctors_id != $me ) {
                    $data['error'][] = 'Invalid appointment id. This appointment is not assigned to you.';
                }

                //2. check appointment is pending
                if ( $appointment->status != '5' ) {
                    $data['error'][] = 'Invalid appointment id. Appointment status is not cancelled.';
                }

                //now do actual work
                if ( !isset($data['error']) ) {
                    $data['appointment'] = $appointment;
                    $data['patient'] = $this->patient_model->getById($appointment->patients_id);
                    $data['reasons'] = $this->reason_model->getList();
                    $data['id'] = $appointment->doctors_id;
                    $data['doctor_stripe'] = $this->doctor_model->get_doctor_stripe($appointment->doctors_id);
                    $data['doctor_available_time'] = $this->doctor_model->getDoctorAvailableDate($me);

                    $data['consultation_types'] = $this->consultationtype_model->getByDoctorList($this->session->userdata('userid'));
                    //get consultation type data
                    if ( is_numeric($appointment->consultation_type) && $appointment->consultation_type > 0 ) {
                        //get data from table
                        $consultation_type_data = $this->consultationtype_model->get_consultation_by_id($appointment->consultation_type);
                        if ( is_object($consultation_type_data) && isset($consultation_type_data->id) ) {
                            $data['consultation_type_data'] = $consultation_type_data;
                        }
                    }

                }

            } else {
                $data['error'][] = 'Invalid Appointment.';
            }

            $data['calling_from_ajax'] = true;
            $data['reschedule_nopayment'] = 'yes';

            //return view
            $this->load->view('doctor/_schedule', $data);
        }
        else {
            show_error('Invalid Request.');
        }
    }


    public function records()
    {
        $this->load->view('header');
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');

		$this->load->model('File_model');
        $this->load->model('messages_model');
		$this->load->library('pagination');
		
		$data['recipient_list'] = $this->messages_model->getRecipients();

		$limit = 10;
		$page = $this->uri->segment(3) !== NULL ? $this->uri->segment(3) : 0;

		$search_text = trim($this->input->get('search', true));
		$data['search_text'] = $search_text;
		if (!empty($search_text)) {
			$data['search'] = 1;
			$data['documents'] = $this->File_model->search_files($limit, $page, $search_text);
			$config['reuse_query_string'] = TRUE;
			$config['total_rows'] = $this->File_model->search_files($limit, $page, $search_text, true);
		} else {
			$search_text = '';
			$data['search'] = 0;
			$data['documents'] = $this->File_model->files($limit, $page);
			$config['reuse_query_string'] = FALSE;
			$config['total_rows'] = $this->File_model->count_files();
		}
		
		//bootstrap changes
		$config['full_tag_open'] = '<nav aria-label="Page navigation" style="text-align: center;"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav><!--pagination-->';
		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class=" page-item">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class=" page-item">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class=" page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
//         $config['display_pages'] = FALSE;
		$config['attributes'] = array('class' => 'page-link');

		//pagination value
		$config['base_url'] = base_url() . $this->uri->segment(1) . '/'.__FUNCTION__.'/';		$config['per_page'] = $limit;
		$this->pagination->initialize($config);
		$data['pagination_link'] = $this->pagination->create_links();

        $this->load->view("records", $data);
        $this->load->view("footer");
    }

    public function delete_image($id)
    {
        $file_del = $this->login_model->patient_upload_delete($id);
        if ($file_del) {
            $ss = 'success';
            $msgs = 'File successfully deleted';
        } else {
            $ss = 'error';
            $msgs = 'Something went wrong when deleteing the file, please try again';
        }
        echo json_encode(array('success' => $ss, 'wrong' => $msgs));
    }

    public function profile()
    {
        $id = $this->session->userdata('userid');

        $doctors = $this->login_model->get_doctors($id);
        $doctor = reset($doctors);

        $data['patient'] = $this->login_model->get_patients();
        $data['doctor'] = $doctors[0];
        $data['availabilities'] = $this->login_model->get_doctors_available($id, date("m"), date("Y-m-d"));

        $data['special'] = $this->specialty_model->getById($doctor->specialty);
        $data['specials'] = $this->login_model->get_doctors_specialization($id);
		
//		dd($data['specials']);

//        $data['degrees'] = $this->login_model->get_doctors_degree($id);
        $data['experiences'] = $this->login_model->get_doctors_experience($id);
        $data['educations'] = $this->login_model->get_doctors_education($id);
		
//		dd($data['educations']);
		
        $data['languages'] = $this->login_model->get_doctors_language($id);
        $data['awards'] = $this->login_model->get_doctors_awards($id);
//        $data['licenced_states'] = $this->login_model->get_doctors_licenced_states($id);
        $data['licenced_states'] = $this->login_model->get_doctors_other_licenced_states($id);
        $data['memberships'] = $this->login_model->get_doctors_professiona_membership($id);
        $data['fav'] = $this->login_model->get_favorite($id);
        $data['id'] = $id;


        $this->load->model('reason_model');
        $data['reasons'] = $this->reason_model->getList();

        $this->load->view('header');
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');
        $this->load->view("doctor/index", $data);
        $this->load->view("footer");
        /*
                $fname = $this->input->post('fname');
                $lname = $this->input->post('lname');
                $email = $this->input->post('email');
                $gender = $this->input->post('gender');
                $dob = $this->input->post('dob');
                $timezone = $this->input->post('timezone');
                $state = $this->input->post('state');
                $city = $this->input->post('city');
                $zip = $this->input->post('zip');
                $phone_home = $this->input->post('phone-office');
                $phone_mobile = $this->input->post('phone-mobile');
                $fax = $this->input->post('fax');
                $pre = $this->input->post('pre_education');
                //$intern = $this->input->post('intern');
                //$country = $this->input->post('country');
                $pratice = $this->input->post('practicing');
                $residency = $this->input->post('residency');
                $dept = $this->input->post('dept');
                $specialty = $this->input->post('specialty');
                $s_specialty = $this->input->post('s_specialty');
                $m_license = $this->input->post('m_license');
                $s_license = $this->input->post('s_license');



                $config['upload_path'] = 'uploads/doctors/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = 1024;
                $config['max_width'] = 1024;
                $config['max_height'] = 768;
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('doctor_image')) {
        //            $error = array('data' => $this->upload->display_errors());
                    //$this->load->view('profile/file', $error);
                    $image_url = $this->input->post('doctor_image_db');
                } else {
                    $fdata = $this->upload->data();
                    $image_url = $config['upload_path'] . $fdata['file_name'];
                }

                $data = array(
                    'firstname' => $fname,
                    'lastname' => $lname,
                    'email' => $email,
                    'timezone' => $timezone,
                    'gender' => $gender,
                    'dob' => $dob,
                    'state' => $state,
                    'city' => $city,
                    'zip' => $zip,
                    'phone_home' => $phone_home,
                    'phone_mobile' => $phone_mobile,
                    'fax' => $fax,
                    'pre_medical_education' => $pre,
                    'practicing_since' => $pratice,
                    'residency' => $residency,
                    'department' => $dept,
                    'specialty' => $specialty,
                    'sub_specialty' => $s_specialty,
                    'npi' => $m_license,
                    'state_license' => $s_license,
                    'image_url' => $image_url
                );


                if ($this->input->post("save") == "1") {
                    $this->db->where('userid', $id);
                    $this->db->update('doctor', $data);
                }
                $data['patients'] = $this->login_model->get_patients();
                $data['doctors'] = $this->login_model->get_doctors($id);
                $data['availabilities'] = $this->login_model->get_doctors_available($id, date("m"), date("Y-m-d"));
                $data['specials'] = $this->login_model->get_doctors_specialization($id);
                $data['degrees'] = $this->login_model->get_doctors_degree($id);
                $data['experiences'] = $this->login_model->get_doctors_experience($id);
                $data['educations'] = $this->login_model->get_doctors_education($id);
                $data['languages'] = $this->login_model->get_doctors_language($id);
                $data['languages'] = $this->login_model->get_doctors_language($id);
                $data['awards'] = $this->login_model->get_doctors_awards($id);
                $data['licenced_states'] = $this->login_model->get_doctors_licenced_states($id);
                $data['memberships'] = $this->login_model->get_doctors_professiona_membership($id);
                $data['fav'] = $this->login_model->get_favorite($id);
                $data['id'] = $id;
                $this->load->view('header');
                $this->load->view('doctor_header_top');
                $this->load->view('doctor_nav');
                $this->load->view("doctor/index", $data);
                $this->load->view("footer");
        */
    }

    public function get_doctors_available_time()
    {
        $id = $this->input->post('id');
        $date = $this->input->post('select_date');
        $month = $this->input->post('select_month');
        $data = $this->login_model->get_doctors_available($id, $month, $date);
        if (is_array($data)) {
            foreach ($data as $av) {
                $start_time = date('h:i:s A', strtotime($av->start_date . " " . $av->start_time));
                $end_time = date('h:i:s A', strtotime($av->start_date . " " . $av->end_time));
                echo '<button type="button" class="picker-slots-button" data-available="' . $av->id . '" data-start_time="' . $start_time . '" data-end_time="' . $end_time . '" data-date="' . $av->start_date . '">' . $start_time . ' - ' . $end_time . '</button>';
            }
        } else {
            echo $data;
        }
    }

    public function patients()
    {
        $result['doctors_patients'] = $this->login_model->get_doctors_patients($this->session->userdata('userid'));
        $this->load->view('header');
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');
        $this->load->view("doctor/doctor_patients", $result);
        $this->load->view("footer");
    }

    public function update_profile()
    {
        $id = $this->session->userdata('userid');
        $doctors = $this->login_model->get_doctors($id);
        $doctor = reset($doctors);
        $data['patients'] = $this->login_model->get_patients();
        $data['doctors'] = $doctors;
        $data['doctor'] = $this->login_model->get_doctor($id);
        $data['availabilities'] = $this->login_model->get_doctors_available($id, date("m"), date("Y-m-d"));
        $data['specials'] = $this->login_model->get_doctors_specialization($id);
        $data['degrees'] = $this->login_model->get_doctors_degree($id);
        $data['experiences'] = $this->login_model->get_doctors_experience($id);
        $data['educations'] = $this->login_model->get_doctors_education($id);
        $this->load->model('language_model');
        $data['languages'] = $this->language_model->getList();
        $data['doctor_languages'] = $this->login_model->get_doctors_language($id);
        $data['awards'] = $this->login_model->get_doctors_awards($id);
        $data['memberships'] = $this->login_model->get_doctors_professiona_membership($id);
        $data['licenced_states'] = $this->login_model->get_doctors_other_licenced_states($id);
        $data['states'] = $this->state_model->getList();
        $data['specialties'] = $this->doctor_model->getSpecialtyList();

        $data['zones'] = $this->timezone_model->getList();

        $data['fav'] = $this->login_model->get_favorite($id);
        $data['id'] = $id;
        $this->load->model('country_model');
        $data['countries'] = $this->country_model->find_all_countries();

        $this->load->view('header', [
            'customStyles' => 'node_modules/cropperjs/dist/cropper.min.css',
            'scriptData' => 'node_modules/cropperjs/dist/cropper.min.js'
        ]);
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');
        $this->load->view("doctor_info/index", $data);
        $this->load->view("footer");
    }

    public function update_doctor_profile(){
        $id = $this->session->userdata('userid');
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $gender = $this->input->post('gender');
        $dob = $this->input->post('dob');
        $timezone = $this->input->post('timezone');
        $state = $this->input->post('state');
        if (!$state) {
            $state = null;
        }
        $city = $this->input->post('city');
        $zip = $this->input->post('zip');
        $country = $this->input->post('country');
        $optional_state = $this->input->post('optional_state');
        $phone_home = $this->input->post('phone-office');
        $phone_mobile = $this->input->post('phone-mobile');
        $fax = $this->input->post('fax');
        $pre = $this->input->post('pre_education');
        $pratice = $this->input->post('practicing');
        $medical_school = $this->input->post('medical_school');
        $residency = $this->input->post('residency');
        $dept = $this->input->post('dept');
        $specialty = $this->input->post('specialty');
        $s_specialty = $this->input->post('s_specialty');
        $m_license = $this->input->post('m_license');
        $s_license = $this->input->post('s_license');

        $config['upload_path'] = 'uploads/doctors/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 1024;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $this->load->library('upload', $config);
        $result = [];
        $result['message'] = '';
        $result['src'] = '';
        $result['bool'] = false;
        $result['isByError'] = false;
        if (!$this->upload->do_upload('croppedImage')) {
            if($this->input->post('doctor_image_db')){
                $error = $this->upload->display_errors();
                if($error == "<p>The image you are attempting to upload doesn't fit into the allowed dimensions.</p>"){
                    $result['message'] = 'The image you are attempting to upload doesn\'t fit into the allowed dimensions. Width = 1024 and Height = 768.';
                    $result['src'] = $config['upload_path'] . $this->input->post('doctor_image_db');
                    $result['line'] = __LINE__;
                } else {
                    $result['bool'] = true;
                    $result['line'] = __LINE__;
                    $result['test'] = $this->upload->data();
                }
            } else {
                $error = $this->upload->display_errors();
                if ($error == "<p>The image you are attempting to upload doesn't fit into the allowed dimensions.</p>") {
                    $result['message'] = 'The image you are attempting to upload doesn\'t fit into the allowed dimensions. Width = 1024 and Height = 768.';
                    $result['line'] = __LINE__;
                } else {
                    $result['bool'] = true;
                    $result['line'] = __LINE__;
                }
            }
            $image_url = $this->input->post('doctor_image_db');

        } else {
            $fdata = $this->upload->data();
            $image_url = $config['upload_path'] . $fdata['file_name'];

//            $this->session->set_userdata('image', $image_url);
            $result['src'] = $image_url;
            $result['bool'] = true;
            $result['isByError'] = true;
        }
        $this->session->set_userdata('image', $image_url);
        $data = array(
            'firstname' => $fname,
            'lastname' => $lname,
            'timezone' => $timezone,
            'gender' => $gender,
            'dob' => $dob,
            'country' => $country,
            'state_id' => $state,
            'optional_state' => $optional_state,
            'city' => $city,
            'zip' => $zip,
            'phone_home' => $phone_home,
            'phone_mobile' => $phone_mobile,
            'fax' => $fax,
            'pre_medical_education' => $pre,
            'practicing_since' => $pratice,
            'residency' => $residency,
            'medical_school' => $medical_school,
            'department' => $dept,
            'specialty' => $specialty,
            'sub_specialty' => $s_specialty,
            'npi' => $m_license,
            'state_license' => $s_license,
            'image_url' => $image_url
        );
        /*if ($this->input->post("save") == "1") {
            $this->db->where('userid', $id);
            $this->db->update('doctor', $data);
        }*/
		
//		dd($timezone);
        $this->db->where('userid', $id);
        $this->db->update('doctor', $data);
        $this->session->set_userdata('full_name', $fname.' '.$lname);
        $this->session->set_userdata('user_timezone', $timezone);
        if($result['bool']){
            $result['message'] = 'Profile updated';
        }
        echo json_encode($result);
    }

    /*public function update_profile()
    {
        $id = $this->session->userdata('userid');

        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $gender = $this->input->post('gender');
        $dob = $this->input->post('dob');
        $timezone = $this->input->post('timezone');
        $state = $this->input->post('state');
        if (!$state) {
            $state = null;
        }
        $city = $this->input->post('city');
        $zip = $this->input->post('zip');
        $phone_home = $this->input->post('phone-office');
        $phone_mobile = $this->input->post('phone-mobile');
        $fax = $this->input->post('fax');
        $pre = $this->input->post('pre_education');
        $pratice = $this->input->post('practicing');
        $residency = $this->input->post('residency');
        $dept = $this->input->post('dept');
        $specialty = $this->input->post('specialty');
        $s_specialty = $this->input->post('s_specialty');
        $m_license = $this->input->post('m_license');
        $s_license = $this->input->post('s_license');

        $config['upload_path'] = 'uploads/doctors/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $this->load->library('upload', $config);
        $error = '';
        if (!$this->upload->do_upload('doctor_image')) {
            if($this->input->post('doctor_image_db')){
                if($error == "<p>The image you are attempting to upload doesn't fit into the allowed dimensions.</p>"){
                    $error = '<p>The image you are attempting to upload doesn\'t fit into the allowed dimensions. Width = 1024 and Height = 768.</p>';
                } else {
                    $error = '';
                }
            }
            $image_url = $this->input->post('doctor_image_db');
        } else {
            $fdata = $this->upload->data();
            $image_url = $config['upload_path'] . $fdata['file_name'];

            $this->session->set_userdata('image', $image_url);
        }

        $data = array(
            'firstname' => $fname,
            'lastname' => $lname,
            'timezone' => $timezone,
            'gender' => $gender,
            'dob' => $dob,
            'state_id' => $state,
            'city' => $city,
            'zip' => $zip,
            'phone_home' => $phone_home,
            'phone_mobile' => $phone_mobile,
            'fax' => $fax,
            'pre_medical_education' => $pre,
            'practicing_since' => $pratice,
            'residency' => $residency,
            'department' => $dept,
            'specialty' => $specialty,
            'sub_specialty' => $s_specialty,
            'npi' => $m_license,
            'state_license' => $s_license,
            'image_url' => $image_url
        );

        if ($this->input->post("save") == "1") {
            $this->db->where('userid', $id);
            $this->db->update('doctor', $data);
        }
        $doctors = $this->login_model->get_doctors($id);
        $doctor = reset($doctors);
        $data['patients'] = $this->login_model->get_patients();
        $data['doctors'] = $doctors;
        $data['availabilities'] = $this->login_model->get_doctors_available($id, date("m"), date("Y-m-d"));
        $data['specials'] = $this->login_model->get_doctors_specialization($id);
        $data['degrees'] = $this->login_model->get_doctors_degree($id);
        $data['experiences'] = $this->login_model->get_doctors_experience($id);
        $data['educations'] = $this->login_model->get_doctors_education($id);
        $data['languages'] = $this->login_model->get_doctors_language($id);
        $data['states'] = $this->state_model->getList();
        $data['special'] = $this->specialty_model->getById($doctor->specialty);


        $data['zones'] = $this->timezone_model->getList();

        $data['fav'] = $this->login_model->get_favorite($id);
        $data['id'] = $id;
        $data['error'] = $error;
        $this->load->view('header');
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');
        $this->load->view("doctor/profile_for_update", $data);
        $this->load->view("footer");
    }*/

    public function calendar()
    {
        $doctor = $this->login_model->get_doctor($this->session->userdata('userid'));
        $doctor = reset($doctor);

        $data = [];
        $data['doctor'] = $doctor;
        $data['zones'] = $this->timezone_model->getList();
        $data['times'] = $this->appointment_model->getTimeArray();
        $data['time_zone'] = date_default_timezone_get();
        $datetime = new DateTime('now', new DateTimeZone(date_default_timezone_get()));
        $datetime_string = $datetime->format('c');
        $data['now'] = $datetime_string;

        /*echo date_default_timezone_get();
        die();*/

        //$data['providers'] = $this->login_model->get_providers();
        $this->load->view('header');
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');
        $this->load->view("calendar/index", $data);
        $this->load->view("footer");
    }

    public function find_doctor_finance()
    {
        $doctor = $this->login_model->get_doctor_finance($this->session->userdata('userid'));

        if ($doctor > 0) {
            $doctor_info_html = '';
            $doctor_info_html .= '<input class="form-control doctor_award_title" placeholder="Per time slot rate" value="$' . $doctor->payment_rate . '" type="text">';

            /*$chat_messages_html .= '<div class="chat-message-photo">';
            $chat_messages_html .= '<img src="img/photo-64-1.jpg" alt="">';
            $chat_messages_html .= '</div>';
            $chat_messages_html .= '<div class="chat-message-header">';
            $chat_messages_html .= '<div class="tbl-row">';
            $chat_messages_html .= '<div class="tbl-cell tbl-cell-name">'.$chat_message->user_name;
            $chat_messages_html .= '</div>';

            $chat_messages_html .= '<div class="tbl-cell tbl-cell-date">'.$chat_message->time_sent;
            $chat_messages_html .= '</div>';
            $chat_messages_html .= '</div>';
            $chat_messages_html .= '</div>';

            $chat_messages_html .= '<div class="chat-message-content">';
            $chat_messages_html .= '<div class="chat-message-txt">'.$chat_message->messages;
            $chat_messages_html .= '</div>';
            $chat_messages_html .= '</div>';
            $chat_messages_html .= '</div>';*/

            /*$result = array('chat_status'=>'ok', 'content'=>$chat_messages_html);
            echo json_encode($result);
            exit();
        echo $chat_messages_html;*/
            $result = array('doctor_info' => 'ok', 'content' => $doctor_info_html);
            echo json_encode($result);
        } else {
            $result = array('doctor_info' => 'no', 'content' => '!');
            echo json_encode($result);
            exit();
        }
    }

    public function save_appointment()
    {
        $avail = $this->input->post('avail');
        $reasonId = $this->input->post('reason_id');
        $did = $this->input->post('did');
        $result = $this->login_model->check_existing__appointment();
        if (empty($reasonId) || empty($avail)) {
            $report['status'] = 'error';
            $report['content'] = 'Select appointment date and Insert reason';
            echo json_encode($report);
        } else if ($result) {
            $status = $this->login_model->save_appointment();
            if ($status) {
                $doctors = $this->login_model->get_doctors($did);
                $report['status'] = 'success';
                $report['content'] = 'Your appointment with Dr. ' . $doctors[0]->firstname . ' ' . $doctors[0]->lastname . ' is conformed.';
                echo json_encode($report);
            }
        } else {
            $report['status'] = 'error';
            $report['content'] = 'You already have an existing Appointment with this doctor';
            echo json_encode($report);
        }
    }


    public function add_overview()
    {
        $overview = $this->input->post('overview');
        $this->db->set('about', $overview);
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->update('doctor');
        $data['msg'] = "Updated succesfully";
        $data['uid'] = $this->session->userdata('userid');
        $data['doctor'] = $this->login_model->get_doctor($this->session->userdata('userid'));
        echo json_encode($data);
    }

    public function add_specialties()
    {
        $specialty = $this->input->post('specialty');
        $data = [];
        $data['isNew'] = false;
        // check this specialty is exists in specialty table or no
        if(!$this->specialty_model->check_exists($specialty)){
            $this->specialty_model->create($specialty);
            $data['isNew'] = true;
        }

		$data = array(
            'userid' => $this->session->userdata('userid'),
            'speciality' => $specialty
        );
		$exist = $this->db->get_where('doctor_specialization', $data);
		if ($exist->num_rows() > 0) {
			echo json_encode(array('msg' => 'Already added'));
			return;
		}
        $id = $this->login_model->insert_doctor_specialty($specialty);
        $data['msg'] = "Inserted succesfully";
        $data['uid'] = $this->session->userdata('userid');
        $data['id'] = $id;
        $data['msg'] = '';
        echo json_encode($data);
    }

    public function add_doctor_education()
    {
        $edu_title = $this->input->post('edu_title');
        $edu_uni = $this->input->post('edu_uni');
        $edu_st_year = $this->input->post('edu_st_year');
        $edu_end_year = $this->input->post('edu_end_year');
        $id = $this->login_model->insert_doctor_education($edu_st_year, $edu_end_year, $edu_title, $edu_uni);
        $data['msg'] = "Inserted succesfully";
        $data['uid'] = $this->session->userdata('userid');
        $data['id'] = $id;
        echo json_encode($data);
    }

    public function add_doctor_award()
    {
        $award_title = $this->input->post('award_title');
        $award_orga = $this->input->post('award_orga');
        $award_year = $this->input->post('award_year');
        $id = $this->login_model->insert_doctor_award($award_title, $award_orga, $award_year);
        $data['msg'] = "Inserted succesfully";
        $data['uid'] = $this->session->userdata('userid');
        $data['id'] = $id;
        echo json_encode($data);
    }

    public function add_doctor_membership()
    {
        $membership_title = $this->input->post('membership_title');
        $membership_orga = $this->input->post('membership_orga');
        $id = $this->login_model->insert_doctor_membership($membership_title, $membership_orga);
        $data['msg'] = "Inserted succesfully";
        $data['uid'] = $this->session->userdata('userid');
        $data['id'] = $id;
        echo json_encode($data);
    }

    public function add_doctor_licenced_state()
    {
        $licenced_state = $this->input->post('states');
        $id = $this->session->userdata('userid');
        $this->doctor_model->remove_doctor_other_states($id);
        if(count(json_decode($licenced_state)) > 0){
            foreach(json_decode($licenced_state) as $value){
                $this->doctor_model->add_doctor_other_states($id, $value);
            }
        }
        echo "Profile settings have been updated.";
    }

    public function add_doctor_language()
    {
        $languages = $this->input->post('languages');
        $id = $this->session->userdata('userid');
        $this->doctor_model->remove_doctor_languages($id);
        if(count(json_decode($languages)) > 0){
            foreach(json_decode($languages) as $value){
                $this->doctor_model->add_doctor_languages($id, $value);
            }
        }
        echo "Changes saved";
    }

    public function update_speciality()
    {
        $speciality_id = $this->input->post('specialty_id');
        $speciality = $this->input->post('specialty');
        $this->login_model->edit_speciality($speciality, $speciality_id);
        echo json_encode([
            'message' => 'Changes Saved.',
            'bool' => true
        ]);
    }

    public function update_education()
    {
        $education_id = $this->input->post('education_id');
        $education_title = $this->input->post('title');
        $university = $this->input->post('university');
        $from_year = $this->input->post('from_year');
        $to_year = $this->input->post('to_year');
        $data = array(
            'from_year' => $from_year,
            'to_year' => $to_year,
            'name' => $education_title,
            'institute' => $university
        );
        $this->login_model->edit_education($data, $education_id);
        echo json_encode([
            'message' => 'Changes Saved',
            'bool' => true
        ]);
    }

    public function update_award()
    {
        $doctor_award_id = $this->input->post('award_id');
        $doctor_award_title = $this->input->post('title');
        $doctor_award_organization = $this->input->post('organization');
        $doctor_award_year = $this->input->post('year');
        $data = array(
            'name' => $doctor_award_title,
            'venue' => $doctor_award_organization,
            'year' => $doctor_award_year,
        );
        $this->login_model->edit_award($data, $doctor_award_id);
        echo json_encode([
            'message' => 'Changes Saved',
            'bool' => true
        ]);
    }

    public function update_membership()
    {
        $doctor_membership_id = $this->input->post('doctor_membership_id');
        $doctor_membership_title = $this->input->post('doctor_membership_title');
        $doctor_membership_organization = $this->input->post('doctor_membership_organization');
        $data = array(
            'membership_post' => $doctor_membership_title,
            'society_name' => $doctor_membership_organization,
        );
        $this->login_model->edit_membership($data, $doctor_membership_id);
        redirect('/doctor/info');
    }

    public function delete_speciality($sp_id)
    {
        $this->login_model->erase_speciality($sp_id);
    }

    public function delete_education($id)
    {
        $this->login_model->erase_education($id);
    }

    public function delete_award($id)
    {
        $this->login_model->erase_award($id);
    }

    public function delete_membership($id)
    {
        $this->login_model->erase_membership($id);
        redirect('/doctor/update_profile');
    }

    //Settings related pages start here

    public function settings() {
        $this->load->view('header');
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');
        $this->load->view("doctor/settings/header");
        $this->load->view("doctor/settings/main");
        $this->load->view("doctor/settings/footer");
        $this->load->view("footer");
    }

    public function current_password_check($current_password){
		if (!password_verify($current_password, $this->session->userdata('userpassword'))) {
            $this->form_validation->set_message('current_password_check', 'Current Password not match');
            return false;
        }
        return true;
    }

    public function change_password()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');

        $config = array(
            array(
                'field' => 'current_password',
                'label' => 'Current Password',
                'rules' => 'trim|required|callback_current_password_check'
            ),
            array(
                'field' => 'password',
                'label' => 'New Password',
                'rules' => 'trim|required|min_length[8]'
            ),
            array(
                'field' => 'passconf',
                'label' => 'Password Confirmation',
                'rules' => 'trim|required|matches[password]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE)
        {
            //load view
        }
        else
        {
            $password = $this->input->post('password', true);
            $update_array = array(
                'user_password' => password($password)
            );
            $where = array(
                'id' => $this->session->userdata('userid')
            );

            $updated = $this->login_model->update_password($update_array, $where);
            if ($updated) {
                $this->session->set_userdata('userpassword', $update_array['user_password']);
                $this->session->set_flashdata('success', 'Your Password Updated Successfully!');
                $this->load->library('email');
                $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
                $this->email->to($this->session->userdata('useremail'));
                $this->email->subject('Password Change');
                $this->email->message('Dear '.$this->session->userdata('full_name').',
                <br />
                <br />
                Your password successfully changed. Your new password is <strong>'.$password.'</strong>
                <br />
                <br />
                <a href="http://www.myvirtualdoctor.com/app/login">Login Here</a>
                <br />
                <br />
                Thank You.');
                $this->email->send(true, true);
				redirect('login/logout?message=Your password has been changed, please login again.');
            }
        }

        $this->load->view('header');
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');
        $this->load->view("doctor/settings/header");
        $this->load->view("doctor/settings/change_password");
        $this->load->view("doctor/settings/footer");
        $this->load->view("footer");
    }

    public function add_consultation_type(){
        $payment_rate = $this->input->post('payment_rate', true);
        $duration = $this->input->post('duration', true);
        $doctorID = $this->session->userdata('userid');

        $data = [];
        $data['message'] = '';
        $data['consultation_type_id'] = 0;
        $data['bool'] = false;

        $this->load->model('consultationtype_model');
        // check if this consultation type is added or not
        if($this->consultationtype_model->isExists($doctorID, $duration)){
            $data['message'] = 'This consultation type is already exists.';
            echo json_encode($data);
            return;
        }

        // add consultation type
        $consultation_type_id = $this->consultationtype_model->add([
            'doctor_id' => $doctorID,
            'duration_id' => $duration,
            'rate' => $payment_rate
        ]);
        if(!$consultation_type_id){
            $data['message'] = 'There is a problem in adding your data.';
            echo json_encode($data);
            return;
        }

        $this->db->where('userid', $doctorID);
        $this->db->update('doctor', [
            'payment_rate' => $payment_rate
        ]);

        $data['message'] = 'Successfully Added.';
        $data['consultation_type_id'] = $consultation_type_id;
        $data['bool'] = true;
        echo json_encode($data);

    }

    public function edit_consultation_type(){
        $consultation_type_id = $this->input->post('consultation_type_id', true);
        $payment_rate = $this->input->post('payment_rate', true);
        $duration = $this->input->post('duration', true);
        $doctorID = $this->session->userdata('userid');
        $data = [];
        $data['message'] = '';
        $data['bool'] = false;
        $this->load->model('consultationtype_model');
        // check if this consultation type is added or not
        if($this->consultationtype_model->isExists($doctorID, $duration, null, $consultation_type_id)){
            $data['message'] = 'This consultation type is already exists.';
            echo json_encode($data);
            return;
        }
        // update consultation type
        $this->consultationtype_model->update($consultation_type_id, [
            'duration_id' => $duration,
            'rate' => $payment_rate
        ]);
        $this->db->where('userid', $doctorID);
        $this->db->update('doctor', [
            'payment_rate' => $payment_rate
        ]);
        $data['message'] = 'Updated.';
        $this->load->model('duration_model');
        $data['duration_in_minutes'] = $this->duration_model->getById($duration)->time;
        $data['bool'] = true;
        echo json_encode($data);
    }

    public function remove_consultation_type($id){
        $this->load->model('consultationtype_model');
        $this->consultationtype_model->remove($id);
    }

    public function payment_rate()
    {
        $config = array(
            array(
                'field' => 'payment_rate',
                'label' => 'Payment Rate',
                'rules' => 'trim|required|numeric'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE)
        {
            //load view
        }
        else
        {
            $payment_rate = $this->input->post('payment_rate', true);
            $duration = $this->input->post('duration', true);
            $isActive = (!empty($this->input->post('isActive', true)) && $this->input->post('isActive', true) == 'on') ? 1 : 0;
            $update_array = array(
                'payment_rate' => $payment_rate,
                'duration' => $duration,
                'consultation_type_is_active' => $isActive
            );
            $where = array(
                'userid' => $this->session->userdata('userid')
            );

            $updated = $this->doctor_model->updateDoctor($update_array, $where);
            $this->session->set_flashdata('success', 'Payment rate has been updated');
            /*if ($updated) {
                $this->session->set_flashdata('success', 'Payment Rate Updated Successfully!');
            }*/
        }

        $data['doctor'] = $this->doctor_model->get_doctor_payment_rate($this->session->userdata('userid'));
        $this->load->model('duration_model');
        $data['durations'] = $this->duration_model->getList();
        $this->load->model('consultationtype_model');
        $data['consultation_types'] = $this->consultationtype_model->getByDoctorList($this->session->userdata('userid'));
        /*echo "<pre>";
        print_r($data);
        die();*/
        $this->load->view('header');
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');
        $this->load->view("doctor/settings/header");
        $this->load->view("doctor/settings/payment_rate", $data);
        $this->load->view("doctor/settings/footer");
        $this->load->view("footer");
    }

    public function stripe()
    {
        $config = array(
            array(
                'field' => 'stripe_one',
                'label' => 'Stripe Secret Key',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'stripe_two',
                'label' => 'Stripe Publishable Key',
                'rules' => 'trim|required'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE)
        {
            //load view
        }
        else
        {
            $stripe_one = $this->input->post('stripe_one', true);
            $stripe_two = $this->input->post('stripe_two', true);
            $update_array = array(
                'stripe_one' => $stripe_one,
                'stripe_two' => $stripe_two
            );
            $where = array(
                'userid' => $this->session->userdata('userid')
            );

            $updated = $this->doctor_model->updateDoctor($update_array, $where);
            $this->session->set_flashdata('success', 'Stripe API Keys Updated Successfully!');
        }
        $data['doctor'] = $this->doctor_model->get_doctor_stripe($this->session->userdata('userid'));
        $this->load->view('header');
        $this->load->view('doctor_header_top');
        $this->load->view('doctor_nav');
        $this->load->view("doctor/settings/header");
        $this->load->view("doctor/settings/stripe", $data);
        $this->load->view("doctor/settings/footer");
        $this->load->view("footer");
    }

    public function waive_fee(){
        $this->index(false, [
            'header' => "doctor/settings/header",
            'footer' => "doctor/settings/footer"
        ]);
//        $result['doctors_patients'] = $this->doctor_model->get_doctors_patients($this->session->userdata('userid'));
//        if (is_array($result['doctors_patients']['general_info'])){
//            $i = 0;
//            foreach ($result['doctors_patients']['general_info'] as $doctors_patient){
//                $result['doctors_patients']['general_info'][$i]->image_url = default_image($doctors_patient->image_url);
//                $result['doctors_patients']['general_info'][$i]->age = date_diff(date_create($doctors_patient->dob), date_create('today'))->y;
//                $i++;
//            }
//        }
//        $this->load->model('waive_model');
//        $result['waive_fee_patients'] = $this->waive_model->getByFields([
//            [
//                'field' => 'doctor_id',
//                'value' => $this->session->userdata('userid')
//            ]
//        ]);
//        $this->load->view('header');
//        $this->load->view('doctor_header_top');
//        $this->load->view('doctor_nav');
//        $this->load->view("doctor/settings/header");
//        $this->load->view("doctor/settings/waive_fee", $result);
//        $this->load->view("doctor/settings/footer");
//        $this->load->view("footer");
    }

    public function add_patient_waive_fee(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
            return;
        }
        $patient_id = $this->input->post('p_id');
        $this->load->model('waive_model');
        if($this->waive_model->check_exists([
            [
                'field' => 'doctor_id',
                'value' => $this->session->userdata('userid')
            ],
            [
                'field' => 'patient_id',
                'value' => $patient_id
            ]
        ])){
            echo json_encode([
                'message' => 'This patient is already disabled.',
                'bool' => false
            ]);
            return;
        }
        $waive_id = $this->waive_model->add([
            'doctor_id' => $this->session->userdata('userid'),
            'patient_id' => $patient_id
        ]);
        echo json_encode([
            'message' => 'Billing has been disabled for the patient',
            'waive_fee_id' => $waive_id,
            'patient' => $this->patient_model->getById($patient_id),
            'bool' => true
        ]);
    }

    public function remove_waive_fee($waive_fee_id = null, $is_echo = true){
        if($waive_fee_id == null){
            if($is_echo){
                echo json_encode([
                    'bool' => false
                ]);
            }
            return;
        }
        $this->load->model('waive_model');
        $this->waive_model->remove($waive_fee_id);
        if($is_echo){
            echo json_encode([
                'bool' => true
            ]);
        }
    }

    public function remove_from_favorite(){
        $p_id = $this->input->post('p_id');
        $d_id = $this->input->post('d_id');
        $this->login_model->remove_from_favorite($p_id, $d_id);
        $fav_html = '';
        $fav_html .= '<a class="btn btn-primary-outline btn-block add_to_favorite" href="#"
                                               data-doctorid="' . $this->session->userdata("userid") . '"
                                               data-patientid="' . $d_id . '">Add Favorite</a>';
        $this->load->model('waive_model');
        if($this->waive_model->check_exists([
            [
                'field' => 'doctor_id',
                'value' => $d_id
            ],
            [
                'field' => 'patient_id',
                'value' => $p_id
            ]
        ])){
            $waive_fee_id = $this->waive_model->getByFields([
                [
                    'field' => 'doctor_id',
                    'value' => $d_id
                ],
                [
                    'field' => 'patient_id',
                    'value' => $p_id
                ]
            ])[0]->id;
            $this->remove_waive_fee($waive_fee_id, false);
        }
        echo json_encode($fav_html);
    }
	
	public function upcoming_appointment() {
		$pid = $this->session->userdata('userid');
        $this->load->model('knocking_model');
        $this->knocking_model->doctor_unavailable($pid);
		$visitor_time = time();

		$time = date('Y-m-d H:i:s');
        $coming_time = date('Y-m-d H:i:s', strtotime("+5 minutes", $visitor_time));

		$upcoming_ppointment = $this->doctor_model->getUpcomingAppointmentForDoctor($pid, $coming_time, $time);
		$current_ppointment = $this->doctor_model->getCurrentAppointmentForDoctor($pid, $time);
		$diff = '';
        $is_delayed = false;
        $is_action = false;
        $this->load->model('appointmentlog_model');
        if ($upcoming_ppointment) {
            if($this->appointmentlog_model->check_exists([
                'appointment_log.appointment_id' => $upcoming_ppointment->id,
                'appointment_log.status !=' => 'cancelled'
            ])){
                $is_action = true;
                $appointmentlog_data = $this->appointmentlog_model->getByFields([
                    'appointment_log.appointment_id' => $upcoming_ppointment->id,
                    'appointment_log.status !=' => 'cancelled'
                ]);
                if($appointmentlog_data[0]->status == 'delayed'){
                    $line = __LINE__;
                    $is_delayed = true;
                    $diff = date_diff(date_create(date('Y-m-d H:i:s', $appointmentlog_data[0]->stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));

                } else if($appointmentlog_data[0]->status == 'accepted') {
                    $line = __LINE__;
                    $diff = date_diff(date_create(date('Y-m-d H:i:s', $upcoming_ppointment->start_date_stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));
                    $line = __LINE__;
                }
            } else {
                $line = __LINE__;
                $diff = date_diff(date_create(date('Y-m-d H:i:s', $upcoming_ppointment->start_date_stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));
            }
            $upcoming_ppointment->start_time = date('g:i A', ($upcoming_ppointment->start_date_stamp));
            $upcoming_ppointment->end_time = date('g:i A', ($upcoming_ppointment->end_date_stamp));
            $upcoming_ppointment->hover_text = 'Patient Name: ' . $upcoming_ppointment->firstname . " " . $upcoming_ppointment->lastname . "\nAppointment Reason: " . $upcoming_ppointment->reason_name . "\nAppointment Time: " . $upcoming_ppointment->start_time;
        } else if($current_ppointment) {
            if($this->appointmentlog_model->check_exists([
                'appointment_log.appointment_id' => $current_ppointment->id,
                'appointment_log.status !=' => 'cancelled'
            ])){
                $upcoming_ppointment = $current_ppointment;
                $is_action = true;
                $appointmentlog_data = $this->appointmentlog_model->getByFields([
                    'appointment_log.appointment_id' => $upcoming_ppointment->id,
                    'appointment_log.status !=' => 'cancelled'
                ]);
                if($appointmentlog_data[0]->status == 'delayed'){
                    $line = __LINE__;
                    $is_delayed = true;
                    if($appointmentlog_data[0]->stamp > $visitor_time){
                        $diff = date_diff(date_create(date('Y-m-d H:i:s', $appointmentlog_data[0]->stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));
                    } else {
                        $diff = '0';
                    }
                } else if($appointmentlog_data[0]->status == 'accepted') {
                    $line = __LINE__;
//                    $diff = date_diff(date_create(date('Y-m-d H:i:s', $upcoming_ppointment->start_date_stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));
                    $diff = '0';
                }
                $upcoming_ppointment->start_time = date('g:i A', ($upcoming_ppointment->start_date_stamp));
                $upcoming_ppointment->end_time = date('g:i A', ($upcoming_ppointment->end_date_stamp));
                $upcoming_ppointment->hover_text = 'Patient Name: ' . $upcoming_ppointment->firstname . " " . $upcoming_ppointment->lastname . "\nAppointment Reason: " . $upcoming_ppointment->reason_name . "\nAppointment Time: " . $upcoming_ppointment->start_time;
            } else {
                $line = __LINE__;
                $this->appointment_event_trigger(false, $current_ppointment->id);
            }
        } else {
            if($this->appointmentlog_model->check_exists([
                'appointment.doctors_id' => $pid,
                "FROM_UNIXTIME(appointment_log.stamp, '%Y-%m-%d %H:%i:%s') <=" => date('Y-m-d H:i:s', strtotime("+15 minutes", $visitor_time)),
                "FROM_UNIXTIME(appointment_log.stamp, '%Y-%m-%d %H:%i:%s') >=" => $time,
                'appointment_log.status' => 'delayed',
                'appointment_log.status !=' => 'cancelled'
            ])){
                $line = __LINE__;
                $is_action = true;
                $appointmentlog_data = $this->appointmentlog_model->getByFields([
                    'appointment.doctors_id' => $pid,
                    "FROM_UNIXTIME(appointment_log.stamp, '%Y-%m-%d %H:%i:%s') <=" => date('Y-m-d H:i:s', strtotime("+15 minutes", $visitor_time)),
                    "FROM_UNIXTIME(appointment_log.stamp, '%Y-%m-%d %H:%i:%s') >=" => $time,
                    'appointment_log.status' => 'delayed',
                    'appointment_log.status !=' => 'cancelled'
                ]);
                $is_delayed = true;
                $diff = date_diff(date_create(date('Y-m-d H:i:s', $appointmentlog_data[0]->stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));
                $line = __LINE__;
                $upcoming_ppointment = $this->appointment_model->get_appointment_by_id_for_consultation_notification($appointmentlog_data[0]->appointment_id);
                $upcoming_ppointment->start_time = date('g:i A', ($upcoming_ppointment->start_date_stamp));
                $upcoming_ppointment->end_time = date('g:i A', ($upcoming_ppointment->end_date_stamp));
                $upcoming_ppointment->hover_text = 'Patient Name: ' . $upcoming_ppointment->firstname . " " . $upcoming_ppointment->lastname . "\nAppointment Reason: " . $upcoming_ppointment->reason_name . "\nAppointment Time: " . $upcoming_ppointment->start_time;
            } else if($this->appointmentlog_model->check_exists([
                'appointment.doctors_id' => $pid,
                'appointment_log.status' => 'accepted',
                "FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') <=" => date('Y-m-d H:i:s', strtotime("+5 minutes", $visitor_time)),
                "FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') >=" => $time,
                'appointment_log.status !=' => 'cancelled'
            ])) {
                $line = __LINE__;
                $is_action = true;
                $appointmentlog_data = $this->appointmentlog_model->getByFields([
                    'appointment.doctors_id' => $pid,
                    'appointment_log.status' => 'accepted',
                    'appointment_log.status !=' => 'cancelled'
                ]);
                $upcoming_ppointment = $this->appointment_model->get_appointment_by_id_for_consultation_notification($appointmentlog_data[0]->appointment_id);
                $diff = date_diff(date_create(date('Y-m-d H:i:s', $upcoming_ppointment->start_date_stamp)), date_create(date('Y-m-d H:i:s', $visitor_time)));

                $upcoming_ppointment->start_time = date('g:i A', ($upcoming_ppointment->start_date_stamp));
                $upcoming_ppointment->end_time = date('g:i A', ($upcoming_ppointment->end_date_stamp));
                $upcoming_ppointment->hover_text = 'Patient Name: ' . $upcoming_ppointment->firstname . " " . $upcoming_ppointment->lastname . "\nAppointment Reason: " . $upcoming_ppointment->reason_name . "\nAppointment Time: " . $upcoming_ppointment->start_time;
            } else {
                $line = __LINE__;
            }
        }
        $diff_text = '';
        if(!is_string($diff)){
            $minutes = $diff->format('%i');
            $seconds = $diff->format('%s');
            $diff_text = ($minutes * 60) + $seconds;
        } else {
            $diff_text = '';
        }
        $data_send = json_encode([
            'appointment_info' => $upcoming_ppointment,
            'coming_time' => $coming_time,
            'time' => $time,
            'diff' => $diff_text,
            'is_delayed' => $is_delayed,
            'is_action' => $is_action,
            'line' => $line
        ]);
		echo $data_send;
        return $data_send;
	}

	public function appointment_event_trigger($is_echo = true, $aid = 0){
	    if($is_echo){
            $a_id = $this->input->post('id', true);
            $action_type = $this->input->post('action_type', true);
            $reason = $this->input->post('reason', true);
            $stamp = $this->input->post('stamp', true);
        } else {
            $a_id = $aid;
            $action_type = 'cancelled';
            $reason = '';
            $stamp = '';
        }
        $this->load->model('appointmentlog_model');
        $al_id = '';

        if($action_type == 'delayed'){
            $stamp = strtotime('+15 minutes', $stamp);
        }

        if($action_type == 'cancelled'){
            $this->appointment_model->updateById($a_id, array('status' => 5));
        }

        if($this->appointmentlog_model->check_exists([
            'appointment_log.appointment_id' => $a_id
        ])){
//            update this
            $this->appointmentlog_model->update($a_id, [
                'status' => $action_type,
                'reason' => $reason,
                'stamp' => $stamp
            ]);
        } else {
//            add new
            $al_id = $this->appointmentlog_model->add([
                'appointment_id' => $a_id,
                'status' => $action_type,
                'reason' => $reason,
                'stamp' => $stamp
            ]);
        }
        if($is_echo){
            echo json_encode([
                'msg' => ($action_type == 'cancelled') ? 'Appointment has been cancelled and patient has been notified' : 'Appointment time has been delayed and patient has been notified',
                'status' => 'success'
            ]);
        }
    }

	public function get_appointment_log_test(){
        echo "<pre>";
        print_r($this->appointment_model->get_appointment_log_a_id(
            91,
            '2016-09-27 09:24',
            '2016-09-27 09:04'
        ));
        die();
    }
	
	function cancel_appointment()
	{
		$id = $this->input->post('id', true);

		$reason = $this->input->post('reason', true);
		$this->db->set('appointment_id', $id);
		$this->db->set('reason', $reason);
        $now = date('Y-m-d H:i:s', time());
		$this->db->set('created_at', $now);
		$this->db->set('status', 'cancelled');
		$this->db->insert('appointment_log');
		
		$this->appointment_model->updateById($id, array('status' => 5));
		echo json_encode(array('status' => 'success', 'msg' => 'Appointment has been cancelled and patient has been notified'));
	}
	
	function delay_appointment()
	{
		$id = $this->input->post('id', true);
		$stamp = $this->input->post('stamp', true);

		$reason = $this->input->post('reason', true);
		$this->db->set('appointment_id', $id);
		$this->db->set('reason', $reason);
        $now = date('Y-m-d H:i:s', time());
        $this->db->set('created_at', $now);
		$this->db->set('status', 'delayed');
		$this->db->insert('appointment_log');
        $this->appointment_model->updateById($id, array('is_delayed' => 1,
            'delay_time' =>  strtotime('+15 minutes', $stamp)));
		
		/*$time = strtotime(date('Y-m-d H:i'));
		$coming_time = strtotime("+15 minutes", $time);
		$this->appointment_model->updateById($id, array('start_date_stamp' => $coming_time ));*/
		echo json_encode(array('status' => 'success', 'msg' => 'Appointment time has been delayed and patient has been notified'));
	}
	
	function invite_patients() {
		$config['upload_path'] = SECURE_UPLOAD . '/temp/';
		$config['allowed_types'] = 'csv';
		$config['encrypt_name'] = true;
		$config['max_size'] = 1024 * 10;
		
		$this->load->library('upload', $config);
		$this->load->helper('security_helper');
		
		ini_set('memory_limit', '128M');

		if (!is_dir($config['upload_path'])) {
			if (!mkdir($config['upload_path'], 0777, true)) {
//				echo json_encode(array('errors' => 'Failed to create directory'));
				die('Failed to create directory');
			}
		}
		
//		$post = $this->input->post(null, true);
//		var_debug($post);
//		dd($_FILES);
		
		if (isset($_FILES['file']['error']) && $_FILES['file']['error'] == 4) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|xss_clean');
			$this->form_validation->set_rules('first_name', 'First Name', 'required|trim|xss_clean');
			$this->form_validation->set_rules('last_name', 'Last Name', 'required|trim|xss_clean');
			$this->form_validation->set_rules('message', 'Message', 'required|trim|xss_clean');
			
			if ($this->form_validation->run() == FALSE) {
				$this->output->set_status_header(400);
				echo json_encode(array('status' => 'error', 'msg' => validation_errors('', '')));
				return ;
			}
			
			$this->load->library('email');
			$this->email->clear();
			$this->email->from($this->session->userdata('email'), $this->session->userdata('full_name'));
			$this->email->to($this->input->post('email', true), $this->input->post('first_name', true) . ' ' . $this->input->post('last_name', true));
			$this->email->subject('A doctor is looking to connect with you via a virtual consultation');
			$this->email->message(str_replace("Thank you", 'Click <a href="'.base_url().'" >here</a> to join. Thank you', $this->input->post('message', true)));

			if (!$this->email->send()) {
				$this->output->set_status_header(400);
				echo json_encode(array('status' => 'error', 'msg' => "There is a problem occurred while sending invitation. Please try again."));
				return ;
			}
		} else {
			if (!$this->upload->do_upload('file')) {
				$this->output->set_status_header(400);
				echo json_encode(array('status' => 'error', 'msg' => $this->upload->display_errors(' ', '<br>') ));
				return;
			} else {
				$data = $this->upload->data();
				$array = array_map('str_getcsv', file($data['full_path']));
	//			var_debug($array);
				foreach ($array as $key => $value) {
//					var_debug($value);
					if ($key > 0) {
						$count = count($value);
						if ($count != 3) {
							$this->output->set_status_header(400);
							echo json_encode(array('status' => 'error', 'msg' => "CSV file should have 3 columns. Your file has $count columns",));
							@unlink($data['full_path']);
							return ;
						}
						$first_name = xss_clean($value[0]);
						$array[$key][0] = $first_name;
						
						$last_name = xss_clean($value[1]);
						$array[$key][1] = $last_name;
						
						$email_clean = xss_clean($value[2]);
						$email = filter_var($email_clean, FILTER_VALIDATE_EMAIL);
						
						$message = $this->input->post('message', true);

						if (!$email) {
							$this->output->set_status_header(400);
							$row = $key;
							echo json_encode(array('status' => 'error', 'msg' => "Email address ($email_clean) at row number $row is not valid."));
							@unlink($data['full_path']);
							return ;
						}
						$array[$key][2] = $email;
					}
				}
//				dd($array);
				foreach ($array as $key => $row) {
//					dd($this->session->userdata('full_name'));
					if ($key > 0) {
						$this->load->library('email');
						$this->email->clear();
						$this->email->from($this->session->userdata('email'), $this->session->userdata('full_name'));
						$this->email->to($row[2], $row[0] . ' ' . $row[1]);
						$this->email->subject('A doctor is looking to connect with you via a virtual consultation');
						$this->email->message($message);

						if (!$this->email->send()) {
							$this->output->set_status_header(400);
							echo json_encode(array('status' => 'error', 'msg' => "There is a problem occurred while sending invitation. Please try again."));
							@unlink($data['full_path']);
							return ;
						}
					}
				}
			}
			@unlink($data['full_path']);
		}
		
		$this->email->start_process();
		
		echo json_encode(array('status' => 'success', 'msg' => 'Invitation(s) sent successfully'));
	}

	public function start_consult($a_id = null){
	    if($this->appointment_model->check_appointment_available($a_id, date('Y-m-d H:i:s', time()), date('Y-m-d H:i:s', time()), 'id', 'OR') == 0){
            // check knocking system is completed (doctor is available and patient approved to come in consultation page)
            $this->load->model('knocking_model');
            $knocking_data = $this->knocking_model->check_patient_approval($a_id);
            if(!$knocking_data){
                echo json_encode([
                    'message' => 'Consultation cannot be started before the set time!',
                    'bool' => false
                ]);
                return;
            }
        }
	    $this->appointment_model->updateById($a_id, [
	        'is_doctor_ready' => 1
        ]);
        echo json_encode([
            'message' => '',
            'bool' => true
        ]);
        return;
    }

	public function end_consult($a_id = null){
	    $this->load->model('appointmentlog_model');
	    $session_start_stamp = $this->appointmentlog_model->getByFields([
	        'appointment_id' => $a_id
        ])[0]->session_start_stamp;
        $current_time = time();
        $interval = date_diff(date_create(date('Y-m-d H:i:s', $session_start_stamp)), date_create(date('Y-m-d H:i:s', $current_time)));
        $hour = $interval->format('%H');
        $minute = $interval->format('%i');
        $second = $interval->format('%s');
        $time_seconds = $hour * 3600 + $minute * 60 + $second;
        $this->appointment_model->updateById($a_id, [
            'is_doctor_ready' => 2,
            'status' => 6,
            'duration' => $time_seconds
        ]);
    }

}
