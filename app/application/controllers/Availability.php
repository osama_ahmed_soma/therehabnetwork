<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Availability extends Auth_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('availability_model');
        $this->load->model('doctor_model');
        $this->load->model('appointment_model');
    }

    public function index()
    {
        /* $data = array(
          'name' => $this->input->post('name'),
          'country'=> $this->input->post('country')
          ); */
        //$name=$this->input->post('end_time');
        //$country=$this->input->post('country');
        $all = $this->availability_model->availability_doctors();
        //echo json_encode($data);
        /*if ($all) {
            echo "Availability Added";
        } else {
            echo "Availability Not Added";
        }*/
        echo $all;
    }

    public function add_new() {
        //echo json_encode( $this->input->post(NULL, true) );
        $all = $this->availability_model->availability_doctors();
        echo json_encode($all);
//        print_r(count($all));
    }

    public function get_available()
    {
        $all = $this->availability_model->available_time();
//        $appointments = $this->doctor_model->getDoctorAppoinmentByAvailabelID($this->session->userdata('userid'));
        /*echo "<pre>";
        print_r($appointments);
        die();*/
        $result = [];
        if (is_array($all)) {
            $cn = 0;
            foreach ($all as $val) {
                // break start date time and end date time into 30 minutes interval
                $date_ar = [];
                $i = 0;
                if ( round(abs($val->end_date_stamp - $val->start_date_stamp) / 60,2) > 30){
                    $tmp_start_date = $val->start_date_stamp;
                    $tmp_end_date = strtotime('+ 30 minutes', $val->start_date_stamp);
                    while( $tmp_end_date <= $val->end_date_stamp ){
                        if ( $this->appointment_model->check_appointment_available($this->session->userdata('userid'), $tmp_start_date, $tmp_end_date) === 0 ){
                            if(array_key_exists($i, $date_ar)){
                                // $i is exists it's mean this is repeat
                                $date_ar[$i]['end_stamp'] = strtotime('+30 minutes', $tmp_start_date);
                                $date_ar[$i]['end'] = date("Y-m-d", (strtotime('+30 minutes', $tmp_start_date))).'T'.date("H:i:s", (strtotime('+30 minutes', $tmp_start_date)));
                            } else {
                                // $i is not exists first time
                                $date_ar[$i] = [
                                    'start_stamp' => $tmp_start_date,
                                    'end_stamp' => strtotime('+30 minutes', $tmp_start_date),
                                    'title' => 'Available Time',
                                    'class' => 'not_appointed',
                                    'innerText' => '',
                                    'status' => '',
                                    'id' => $val->id,
                                    'start' => date("Y-m-d", ($tmp_start_date)).'T'.date("H:i:s", ($tmp_start_date)),
                                    'end' => date("Y-m-d", (strtotime('+30 minutes', $tmp_start_date))).'T'.date("H:i:s", (strtotime('+30 minutes', $tmp_start_date)))
                                ];
                            }
                        } else {
                            $i++;
                            $appintments = $this->availability_model->getDoctorAppoinmentByAvailabelID($this->session->userdata('userid'), $val->id, $tmp_start_date, $tmp_end_date);
                            foreach ($appintments as $value){
                                if($value->app_status == 1){
                                    $date_ar[$i] = [
                                        'start_stamp' => $tmp_start_date,
                                        'end_stamp' => strtotime('+30 minutes', $tmp_start_date),
                                        'title' => 'Session',
                                        'class' => 'appointed pending',
                                        'innerText' => 'Session with '.$value->firstname.' '.$value->lastname.' (Pending Approval) for '.$value->reason_name,
                                        'status' => $value->app_status,
                                        'id' => $val->id,
                                        'start' => date("Y-m-d", ($tmp_start_date)) . 'T' . date("H:i:s", ($tmp_start_date)),
                                        'end' => date("Y-m-d", (strtotime('+30 minutes', $tmp_start_date))) . 'T' . date("H:i:s", (strtotime('+30 minutes', $tmp_start_date)))
                                    ];
                                } else {
                                    $date_ar[$i] = [
                                        'start_stamp' => $tmp_start_date,
                                        'end_stamp' => strtotime('+30 minutes', $tmp_start_date),
                                        'title' => 'Session',
                                        'class' => 'appointed',
                                        'innerText' => 'Session with '.$value->firstname.' '.$value->lastname.' for '.$value->reason_name,
                                        'status' => $value->app_status,
                                        'id' => $val->id,
                                        'start' => date("Y-m-d", ($tmp_start_date)) . 'T' . date("H:i:s", ($tmp_start_date)),
                                        'end' => date("Y-m-d", (strtotime('+30 minutes', $tmp_start_date))) . 'T' . date("H:i:s", (strtotime('+30 minutes', $tmp_start_date)))
                                    ];
                                }
                            }
                            $i++;
                        }
                        $tmp_start_date = $tmp_end_date;
                        $tmp_end_date = strtotime('+30 minutes', $tmp_start_date);
                    }
//                    $temp_ar[$cn] = $date_ar;
                } else {
                    // no break needed
                    $i++;
                    if ( $this->appointment_model->check_appointment_available($this->session->userdata('userid'), $val->start_date_stamp, $val->end_date_stamp) === 0 ){
                        $date_ar[$i] = [
                            'start_stamp' => $val->start_date_stamp,
                            'end_stamp' => $val->end_date_stamp,
                            'title' => 'Available Time',
                            'class' => 'not_appointed',
                            'innerText' => '',
                            'status' => '',
                            'id' => $val->id,
                            'start' => date("Y-m-d", ($val->start_date_stamp)).'T'.date("H:i:s", ($val->start_date_stamp)),
                            'end' => date("Y-m-d", ($val->end_date_stamp)).'T'.date("H:i:s", ($val->end_date_stamp))
                        ];
                    } else {
                        $appintments = $this->availability_model->getDoctorAppoinmentByAvailabelID($this->session->userdata('userid'), $val->id, $val->start_date_stamp, $val->end_date_stamp);
                        foreach ($appintments as $value){
                            if($value->app_status == 1){
                                $date_ar[$i] = [
                                    'start_stamp' => $val->start_date_stamp,
                                    'end_stamp' => $val->end_date_stamp,
                                    'title' => 'Session',
                                    'class' => 'appointed pending',
                                    'innerText' => 'Session with '.$value->firstname.' '.$value->lastname.' (Pending Approval) for '.$value->reason_name,
                                    'status' => $value->app_status,
                                    'id' => $val->id,
                                    'start' => date("Y-m-d", ($val->start_date_stamp)) . 'T' . date("H:i:s", ($val->start_date_stamp)),
                                    'end' => date("Y-m-d", ($val->end_date_stamp)) . 'T' . date("H:i:s", ($val->end_date_stamp))
                                ];
                            } else {
                                $date_ar[$i] = [
                                    'start_stamp' => $val->start_date_stamp,
                                    'end_stamp' => $val->end_date_stamp,
                                    'title' => 'Session',
                                    'class' => 'appointed',
                                    'innerText' => 'Session with '.$value->firstname.' '.$value->lastname.' for '.$value->reason_name,
                                    'status' => $value->app_status,
                                    'id' => $val->id,
                                    'start' => date("Y-m-d", ($val->start_date_stamp)) . 'T' . date("H:i:s", ($val->start_date_stamp)),
                                    'end' => date("Y-m-d", ($val->end_date_stamp)) . 'T' . date("H:i:s", ($val->end_date_stamp))
                                ];
                            }
                        }
                    }
                    $i++;
                }
                $result[$cn] = $date_ar;



                /*$result[$cn]['title'] = "Available Time";
                $result[$cn]['class'] = 'not_appointed';
                $result[$cn]['innerText'] = '';
                $result[$cn]['status'] = '';
                $result[$cn]['start_stamp'] = $val->start_date_stamp;
                $result[$cn]['end_stamp'] = $val->end_date_stamp;
                $result[$cn]['start'] = date("Y-m-d", ($val->start_date_stamp)).'T'.date("H:i:s", ($val->start_date_stamp));
                $result[$cn]['end'] = date("Y-m-d", ($val->end_date_stamp)).'T'.date("H:i:s", ($val->end_date_stamp));
                foreach ($appointments as $index => $value){
                    if($val->id == $value->available_id){
                        $result[$cn]['status'] = $value->app_status;
                        $result[$cn]['title'] = "Session";
                        if($value->status == 1){
                            $result[$cn]['class'] = 'appointed pending';
                            $result[$cn]['innerText'] = 'Session with '.$value->firstname.' '.$value->lastname.' (Pending Approval) for '.$value->reason_name;
                        } else {
                            $result[$cn]['class'] = 'appointed';
                            $result[$cn]['innerText'] = 'Session with '.$value->firstname.' '.$value->lastname.' for '.$value->reason_name;
                        }
                    }
                }
                $result[$cn]['id'] = $val->id;
                $cn = $cn + 1;*/
                $cn = $cn + 1;
            }
            $new_result = [];
            foreach ($result as $key => $value) {
                foreach ($value as $ind => $val) {
                    $new_result[] = $val;
                }
            }
            echo json_encode($new_result);
			die;
            /*echo "<pre>";
            print_r($new_result);
            die();*/
        } else {
            echo "not inserted";
        }
    }

    public function update(){
        $update = $this->availability_model->update_time();
        echo $update;
    }

    public function remove(){
        $this->availability_model->remove_time();
    }

}

?>
