<?php
/**
 * Description of Blog
 *
 * @author User
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Blog extends Auth_Controller
{

    //put your code here
    public function index()
    {
        $bPost = array(
            'all_posts' => $this->login_model->get_all_blogposts()
        );
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("blog/index", $bPost);
        $this->load->view("footer");
    }

    public function post($post_id)
    {
        $blog = array();
        $blog['one_blog'] = $this->login_model->post($post_id);
        $blog['comments'] = $this->login_model->comment_view($post_id);
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("blog/single", $blog);
        $this->load->view("footer");
    }

    //unpinned data passing
    public function pinnedpost($post_id)
    {
        $blog = array();
        $blog['one_blog'] = $this->login_model->post($post_id);
        $blog['comments'] = $this->login_model->comment_view($post_id);
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("blog/single_unpin", $blog);
        $this->load->view("footer");
    }

    public function pined()
    {
        $pinpost_id = $this->input->post("pin_id");
        $pin = $this->login_model->pin_post($pinpost_id);
        echo $pin;
    }

    public function updata_pin()
    {
        $upin_id = $this->input->post("udpin_id");
        $uppin = $this->login_model->pin_post_updata($upin_id);
        echo $uppin;
    }

    public function single_comments()
    {
        $post_id = $this->input->post('sig_post_id');
        $comment_id = $this->input->post('sig_comment');
        $sing_comment = $this->login_model->sinlge_comment_insert($post_id, $comment_id);
        echo $sing_comment;
    }

    public function pined_posts()
    {
        $pined_catch['pinned_one'] = $this->login_model->pinned_data_view();
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("blog/pinned_view", $pined_catch);
        $this->load->view("footer");
    }

}
