<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Class Doctor_search
 * @property Login_model $login_model
 * @property Doctor_Model $doctor_model
 * @property Reason_Model $reason_model
 * @property Language_Model $language_model
 * @property CI_Input $input
 * @property CI_Loader $load
 */
class Doctor_search extends Auth_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('doctor_model');
        $this->load->model('reason_model');
        $this->load->model('language_model');
    }


    public function index()
    {
        $speciality = $this->input->get('speciality');
        $name = $this->input->get('name');
        $stateId = $this->input->get('state_id');
        $doctors = $this->doctor_model->getListByStateOrSpecialityOrName($name, $speciality, $stateId);
        $data['doctors'] = array_map("unserialize", array_unique(array_map("serialize", $doctors)));
        foreach ($data['doctors'] as $index => $doctor){
            $data['doctors'][$index]->specials = $this->login_model->get_doctors_specialization($doctor->userid);
            $data['doctors'][$index]->degrees = $this->doctor_model->get_doctors_degree($doctor->userid);
        }
        if (!$data['doctors']) {
            echo "<span class='doctor_search_result_bottom_text'>No Doctors Found matching this search. If you cant find your doctor then click <a href='' data-toggle=\"modal\" data-target=\"#invite_doctor_after_search_modal\">here</a> to invite him/her to My Virtual Doctor, it's very easy!</span>";

            return;
        }

        $this->load->view("doctor/doctor_search", $data);
    }

    public function doctorinfo($id)
    {

        $languages = $this->login_model->get_doctors_language($id);
        $langIds = [];
        foreach ($languages as $lang) {
            $langIds[] = $lang->language_id;
        }
        $languages = $this->language_model->getByIds($langIds);

        $data['reasons'] = $this->reason_model->getList();
        $data['patients'] = $this->login_model->get_patients();
        $data['doctors'] = $this->login_model->get_doctors($id);
        $data['availabilities'] = $this->login_model->get_doctors_available($id, date("m"), date("Y-m-d"));
        $data['specials'] = $this->login_model->get_doctors_specialization($id);
        $data['degrees'] = $this->login_model->get_doctors_degree($id);
        $data['experiences'] = $this->login_model->get_doctors_experience($id);
        $data['educations'] = $this->login_model->get_doctors_education($id);
        $data['languages'] = $languages;
        $data['awards'] = $this->login_model->get_doctors_awards($id);
        $data['licenced_states'] = $this->login_model->get_doctors_licenced_states($id);
        $data['memberships'] = $this->login_model->get_doctors_professiona_membership($id);
        $data['fav'] = $this->login_model->get_favorite($id);
        $data['reasons'] = $this->login_model->get_reasons();
        $data['id'] = $id;
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('nav');
        $this->load->view("patient/doctor_profile_for_appointment", $data);
        $this->load->view("footer");
        /*
                $fname = $this->input->post('fname');
                $lname = $this->input->post('lname');
                $email = $this->input->post('email');
                $gender = $this->input->post('gender');
                $dob = $this->input->post('dob');
                $timezone = $this->input->post('timezone');
                $state = $this->input->post('state');
                $city = $this->input->post('city');
                $zip = $this->input->post('zip');
                $phone_home = $this->input->post('phone-office');
                $phone_mobile = $this->input->post('phone-mobile');
                $fax = $this->input->post('fax');
                $pre = $this->input->post('pre_education');
                //$intern = $this->input->post('intern');
                //$country = $this->input->post('country');
                $pratice = $this->input->post('practicing');
                $residency = $this->input->post('residency');
                $dept = $this->input->post('dept');
                $specialty = $this->input->post('specialty');
                $s_specialty = $this->input->post('s_specialty');
                $m_license = $this->input->post('m_license');
                $s_license = $this->input->post('s_license');



                $config['upload_path'] = 'uploads/doctors/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = 1024;
                $config['max_width'] = 1024;
                $config['max_height'] = 768;
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('doctor_image')) {
        //            $error = array('data' => $this->upload->display_errors());
                    //$this->load->view('profile/file', $error);
                    $image_url = $this->input->post('doctor_image_db');
                } else {
                    $fdata = $this->upload->data();
                    $image_url = $config['upload_path'] . $fdata['file_name'];
                }

                $data = array(
                    'firstname' => $fname,
                    'lastname' => $lname,
                    'email' => $email,
                    'timezone' => $timezone,
                    'gender' => $gender,
                    'dob' => $dob,
                    'state' => $state,
                    'city' => $city,
                    'zip' => $zip,
                    'phone_home' => $phone_home,
                    'phone_mobile' => $phone_mobile,
                    'fax' => $fax,
                    'pre_medical_education' => $pre,
                    'practicing_since' => $pratice,
                    'residency' => $residency,
                    'department' => $dept,
                    'specialty' => $specialty,
                    'sub_specialty' => $s_specialty,
                    'npi' => $m_license,
                    'state_license' => $s_license,
                    'image_url' => $image_url
                );


                if ($this->input->post("save") == "1") {
                    $this->db->where('userid', $id);
                    $this->db->update('doctor', $data);
                }
                $data['patients'] = $this->login_model->get_patients();
                $data['doctors'] = $this->login_model->get_doctors($id);
                $data['availabilities'] = $this->login_model->get_doctors_available($id, date("m"), date("Y-m-d"));
                $data['specials'] = $this->login_model->get_doctors_specialization($id);
                $data['degrees'] = $this->login_model->get_doctors_degree($id);
                $data['experiences'] = $this->login_model->get_doctors_experience($id);
                $data['educations'] = $this->login_model->get_doctors_education($id);
                $data['languages'] = $this->login_model->get_doctors_language($id);
                $data['languages'] = $this->login_model->get_doctors_language($id);
                $data['awards'] = $this->login_model->get_doctors_awards($id);
                $data['licenced_states'] = $this->login_model->get_doctors_licenced_states($id);
                $data['memberships'] = $this->login_model->get_doctors_professiona_membership($id);
                $data['fav'] = $this->login_model->get_favorite($id);
                $data['id'] = $id;
                $this->load->view('header');
                $this->load->view('doctor_header_top');
                $this->load->view('doctor_nav');
                $this->load->view("doctor/index", $data);
                $this->load->view("footer");
        */

        /*        $data = array();
                $data['doctors'] = $this->login_model->get_doctors($id);
                $data['specials'] = $this->login_model->get_doctors_specialization($id);
                $data['degrees'] = $this->login_model->get_doctors_degree($id);
                $data['experiences'] = $this->login_model->get_doctors_experience($id);
                $data['educations'] = $this->login_model->get_doctors_education($id);
                $data['languages'] = $this->login_model->get_doctors_language($id);
                $data['languages'] = $this->login_model->get_doctors_language($id);
                $data['awards'] = $this->login_model->get_doctors_awards($id);
                $data['memberships'] = $this->login_model->get_doctors_professiona_membership($id);
                $data['licenced_states'] = $this->login_model->get_doctors_licenced_states($id);
                $data['fav'] = $this->login_model->get_favorite($id);
                //$data['doctor'] = $this->login_model->get_doctor($doctor_id);
                $this->load->view('header');
                $this->load->view('header_top');
                $this->load->view('nav');
                $this->load->view("doctor/index",$data);
                $this->load->view("footer");*/

    }

}

?>
