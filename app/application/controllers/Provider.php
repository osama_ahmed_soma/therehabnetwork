<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Provider extends Auth_Controller
{

    function __construct()
    {
        parent::__construct();
        //$this->is_logged_in();
        //$this->load->model('login_model');
    }


    public function add_to_fav()
    {
        $p_id = $this->input->post('p_id');
        $d_id = $this->input->post('d_id');
        $data = $this->login_model->add_to_fav($p_id, $d_id);
        echo $data;
    }

    public function remove_from_fav()
    {
        $d_id = $this->input->post('d_id');
        $data = $this->login_model->remove_from_fav($d_id);
        if ($data) {
            $result = array(
                'text' => "Removed From Provider",
                'decision' => TRUE,
            );
            echo json_encode($result);
        } else {
            $result = array(
                'text' => "Something Wrong",
                'decision' => FALSE,
            );
            echo json_encode($result);
        }
    }

}

?>