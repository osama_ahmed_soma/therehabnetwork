<?php

/**
 * Class Register
 *
 * @property CI_Form_validation $form_validation
 * @property CI_DB_query_builder $db
 * @property State_Model $state_model
 * @property Patient_model $patient_model
 * @property Doctor_Model $doctor_model
 * @property Login_model $login_model
 * @property Specialty_Model $specialty_model
 * @property Language_Model $language_model
 *
 * @property CI_Input $input
 * @property CI_Session $session
 *
 */
class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('logged_in')) {
            if ($this->session->userdata('type') == 'patient') {
                redirect('/patient');
            } else {
                redirect('/home/doctor');
            }
        }

        $this->load->model('state_model');
        $this->load->model('patient_model');
        $this->load->model('doctor_model');
        $this->load->model('login_model');
        $this->load->model('specialty_model');
        $this->load->model('language_model');

        $this->load->helper('captcha');
        captcha_required(false);
    }

    public function index()
    {
        $this->load->view('header');
        $this->load->view("register/index");
        $this->load->view("footer");
    }

    public function patient()
    {
        $email = $this->input->post('email');
        $username = $this->input->post('username');
        $password = password($this->input->post('password', true));

        $user = array(
            'user_email' => $email,
            'user_name' => $username,
            'user_password' => $password,
            'type' => "patient"
        );

        /*
//		I think we are not using this condition for saving, we using patient_create()
        if ($this->input->post("save") == "1") {
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == true) {
                $this->db->insert('user', $user);
                $insert_id = $this->db->insert_id();
                $data = array(
                    'email' => $email,
                    'user_id' => $insert_id
                );
                $this->db->insert('patient', $data);
                $this->session->set_flashdata('message', 'Successfully Registered. Please Login with correct info....');
                redirect('/home');
                $this->load->view('header');
                //$this->load->view('header_top');
                //$this->load->view('nav');
                $this->load->view("signin/signin");
                $this->load->view("footer");
            }
        }
        */
        $this->load->view('header');
        //$this->load->view('header_top');
        //$this->load->view('nav');
        $this->load->view("patient/registration");
        $this->load->view("footer");
    }

    public function doctor($company = null)
    {
        $viewData['states'] = $this->state_model->getList();
        $viewData['specialties'] = $this->doctor_model->getSpecialtyList();
        $viewData['languages'] = $this->language_model->getList();
        $company_id = 0;
        if ($company != null) {
            $this->load->model('company_model');
            $company_id = $this->company_model->getByField('code', $company)->id;
        }
        $viewData['company_id_ref'] = $company_id;

        $this->load->view('header');
        $this->load->view("doctor/registration", $viewData);
        $this->load->view("footer");
    }

    public function doctorCreate()
    {
        $company_id_ref = $this->input->post('company_id_ref');
        $email = $this->input->post('email');

//		dd($GLOBALS['captcha_error']);

        if (!empty($GLOBALS['captcha_error'])) {
            redirect('/register/doctor');
        }

        if ($this->user_model->email_exists($email)) {
            $this->session->set_flashdata('error', 'This email address is already in use, please use another email address to signup');
            redirect('/register/doctor');
        }

        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
//        $p_title = $this->input->post('p_title');
//        list($specialtyId, $specialty) = explode(":", $this->input->post('specialty'));
//        $otherSpecialty = $this->input->post('otherSpecialty');
//        if ($otherSpecialty != "") {
//            $specialtyId = $this->specialty_model->create($otherSpecialty);
//            $specialty = $otherSpecialty;
//        }
//        $languages_array = $this->input->post('language');
//        $languageIds = [];
//        $languages = [];
//        foreach($languages_array as $value){
//            $languageIds[] = explode(":", $value)[0];
//            $languages[] = explode(":", $value)[1];
//        }
//        $otherLanguage = $this->input->post('otherLanguage');
//        if ($otherLanguage != "") {
//            $languageIds = array_merge($languageIds, $this->language_model->addLanguage($otherLanguage));
//        }
//        $board_certified = $this->input->post('board_certified');
//        if($board_certified == ''){
//            $board_certified = 1;
//        }
//        $years_in_practic = $this->input->post('years_in_practic');
//        $phone = $this->input->post('phone');
//        list($stateId, $state) = explode(":", $this->input->post('state'));
//        $other_licenses_array = $this->input->post('other_licenses');
//        $other_licenses_id = [];
//        $other_licenses = [];
//        foreach($other_licenses_array as $value){
//            $other_licenses_id[] = explode(":", $value)[0];
//            $other_licenses[] = explode(":", $value)[1];
//        }
        $password = random_string();

        $this->load->library('email');

        $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
        $this->email->to($email);

        $this->email->subject('Get Started With My Virtual Doctor');


        // getting template
        $body = $this->db->get_where('email_templates', ['key' => 'doctor_signup'])->row('body');

        $message = str_replace(['{FULL_NAME}', '{URL}', '{EMAIL}', '{PASSWORD}'], [$fname . ' ' . $lname, base_url('login'), $email, $password], $body);
//		echo $message;
//		die;

        $this->email->message($message);

//        $this->email->message('
//<html>
//<head>
//    <title></title>
//</head>
//<body>
//    <h4>Dear '.$fname.' '.$lname.'</h4>
//    <p></p>
//    <p>Your account has been successfully created and you can now login to our doctor portal using the following information</p>
//    <p></p>
//    <p>Email: <strong>'.$email.'</strong></p>
//    <p>Password: <strong>'.$password.'</strong></p>
//    <p></p>
//    <p>Click this link to login <a href="http://www.myvirtualdoctor.com/app/login">http://www.myvirtualdoctor.com/app/login</a></p>
//    <p></p>
//    <p>Please don\'t hesitate to contact us at  1-855-80-DOCTOR or at info@myvirtualdoctor.com for further assistance.</p>
//    <p></p>
//    <p>Sincerely,<br />
//My Virtual Doctor</p>
//</body>
//</html>');

        if (!$this->email->send(true, true)) {
            $this->session->set_flashdata('error', 'There is some problem while mailing to you. Please try again.');
            redirect('/register/doctor');
        }

        $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
        $this->email->to('info@myvirtualdoctor.com');

        $this->email->subject('New Doctor Signup (' . $fname . ' ' . $lname);
        $this->email->message('Dear Administrator,
            <br />
            <br />
            A doctor just signed up to the system and here is the information:
            <br />
            <br />
            First Name: ' . $fname . '
            <br />
            Last Name: ' . $lname . '
            <br />
            Email: ' . $email . '
            <br />
            <br />
            Thank You.');

        if (!$this->email->send(true, true)) {
            $this->session->set_flashdata('error', 'There is some problem while mailing to admin. Please try again.');
            redirect('/register/doctor');
        }

        $result = $this->doctor_model->createDoctor(
            $fname,
            $lname,
            $email,
            password($password),
            $company_id_ref
        );
        if ($result) {
            $this->session->set_flashdata('register_doctor_success', 'An email has been sent to you with your login and password. Once you receive the email please click <a href="' . site_url('login') . '">here</a> to login to the doctor portal.');
            redirect('/register/doctor');
        }

        redirect('/register/doctor');
    }

    public function testMail()
    {
        echo 123;
        $mail = new PHPMailer();

//        $mail->isSMTP();                                      // Set mailer to use SMTP
//        $mail->Host = 'email-smtp.us-east-1.amazonaws.com';  // Specify main and backup SMTP servers
//        $mail->SMTPAuth = true;                               // Enable SMTP authentication
//        $mail->Username = 'AKIAJLZ2AVD5QZ33ZJIA';                 // SMTP username
//        $mail->Password = 'AkHbDg9s3SZf6LfBuENyQvDt86EDEUhoAOb0+KFOrjpK';                           // SMTP password
//        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
//        $mail->Port = 587;                                    // TCP port to connect to

        $mail->isSMTP();
        $mail->SMTPDebug = 2;
        $mail->Host = "smtp.gmail.com";
        $mail->Username = "info@myvirtualdoctor.com";
        $mail->Passwordd = "Jacobryan17";
        $mail->SMTPSecure = true;
        $mail->Port = 465;
        $mail->SMTPAuth = true;
        $mail->IsHTML(true);

        $mail->setFrom('info@myvirtualdoctor.com', '', false);
        $mail->addAddress('info@myvirtualdoctor.com', 'Ramil Amerzyanov');     // Add a recipient

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'Here is the subject';
        $mail->Body = 'This is the HTML message body <b>in bold!</b>';
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        if (!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
    }

    function patient_create()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('userFname', 'First Name', 'required|trim');
        $this->form_validation->set_rules('userLname', 'Last Name', 'required|trim');
        $this->form_validation->set_rules('userEmail', 'Email address', 'required|trim|valid_email|is_unique[user.user_email]');
        $this->form_validation->set_rules('consent', 'Terms', 'required|trim', ['required' => 'Please agree with Terms of Use']);
        $this->form_validation->set_error_delimiters('', '<br>');

        if (!empty($GLOBALS['captcha_error'])) {
            $this->output->set_status_header(400);
            echo json_encode(array('status' => 'error', 'msg' => 'Human verification failed <br>'));
            return;
        }

        if ($this->form_validation->run() == FALSE) {
            $this->output->set_status_header(400);
            echo json_encode(array('status' => 'error', 'msg' => validation_errors()));
            return;
        }

        $password = random_string();
        $userFname = $this->input->post('userFname', true);
        $userLname = $this->input->post('userLname', true);
        $userEmail = $this->input->post('userEmail', true);

        $check = $this->patient_model->createPatient($userFname, $userLname, $userEmail, password($password));

        if (!$check) {
            $this->output->set_status_header(400);
            echo json_encode(array('status' => 'error', 'msg' => 'Something went wrong in creating patient.'));
            return;
        }

        $this->load->library('email');
        $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
        $this->email->to($userEmail, $userFname . ' ' . $userLname);

        $this->email->subject('Welcome to Myvirtualdoctor.com');
        $message = "Dear " . $userFname . " " . $userLname . ",
            <br /><br />
            Thank you for signing up on My Virtual Doctor.
            <br /><br />
            Your login is <strong>" . $userEmail . "</strong> and password is <strong>" . $password . "</strong>
            <br /><br />
            Please use the following link in order to login to the patient portal
            <br /><br />
            <a href=\"http://www.myvirtualdoctor.com/app/login\">Login Here</a>
            <br /><br />
            Thank you,";
//		echo $message;
//		die;
        $this->email->message($message);

        if (!$this->email->send(true, true)) {
            echo json_encode(array('status' => 'error', 'msg' => 'Something went wrong in sending email'));
            return;
        }

        $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
        $this->email->to($userEmail, $userFname . ' ' . $userLname);

        // getting template
        $template = $this->db->get_where('email_templates', ['key' => 'patient_signup_getting_started'])->row();

        $this->email->subject($template->subject);

        $message = str_replace(['{FULL_NAME}', '{URL}', '{FAQ_URL}', '{EMAIL}', '{PASSWORD}'], [$userFname . ' ' . $userLname, base_url('login'), base_url() . '../faqs.html', $userEmail, $password], $template->body);
//		echo $message;
//		die;s

        $this->email->message($message);

        if (!$this->email->send(true, true)) {
            echo json_encode(array('status' => 'error', 'msg' => 'Something went wrong in sending email'));
            return;
        }
        echo json_encode(array('status' => 'success', 'msg' => 'Thank you for signing up! We just sent you an email with your new password for logging into My Virtual Doctor. Once you receive the email, you may login by clicking this link <a href="' . base_url() . '/login">Here</a>'));

    }

    public function create_patient_api()
    {
        error_reporting(0);
        $password = random_string();
        $userFname = $_REQUEST['userFname'];
        $userLname = $_REQUEST['userLname'];
        $userEmail = $_REQUEST['userEmail'];
        $check = $this->patient_model->createPatient($userFname, $userLname, $userEmail, password($password));

        if (!$check) {
            echo json_encode([
                'message' => 'Something went wrong in creating patient.',
                'bool' => false
            ]);
            die();
        }
        $this->load->library('email');
        $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
        $this->email->to($userEmail, $userFname . ' ' . $userLname);

        $this->email->subject('Welcome to Myvirtualdoctor.com');
        $message = "Dear " . $userFname . " " . $userLname . ",
            <br /><br />
            Thank you for signing up on My Virtual Doctor.
            <br /><br />
            Your login is <strong>" . $userEmail . "</strong> and password is <strong>" . $password . "</strong>
            <br /><br />
            Please use the following link in order to login to the patient portal
            <br /><br />
            <a href=\"http://www.myvirtualdoctor.com/app/login\">Login Here</a>
            <br /><br />
            Thank you,";
        $this->email->message($message);

        if (!$this->email->send(true, true)) {
            echo json_encode([
                'message' => 'Something went wrong in sending email',
                'bool' => false
            ]);
            die();
        }
        $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
        $this->email->to($userEmail, $userFname . ' ' . $userLname);

        // getting template
        $template = $this->db->get_where('email_templates', ['key' => 'patient_signup_getting_started'])->row();
        $this->email->subject($template->subject);

        $message = str_replace(['{FULL_NAME}', '{URL}', '{FAQ_URL}', '{EMAIL}', '{PASSWORD}'], [$userFname . ' ' . $userLname, base_url('login'), base_url() . '../faqs.html', $userEmail, $password], $template->body);
        $this->email->message($message);
        if (!$this->email->send(true, true)) {
            echo json_encode([
                'message' => 'Something went wrong in sending email',
                'bool' => false
            ]);
            die();
        }
        echo json_encode([
            'message' => 'Thank you for signing up! We just sent you an email with your new password for logging into My Virtual Doctor. Once you receive the email, you may login',
            'bool' => true
        ]);
        die();
    }


}