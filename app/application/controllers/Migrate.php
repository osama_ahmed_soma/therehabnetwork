<?php

defined("BASEPATH") or exit("No direct script access allowed");

class Migrate extends CI_Controller {

	public function __construct() {
		parent::__construct();

		if (!DEBUG) {
			show_error('Not Allowed', 403);
		}
		$this->input->is_cli_request()
				or exit("Execute via command line: php index.php migrate");

		$this->load->library('migration');
	}

	public function index($version) {
		$this->load->library("migration");

		if (!$this->migration->version($version)) {
			show_error($this->migration->error_string());
		}
	}

}
