<?php


class Ajax_Controller extends CI_Controller
{

    public function find_country_states($country_id)
    {
        header('Content-Type: application/x-json; charset=utf-8');
        echo json_encode($this->country_model->find_country_state($country_id));
    }

    public function find_partner_messages($partner_id)
    {
        header('Content-Type: application/x-json; charset=utf-8');
        echo json_encode($this->message_model->find_chat_partner_messages($partner_id));
    }
}