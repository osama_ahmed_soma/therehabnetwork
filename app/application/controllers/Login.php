<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Class Login
 *
 * @property Login_model $login_model
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Session $session
 */
class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //$this->is_logged_in();

        $this->load->helper('captcha');
        captcha_required();
    }

    public function index($message = null)
    {
        if ($this->session->userdata('logged_in')) {
            if ($this->session->userdata('type') == 'patient') {
                redirect('/patient');
            } else {
                redirect('/home/doctor');
            }
        }

        $data = [];
//        $data['logout_message'] = ($message) ? 'Your password has been changed, please login again.' : '';

        $this->load->view('header');
        //$this->load->view('nav');
        //$user=$this->login_model->all_user();
        $this->load->view("signin/signin", $data);
        $this->load->view("footer");
    }

    public function add_condition()
    {
        $condition = $this->input->post('condition');
        $source = $this->input->post('source');
        $date = $this->input->post('date_t');
        $type = $this->input->post('type');

        $dosage = $this->input->post('dosage');
        $freq = $this->input->post('freq');
        $time = $this->input->post('time');

        $severity = $this->input->post('severity');
        $reaction = $this->input->post('reaction');
        $ndc = @$this->input->post('ndc');

        $id = $this->login_model->insert_health_condition($condition, $source, $date, $dosage, $freq, $time, $severity, $reaction, $type, $ndc);
        $data['msg'] = "Inserted succesfully";
        $data['uid'] = $this->session->userdata('userid');
        $data['id'] = $id;
        echo json_encode($data);
    }

    public function login_auth()
    {
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $email = $this->input->post('email');
        $pass = $this->input->post('password', true);

        if ($this->form_validation->run() === TRUE && empty($this->session->flashdata('captcha_error')) === true) {
            $type = $this->login_model->getLoginUser($email, $pass);
            if (!$type) {
                $data['message'] = 'Wrong Information';
                failed_attempt();
                $this->load->view('header');
                $this->load->view("signin/signin", $data);
                $this->load->view("footer");
            } else {
                reset_attempts();
                $this->load->model('user_model');
                $this->user_model->update(['login_date_time' => date('Y-m-d H:i:s', time()), 'is_joined' => 1], 'id', $this->session->userdata('userid'));
                if ($type == 'patient') {
                    redirect('/patient');
                } else {
                    redirect('/home/doctor');
                }
            }
        } else {
            failed_attempt();
            $this->index();
        }
    }

    public function login_apiCheck()
    {
//        $email = $this->input->post('email');
//        $pass = $this->input->post('password', true);
        $email = $_REQUEST['email'];
        $pass = $_REQUEST['password'];
        if (!$email || !$pass) {
            echo json_encode([
                'message' => 'Email and Password is required.',
                'bool' => false
            ]);
            die();
        }
        $data = $this->login_model->getLoginUser($email, $pass, true);
        if (!$data['type']) {
            echo json_encode([
                'message' => 'Wrong Information',
                'bool' => false
            ]);
            die();
        }

        $this->load->model('user_model');
        $this->user_model->update(['login_date_time' => date('Y-m-d H:i:s', time()), 'is_joined' => 1], 'id', $data['userid']);
        if ($data['type'] != 'patient') {
            echo json_encode([
                'message' => 'Wrong Information',
                'bool' => false
            ]);
            die();
        }
        echo json_encode([
            'message' => 'Successfully Logged in',
            'userData' => $data,
            'bool' => true
        ]);
        die();

    }

    function login_as_user($id)
    {
        if (!$this->session->userdata('admin_sess')['logged_in']) {
            show_error('You are not allowed here', 403);
        }
        $type = $this->login_model->LoginAsUser($id);
        if (!$type) {
            echo json_encode(['redirect' => '', 'msg' => 'Invalid User']);
            return;
        }
        if ($type == 'patient') {
            $redirect = base_url('patient');
        } else {
            $redirect = base_url('home/doctor');
        }
        echo json_encode(['redirect' => $redirect, 'msg' => ' Please wait you\'ll be redirect to the ' . $type . ' account in new window or tab']);
        return;
    }

    public function logout()
    {
        if ($this->input->get('message')) {
            $this->session->set_flashdata('success', $this->input->get('message', true));
        } else {
            $this->session->set_flashdata('success', 'Logged out successfully.');
        }
//		dd($message);
//        $this->session->sess_destroy();
        $this->session->unset_userdata('userid');
        $this->session->unset_userdata('useremail');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('userpassword');
        $this->session->unset_userdata('type');
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('doctor');
        $this->session->unset_userdata('full_name');
//        $this->session->unset_userdata('timezone');
        $this->session->unset_userdata('user_timezone');
        $this->session->unset_userdata('image');
        redirect('login');
    }

}
