<?php

class Upload extends Auth_Controller {

	public function __construct() {
		parent::__construct();
		//$this->load->helper(array('form', 'url'));
		$this->documents_path = DOCUMENTS_DIR . '/' . $this->session->userdata('type') . '/' . $this->session->userdata('userid') . '/';
	}

	public function do_upload() {
		$config['upload_path'] = 'uploads/';
		$config['allowed_types'] = 'gif|jpg|png|zip|pdf|doc';
		$config['max_size'] = 1024;
		$config['max_width'] = 1024;
		$config['max_height'] = 768;

		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('userfile')) {
			$error = array('data' => $this->upload->display_errors());
			//$this->load->view('profile/file', $error);
			echo json_encode($error);
		} else {
			$data = array('data' => $this->upload->data());
			echo json_encode($data);
		}
	}

	public function do_upload_doctor() {
		$this->doctor_access_check();
		$config['upload_path'] = 'uploads/doctors/';
		$config['allowed_types'] = 'gif|jpg|png|zip|pdf|doc';
		$config['max_size'] = 1024;
		$config['max_width'] = 1024;
		$config['max_height'] = 768;

		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('userfile')) {
			$error = array('data' => $this->upload->display_errors());
			//$this->load->view('profile/file', $error);
			echo json_encode($error);
		} else {
			$data = array('data' => $this->upload->data());
			//$this->doctor_model->update_doctor_image($data['file_name'], $this->session->userdata('userid'));
			echo json_encode($data);
		}
	}

	public function do_upload_file() {
		$status = "";
		$msg = "";
		$file_element_name = 'userfile';

		if (empty($_POST['title'])) {
			$status = "error";
			$msg = "Please enter a title";
		}

		if ($status != "error") {
			$config['upload_path'] = 'uploads/doctors/';
			$config['allowed_types'] = 'gif|jpg|png|doc|txt';
			$config['max_size'] = 1024 * 8;
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload($file_element_name)) {
				$status = 'error';
				$msg = $this->upload->display_errors('', '');
			} else {
				$data = $this->upload->data();
				$sdata = array();
				$sdata ['doctor_image'] = $data['file_name'];
				$this->session->set_userdata($sdata);
				//$file_id = $this->files_model->insert_file($data['file_name'], $_POST['title']);
				if ($data) {
					$status = "success";
					$msg = "File successfully uploaded";
				} else {
					unlink($data['full_path']);
					$status = "error";
					$msg = "Something went wrong when saving the file, please try again.";
				}
			}
			@unlink($_FILES[$file_element_name]);
		}
		echo json_encode(array('status' => $status, 'msg' => $msg));
	}

	function document() {
		$config['upload_path'] = $this->documents_path;
		$config['allowed_types'] = 'gif|jpg|jpeg|png|docx|doc|xls|xlsx|pdf|bmp|txt';
		$config['max_size'] = 1024 * 10;

		$this->load->library('upload', $config);

		if (!is_dir($config['upload_path'])) {
			if (!mkdir($config['upload_path'], 0777, true)) {
//				echo json_encode(array('errors' => 'Failed to create directory'));
				die('Failed to create directory');
			}
		}

//		if (!is_writable($config['upload_path'])) {
//			echo json_encode(array('errors' => 'Directory not writable'));
//		}

		if (!$this->upload->do_upload('file')) {
			$this->output->set_status_header(400);
//			echo json_encode(array('errors' => $this->upload->display_errors()));
			echo $this->upload->display_errors('', '');
		} else {
			$data = $this->upload->data();
			$this->load->model('File_model');
			$file_info = $this->File_model->insert($data);
//			$file_info->title = (empty($file_info->title)) ? $file_info->filename : $file_info->title;
			$type = pathinfo($file_info->filename, PATHINFO_EXTENSION);
			$icons = $this->config->item('file_icons');
			$file_info->icon_class = (isset($icons[$type])) ? $icons[$type] : $icons['default'];
			$file_info->filesize = humanFileSize($file_info->filesize);
			$file_info->date_time = date('m/d/Y', strtotime($file_info->date_time));
			echo json_encode(array('msg' => "File successfully uploaded", 'file_info' => $file_info, 'csrf_hash' => $this->security->get_csrf_hash() ));
		}
	}

}
