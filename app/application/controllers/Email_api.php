<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Email_Api extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//		var_dump($this->input->ip_address());
//		var_dump(filter_input(INPUT_SERVER, 'SERVER_ADDR', FILTER_VALIDATE_IP));
//		var_dump(getHostByName(getHostName()));
//		die;

		if (!in_array($this->input->ip_address(), array('52.44.81.78', '127.0.0.1'))) {
			echo json_encode(array('msg' => 'Not authorizied'));
			die;
		}
	}

	function index()
	{
		$userFname = $this->input->get('userFname');
		$userLname = $this->input->get('userLname');
		$userEmail = $this->input->get('userEmail');
		$userPass = $this->input->get('userPass');

		$this->load->library('email');
		$this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
		$this->email->to($userEmail, $userFname . ' ' . $userLname);

		$this->email->subject('Welcome to Myvirtualdoctor.com');
		$this->email->message("Dear " . $userFname . " " . $userLname . ",
            <br /><br />
            Thank you for signing up on My Virtual Doctor.
            <br /><br />
            Your login is " . $userEmail . " and password is " . $userPass . "
            <br /><br />
            Please use the following link in order to login to the patient portal
            <br /><br />
            <a href=\"http://www.myvirtualdoctor.com/app/login\">Login Here</a>
            <br /><br />
            Thank you,");

		if ($this->email->send()) {
			echo json_encode(array('status' => 'sent'));
			return;
		}
		echo json_encode(array('status' => 'error', 'msg' => 'If dont see headers then turn on debugging'));
		if (DEBUG) {
			echo $this->email->print_debugger(array('headers'));
		}
	}

	function lander()
	{
		$first_name = urldecode($this->input->get('first_name', true));
		$last_name = urldecode($this->input->get('last_name', true));
		$email_add = urldecode($this->input->get('email_add', true));
		$phone_number = urldecode($this->input->get('phone_number', true));
		$subject = urldecode($this->input->get('subject', true));
		$ip_address = urldecode($this->input->get('ip_address', true));
		$to = 'info@myvirtualdoctor.com';

		$this->load->library('email');
		$this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
		$this->email->to($to);

		$this->email->subject($subject);
		$this->email->message('<html>
 <head>
  <title>' . $subject . '</title>
 </head>
 <body>
 <p><strong>Dear Administrator,</strong></p>
  <p>Here are the details of the form submission for the lander</p>
  <p>First Name: ' . $first_name . '<br />
  Last Name: ' . $last_name . ',<br />
  Email address: ' . $email_add . ',<br />
  Phone Number: ' . $phone_number . '<br />
  IP Address: ' . $ip_address . '</p>
   <p><strong>Thank you,<br/>My Virtual Doctor</strong></p>
 </body> 
 </html>');

		if ($this->email->send()) {
			echo json_encode(array('status' => 'sent'));
			return;
		}
		echo json_encode(array('status' => 'error', 'msg' => 'If dont see headers then turn on debugging'));
		if (DEBUG) {
			echo $this->email->print_debugger(array('headers'));
		}
	}

}
