<?php

/**
 * Created by PhpStorm.
 * User: ziku
 * Date: 8/13/16
 * Time: 3:06 AM
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Messages extends Auth_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('messages_model');
		$this->load->model('user_model');
		$this->load->helper('text');
	}

	private function security_check($message) {
		if (!is_object($message)) {
			return false;
		}
		$me = $this->session->userdata('userid');

		//I only can view my sent messages or I can view messages sent to me
		if ($message->from_userid != $me && $message->to_userid != $me) {
			$this->session->sess_destroy();
			redirect('login');
		}

		//check this message is permanently deleted
		if ($message->from_userid == $me) {
			//me is the sender
			if ($message->sender_delete == 2) {
				return false;
			}
		} else {
			if ($message->recipient_delete == 2) {
				return false;
			}
		}
		return true;
	}

	public function ajaxPermanentDeleteMessage($message_id) {
		//ajax restore message
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			//get message first
			$me = $this->session->userdata('userid');
			$data = array();
			$message = $this->messages_model->getMessage($message_id);

			if (is_object($message) && $this->security_check($message)) {
				//check which one to delete, sender or receiver
				if ($message->from_userid == $me) {
					//me is the sender
					$data['sender_delete'] = 2;
				} else {
					$data['recipient_delete'] = 2;
				}
				$where = array(
					'id' => $message_id
				);

				$ret = $this->messages_model->updateMessage($data, $where);
				$data = array();

				if ($ret == 1) {
					//update successful
					$data['success'] = 1;
					$data['message'] = 'Message <strong>' . $message->subject . '</strong> deleted successfully.';
				} else {
					$data['failed'] = 1;
					$data['message'] = 'Something went wrong. Message not deleted.';
				}
			} else {
				//invalid message id
				$data['error'] = 1;
				$data['message'] = 'Invalid message selected.';
			}

			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($data);
		} else {
			die('Ajax only function.');
		}
	}

	public function ajaxRestoreMessage($message_id) {
		//ajax restore message
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			//get message first
			$me = $this->session->userdata('userid');
			$data = array();
			$message = $this->messages_model->getMessage($message_id);

			if (is_object($message) && $this->security_check($message)) {
				//check which one to delete, sender or receiver
				if ($message->from_userid == $me) {
					//me is the sender
					$data['sender_delete'] = 0;
				} else {
					$data['recipient_delete'] = 0;
				}
				$where = array(
					'id' => $message_id
				);

				$ret = $this->messages_model->updateMessage($data, $where);
				$data = array();

				if ($ret == 1) {
					//update successful
					$data['success'] = 1;
					$data['message'] = 'Message <strong>' . $message->subject . '</strong> restored successfully.';
				} else {
					$data['failed'] = 1;
					$data['message'] = 'Something went wrong. Message not restored.';
				}
			} else {
				//invalid message id
				$data['error'] = 1;
				$data['message'] = 'Invalid message selected.';
			}

			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($data);
		} else {
			die('Ajax only function.');
		}
	}

	public function ajaxMoveMessageToTrash($message_id) {
		//ajax delete message
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			//get message first
			$me = $this->session->userdata('userid');
			$data = array();
			$message = $this->messages_model->getMessage($message_id);

			if (is_object($message) && $this->security_check($message)) {
				//check which one to delete, sender or receiver
				if ($message->from_userid == $me) {
					//me is the sender
					$data['sender_delete'] = 1;
				} else {
					$data['recipient_delete'] = 1;
				}
				$where = array(
					'id' => $message_id
				);

				$ret = $this->messages_model->updateMessage($data, $where);
				$data = array();

				if ($ret == 1) {
					//update successful
					$data['success'] = 1;
					$data['message'] = 'Message <strong>' . $message->subject . '</strong> deleted successfully.';
				} else {
					$data['failed'] = 1;
					$data['message'] = 'Something went wrong. Message not deleted.';
				}
			} else {
				//invalid message id
				$data['error'] = 1;
				$data['message'] = 'Invalid message selected.';
			}

			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($data);
		} else {
			die('Ajax only function.');
		}
	}

	public function view($message_id) {

		$me = $this->session->userdata('userid');
		$data = array();
		$message = $this->messages_model->getMessage($message_id);

		if (is_object($message) && $this->security_check($message)) {

			//check message is viewed or not
			if ($message->to_userid == $me && $message->opened == 0) {
				//set message view to 1
				$this->messages_model->setView($message_id);
			}

			//fix from - to and get doctor/patient name
			$message = $this->messages_model->fixMessageForDisplay($message);

			$data['message'] = $message;

			//now check for reply
			if (intval($message->parent) > 0) {
				//recursively get upto 5 messages and fix from/to and doctorname
				$replies = array();
				$this->messages_model->getLatestFiveReplysForMessage($message->parent, $replies, 0);
				$data['replies'] = $replies;
			}
		} else {
			//invalid message id
			$data['error'] = 1;
			$data['message'] = 'Invalid message selected.';
		}

		//current page
		$data['page'] = $this->input->get('page', true);

		//load header
		$this->load->view('header');

		//load nav
		if ($this->session->userdata('type') == 'patient') {
			$this->load->view('header_top');
			$this->load->view('nav');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->load->view('doctor_header_top');
			$this->load->view('doctor_nav');
		}

		//now load main view
		$this->load->view('messages/header', array( 'recipient_list' => $this->messages_model->getRecipients(), 'page' => $this->input->get('page', true) ));
		$this->load->view('messages/view', $data);

		//load footer
		$this->load->view("footer");
	}

	public function reply($message_id) {

		//get required data
		$me = $this->session->userdata('userid');
		$data = array();

		$message = $this->messages_model->getMessage($message_id);

		if (is_object($message) && $this->security_check($message)) {
			//fix subject RE:
			if (strpos($message->subject, 'RE:') === false) {
				$message->subject = 'RE: ' . $message->subject;
			}

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');

			$config = array(
				array(
					'field' => 'subject',
					'label' => 'Subject',
					'rules' => array('required', 'min_length[5]'),
					'errors' => array(
						'required' => 'You must provide a %s.',
					),
				),
				array(
					'field' => 'message',
					'label' => 'Message',
					'rules' => 'required'
				)
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) {
				echo json_encode(array('errors' => validation_errors()));
				die;
			} else {
				//fix from_userid and to_userid
				$from_userid = 0;
				$to_userid = 0;

				//check this message is from me
				if ($message->from_userid == $me) {
					//newmsg from_userid will be $me, $to_userid = $message->to_userid
					$from_userid = $me;
					$to_userid = $message->to_userid;
				} else {
					//this message was sent to me,
					$from_userid = $me;
					$to_userid = $message->from_userid;
				}

				//
				$data = array(
					'from_userid' => $from_userid,
					'to_userid' => $to_userid,
					'parent' => $message_id,
					'subject' => $this->input->post('subject', true),
					'message' => $this->input->post('message', true),
					'added' => time()
				);

				
				// there is no point of this useless code here, will remove it later
				if ($this->input->post('save') !== NULL) {
					$data['draft'] = 1;
				} else {
					$data['draft'] = 0;
				}

				$id = $this->messages_model->addMessage($data);

				if ($id > 0) {
					if ($data['draft'] == 1) {
//						redirect('messages/drafts');
						echo json_encode(array('redirect' => base_url('messages/drafts')));
					} else {
//						redirect('messages/sent');
						echo json_encode(array('redirect' => base_url('messages/sent')));
					}
					die;
				}
			}
		} else {
			//invalid message id
			$data['errors'][] = 'Invalid message selected.';
			echo json_encode( $data );
			die;
		}
	}

	public function send_draft($message_id) {

		//get required data
		$me = $this->session->userdata('userid');
		$data = array();

		$message = $this->messages_model->getMessage($message_id);

		if (is_object($message) && $this->security_check($message)) {

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');

			$config = array(
					array(
							'field' => 'subject',
							'label' => 'Subject',
							'rules' => array('required', 'min_length[5]'),
							'errors' => array(
									'required' => 'You must provide a %s.',
							),
					),
					array(
							'field' => 'message',
							'label' => 'Message',
							'rules' => 'required'
					)
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) {
				echo json_encode(array('errors' => validation_errors()));
				die;
			} else {
				//
				$data = array(
						'subject' => $this->input->post('subject', true),
						'message' => $this->input->post('message', true),
						'added' => time()
				);


				// there is no point of this useless code here, will remove it later
				if ($this->input->post('save') !== NULL) {
					$data['draft'] = 1;
				} else {
					$data['draft'] = 0;
				}

				$where = array(
					'id' => $message_id
				);

				$id = $this->messages_model->updateMessage($data, $where);

				if ($id > 0) {
					if ($data['draft'] == 1) {
//						redirect('messages/drafts');
						echo json_encode(array('redirect' => base_url('messages/drafts')));
					} else {
//						redirect('messages/sent');
						echo json_encode(array('redirect' => base_url('messages/sent')));
					}
					die;
				}
			}
		} else {
			//invalid message id
			$data['errors'][] = 'Invalid message selected.';
			echo json_encode( $data );
			die;
		}
	}

	public function compose() {
		//get required data
		$data = array();
		$me = $this->session->userdata('userid');

		//check url for message_id
		$message_id = $this->uri->segment(3) !== NULL ? intval($this->uri->segment(3)) : 0;

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');

		if (!$message_id) {
			$message_id = $this->input->post('message_id') !== NULL ? intval($this->input->post('message_id', true)) : 0;
		}

		if ($message_id > 0) {
			$message = $this->messages_model->getMessage($message_id);
			if (is_object($message) && $message->draft == 1 && $message->from_userid == $me) {
				$data['message'] = $message;
			}
		}

		$config = array(
			array(
				'field' => 'recipients',
				'label' => 'Recipients',
				'rules' => 'required'
			),
			array(
				'field' => 'subject',
				'label' => 'Subject',
				'rules' => array('required', 'min_length[5]'),
				'errors' => array(
					'required' => 'You must provide a %s.',
				),
			),
			array(
				'field' => 'message',
				'label' => 'Message',
				'rules' => 'required'
			)
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE) {
			echo json_encode(array('errors' => validation_errors()));
			die;
//			$data['recipient_list'] = $this->messages_model->getRecipients();
//			$this->load->view('messages/compose', $data);
		} else {
			$data = array(
				'from_userid' => $me,
				'to_userid' => $this->input->post('recipients', true),
				'subject' => $this->input->post('subject', true),
				'message' => $this->input->post('message', true),
				'added' => time()
			);

			if ($this->input->post('save') === 'Save') {
				$data['draft'] = 1;
			} else {
				$data['draft'] = 0;
			}

			if (intval($this->input->post('message_id')) > 0) {
				$where = array(
					'id' => intval($this->input->post('message_id', true))
				);
				$id = $this->messages_model->updateMessage($data, $where);
			} else {
				$id = $this->messages_model->addMessage($data);
			}

			if ($id > 0) {
				if ($data['draft'] == 1) {
					echo json_encode(array('redirect' => base_url('messages/drafts')));
//					redirect('messages/drafts');
				} else {
					echo json_encode(array('redirect' => base_url('messages/sent')));
//					redirect('messages/sent');
				}
				die;
			}
		}
	}

	public function index() {
		//load pagination library
		$this->load->library('pagination');

		//generate data
		$limit = 10;
		$page = $this->uri->segment(3) !== NULL ? $this->uri->segment(3) : 0;
		$data = array();
		$config = array();

		$search_text = trim($this->input->get('search', true));
		if (!empty($search_text)) {
			$data['search'] = 1;
			$data['messages'] = $this->messages_model->searchInboxMessages($limit, $page, $search_text);
			$config['reuse_query_string'] = TRUE;
			$config['total_rows'] = $this->messages_model->searchInboxMessages($limit, $page, $search_text, true);
		} else {
			$search_text = '';
			$data['search'] = 0;
			$data['messages'] = $this->messages_model->getInbox($limit, $page);
			$config['reuse_query_string'] = FALSE;
			$config['total_rows'] = $this->messages_model->countInbox();
		}


		//load header
		$this->load->view('header');

		//load nav
		if ($this->session->userdata('type') == 'patient') {
			$this->load->view('header_top');
			$this->load->view('nav');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->load->view('doctor_header_top');
			$this->load->view('doctor_nav');
		}

		//get message counter
		$counter = $this->messages_model->getCounters();
		
		//load message header
		$this->load->view("messages/header", array('search' => $data['search'], 'search_text' => $search_text, 'recipient_list' => $this->messages_model->getRecipients(), 'index_count' => $counter['index_count'], 'sent_count' => $counter['sent_count'], 'drafts_count' => $counter['drafts_count'], 'trash_count' => $counter['trash_count']));

		//bootstrap changes
		$config['full_tag_open'] = '<nav aria-label="Page navigation" style="text-align: center;"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav><!--pagination-->';
		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class=" page-item">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class=" page-item">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class=" page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
//         $config['display_pages'] = FALSE;
		$config['attributes'] = array('class' => 'page-link');

		//pagination value
		$config['base_url'] = base_url() . 'messages/index/';
		$config['per_page'] = $limit;
		$this->pagination->initialize($config);
		$data['pagination_link'] = $this->pagination->create_links();

		//load message/index view
		$this->load->view("messages/index", $data);

		//load message footer
		$this->load->view("messages/footer");

		//load footer
		$this->load->view("footer");
	}

	public function sent() {
		//load pagination library
		$this->load->library('pagination');

		//generate data
		$limit = 10;
		$page = $this->uri->segment(3) !== NULL ? $this->uri->segment(3) : 0;
		$data = array();
		$config = array();


		$search_text = trim($this->input->get('search', true));
		if (!empty($search_text)) {
			$data['search'] = 1;
			$data['messages'] = $this->messages_model->searchSentMessages($limit, $page, $search_text);
			$config['reuse_query_string'] = TRUE;
			$config['total_rows'] = $this->messages_model->searchSentMessages($limit, $page, $search_text, true);
		} else {
			$search_text = '';
			$data['search'] = 0;
			$data['messages'] = $this->messages_model->getSentBox($limit, $page);
			$config['reuse_query_string'] = FALSE;
			$config['total_rows'] = $this->messages_model->countSentBox();
		}


		//load header
		$this->load->view('header');

		//load nav
		if ($this->session->userdata('type') == 'patient') {
			$this->load->view('header_top');
			$this->load->view('nav');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->load->view('doctor_header_top');
			$this->load->view('doctor_nav');
		}

		//get message counter
		$counter = $this->messages_model->getCounters();

		$this->load->view("messages/header", array('search_text' => $search_text, 'recipient_list' => $this->messages_model->getRecipients(), 'index_count' => $counter['index_count'], 'sent_count' => $counter['sent_count'], 'drafts_count' => $counter['drafts_count'], 'trash_count' => $counter['trash_count']));

		$config['base_url'] = base_url() . 'messages/sent/';
		//bootstrap changes
		$config['full_tag_open'] = '<nav aria-label="Page navigation" style="text-align: center;"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav><!--pagination-->';
		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		// $config['display_pages'] = FALSE;
		//$config['reuse_query_string'] = FALSE;
		$config['attributes'] = array('class' => 'page-link');

		//pagination value

		$config['per_page'] = $limit;
		$this->pagination->initialize($config);
		$data['pagination_link'] = $this->pagination->create_links();

		//load message/index view
		$this->load->view("messages/sent", $data);

		//load message footer
		$this->load->view("messages/footer");

		//load footer
		$this->load->view("footer");
	}

	public function drafts() {
		//load pagination library
		$this->load->library('pagination');
		//load header
		$this->load->view('header');

		//load nav
		if ($this->session->userdata('type') == 'patient') {
			$this->load->view('header_top');
			$this->load->view('nav');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->load->view('doctor_header_top');
			$this->load->view('doctor_nav');
		}


		//generate data
		$limit = 10;
		$page = $this->uri->segment(3) !== NULL ? $this->uri->segment(3) : 0;
		$data = array();

		$search_text = trim($this->input->get('search', true));
		if (!empty($search_text)) {
			$data['messages'] = $this->messages_model->searchDraftMessages($limit, $page, $search_text);
			$config['reuse_query_string'] = TRUE;
			$config['total_rows'] = $this->messages_model->searchDraftMessages($limit, $page, $search_text, true);
		} else {
			$search_text = '';
			$data['messages'] = $this->messages_model->getDraftBox($limit, $page);
			$config['reuse_query_string'] = FALSE;
			$config['total_rows'] = $this->messages_model->countDraftBox();
		}

		//get message counter
		$counter = $this->messages_model->getCounters();

		//load message header
		$this->load->view("messages/header", array('search_text' => $search_text, 'recipient_list' => $this->messages_model->getRecipients(), 'index_count' => $counter['index_count'], 'sent_count' => $counter['sent_count'], 'drafts_count' => $counter['drafts_count'], 'trash_count' => $counter['trash_count']));

		$config['base_url'] = base_url() . 'messages/drafts/';
		//bootstrap changes
		$config['full_tag_open'] = '<nav aria-label="Page navigation" style="text-align: center;"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav><!--pagination-->';
		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		// $config['display_pages'] = FALSE;
		$config['attributes'] = array('class' => 'page-link');

		//pagination value
		$config['per_page'] = $limit;
		$this->pagination->initialize($config);
		$data['pagination_link'] = $this->pagination->create_links();

		//load message/index view
		$this->load->view("messages/drafts", $data);

		//load message footer
		$this->load->view("messages/footer");

		//load footer
		$this->load->view("footer");
	}

	public function trash() {
		//load pagination library
		$this->load->library('pagination');
		//load header
		$this->load->view('header');

		//load nav
		if ($this->session->userdata('type') == 'patient') {
			$this->load->view('header_top');
			$this->load->view('nav');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->load->view('doctor_header_top');
			$this->load->view('doctor_nav');
		}

		//generate data
		$limit = 10;
		$page = $this->uri->segment(3) !== NULL ? $this->uri->segment(3) : 0;
		$data = array();

		$search_text = trim($this->input->get('search', true));
		if (!empty($search_text)) {
			$data['search'] = 1;
			$data['messages'] = $this->messages_model->searchTrashMessages($limit, $page, $search_text);
			$config['reuse_query_string'] = TRUE;
			$config['total_rows'] = $this->messages_model->searchTrashMessages($limit, $page, $search_text, true);
		} else {
			$search_text = '';
			$data['search'] = 0;
			$data['messages'] = $this->messages_model->getTrashBox($limit, $page);
			$config['reuse_query_string'] = FALSE;
			$config['total_rows'] = $this->messages_model->countTrashBox();
		}

		//get message counter
		$counter = $this->messages_model->getCounters();

		//load message header
		$this->load->view("messages/header", array('search_text' => $search_text, 'recipient_list' => $this->messages_model->getRecipients(), 'index_count' => $counter['index_count'], 'sent_count' => $counter['sent_count'], 'drafts_count' => $counter['drafts_count'], 'trash_count' => $counter['trash_count']));

		$config['base_url'] = base_url() . 'messages/trash/';
		//bootstrap changes
		$config['full_tag_open'] = '<nav aria-label="Page navigation" style="text-align: center;"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav><!--pagination-->';
		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		// $config['display_pages'] = FALSE;
		$config['attributes'] = array('class' => 'page-link');

		//pagination value
		$config['per_page'] = $limit;
		$this->pagination->initialize($config);
		$data['pagination_link'] = $this->pagination->create_links();

		//load message/index view
		$this->load->view("messages/trash", $data);

		//load message footer
		$this->load->view("messages/footer");

		//load footer
		$this->load->view("footer");
	}

	public function settings() {
		//load header
		$this->load->view('header');

		//load nav
		if ($this->session->userdata('type') == 'patient') {
			$this->load->view('header_top');
			$this->load->view('nav');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->load->view('doctor_header_top');
			$this->load->view('doctor_nav');
		}

		//load message header
		$this->load->view("messages/header");

		//generate data
		$data = array();

		$data = $this->messages_model->getInbox();

		//load message/index view
		$this->load->view("messages/settings", $data);

		//load message footer
		$this->load->view("messages/footer");

		//load footer
		$this->load->view("footer");
	}
	
	function recipients() {
		$data = $this->messages_model->getRecipients();
//		var_dump($data);
		
		$recipents = array();
		foreach ($data as $key => $value) {
			$recipents[] =
//					array('name' => 
				$value->firstname . $value->lastname
//					, 'id' => $value->id
//					)
					;
		}
		
		echo json_encode($recipents);
	}

}
