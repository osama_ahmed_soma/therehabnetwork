<?php
/**
 * Created by PhpStorm.
 * User: ziku
 * Date: 8/26/16
 * Time: 2:48 PM
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notification extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('notification_model');
    }

    public function index() {
        //load pagination library
        $this->load->library('pagination');

        //generate data
        $limit = 10;
        $page = $this->uri->segment(3) !== NULL ? $this->uri->segment(3) : 0;
        $data = array();
        $config = array();

        //get notifications
        $data['notifications'] = $this->notification_model->getNotifications($limit, $page);

        //load header
        $this->load->view('header');

        //load nav
        if ($this->session->userdata('type') == 'patient') {
            $this->load->view('header_top');
            $this->load->view('nav');
        } elseif ($this->session->userdata('type') == 'doctor') {
            $this->load->view('doctor_header_top');
            $this->load->view('doctor_nav');
        }

        //bootstrap changes
        $config['reuse_query_string'] = FALSE;
        $config['total_rows'] = $this->notification_model->countNotifications();
        $config['full_tag_open'] = '<nav aria-label="Page navigation" style="text-align: center;"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class=" page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class=" page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class=" page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
//         $config['display_pages'] = FALSE;
        $config['attributes'] = array('class' => 'page-link');

        //pagination value
        $config['base_url'] = base_url() . 'notification/index/';
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['pagination_link'] = $this->pagination->create_links();

        //load notification/index view
        $this->load->view("notification/index", $data);

        //load footer
        $this->load->view("footer");
    }

    private function check_for_security($notification_id) {
        $me = $this->session->userdata('userid');

        if ( $this->notification_model->getNotification($notification_id) > 0 ) {
            return true;
        } else {
            return false;
        }
    }

    public function readNotification($notificationID)
    {
        if ( $this->check_for_security($notificationID) ) {
            $this->notification_model->readNotification($notificationID);
        }

    }

    public function readAllNotifications()
    {
        $data = [];
        $data['message'] = '';
        $data['bool'] = false;
        if($this->session->userdata('type') == 'doctor'){
            $this->notification_model->readAllNotifications_doctor($this->session->userdata('userid'));
        } else if($this->session->userdata('type') == 'patient'){
            $this->notification_model->readAllNotifications_patient($this->session->userdata('userid'));
        }
    }

    public function deleteNotification( $notificationID ) {
        if ( $this->check_for_security( $notificationID ) ) {
            $this->notification_model->deleteNotification( $notificationID );
        }
    }
}