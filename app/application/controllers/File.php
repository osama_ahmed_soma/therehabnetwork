<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class File extends Auth_Controller
{

	public $path;

	public function __construct()
	{
		parent::__construct();
		$this->documents_path = DOCUMENTS_DIR . '/' . $this->session->userdata('type') . '/' . $this->session->userdata('userid') . '/';
		$this->load->model('File_model');
	}

	function download_document($file_id = null, $patient_or_doctor_id = null)
	{
//		if ($this->session->userdata('type') == 'patient') {
//			$file = $this->File_model->get_file($file_id);
//		} else if ($this->session->userdata('type') == 'doctor') {
//			$file = $this->File_model->get_file_shared_by_patient($file_id, $patient_or_doctor_id);
//		}

		if ($patient_or_doctor_id == null) {
			$file = $this->File_model->get_file($file_id);
		} else {
//			if ($this->session->userdata('type') == 'patient') {
//				$file = $this->File_model->get_file_shared($file_id, $patient_or_doctor_id);
//			} else if ($this->session->userdata('type') == 'patient') {
				$file = $this->File_model->get_file_shared($file_id, $patient_or_doctor_id);
//			}
		}
		
//		var_debug($file);
//		die;
		
		if (!$file) {
			show_error('File/record does not exist', 404);
		}

		$type = $this->session->userdata('type') == 'doctor' ? 'patient' : 'doctor';
		
		if ($patient_or_doctor_id !== null) {
			$file_to_download = DOCUMENTS_DIR . '/'.$type.'/' . $patient_or_doctor_id . '/' . $file->filename;
		} else {
			$file_to_download = $this->documents_path . $file->filename;
		}
//		dd($file_to_download);

		if (is_file($file_to_download)) {
			$this->load->helper('download_helper');
			force_download($file_to_download, NULL);
			return;
		}
		show_error('File does not exist', 404);
	}

	function delete($file_id)
	{
		$check = $this->File_model->delete($file_id);
		if ($check) {
			$ss = 'success';
			$msgs = 'File successfully deleted';
		} else {
			$ss = 'error';
			$msgs = 'Something went wrong when deleteing the file, please try again';
		}
		echo json_encode(array('status' => $ss, 'msg' => $msgs));
	}

	function delete_shared($id)
	{
		$check = $this->File_model->delete_shared($id);
		if ($check) {
			$ss = 'success';
			$msgs = 'Shared successfully deleted';
		} else {
			$ss = 'error';
			$msgs = 'Something went wrong when deleteing share, please try again';
		}
		echo json_encode(array('status' => $ss, 'msg' => $msgs));
	}

	function share($shared_to_id, $file_id)
	{
		$shared_file = $this->File_model->get_shared_file($file_id, $shared_to_id);
		if ($shared_file) {
			echo json_encode(array('msg' => 'Already shared'));
			die;
		}
		$check = $this->File_model->share_file($shared_to_id, $file_id);
		if ($check) {
			echo json_encode(array('msg' => 'Shared successfully'));
		} else {
			echo json_encode(array('msg' => 'Something went wrong in sharing file'));
		}
	}

	function shared_with($file_id)
	{
		$this->load->view('shared_with', array('records' => $this->File_model->shared_files($file_id)));
	}

	function update($file_id)
	{
		$post = $this->input->post(null, true);
		$check = $this->File_model->update($file_id, $post);
		if ($check) {
			$ss = 'success';
			$msgs = 'Updated successfully';
		} else {
			$ss = 'error';
			$msgs = 'Something went wrong in updating record';
		}
		echo json_encode(array('status' => $ss, 'msg' => $msgs, 'file_info' => $this->File_model->get_file($file_id)));
	}

}
