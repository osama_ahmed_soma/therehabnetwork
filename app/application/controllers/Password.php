<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Class Login
 *
 * @property Login_model $login_model
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Session $session
 */
class Password extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
//        $this->load->library('user_model');
        $this->load->library('email');
        $this->load->helper('captcha');
        captcha_required();
    }

    public function index($data = [])
    {
//		var_debug($data);
        $this->load->view('header');
        $this->load->view("password/reset", $data);
        $this->load->view("footer");
    }

    public function reset()
    {
        $this->form_validation->set_rules('email', 'Email', 'required');
        $email = $this->input->post('email', true);
        if ($this->form_validation->run() == true && empty($this->session->flashdata('captcha_error')) === true) {
//			$this->user_model->email_exists($email);
            if (!$this->user_model->email_exists($email)) {
                failed_attempt();
                $data['message'] = "This email does not exist.";
                $this->index($data);
                return;
            }

            $this->load->helper('string');
            $token = sha1(random_string());
            $this->user_model->update(['token' => $token, 'token_expiry' => date('Y-m-d H:i:s', strtotime('+24 hours'))], 'user_email', $email);

            if (!$this->sendResetPasswordEmail($email, $token)) {
                $data['message'] = "Failed to send password reset email!";
                $this->index($data);
                return;
            }
            reset_attempts();
            $data['success'] = "Check your email for password reset instructions";
            $this->index($data);
        } else {
            failed_attempt();
            $this->index();
        }
    }

    public function reset_api()
    {
        error_reporting(0);
        $email = $_REQUEST['email'];
        $result = [];
        $result['message'] = '';
        $result['bool'] = false;
        if (!$this->user_model->email_exists($email)) {
            $result['message'] = "This email does not exist.";
            echo $result;
            die();
        }
        $this->load->helper('string');
        $token = sha1(random_string());
        $this->user_model->update(['token' => $token, 'token_expiry' => date('Y-m-d H:i:s', strtotime('+24 hours'))], 'user_email', $email);
        if (!$this->sendResetPasswordEmail($email, $token)) {
            $result['message'] = "Failed to send password reset email!";
            echo $result;
            die();
        }
        $result['message'] = "Check your email for password reset instructions";
        $result['bool'] = true;
        echo $result;
        die();
    }

    private function sendResetPasswordEmail($email, $token)
    {
        $this->email->from('admin@myvirtualdoctor.com', 'My Virtual Doctor');
        $this->email->to($email);
        $this->email->subject('My Virtual Doctor password Reset Link');

//		$name = $this->user_model->getName('user_email', $email);

        $this->db->select("id, type");
        $this->db->from('user');
        $this->db->where('user_email', $email);
        $row = $this->db->get()->row();
//		var_debug($row->id);
        if ($row->type == 'doctor') {
            $this->load->model('doctor_model');
            $user_details = $this->doctor_model->get_doctor($row->id);
        } else if ($row->type == 'patient') {
            $this->load->model('patient_model');
            $user_details = $this->patient_model->get_patient($row->id);
        }
//		var_debug($user_details);
        $message = "Dear $user_details->firstname $user_details->lastname, <br><br>";
        $message .= "Password reset link: <br>";
        $message .= "<a target=\"_blank\" href=" . base_url('password/change/' . $token) . ">" . base_url('password/change/' . $token) . "</a><br><br> Note: This link will stay valid only for next 24 hours.<br><br>";
        $message .= "You can always change your password after logging in to the system.";

//		echo $message;
//		die;

        $this->email->message($message);

        return $this->email->send(true, true);
    }

    public function change($token = '')
    {
        $this->load->helper('security');
        $data['token'] = trim(xss_clean($token));

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|min_length[8]|max_length[125]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]|max_length[15]');
        $this->form_validation->set_rules('passconf', 'Confirm Password', 'required|min_length[8]|max_length[15]|matches[password]');

        $this->form_validation->set_error_delimiters('', '<br>');

        if ($this->input->post()) {
            $data['token'] = trim($this->input->post('token', true));
        }

//		var_debug($this->input->post(null, true));
//		var_debug($data);
//		var_debug(empty($data['token']));
//		var_debug($this->user_model->token_match($data['token']));

        if (empty($data['token']) === true || $this->user_model->token_match($data['token']) === false) {
            failed_attempt();
            $this->session->set_flashdata('error', 'Invalid token');
        } else {

            if ($this->form_validation->run() !== FALSE && empty($this->session->flashdata('captcha_error')) == true) {
                $email = $this->input->post('email', true);

                if (!$this->user_model->email_exists($email)) {
                    failed_attempt();
                    $data['message'] = "This email does not exist.";
                } else {
                    $password = password($this->input->post('password', true));
                    $update_check = $this->user_model->update(['user_password' => $password, 'token' => null, 'token_expiry' => '0000-00-00 00:00:00'], 'user_email', $email);
                    if ($update_check) {
                        reset_attempts();
                        $this->session->set_flashdata('success', 'Password successfully updated.');
                        redirect('/login');
                    } else {
                        $this->session->set_flashdata('error', 'Password update failed');
                    }
                }
            }
        }

        $this->load->view('header');
        $this->load->view("password/change", $data);
        $this->load->view("footer");
    }

    public function change_api($token = '')
    {
        error_reporting(0);
        $result = [];
        $result['message'] = '';
        $result['bool'] = false;

        $this->load->helper('security');
        $data['token'] = trim(xss_clean($token));
        if (empty($data['token']) === true || $this->user_model->token_match($data['token']) === false) {
            $result['message'] = 'Invalid token';
            echo $result;
            die();
        }
        $email = $_REQUEST['email'];
        $password = $_REQUEST['password'];
        if (!$this->user_model->email_exists($email)){
            $result['message'] = 'This email does not exist.';
            echo $result;
            die();
        }
        $password = password($password);
        $update_check = $this->user_model->update(['user_password' => $password, 'token' => null, 'token_expiry' => '0000-00-00 00:00:00'], 'user_email', $email);
        if ($update_check) {
            $result['message'] = 'Password successfully updated.';
            $result['bool'] = true;
            echo $result;
        } else {
            $result['message'] = 'Password update failed';
            echo $result;
        }
        die();
    }

}
