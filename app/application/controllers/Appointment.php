<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Class Appointment
 *
 * @property CI_Loader $load
 * @property Appointment_Model $appointment_model
 * @property Login_model $login_model
 * @property Reason_Model $reason_model
 */
class Appointment extends Auth_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('appointment_model');
        $this->load->model('reason_model');
        $this->load->model('doctor_model');
        $this->load->model('patient_model');
        $this->load->model('notification_model');
        $this->load->model('consultationtype_model');

        $this->config->load('tokbox');

    }

    protected function getDataForReport()
    {
        $id = $this->session->userdata('userid');
        $type = $this->session->userdata('type');
        $appointmentId = $this->input->get('aid');

        $appointment = $this->appointment_model->getDetailedById($appointmentId);
        $appointment->reason_name = $this->reason_model->getById($appointment->reason_id)->name;
        //var_dump($appointment);die;

        //get doctor
        $doctor = $this->login_model->get_doctor($appointment->doctors_id);

        //get basic patient info
        $patient = $this->login_model->get_patient($appointment->patients_id);
        $patient = reset($patient);

        //get patient age
        $patient->age = getPatientAge($patient->dob);


        $data = [];
        $data['appointment'] = $appointment;
        $data['patient'] = $patient;
        $data['doctor'] = $doctor;

        return $data;
    }

    public function report()
    {
        $data = $this->getDataForReport();

        $this->load->view('header');

        //load nav
        if ($this->session->userdata('type') == 'patient')
        {
            $this->load->view('header_top');
            $this->load->view('nav');
        }
        elseif ($this->session->userdata('type') == 'doctor')
        {
            $this->load->view('doctor_header_top');
            $this->load->view('doctor_nav');
        }
        $this->load->view('appointment/report', $data);
        $this->load->view("footer");

    }

    public function reportPdf()
    {
        $data = $this->getDataForReport();

        $mpdf = new mPDF();
        $mpdf->WriteHTML($this->load->view('appointment/_report_table', $data, true));
        $mpdf->Output();
    }

    public function start()
    {
        $id = $this->session->userdata('userid');
        $type = $this->session->userdata('type');

        $appointmentId = $this->input->get('aid');
        if (!$appointmentId) {
            show_error('Wrong action.', 404);
        }
		
		// you are allowed to access this page only between the time of consultation
//		if($this->appointment_model->check_appointment_available($appointmentId, time(), time(), 'id', 'AND') == 0){
//			show_error('you are allowed to access this page only between the time of consultation', 400);
//		}

        $tokboxApiKey = $this->config->item('api_key');
        $tokboxApiSecret = $this->config->item('api_secret');

        $app = $this->appointment_model->getById($appointmentId);
        /*echo "<pre>";
        print_r($app);
        print_r($type);
        die();*/
        if (!$app->tokbox_session) {

            $sessionId = $this->appointment_model->generateTokboxSession($tokboxApiKey, $tokboxApiSecret);
            $this->appointment_model->saveSession($appointmentId, $sessionId);

            $app->tokbox_session = $sessionId;
        }
        $reason = $this->reason_model->getById($app->reason_id);
        $app->reason_name =  is_object($reason) && isset($reason->name) ? $reason->name : '';

        if ($type == 'doctor') {
            if ($app->doctors_id != $id) {
                show_error('The action you have requested is not allowed.', 403);
            }
        }
        else {
            if ($app->patients_id != $id) {
                show_error('The action you have requested is not allowed.', 403);
            }
        }

        $app->durationReadable = gmdate("i:s", $app->duration);

        /*echo $app->durationReadable;
        die();*/

        $patient = null;
        $isDoctor = $type == 'doctor';
        $doctorFiles = null;
        $patientFiles = null;
        if ($isDoctor) {
            $patientId = $app->patients_id;

            //get basic patient info
            $patient = $this->login_model->get_patient($patientId);
            $patient = reset($patient);

            //get patient age
            $age = getPatientAge($patient->dob);

            //get surgeries
            $health = $this->login_model->get_patient_health_condition('health', $patientId);
            //get surgeries
            $medication = $this->login_model->get_patient_health_condition('medication', $patientId);
            //get surgeries
            $surgeries = $this->login_model->get_patient_health_condition('surgery', $patientId);
            //get allergies
            $allergies = $this->login_model->get_patient_health_condition('allergies', $patientId);

            $patient->health = $health;
            $patient->medication = $medication;
            $patient->surgeries = $surgeries;
            $patient->allergies = $allergies;
            $patient->gender = gender($patient->gender);
            $patient->age = $age;

            $doctorFiles = $this->login_model->patient_upload_retrive($app->doctors_id);
//            $patientFiles = $this->login_model->patient_upload_retrive($patientId);
			$this->load->model('File_model');
            $patientFiles = $this->File_model->get_files_shared_by_patient($patientId);


        }

        $data = [];
        $data['apiKey'] = $tokboxApiKey;
        $data['token'] = $this->appointment_model->generateTokboxToken($tokboxApiKey, $tokboxApiSecret, $app->tokbox_session);
        $data['isDoctor'] = $isDoctor;
        $data['patient'] = $patient;
        $data['appointment'] = $app;
        $this->load->model('appointmentlog_model');
        $data['appointment_log'] = $this->appointmentlog_model->getByFields([
            'appointment_id' => $app->id
        ]);
        $data['doctorFiles'] = $doctorFiles;
        $data['patientFiles'] = $patientFiles;
        
//        echo "<pre>";
//        print_r($data);
//        die();

        //load view
        $this->load->view('header');
        if ($this->session->userdata('type') == 'patient') {
            $this->load->view('header_top');
            //$this->load->view('nav');
        } elseif ($this->session->userdata('type') == 'doctor') {
            $this->load->view('doctor_header_top');
            //$this->load->view('doctor_nav');
        }
        $this->load->view('appointment/start', $data);
        $this->load->view("footer");
    }

    public function beginSession()
    {
        $appointmentId = $this->input->post('aid');
        if (!$appointmentId) {
            show_error('Wrong action.', 404);
        }

        $this->appointment_model->updateById($appointmentId, [
            'session_start' => date('Y-m-d H:i:s'),
        ]);
    }

    public function endSession()
    {
        $appointmentId = $this->input->post('aid');
        if (!$appointmentId) {
            show_error('Wrong action.', 404);
        }

        $this->appointment_model->endSession($appointmentId);
    }

    public function endCall()
    {
        $type = $this->session->userdata('type');
        if ( $this->input->is_ajax_request() && $type === 'doctor')
        {
            $appointmentId = $this->input->post('aid');
            if (!$appointmentId) {
                $this->output->set_status_header(403);
                echo 'Access Denied! Invalid Appointment id.';
                exit;
            }

            $this->appointment_model->endCall($appointmentId);
        }
        else {
            $this->output->set_status_header(403);
            echo 'Access Denied! Invalid user type: ' . $this->session->userdata('type') .'.';
            exit;
        }

    }

    public function saveSoap()
    {
        $data = $this->input->post();
        $appointmentId = $data['id'];
        unset($data['id']);

        $result = $this->appointment_model->updateById($appointmentId, $data);

        if ($result) {
            echo 1;
        }
        else {
            echo 0;
        }
    }

    public function test()
    {
        $this->load->view('header');
        $this->load->view('header_top');
        $this->load->view('appointment/_appointment_container_new');
        $this->load->view("footer");
    }

    public function create()
    {
        $patientId = $this->session->userdata('userid');
        $doctorId = $this->input->post('doctor_id');
        $reasonId = $this->input->post('reason_id');
        $availId = $this->input->post('avail_id');
    }

    public function ajax_create_new() {
        $ret = array();
        if ( $this->input->is_ajax_request() ) {
            //get required
            $address_1 = $this->input->post('address_1', true);
            $address_2 = $this->input->post('address_2', true);
            $appointment_notes = $this->input->post('appointment_notes', true);
            $appointment_date = $this->input->post('avail', true);
            $city = $this->input->post('city', true);
            $doctor_id = $this->input->post('did', true);
            $name_on_card = $this->input->post('name_on_card', true);
            $phone = $this->input->post('phone', true);
            $patient_id = $this->input->post('pid', true);
            $state = $this->input->post('state', true);
            $stripeToken = $this->input->post('stripeToken', true);
            $no_token = $this->input->post('no_token', true);
            $zip = $this->input->post('zip', true);
            $country = $this->input->post('country', true);
            $reason_id = $this->input->post('reason_id', true);
            $reason = $this->input->post('reason', true);
            $avaialble_time_id = $this->input->post('avaialble_time_id', true);
            $consultation_type_id = $this->input->post('consultation_type', true);

            //fix appointment date
            $appointment_date_temp = explode('-', $appointment_date);
            $start_date = $appointment_date_temp[0];
            $end_date = $appointment_date_temp[1];

            $doctor = $this->doctor_model->get_doctor( $doctor_id );
            $patient = $this->patient_model->get_patient( $patient_id );

            $tmp_start = date('m/d/Y h:i a', $start_date);
            $tmp_end = date('m/d/Y h:i a', $end_date);

            if ( ! is_object($doctor) ) {
                $ret['error'][] = "Invalid doctor selected.";
            }

            if ( ! is_object($patient) ) {
                $ret['error'][] = "Invalid patient selected.";
            }

            //checked patient waived or not
            $patient_waived = $this->patient_model->patient_check_waive($doctor_id);

            if ( $patient_waived == 0 ) {
                //check for consultation_type and get payment rate
                if ( is_numeric($consultation_type_id) && $consultation_type_id > 0 ) {
                    $consultation_type_data = $this->consultationtype_model->get_consultation_by_id($consultation_type_id);
                    if (is_object($consultation_type_data) && isset($consultation_type_data->doctor_id) && $consultation_type_data->doctor_id == $doctor_id) {
                        $payment_rate = $consultation_type_data->rate;
                    } else {
                        //error
                        $ret['error'][] = "Invalid Consultation Type Selected.";
                    }
                } else {
                    $ret['error'][] = "No Consultation Type is Selected.";
                }
            }
            else {
                $payment_rate = $doctor->fee;
            }




            if ( empty($ret['error']) ) {
                //first check given doctor has this time frame
                if ( $this->appointment_model->check_appointment_available_for_doctor($doctor_id, $avaialble_time_id) == 0 ) {
                    $ret['error'][] = "Selected time ($tmp_start - $tmp_end) is invalid.";
                }
            }

            if ( empty($ret['error']) ) {
                //second check given time is available
                if ( $this->appointment_model->check_appointment_available($doctor_id, $start_date, $end_date) > 0 ) {

                    $ret['error'][] = "Selected time ($tmp_start - $tmp_end) is not available for new schedule.";
                }
            }

            if ( $stripeToken && empty($ret['error']) ) {

                try {

                    $ip =   getenv( 'HTTP_CLIENT_IP' )      ?: getenv( 'HTTP_X_FORWARDED_FOR' ) ?:
                        getenv( 'HTTP_X_FORWARDED' )    ?: getenv( 'HTTP_FORWARDED_FOR' ) ?:
                            getenv( 'HTTP_FORWARDED' )      ?: getenv( 'REMOTE_ADDR' );

                    $description_text = "My Virtual Doctor charge patient: $name_on_card, start date: {$tmp_start}, end date: {$tmp_end} ";

                    \Stripe\Stripe::setApiKey($doctor->stripe_one);

                    $charge = \Stripe\Charge::create(array(
                        "amount" => $payment_rate * 100,
                        "currency" => "usd",
                        "source" => $stripeToken, // obtained with Stripe.js
                        "description" => $description_text,
                        "metadata" => array(
                            'ip_address' => $ip,
                            'full_name' => $name_on_card,
                            'address_line_1' => $address_1,
                            'address_line_2' => $address_2,
                            'phone' => $phone,
                            'country' => $country,
                            'city' => $city,
                            'zip' => $zip,
                            'state' => $state,
                            'patient_id' => $patient_id,
                            'start_date' => $tmp_start,
                            'end_date' => $tmp_end,
                            'available_id' => $avaialble_time_id
                        )
                    ));

                    $charge_obj = $charge->__toArray( true );

                    $charge_id = $charge_obj['id'];

                    //get charge via api to check payment is completed or not
                    $retrive_charge = \Stripe\Charge::retrieve( $charge_id );
                    $retrive_charge_obj = $retrive_charge->__toArray( true );

                    if ( $retrive_charge_obj['status'] == 'succeeded' ) {
                        //four insert into appointment table
                        $data_for_db = array(
                            'patients_id' => $patient_id,
                            'doctors_id' => $doctor_id,
                            'available_id' => $avaialble_time_id,
                            'start_date_stamp' => $start_date,
                            'end_date_stamp' => $end_date,
                            'reason_id' => $reason_id,
                            'reason' => $reason,
                            'notes' => $appointment_notes,
                            'created_date' => date('Y-m-d H:i:s', time()),
                            'session_start' => date('Y-m-d H:i:s', $start_date),
                            'session_end' => date('Y-m-d H:i:s', $end_date),
                            'stripe_transaction_id' => $charge_id,
                            //'consultation_type' => $consultation_type_id,
                            'amount' => $payment_rate,
                            'status' => 3,
                            'added' => time()
                        );

                        if ( $consultation_type_id > 0 ) {
                            $data_for_db['consultation_type'] = $consultation_type_id;
                        }


                    }

                } catch (Exception $e) {
                    $error = true;
                    $fail_message = $e->getMessage();
                    $ret['error'][] = $fail_message;
                }

            }
            elseif( isset($no_token) &&  $no_token == 'yes' && empty($ret['error']) ) {

                //make sure that doctor enabled waived for this patient
                $waived = $this->patient_model->patient_check_waive($doctor_id);

                if ( $waived > 0 ) {
                    $data_for_db = array(
                        'patients_id' => $patient_id,
                        'doctors_id' => $doctor_id,
                        'available_id' => $avaialble_time_id,
                        'start_date_stamp' => $start_date,
                        'end_date_stamp' => $end_date,
                        'reason_id' => $reason_id,
                        'reason' => $reason,
                        'notes' => $appointment_notes,
                        'created_date' => date('Y-m-d H:i:s', time()),
                        'session_start' => date('Y-m-d H:i:s', $start_date),
                        'session_end' => date('Y-m-d H:i:s', $end_date),
                        'stripe_transaction_id' => 'waived',
                        //'consultation_type' => $consultation_type_id,
                        'amount' => $doctor_id,
                        'status' => 3,
                        'added' => time()
                    );

                    if ( $consultation_type_id > 0 ) {
                        $data_for_db['consultation_type'] = $consultation_type_id;
                    }
                }
                else {
                    $ret['error'][] = 'Invalid Input. Doctor didn\'t added free payment for this patient.';
                }
            }
            else {
                $ret['error'][] = 'Invalid Stripe Token.';
            }


            if ( isset($data_for_db) && is_array($data_for_db) && !empty($data_for_db) && empty($ret['error'])) {
                $insert_id = $this->appointment_model->add_appointment($data_for_db);

                if ( $insert_id > 0 ) {
                    //fifth send success message
                    $ret['success'] = 'yes';
                    $ret['message'][] = "<strong>Congratulations!</strong> Schedule is successfully placed.";


                    //send doctor notification
                    $patient_url = base_url() . 'doctor/patientinfo/' . $patient->id;
                    $consult_url = base_url() . 'doctor/consultations';
                    $message = "<a href='{$patient_url}'>{$patient->firstname} {$patient->lastname}</a> requested a session on <a href='{$consult_url}'>{$tmp_start}</a>.";

                    $data = array(
                        'from_userid' => $patient_id,
                        'to_userid' => $doctor_id,
                        'from_usertype' => 'patient',
                        'to_usertype' => 'doctor',
                        'message' => $message,
                        'type' => 'appointment',
                        'added' => time()
                    );
                    $this->notification_model->add_notitication($data);

                    //send patient email about this appointment
                    $this->load->library('email');

                    //now send doctor the same email
                    $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
                    $this->email->to($doctor->user_email);

                    $this->email->subject('MyVirtualDoctor – Consultation Notification');
                    $this->email->message('
<html>
<head>
    <title></title>
</head>
<body>
    <h4>Dear ' . $doctor->firstname . '</h4>
    <p></p>
    <p>Patient '. $patient->firstname . ' ' . $patient->lastname .' has scheduled an online consultation with you on <a href=' . $consult_url .'>' .$tmp_start .'</a></p>
    <p></p>
    <p>Please login to your account on My Virtual Doctor using the link below for more details.</p>
    <p></p>
    <p>Click this link to login <a href="' . base_url() . 'login">' . base_url() .'login</a></p>
    <p></p>
    <p>Please don\'t hesitate to contact us at  1-855-80-DOCTOR or at info@myvirtualdoctor.com for further assistance.</p>
    <p></p>
    <p>Sincerely,<br />
My Virtual Doctor</p>
</body>
</html>');
                    $this->email->send(true, true);

                }
            }

        }
        else {
            $ret['error'][] = 'Invalid request';
        }
        header('Content-Type: application/json');
        echo json_encode( $ret );
    }

    public function ajax_edit_appointment() {
        $ret = array();
        if ( $this->input->is_ajax_request() ) {
            //get required fields
            $appointment_id = $this->input->post('appointment_id', true);
            $stripeToken = $this->input->post('stripeToken', true);
            $no_token = $this->input->post('no_token', true);
            $me = $this->session->userdata('userid');

            //now get appointment info
            $appointment = $this->appointment_model->get_appointment_by_id( $appointment_id );

            $address_1 = $this->input->post('address_1', true);
            $address_2 = $this->input->post('address_2', true);
            $appointment_notes = $this->input->post('appointment_notes', true);

            $city = $this->input->post('city', true);
            $name_on_card = $this->input->post('name_on_card', true);
            $phone = $this->input->post('phone', true);
            $state = $this->input->post('state', true);
            $zip = $this->input->post('zip', true);
            $country = $this->input->post('country', true);
            $reason_id = $this->input->post('reason_id', true);
            $reason = $this->input->post('reason', true);

            //fix appointment date
            $start_date = $appointment->start_date_stamp;
            $end_date = $appointment->end_date_stamp;

            $doctor = $this->doctor_model->get_doctor( $appointment->doctors_id );
            $patient = $this->patient_model->get_patient( $appointment->patients_id );

            $tmp_start = date('m/d/Y h:i a', $start_date);
            $tmp_end = date('m/d/Y h:i a', $end_date);

            if ( ! is_object($doctor) ) {
                $ret['error'][] = "Invalid doctor selected.";
            }

            if ( ! is_object($patient) ) {
                $ret['error'][] = "Invalid patient selected.";
            }

            if ( is_object($appointment) && $stripeToken && $appointment->patients_id == $me && empty($ret['error']) ) {
                    try {

                        $ip =   getenv( 'HTTP_CLIENT_IP' )      ?: getenv( 'HTTP_X_FORWARDED_FOR' ) ?:
                            getenv( 'HTTP_X_FORWARDED' )    ?: getenv( 'HTTP_FORWARDED_FOR' ) ?:
                                getenv( 'HTTP_FORWARDED' )      ?: getenv( 'REMOTE_ADDR' );

                        $description_text = "My Virtual Doctor charge patient: $name_on_card, start date: {$tmp_start}, end date: {$tmp_end} ";

                        \Stripe\Stripe::setApiKey($doctor->stripe_one);

                        $charge = \Stripe\Charge::create(array(
                            "amount" => $appointment->amount * 100,
                            "currency" => "usd",
                            "source" => $stripeToken, // obtained with Stripe.js
                            "description" => $description_text,
                            "metadata" => array(
                                'ip_address' => $ip,
                                'full_name' => $name_on_card,
                                'address_line_1' => $address_1,
                                'address_line_2' => $address_2,
                                'phone' => $phone,
                                'country' => $country,
                                'city' => $city,
                                'zip' => $zip,
                                'state' => $state,
                                'patient_id' => $appointment->patients_id,
                                'start_date' => $tmp_start,
                                'end_date' => $tmp_end,
                                'available_id' => $appointment->available_id
                            )
                        ));

                        $charge_obj = $charge->__toArray( true );

                        $charge_id = $charge_obj['id'];

                        //get charge via api to check payment is completed or not
                        $retrive_charge = \Stripe\Charge::retrieve( $charge_id );
                        $retrive_charge_obj = $retrive_charge->__toArray( true );

                        if ( $retrive_charge_obj['status'] == 'succeeded' ) {
                            $data_for_db = array(
                                'reason_id' => $reason_id,
                                'reason' => $reason,
                                'notes' => $appointment_notes,
                                'stripe_transaction_id' => $charge_id,
                                'status' => 3
                            );

                        }

                    } catch (Exception $e) {
                        $error = true;
                        $fail_message = $e->getMessage();
                        $ret['error'][] = $fail_message;
                    }


            }
            elseif( is_object($appointment) && $appointment->patients_id == $me && isset($no_token) &&  $no_token == 'yes' && empty($ret['error']) ) {

                //make sure that doctor enabled waived for this patient
                $waived = $this->patient_model->patient_check_waive($appointment->doctors_id);

                if ( $waived > 0 ) {
                    $data_for_db = array(
                        'reason_id' => $reason_id,
                        'reason' => $reason,
                        'notes' => $appointment_notes,
                        'stripe_transaction_id' => 'waived',
                        'status' => 3
                    );
                }
                else {
                    $ret['error'][] = 'Invalid Input. Doctor didn\'t added free payment for this patient.';
                }
            }
            else {
                $ret['error'][] = 'Invalid appointment selected.';
            }

        }
        else {
            $ret['error'][] = 'Invalid request';
        }

        //now make database entry
        if ( isset($data_for_db) && is_array($data_for_db) && !empty($data_for_db) && empty($ret['error']) ) {
            //four insert into appointment table


            $where = array(
                'id' => $appointment->id
            );

            $count = $this->appointment_model->update_appointment($data_for_db, $where);

            if ( $count > 0 ) {
                //fifth send success message
                $ret['success'] = 'yes';
                $ret['message'][] = "<strong>Congratulations!</strong> Schedule is successfully placed.";


                //send doctor notification
                $patient_url = base_url() . 'doctor/patientinfo/' . $patient->id;
                $consult_url = base_url() . 'doctor/consultations';
                $appointment_date = date('m/d/Y h:i a', $appointment->added);
                $message = "<a href='{$patient_url}'>{$patient->firstname} {$patient->lastname}</a> accepted a session on <a href='{$consult_url}'>{$tmp_start}</a> created by you on {$appointment_date}.";

                $data = array(
                    'from_userid' => $appointment->patients_id,
                    'to_userid' => $appointment->doctors_id,
                    'from_usertype' => 'patient',
                    'to_usertype' => 'doctor',
                    'message' => $message,
                    'type' => 'appointment',
                    'added' => time()
                );
                $this->notification_model->add_notitication($data);

                //send patient email about this appointment
                $this->load->library('email');

                //now send doctor the same email
                $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
                $this->email->to($doctor->user_email);

                $this->email->subject('MyVirtualDoctor – Consultation Notification');
                $this->email->message('
<html>
<head>
    <title></title>
</head>
<body>
    <h4>Dear ' . $doctor->firstname . '</h4>
    <p></p>
    <p>Patient '. $patient->firstname . ' ' . $patient->lastname .' has confirmed the consultation that was setup earlier by you for <a href=' . $consult_url .'>' .$tmp_start .'</a> created on ' . $appointment_date .'</p>
    <p></p>
    <p>Please login to your account on My Virtual Doctor using the link below for more details.</p>
    <p></p>
    <p>Click this link to login <a href="' . base_url() . 'login">' . base_url() .'login</a></p>
    <p></p>
    <p>Please don\'t hesitate to contact us at  1-855-80-DOCTOR or at info@myvirtualdoctor.com for further assistance.</p>
    <p></p>
    <p>Sincerely,<br />
My Virtual Doctor</p>
</body>
</html>');
                $this->email->send(true, true);

            }
        }


        header('Content-Type: application/json');
        echo json_encode( $ret );
    }


    public function ajax_get_doctors_available_time()
    {
        if ( $this->input->is_ajax_request() ) {
            $id = $this->input->post('id', true);
            $date = $this->input->post('select_date', true);
            $month = $this->input->post('select_month', true);
            $data = $this->appointment_model->get_doctors_available($id, $month, $date);
            if (is_array($data)) {
                foreach ($data as $av) {

                    //divide date into 30 minutes chunks
                    $tmp_start_date = $av->start_date_stamp;
                    $tmp_end_date = strtotime('+ 30 minutes', $av->start_date_stamp);

                    if ( round(abs($av->end_date_stamp - $av->start_date_stamp) / 60,2) > 30) {

                        while( $tmp_end_date <= $av->end_date_stamp ) {
                            if ( $this->appointment_model->check_appointment_available($id, $tmp_start_date, $tmp_end_date) === 0 )
                            {
                                //check for past hours
                                /*
                                 $now = time();
                                $btn_class = 'picker-slots-button';
                                if ( $tmp_start_date <= $now ) {
                                    $disabled = 'disabled="disabled"';
                                    $btn_class = '';
                                } else {
                                    $disabled = '';
                                }
                                 */
                                $now = time();
                                $btn_class = 'picker-slots-button';
                                if ( $tmp_start_date <= $now ) {
                                    $disabled = 'disabled="disabled"';
                                    $btn_class = '';
                                } else {
                                    $start_time = date('h:i A', $tmp_start_date);
                                    $end_time = date('h:i A', $tmp_end_date);
                                    $full_date = date('l, F d Y', $tmp_start_date);
                                    echo '<button type="button" class="'. $btn_class .' pt-cl-cnt-btn" data-available="' . $av->id . '" data-start_time="' . $start_time . '" data-end_time="' . $end_time . '" data-date="' . $av->start_date . '" data-fulldate="'. $full_date. '" data-start_date="'. $tmp_start_date .'" data-end_date="' . $tmp_end_date . '">' . $start_time . '</button>';
                                }
                            }
                            $tmp_start_date = $tmp_end_date;
                            $tmp_end_date = strtotime('+30 minutes', $tmp_start_date);

                        }

                    } else {
                        //check this time is already selected or not
                        if ( $this->appointment_model->check_appointment_available($id, $av->start_date_stamp, $av->end_date_stamp) === 0 )
                        {
                            $now = time();
                            $btn_class = 'picker-slots-button ';
                            if ( $tmp_start_date <= $now ) {
                                $disabled = 'disabled="disabled"';
                                $btn_class = '';
                            } else {
                                $start_time = date('h:i A', $av->start_date_stamp);
                                $end_time = date('h:i A', $av->end_date_stamp);
                                $full_date = date('l, F d Y', $tmp_start_date);
                                echo '<button type="button" class="'. $btn_class .' pt-cl-cnt-btn" data-available="' . $av->id . '" data-start_time="' . $start_time . '" data-end_time="' . $end_time . '" data-date="' . $av->start_date . '" data-fulldate="'. $full_date. '" data-start_date="'. $tmp_start_date .'" data-end_date="' . $tmp_end_date . '">' . $start_time . '</button>';
                            }
                        }
                    }
                }
            } else {
                echo $data;
            }
        } else {
            echo 'Invalid request.';
        }

    }

    public function ajax_doctor_create_new() {
        $ret = array();
        if ( $this->input->is_ajax_request() ) {
            //get required
            $appointment_notes = $this->input->post('appointment_notes', true);
            $appointment_date = $this->input->post('avail', true);
            $doctor_id = $this->session->userdata('userid');
            $patient_id = $this->input->post('pid', true);
            $reason_id = $this->input->post('reason_id', true);
            $reason = $this->input->post('reason', true);
            $avaialble_time_id = $this->input->post('avaialble_time_id', true);
            $consultation_type_id = $this->input->post('consultation_type', true);

            //fix appointment date
            $appointment_date = explode('-', $appointment_date);
            $start_date = $appointment_date[0];
            $end_date = $appointment_date[1];

            $doctor = $this->doctor_model->get_doctor( $doctor_id );
            $patient = $this->patient_model->get_patient( $patient_id );

            $tmp_start = date('m/d/Y h:i a', $start_date);
            $tmp_end = date('m/d/Y h:i a', $end_date);

            if ( ! is_object($doctor) ) {
                $ret['error'][] = "Invalid doctor selected.";
            }

            if ( ! is_object($patient) ) {
                $ret['error'][] = "Invalid patient selected.";
            }

            //checked patient waived or not
            $patient_waived = $this->doctor_model->doctor_check_waive($patient_id);

            if ( $patient_waived == 0 ) {
                //check for consultation_type and get payment rate
                if ( is_numeric($consultation_type_id) && $consultation_type_id > 0 ) {
                    $consultation_type_data = $this->consultationtype_model->get_consultation_by_id($consultation_type_id);
                    if (is_object($consultation_type_data) && isset($consultation_type_data->doctor_id) && $consultation_type_data->doctor_id == $doctor_id) {
                        $payment_rate = $consultation_type_data->rate;
                    } else {
                        //error
                        $ret['error'][] = "Invalid Consultation Type Selected.";
                    }
                } else {
                    $ret['error'][] = "No Consultation Type is Selected.";
                }
            }
            else
            {
                $payment_rate = $doctor->fee;
            }


            if ( empty($ret['error']) ) {
                //first check given doctor has this time frame
                if ( $this->appointment_model->check_appointment_available_for_doctor($doctor_id, $avaialble_time_id) == 0 ) {
                    $ret['error'][] = "Selected time ($tmp_start - $tmp_end) is invalid.";
                }
            }

            if ( empty($ret['error']) ) {
                //second check given time is available
                if ( $this->appointment_model->check_appointment_available($doctor_id, $start_date, $end_date) > 0 ) {

                    $ret['error'][] = "Selected time ($tmp_start - $tmp_end) is not available for new schedule.";
                }
            }

            if ( empty($ret['error']) ) {
                //now insert new appointment
                $data = array(
                    'patients_id' => $patient_id,
                    'doctors_id' => $doctor_id,
                    'available_id' => $avaialble_time_id,
                    'start_date_stamp' => $start_date,
                    'end_date_stamp' => $end_date,
                    'reason_id' => $reason_id,
                    'reason' => $reason,
                    'notes' => $appointment_notes,
                    'created_date' => date('Y-m-d H:i:s', time()),
                    'session_start' => date('Y-m-d H:i:s', $start_date),
                    'session_end' => date('Y-m-d H:i:s', $end_date),
                    //'consultation_type' => $consultation_type_id,
                    'amount' => $payment_rate,
                    'status' => 1,
                    'added' => time()
                );

                if ( $consultation_type_id > 0 ) {
                    $data['consultation_type'] = $consultation_type_id;
                }

                $insert_id = $this->appointment_model->add_appointment($data);

                if ($insert_id > 0) {
                    //fifth send success message
                    $ret['success'] = 'yes';
                    $ret['message'][] = "<strong>Congratulations!</strong> Schedule has been successfully placed.";


                    //send patient notification
                    $doctor_url = base_url() . 'patient/doctorinfo/' . $doctor->id;
                    $consult_url = base_url() . 'patient/appointments';
                    $view_link = base_url() . 'patient/appointments/' . $insert_id;

                    $message = "<a href='{$doctor_url}'>{$doctor->firstname} {$doctor->lastname}</a> has scheduled an online session with you for <a href='{$consult_url}'>{$tmp_start}</a>. Click <a href='{$view_link}' data-id='{$insert_id}' class='pending_appoint_for_patient'>here</a> to view this appointment.";

                    //sachin parikh has scheduled an online session with you for 8-23-2016 : 6:15 am (EDT).

                    $data = array(
                        'from_userid' => $doctor_id,
                        'to_userid' => $patient_id,
                        'from_usertype' => 'doctor',
                        'to_usertype' => 'patient',
                        'message' => $message,
                        'type' => 'appointment',
                        'added' => time()
                    );
                    $this->notification_model->add_notitication($data);

                    //send patient email about this appointment
                    $this->load->library('email');

                    $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
                    $this->email->to($patient->user_email);

                    $this->email->subject('MyVirtualDoctor – Consultation Notification');
                    $this->email->message('
<html>
<head>
    <title></title>
</head>
<body>
    <h4>Dear ' . $patient->firstname . '</h4>
    <p></p>
    <p>Doctor ' . $doctor->firstname . ' ' . $doctor->lastname . ' has scheduled an online consultation with you on ' . $tmp_start . '. However, the consultation is now pending approval from your end.</p>
    <p></p>
    <p>Please login to your account on My Virtual Doctor using the link below for more details.</p>
    <p></p>
    <p>Click this link to login <a href="' . base_url() . 'login">' . base_url() . 'login</a></p>
    <p></p>
    <p>Please don\'t hesitate to contact us at  1-855-80-DOCTOR or at info@myvirtualdoctor.com for further assistance.</p>
    <p></p>
    <p>Sincerely,<br />
My Virtual Doctor</p>
</body>
</html>');
                    $this->email->send(true, true);

                }

            }



        }
        else {
            $ret['error'][] = 'Invalid request';
        }
        header('Content-Type: application/json');
        echo json_encode( $ret );
    }

    public function ajax_doctor_edit_appointment( $appointment_id ) {
        $ret = array();
        if ( $this->input->is_ajax_request() ) {
            //now get appointment info
            $appointment = $this->appointment_model->get_appointment_by_id( $appointment_id );
            $me = $this->session->userdata('userid');
            //now security check
            if ( is_object($appointment) ) {
                //now check security
                //1. check this patient appointment
                if ( $appointment->doctors_id != $me ) {
                    $ret['error'][] = 'Invalid appointment id. This appointment is not assigned to you.';
                }

                //2. check appointment is pending
                if ( $appointment->status != '5' ) {
                    $ret['error'][] = 'Invalid appointment id. Appointment status is not cancelled.';
                }

                //get required
                $appointment_notes = $this->input->post('appointment_notes', true);
                $appointment_date = $this->input->post('avail', true);
                $reason_id = $this->input->post('reason_id', true);
                $reason = $this->input->post('reason', true);
                $available_time_id = $this->input->post('avaialble_time_id', true);

                //fix appointment date
                $appointment_date = explode('-', $appointment_date);
                $start_date = $appointment_date[0];
                $end_date = $appointment_date[1];

                $doctor = $this->doctor_model->get_doctor( $appointment->doctors_id );
                $patient = $this->patient_model->get_patient( $appointment->patients_id );

                $tmp_start = date('m/d/Y h:i a', $start_date);
                $tmp_end = date('m/d/Y h:i a', $end_date);

                if ( empty($ret['error']) ) {
                    //first check given doctor has this time frame
                    if ( $this->appointment_model->check_appointment_available_for_doctor($appointment->doctors_id, $available_time_id) == 0 ) {
                        $ret['error'][] = "Selected time ($tmp_start - $tmp_end) is invalid.";
                    }
                }

                if ( empty($ret['error']) ) {
                    //second check given time is available
                    if ( $this->appointment_model->check_appointment_available($appointment->doctors_id, $start_date, $end_date) > 0 ) {

                        $ret['error'][] = "Selected time ($tmp_start - $tmp_end) is not available for new schedule.";
                    }
                }

                //now update appointment
                if ( empty($ret['error']) ) {
                    $data = array(
                        'available_id' => $available_time_id,
                        'start_date_stamp' => $start_date,
                        'end_date_stamp' => $end_date,
                        'reason_id' => $reason_id,
                        'reason' => $reason,
                        'notes' => $appointment_notes,
                        'created_date' => date('Y-m-d H:i:s', time()),
                        'session_start' => date('Y-m-d H:i:s', $start_date),
                        'session_end' => date('Y-m-d H:i:s', $end_date),
                        'status' => 3,
                        'added' => time()
                    );
                    $where = array(
                        'id' => $appointment->id
                    );

                    $insert_id = $this->appointment_model->update_appointment($data, $where);

                    if ($insert_id > 0) {
                        //fifth send success message
                        $ret['success'] = 'yes';
                        $ret['message'][] = "<strong>Congratulations!</strong> Appointment has been successfully rescheduled!";


                        //send patient notification
                        $doctor_url = base_url() . 'patient/doctorinfo/' . $doctor->id;
                        $consult_url = base_url() . 'patient/appointments';
                        $view_link = base_url() . 'patient/appointments/' . $insert_id;

                        $message = "<a href='{$doctor_url}'>{$doctor->firstname} {$doctor->lastname}</a> has rescheduled an online session with you for <a href='{$consult_url}'>{$tmp_start}</a>. Click <a href='{$view_link}'>here</a> to view this appointment.";


                        $data = array(
                            'from_userid' => $appointment->doctors_id,
                            'to_userid' => $appointment->patients_id,
                            'from_usertype' => 'doctor',
                            'to_usertype' => 'patient',
                            'message' => $message,
                            'type' => 'appointment',
                            'added' => time()
                        );
                        $this->notification_model->add_notitication($data);

                        //send patient email about this appointment
                        $this->load->library('email');

                        //now send patient email
                        $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
                        $this->email->to($patient->user_email);

                        $this->email->subject('MyVirtualDoctor – Consultation Notification');
                        $this->email->message('
<html>
<head>
    <title></title>
</head>
<body>
    <h4>Dear ' . $patient->firstname . '</h4>
    <p></p>
    <p>Doctor ' . $doctor->firstname . ' ' . $doctor->lastname . ' has re-scheduled the online consultation with you on ' . $tmp_start . '</p>
    <p>No confirmation is needed from your end at this point.</p>
    <p></p>
    <p>Please login to your account on My Virtual Doctor using the link below for more details.</p>
    <p></p>
    <p>Click this link to login <a href="' . base_url() . 'login">' . base_url() . 'login</a></p>
    <p></p>
    <p>Please don\'t hesitate to contact us at  1-855-80-DOCTOR or at info@myvirtualdoctor.com for further assistance.</p>
    <p></p>
    <p>Sincerely,<br />
My Virtual Doctor</p>
</body>
</html>');
                        $this->email->send(true, true);


                    }

                }



            } else {
                $ret['error'][] = 'Invalid Appointment.';
            }
        }
        else {
            $ret['error'][] = 'Invalid request';
        }
        header('Content-Type: application/json');
        echo json_encode( $ret );
    }

    public function get_consultation_start_time($a_id){
        $this->load->model('appointmentlog_model');
        $appointment_log_arr = $this->appointmentlog_model->getByFields([
            'appointment_id' => $a_id
        ]);
        if(count($appointment_log_arr) == 0){
            $al_id = $this->appointmentlog_model->add([
                'appointment_id' => $a_id,
                'status' => 'accepted',
                'reason' => '',
                'stamp' => ''
            ]);
            echo json_encode([
                'bool' => false
            ]);
            return;
        }
        if(empty($appointment_log_arr[0]->session_start_stamp)){
            echo json_encode([
                'bool' => false
            ]);
            return;
        }
        $current_time = time();
        $interval = date_diff(date_create(date('Y-m-d H:i:s', $appointment_log_arr[0]->session_start_stamp)), date_create(date('Y-m-d H:i:s', $current_time)));
        $hour = $interval->format('%H');
        $minute = $interval->format('%i');
        $second = $interval->format('%s');
        $time_seconds = $hour * 3600 + $minute * 60 + $second;
        echo json_encode([
            'seconds' => $time_seconds,
            'bool' => true
        ]);
    }

    public function request_to_start_consult_btn($a_id, $isVideoPage = false){
        $this->load->model('knocking_model');
        if($isVideoPage) {
            $this->knocking_model->doctor_available($a_id);
        }
        $this->load->model('appointment_model');
        $appointment_data = $this->appointment_model->getById($a_id);
        $current_time = time();
        $return = [];
        $return['message'] = '';
        $return['is_waiting'] = false;
        $return['show_button'] = false;
        $patient_approved_arr = $this->knocking_model->check_patient_approval($a_id);
        $return['patient_approved'] = (($patient_approved_arr) ? $patient_approved_arr['bool'] : false );
        $return['bool'] = false;
        if($appointment_data){
            $start_time = $appointment_data->start_date_stamp;
            $end_time = $appointment_data->end_date_stamp;
            $return['debug'] = [
                'current_time' => $current_time,
                'start_time' => $start_time,
                'before_5_mints_start_time' => strtotime("-5 minutes", $start_time)
            ];
            if(($current_time >= $start_time || $current_time >= strtotime("-5 minutes", $start_time)) && $current_time <= $end_time){
//                true
                if($this->session->userdata('type') == 'patient'){
                    if($current_time >= strtotime("-5 minutes", $start_time)){
                        $return['is_waiting'] = true;
                    }
                } else if($this->session->userdata('type') == 'doctor') {
                    if($current_time >= $start_time){
                        $return['show_button'] = true;
                    }
                }
                $return['bool'] = true;
            } else {
//                false
                $this->load->model('appointmentlog_model');
                $appointmentlog_data = $this->appointmentlog_model->getByFields([
                    'appointment_log.appointment_id' => $a_id,
                    'appointment_log.status' => 'delayed'
                ]);
                if($appointmentlog_data){
                    $start_time = strtotime('15 minutes', $start_time);
                    $end_time = strtotime('15 minutes', $end_time);
                    if(($current_time >= $start_time || $current_time >= strtotime("-20 minutes", $start_time)) && $current_time <= $end_time){
//                            true
                        if($this->session->userdata('type') == 'patient'){
                            if($current_time >= strtotime("-20 minutes", $start_time)){
                                $return['is_waiting'] = true;
                            }
                        } else if($this->session->userdata('type') == 'doctor') {
                            if($current_time >= $start_time){
                                $return['show_button'] = true;
                            }
                        }
                        $return['bool'] = true;
                    }
                }
            }
        }
        if(!$return['bool']){
            if($this->session->userdata('type') == 'patient'){
                $return['message'] = 'Your consultation is not ready yet. You can only enter the waiting room 5 minutes before the appointment time.';
            } else if($this->session->userdata('type') == 'doctor') {
                $return['message'] = 'This consultation is not ready yet. You can only enter the video conference screen 5 minutes prior to the appointment time.';
            }
        }
        echo json_encode($return);
    }
}