<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Queue_email extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		if (!$this->input->is_cli_request()) {
//			show_error('CLI only');
		}
		
//		echo PHP_EOL . get_current_user() . PHP_EOL;

		$this->load->library('email');
	}

	public function index()
	{
		show_error('CLI only');
	}

	public function send_queue()
	{
		$this->email->send_queue();
	}

	public function retry_queue()
	{
		$this->email->retry_queue();
	}
	
	function empty_cron_table() {
		// cron is working fine, now we don't need this because we are no longer storing things in cron table
//		$this->db->where('running', 0);
//		$this->db->where('DATE_FORMAT(ran_at, "%Y-%m-%d") <', date('Y-m-d H:i:s', strtotime('-24 hours')));
//		$this->db->delete('cron');
//		echo $this->db->last_query();
	}

}
