<?php


class Change extends Auth_Controller
{
	function __construct() {
		parent::__construct();
	}
	
    public function emailpassword()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
        $config = array(
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|min_length[8]'
            ),
            array(
                'field' => 'passconf',
                'label' => 'Password Confirmation',
                'rules' => 'trim|required|matches[password]'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            //load view
        }
        else
        {
            $password = $this->input->post('password', true);
            $update_array = array(
                'user_password' => password($password)
            );
            $where = array(
                'id' => $this->session->userdata('userid')
            );

            $updated = $this->login_model->update_password($update_array, $where);

            if ($updated) {
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Your Password Updated Successfully!</div>');
                $this->load->library('email');
                $this->email->from('info@myvirtualdoctor.com', 'Myvirtualdoctor.com');
                $this->email->to($this->user_model->getEmail('id', $this->session->userdata('userid')));
                $this->email->subject('Password Change');
                $patientName = $this->patient_model->get_patient();
                $this->email->message('Dear '.$patientName->firstname.' '.$patientName->lastname.',
                <br />
                <br />
                Your password successfully changed. Your new password is <strong>'.$password.'</strong>
                <br />
                <br />
                <a href="http://www.myvirtualdoctor.com/app/login">Login Here</a>
                <br />
                <br />
                Thank You.');
                $this->email->send();
            }
        }
        if ($this->session->userdata('patient') == 'patient'){
            $this->load->view('header');
            $this->load->view('header_top');
            $this->load->view('nav');
            $this->load->view("emailpassword/change_email_password");
            $this->load->view("footer");
//            unset($_SESSION['message']);
//            $this->session->unset_userdata('message');
        }





        /*$email = $this->input->post('email');
        $password = $this->input->post('password');
        $update_array = array(
            'user_email' => $email,
            'user_password' => $password
        );
        $data['user'] = $this->login_model->get_user($this->session->userdata('userid'));
        if ($this->session->userdata('patient') == 'patient') {
            if ($this->input->post('save') == '1') {
                $updated = $this->login_model->update_email_password($update_array);
                if ($updated) {
                    $this->login_model->update_patient_email($email);
                    $this->session->set_flashdata('message', 'Your Information Updated Successfully!');
                }
            }
            $data['user'] = $this->login_model->get_user($this->session->userdata('userid'));
            $this->load->view('header');
            $this->load->view('header_top');
            $this->load->view('nav');
            $this->load->view("emailpassword/change_email_password", $data);
            $this->load->view("footer");
        } elseif ($this->session->userdata('doctor') == 'doctor') {
            if ($this->input->post('save') == '1') {
                $updated = $this->login_model->update_email_password($update_array);
                if ($updated) {
                    $this->login_model->update_doctor_email($email);
                    $this->session->set_flashdata('message', 'Your Information Updated Successfully!');
                }
            }
            $data['user'] = $this->login_model->get_user($this->session->userdata('userid'));
            $this->load->view('header');
            $this->load->view('doctor_header_top');
            $this->load->view('doctor_nav');
            $this->load->view("emailpassword/change_email_password", $data);
            $this->load->view("footer");
        }*/

    }

}