<?php /** @var array $familyMembers */?>

<?php echo form_open('patient/addFamilyMember', ['id'=>'form-add-family']); ?>
    <section class="card">
        <div class="card-block">
            <?php if (count($familyMembers)) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h5>Your family</h5>
                        <ul class="list-group" style="list-style: disc;padding-left: 10px;">
                            <?php foreach ($familyMembers as $member) { ?>
                                <li class="list-group-item">
                                    <?php echo $member->firstname. ' ' . $member->lastname ?>
                                    <a class="delete_family_member pull-right" data-userid="<?php echo $member->user_id; ?>"><i class="fa fa-trash-o"></i></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div><br/>
            <?php } ?>

<!--            <div class="row">-->
<!--                <div class="col-md-2">-->
<!--                    <button type="button" class="btn btn-inline btn-success" id="button-add-member">Add new family member</button>-->
<!--                </div>-->
<!--            </div>-->

            <div class="row" id="row-add-member" style="display: none;">
                <div class="col-sm-6">
                    <fieldset class="form-group">
                        <label class="form-label semibold">First Name</label>
                        <input type="text" class="form-control" name="fname">
                    </fieldset>
                </div>
                <div class="col-sm-6">
                    <fieldset class="form-group">
                        <label class="form-label semibold">Last Name</label>
                        <input type="text" class="form-control" name="lname">
                    </fieldset>
                </div>

                <div class="col-sm-6">
                    <fieldset class="form-group">
                        <label class="form-label semibold">Email</label>
                        <input type="email" class="form-control"name="email">
                    </fieldset>
                </div>

                <div class="col-sm-6">
                    <fieldset class="form-group">
                        <label class="form-label semibold">Address</label>
                        <input type="text" class="form-control" name="address">
                    </fieldset>
                </div>

                <div class="col-sm-6">
                    <fieldset class="form-group">
                        <label class="form-label semibold">City</label>
                        <input type="text" class="form-control" name="city">
                    </fieldset>
                </div>

                <div class="col-sm-6">
                    <fieldset class="form-group">
                        <label class="form-label semibold">State</label>
                        <select name="state" class="select2">
                            <option value="">Select States</option>
                            <?php foreach ($states as $index => $state) { ?>
                                <option value="<?php echo $index; ?>">
                                    <?php echo $state; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>
                </div>

                <div class="col-sm-6">
                    <fieldset class="form-group">
                        <label class="form-label semibold">Zip</label>
                        <input type="text" class="form-control" name="zip">
                    </fieldset>
                </div>

                <div class="col-sm-6">
                    <fieldset class="form-group">
                        <label class="form-label semibold">Date of Birth</label>

                        <div class='input-group date'>
                            <input class="form-control datepicker" id="dob" name="dob" type="text">
                            <span class="input-group-addon">
                               <i class="font-icon font-icon-calend"></i>
                            </span>
                        </div>
                    </fieldset>
                </div>

                <div class="col-sm-6">
                    <fieldset class="form-group">
                        <label class="form-label semibold">Gender</label>
                        <select name="gender" class="select2">
                            <option value="">Select Gender</option>
                            <?php
                            $genders = array(
                                '1' => 'Male', '2' => 'Female'
                            );
                            foreach ($genders as $genderKey => $genderValue) {

                                ?>
                                <option value="<?php echo $genderKey; ?>">
                                    <?php echo $genderValue; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </fieldset>
                </div>

                <div class="col-sm-6">
                    <fieldset class="form-group">
                        <label class="form-label semibold">Time Zone</label>
                        <select name="timezone" class="select2">
                        <?php foreach (tz_list() as $zoneValue) { ?>
							<option value="<?php echo $zoneValue['zone']; ?>">
								<?php echo $zoneValue['zone']; ?></option>
						<?php } ?>
                        </select>
                    </fieldset>
                </div>

                <div class="col-sm-6">
                    <fieldset class="form-group">
                        <label class="form-label semibold">Cell Phone</label>
                        <input type="text" class="form-control" name="phone-mobile">
                    </fieldset>
                </div>

            </div>
        </div>
    </section>


    <div class="row">
        <div class="col-md-2 col-md-offset-5">
            <button type="button" class="btn btn-inline btn-success" id="button-add-member">Add new family member</button>
<!--            <input type="submit" class="btn btn-inline btn-success" value="Create">-->
        </div>
    </div>

</form>


<script>

    $(document).ready(function() {

        $("#button-add-member").on('click', function () {
            $(this).removeAttr("id");
            $(this).attr('type', 'submit');
            $(this).parent().removeClass('col-md-2').addClass('col-md-4').append('<button type="button" class="btn btn-inline btn-primary" id="button-cancle-member">Cancle</button>');
            $("#row-add-member").show();

            return false;
        });

        $(document).on('click', '#button-cancle-member', function(){
            $(this).remove();
            $(this).parent().removeClass('col-md-4').addClass('col-md-2').children('button[type="submit"]').attr('id', 'button-add-member').attr('type', 'button');
            $("#row-add-member").hide();
            return false;
        });

        $('.delete_family_member').click(function(){
            var self = this;
            var userID = $(self).data('userid');
            var str = '<?php echo base_url(); ?>patient/removeFamilyMember';
            $.ajax({
                type: "POST",
                url: str,
                data: {
                    id: userID
                },
//                dataType: "text",
                success: function () {
                    $(self).parent().remove();
                    notySuccess('Removed');
                },
                error: function() {
                    notyError("Error");
                }
            });
            return false;
        });


        var $form = $('#form-add-family');
        $form.submit(function(e) {
            //e.preventDefault();

            $form.find('.form-group').removeClass('has-danger');

            var $firstName = $form.find('[name=fname]');
            var $lastName = $form.find('[name=lname]');
            var $email = $form.find('[name=email]');
            var $confirmEmail = $form.find('[name=confirmEmail]');

            if ($firstName.val() == "") {
                $firstName.parent().addClass('has-danger');
                notyError('First name is empty');

                return false;
            }

            if ($lastName.val() == "") {
                $lastName.parent().addClass('has-danger');
                notyError('Last name is empty');

                return false;
            }

            if ($email.val() == "") {
                $email.parent().addClass('has-danger');
                notyError('Email is empty');

                return false;
            }
        });
    });
</script>