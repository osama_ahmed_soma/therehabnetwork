<?php /** @var $user */?>
<?php /** @var $states */?>
<?php /** @var $zones */?>
<?php /** @var $card */?>
<?php /** @var array $familyMembers */?>
<?php /** @var $stripePublishableKey */?>
<?php /** @var CI_Loader $this */?>
<div class="page-content page-patient-update_profile">
    <div class="container-fluid">
    <section class="navi-top clearfix">
			<div class="navi-heading">
				<h2>Profile Update</h2>
            </div>
        </section>
    
        <ul class="nav nav-tabs hidden">
            <li class="nav-item"><a data-toggle="tab" class="nav-link active" href="#main">Profile</a></li>
<!--            <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#billing">Billing Information</a></li>-->
<!--            <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#family">Family members</a></li>-->
<!--            <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#">Login & password</a></li>-->
        </ul>
        <script type="text/javascript">
            $(document).ready(function () {
                var hash = window.location.hash;
                hash && $('ul.nav-tabs a[href="' + hash + '"]').tab('show');
                $('[data-toggle="tooltip"]').tooltip()
            });
        </script>

        <div class="tab-content">
            <div id="main" class="tab-pane fade in active">
                <?php $this->view('profile/_account_details', [
                    'user' => $user,
                    'states' => $states,
                    'zones' => $zones,
                    'countries' => $countries
                ]); ?>
            </div>

<!--            <div id="billing" class="tab-pane fade in">-->
<!--                --><?php //$this->view('profile/_billing', [
//                    'card' => $card,
//                    'stripePublishableKey' => $stripePublishableKey,
//                ]); ?>
<!--            </div>-->
<!---->
<!--            <div id="family" class="tab-pane fade in">-->
<!--                --><?php //$this->view('profile/_add_family', [
//                    'familyMembers' => $familyMembers,
//                ]); ?>
<!--            </div>-->
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="image_cropper_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="border-radius: 0px;">
            <div class="modal-body" style="padding: 0px; overflow: hidden;">
                <div class="" id="pending_appoint_for_patient2"></div>
                <img id="my_custom_cropper_image_element" style="max-width: 100%; height: 500px;">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary " onclick="save_cropped_image()">Save changes</button>
            </div>
        </div>
    </div>
</div>
