<?php /*echo "<pre>"; print_r($user); die();*/ ?>
<form method="post" enctype="multipart/form-data" accept-charset="utf-8" id="form-account-detail">
    <section class="card">
        <div class="card-block">
            <div class="row">
                <div class="col-md-3 profile-update-sc">
                    <div class="row">
                        <div class="col-md-12" style="min-height: 0;">
                            <h5>Upload Your Image</h5>
                        </div>
                        <div class="col-md-12">
                            <img src="<?php echo default_image($user->image_url); ?>" width="90%"
                                 class="preview" id="preview_patient">
                        </div>
                        <div class="col-md-12">
                            <label class="btn btn-block btn-primary btn-file">
                                <span><i class="fa fa-file"></i></span> Replace Image
                                <input id="imgInpPatient" onchange="loadFilePatient(event, this);"
                                       type="file" class="btn btn-primary btn-block"
                                       name="patient_image">
                                <input type="hidden" id="patient_image_db" name="patient_image_db"
                                       value="<?php echo $user->image_url; ?>">
                            </label>
                        </div>

                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                <label class="form-label semibold">First Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="fname" value="<?php echo $user->firstname; ?>" required>
                            </fieldset>
                        </div>
                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Last Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="lname" placeholder="Last Name" value="<?php echo $user->lastname; ?>" required>
                            </fieldset>
                        </div>
                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Email <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="email" value="<?php echo $user->email; ?>" required readonly>
                            </fieldset>
                        </div>
                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Address</label>
                                <input type="text" class="form-control" name="address" value="<?php echo $user->address; ?>">
                            </fieldset>
                        </div>

                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                <label class="form-label semibold">City <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="city" value="<?php echo $user->city; ?>" required>
                            </fieldset>
                        </div>
                        <div class="col-sm-6">
                            <fieldset class="form-group states_section"></fieldset>
                        </div>
                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Zip</label>
                                <input type="text" class="form-control" name="zip" value="<?php echo $user->zip; ?>" >
                            </fieldset>
                        </div>
                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Country <span class="text-danger">*</span></label>
                                <select name="country" class="select2" onchange="checkCountry_showState()" required>
                                    <option value="">Select Country</option>
                                    <?php foreach ($countries as $index => $country) { ?>
                                        <option value="<?php echo $country->full_name; ?>" <?php echo (($user->country) ? (($user->country == $country->full_name) ? 'selected' : '') : (($country->full_name == 'United States') ? 'selected' : '')); ?>>
                                            <?php echo $country->full_name; ?></option>
                                    <?php } ?>
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Date of Birth</label>

                                <div class='input-group date'>
                                    <input class="form-control datepicker" id="dob" name="dob" value="<?php echo $user->dob; ?>"
                                           type="text">
                                    <span class="input-group-addon" onclick="focus_dob()">
										<script>
																<?php
                                                                if (empty($user->dob)) {
                                                                    echo 'empty_it = true';
                                                                } else {
                                                                    ?>
                                                                $('.datepicker').val('<?php echo $user->dob; ?>');
                                                                <?php
                                                                }
                                                                ?>
															</script>
                                               <i class="font-icon font-icon-calend"></i>
                                            </span>
                                </div>
                            </fieldset>
                        </div>

                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Gender <span class="text-danger">*</span></label>
                                <select name="gender" class="select2" required>
                                    <option value="">Select Gender</option>
                                    <?php
                                    $genders = array(
                                        '1' => 'Male', '2' => 'Female'
                                    );
                                    foreach ($genders as $genderKey => $genderValue) {

                                        ?>
                                        <option value="<?php echo $genderKey; ?>"
                                            <?php echo $user->gender == $genderKey ? 'selected' : '' ?>>
                                            <?php echo $genderValue; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </fieldset>
                        </div>

                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Time Zone <span class="text-danger">*</span></label>
                                <select name="timezone" class="select2" required>
                                    <option value="">Select Timezone</option>
                                    <?php foreach (tz_list() as $zoneValue) { ?>
                                        <option value="<?php echo $zoneValue['zone']; ?>" <?php echo ($user->timezone == $zoneValue['zone']) ? 'selected' : '' ?>>
                                            <?php echo $zoneValue['zone_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </fieldset>
                        </div>


                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Cell Phone <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="phone-mobile" value="<?php echo $user->phone_mobile; ?>" placeholder="XXX-XXX-XXXX">
                            </fieldset>
                        </div>
                       
                        <div class="col-sm-6">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Weight <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="number" class="form-control" name="user_weight" value="<?php echo $user->weight; ?>" required>
                                    <span class="input-group-addon">lbs</span>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Height <span class="text-danger">*</span></label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="user_height_ft" value="<?php echo $user->height_ft; ?>" placeholder="Ft" required>
                                            <span class="input-group-addon">ft.</span>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-sm-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">&nbsp;<span class="text-danger">&nbsp;</span></label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="user_height_inch" value="<?php echo $user->height_inch; ?>" placeholder="Inches" required>
                                            <span class="input-group-addon">in.</span>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div class="row">
        <div class="text-right">
            <input type="submit" class="btn btn-inline btn-primary" value="Save">
        </div>
    </div>

</form>

<script>
    function checkCountry_showState(){
        var html;
        if($('select[name="country"]').val() != 'United States'){
            // non u.s states optional
            html = '<label class="form-label semibold">State / Province (optional)</label>' +
                '<input type="text" class="form-control" name="optional_state" value="<?php echo $user->optional_state; ?>">';
        } else {
            html = '<label class="form-label semibold">State <span class="text-danger">*</span></label>' +
                '<select name="state" class="select2" required>' +
                '<option value="">Select States</option>' +
            <?php foreach ($states as $index => $state) { ?>
            '<option value="<?php echo $index; ?>" <?php echo $user->state_id == $index ? 'selected' : '' ?>>' +
                '<?php echo $state; ?></option>'+
                <?php } ?>
                '</select>';
        }
        $('.states_section').html(html);
        $('.select2').select2();
    }
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    function focus_dob(){
        $('input.datepicker[name="dob"]').focus();
    }
    $(document).ready(function() {
        checkCountry_showState();
        var $form = $('#form-account-detail');
        $form.submit(function(e) {
            e.preventDefault();
            if (typeof FormData !== 'undefined'){
                var formData_custom = new FormData( $("#form-account-detail")[0] );
                if(blob_data != '' && image_event_data != ''){
                    formData_custom.append('croppedImage', blob_data, image_event_data.target.files[0].name);
                } else {
                    formData_custom.append('croppedImage', '');
                }
                if(!validateEmail($('[name="email"]').val())){
                    notyError('This email address is not valid email.');
                    $('#image_cropper_modal').modal('hide');
                    return;
                }
                var loading = $('#pending_appoint_for_patient');
                loading.empty();
                loading.addClass('patient_load_modal_loading patient_load_modal').removeClass('view-doctor-profile').show();

                $.ajax({
                    url : "<?php echo base_url(); ?>patient/updateProfile",
                    type : 'POST',
                    data : formData_custom,
                    cache : false,
                    contentType : false,
                    processData : false,
                    success : function(data) {
                        loading.removeClass('patient_load_modal_loading patient_load_modal').addClass('view-doctor-profile');
                        loading.html('');
                        var result = JSON.parse(data);
                        if(result.bool){
                            $('#image_cropper_modal').modal('hide');
                            blob_data = '';
                            image_event_data = '';
                            var fname = $('input[name="fname"]').val();
                            var lname = $('input[name="lname"]').val();
                            $('h3.names').text(fname+' '+lname);
                            $('span.header_user_fname').text(fname);
                            notySuccess(result.message);
                            profile_completion_error();
//                            $.ajax({
//                                type: "POST",
//                                url: '<?php //echo base_url(); ?>//patient/profile_completion_status',
//                                data: {},
//                                dataType: "json",
//                                success: function (response) {
//                                    $('#patient_profile_completion_notification_container').html(response.message);
//                                }
//                            });
                            if(result.isByError){
                                document.getElementById('navigation_profile_image').src = '<?php echo base_url(); ?>'+result.src;
                                document.getElementById('header_profile_image_icon').src = '<?php echo base_url(); ?>'+result.src;
                                document.getElementById('preview_patient').src = '<?php echo base_url(); ?>'+result.src;
                                document.getElementById('patient_image_db').value = result.src;
                            }
                        } else {
                            notyError(result.message);
                            var preview_patient = document.getElementById('preview_patient');
                            preview_patient.src = '<?php echo base_url() . $user->image_url; ?>';
                            setTimeout(function(){
                                location.reload();
                            }, 3000);
                        }
                    },
                    error: function() {
                        loading.removeClass('patient_load_modal_loading patient_load_modal').addClass('view-doctor-profile');
                        loading.html('');
                        notyError("Saving error");
                        $('#image_cropper_modal').modal('hide');
                    }
                });
            } else {
                notyError("Your Browser Don't support FormData API! Use IE 10 or Above!");
                $('#image_cropper_modal').modal('hide');
            }
        });
    });
</script>