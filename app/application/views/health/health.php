<script src="<?php echo base_url() . $script; ?>"></script>
<style>
    .btn-primary {
        font-size: 14px !important;
    }

    .noty_bar.noty_type_success {
        background-color: rgb(137, 222, 143) !important;
    }
    .above_all {
        z-index: 9999;
    }
</style>

<div class="page-content page-my-health"><!-- 'page-my-health' class added-->
    <div class="container-fluid">
        <section class="health-madication-tabs">

            <div class="row">
                <div class="nav-pills">
                    <div class="col-md-2 col-xs-12 dns-icon active"><a id="condition_tab_auto"
                                                                       href="#health-history-custom"
                                                                       data-target="health-history-custom"
                                                                       data-toggle="tab"><i
                                class="fa fa-heartbeat"></i> My Health History</a></div>
                    <div class="col-md-2 col-xs-12 dns-icon"><a id="medication_tab_auto" href="#providers-custom"
                                                                data-target="providers-custom" data-toggle="tab"><i
                                class="fa fa-medkit"></i>
                            My Medications</a></div>
                    <div class="col-md-2 col-xs-12 dns-icon"><a id="allergies_tab_auto" href="#allergies-custom"
                                                                data-target="allergies-custom" data-toggle="tab"><i
                                class="fa fa-stethoscope"></i>
                            My Allergies</a></div>
                    <div class="col-md-2 col-xs-12 dns-icon"><a id="sugeries_tab_auto" href="#records-custom"
                                                                data-target="records-custom" data-toggle="tab"><i
                                class="fa fa-user-md"></i>My Surgeries</a></div>
                </div>
            </div>


            <div class="tab-content">
                <div class="row haelth-section clearfix tab-pane fade in active" role="tabpanel"
                     id="health-history-custom">
                    <div class="hs-list myhealth">
                        <div class="hs-l-main">
                            <div class="hs-l-icon"><i class="fa fa-heartbeat"></i></div>
                            <div class="hs-l-heading">My Health Conditions</div>
                            <div class="hs-l-questions quesiton_my_conditoin">
                                Do you have any health conditions?
                                <?php
                                $id = $this->session->userdata('userid');
                                ?>
                            </div>
                            <div class="hs-l-questions-checkbox">
                                <div class="btn-group" data-toggle="buttons">
                                    <label data-action_on="health" data-value="2" class="btn btn-default myhealth_action_btn <?php echo (($get_patient->is_health == 2) ? 'active' : ''); ?>" id="healthconditions-yes">Yes
                                        <input type="radio" name="option_health" id="Yes" autocomplete="off" <?php echo (($get_patient->is_health == 2) ? 'checked' : ''); ?> >
                                    </label>

                                    <label data-action_on="health" data-value="1" class="btn btn-default myhealth_action_btn <?php echo (($get_patient->is_health == 1) ? 'active' : ''); ?>" id="healthconditions-no">No
                                        <input type="radio" name="option_health" id="No" autocomplete="off" <?php echo (($get_patient->is_health == 1) ? 'checked' : ''); ?>>
                                    </label>
                                </div>
                            </div>
                        </div>
                            <form id="my_condition" class="health_data_section <?php echo (($get_patient->is_health) ? (($get_patient->is_health == 1) ? 'hidden' : '') : 'hidden'); ?>" name="my_health_condition" method="post" onsubmit="return false;">
                                <div class="hs-l-questions-txt desc" id="healthconditions-div">
                                    <div class="more-info select-yes">
                                        <div class="row table-container my-conditions my">
                                            <div class="table" id="health_conditions_table">
                                                <div class="table-row tbl-header">
                                                    <div class="table-cell" style="width:50%;">My Conditions</div>
                                                    <div class="table-cell" style="width:20%;">Reported Date</div>
                                                    <div class="table-cell" style="width:10%;text-align:center;">Action
                                                    </div>
                                                </div>
                                                <?php

                                                if (count($health) > 0 && is_array($health)) {
                                                    $i = 0;
                                                    foreach ($health as $conditions):

                                                        ?>
                                                        <div class="table-row no-records no-records-delete"
                                                             data-to-delete="<?php echo $conditions->id; ?>">
                                                            <div
                                                                class="table-cell condition_term_text"><?php echo $conditions->condition_term; ?></div>
                                                            <div
                                                                class="table-cell"><?php echo $conditions->date; ?></div>
                                                            <div class="table-cell text-center">
                                                                <a class="edit_health_btn"
                                                                   data-toggle="modal" data-target="#health_edit_modal"
                                                                   data-condition_term="<?php echo $conditions->condition_term; ?>"
                                                                   data-id="<?php echo $conditions->id; ?>">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>
                                                                <a class="deleteMessage" data-id="<?php echo $id; ?>"
                                                                   data-rowid="<?php echo $conditions->id; ?>"
                                                                   data-title="<?php echo $conditions->condition_term; ?>"
                                                                   title="Delete <?php echo $conditions->condition_term; ?>"><i
                                                                        class="fa fa-trash-o"></i></a>
                                                            </div>
                                                        </div>

                                                        <?php
                                                        $i++;
                                                    endforeach;
                                                } else {
                                                    ?>
                                                    <div class="table-row no-records no-records-delete no_data_box_health">
                                                        <div class="table-cell">No medical conditions reported</div>
                                                        <div class="table-cell"></div>
                                                        <div class="table-cell delete" style="cursor:pointer"></div>

                                                    </div>

                                                    <?php
                                                }
                                                ?>

                                            </div>
                                        </div>
                                        <div class="add-condition">

                                            <div class="clearfix">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input class="form-control input healthConditionDesc condition_term"
                                                               autocomplete="off" id="healthConditionDesc"
                                                               name="healthConditionDesc"
                                                               placeholder="Condition"
                                                               type="text">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <button type="button" id="add_health_condition"
                                                                class="btn btn-info btn-block pull-right add_condition">Add
                                                            Condition
                                                        </button>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                            <div class="message health_message" id="my_condition_error"></div>
                                        </div>
                                    </div>


                                </div>
                                <input type="hidden" class="condition_type"
                                       value="health"/>
                            </form>
                    </div>
                    <div class="modal fade" id="health_edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form class="edit_health_form">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Edit My Health Conditions</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input class="form-control edit_condition_term" placeholder="Edit Condition" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" class="edit_health_id" value="" />
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row haelth-section clearfix tab-pane fade " role="tabpanel" id="providers-custom">
                    <div class="hs-list myhealth">
                        <div class="hs-l-main">
                            <div class="hs-l-icon"><i class="fa fa-medkit"></i></div>
                            <div class="hs-l-heading">My Medications</div>
                            <div class="hs-l-questions">Are you currently taking any medication?</div>
                            <div class="hs-l-questions-checkbox">
                                <div class="btn-group" data-toggle="buttons">
                                    <label data-action_on="medication" data-value="2" class="btn btn-default myhealth_action_btn <?php echo (($get_patient->is_medication == 2) ? 'active' : ''); ?>" id="healthconditions-yes">Yes
                                        <input type="radio" name="option_medication" id="Yes" autocomplete="off" <?php echo (($get_patient->is_medication == 2) ? 'checked' : ''); ?> >
                                    </label>

                                    <label data-action_on="medication" data-value="1" class="btn btn-default myhealth_action_btn <?php echo (($get_patient->is_medication == 1) ? 'active' : ''); ?>" id="healthconditions-no">No
                                        <input type="radio" name="option_medication" id="No" autocomplete="off" <?php echo (($get_patient->is_medication == 1) ? 'checked' : ''); ?>>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="medication_data_section hs-l-questions-txt <?php echo (($get_patient->is_medication) ? (($get_patient->is_medication == 1) ? 'hidden' : '') : 'hidden'); ?>" id="medications-div">
                            <div class="more-info select-yes">
                                <div class="row table-container my-conditions my">
                                    <div class="table" id="health_conditions_table_medication">
                                        <div class="table-row tbl-header">
                                            <div class="table-cell">My Medications</div>
                                            <div class="table-cell">Special Offers</div>
                                            <div class="table-cell">Quantity</div>
                                            <div class="table-cell">Frequency</div>
<!--                                            <div class="table-cell">Reported Date</div>-->
                                            <div class="table-cell">Action</div>
                                        </div>
                                        <?php
                                        if (count($medication) > 0 && is_array($medication)): ?>
                                            <?php foreach ($medication as $conditions): ?>
                                                <div class="table-row no-records no-records-delete"
                                                     data-to-delete="<?php echo $conditions->id; ?>">
                                                    <div
                                                        class="table-cell medication_text"><?php echo $conditions->condition_term; ?></div>
                                                    <div class="table-cell">
                                                        <a class="btn btn-success card_modal_btn"
                                                           data-drug_ndc="<?php echo $conditions->ndc; ?>"
                                                           data-drug_qty="<?php echo $conditions->dosage; ?>"
                                                           data-drug_freq="<?php echo $conditions->frequency; ?>"
                                                           data-drug_time="<?php echo $conditions->time; ?>"
                                                           data-target_c="#card-discount">
                                                            <i class="fa fa-users"></i> <span>View Available Deals<span>
                                                        </a>
                                                    </div>
                                                    <div class="table-cell dosage_text"><?php echo $conditions->dosage; ?></div>
                                                    <div
                                                        class="table-cell frequency_time_text"><?php echo $conditions->frequency . '-' . $conditions->time; ?></div>
<!--                                                    <div class="table-cell">--><?php //echo $conditions->date; ?><!--</div>-->
                                                    <div class="table-cell text-center">
                                                        <a class="edit_medication_btn"
                                                           data-toggle="modal" data-target="#medication_edit_modal"
                                                           data-medication="<?php echo $conditions->condition_term; ?>"
                                                           data-dosage="<?php echo $conditions->dosage; ?>"
                                                           data-frequency="<?php echo $conditions->frequency; ?>"
                                                           data-time="<?php echo $conditions->time; ?>"
                                                           data-ndc="<?php echo $conditions->ndc; ?>"
                                                           data-id="<?php echo $conditions->id; ?>">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <a class="deleteMessage" data-id="<?php echo $id; ?>"
                                                           data-rowid="<?php echo $conditions->id; ?>"
                                                           data-title="<?php echo $conditions->condition_term; ?>"
                                                           title="Delete <?php echo $conditions->condition_term; ?>"><i
                                                                class="fa fa-trash-o"></i>
                                                        </a>
                                                    </div>
                                                </div>

                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <div class="table-row no-records no-records-delete no_data_box_medication"
                                                 id="temp_loading">
                                                <div class="table-cell">No medications reported</div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell delete" style="cursor:pointer"></div>

                                            </div>
                                        <?php endif; ?>

                                    </div>
                                </div>
                                <div class="add-condition">
                                    <div class="clearfix">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?php
                                                $array = [];

                                                foreach ($drugs as $abc) {
                                                    $array[] = $abc->BrandName;
                                                }
                                                ?>

                                                <input type="text" id="condition_term_medication" placeholder="Medication"
                                                       class="typeahead tt-query form-control input condition_term_medication"
                                                       autocomplete="off" spellcheck="false">
                                                <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
                                                <script type="text/javascript">
                                                    var recieved_drug_data = {};
                                                    $(document).ready(function () {
                                                        $("#condition_term_medication").autocomplete({
                                                            source: function (request, response) {
                                                                show_loader = true;
                                                                $.ajax({
                                                                    url: '<?php echo base_url(); ?>patient/getMedicine_api/' + request.term,
                                                                    dataType: 'json',
                                                                    type: 'GET',
                                                                    success: function (data) {
                                                                        show_loader = false;
                                                                        if(data.names.length == 0){
                                                                            notyError("No Medication Found.");
                                                                            recieved_drug_data = {};
                                                                            $('#condition_term_medication').val('');
                                                                            response([]);
                                                                            return;
                                                                        }
                                                                        recieved_drug_data = data.full_data;
                                                                        response(data.names);
                                                                    }
                                                                });
                                                            },
                                                            minLength: 3,
                                                            select: function (event, ui) {
//                                                                log("Selected: " + ui.item.value + " aka " + ui.item.id);
                                                            }
                                                        });
                                                    });
                                                </script>
                                            </div>
                                            <div class="col-md-6">
                                                <input class="form-control input condition_dosage_medication"
                                                       placeholder="Quantity (1 Pill)"
                                                       type="text">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <select name="condition_freq_medication"
                                                        class="form-control input select2 condition_freq_medication">
                                                    <option value="">Frequency</option>
                                                    <option value="Once">Once</option>
                                                    <option value="Twice">Twice</option>
                                                    <option value="Three times">Three times</option>
                                                    <option value="Four times">Four times</option>
                                                    <option value="Five times">Five times</option>
                                                    <option value="Six times">Six times</option>
                                                    <option value="As Needed">As Needed</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <select name="condition_time_medication"
                                                        class="form-control input select2 condition_time_medication">
                                                    <option value="">Time</option>
<!--                                                    <option value="Hourly">Hourly</option>-->
                                                    <option value="Daily">Daily</option>
                                                    <option value="Weekly">Weekly</option>
                                                    <option value="Monthly">Monthly</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="hidden" class="condition_type_medication"
                                                       value="medication">
                                                <button type="button"
                                                        class="btn btn-info btn-block pull-right add_condition_medication">
                                                    Add Medication
                                                </button>
                                            </div>
                                        </div>


                                        <br>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                    <div class="modal fade" id="medication_edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form class="edit_medication_form">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Edit My Medications</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6" style="margin-bottom: 10px;">
                                                <input type="text" placeholder="Edit Medication" class="typeahead form-control edit_condition_term_medication">
                                                <script>
                                                    var edit_recieved_drug_data = {};
                                                    $(document).ready(function(){
                                                        $('.edit_condition_term_medication').autocomplete({
                                                            source: function (request, response) {
                                                                show_loader = true;
                                                                $.ajax({
                                                                    url: '<?php echo base_url(); ?>patient/getMedicine_api/' + request.term,
                                                                    dataType: 'json',
                                                                    type: 'GET',
                                                                    success: function (data) {
                                                                        show_loader = false;
                                                                        if(data.names.length == 0){
                                                                            notyError("No Medication Found.");
                                                                            edit_recieved_drug_data = {};
                                                                            $('#edit_condition_term_medication').val('');
                                                                            response([]);
                                                                            return;
                                                                        }
                                                                        edit_recieved_drug_data = data.full_data;
                                                                        response(data.names);
                                                                    }
                                                                });
                                                            },
                                                            minLength: 3,
                                                            select: function (event, ui) {
//                                                                log("Selected: " + ui.item.value + " aka " + ui.item.id);
                                                            }
                                                        });
                                                    });
                                                </script>
                                            </div>
                                            <div class="col-md-6" style="margin-bottom: 10px;">
                                                <input type="text" placeholder="Edit Quantity (1 Pill)" class="form-control edit_condition_dosage_medication">
                                            </div>
                                            <div class="col-md-6" style="margin-bottom: 10px;">
                                                <select class="form-control select2 edit_condition_freq_medication">
                                                    <option value="">Frequency</option>
                                                    <option value="Once">Once</option>
                                                    <option value="Twice">Twice</option>
                                                    <option value="Three times">Three times</option>
                                                    <option value="Four times">Four times</option>
                                                    <option value="Five times">Five times</option>
                                                    <option value="Six times">Six times</option>
                                                    <option value="As Needed">As Needed</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6" style="margin-bottom: 10px;">
                                                <select class="form-control select2 edit_condition_time_medication">
                                                    <option value="">Time</option>
<!--                                                    <option value="Hourly">Hourly</option>-->
                                                    <option value="Daily">Daily</option>
                                                    <option value="Weekly">Weekly</option>
                                                    <option value="Monthly">Monthly</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" class="edit_medication_id" value="" />
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row haelth-section clearfix tab-pane fade " role="tabpanel" id="allergies-custom">
                    <div class="hs-list myhealth">
                        <div class="hs-l-main">
                            <div class="hs-l-icon"><i class="fa fa-stethoscope"></i></div>
                            <div class="hs-l-heading">My Allergies</div>
                            <div class="hs-l-questions">Do you have any Allergies or Drug Sensitivities?</div>
                            <div class="hs-l-questions-checkbox">
                                <div class="btn-group" data-toggle="buttons">
                                    <label data-action_on="allergies" data-value="2" class="btn btn-default myhealth_action_btn <?php echo (($get_patient->is_allergies == 2) ? 'active' : ''); ?>" id="healthconditions-yes">Yes
                                        <input type="radio" name="option_allergies" id="Yes" autocomplete="off" <?php echo (($get_patient->is_allergies == 2) ? 'checked' : ''); ?> >
                                    </label>

                                    <label data-action_on="allergies" data-value="1" class="btn btn-default myhealth_action_btn <?php echo (($get_patient->is_allergies == 1) ? 'active' : ''); ?>" id="healthconditions-no">No
                                        <input type="radio" name="option_allergies" id="No" autocomplete="off" <?php echo (($get_patient->is_allergies == 1) ? 'checked' : ''); ?>>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="allergies_data_section hs-l-questions-txt <?php echo (($get_patient->is_allergies) ? (($get_patient->is_allergies == 1) ? 'hidden' : '') : 'hidden'); ?>" id="medications-div">
                            <div class="more-info select-yes">
                                <div class="row table-container my-conditions my">
                                    <div class="table" id="health_conditions_table_allergies">
                                        <div class="table-row tbl-header">
                                            <div class="table-cell">My Allergies</div>
                                            <div class="table-cell">Severity</div>
                                            <div class="table-cell">Reaction</div>
                                            <div class="table-cell">Date</div>
                                            <div class="table-cell">Action</div>
                                        </div>
                                        <?php

                                        if (count($allergies) > 0 && is_array($allergies)) {
                                            foreach ($allergies as $conditions):

                                                ?>
                                                <div class="table-row no-records no-records-delete"
                                                     data-to-delete="<?php echo $conditions->id; ?>">
                                                    <div
                                                        class="table-cell allergies_text"><?php echo $conditions->condition_term; ?></div>
                                                    <div class="table-cell severity_text"><?php echo $conditions->severity; ?></div>
                                                    <div class="table-cell reaction_text"><?php echo $conditions->reaction; ?></div>
                                                    <div class="table-cell"><?php echo $conditions->date; ?></div>
                                                    <div class="table-cell text-center">
                                                        <a class="edit_allergies_btn"
                                                           data-toggle="modal" data-target="#allergies_edit_modal"
                                                           data-allergies="<?php echo $conditions->condition_term; ?>"
                                                           data-severity="<?php echo $conditions->severity; ?>"
                                                           data-reaction="<?php echo $conditions->reaction; ?>"
                                                           data-id="<?php echo $conditions->id; ?>">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <a class="deleteMessage" data-id="<?php echo $id; ?>"
                                                           data-rowid="<?php echo $conditions->id; ?>"
                                                           data-title="<?php echo $conditions->condition_term; ?>"
                                                           title="Delete <?php echo $conditions->condition_term; ?>"><i
                                                                class="fa fa-trash-o"></i></a>
                                                    </div>
                                                </div>

                                                <?php
                                            endforeach;
                                        } else {
                                            ?>
                                            <div class="table-row no-records no-records-delete no_data_box_allergies"
                                                 id="temp_loading">
                                                <div class="table-cell">No allergies reported</div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell delete" style="cursor:pointer"></div>

                                            </div>

                                            <?php
                                        }
                                        ?>

                                    </div>
                                </div>
                                <div class="add-condition">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input class="form-control input condition_term_allergies"
                                                   placeholder="Allergy"
                                                   type="text">
                                        </div>
                                        <div class="col-md-3">
                                            <select name="condition_severity_allergies"
                                                    class="select2 condition_severity_allergies">
                                                <option value="Severity">Severity</option>
                                                <option value="Unspecified">Not Sure</option>
                                                <option value="Mild">Mild</option>
                                                <option value="Moderate">Moderate</option>
                                                <option value="Severe">Severe</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <input class="form-control input condition_reaction_allergies"
                                                   placeholder="Reaction"
                                                   type="text">
                                        </div>
                                        <div class="col-md-3">
                                            <input type="hidden" class="condition_type_allergies" value="allergies">
                                            <button type="button"
                                                    class="btn btn-info btn-block pull-right add_condition_allergies">
                                                Add Allergy
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                    <div class="modal fade" id="allergies_edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form class="edit_allergies_form">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Edit My Allergies</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input class="form-control edit_condition_term_allergies" placeholder="Edit Allergy" type="text">
                                            </div>
                                            <div class="col-md-4">
                                                <select class="select2 edit_condition_severity_allergies">
                                                    <option value="Severity">Severity</option>
                                                    <option value="Unspecified">Not Sure</option>
                                                    <option value="Mild">Mild</option>
                                                    <option value="Moderate">Moderate</option>
                                                    <option value="Severe">Severe</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <input class="form-control edit_condition_reaction_allergies" placeholder="Edit Reaction" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" class="edit_allergies_id" value="" />
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row haelth-section clearfix tab-pane fade" role="tabpanel" id="records-custom">

                    <div class="hs-list myhealth">
                        <div class="hs-l-main">
                            <div class="hs-l-icon"><i class="fa fa-user-md"></i></div>
                            <div class="hs-l-heading">My Surgeries and Procedures</div>
                            <div class="hs-l-questions">Have you ever had any surgeries or medical procedures?</div>
                            <div class="hs-l-questions-checkbox">
                                <div class="btn-group" data-toggle="buttons">
                                    <label data-action_on="surgeries" data-value="2" class="btn btn-default myhealth_action_btn <?php echo (($get_patient->is_surgeries == 2) ? 'active' : ''); ?>" id="healthconditions-yes">Yes
                                        <input type="radio" name="option_surgeries_procedures" id="Yes" autocomplete="off" <?php echo (($get_patient->is_surgeries == 2) ? 'checked' : ''); ?> >
                                    </label>

                                    <label data-action_on="surgeries" data-value="1" class="btn btn-default myhealth_action_btn <?php echo (($get_patient->is_surgeries == 1) ? 'active' : ''); ?>" id="healthconditions-no">No
                                        <input type="radio" name="option_surgeries_procedures" id="No" autocomplete="off" <?php echo (($get_patient->is_surgeries == 1) ? 'checked' : ''); ?>>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="surgeries_data_section hs-l-questions-txt <?php echo (($get_patient->is_surgeries) ? (($get_patient->is_surgeries == 1) ? 'hidden' : '') : 'hidden'); ?>" id="allergies-div">
                            <div class="more-info select-yes">
                                <div class="row table-container my-conditions my">
                                    <div class="table" id="health_conditions_table_sugery">
                                        <div class="table-row tbl-header">
                                            <div class="table-cell">My Surgeries</div>
                                            <div class="table-cell">Year</div>
                                            <div class="table-cell">Reported Date</div>
                                            <div class="table-cell">Action</div>
                                        </div>

                                        <?php

                                        if (count($surgery) > 0 && is_array($surgery)) {
                                            foreach ($surgery as $conditions):

                                                ?>
                                                <div class="table-row no-records no-records-delete"
                                                     data-to-delete="<?php echo $conditions->id; ?>">
                                                    <div
                                                        class="table-cell surgery_text"><?php echo $conditions->condition_term; ?></div>
                                                    <div class="table-cell year_text"><?php echo $conditions->source; ?></div>
                                                    <div class="table-cell"><?php echo $conditions->date; ?></div>
                                                    <div class="table-cell text-center">
                                                        <a class="edit_surgery_btn"
                                                           data-toggle="modal" data-target="#surgery_edit_modal"
                                                           data-surgery="<?php echo $conditions->condition_term; ?>"
                                                           data-year="<?php echo $conditions->source; ?>"
                                                           data-id="<?php echo $conditions->id; ?>">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <a class="deleteMessage" data-id="<?php echo $id; ?>"
                                                           data-rowid="<?php echo $conditions->id; ?>"
                                                           data-title="<?php echo $conditions->condition_term; ?>"
                                                           title="Delete <?php echo $conditions->condition_term; ?>"><i
                                                                class="fa fa-trash-o"></i></a>
                                                    </div>
                                                </div>

                                                <?php
                                            endforeach;
                                        } else {
                                            ?>
                                            <div class="table-row no-records no-records-delete no_data_box_surgeries"
                                                 id="temp_loading">
                                                <div class="table-cell">No Surgeries and Procedures reported</div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell"></div>
                                                <div class="table-cell delete" style="cursor:pointer"></div>

                                            </div>

                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="add-condition">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input class="form-control input condition_term_surgery"
                                                   placeholder="Surgery or Procedure"
                                                   type="text">
                                        </div>
                                        <div class="col-md-4">
                                            <select name="condition_severity_allergies"
                                                    class="select2 condition_year_surgery">
                                                <option value="">Year</option>
                                                <?php
                                                $firstYear = (int)date('Y') - 15;
                                                $lastYear = $firstYear + 20;
                                                for ($i = $firstYear; $i <= $lastYear; $i++) {
                                                    echo '<option value=' . $i . '>' . $i . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="hidden" class="condition_type_surgery" value="surgery">
                                            <button type="button"
                                                    class="btn btn-info btn-block pull-right add_condition_surgery">Add
                                                Procedure
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>


                    </div>
                    <div class="modal fade" id="surgery_edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form class="edit_surgery_form">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Edit My Surgeries and Procedures</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input class="form-control edit_condition_term_surgery" placeholder="Edit Surgery or Procedure" type="text">
                                            </div>
                                            <div class="col-md-6">
                                                <select class="select2 edit_condition_year_surgery">
                                                    <option value="">Year</option>
                                                    <?php
                                                    $firstYear = (int)date('Y') - 15;
                                                    $lastYear = $firstYear + 20;
                                                    for ($i = $firstYear; $i <= $lastYear; $i++) {
                                                        echo '<option value=' . $i . '>' . $i . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" class="edit_surgery_id" value="" />
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.nav-pills div a').on('click custom_click', function () {
            $('.nav-pills div.active').removeClass('active');
            $(this).parent('div').addClass('active');
            $('div.tab-pane').removeClass('active in');
            $('#' + $(this).attr('data-target')).addClass('active in');
        });
        <?php if($tab != 'condition'): ?>
        $('.nav-pills div a#<?=$tab;?>_tab_auto').triggerHandler("custom_click");
        <?php endif; ?>

        $(document).on('click', '.edit_health_btn', function(){
            var self = $(this);
            var condition_term = self.attr('data-condition_term');
            var id = self.attr('data-id');
            $('.edit_condition_term').val(condition_term);
            $('.edit_health_id').val(id);
        });
        
        $('.edit_health_form').submit(function(e){
            e.preventDefault();
            var request = {};
            request.condition_term = $('.edit_condition_term').val();
            request.id = $('.edit_health_id').val();
            if (request.condition_term == '' || request.id == '') {
                notyError("You have to fill all fields");
                return;
            }
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>patient/edit_health_condition',
                data: request,
                dataType: "json",
                success: function (data) {
                    if(!data.bool){
                        notyError(data.message);
                        return;
                    }
                    $('#health_edit_modal').modal('hide');
                    notySuccess(data.message);
                    $('[data-to-delete="'+request.id+'"] .condition_term_text').text(request.condition_term);
                    $('[data-to-delete="'+request.id+'"] .edit_health_btn').attr('data-condition_term', request.condition_term);
                    $('[data-to-delete="'+request.id+'"] .deleteMessage')
                        .attr('data-title', request.condition_term)
                        .attr('title', 'Delete '+request.condition_term);
                },
                error: function() {
                    notyError("Error in request.");
                }
            });
            return false;
        });

        var static_edit_condition_term_medication, static_ndc;
        $(document).on('click', '.edit_medication_btn', function () {
            var self = $(this);
            var medication = self.attr('data-medication');
            static_edit_condition_term_medication = medication;
            var dosage = self.attr('data-dosage');
            var frequency = self.attr('data-frequency');
            var time = self.attr('data-time');
            var ndc = self.attr('data-ndc');
            static_ndc = ndc;
            var id = self.attr('data-id');
            $('.edit_condition_term_medication').val(medication);
            $('.edit_condition_dosage_medication').val(dosage);
            $('.edit_condition_freq_medication').val(frequency);
            $('.edit_condition_time_medication').val(time);
            $('.edit_medication_id').val(id);
            $('.edit_condition_freq_medication').select2({dropdownCssClass: "above_all"});
            $('.edit_condition_time_medication').select2({dropdownCssClass: "above_all"});
        });

        $('.edit_medication_form').submit(function(e){
            e.preventDefault();
            show_loader = true;
            var request = {}, ndc;
//            request.medication = $('.tt-input.edit_condition_term_medication').val();
            request.medication = $('.edit_condition_term_medication').val();
            request.dosage = $('.edit_condition_dosage_medication').val();
            request.frequency = $('.edit_condition_freq_medication').val();
            request.time = $('.edit_condition_time_medication').val();
            request.id = $('.edit_medication_id').val();
            if (request.medication == '' || request.dosage == '' ||  request.frequency == ''  ||  request.time == '' || request.id == '') {
                show_loader = false;
                notyError("You have to fill all fields");
                return;
            }
            if(request.medication != static_edit_condition_term_medication){
                if(!$.isEmptyObject(edit_recieved_drug_data[request.medication])){
                    if(edit_recieved_drug_data[request.medication].NDC == static_ndc){
                        show_loader = false;
                        notyError("You have to select one medicine in dropdown.");
                        $('#medication_edit_modal').modal('hide');
                        return;
                    }
                } else {
                    show_loader = false;
                    notyError("You have to select one medicine in dropdown.");
                    $('#medication_edit_modal').modal('hide');
                    return;
                }
                request.ndc = edit_recieved_drug_data[request.medication].NDC;
            } else {
                request.ndc = static_ndc;
            }
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>patient/edit_medication',
                data: request,
                dataType: "json",
                success: function (data) {
                    show_loader = false;
                    if(!data.bool){
                        notyError(data.message);
                        return;
                    }
                    $('#medication_edit_modal').modal('hide');
                    notySuccess(data.message);
                    $('[data-to-delete="'+request.id+'"] .medication_text').text(request.medication);
                    $('[data-to-delete="'+request.id+'"] .card_modal_btn')
                        .attr('data-drug_ndc', edit_recieved_drug_data[request.medication].NDC)
                        .attr('data-drug_qty', request.dosage)
                        .attr('data-drug_freq', request.frequency)
                        .attr('data-drug_time', request.time);
//                    $('[data-to-delete="'+request.id+'"] .card_modal_btn').attr('data-drug_name', request.medication);
                    $('[data-to-delete="'+request.id+'"] .dosage_text').text(request.dosage);
                    $('[data-to-delete="'+request.id+'"] .frequency_time_text').text(request.frequency+'-'+request.time);
                    $('[data-to-delete="'+request.id+'"] .edit_medication_btn')
                        .attr('data-medication', request.medication)
                        .attr('data-dosage', request.dosage)
                        .attr('data-frequency', request.frequency)
                        .attr('data-time', request.time);
                    $('[data-to-delete="'+request.id+'"] .deleteMessage')
                        .attr('data-title', request.medication)
                        .attr('title', 'Delete '+request.medication);
                },
                error: function() {
                    notyError("Error in request.");
                }
            });
            return false;
        });

        $(document).on('click', '.edit_allergies_btn', function(){
            var self = $(this);
            var allergies = self.attr('data-allergies');
            var severity = self.attr('data-severity');
            var reaction = self.attr('data-reaction');
            var id = self.attr('data-id');
            $('.edit_condition_term_allergies').val(allergies);
            $('.edit_condition_severity_allergies').val(severity);
            $('.edit_condition_reaction_allergies').val(reaction);
            $('.edit_allergies_id').val(id);
            $('.edit_condition_severity_allergies').select2({ dropdownCssClass : "above_all" });
        });

        $('.edit_allergies_form').submit(function(e){
            e.preventDefault();
            var request = {};
            request.allergies = $('.edit_condition_term_allergies').val();
            request.severity = $('.edit_condition_severity_allergies').val();
            request.reaction = $('.edit_condition_reaction_allergies').val();
            request.id = $('.edit_allergies_id').val();
            if (request.allergies == '' || request.severity == '' ||  request.reaction == ''  || request.id == '') {
                notyError("You have to fill all fields");
                return;
            }
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>patient/edit_allergies',
                data: request,
                dataType: "json",
                success: function (data) {
                    if(!data.bool){
                        notyError(data.message);
                        return;
                    }
                    $('#allergies_edit_modal').modal('hide');
                    notySuccess(data.message);
                    $('[data-to-delete="'+request.id+'"] .allergies_text').text(request.allergies);
                    $('[data-to-delete="'+request.id+'"] .severity_text').text(request.severity);
                    $('[data-to-delete="'+request.id+'"] .reaction_text').text(request.reaction);
                    $('[data-to-delete="'+request.id+'"] .edit_allergies_btn')
                        .attr('data-allergies', request.allergies)
                        .attr('data-severity', request.severity)
                        .attr('data-reaction', request.reaction);
                    $('[data-to-delete="'+request.id+'"] .deleteMessage')
                        .attr('data-title', request.allergies)
                        .attr('title', 'Delete '+request.allergies);
                },
                error: function() {
                    notyError("Error in request.");
                }
            });
            return false;
        });

        $(document).on('click', '.edit_surgery_btn', function(){
            var self = $(this);
            var surgery = self.attr('data-surgery');
            var year = self.attr('data-year');
            var id = self.attr('data-id');
            $('.edit_condition_term_surgery').val(surgery);
            $('.edit_condition_year_surgery').val(year);
            $('.edit_surgery_id').val(id);
            $('.edit_condition_year_surgery').select2({ dropdownCssClass : "above_all" });
        });

        $('.edit_surgery_form').submit(function(e){
            e.preventDefault();
            var request = {};
            request.surgery = $('.edit_condition_term_surgery').val();
            request.year = $('.edit_condition_year_surgery').val();
            request.id = $('.edit_surgery_id').val();
            if (request.surgery == '' || request.year == '' || request.id == '') {
                notyError("You have to fill all fields");
                return;
            }
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>patient/edit_surgery',
                data: request,
                dataType: "json",
                success: function (data) {
                    if(!data.bool){
                        notyError(data.message);
                        return;
                    }
                    $('#surgery_edit_modal').modal('hide');
                    notySuccess(data.message);
                    $('[data-to-delete="'+request.id+'"] .surgery_text').text(request.surgery);
                    $('[data-to-delete="'+request.id+'"] .year_text').text(request.year);
                    $('[data-to-delete="'+request.id+'"] .edit_surgery_btn')
                        .attr('data-surgery', request.surgery)
                        .attr('data-year', request.year);
                    $('[data-to-delete="'+request.id+'"] .deleteMessage')
                        .attr('data-title', request.surgery)
                        .attr('title', 'Delete '+request.surgery);
                },
                error: function() {
                    notyError("Error in request.");
                }
            });
            return false;
        });

        $('.myhealth_action_btn').click(function(e){
            e.preventDefault();
            var self = this;
            var action_on = $(self).attr('data-action_on');
            var value = $(self).attr('data-value');
            if(value == 1){
//                hide data
                $('.'+action_on+'_data_section').addClass('hidden');
            } else if(value == 2){
//                show data
                $('.'+action_on+'_data_section').removeClass('hidden');
            }
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>patient/myhealth_actions',
                data: {
                    "field": action_on,
                    "value": value
                },
//                dataType: "json",
                success: function (data) {
                    profile_completion_error();
                },
                error: function() {
                    notyError("Error in request.");
                }
            });
        });


    });
</script>