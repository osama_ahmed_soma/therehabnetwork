<?php
function generate_html($array){
    $html = '';
    if (count($array) > 0 && is_array($array)){
        if(count($array) > 5){
            $i = 0;
            $is_show_span = true;
            $is_close_span = false;
            foreach($array as $conditions){
                if($i > 4){
                    if($is_show_span){
                        $is_show_span = false;
                        $is_close_span = true;
                        $html .= '<span class="colspan_show btn-block" data-id="list1" style="display:none;">';
                    }
                    $html .= '<li>'.$conditions->condition_term.'</li>';
                    if($i == (count($array) - 1)){
                        if($is_close_span){
                            $html .= '<li>'.$conditions->condition_term.'</li>';
                        }
                    }
                } else {
                    $html .= '<li>'.$conditions->condition_term.'</li>';
                }
                $i++;
            }
        } else if(count($array) == 5){
            foreach($array as $conditions){
                $html .= '<li>'.$conditions->condition_term.'</li>';
            }
        } else if(count($array) < 5){
            for($i = 0; $i < 5; $i++){
                if($i > (count($array) - 1)){
                    $html .= '<li>&nbsp;</li>';
                } else {
                    $html .= '<li>'.$array[$i]->condition_term.'</li>';
                }
            }
        }
    } else {
        $html .= '<li>&nbsp;</li>
                    <li>&nbsp;</li>
                    <li>None Added</li>
                    <li>&nbsp;</li>
                    <li>&nbsp;</li>';
    }
    return $html;
}
?>

<div class="page-content">
    <div class="container-fluid">
    	<div class="home_welcome">Welcome to My Virtual Doctor <strong><?php echo $this->session->userdata('full_name') ?>!</strong></div>
    	<div class="hs-list myhealth dshboard-health hidden">
            <div class="col-lg-12">
                <div class="col-md-3 col-sm-6">
                    <div class="hs-l-main">
                        <a href="<?php echo site_url("/patient/myhealth/condition"); ?>">
                            <div class="hs-l-icon"><i class="fa fa-heartbeat"></i></div>
                            <div class="hs-l-heading">My Conditions</div>
                            <div class="hs-l-questions quesiton_my_conditoin">
                                Do you have any health conditions?
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="hs-l-main">
                        <a href="<?php echo site_url("/patient/myhealth/medication"); ?>">
                            <div class="hs-l-icon"><i class="fa fa-medkit"></i></div>
                            <div class="hs-l-heading">My Medications</div>
                            <div class="hs-l-questions">Are you currently taking any medication?</div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="hs-l-main">
                        <a href="<?php echo site_url("/patient/myhealth/allergies"); ?>">
                            <div class="hs-l-icon"><i class="fa fa-stethoscope"></i></div>
                            <div class="hs-l-heading">My Allergies</div>
                            <div class="hs-l-questions">Do you have any Allergies or Drug Sensitivities?</div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="hs-l-main">
                        <a href="<?php echo site_url("/patient/myhealth/sugeries"); ?>">
                            <div class="hs-l-icon"><i class="fa fa-user-md"></i></div>
                            <div class="hs-l-heading">My Surgeries</div>
                            <div class="hs-l-questions">Have you ever had any surgeries or medical procedures?</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <section class="card doc_search_home">
            
                <div class="card-block" id="doctor_search">
                    <h5 class="with-border ds-heading-sec"><i class="fa fa-stethoscope"></i> See a Doctor <span>Find a provider and make an appointment. It's that simple.</span>
                    </h5>
                    <div class="clearfix bck-crd">
                        <div class="col-xl-12">
                            <label class="form-label" for="exampleInputEmail1"></label>
                        </div>
                        <div class="col-md-8 ds-feild-section">
                            <form id="form-doctor-search">
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <select name="state_id" class="select2 form-control" id="state">
                                            <option value="">Select a State</option>
                                            <?php foreach ($states as $index => $name) { ?>
                                                <option value="<?php echo $index; ?>"><?php echo $name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <select name="speciality" class="select2 form-control" id="speciality">
                                            <option value="">Select a Specialty</option>
                                            <?php foreach ($specialties as $index => $specialty) { ?>
                                                <option value="<?php echo $specialty; ?>"><?php echo $specialty; ?></option>
                                            <?php } ?>
                                        </select>
                                    </fieldset>
                                    <!--<fieldset class="form-group">
                                        <div class="form-control-wrapper form-control-icon-right">
                                            <input type="text" class="form-control speciality"
                                                   placeholder="Type doctor's speciality" name="speciality"/>
                                        </div>
                                    </fieldset>-->
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <div class="form-control-wrapper form-control-icon-right">
                                            <input type="text" class="form-control doctor_name"
                                                   placeholder="Type doctor's name" name="name"/>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <fieldset class="form-group">
                                        <div class="form-control-wrapper form-control-icon-right">
                                            <input type="submit" class="btn btn-primary doctor_search_button"
                                                   value="Search">
                                        </div>
                                    </fieldset>
                                </div>
                            </form>
                        </div>
                    </div><!--.row-->
                </div>
            </section>
        </div>

        <div class="row" id="doctors_result_by_search" style="display: none;">
            <div class="container-fluid">
                <div class="col-lg-12 dr-search-result">
                    <div class="home_welcome">Search Results for <strong>Doctors</strong></div>
                    <h5 class="with-border"></h5>
                </div>
                <div class="doctors_home_area">

                </div>
            </div>
        </div>
    </div><!--.container-fluid-->
</div><!--.page-content-->
<div class="modal fade" id="invite_doctor_after_search_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="invite_doctor_modal_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Invite Your Doctor</h4>
                    <span class="text-muted">Please let us know a bit about your doctor so we can invite him to join My Virtual Doctor</span>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-sm-6">
                                    <fieldset class="form-group">
                                        <label class="form-label">Doctor Name</label>
                                        <input type="text" class="form-control" name="invite_first_name" value="" placeholder="First Name">
                                    </fieldset>
                                </div>
                                <div class="col-sm-6">
                                    <fieldset class="form-group">
                                        <label class="form-label">&nbsp;</label>
                                        <input type="text" class="form-control" name="invite_last_name" value="" placeholder="Last Name">
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label class="form-label">Specialty</label>
                                <select name="invite_specialty" class="select2" id="specialty">
                                    <option value="">Select a Specialty</option>
                                    <?php foreach ($specialties as $index => $specialty) { ?>
                                        <option value="<?php echo $specialty; ?>"><?php echo $specialty; ?></option>
                                    <?php } ?>
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label class="form-label">State</label>
                                <select name="invite_state_id" class="select2">
                                    <option value="">Select a State</option>
                                    <?php foreach ($states as $index => $name) { ?>
                                        <option value="<?php echo $index; ?>"><?php echo $name; ?></option>
                                    <?php } ?>
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label class="form-label">Email</label>
                                <input type="text" class="form-control" name="invite_email" value="">
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Send Invitiation!</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
        var is_show = true;
		$('.colspan').click(function(){
			if(is_show){ is_show = false; } else { is_show = true; }
            $data = $(this).attr('data-att');
            $('.colspan_show[data-id='+$data+']').toggle('slow');
				if(!is_show){	$('.colspan').addClass('down').removeClass('up');}else{	$('.colspan').addClass('up').removeClass('down');}
					
			
        });
    </script>
<script>

    $(document).ready(function () {
        $('#invite_doctor_modal_form').submit(function(e){
            e.preventDefault();
            var invite_first_name = $('[name="invite_first_name"]').val();
            var invite_last_name = $('[name="invite_last_name"]').val();
            var invite_specialty = $('[name="invite_specialty"]').val();
            var invite_state_id = $('[name="invite_state_id"]').val();
            var invite_email = $('[name="invite_email"]').val();
            if(invite_first_name == '' || invite_last_name == '' || invite_specialty == '' || invite_state_id == '' || invite_email == ''){
                notyError('Fill all fields first.');
                return;
            }
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>patient/invite_doctors',
                data: {
                    first_name: invite_first_name,
                    last_name: invite_last_name,
                    specialty: invite_specialty,
                    state_id: invite_state_id,
                    email: invite_email
                },
                dataType: "json",
                success: function (data) {
                    if(data.boolean){
                        notySuccess(data.message);
                        $('[name="invite_first_name"]').val('');
                        $('[name="invite_last_name"]').val('');
                        $('[name="invite_specialty"]').val('');
                        $('[name="invite_state_id"]').val('');
                        $('[name="invite_email"]').val('');
                        $('#invite_doctor_after_search_modal').modal('hide');
                    } else {
                        notyError(data.message);
                    }
                }
            });
            return false;
        });
        $("#form-doctor-search").submit(function (e) {
            e.preventDefault();
			
            var str = "<?php echo base_url(); ?>doctor_search";
            var speciality = $("#speciality").val();
            var name = $(".doctor_name").val();
            var state = $("#state").val();
            if (speciality == '' && name == '' && state == '') {
                notyError("Select state, speciality or name");
//                $('#doctors_result_by_search').css('display', 'none');
//                $(".doctors_home_area").addClass('doctors_home_area_alert').html("Select state or name");

                return;
            } else {
//                notySuccess('Please wait doctors list are fetching...');
//				$("#form-doctor-search").find('fieldset').hide();
//				$("#form-doctor-search").addClass('patient_load_modal_loading patient_load_modal').show();
//				return false;
            }

            $.ajax({
                type: "GET",
                url: str,
                data: $("#form-doctor-search").serialize(),
                dataType: "text",
                success: function (data) {
                    $('#doctors_result_by_search').css('display', 'block');
//					$("#form-doctor-search").removeClass('patient_load_modal_loading patient_load_modal');
//				$("#form-doctor-search").find('fieldset').show();
                    $(".doctors_home_area").removeClass('doctors_home_area_alert').html(data);
                    $('html, body').animate({
                        scrollTop: $(".doctors_home_area").offset().top
                    }, 1500);
                    $('.colspan_show[data-id="myspecialities"]').hide('slow');
                }
            });
        });
    });
</script>