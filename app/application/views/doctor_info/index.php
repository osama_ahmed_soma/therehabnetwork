<div class="page-content page-doctor-info"><!--added class 'page-doctor-info'-->
    <div class="container-fluid">
        <section class="navi-top clearfix">
            <div class="navi-heading">
                <h2>Update Profile</h2>
            </div>

        </section>
        <section class="message-tabs clearfix">
            <div class="tabs-ul pull-left">
                <ul>
                    <li class="nav-item">
                        <a class="nav-link active" href="#update_profile_tab" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    General Info
                                </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-tab-1" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    About
                                </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-tab-2" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Specialties
                                </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-tab-3" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Education
                                </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-tab-4" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Awards
                                </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-tab-6" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Licensed State(s)
                                </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#language_spoken_tab" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Languages Spoken
                                </span>
                        </a>
                    </li>
                </ul>
            </div>
        </section>
        <?php /*?><section class="tabs-section">
            <div class="tabs-section-nav tabs-section-nav-icons">
                <div class="tbl">
                    <ul class="nav" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#update_profile_tab" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Update Profile
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-tab-1" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    About me
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-tab-2" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    My Specialties
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-tab-3" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Education
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-tab-4" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Awards
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-tab-6" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Licensed State(s)
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#language_spoken_tab" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Languages Spoken
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div><!--.tabs-section-nav--><?php */ ?>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="update_profile_tab">
                <?php foreach ($doctors as $doctor): ?>
                    <?php
                    echo form_open('doctor/update_doctor_profile', array('id' => 'doctor_profile_update_form'));
                    ?>

                    <section class="card">

                        <!--<center class="text-danger" style="padding: 15px 0px;">
                                <?php /*echo @$error; */ ?>
                            </center>
                            <center><?php /*echo validation_errors(); */ ?>
                                <?php /*echo $this->session->flashdata('message'); */ ?>
                            </center>-->
                        <div class="card-block">
                            <div class="">


                                <div class="col-md-3 profile-update-sc">
                                    <div class="row">
                                        <div class="col-md-12 " style="min-height: 0;">
                                            <h5 class="text-center">Upload Your Image</h5>
                                        </div>
                                        <div class="col-md-12">
                                            <img src="<?php echo default_image($doctor->image_url); ?>" width="100%"
                                                 class="preview" id="preview_patient"/>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="btn btn-primary btn-block btn-file">
                                                <span><i class="fa fa-file"></i></span> Replace Image
                                                <input id="imgInpProfile" onchange="loadFileProfile(event, this);"
                                                       type="file" class="btn btn-primary btn-block"
                                                       name="doctor_image">
                                                <input type="hidden" id="doctor_image_db" name="doctor_image_db"
                                                       value="<?php echo $doctor->image_url; ?>">
                                            </label>
                                        </div>

                                    </div>
                                </div>


                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">First Name <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="fname"
                                                       required=""
                                                       value="<?php echo $doctor->firstname; ?>">

                                            </fieldset>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Last Name <span
                                                        class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="lname"
                                                       placeholder="Last Name"
                                                       required=""
                                                       value="<?php echo $doctor->lastname; ?>">

                                            </fieldset>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Gender <span
                                                        class="text-danger">*</span></label>
                                                <select name="gender" class="select2" required>
                                                    <option value="">Select Gender</option>
                                                    <?php
                                                    $genders = array(
                                                        'Male' => 'Male', 'Female' => 'Female'
                                                    );
                                                    foreach ($genders as $key_gender => $value_gender) {

                                                        ?>
                                                        <option
                                                            value="<?php echo $value_gender; ?>"
                                                            <?php echo $doctor->gender == $value_gender ? 'selected' : '' ?>>
                                                            <?php echo $key_gender; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Date of Birth <span
                                                        class="text-danger">*</span></label>

                                                <div class='input-group date'>
                                                    <input id="daterange_dob"
                                                           class="form-control daterange_picker" name="dob"
                                                           required=""
                                                           value="<?php echo $doctor->dob; ?>"
                                                           type="text" placeholder="Date Of Birth">
                                                    <span class="input-group-addon" onclick="focus_dob()">
															<script>
																<?php
                                                                if (empty($doctor->dob)) {
                                                                    echo 'empty_it = true';
                                                                }
                                                                ?>
															</script>
                                           <i class="font-icon font-icon-calend"></i>
                                        </span>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Time Zone <span
                                                        class="text-danger">*</span></label>
                                                <select name="timezone" class="select2" required>
                                                    <option value="">Select Timezone</option>
                                                    <?php foreach (tz_list() as $zoneValue) { ?>
                                                        <option
                                                            value="<?php echo $zoneValue['zone']; ?>" <?php echo ($doctor->timezone == $zoneValue['zone']) ? 'selected' : '' ?>>
                                                            <?php echo $zoneValue['zone_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <fieldset class="form-group states_section"></fieldset>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">City <span
                                                        class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="city"
                                                       required=""
                                                       value="<?php echo $doctor->city; ?>"
                                                       placeholder="City">

                                            </fieldset>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Zip <span
                                                        class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="zip"
                                                       required=""
                                                       value="<?php echo $doctor->zip; ?>"
                                                       placeholder="Zip">

                                            </fieldset>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Country <span
                                                        class="text-danger">*</span></label>
                                                <select name="country" class="select2"
                                                        onchange="checkCountry_showState()" required>
                                                    <option value="">Select Country</option>
                                                    <?php foreach ($countries as $index => $country) { ?>
                                                        <option
                                                            value="<?php echo $country->full_name; ?>" <?php echo(($doctor->country) ? (($doctor->country == $country->full_name) ? 'selected' : '') : (($country->full_name == 'United States') ? 'selected' : '')); ?>>
                                                            <?php echo $country->full_name; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Phone-Office</label>
                                                <input type="text" class="form-control" name="phone-office"
                                                       value="<?php echo $doctor->phone_home; ?>"
                                                       placeholder="Phone-Office">

                                            </fieldset>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Phone-Mobile <span
                                                        class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="phone-mobile"
                                                       required=""
                                                       value="<?php echo $doctor->phone_mobile; ?>"
                                                       placeholder="XXX-XXX-XXXX">

                                            </fieldset>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">NPI Number <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="m_license"
                                                       required=""
                                                       value="<?php echo $doctor->npi; ?>"
                                                       placeholder="NPI Number">

                                            </fieldset>
                                        </div>


                                    </div>
                                </div>
                            </div>


                        </div>
                    </section>

                    <section class="card">
                        <div class="card-block">

                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Pre-Medical Education</label>
                                        <input type="text" class="form-control" name="pre_education"
                                               value="<?php echo $doctor->pre_medical_education; ?>"
                                               placeholder="Pre-Medical Education">

                                    </fieldset>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Medical School</label>
                                        <input type="text" class="form-control" name="medical_school"
                                               value="<?php echo $doctor->medical_school; ?>"
                                               placeholder="Medical School">

                                    </fieldset>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Residency</label>
                                        <input type="text" class="form-control" name="residency"
                                               value="<?php echo $doctor->residency; ?>"
                                               placeholder="Residency">

                                    </fieldset>
                                </div>
                                <!--<div class="col-lg-3 col-md-3 col-sm-3">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">State License</label>
                                            <input type="text" class="form-control" name="s_license"
                                                   value="<?php /*echo $doctor->state_license; */ ?>"
                                                   placeholder="State License">

                                        </fieldset>
                                    </div>-->
                            </div>

                        </div>
                    </section>
                    <div class="text-right">
                        <input type="hidden" value="1" name="save">
                        <input type="hidden" value="" name="blob_obj">
                        <button type="submit" class="btn btn-inline btn-primary">Save</button>
                        <button type="button" class="btn btn-inline btn-default cancel">Cancel</button>
                    </div>
                    </form>
                    <div class="" id="pending_appoint_for_patient"></div>
                    <script>
                        function focus_dob(){
                            $('input.daterange_picker[name="dob"]').focus();
                        }
                        function checkCountry_showState() {
                            var html;
                            if ($('select[name="country"]').val() != 'United States') {
                                // non u.s states optional
                                html = '<label class="form-label semibold">State / Province (optional)</label>' +
                                    '<input type="text" class="form-control" name="optional_state" value="<?php echo $doctor->optional_state; ?>">';
                            } else {
                                html = '<label class="form-label semibold">State <span class="text-danger">*</span></label>' +
                                    '<select name="state" id="state" class="select2" required>' +
                                    '<option value="">Select States</option>' +
                                    <?php foreach ($states as $key_state => $value_state) { ?>
                                    '<option ' +
                                    'value="<?php echo $key_state; ?>" <?php echo $doctor->state_id == $key_state ? 'selected' : '' ?>>' +
                                    '<?php echo $value_state; ?></option>' +
                                    <?php } ?>
                                    '</select>';
                            }
                            $('.states_section').html(html);
                            $('.select2').select2();
                        }
                        // non-digit characters which are allowed in phone numbers
                        var phoneNumberDelimiters = "()- ";
                        // characters which are allowed in international phone numbers
                        // (a leading + is OK)
                        var validWorldPhoneChars = phoneNumberDelimiters + "+";
                        // Minimum no of digits in an international phone no.
                        var minDigitsInIPhoneNumber = 10;

                        function isInteger(s) {
                            var i;
                            for (i = 0; i < s.length; i++) {
                                // Check that current character is number.
                                var c = s.charAt(i);
                                if (((c < "0") || (c > "9"))) return false;
                            }
                            // All characters are numbers.
                            return true;
                        }
                        function trim(s) {
                            var i;
                            var returnString = "";
                            // Search through string's characters one by one.
                            // If character is not a whitespace, append to returnString.
                            for (i = 0; i < s.length; i++) {
                                // Check that current character isn't whitespace.
                                var c = s.charAt(i);
                                if (c != " ") returnString += c;
                            }
                            return returnString;
                        }
                        function stripCharsInBag(s, bag) {
                            var i;
                            var returnString = "";
                            // Search through string's characters one by one.
                            // If character is not in bag, append to returnString.
                            for (i = 0; i < s.length; i++) {
                                // Check that current character isn't whitespace.
                                var c = s.charAt(i);
                                if (bag.indexOf(c) == -1) returnString += c;
                            }
                            return returnString;
                        }

                        function checkInternationalPhone(strPhone) {
                            var bracket = 3
                            strPhone = trim(strPhone)
                            if (strPhone.indexOf("+") > 1) return false
                            if (strPhone.indexOf("-") != -1)bracket = bracket + 1
                            if (strPhone.indexOf("(") != -1 && strPhone.indexOf("(") > bracket)return false
                            var brchr = strPhone.indexOf("(")
                            if (strPhone.indexOf("(") != -1 && strPhone.charAt(brchr + 2) != ")")return false
                            if (strPhone.indexOf("(") == -1 && strPhone.indexOf(")") != -1)return false
                            s = stripCharsInBag(strPhone, validWorldPhoneChars);
                            return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
                        }

                        function ValidatePhone(Phone) {

                            if ((Phone.value == null) || (Phone.value == "")) {
                                notyError('Please Enter your Phone-Mobile');
                                $('input[name="phone-mobile"]').parent().addClass('has-danger');
                                $('input[name="phone-mobile"]').focus();
                                return false
                            }
                            if (checkInternationalPhone(Phone.value) == false) {
                                notyError("Please Enter a Valid Phone-Mobile");
                                $('input[name="phone-mobile"]').parent().addClass('has-danger');
                                $('input[name="phone-mobile"]').focus();
                                return false
                            }
                            return true
                        }


                        $(document).ready(function () {
                            checkCountry_showState();
                            var $form = $('#doctor_profile_update_form');
                            $form.submit(function (e) {
                                e.preventDefault();
                                $('input[name="phone-mobile"]').parent().removeClass('has-danger');
                                if ($('select[name="country"]').val() == 'United States') {
                                    if (!ValidatePhone({value: $('input[name="phone-mobile"]').val()})) {
                                        $('#image_cropper_modal').modal('hide');
                                        return;
                                    }
                                }
                                var npi_number = $('input[name="m_license"]').val();
                                npi_number = npi_number.replace(/[^\w\s]/gi, '');
                                if(npi_number.length > 10){
                                    notyError("NPI Number should be a 10 digit number");
                                    $('#image_cropper_modal').modal('hide');
                                    return;
                                }
                                if (typeof FormData !== 'undefined') {
                                    var formData = new FormData($("#doctor_profile_update_form")[0]);
                                    if (blob_data != '' && image_event_data != '') {
                                        formData.append('croppedImage', blob_data, image_event_data.target.files[0].name);
                                    } else {
                                        formData.append('croppedImage', '');
                                    }
                                    var loading = $('#pending_appoint_for_patient');
                                    loading.html('');
                                    loading.addClass('patient_load_modal_loading patient_load_modal').removeClass('view-doctor-profile').show();
                                    $.ajax({
                                        url: "<?php echo base_url(); ?>doctor/update_doctor_profile",
                                        type: 'POST',
                                        data: formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success: function (data) {
                                            $('#image_cropper_modal').modal('hide');
                                            loading.removeClass('patient_load_modal_loading patient_load_modal').addClass('view-doctor-profile');
                                            loading.html('');
                                            var result = JSON.parse(data);
                                            if (result.bool) {
                                                blob_data = '';
                                                image_event_data = '';
                                                var fname = $('input[name="fname"]').val();
                                                var lname = $('input[name="lname"]').val();
                                                $('h3.names').text(fname + ' ' + lname);
                                                $('span.header_user_fname').text(fname);
                                                notySuccess(result.message);
                                                profile_completion_error();
//                                                    $.ajax({
//                                                        type: "POST",
//                                                        url: '<?php //echo base_url(); ?>//doctor/profile_completion_status',
//                                                        data: {},
//                                                        dataType: "json",
//                                                        success: function (response) {
//                                                            if(response.boolean){
//                                                                $('.my_custom_error_alert_on_profile_completion').empty();
//                                                            } else {
//                                                                $('.my_custom_error_alert_on_profile_completion').html(response.message);
//                                                            }
//                                                        }
//                                                    });
                                                if (result.isByError) {
                                                    document.getElementById('navigation_profile_image').src = '<?php echo base_url(); ?>' + result.src;
                                                    document.getElementById('header_profile_image_icon').src = '<?php echo base_url(); ?>' + result.src;
                                                    document.getElementById('preview_patient').src = '<?php echo base_url(); ?>' + result.src;
                                                    document.getElementById('doctor_image_db').value = result.src;
                                                }
                                            } else {
                                                notyError(result.message);
//                                                    document.getElementById('doctor_image_db').value = result.src;
                                                var preview_patient = document.getElementById('preview_patient');
                                                preview_patient.src = '<?php echo base_url() . $this->session->userdata('image'); ?>';
                                                setTimeout(function () {
                                                    location.reload();
                                                }, 3000);
                                            }
                                        },
                                        error: function () {
                                            loading.removeClass('patient_load_modal_loading patient_load_modal').addClass('view-doctor-profile');
                                            loading.html('');
                                            notyError("Saving error");
                                            $('#image_cropper_modal').modal('hide');
                                        }
                                    });
                                } else {
                                    notyError("Your Browser Don't support FormData API! Use IE 10 or Above!");
                                    $('#image_cropper_modal').modal('hide');
                                }
                                return false;
                            });
                        });
                    </script>
                <?php endforeach; ?>
            </div><!--.tab-pane-->
            <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-1">
                <div class="card-block"> <?php
                    //print_r($condition);
                    $id = $this->session->userdata('userid');
                    ?>
                    <div class="form-group overview_doctor">
                        <textarea name="doctor_overview" rows="5" class="form-control doctor_overview"
                                  placeholder="Type a message"><?php echo $doctor->about; ?></textarea>
                    </div>
                    <div class="row">
                        <div class="text-right">
                            <button type="button" class="btn btn-primary btn-inline add_doctor_overview">Save</button>
                            <button type="button" class="btn btn-default btn-inline">Cancel</button>
                            <br>
                            <div class="success_overview"></div>
                        </div>
                    </div>
                </div>
            </div><!--.tab-pane-->

            <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-2">
                <section class="upcoming-sessions">
                    <div class="card-block">
                        <div class="condition condition-clear"><!--added class 'condition-clear'-->
                            <div class="row">
                                <table class="table table-hover doctor_specialization_table">
                                    <thead>
                                    <tr>
                                        <th>Specialty</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="doctor_update_profile_specialties">
                                    <?php
                                    $empty_message = 'You haven\'t yet add any specialties!';
                                    if (is_array($specials)) {
                                        foreach ($specials as $special) {
                                            echo '<tr class="specialty_'.$special->id.'">
                                                <td class="specialty">' . $special->speciality . '</td>
                                                <td class="table-photo">
                                                    <a 
                                                        class="tabledit-edit-button btn btn-sm btn-default edit_specialty_btn" 
                                                        data-toggle="modal" data-target="#edit_specialty_modal" 
                                                        data-specialty="'.$special->speciality.'" data-specialty_id="'.$special->id.'" 
                                                        style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;margin-right: 5px;">
                                                        <span class="glyphicon glyphicon-pencil"></span>
                                                    </a>
                                                    <a 
                                                        href="" 
                                                        data-specialtyID="' . $special->id . '" 
                                                        class="tabledit-delete-button btn btn-sm btn-default doctor_update_profile_delete_specialty" 
                                                        style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </a>
                                                </td>
                                            </tr>';
                                        }
                                    } else
                                        echo '<tr id="no_specialty_row">
                                            <td colspan="7" style="text-align: left;">No Specialty Found..</td>
                                        <tr>';
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="condition condition-clear">
                            <div class="row">
                                <div class="text-right">
                                    <button class="add btn btn-primary" id="add-spec">Add</button>
                                </div>
                            </div>
                        </div>

                        <div class="condition condition-form" id="form-add-spec">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold" for="exampleInput">Specialty</label>
                                        <select name="state_id" class="select2 doctor_specialty otherField_onSelect">
                                            <option value="">Select a Specialty</option>
                                            <?php foreach ($specialties as $index => $specialty) { ?>
                                                <option
                                                    value="<?php echo $specialty; ?>"><?php echo $specialty; ?></option>
                                            <?php } ?>
                                            <option value="other">Other</option>
                                        </select>
                                        <br/>
                                        <br/>
                                        <input type="text" style="display: none;"
                                               class="form-control other_field doctor_specialty_other"
                                               placeholder="Other Specialty">
                                    </fieldset>
                                </div>
                                <div class="text-right">

                                    <button type="button" class="btn btn-primary btn-inline add_doctor_speciality">Save
                                    </button>
                                    <button type="button" class="btn btn-primary-outline btn-inline cancel">Cancel
                                    </button>
                                    <div class="success_speciality"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="edit_specialty_modal" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form name="edit_specialty_form">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Edit Specialty</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label class="form-label semibold" for="exampleInput">Specialty</label>
                                                    <select class="select2 edit_doctor_specialty">
                                                        <?php foreach ($specialties as $index => $specialty) { ?>
                                                            <option
                                                                value="<?php echo $specialty; ?>"><?php echo $specialty; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input class="edit_doctor_specialty_id" value=""
                                                   type="hidden">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div><!--.tab-pane-->
                </section>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-3">
                <section class="upcoming-sessions">
                    <div class="card-block">
                        <div class="condition condition-clear"><!--added class 'condition-clear'-->
                            <div class="row">
                                <table class="table table-hover education_table">
                                    <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>University</th>
                                        <th>Year</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="education_listing">
                                    <?php
                                    if (is_array($educations)) {
                                        foreach ($educations as $education) {
                                            echo '<tr class="education_' . $education->id . '">
                                                <td class="title">' . $education->name . '</td>
                                                <td class="university">' . $education->institute . '</td>
                                                <td class="from_and_to_year">' . $education->from_year . ' - ' . $education->to_year . '</td>
                                                <td class="table-photo">
                                                    <a class="tabledit-edit-button btn btn-sm btn-default edit_education_btn" 
                                                        data-toggle="modal" data-target="#edit_education_modal" 
                                                        data-education_id="' . $education->id . '" 
                                                        data-title="' . $education->name . '" data-university="' . $education->institute . '" 
                                                        data-from_year="' . $education->from_year . '" data-to_year="' . $education->to_year . '" 
                                                        style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;margin-right: 5px;">
                                                        <span class="glyphicon glyphicon-pencil"></span>
                                                    </a>
                                                    <a href="" data-educationID="' . $education->id . '" class="tabledit-delete-button btn btn-sm btn-default doctor_update_profile_delete_education" style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </a>
                                                </td>
                                            </tr>';
                                        }
                                    } else {
                                        echo '<tr id="no_education_row">
                                            <td colspan="7" style="text-align: left;">No Education Found..</td>
                                        </tr>';
                                    }
                                    //echo $allergies;
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="condition condition-clear">
                            <div class="row">
                                <div class="text-right">
                                    <button class="add btn btn-primary" id="add-edu">Add</button>
                                </div>
                            </div>
                        </div>

                        <div class="condition condition-form" id="form-add-edu">
                            <div class="row">
                                <div class="col-lg-5 col-md-5">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold" for="exampleInput">Education</label>
                                        <input class="form-control doctor_education_title" placeholder="Title"
                                               value="" type="text">
                                    </fieldset>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold" for="exampleInput">University</label>
                                        <input class="form-control doctor_education_university" placeholder="University"
                                               value="" type="text">
                                    </fieldset>
                                </div>
                                <div class="col-lg-2 col-md-2">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold" for="exampleInput">From</label>
                                        <select class="select2 form-control doctor_education_year_start">
                                            <?php $i = 0;
                                            $year = 1950;
                                            for ($i = 0; $i < 66; $i++) {
                                                $year += 1;
                                                ?>
                                                <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                <?php
                                            }
                                            ?>

                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-lg-2 col-md-2">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold" for="exampleInput">To</label>
                                        <select class="select2 form-control doctor_education_year_end">
                                            <?php $i = 0;
                                            $year = 1950;
                                            for ($i = 0; $i < 66; $i++) {
                                                $year += 1;
                                                ?>
                                                <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                <?php
                                            }
                                            ?>

                                        </select>
                                    </fieldset>
                                </div>
                                <div class="text-right">
                                    <button type="button" class="btn btn-primary btn-inline add_doctor_education">Save
                                    </button>
                                    <button type="button" class="btn btn-default btn-inline cancel">Cancel</button>
                                    <br>
                                    <div class="success_education"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="edit_education_modal" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form name="edit_education_form">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Edit Education</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label class="form-label semibold"
                                                           for="exampleInput">Education</label>
                                                    <input class="form-control edit_doctor_education_title"
                                                           placeholder="Title"
                                                           value="" type="text">
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="form-label semibold"
                                                           for="exampleInput">University</label>
                                                    <input class="form-control edit_doctor_education_university"
                                                           placeholder="University"
                                                           value="" type="text">
                                                </div>
                                                <div class="col-md-2">
                                                    <label class="form-label semibold" for="exampleInput">From</label>
                                                    <select
                                                        class="select2 form-control edit_doctor_education_year_start">
                                                        <?php $i = 0;
                                                        $year = 1950;
                                                        for ($i = 0; $i < 66; $i++) {
                                                            $year += 1;
                                                            ?>
                                                            <option
                                                                value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                            <?php
                                                        }
                                                        ?>

                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <label class="form-label semibold" for="exampleInput">To</label>
                                                    <select class="select2 form-control edit_doctor_education_year_end">
                                                        <?php $i = 0;
                                                        $year = 1950;
                                                        for ($i = 0; $i < 66; $i++) {
                                                            $year += 1;
                                                            ?>
                                                            <option
                                                                value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                            <?php
                                                        }
                                                        ?>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input name="doctor_education_id" class="edit_doctor_education_id" value=""
                                                   type="hidden">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div><!--.tab-pane-->
                </section>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-4">
                <section class="upcoming-sessions">
                    <div class="card-block">
                        <div class="condition condition-clear"><!--added class 'condition-clear'-->
                            <div class="row">
                                <table class="table table-hover award_table">
                                    <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Organization</th>
                                        <th>Year</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="doctor_awards_listing">
                                    <?php
                                    if (is_array($awards)) {
                                        foreach ($awards as $award) {
                                            echo '<tr class="award_'.$award->id.'">
                                                <td class="title">' . $award->name . '</td>
                                                <td class="organization">' . $award->venue . '</td>
                                                <td class="year">' . $award->year . '</td>
                                                <td class="table-photo">
                                                    <a class="tabledit-edit-button btn btn-sm btn-default edit_award_btn" 
                                                        data-toggle="modal" data-target="#edit_award_modal" 
                                                        data-title="'.$award->name.'" data-organization="'.$award->venue.'" 
                                                        data-year="'.$award->year.'" data-award_id="'.$award->id.'" 
                                                        style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;margin-right: 5px;">
                                                        <span class="glyphicon glyphicon-pencil"></span>
                                                    </a>
                                                    <a href="" 
                                                        data-awardID="' . $award->id . '" 
                                                        class="tabledit-delete-button btn btn-sm btn-default doctor_update_profile_delete_award" 
                                                        style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </a>
                                                </td>
                                            </tr>';
                                        }
                                    } else {
                                        echo '<tr id="no_award_row">
                                            <td colspan="7" style="text-align: left;">No Award Found..</td>
                                        </tr>';
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="condition condition-clear">
                            <div class="row">
                                <div class="text-right">
                                    <button class="add btn btn-primary" id="add-award">Add</button>
                                </div>
                            </div>
                        </div>

                        <div class="condition condition-form" id="form-add-award">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold" for="exampleInput">Title</label>
                                        <input class="form-control doctor_award_title" placeholder="Title"
                                               value="" type="text">
                                    </fieldset>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold" for="exampleInput">Organization</label>
                                        <input class="form-control doctor_award_organization" placeholder="Organization"
                                               value="" type="text">
                                    </fieldset>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold" for="exampleInput">Year</label>
                                        <select class="select2 form-control doctor_award_year">
                                            <?php $i = 0;
                                            $year = 1950;
                                            for ($i = 0; $i < 66; $i++) {
                                                $year += 1;
                                                ?>
                                                <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                <?php
                                            }
                                            ?>

                                        </select>
                                    </fieldset>
                                </div>
                                <div class="text-right">

                                    <button type="button" class="btn btn-primary btn-inline add_doctor_award">Save
                                    </button>
                                    <button type="button" class="btn btn-default btn-inline cancel">Cancel</button>
                                    <br>
                                    <div class="success_award"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="edit_award_modal" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form name="edit_award_form">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Edit Award</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="form-label semibold" for="exampleInput">Title</label>
                                                    <input class="form-control edit_doctor_award_title" placeholder="Title"
                                                           value="" type="text">
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="form-label semibold" for="exampleInput">Organization</label>
                                                    <input class="form-control edit_doctor_award_organization" placeholder="Organization"
                                                           value="" type="text">
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="form-label semibold" for="exampleInput">Year</label>
                                                    <select class="select2 form-control edit_doctor_award_year">
                                                        <?php $i = 0;
                                                        $year = 1950;
                                                        for ($i = 0; $i < 66; $i++) {
                                                            $year += 1;
                                                            ?>
                                                            <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                            <?php
                                                        }
                                                        ?>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input class="edit_doctor_award_id" value=""
                                                   type="hidden">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
						<?php echo form_open('doctor/update_award'); ?>
                            <div class="overlay-edit">
                                <div class="container">
                                    <div class="condition condition-edit">
                                        <div class="row">
                                            <div>
                                                <input name="doctor_award_id" value="" type="hidden">
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <fieldset class="form-group">
                                                    <label class="form-label semibold" for="exampleInput">Title</label>
                                                    <input class="form-control doctor_award_title" placeholder="Title"
                                                           name="doctor_award_title"
                                                           value="" type="text">
                                                </fieldset>
                                            </div>
                                            <div class="col-lg-3 col-md-3">
                                                <fieldset class="form-group">
                                                    <label class="form-label semibold"
                                                           for="exampleInput">Organization</label>
                                                    <input class="form-control doctor_award_organization"
                                                           placeholder="Organization"
                                                           name="doctor_award_organization" value="" type="text">
                                                </fieldset>
                                            </div>
                                            <div class="col-lg-3 col-md-3">
                                                <fieldset class="form-group">
                                                    <label class="form-label semibold" for="exampleInput">Year</label>
                                                    <select class="select2 form-control doctor_award_year"
                                                            name="doctor_award_year">
                                                        <?php $i = 0;
                                                        $year = 1950;
                                                        for ($i = 0; $i < 66; $i++) {
                                                            $year += 1;
                                                            ?>
                                                            <option
                                                                value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                            <?php
                                                        }
                                                        ?>

                                                    </select>
                                                </fieldset>
                                            </div>
                                            <div class="text-right">
                                                <button type="submit"
                                                        class="btn btn-primary btn-inline update_doctor_award">Save
                                                </button>
                                                <button type="button" class="btn btn-default btn-inline cancel">
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div><!--.tab-pane-->

            <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-5">
                <section class="upcoming-sessions">
                    <div class="card-block">
                        <div class="condition condition-clear"><!--added class 'condition-clear'-->
                            <div class="row">
                                <table class="table table-hover membership_table">
                                    <thead>
                                    <tr>
                                        <th>Post</th>
                                        <th>Organization/Society</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //print_r($condition);
                                    //$id = $this->session->userdata('userid');
                                    if (is_array($memberships)) {
                                        foreach ($memberships as $membership) {
                                            echo '<tr>
                                <td style="display: none;">
                                    ' . $membership->membership_id . '
                                </td>
                                <td>
                                    ' . $membership->membership_post . '
                                </td>
                                <td>
                                    ' . $membership->society_name . '
                                </td>
                                <td class="table-photo">
                                    <a class="tabledit-edit-button btn btn-sm btn-default" style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;margin-right: 5px;"><span class="glyphicon glyphicon-pencil"></span></a>
                                    <a href="' . base_url() . '/doctor/delete_membership/' . $membership->membership_id . '" class="tabledit-delete-button btn btn-sm btn-default" style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>';
                                        }
                                    } else
                                    //echo $allergies;
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="condition condition-clear">
                            <div class="row">
                                <div class="col-md-2 col-md-offset-5">
                                    <button class="add">Add</button>
                                </div>
                            </div>
                        </div>

                        <div class="condition condition-form">
                            <div class="row">
                                <div class="col-lg-8 col-md-8">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold" for="exampleInput">Title</label>
                                        <input class="form-control doctor_membership_title" placeholder="Title"
                                               value="" type="text">
                                    </fieldset>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold" for="exampleInput">Organization</label>
                                        <input class="form-control doctor_membership_organization"
                                               placeholder="Organization"
                                               value="" type="text">
                                    </fieldset>
                                </div>
                                <div class="text-right">
                                    <button type="button" class="btn btn-primary btn-inline add_doctor_membership">Save
                                    </button>
                                    <button type="button" class="btn btn-primary-outline btn-inline cancel">Cancel
                                    </button>
                                    <br>
                                    <div class="success_membership"></div>
                                </div>
                            </div>
                        </div>
						<?php echo form_open('doctor/update_membership'); ?>
                            <div class="overlay-edit">
                                <div class="container">
                                    <div class="condition condtion-edit">
                                        <div class="row">
                                            <div>
                                                <input name="doctor_membership_id" value="" type="hidden">
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <fieldset class="form-group">
                                                    <label class="form-label semibold" for="exampleInput">Title</label>
                                                    <input class="form-control doctor_membership_title"
                                                           placeholder="Title"
                                                           name="doctor_membership_title" value="" type="text">
                                                </fieldset>
                                            </div>
                                            <div class="col-lg-4 col-md-4">
                                                <fieldset class="form-group">
                                                    <label class="form-label semibold"
                                                           for="exampleInput">Organization</label>
                                                    <input class="form-control doctor_membership_organization"
                                                           placeholder="Organization"
                                                           name="doctor_membership_organization"
                                                           value="" type="text">
                                                </fieldset>
                                            </div>
                                            <div class="text-right">
                                                <button type="submit"
                                                        class="btn btn-primary btn-inline update_doctor_membership">Save
                                                </button>
                                                <button type="button" class="btn btn-primary-outline btn-inline cancel">
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div><!--.tab-pane-->
            <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-6">

                <section class="upcoming-sessions">
                    <div class="card-block">
                        <form class="doctor_licenced_state_form">
                            <?php
                            /*                        $us_state_names = array(
                                                        'ALABAMA' => 'ALABAMA', 'ALASKA' => 'ALASKA', 'ARIZONA' => 'ARIZONA', 'ARKANSAS' => 'ARKANSAS', 'CALIFORNIA' => 'CALIFORNIA', 'COLORADO' => 'COLORADO', 'CONNECTICUT' => 'CONNECTICUT', 'DELAWARE' => 'DELAWARE', 'FLORIDA' => 'FLORIDA', 'GEORGIA' => 'GEORGIA', 'HAWAII' => 'HAWAII', 'IDAHO' => 'IDAHO', 'ILLINOIS' => 'ILLINOIS', 'INDIANA' => 'INDIANA', 'IOWA' => 'IOWA', 'KANSAS' => 'KANSAS', 'KENTUCKY' => 'KENTUCKY', 'LOUISIANA' => 'LOUISIANA', 'MAINE' => 'MAINE', 'MARYLAND' => 'MARYLAND', 'MASSACHUSETTS' => 'MASSACHUSETTS', 'MICHIGAN' => 'MICHIGAN', 'MINNESOTA' => 'MINNESOTA', 'MISSISSIPPI' => 'MISSISSIPPI', 'MISSOURI' => 'MISSOURI', 'MONTANA' => 'MONTANA', 'NEBRASKA' => 'NEBRASKA', 'NEVADA' => 'NEVADA', 'NEW HAMPSHIRE' => 'NEW HAMPSHIRE', 'NEW JERSEY' => 'NEW JERSEY', 'NEW MEXICO' => 'NEW MEXICO', 'NEW YORK' => 'NEW YORK', 'NORTH CAROLINA' => 'NORTH CAROLINA', 'NORTH DAKOTA' => 'NORTH DAKOTA', 'OHIO' => 'OHIO', 'OKLAHOMA' => 'OKLAHOMA', 'OREGON' => 'OREGON', 'PENNSYLVANIA' => 'PENNSYLVANIA', 'RHODE ISLAND' => 'RHODE ISLAND', 'SOUTH CAROLINA' => 'SOUTH CAROLINA', 'SOUTH DAKOTA' => 'SOUTH DAKOTA', 'TENNESSEE' => 'TENNESSEE', 'TEXAS' => 'TEXAS', 'UTAH' => 'UTAH', 'VERMONT' => 'VERMONT', 'VIRGINIA' => 'VIRGINIA', 'WASHINGTON' => 'WASHINGTON', 'WEST VIRGINIA' => 'WEST VIRGINIA', 'WISCONSIN' => 'WISCONSIN', 'WYOMING' => 'WYOMING'
                                                    );
                                                    */ ?>
                            <div class="row">
                                <?php $i = 0; ?>
                                <?php foreach ($states as $id => $state): ?>
                                    <?php /*echo "<pre>"; print_r($licenced_states); die(); */ ?>
                                    <?php
                                    $isChecked = false;
                                    if (count($licenced_states) > 0 && $licenced_states != 'No result found') {
                                        foreach ($licenced_states as $key => $selected_state) {
                                            if ($selected_state->state_id == $id) {
                                                $isChecked = true;
                                                break;
                                            }
                                        }
                                    }
                                    ?>
                                    <?php if ($isChecked): ?>
                                        <div class="col-md-3 col-sm-4 col-xs-6 col-lg-3">
                                            <input checked name="state[]" id="doctor_licenced_state_<?= $i; ?>"
                                                   type="checkbox" value="<?= $id; ?>"
                                                   class="doctor_licenced_state">
                                            <label for="doctor_licenced_state_<?= $i; ?>"
                                                   style="display: inline;"><?= $state; ?></label>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-md-3 col-sm-4 col-xs-6 col-lg-3">
                                            <input name="state[]" id="doctor_licenced_state_<?= $i; ?>" type="checkbox"
                                                   value="<?= $id; ?>"
                                                   class="doctor_licenced_state">
                                            <label for="doctor_licenced_state_<?= $i; ?>"
                                                   style="display: inline;"><?= $state; ?></label>
                                        </div>
                                    <?php endif; ?>
                                    <?php $i++; ?>
                                <?php endforeach; ?>
                            </div>

                            <br>
                            <div class="row">
                                <div class="text-right">
                                    <button type="button" class="btn btn-primary btn-inline add_doctor_licenced_state">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default btn-inline cancel">Cancel
                                    </button>
                                    <br>
                                    <div class="success_licenced_state"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div><!--.tab-pane-->

            <div role="tabpanel" class="tab-pane fade" id="language_spoken_tab">
                <section class="upcoming-sessions">
                    <div class="card-block">
                        <form class="doctor_language_form">
                            <?php
                            /*                        $us_state_names = array(
                                                        'ALABAMA' => 'ALABAMA', 'ALASKA' => 'ALASKA', 'ARIZONA' => 'ARIZONA', 'ARKANSAS' => 'ARKANSAS', 'CALIFORNIA' => 'CALIFORNIA', 'COLORADO' => 'COLORADO', 'CONNECTICUT' => 'CONNECTICUT', 'DELAWARE' => 'DELAWARE', 'FLORIDA' => 'FLORIDA', 'GEORGIA' => 'GEORGIA', 'HAWAII' => 'HAWAII', 'IDAHO' => 'IDAHO', 'ILLINOIS' => 'ILLINOIS', 'INDIANA' => 'INDIANA', 'IOWA' => 'IOWA', 'KANSAS' => 'KANSAS', 'KENTUCKY' => 'KENTUCKY', 'LOUISIANA' => 'LOUISIANA', 'MAINE' => 'MAINE', 'MARYLAND' => 'MARYLAND', 'MASSACHUSETTS' => 'MASSACHUSETTS', 'MICHIGAN' => 'MICHIGAN', 'MINNESOTA' => 'MINNESOTA', 'MISSISSIPPI' => 'MISSISSIPPI', 'MISSOURI' => 'MISSOURI', 'MONTANA' => 'MONTANA', 'NEBRASKA' => 'NEBRASKA', 'NEVADA' => 'NEVADA', 'NEW HAMPSHIRE' => 'NEW HAMPSHIRE', 'NEW JERSEY' => 'NEW JERSEY', 'NEW MEXICO' => 'NEW MEXICO', 'NEW YORK' => 'NEW YORK', 'NORTH CAROLINA' => 'NORTH CAROLINA', 'NORTH DAKOTA' => 'NORTH DAKOTA', 'OHIO' => 'OHIO', 'OKLAHOMA' => 'OKLAHOMA', 'OREGON' => 'OREGON', 'PENNSYLVANIA' => 'PENNSYLVANIA', 'RHODE ISLAND' => 'RHODE ISLAND', 'SOUTH CAROLINA' => 'SOUTH CAROLINA', 'SOUTH DAKOTA' => 'SOUTH DAKOTA', 'TENNESSEE' => 'TENNESSEE', 'TEXAS' => 'TEXAS', 'UTAH' => 'UTAH', 'VERMONT' => 'VERMONT', 'VIRGINIA' => 'VIRGINIA', 'WASHINGTON' => 'WASHINGTON', 'WEST VIRGINIA' => 'WEST VIRGINIA', 'WISCONSIN' => 'WISCONSIN', 'WYOMING' => 'WYOMING'
                                                    );
                                                    */ ?>
                            <div class="row">
                                <?php $i = 0; ?>
                                <?php foreach ($languages as $id => $language): ?>
                                    <?php
                                    $isChecked = false;
                                    if (count($doctor_languages) > 0 && $doctor_languages != 'No result found') {
                                        foreach ($doctor_languages as $key => $selected_language) {
                                            if ($selected_language->language_id == $id) {
                                                $isChecked = true;
                                                break;
                                            }
                                        }
                                    }
                                    ?>
                                    <?php if ($isChecked): ?>
                                        <div class="col-md-3 col-lg-3">
                                            <input checked name="language[]" id="doctor_language_<?= $i; ?>"
                                                   type="checkbox" value="<?= $id; ?>"
                                                   class="doctor_language">
                                            <label for="doctor_language_<?= $i; ?>"
                                                   style="display: inline;"><?= $language; ?></label>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-md-3 col-lg-3">
                                            <input name="language[]" id="doctor_language_<?= $i; ?>" type="checkbox"
                                                   value="<?= $id; ?>"
                                                   class="doctor_language">
                                            <label for="doctor_language_<?= $i; ?>"
                                                   style="display: inline;"><?= $language; ?></label>
                                        </div>
                                    <?php endif; ?>
                                    <?php $i++; ?>
                                <?php endforeach; ?>
                            </div>

                            <br>
                            <div class="row">
                                <div class="text-right">
                                    <button type="button" class="btn btn-primary btn-inline add_doctor_language">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default btn-inline cancel">Cancel
                                    </button>
                                    <br>
                                    <div class="success_language"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div><!--.tab-content-->
        </section><!--.tabs-section-->
    </div>
</div>

<script>
    function removeThings(obj, url, message, callBack) {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>' + url,
            data: {},
            success: function () {
                obj.remove();
                notySuccess(message);
                callBack();
            }
        });
    }
    $(document).ready(function () {
        <?php if($this->uri->segment(3)): ?>
        $('a.nav-link').removeClass('active');
        $('div.tab-pane').removeClass('active in');
        $('a.nav-link[href="#<?php echo $this->uri->segment(3); ?>"]').addClass('active');
        $('div.tab-pane#<?php echo $this->uri->segment(3); ?>').addClass('active in');
        <?php endif; ?>
        $(document).on('click', '.doctor_update_profile_delete_award', function (e) {
            e.preventDefault();
            var self = $(this);
            var awardID = self.attr('data-awardID');
            notyMessage('You really want to delete this award', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            removeThings(self.parent().parent(), 'doctor/delete_award/' + awardID, 'Award Deleted', function(){
                                if ($('#doctor_awards_listing tr').length == 0) {
                                    $('#doctor_awards_listing').html('<tr id="no_award_row">' +
                                        '<td colspan="7" style="text-align: left;">No Award Found..</td>' +
                                    '</tr>');
                                }
                            });
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });
        $(document).on('click', '.edit_award_btn', function (e) {
            var self = $(this);
            var id = self.attr('data-award_id');
            var title = self.attr('data-title');
            var organization = self.attr('data-organization');
            var year = self.attr('data-year');
            $('.edit_doctor_award_id').val(id);
            $('.edit_doctor_award_title').val(title);
            $('.edit_doctor_award_organization').val(organization);
            $('.edit_doctor_award_year').val(year);
            $('.select2').select2();
        });
        $('form[name="edit_award_form"]').submit(function (e) {
            e.preventDefault();
            var award_id = $('.edit_doctor_award_id').val();
            var title = $('.edit_doctor_award_title').val();
            var organization = $('.edit_doctor_award_organization').val();
            var year = $('.edit_doctor_award_year').val();
            if (award_id == '' || title == '' || organization == '' || year == '') {
                notyError('Please make sure all fields are filled.');
                return;
            }
            var request = {};
            request.award_id = award_id;
            request.title = title;
            request.organization = organization;
            request.year = year;
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>doctor/update_award',
                data: request,
                dataType: "json",
                success: function (data) {
                    if (!data.bool) {
                        notyError(data.message);
                        return;
                    }
                    $('#edit_award_modal').modal('hide');
                    notySuccess(data.message);
                    $('.award_' + award_id + ' .title').text(title);
                    $('.award_' + award_id + ' .organization').text(organization);
                    $('.award_' + award_id + ' .year').text(year);
                    $('.award_' + award_id + ' a.edit_award_btn')
                        .attr('data-title', title)
                        .attr('data-organization', organization)
                        .attr('data-year', year);
                }
            });
            return false;
        });
        $(document).on('click', '.doctor_update_profile_delete_education', function (e) {
            e.preventDefault();
            var self = $(this);
            var educationID = self.attr('data-educationID');
            notyMessage('You really want to delete this education', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            removeThings(self.parent().parent(), 'doctor/delete_education/' + educationID, 'Education Deleted', function () {
                                if ($('#education_listing tr').length == 0) {
                                    $('#education_listing').html('<tr id="no_education_row">' +
                                        '<td colspan="7" style="text-align: left;">No Education Found..</td>' +
                                        '</tr>');
                                }
                            });
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });
        $(document).on('click', '.edit_education_btn', function (e) {
            var self = $(this);
            var id = self.attr('data-education_id');
            var title = self.attr('data-title');
            var university = self.attr('data-university');
            var from_year = self.attr('data-from_year');
            var to_year = self.attr('data-to_year');
            $('.edit_doctor_education_id').val(id);
            $('.edit_doctor_education_title').val(title);
            $('.edit_doctor_education_university').val(university);
            $('.edit_doctor_education_year_start').val(from_year);
            $('.edit_doctor_education_year_end').val(to_year);
            $('.select2').select2();
        });
        $('form[name="edit_education_form"]').submit(function (e) {
            e.preventDefault();
            var education_id = $('.edit_doctor_education_id').val();
            var title = $('.edit_doctor_education_title').val();
            var university = $('.edit_doctor_education_university').val();
            var from_year = $('.edit_doctor_education_year_start').val();
            var to_year = $('.edit_doctor_education_year_end').val();
            if (education_id == '' || title == '' || university == '' || from_year == '' || to_year == '') {
                notyError('Please make sure all fields are filled.');
                return;
            }
            var request = {};
            request.education_id = education_id;
            request.title = title;
            request.university = university;
            request.from_year = from_year;
            request.to_year = to_year;
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>doctor/update_education',
                data: request,
                dataType: "json",
                success: function (data) {
                    if (!data.bool) {
                        notyError(data.message);
                        return;
                    }
                    $('#edit_education_modal').modal('hide');
                    notySuccess(data.message);
                    $('.education_' + education_id + ' .title').text(title);
                    $('.education_' + education_id + ' .university').text(university);
                    $('.education_' + education_id + ' .from_and_to_year').text(from_year + ' - ' + to_year);
                    $('.education_' + education_id + ' a.edit_education_btn')
                        .attr('data-title', title)
                        .attr('data-university', university)
                        .attr('data-from_year', from_year)
                        .attr('data-to_year', to_year);
                }
            });
            return false;
        });
        $(document).on('click', '.doctor_update_profile_delete_specialty', function (e) {
            e.preventDefault();
            var self = $(this);
            var specialtyID = self.attr('data-specialtyID');
            notyMessage('You really want to delete this specialty', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            removeThings(self.parent().parent(), 'doctor/delete_speciality/' + specialtyID, 'Specialty Deleted', function(){
                                if ($('#doctor_update_profile_specialties tr').length == 0) {
                                    $('#doctor_update_profile_specialties').html('<tr id="no_specialty_row">' +
                                        '<td colspan="7" style="text-align: left;">No Specialty Found..</td>' +
                                    '</tr>');
                                }
                            });
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });
        $(document).on('click', '.edit_specialty_btn', function (e) {
            var self = $(this);
            var id = self.attr('data-specialty_id');
            var specialty = self.attr('data-specialty');
            $('.edit_doctor_specialty_id').val(id);
            $('.edit_doctor_specialty').val(specialty);
            $('.select2').select2();
        });
        $('form[name="edit_specialty_form"]').submit(function(e){
            e.preventDefault();
            var specialty_id = $('.edit_doctor_specialty_id').val();
            var specialty = $('.edit_doctor_specialty').val();
            if (specialty_id == '' || specialty == '') {
                notyError('Please make sure all fields are filled.');
                return;
            }
            var request = {};
            request.specialty_id = specialty_id;
            request.specialty = specialty;
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>doctor/update_speciality',
                data: request,
                dataType: "json",
                success: function (data) {
                    if (!data.bool) {
                        notyError(data.message);
                        return;
                    }
                    $('#edit_specialty_modal').modal('hide');
                    notySuccess(data.message);
                    $('.specialty_' + specialty_id + ' .specialty').text(specialty);
                    $('.specialty_' + specialty_id + ' a.edit_specialty_btn')
                        .attr('data-specialty', specialty);
                }
            });
            return false;
        });
        $(".add_doctor_speciality").click(function (e) {
            e.preventDefault();
            var str = "<?php echo base_url(); ?>doctor/add_specialties";
            var doctor_specialty = $(".doctor_specialty").val();
            if (doctor_specialty == 'other') {
                doctor_specialty = $('.doctor_specialty_other').val();
            }
            if (doctor_specialty == '') {
                notyError('Please Fill the specialty first')
            }
            else {
                $.ajax({
                    type: "POST",
                    url: str,
                    data: {specialty: doctor_specialty},
                    dataType: "json",
                    success: function (data) {
                        if (data.msg === '') {
                            if (data.isNew) {
                                $('.doctor_specialty').prepend('<option value="' + doctor_specialty + '">' + doctor_specialty + '</option>');
                            }
                            $(".doctor_specialization_table tbody").append('<tr class="specialty_'+data.id+'">' +
                                '<td class="specialty">' + doctor_specialty + '</td>' +
                                '<td class="table-photo">' +
                                '<a ' +
                                'class="tabledit-edit-button btn btn-sm btn-default edit_specialty_btn" ' +
                                'data-toggle="modal" data-target="#edit_specialty_modal" ' +
                                'data-specialty="'+doctor_specialty+'" data-specialty_id="'+data.id+'" ' +
                                'style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;margin-right: 5px;">' +
                                '<span class="glyphicon glyphicon-pencil"></span>' +
                                '</a>' +
                                '<a ' +
                                'class="tabledit-delete-button btn btn-sm btn-default doctor_update_profile_delete_specialty"' +
                                'data-specialtyID="' + data.id + '"' +
                                'style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;">' +
                                '<span class="glyphicon glyphicon-trash"></span>' +
                                '</a>' +
                                '</td>' +
                                '</tr>');
                            notySuccess("Added Successfully");
                            $(".doctor_specialty").val('');
                            $('.doctor_specialty_other').val('')
                            $("#form-add-spec").hide();
                            $("#add-spec").show();
                            $('#no_specialty_row').remove();
                            profile_completion_error();
                        } else {
                            notyError(data.msg);
                        }
                    }
                });
            }
        });
    });

</script>

<script type="text/javascript">
    //    var rowEditButtons = document.querySelectorAll('.condition-clear .tabledit-edit-button');
    var addButtons = document.querySelectorAll('.condition-clear .add');
    var cancelButtons = document.querySelectorAll('.condition-form .cancel');
    var overlayCancelButtons = document.querySelectorAll('.overlay-edit .cancel');
    var addDcButtons = document.querySelectorAll(".condition-form button.btn.btn-primary.btn-inline:not(.cancel)");

    function checkParentClass(clsName, childObj) {
        while (!childObj.parentNode.classList.contains(clsName)) {
            childObj = childObj.parentNode;
        }
        return childObj.parentNode;
    }

    //    for (i = 0; i < rowEditButtons.length; i++) {
    //        rowEditButtons[i].addEventListener('click', function () {
    //            var curOverlay = checkParentClass('tab-pane', this).querySelector('.overlay-edit');
    //            var formCols = curOverlay.querySelector('.row').children;
    //            var curTR = this;
    //            var tabHeads = document.querySelectorAll('.page-doctor-info .nav-link');
    //
    //            while (curTR.parentNode.tagName !== "TR") {
    //                curTR = curTR.parentNode;
    //            }
    //            var curTD = curTR.parentNode.children;
    //
    //            for (i = 0; i < curTD.length - 1; i++) {
    //                if (formCols[i].querySelector('input')) {
    //                    formCols[i].querySelector('input').value = curTD[i].innerHTML.trim();
    //                } else if (formCols[i].querySelector('select.select2')) {
    //                    if (tabHeads[3].classList.contains('active')) {
    //                        $('select.doctor_award_year').val(curTD[i].innerHTML.trim()).change();
    //                    } else if (tabHeads[2].classList.contains('active')) {
    //                        var curTDranged = curTD[i].innerHTML.trim();
    //                        console.log(curTDranged);
    //                        $('select.doctor_education_year_start').val(curTDranged.substr(0, curTDranged.indexOf(' - '))).change();
    //                        $('select.doctor_education_year_end').val(curTDranged.substr(curTDranged.indexOf(' - ') + 3)).change();
    //                    }
    //                } else {
    //                    console.log(false);
    //                }
    //            }
    //            curOverlay.style.display = 'block';
    //        }, false)
    //    }
    for (i = 0; i < overlayCancelButtons.length; i++) {
        overlayCancelButtons[i].addEventListener('click', function () {
            checkParentClass('overlay-edit', this).removeAttribute('style');
        }, false)
    }

    for (i = 0; i < addButtons.length; i++) {
        addButtons[i].addEventListener('click', function () {
            this.style.display = 'none';
            this.parentNode.parentNode.parentNode.nextElementSibling.style.display = 'block';
        }, false)
    }
    for (i = 0; i < cancelButtons.length; i++) {
        cancelButtons[i].addEventListener('click', function () {
            this.parentNode.parentNode.parentNode.parentNode.querySelector('.add').removeAttribute('style');
            this.parentNode.parentNode.parentNode.removeAttribute('style');
        }, false)
    }
</script>


<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
    Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="image_cropper_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="border-radius: 0px;">
            <div class="modal-body" style="padding: 0px; overflow: hidden;">
                <div class="" id="pending_appoint_for_patient2"></div>
                <img id="my_custom_cropper_image_element" style="max-width: 100%; height: 500px;">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary " onclick="save_cropped_image()">Save changes</button>
            </div>
        </div>
    </div>
</div>