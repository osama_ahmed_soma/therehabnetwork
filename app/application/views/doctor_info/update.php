<div class="page-content">
    <div class="container-fluid">
        <section class="tabs-section">

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-1">
                    <div class="condition">
                        <div class="row">
                            <?php if ($this->session->flashdata('message')) { ?>
                                <div class="alert alert-success">
                                    <?php echo $this->session->flashdata('message') ?>
                                </div>
                            <?php } ?>

                            <?php
                            if (is_array($condition)) {
                                foreach ($condition as $conditions) {
									echo form_open('myhealth/update_condition');
                                    ?>
                                        <div class="col-lg-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold" for="exampleInput">Condition</label>
                                                <input class="form-control condition_term" placeholder="Condition"
                                                       name="condition"
                                                       value="<?php echo $conditions->condition_term; ?>" type="text">
                                            </fieldset>
                                        </div>
                                        <div class="col-lg-3">
                                            <fieldset class="form-group">
                                                <label class="form-label" for="exampleInputEmail1">Date</label>
                                                <div class="input-group date ">
                                                    <input class="form-control daterange_picker" type="text" name="date"
                                                           placeholder="date" value="<?php echo $conditions->date; ?>">
                                                    <span class="input-group-addon">
                                                        <i class="font-icon font-icon-calend"></i>
                                                    </span>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-lg-3">
                                            <fieldset class="form-group">
                                                <label class="form-label" for="exampleInputPassword1">Source</label>
                                                <input class="form-control source_term" placeholder="Source"
                                                       name="source" type="text"
                                                       value="<?php echo $conditions->source; ?>">
                                            </fieldset>
                                        </div>
                                        <?php
                                        if ($conditions->dosage != '' || $conditions->time != '' || $conditions->frequency != '') {
                                            ?>
                                            <div class="col-lg-4">
                                                <fieldset class="form-group">
                                                    <label class="form-label">Dosage</label>
                                                    <div class="input-group">
                                                        <input class="form-control condition_dosage_medication"
                                                               name="dosage" type="text"
                                                               value="<?php echo $conditions->dosage; ?>"
                                                               placeholder="1 Pill">
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-lg-4">
                                                <fieldset class="form-group">
                                                    <label class="form-label">Frequency</label>
                                                    <select name="frequency" class="select2 condition_freq_medication">
                                                        <option
                                                            value="Once" <?php if ($conditions->frequency == 'once') echo "selected='selected'"; ?>>
                                                            Once
                                                        </option>
                                                        <option
                                                            value="Twice" <?php if ($conditions->frequency == 'Twice') echo "selected='selected'"; ?>>
                                                            Twice
                                                        </option>
                                                        <option
                                                            value="Three times" <?php if ($conditions->frequency == 'Three times') echo "selected='selected'"; ?>>
                                                            Three times
                                                        </option>
                                                        <option
                                                            value="Four times" <?php if ($conditions->frequency == 'Four times') echo "selected='selected'"; ?>>
                                                            Four times
                                                        </option>
                                                        <option
                                                            value="Five times" <?php if ($conditions->frequency == 'Five times') echo "selected='selected'"; ?>>
                                                            Five times
                                                        </option>
                                                        <option
                                                            value="Six times" <?php if ($conditions->frequency == 'Six times') echo "selected='selected'"; ?>>
                                                            Six times
                                                        </option>
                                                        <option
                                                            value="As Needed" <?php if ($conditions->frequency == 'As Needed') echo "selected='selected'"; ?>>
                                                            As Needed
                                                        </option>
                                                    </select>
                                                </fieldset>
                                            </div>
                                            <div class="col-lg-4">
                                                <fieldset class="form-group">
                                                    <label class="form-label">&nbsp;</label>
                                                    <select name="time" class="select2 condition_time_medication">
                                                        <option
                                                            value="Hourly" <?php if ($conditions->time == 'Hourly') echo "selected='selected'"; ?>>
                                                            Hourly
                                                        </option>
                                                        <option
                                                            value="Daily" <?php if ($conditions->time == 'Daily') echo "selected='selected'"; ?>>
                                                            Daily
                                                        </option>
                                                        <option
                                                            value="Weekly" <?php if ($conditions->time == 'Weekly') echo "selected='selected'"; ?>>
                                                            Weekly
                                                        </option>
                                                        <option
                                                            value="Monthly" <?php if ($conditions->time == 'Monthly') echo "selected='selected'"; ?>>
                                                            Monthly
                                                        </option>
                                                    </select>
                                                </fieldset>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                        <?php
                                        if ($conditions->severity != '' || $conditions->reaction != '') {
                                            ?>
                                            <div class="col-lg-6">
                                                <fieldset class="form-group">
                                                    <label class="form-label">Severity</label>
                                                    <div class="input-group">
                                                        <select name="severity"
                                                                class="select2 condition_severity_allergies">
                                                            <option
                                                                value="Unspecified" <?php if ($conditions->severity == 'Unspecified') echo "selected='selected'"; ?>>
                                                                Not Sure
                                                            </option>
                                                            <option
                                                                value="Mild" <?php if ($conditions->severity == 'Mild') echo "selected='selected'"; ?>>
                                                                Mild
                                                            </option>
                                                            <option
                                                                value="Moderate" <?php if ($conditions->severity == 'Moderate') echo "selected='selected'"; ?>>
                                                                Moderate
                                                            </option>
                                                            <option
                                                                value="Severe" <?php if ($conditions->severity == 'Severe') echo "selected='selected'"; ?>>
                                                                Severe
                                                            </option>
                                                        </select>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-lg-6">
                                                <fieldset class="form-group">
                                                    <label class="form-label">Reaction</label>
                                                    <input class="form-control condition_reaction_allergies"
                                                           name="reaction" placeholder="Rashes On the skin"
                                                           value="<?php echo $conditions->reaction; ?>" type="text">
                                                </fieldset>
                                            </div>
                                        <?php }
                                        ?>
                                        <center>
                                            <input type="hidden" class="condition_type" name="id"
                                                   value="<?php echo $conditions->id; ?>">
                                            <button type="submit" class="btn btn-inline">Add Condition</button>
                                            <button type="button" class="btn btn-inline cancel">Cancel</button>
                                        </center>
                                    </form>
                                    <?php
                                }
                            }
                            ?>

                        </div>
                    </div>

                </div><!--.tab-pane-->
            </div><!--.tab-content-->
        </section><!--.tabs-section-->
    </div>
</div>

