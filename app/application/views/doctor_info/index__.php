<div class="page-content page-doctor-info"><!--added class 'page-doctor-info'-->
    <div class="container-fluid">
        <section class="tabs-section">
            <div class="tabs-section-nav tabs-section-nav-icons">
                <div class="tbl">
                    <ul class="nav" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#tabs-1-tab-1" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Overview
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-tab-2" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    My Specialties
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-tab-3" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Education
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-tab-4" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Awards
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-tab-5" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Professional Membership
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-tab-6" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    License State
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-tab-7" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Language
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div><!--.tabs-section-nav-->

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-1">
                    <?php
                    //print_r($condition);
                    $id = $this->session->userdata('userid');
                    ?>
                    <div class="form-group overview_doctor">
                        <textarea name="doctor_overview" rows="10" class="form-control doctor_overview"
                                  placeholder="Type a message"><?php echo $doctor->about; ?></textarea>
                    </div>
                    <div class="row">
                        <center>
                            <input type="hidden" class="condition_type" value="health">
                            <button type="button" class="btn btn-primary btn-inline add_doctor_overview">Save</button>
                            <button type="button" class="btn btn-primary-outline btn-inline">Cancel</button>
                            <br>
                            <div class="success_overview"></div>
                        </center>
                    </div>
                </div><!--.tab-pane-->

                <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-2">

                    <div class="condition condition-clear"><!--added class 'condition-clear'-->
                        <div class="row">
                            <table class="table table-hover doctor_specialization_table">
                                <thead>
                                <tr>
                                    <th>Specialty</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $empty_message = 'You haven\'t yet add any specialties!';
                                //print_r($condition);
                                //$id = $this->session->userdata('userid');
                                if (is_array($specials)) {
                                    foreach ($specials as $special) {
                                        echo '<tr>
                                <td>
                                    ' . $special->speciality . '
                                </td>
                                
                                <td class="table-photo">
                                    <a class="tabledit-edit-button btn btn-sm btn-default" style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;margin-right: 5px;"><span class="glyphicon glyphicon-pencil"></span></a>
                                    <a href="myhealth/delete_health/' . $special->id . '/' . $id . '" class="tabledit-delete-button btn btn-sm btn-default" style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>';
                                    }
                                } else
                                    echo '<tr>
                                <td>
                                    
                                </td>
                                <tr>';
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="condition condition-clear">
                        <div class="row">
                            <div class="col-md-2 col-md-offset-5">
                                <button class="add">Add</button>
                            </div>
                        </div>
                    </div>

                    <div class="condition condition-form">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="exampleInput">Speciality</label>
                                    <input class="form-control doctor_specialty" placeholder="Specialty"
                                           value="" type="text">
                                </fieldset>
                            </div>
                            <center>
                                <input type="hidden" class="condition_type_medication" value="medication">
                                <button type="button" class="btn btn-primary btn-inline add_doctor_speciality">Save
                                </button>
                                <button type="button" class="btn btn-primary-outline btn-inline cancel">Cancel</button>
                                <div class="success_speciality"></div>
                            </center>
                        </div>
                    </div>
                    <div class="overlay-edit">
                        <div class="container">
                            <div class="condition condition-edit">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold" for="exampleInput">Speciality</label>
                                            <input class="form-control doctor_specialty" placeholder="Specialty"
                                                   value="" type="text">
                                        </fieldset>
                                    </div>
                                    <center>
                                        <input type="hidden" class="condition_type_medication" value="medication">
                                        <button type="button" class="btn btn-primary btn-inline add_doctor_speciality">
                                            Save
                                        </button>
                                        <button id="tester-id" type="button"
                                                class="btn btn-primary-outline btn-inline cancel">Cancel
                                        </button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--.tab-pane-->

                <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-3">

                    <div class="condition condition-clear"><!--added class 'condition-clear'-->
                        <div class="row">
                            <table class="table table-hover education_table">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>University</th>
                                    <th>Year</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                //print_r($condition);
                                //$id = $this->session->userdata('userid');
                                if (is_array($educations)) {
                                    foreach ($educations as $education) {
                                        echo '<tr>
                                <td>
                                    ' . $education->name . '
                                </td>
                                <td>
                                    ' . $education->institute . '
                                </td>
                                <td>
                                    ' . $education->from_year . ' - ' . $education->to_year . '
                                </td>
                                <td class="table-photo">
                                    <a class="tabledit-edit-button btn btn-sm btn-default" style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;margin-right: 5px;"><span class="glyphicon glyphicon-pencil"></span></a>
                                    <a href="myhealth/delete_health/' . $education->id . '/' . $id . '" class="tabledit-delete-button btn btn-sm btn-default" style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>';
                                    }
                                } else
                                //echo $allergies;
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="condition condition-clear">
                        <div class="row">
                            <div class="col-md-2 col-md-offset-5">
                                <button class="add">Add</button>
                            </div>
                        </div>
                    </div>

                    <div class="condition condition-form">
                        <div class="row">
                            <div class="col-lg-5 col-md-5">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="exampleInput">Education</label>
                                    <input class="form-control doctor_education_title" placeholder="Title"
                                           value="" type="text">
                                </fieldset>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="exampleInput">University</label>
                                    <input class="form-control doctor_education_university" placeholder="University"
                                           value="" type="text">
                                </fieldset>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="exampleInput">From</label>
                                    <select class="select2 form-control doctor_education_year_start">
                                        <?php $i = 0;
                                        $year = 1950;
                                        for ($i = 0; $i < 66; $i++) {
                                            $year += 1;
                                            ?>
                                            <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                            <?php
                                        }
                                        ?>

                                    </select>
                                </fieldset>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="exampleInput">To</label>
                                    <select class="select2 form-control doctor_education_year_end">
                                        <?php $i = 0;
                                        $year = 1950;
                                        for ($i = 0; $i < 66; $i++) {
                                            $year += 1;
                                            ?>
                                            <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                            <?php
                                        }
                                        ?>

                                    </select>
                                </fieldset>
                            </div>
                            <center>
                                <input type="hidden" class="condition_type_medication" value="medication">
                                <button type="button" class="btn btn-primary btn-inline add_doctor_education">Save
                                </button>
                                <button type="button" class="btn btn-primary-outline btn-inline cancel">Cancel</button>
                            </center>
                        </div>
                    </div>
                    <div class="overlay-edit">
                        <div class="container">
                            <div class="condition condition-edit">
                                <div class="row">
                                    <div class="col-lg-5 col-md-5">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold" for="exampleInput">Education</label>
                                            <input class="form-control doctor_education_title" placeholder="Title"
                                                   value="" type="text">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold" for="exampleInput">University</label>
                                            <input class="form-control doctor_education_university"
                                                   placeholder="University"
                                                   value="" type="text">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold" for="exampleInput">From</label>
                                            <select class="select2 form-control doctor_education_year_start">
                                                <?php $i = 0;
                                                $year = 1950;
                                                for ($i = 0; $i < 66; $i++) {
                                                    $year += 1;
                                                    ?>
                                                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                    <?php
                                                }
                                                ?>

                                            </select>
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold" for="exampleInput">To</label>
                                            <select class="select2 form-control doctor_education_year_end">
                                                <?php $i = 0;
                                                $year = 1950;
                                                for ($i = 0; $i < 66; $i++) {
                                                    $year += 1;
                                                    ?>
                                                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                    <?php
                                                }
                                                ?>

                                            </select>
                                        </fieldset>
                                    </div>
                                    <center>
                                        <input type="hidden" class="condition_type_medication" value="medication">
                                        <button type="button" class="btn btn-primary btn-inline add_doctor_education">
                                            Save
                                        </button>
                                        <button type="button" class="btn btn-primary-outline btn-inline cancel">Cancel
                                        </button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--.tab-pane-->

                <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-4">
                    <div class="condition condition-clear"><!--added class 'condition-clear'-->
                        <div class="row">
                            <table class="table table-hover award_table">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Organization</th>
                                    <th>Year</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                //print_r($condition);
                                //$id = $this->session->userdata('userid');
                                if (is_array($awards)) {
                                    foreach ($awards as $award) {
                                        echo '<tr>
                                <td>
                                    ' . $award->name . '
                                </td>
                                <td>
                                    ' . $award->venue . '
                                </td>
                                <td>
                                    ' . $award->year . '
                                </td>
                                <td class="table-photo">
                                    <a class="tabledit-edit-button btn btn-sm btn-default" style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;margin-right: 5px;"><span class="glyphicon glyphicon-pencil"></span></a>
                                    <a href="myhealth/delete_health/' . $award->id . '/' . $id . '" class="tabledit-delete-button btn btn-sm btn-default" style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>';
                                    }
                                } else
                                //echo $allergies;
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="condition condition-clear">
                        <div class="row">
                            <div class="col-md-2 col-md-offset-5">
                                <button class="add">Add</button>
                            </div>
                        </div>
                    </div>

                    <div class="condition condition-form">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="exampleInput">Title</label>
                                    <input class="form-control doctor_award_title" placeholder="Title"
                                           value="" type="text">
                                </fieldset>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="exampleInput">Organization</label>
                                    <input class="form-control doctor_award_organization" placeholder="Organization"
                                           value="" type="text">
                                </fieldset>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="exampleInput">Year</label>
                                    <select class="select2 form-control doctor_award_year">
                                        <?php $i = 0;
                                        $year = 1950;
                                        for ($i = 0; $i < 66; $i++) {
                                            $year += 1;
                                            ?>
                                            <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                            <?php
                                        }
                                        ?>

                                    </select>
                                </fieldset>
                            </div>
                            <center>
                                <input type="hidden" class="condition_type_medication" value="medication">
                                <button type="button" class="btn btn-primary btn-inline add_doctor_award">Save
                                </button>
                                <button type="button" class="btn btn-primary-outline btn-inline cancel">Cancel</button>
                            </center>
                        </div>
                    </div>
                    <div class="overlay-edit">
                        <div class="container">
                            <div class="condition condition-edit">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold" for="exampleInput">Title</label>
                                            <input class="form-control doctor_award_title" placeholder="Title"
                                                   value="" type="text">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold" for="exampleInput">Organization</label>
                                            <input class="form-control doctor_award_organization"
                                                   placeholder="Organization"
                                                   value="" type="text">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold" for="exampleInput">Year</label>
                                            <select class="select2 form-control doctor_award_year">
                                                <?php $i = 0;
                                                $year = 1950;
                                                for ($i = 0; $i < 66; $i++) {
                                                    $year += 1;
                                                    ?>
                                                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                    <?php
                                                }
                                                ?>

                                            </select>
                                        </fieldset>
                                    </div>
                                    <center>
                                        <input type="hidden" class="condition_type_medication" value="medication">
                                        <button type="button" class="btn btn-primary btn-inline add_doctor_award">Save
                                        </button>
                                        <button type="button" class="btn btn-primary-outline btn-inline cancel">Cancel
                                        </button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!--.tab-pane-->

                <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-5">
                    <div class="condition condition-clear"><!--added class 'condition-clear'-->
                        <div class="row">
                            <table class="table table-hover membership_table">
                                <thead>
                                <tr>
                                    <th>Post</th>
                                    <th>Organization/Society</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                //print_r($condition);
                                //$id = $this->session->userdata('userid');
                                if (is_array($memberships)) {
                                    foreach ($memberships as $membership) {
                                        echo '<tr>
                                <td>
                                    ' . $membership->membership_post . '
                                </td>
                                <td>
                                    ' . $membership->society_name . '
                                </td>
                                <td class="table-photo">
                                    <a class="tabledit-edit-button btn btn-sm btn-default" style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;margin-right: 5px;"><span class="glyphicon glyphicon-pencil"></span></a>
                                    <a href="myhealth/delete_health/' . $membership->membership_id . '/' . $id . '" class="tabledit-delete-button btn btn-sm btn-default" style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>';
                                    }
                                } else
                                //echo $allergies;
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="condition condition-clear">
                        <div class="row">
                            <div class="col-md-2 col-md-offset-5">
                                <button class="add">Add</button>
                            </div>
                        </div>
                    </div>

                    <div class="condition condition-form">
                        <div class="row">
                            <div class="col-lg-8 col-md-8">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="exampleInput">Title</label>
                                    <input class="form-control doctor_membership_title" placeholder="Title"
                                           value="" type="text">
                                </fieldset>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="exampleInput">Organization</label>
                                    <input class="form-control doctor_membership_organization"
                                           placeholder="Organization"
                                           value="" type="text">
                                </fieldset>
                            </div>
                            <center>
                                <input type="hidden" class="condition_type_medication" value="medication">
                                <button type="button" class="btn btn-primary btn-inline add_doctor_membership">Save
                                </button>
                                <button type="button" class="btn btn-primary-outline btn-inline cancel">Cancel</button>
                            </center>
                        </div>
                    </div>
                    <div class="overlay-edit">
                        <div class="container">
                            <div class="condition condtion-edit">
                                <div class="row">
                                    <div class="col-lg-8 col-md-8">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold" for="exampleInput">Title</label>
                                            <input class="form-control doctor_membership_title" placeholder="Title"
                                                   value="" type="text">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold" for="exampleInput">Organization</label>
                                            <input class="form-control doctor_membership_organization"
                                                   placeholder="Organization"
                                                   value="" type="text">
                                        </fieldset>
                                    </div>
                                    <center>
                                        <input type="hidden" class="condition_type_medication" value="medication">
                                        <button type="button" class="btn btn-primary btn-inline add_doctor_membership">
                                            Save
                                        </button>
                                        <button type="button" class="btn btn-primary-outline btn-inline cancel">Cancel
                                        </button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!--.tab-pane-->
                <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-6">
                    <form class="doctor_licenced_state">
                        <?php
                        $us_state_names = array(
                            'ALABAMA' => 'ALABAMA', 'ALASKA' => 'ALASKA', 'ARIZONA' => 'ARIZONA', 'ARKANSAS' => 'ARKANSAS', 'CALIFORNIA' => 'CALIFORNIA', 'COLORADO' => 'COLORADO', 'CONNECTICUT' => 'CONNECTICUT', 'DELAWARE' => 'DELAWARE', 'FLORIDA' => 'FLORIDA', 'GEORGIA' => 'GEORGIA', 'HAWAII' => 'HAWAII', 'IDAHO' => 'IDAHO', 'ILLINOIS' => 'ILLINOIS', 'INDIANA' => 'INDIANA', 'IOWA' => 'IOWA', 'KANSAS' => 'KANSAS', 'KENTUCKY' => 'KENTUCKY', 'LOUISIANA' => 'LOUISIANA', 'MAINE' => 'MAINE', 'MARYLAND' => 'MARYLAND', 'MASSACHUSETTS' => 'MASSACHUSETTS', 'MICHIGAN' => 'MICHIGAN', 'MINNESOTA' => 'MINNESOTA', 'MISSISSIPPI' => 'MISSISSIPPI', 'MISSOURI' => 'MISSOURI', 'MONTANA' => 'MONTANA', 'NEBRASKA' => 'NEBRASKA', 'NEVADA' => 'NEVADA', 'NEW HAMPSHIRE' => 'NEW HAMPSHIRE', 'NEW JERSEY' => 'NEW JERSEY', 'NEW MEXICO' => 'NEW MEXICO', 'NEW YORK' => 'NEW YORK', 'NORTH CAROLINA' => 'NORTH CAROLINA', 'NORTH DAKOTA' => 'NORTH DAKOTA', 'OHIO' => 'OHIO', 'OKLAHOMA' => 'OKLAHOMA', 'OREGON' => 'OREGON', 'PENNSYLVANIA' => 'PENNSYLVANIA', 'RHODE ISLAND' => 'RHODE ISLAND', 'SOUTH CAROLINA' => 'SOUTH CAROLINA', 'SOUTH DAKOTA' => 'SOUTH DAKOTA', 'TENNESSEE' => 'TENNESSEE', 'TEXAS' => 'TEXAS', 'UTAH' => 'UTAH', 'VERMONT' => 'VERMONT', 'VIRGINIA' => 'VIRGINIA', 'WASHINGTON' => 'WASHINGTON', 'WEST VIRGINIA' => 'WEST VIRGINIA', 'WISCONSIN' => 'WISCONSIN', 'WYOMING' => 'WYOMING'
                        );
                        ?>
                        <div class="row">
                            <?php
                            function object_to_array($data)
                            {
                                if (is_array($data) || is_object($data)) {
                                    $result = array();
                                    foreach ($data as $key => $value) {
                                        $result[$key] = object_to_array($value);
                                    }
                                    return $result;
                                }
                                return $data;
                            }

                            $licened_sts = object_to_array($licenced_states);
                            //print_r($licened_sts);
                            //print_r($licenced_states);

                            $d = 0;
                            foreach ($us_state_names as $key_state => $value_state) {
                                if (is_array($licened_sts)) {
                                    for ($i = 0; $i < count($licened_sts); $i++) {
                                        if ($licened_sts[$i]['licenced_state_name'] == $value_state) {
                                            //echo "Halum".$value_state."<br>";
                                            $d = 1;
                                            break;
                                        } else {
                                            $d = 0;
                                        }
                                    }
                                } else {
                                    $d = 0;
                                }

                                if ($d) {
                                    ?>

                                    <div class="col-md-3 col-lg-3">
                                        <input checked name="state[]" type="checkbox"
                                               value="<?php echo $value_state; ?>" class="doctor_licenced_state">
                                        <span><?php echo $value_state; ?></span>
                                    </div>
                                    <?php
//                                echo 'Halum'.$value_state."<br>";

                                } else {
//                                echo $value_state."<br>";
                                    ?>

                                    <div class="col-md-3 col-lg-3">
                                        <input name="state[]" type="checkbox" value="<?php echo $value_state; ?>"
                                               class="doctor_licenced_state">
                                        <span><?php echo $value_state; ?></span>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <center>
                                    <input type="hidden" class="condition_type_medication" value="medication">
                                    <button type="button" class="btn btn-primary btn-inline add_doctor_licenced_state">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-primary-outline btn-inline cancel">Cancel
                                    </button>
                                </center>
                            </div>
                        </div>
                    </form>
                </div><!--.tab-pane-->
                <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-7">
                    <?php
                    $languages_array = array('aa' => 'Afar', 'ab' => 'Abkhaz', 'ae' => 'Avestan', 'af' => 'Afrikaans', 'ak' => 'Akan', 'am' => 'Amharic', 'an' => 'Aragonese', 'ar' => 'Arabic', 'as' => 'Assamese', 'av' => 'Avaric', 'ay' => 'Aymara', 'az' => 'Azerbaijani', 'ba' => 'Bashkir', 'be' => 'Belarusian', 'bg' => 'Bulgarian', 'bh' => 'Bihari', 'bi' => 'Bislama', 'bm' => 'Bambara', 'bn' => 'Bengali', 'bo' => 'Tibetan', 'br' => 'Breton', 'bs' => 'Bosnian', 'ca' => 'Catalan; Valencian', 'ce' => 'Chechen', 'ch' => 'Chamorro', 'co' => 'Corsican', 'cr' => 'Cree', 'cs' => 'Czech', 'cu' => 'Old Church Slavonic', 'cv' => 'Chuvash', 'cy' => 'Welsh', 'da' => 'Danish', 'de' => 'German', 'dv' => 'Maldivian;', 'dz' => 'Dzongkha', 'ee' => 'Ewe', 'el' => 'Greek', 'en' => 'English', 'eo' => 'Esperanto', 'es' => 'Spanish', 'et' => 'Estonian', 'eu' => 'Basque', 'fa' => 'Persian', 'ff' => 'Pular', 'fi' => 'Finnish', 'fj' => 'Fijian', 'fo' => 'Faroese', 'fr' => 'French', 'fy' => 'Western Frisian', 'ga' => 'Irish', 'gd' => 'Scottish Gaelic', 'gl' => 'Galician', 'gn' => 'GuaranÃ­', 'gu' => 'Gujarati', 'gv' => 'Manx', 'ha' => 'Hausa', 'he' => 'Hebrew', 'hi' => 'Hindi', 'ho' => 'Hiri Motu', 'hr' => 'Croatian', 'ht' => 'Haitian', 'hu' => 'Hungarian', 'hy' => 'Armenian', 'hz' => 'Herero', 'ia' => 'Interlingua', 'id' => 'Indonesian', 'ie' => 'Interlingue', 'ig' => 'Igbo', 'ii' => 'Nuosu', 'ik' => 'Inupiaq', 'io' => 'Ido', 'is' => 'Icelandic', 'it' => 'Italian', 'iu' => 'Inuktitut', 'ja' => 'Japanese', 'jv' => 'Javanese', 'ka' => 'Georgian', 'kg' => 'Kongo', 'ki' => 'Kikuyu, Gikuyu', 'kj' => 'Kwanyama, Kuanyama', 'kk' => 'Kazakh', 'kl' => 'Greenlandic', 'km' => 'Khmer', 'kn' => 'Kannada', 'ko' => 'Korean', 'kr' => 'Kanuri', 'ks' => 'Kashmiri', 'ku' => 'Kurdish', 'kv' => 'Komi', 'kw' => 'Cornish', 'ky' => 'Kirghiz, Kyrgyz', 'la' => 'Latin', 'lb' => 'Luxembourgish', 'lg' => 'Luganda', 'li' => 'Limburgish', 'ln' => 'Lingala', 'lo' => 'Lao', 'lt' => 'Lithuanian', 'lu' => 'Luba-Katanga', 'lv' => 'Latvian', 'mg' => 'Malagasy', 'mh' => 'Marshallese', 'mi' => 'Maori', 'mk' => 'Macedonian', 'ml' => 'Malayalam', 'mn' => 'Mongolian', 'mr' => 'Marathi', 'ms' => 'Malay', 'mt' => 'Maltese', 'my' => 'Burmese', 'na' => 'Nauru', 'nb' => 'Norwegian BokmÃ¥l', 'nd' => 'North Ndebele', 'ne' => 'Nepali', 'ng' => 'Ndonga', 'nl' => 'Dutch', 'nn' => 'Norwegian Nynorsk', 'no' => 'Norwegian', 'nr' => 'South Ndebele', 'nv' => 'Navajo', 'ny' => 'Chichewa', 'oc' => 'Occitan', 'oj' => 'Ojibwe', 'om' => 'Oromo', 'or' => 'Oriya', 'os' => 'Ossetian, Ossetic', 'pa' => 'Punjabi', 'pi' => 'Pali', 'pl' => 'Polish', 'ps' => 'Pashto', 'pt' => 'Portuguese', 'qu' => 'Quechua', 'rm' => 'Romansh', 'rn' => 'Kirundi', 'ro' => 'Romanian', 'ru' => 'Russian', 'rw' => 'Kinyarwanda', 'sa' => 'Sanskrit (Sa?sk?ta)', 'sc' => 'Sardinian', 'sd' => 'Sindhi', 'se' => 'Northern Sami', 'sg' => 'Sango', 'si' => 'Sinhala, Sinhalese', 'sk' => 'Slovak', 'sl' => 'Slovene', 'sm' => 'Samoan', 'sn' => 'Shona', 'so' => 'Somali', 'sq' => 'Albanian', 'sr' => 'Serbian', 'ss' => 'Swati', 'st' => 'Southern Sotho', 'su' => 'Sundanese', 'sv' => 'Swedish', 'sw' => 'Swahili', 'ta' => 'Tamil', 'te' => 'Telugu', 'tg' => 'Tajik', 'th' => 'Thai', 'ti' => 'Tigrinya', 'tk' => 'Turkmen', 'tl' => 'Tagalog', 'tn' => 'Tswana', 'to' => 'Tonga (Tonga Islands)', 'tr' => 'Turkish', 'ts' => 'Tsonga', 'tt' => 'Tatar', 'tw' => 'Twi', 'ty' => 'Tahitian', 'ug' => 'Uighur, Uyghur', 'uk' => 'Ukrainian', 'ur' => 'Urdu', 'uz' => 'Uzbek', 've' => 'Venda', 'vi' => 'Vietnamese', 'vo' => 'VolapÃ¼k', 'wa' => 'Walloon', 'wo' => 'Wolof', 'xh' => 'Xhosa', 'yi' => 'Yiddish', 'yo' => 'Yoruba', 'za' => 'Zhuang, Chuang', 'zh' => 'Chinese', 'zu' => 'Zulu',
                    );
                    ?>
                    <div class="row">
                        <?php
                        function object_to_array_language($data)
                        {
                            if (is_array($data) || is_object($data)) {
                                $result = array();
                                foreach ($data as $key => $value) {
                                    $result[$key] = object_to_array($value);
                                }
                                return $result;
                            }
                            return $data;
                        }

                        //$licened_sts = object_to_array($licenced_states);
                        $spoken_languages = object_to_array_language($languages);
                        //print_r($licened_sts);
                        //print_r($licenced_states);

                        $d = 0;

                        foreach ($languages_array as $key_language => $value_language) {
                            if (is_array($spoken_languages)) {
                                for ($i = 0; $i < count($spoken_languages); $i++) {
                                    if ($spoken_languages[$i]['language'] == $value_language) {
                                        //echo "Halum".$value_state."<br>";
                                        $d = 1;
                                        break;
                                    } else {
                                        $d = 0;
                                    }
                                }
                            } else {
                                $d = 0;
                            }
                            if ($d) {
                                ?>

                                <div class="col-md-3 col-lg-3">
                                    <input checked name="language[]" type="checkbox">
                                    <span><?php echo $value_language; ?></span>
                                </div>
                                <?php
//                                echo 'Halum'.$value_state."<br>";
                            } else {
//                                echo $value_state."<br>";
                                ?>

                                <div class="col-md-3 col-lg-3">
                                    <input name="language[]" type="checkbox">
                                    <span><?php echo $value_language; ?></span>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <center>
                                <input type="hidden" class="condition_type_medication" value="medication">
                                <button type="button" class="btn btn-primary btn-inline add_doctor_licenced_state">Save
                                </button>
                                <button type="button" class="btn btn-primary-outline btn-inline cancel">Cancel</button>
                            </center>
                        </div>
                    </div>
                </div><!--.tab-pane-->
            </div><!--.tab-content-->
        </section><!--.tabs-section-->
    </div>
</div>

<script type="text/javascript">
    var rowEditButtons = document.querySelectorAll('.condition-clear .tabledit-edit-button');
    var addButtons = document.querySelectorAll('.condition-clear .add');
    var cancelButtons = document.querySelectorAll('.condition-form .cancel');
    var overlayCancelButtons = document.querySelectorAll('.overlay-edit .cancel');
    var addDcButtons = document.querySelectorAll(".condition-form button.btn.btn-primary.btn-inline:not(.cancel)");

    function checkParentClass(clsName, childObj) {
        while (!childObj.parentNode.classList.contains(clsName)) {
            childObj = childObj.parentNode;
        }
        return childObj.parentNode;
    }

    for (i = 0; i < rowEditButtons.length; i++) {
        rowEditButtons[i].addEventListener('click', function () {
            var curOverlay = checkParentClass('tab-pane', this).querySelector('.overlay-edit');
            var formCols = curOverlay.querySelector('.row').children;
            var curTR = this;
            var tabHeads = document.querySelectorAll('.page-doctor-info .nav-link');

            while (curTR.parentNode.tagName !== "TR") {
                curTR = curTR.parentNode;
            }
            var curTD = curTR.parentNode.children;

            for (i = 0; i < curTD.length - 1; i++) {
                if (formCols[i].querySelector('input')) {
                    formCols[i].querySelector('input').value = curTD[i].innerHTML.trim();
                } else if (formCols[i].querySelector('select.select2')) {
                    if (tabHeads[3].classList.contains('active')) {
                        $('select.doctor_award_year').val(curTD[i].innerHTML.trim()).change();
                    } else if (tabHeads[2].classList.contains('active')) {
                        var curTDranged = curTD[i].innerHTML.trim();
                        console.log(curTDranged);
                        $('select.doctor_education_year_start').val(curTDranged.substr(0, curTDranged.indexOf(' - '))).change();
                        $('select.doctor_education_year_end').val(curTDranged.substr(curTDranged.indexOf(' - ') + 3)).change();
                    }
                } else {
                    console.log(false);
                }
            }
            curOverlay.style.display = 'block';
        }, false)
    }
    for (i = 0; i < overlayCancelButtons.length; i++) {
        overlayCancelButtons[i].addEventListener('click', function () {
            checkParentClass('overlay-edit', this).removeAttribute('style');
        }, false)
    }

    for (i = 0; i < addButtons.length; i++) {
        addButtons[i].addEventListener('click', function () {
            this.style.display = 'none';
            this.parentNode.parentNode.parentNode.nextElementSibling.style.display = 'block';
        }, false)
    }
    for (i = 0; i < cancelButtons.length; i++) {
        cancelButtons[i].addEventListener('click', function () {
            this.parentNode.parentNode.parentNode.parentNode.querySelector('.add').removeAttribute('style');
            this.parentNode.parentNode.parentNode.removeAttribute('style');
        }, false)
    }
</script>