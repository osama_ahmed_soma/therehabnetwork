<section class="content-header">
    <h1>
        All Timezones
        <a href="#" class="btn btn-primary create_model_btn" data-toggle="modal" data-target="#create_modal">Add
            Timezone</a>
		<?php echo form_open('', ['class' => 'form-inline search_form', 'style' => 'display: inline;']); ?>
            <fieldset class="form-group">
                <input type="text" name="search" class="form-control" placeholder="Variable or Name">
            </fieldset>
            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
        </form>
        <a href="#" class="btn btn-primary reset_form" title="Refresh Data"><span class="glyphicon glyphicon-refresh"></span></a>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li class="active">Timezones</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover tablesorter customTableSort">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Variable</th>
                                    <th>Name</th>
                                    <th>Order By</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody class="timezones_tbody">
                                <?php
                                if (!empty($timezones)): ?>
                                    <?php foreach ($timezones as $timezone): ?>
                                        <tr>
                                            <td><?php echo $timezone->id; ?></td>
                                            <td><?php echo $timezone->var; ?></td>
                                            <td><?php echo $timezone->name; ?></td>
                                            <td><?php echo $timezone->order_by; ?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-sm btn-primary timezone_edit" title="Edit"
                                                       data-toggle="modal" data-target="#edit_modal"
                                                       data-id="<?php echo $timezone->id; ?>"
                                                       data-var="<?php echo $timezone->var; ?>"
                                                       data-name="<?php echo $timezone->name; ?>"
                                                       data-order_by="<?php echo $timezone->order_by; ?>"
                                                       href="">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a class="delete btn btn-sm btn-danger timezone_remove"
                                                       data-id="<?php echo $timezone->id; ?>" title="Delete"
                                                       data-name="<?php echo $timezone->name; ?>">
                                                        <i class="fa fa-remove"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="7">There is no Timezone.</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Variable</th>
                                    <th>Name</th>
                                    <th>Order By</th>
                                    <th>Action(s)</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<?php echo form_open('', ['class' => 'create_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Timezone</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Variable <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="var"
                                       value="<?php echo set_value('var', ''); ?>" placeholder="Variable">
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="name"
                                       value="<?php echo set_value('name', ''); ?>" placeholder="Name">
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Order By <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" name="order_by" placeholder="Order By">
                            </fieldset>
                        </div>
                        <input type="hidden" value="1" name="save">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<?php echo form_open('', ['class' => 'edit_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Timezone</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Edit Variable <span
                                        class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="e_var" placeholder="Edit Variable">
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Edit Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="e_name" placeholder="Edit Name" required>
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Edit Order By <span
                                        class="text-danger">*</span></label>
                                <input type="number" class="form-control" name="e_order_by" placeholder="Edit Order By">
                            </fieldset>
                        </div>
                        <input type="hidden" name="e_id">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function generate(data){
        if (data.length == 0) {
            return;
        }
        var html = '';
        for (var i = 0; i < data.length; i++) {
            var temp_html = '';
            temp_html += '<tr>' +
                '<td>' + data[i].id + '</td>' +
                '<td>' + data[i].var + '</td>' +
                '<td>' + data[i].name + '</td>' +
                '<td>' + data[i].order_by + '</td>' +
                '<td>' +
                '<div class="btn-group">' +
                '<a class="btn btn-sm btn-primary timezone_edit" title="Edit" ' +
                'data-toggle="modal" data-target="#edit_modal" ' +
                'data-id="' + data[i].id + '" ' +
                'data-var="' + data[i].var + '" ' +
                'data-name="' + data[i].name + '" ' +
                'data-order_by="' + data[i].order_by + '" ' +
                'href="">' +
                '<i class="fa fa-edit"></i>' +
                '</a>' +
                '<a class="delete btn btn-sm btn-danger timezone_remove" ' +
                'data-id="' + data[i].id + '" title="Delete" ' +
                'data-name="' + data[i].name + '" >' +
                '<i class="fa fa-remove"></i>' +
                '</a>' +
                '</div>' +
                '</td>' +
                '</tr>';
            html += temp_html;
        }
        $('.timezones_tbody').html(html);
        $('.customTableSort').trigger('update');
    }
    function re_render(auto_data) {
        if(auto_data != null){
            generate(auto_data);
            return;
        }
        $.ajax({
            url: '<?php echo base_url(); ?>admin/timezones/index',
            data: {
                'is_request': 'yes'
            },
            method: "POST",
            dataType: "json"
        }).success(function (data) {
            generate(data);
        });
    }
    $(document).ready(function () {
        $(document).on('submit', '.create_form', function (e) {
            e.preventDefault();
            var self = this;
            var variable = $('[name="var"]').val();
            var name = $('[name="name"]').val();
            var order_by = $('[name="order_by"]').val();
            if (variable == '' || name == '') {
                notyError('All fields are required.');
                return;
            }
            $.ajax({
                url: '<?php echo base_url(); ?>admin/timezones/add',
                data: {
                    'var': variable,
                    'name': name,
                    'order_by': order_by
                },
                method: "POST",
                dataType: "json"
            }).success(function (data) {
                if (!data.bool) {
                    notyError(data.message);
                    return;
                }
                re_render(null);
                notySuccess(data.message);
                $('#create_modal').modal('hide');
                self.reset();
            });
            return false;
        });
        $(document).on('submit', '.edit_form', function (e) {
            e.preventDefault();
            var self = this;
            var id = $('[name="e_id"]').val();
            var variable = $('[name="e_var"]').val();
            var name = $('[name="e_name"]').val();
            var order_by = $('[name="e_order_by"]').val();
            if (variable == '' || name == '' || id == '') {
                notyError('All fields are required.');
                return;
            }
            $.ajax({
                url: '<?php echo base_url(); ?>admin/timezones/update',
                data: {
                    'id': id,
                    'var': variable,
                    'name': name,
                    'order_by': order_by
                },
                method: "POST",
                dataType: "json"
            }).success(function (data) {
                if (!data.bool) {
                    notyError(data.message);
                    return;
                }
                re_render(null);
                notySuccess(data.message);
                $('#edit_modal').modal('hide');
                self.reset();
            });
            return false;
        });
        $(document).on('click', '.timezone_edit', function (e) {
            var self = this;
            $('[name="e_var"]').val($(self).attr('data-var'));
            $('[name="e_name"]').val($(self).attr('data-name'));
            $('[name="e_order_by"]').val($(self).attr('data-order_by'));
            $('[name="e_id"]').val($(self).attr('data-id'));
        });
        $(document).on('click', '.timezone_remove', function (e) {
            e.preventDefault();
            var self = this;
            notyMessage('Are you sure that you want to DELETE <strong>' + $(self).attr('data-name') + '</strong>', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            $.ajax({
                                url: '<?php echo base_url(); ?>admin/timezones/remove',
                                data: {
                                    'id': $(self).attr('data-id')
                                },
                                method: "POST",
                                dataType: "json"
                            }).success(function (data) {
                                if (!data.bool) {
                                    notyError(data.message);
                                    return;
                                }
                                re_render(null);
                                notySuccess(data.message);
                            });
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });
        $(document).on('submit', '.search_form', function(e){
            e.preventDefault();
            var self = this;
            var search = $('[name="search"]').val();
            if(search == ''){
                $('[name="search"]').focus();
                notyError('Search is empty.');
                return;
            }
            $.ajax({
                url: '<?php echo base_url(); ?>admin/timezones/search',
                data: {
                    'search': search
                },
                method: "POST",
                dataType: "json"
            }).success(function (data) {
                if (!data.bool) {
                    notyError(data.message);
                    return;
                }
                re_render(data.result);
                notySuccess(data.message);
                self.reset();
            });
            return false;
        });
        $(document).on('click', '.reset_form', function(e){
            e.preventDefault();
            re_render();
            $('.search_form').reset();
            return false;
        });
    });
</script>