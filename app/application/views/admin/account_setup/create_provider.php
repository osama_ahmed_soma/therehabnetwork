<!-- Content Header (Page header) -->
<section class="content-header" xmlns="http://www.w3.org/1999/html">
    <h1>
        Create Provider
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Account Setup</li>
        <li class="active">Create Provider</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php if( isset($messages) && !empty($messages)): foreach( $messages as $message ):?>
                <div class="alert alert-success alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $message; ?>
                </div>
            <?php endforeach; endif; ?>

            <?php if( isset($errors) && !empty($errors)): foreach( $errors as $error ):?>
                <div class="alert alert-danger alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $error; ?>
                </div>
            <?php endforeach; endif; ?>

            <?php echo validation_errors(); ?>
            <div class="box">
                <div class="box-body">
                    <?php echo form_open('admin/account_setup/create_provider'); ?>
                        <div class="row">
                            <fieldset class="form-group col-md-6">
                                <label class="form-label semibold">First Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="fname" value="<?php echo set_value('fname', ''); ?>" placeholder="First Name">
                            </fieldset>

                            <fieldset class="form-group col-md-6">
                                <label class="form-label semibold">Last Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="lname" value="<?php echo set_value('lname', ''); ?>" placeholder="Last Name">
                            </fieldset>
                        </div>

                        <div class="row">
                            <fieldset class="form-group col-md-6">
                                <label class="form-label semibold">Professional Title</label>
                                <select class="form-control select2" name="p_title">
                                    <option value="Mr.">Mr.</option>
                                    <option value="Ms.">Ms.</option>
                                    <option value="Mrs.">Mrs.</option>
                                    <option value="Dr.">Dr.</option>
                                    <option value="D.O.">D.O.</option>
                                    <option value="Ph.D.">Ph.D.</option>
                                </select>
                            </fieldset>

                            <fieldset class="form-group col-md-6">
                                <label class="form-label semibold">Primary Specialty</label>
                                <select class="form-control select2" name="specialty">
                                    <?php foreach ($specialties as $index => $specialty) { ?>
                                        <option value="<?php echo $index.':'.$specialty; ?>"><?php echo $specialty; ?></option>
                                    <?php } ?>
                                    <option value="-1">Other</option>
                                </select>
                                <input type="text" class="form-control" name="otherSpecialty" id="otherSpecialty"
                                       style="display: none" placeholder="Other specialty">
                            </fieldset>
                        </div>

                        <div class="row">
                            <fieldset class="form-group col-md-6">
                                <label class="form-label semibold">Email Address <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email" placeholder="Email">
                            </fieldset>

                            <fieldset class="form-group col-md-6">
                                <label class="form-label semibold">Primary State License</label>
                                <select name="state" class="select2 form-control">
                                    <?php foreach ($states as $index => $name) { ?>
                                        <option value="<?php echo $index.':'.$name; ?>"><?php echo $name; ?></option>
                                    <?php } ?>
                                </select>
                            </fieldset>
                        </div>
                        <input type="hidden" value="1" name="save">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Create Account</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<script>
    jQuery(document).ready(function ($) {

        $('.select2').select2();

        var $form = $("#form-doctor-signup");

        var $language = $form.find('[name=language]');

        $language.on("change", function (e) {

            var values = $(this).val();

            var index = values.indexOf("-1");

            if (index > -1) {

                if (values.length > 1) {

                    values.splice(index, 1);

                    $(this).val(values).trigger("change");

                }


                $("#otherLanguage").show().focus();


            }

        });


        var $specialty = $form.find('[name=specialty]');

        $specialty.on("change", function (e) {

            var values = $(this).val();

            var index = values.indexOf("-1");

            if (index > -1) {


                $("#otherSpecialty").show().focus();


            }

        });


        $form.submit(function () {


            $form.find('.form-group').removeClass('has-danger');


            var $firstName = $form.find('[name=fname]');

            var $lastName = $form.find('[name=lname]');

            var $p_title = $form.find('[name=p_title]');

            var $email = $form.find('[name=email]');

//            var $practicing = $form.find('[name=practicing]');

            var $npi = $form.find('[name=npi]');

            var $stateId = $form.find('[name=state]');


            if ($firstName.val() == "") {

                $firstName.parent().addClass('has-danger');

                notyError("First name is empty");


                return false;

            }


            if ($lastName.val() == "") {

                $lastName.parent().addClass('has-danger');

                notyError("Last name is empty");


                return false;

            }


            if ($email.val() == "") {

                $email.parent().addClass('has-danger');

                notyError("Email is empty");


                return false;

            }



            if ($stateId.val() == "") {

                $stateId.parent().addClass('has-danger');

                notyError("Primary State License is empty");


                return false;

            }

        });

    });
</script>