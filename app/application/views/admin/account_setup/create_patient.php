<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Create Patient
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Account Setup</li>
        <li class="active">Create Patient</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">

            <?php if( isset($messages) && !empty($messages)): foreach( $messages as $message ):?>
                <div class="alert alert-success alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $message; ?>
                </div>
            <?php endforeach; endif; ?>

            <?php if( isset($errors) && !empty($errors)): foreach( $errors as $error ):?>
                <div class="alert alert-danger alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $error; ?>
                </div>
            <?php endforeach; endif; ?>

            <?php echo validation_errors(); ?>

            <?php echo form_open('admin/account_setup/create_patient'); ?>
                <div class="row">
                    <div class="col-md-4">
                        <fieldset class="form-group">
                            <label class="form-label semibold">First Name</label>
                            <input type="text" class="form-control" name="user_firstname" value="<?php echo set_value('user_firstname', ''); ?>"
                                   placeholder="First Name">
                        </fieldset>
                    </div>
                    <div class="col-md-4">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Last Name</label>
                            <input type="text" class="form-control" name="user_lastname" value="<?php echo set_value('user_lastname', ''); ?>"
                                   placeholder="Last Name">
                        </fieldset>
                    </div>
                    <div class="col-md-4">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Email Address</label>
                            <input type="text" class="form-control" name="user_email" value="<?php echo set_value('user_email', ''); ?>"
                                   placeholder="Email Address">
                        </fieldset>
                    </div>
                    <div class="col-md-6 col-md-offset-3">
                        <input type="hidden" value="1" name="save">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Create Account</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
