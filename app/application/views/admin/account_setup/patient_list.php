<?php
/**
 * Created by PhpStorm.
 * User: osama
 * Date: 9/8/2016
 * Time: 4:50 AM
 */
?>
<section class="content-header">
    <h1>
        All Patients
        <a href="#" class="btn btn-primary create_model_btn" data-toggle="modal" data-target="#create_modal">Create Patient</a>
		<?php echo form_open('', ['class' => 'form-inline search_form', 'style' => 'display: inline;']); ?>
            <fieldset class="form-group">
                <input type="text" name="search" class="form-control" placeholder="Name, Email or Phone">
            </fieldset>
            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
        </form>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Account Setup</li>
        <li class="active">Patient List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table id="doctors_all" class="table table-bordered table-hover tablesorter customTableSort">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone (Mobile)</th>
                            <th>Primary Physician</th>
                            <th>Signup Date</th>
                            <th>Status</th>
                            <th>Last Login</th>
                            <th>Action(s)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
						if(!empty($patients)): ?>
                            <?php foreach($patients as $patient): ?>
                                <tr>
                                    <td><?php echo ($patient->firstname || $patient->lastname) ? $patient->firstname.' '.$patient->lastname : 'N/A'; ?></td>
                                    <td><?php echo $patient->user_email; ?></td>
                                    <td><?php echo ($patient->phone_mobile) ? $patient->phone_mobile : 'N/A'; ?></td>
                                    <td><?php echo (($patient->primary_physician) ? '<a href="" data-toggle="modal" data-target="#primary_physician_detail" data-first_name="'.$patient->primary_physician->firstname.'" data-last_name="'.$patient->primary_physician->lastname.'" data-email="'.$patient->primary_physician->user_email.'" data-phone_number="'.$patient->primary_physician->phone_mobile.'" data-city="'.$patient->primary_physician->city.'" data-state="'.$patient->primary_physician->state_name.'" data-zip="'.$patient->primary_physician->zip.'" data-is_joined="'.(($patient->primary_physician->is_joined == 1) ? 'Yes' : 'No').'" class="primary_physician_modal">'.$patient->primary_physician->firstname.' '.$patient->primary_physician->lastname.'</a>' : 'None'); ?></td>
                                    <td><?php echo date('m/d/Y', strtotime($patient->created_at)); ?></td>
                                    <td class="is_active"><?php echo ($patient->active == 1) ? 'Active' : 'Inactive' ; ?></td>
                                    <td><?php echo ($patient->is_joined == 1) ? date('m/d/Y h:i A', strtotime($patient->login_date_time)) : 'Never'; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-primary login_as_user" title="Login"
                                               data-userid="<?php echo $patient->user_id; ?>"
                                               href="">
                                                <i class="fa fa-sign-in"></i>
                                            </a>
                                            <?php if($patient->active == 1): ?>
                                                <a class="btn btn-sm btn-danger patient_deactivate" data-userid="<?php echo $patient->user_id; ?>" title="Deactivate" href="#">
                                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                                </a>
                                            <?php else: ?>
                                                <a class="btn btn-sm btn-success patient_activate" data-userid="<?php echo $patient->user_id; ?>" title="Activate" href="#">
                                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                                </a>
                                            <?php endif; ?>
                                            <a class="btn btn-sm btn-primary" title="Edit" href="index.php?do=doctors&amp;page=new&amp;act=edit&amp;actId=37">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="delete btn btn-sm btn-danger patient_remove" data-name="<?php echo $patient->firstname; ?> <?php echo $patient->lastname; ?>" data-userid="<?php echo $patient->user_id; ?>" title="Delete">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone (Mobile)</th>
                            <th>Primary Physician</th>
                            <th>Signup Date</th>
                            <th>Status</th>
                            <th>Last Login</th>
                            <th>Action(s)</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <?php echo (isset($pagination_link)) ? $pagination_link : '' ; ?>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
			<?php echo form_open('', ['class' => 'create_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Patient</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <fieldset class="form-group">
                                <label class="form-label semibold">First Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="fname" value="" placeholder="First Name">
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Last Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="lname" value="" placeholder="Last Name">
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Email Address <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="email" placeholder="Email">
                            </fieldset>
                        </div>
                        <input type="hidden" value="1" name="save">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create Account</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="primary_physician_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Primary Physician</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 "><strong>First Name: </strong><span class="first_name"></span></div>
                    <div class="col-md-6 "><strong>Last Name: </strong><span class="last_name"></span></div>
                    <div class="col-md-6 "><strong>Email: </strong><span class="email"></span></div>
                    <div class="col-md-6 "><strong>Phone Number: </strong><span class="phone_number"></span></div>
                    <div class="col-md-6 "><strong>City: </strong><span class="city"></span></div>
                    <div class="col-md-6 "><strong>State: </strong><span class="state"></span></div>
                    <div class="col-md-6"><strong>Zip: </strong><span class="zip"></span></div>
                    <div class="col-md-6"><strong>Is Joined: </strong><span class="is_joined"></span></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function removeProvider(id, self){
        var str = '<?php echo base_url(); ?>admin/account_setup/remove_patient';
        $.ajax({
            url: str,
            data: {userid: id},
            method: "POST"
//            dataType: "json"
        }).error(function () {

        }).done(function () {

        }).success(function (data) {
            self.parent().parent().parent().remove();
            notySuccess('Patient Deleted Successfully.');
        });
    }
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    $(document).ready(function(){
        $(document).on('click', '.primary_physician_modal', function(e){
            var first_name = $(this).attr('data-first_name');
            var last_name = $(this).attr('data-last_name');
            var email = $(this).attr('data-email');
            var phone_number = $(this).attr('data-phone_number');
            var city = $(this).attr('data-city');
            var state = $(this).attr('data-state');
            var zip = $(this).attr('data-zip');
            var is_joined = $(this).attr('data-is_joined');
            var primary_physician_detail = $('#primary_physician_detail');
            $('#primary_physician_detail .first_name').text(first_name);
            $('#primary_physician_detail .last_name').text(last_name);
            $('#primary_physician_detail .email').text(email);
            $('#primary_physician_detail .phone_number').text(phone_number);
            $('#primary_physician_detail .city').text(city);
            $('#primary_physician_detail .state').text(state);
            $('#primary_physician_detail .zip').text(zip);
            $('#primary_physician_detail .is_joined').text(is_joined);
        });
        $('.search_form').submit(function(e){
            e.preventDefault();
            var self = $(this);
            $('input[name="search"]').parent().removeClass('has-error');
            var search = $('input[name="search"]').val();
            if(search == ''){
                notyError("Please fill search field first.");
                $('input[name="search"]').parent().addClass('has-error');
                return;
            }
            $.ajax({
                url: '<?php echo base_url(); ?>admin/account_setup/search_patient',
                data: self.serialize(),
                method: "POST",
                dataType: "json"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                if(data.bool){
                    $('input[name="search"]').val('');
                    notySuccess(data.patients.length+" patient found.");
                    $('.pagination').hide();
                    var html = '';
                    for(var i = 0; i < data.patients.length; i++){
                        html += '<tr>' +
                            '<td>'+ data.patients[i].full_name +'</td>' +
                            '<td>'+ data.patients[i].user_email +'</td>' +
                            '<td>'+ data.patients[i].phone_mobile +'</td>' +
                            '<td>'+ data.patients[i].primary_physician.full_name +'</td>' +
                            '<td>'+ data.patients[i].created_at +'</td>' +
                            '<td class="is_active">';
                            if(data.patients[i].active == 1){
                                html += 'Active';
                            } else {
                                html += 'Inactive';
                            }
                            html += '</td>' +
                            '<td>'+ data.patients[i].login_date_time +'</td>' +
                            '<td>' +
                                '<div class="btn-group">' +
                                    '<a class="btn btn-sm btn-primary login_as_user" title="Login"' +
                                        'data-userid="'+ data.patients[i].user_id +'"' +
                                        'href="">' +
                                        '<i class="fa fa-sign-in"></i>' +
                                    '</a>';
                                    if(data.patients[i].active == 1){
                                        html += '<a class="btn btn-sm btn-danger patient_deactivate"' +
                                            'data-userid="'+ data.patients[i].user_id +'"' +
                                            'title="Deactivate" href="#">' +
                                            '<i class="fa fa-refresh" aria-hidden="true"></i>' +
                                        '</a>';
                                    } else {
                                        html += '<a class="btn btn-sm btn-success patient_activate"' +
                                            'data-userid="'+ data.patients[i].user_id +'"' +
                                            'title="Activate" href="#">' +
                                            '<i class="fa fa-refresh" aria-hidden="true"></i>' +
                                        '</a>';
                                    }
                                    html += '<a class="btn btn-sm btn-primary" title="Edit"' +
                                        'href="">' +
                                        '<i class="fa fa-edit"></i>' +
                                    '</a>' +
                                    '<a class="delete btn btn-sm btn-danger patient_remove"' +
                                        'data-name="'+ data.patients[i].firstname +' '+ data.patients[i].lastname +'"' +
                                        'data-userid="'+ data.patients[i].user_id +'" title="Delete">' +
                                        '<i class="fa fa-remove"></i>' +
                                    '</a>' +
                                '</div>' +
                            '</td>' +
                        '</tr>';
                    }
                    $('#doctors_all tbody').html(html);
                    $('.customTableSort').trigger('update');
                } else {
                    notyError("No patient found.");
                }
            });
        });
        $(document).on('click', '.login_as_user', function(e){
            e.preventDefault();
            var self = $(this);
            $.ajax({
                url: '<?php echo base_url(); ?>login/login_as_user/' + self.attr('data-userid') ,
                method: "GET"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
				data = JSON.parse(data);
				if (data.redirect !== '') {
					notySuccess(data.msg);
					redirect = data.redirect;
					setTimeout(function(){
						var newWin = window.open(redirect);             
						if(!newWin || newWin.closed || typeof newWin.closed=='undefined') 
						{
							 alert('Please enable popups for <?php echo base_url(); ?>');
						}
					}, 2000);
				} else {
					notyError(data.msg);
				}
            });
//            return false;
        });
        $('.create_form').submit(function(e){
            e.preventDefault();
            var self = $(this);
            $('input[name="email"]').parent().removeClass('has-error');
            var first_name = $('input[name="fname"]').val();
            var last_name = $('input[name="lname"]').val();
            var email = $('input[name="email"]').val();
            if(first_name == '' || last_name == '' || email == ''){
                notyError("Please fill all required fields first.");
                return;
            }
            if(!validateEmail(email)){
                notyError(email+" is not valid.");
                $('input[name="email"]').parent().addClass('has-error');
                return;
            }

            $.ajax({
                url: '<?php echo base_url(); ?>admin/account_setup/create_patient',
                data: self.serialize(),
                method: "POST",
                dataType: "json"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                if(data.bool){
                    notySuccess(data.message);
                    $('input[name="fname"]').val('');
                    $('input[name="lname"]').val('');
                    $('input[name="email"]').val('');
                    $('#create_modal').modal('toggle');
                    setTimeout(function(){
                        location.reload();
                    }, 3000);
                } else {
                    notyError(data.message);
                }
            });

            return false;
        });
        $(document).on('click', '.patient_deactivate', function(e){
            e.preventDefault();
            var self = $(this);
            var id = $(this).data('userid');
            $.ajax({
                url: '<?php echo base_url(); ?>admin/account_setup/deactivate_patient',
                data: {userid: id},
                method: "POST"
//            dataType: "json"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                self.removeClass('patient_deactivate')
                    .removeClass('btn-danger')
                    .addClass('patient_activate')
                    .addClass('btn-success')
                    .html('<i class="fa fa-refresh" aria-hidden="true"></i>');
                self.attr('title', 'Activate');
                self.parent().parent().parent().children('.is_active').html('Inactive');
                notySuccess('You Successfully activate this patient.');
            });
        });
        $(document).on('click', '.patient_activate', function(e){
            e.preventDefault();
            var self = $(this);
            var id = $(this).data('userid');
            $.ajax({
                url: '<?php echo base_url(); ?>admin/account_setup/activate_patient',
                data: {userid: id},
                method: "POST"
//            dataType: "json"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                self.removeClass('patient_activate')
                    .removeClass('btn-success')
                    .addClass('patient_deactivate')
                    .addClass('btn-danger')
                    .html('<i class="fa fa-refresh" aria-hidden="true"></i>');
                self.attr('title', 'Deactivate');
                self.parent().parent().parent().children('.is_active').html('Active');
                notySuccess('You Successfully deactivate this patient.');
            });
        });
        $(document).on('click', '.patient_remove', function(e){
            e.preventDefault();
            var self = $(this);
            var userid = $(this).data('userid');
            var patientName = $(this).data('name');
            notyMessage('Are you sure that you want to DELETE <strong>' + patientName + '</strong>', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function($noty) {
                            $noty.close();
                            notySuccess('Please wait......');
                            removeProvider(userid, self);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function($noty) {
                            $noty.close();
                        }
                    }
                ]
            });

            return false;
        });
    });
</script>
