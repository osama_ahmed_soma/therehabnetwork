<section class="content-header">
    <h1>
        All Providers
        <a href="#" class="btn btn-primary create_model_btn" data-toggle="modal" data-target="#create_modal">Create Provider</a>
		<?php echo form_open('', ['class' => 'form-inline search_form', 'style' => 'display: inline;']); ?>
            <fieldset class="form-group">
                <input type="text" name="search" class="form-control" placeholder="Name, Email or Phone">
            </fieldset>
            <fieldset class="form-group">
                <select name="company" class="form-control">
                    <option value="">Select Company</option>
                    <?php foreach ($companies as $company) { ?>
                        <option value="<?php echo $company->id; ?>"><?php echo $company->name; ?></option>
                    <?php } ?>
                </select>
            </fieldset>
            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
        </form>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Account Setup</li>
        <li class="active">Provider List</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="doctors_all"
                                   class="table table-bordered table-hover tablesorter customTableSort">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone (Mobile)</th>
                                    <th>Specialty</th>
                                    <th>Signup Date</th>
                                    <th>Company</th>
                                    <th>Status</th>
                                    <th>Last Login</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
//								dd($doctors);
								if (!empty($doctors)): ?>
                                    <?php foreach ($doctors as $doctor): ?>
                                        <tr>
                                            <td><?php echo $doctor->firstname; ?> <?php echo $doctor->lastname; ?></td>
                                            <td><?php echo $doctor->email; ?></td>
                                            <td><?php echo ($doctor->phone_mobile) ? $doctor->phone_mobile : 'N/A'; ?></td>
                                            <td><?php echo $doctor->doctor_specialty; ?></td>
                                            <td><?php echo date('m/d/Y', strtotime($doctor->created_at)); ?></td>
                                            <td><?php echo ($doctor->company_name) ? $doctor->company_name->name : 'N/A'; ?></td>
                                            <td class="is_active"><?php echo ($doctor->active == 1) ? 'Active' : 'Inactive'; ?></td>
                                            <td><?php echo ($doctor->is_joined == 1) ? date('m/d/Y h:i A', strtotime($doctor->login_date_time)) : 'Never'; ?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-sm btn-primary login_as_user" title="Login"
                                                       data-userid="<?php echo $doctor->userid; ?>"
                                                       href="">
                                                        <i class="fa fa-sign-in"></i>
                                                    </a>
                                                    <?php if ($doctor->active == 1): ?>
                                                        <a class="btn btn-sm btn-danger provider_deactivate"
                                                           data-userid="<?php echo $doctor->userid; ?>"
                                                           title="Deactivate" href="#">
                                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                                        </a>
                                                    <?php else: ?>
                                                        <a class="btn btn-sm btn-success provider_activate"
                                                           data-userid="<?php echo $doctor->userid; ?>" title="Activate"
                                                           href="#">
                                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                                        </a>
                                                    <?php endif; ?>
                                                    <a class="btn btn-sm btn-primary" title="Edit"
                                                       href="index.php?do=doctors&amp;page=new&amp;act=edit&amp;actId=37">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a class="delete btn btn-sm btn-danger provider_remove"
                                                       data-name="<?php echo $doctor->firstname; ?> <?php echo $doctor->lastname; ?>"
                                                       data-userid="<?php echo $doctor->userid; ?>" title="Delete">
                                                        <i class="fa fa-remove"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="7">There is no Provider.</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone (Mobile)</th>
                                    <th>Specialty</th>
                                    <th>Signup Date</th>
                                    <th>Company</th>
                                    <th>Status</th>
                                    <th>Last Login</th>
                                    <th>Action(s)</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <?php echo (isset($pagination_link)) ? $pagination_link : ''; ?>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			
            <?php echo form_open('', ['class' => 'create_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Provider</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">First Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="fname" value="<?php echo set_value('fname', ''); ?>" placeholder="First Name">
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Last Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="lname" value="<?php echo set_value('lname', ''); ?>" placeholder="Last Name">
                            </fieldset>
                        </div>
<!--                        <div class="col-md-6">-->
<!--                            <fieldset class="form-group">-->
<!--                                <label class="form-label semibold">Professional Title</label>-->
<!--                                <select class="form-control select2" name="p_title">-->
<!--                                    <option value="Mr.">Mr.</option>-->
<!--                                    <option value="Ms.">Ms.</option>-->
<!--                                    <option value="Mrs.">Mrs.</option>-->
<!--                                    <option value="Dr.">Dr.</option>-->
<!--                                    <option value="D.O.">D.O.</option>-->
<!--                                    <option value="Ph.D.">Ph.D.</option>-->
<!--                                </select>-->
<!--                            </fieldset>-->
<!--                        </div>-->
<!--                        <div class="col-md-6">-->
<!--                            <fieldset class="form-group">-->
<!--                                <label class="form-label semibold">Primary Specialty</label>-->
<!--                                <select class="form-control select2" name="specialty">-->
<!--                                    --><?php //foreach ($specialties as $index => $specialty) { ?>
<!--                                        <option value="--><?php //echo $index.':'.$specialty; ?><!--">--><?php //echo $specialty; ?><!--</option>-->
<!--                                    --><?php //} ?>
<!--                                    <option value="-1">Other</option>-->
<!--                                </select>-->
<!--                                <input type="text" class="form-control" name="otherSpecialty" id="otherSpecialty"-->
<!--                                       style="display: none" placeholder="Other specialty">-->
<!--                            </fieldset>-->
<!--                        </div>-->
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Email Address <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="email" placeholder="Email">
                            </fieldset>
                        </div>
<!--                        <div class="col-md-6">-->
<!--                            <fieldset class="form-group">-->
<!--                                <label class="form-label semibold">Primary State License</label>-->
<!--                                <select name="state" class="select2 form-control">-->
<!--                                    --><?php //foreach ($states as $index => $name) { ?>
<!--                                        <option value="--><?php //echo $index.':'.$name; ?><!--">--><?php //echo $name; ?><!--</option>-->
<!--                                    --><?php //} ?>
<!--                                </select>-->
<!--                            </fieldset>-->
<!--                        </div>-->
                        <input type="hidden" value="1" name="save">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create Account</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function removeProvider(id, self) {
        var str = '<?php echo base_url(); ?>admin/account_setup/remove_provider';
        $.ajax({
            url: str,
            data: {userid: id},
            method: "POST"
//            dataType: "json"
        }).error(function () {

        }).done(function () {

        }).success(function (data) {
            self.parent().parent().parent().remove();
            notySuccess('Provider Deleted Successfully.');
        });
    }
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    $(document).ready(function () {
        $('.search_form').submit(function(e){
            e.preventDefault();
            var self = $(this);
            var search = $('input[name="search"]').val();
            var company = $('select[name="company"]').val();
            if(search == '' && company == ''){
                notyError("Please fill or select search fields first.");
                return;
            }
            $.ajax({
                url: '<?php echo base_url(); ?>admin/account_setup/search_provider',
                data: self.serialize(),
                method: "POST",
                dataType: "json"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                if(data.bool){
                    $('input[name="search"]').val('');
                    notySuccess(data.doctors.length+" provider found.");
                    $('.pagination').hide();
                    var html = '';
                    for(var i = 0; i < data.doctors.length; i++){
                        html += '<tr>' +
                            '<td>'+ data.doctors[i].firstname +' '+ data.doctors[i].lastname +'</td>' +
                            '<td>'+ data.doctors[i].email +'</td>' +
                            '<td>'+ data.doctors[i].phone_mobile +'</td>' +
                            '<td>'+ data.doctors[i].doctor_specialty +'</td>' +
                            '<td>'+ data.doctors[i].created_at +'</td>' +
                            '<td>'+ data.doctors[i].company_name +'</td>' +
                            '<td class="is_active">';
                                if(data.doctors[i].active == 1){
                                    html += 'Active';
                                } else {
                                    html += 'Inactive';
                                }
                            html += '</td>' +
                            '<td>'+ data.doctors[i].login_date_time +'</td>' +
                            '<td>' +
                                '<div class="btn-group">' +
                                    '<a class="btn btn-sm btn-primary login_as_user" title="Login"' +
                                        'data-userid="'+ data.doctors[i].userid +'"' +
                                        'href="">' +
                                        '<i class="fa fa-sign-in"></i>' +
                                    '</a>';
                                    if(data.doctors[i].active == 1){
                                        html += '<a class="btn btn-sm btn-danger provider_deactivate"' +
                                            'data-userid="'+ data.doctors[i].userid +'"' +
                                            'title="Deactivate" href="#">' +
                                            '<i class="fa fa-refresh" aria-hidden="true"></i>' +
                                        '</a>';
                                    } else {
                                        html += '<a class="btn btn-sm btn-success provider_activate"' +
                                            'data-userid="'+ data.doctors[i].userid +'"' +
                                            'title="Activate" href="#">' +
                                            '<i class="fa fa-refresh" aria-hidden="true"></i>' +
                                        '</a>';
                                    }
                                    html += '<a class="btn btn-sm btn-primary" title="Edit"' +
                                        'href="">' +
                                        '<i class="fa fa-edit"></i>' +
                                    '</a>' +
                                    '<a class="delete btn btn-sm btn-danger provider_remove"' +
                                        'data-name="'+ data.doctors[i].firstname +' '+ data.doctors[i].lastname +'"' +
                                        'data-userid="'+ data.doctors[i].userid +'" title="Delete">' +
                                        '<i class="fa fa-remove"></i>' +
                                    '</a>' +
                                '</div>' +
                            '</td>' +
                        '</tr>';
                    }
                    $('#doctors_all tbody').html(html);
                    $('.customTableSort').trigger('update');
                } else {
                    notyError("No provider found.");
                }
            });
        });
        $(document).on('click', '.login_as_user', function(e){
            e.preventDefault();
            var self = $(this);
            $.ajax({
                url: '<?php echo base_url(); ?>login/login_as_user/' + self.attr('data-userid') ,
                method: "GET"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
				data = JSON.parse(data);
				if (data.redirect !== '') {
					notySuccess(data.msg);
					redirect = data.redirect;
					setTimeout(function(){
						var newWin = window.open(redirect);             
						if(!newWin || newWin.closed || typeof newWin.closed=='undefined') 
						{
							 alert('Please enable popups for <?php echo base_url(); ?>');
						}
					}, 2000);
				} else {
					notyError(data.msg);
				}
            });
//            return false;
        });
        $('.create_form').submit(function(e){
            e.preventDefault();
            var self = $(this);
            $('input[name="email"]').parent().removeClass('has-error');
            var first_name = $('input[name="fname"]').val();
            var last_name = $('input[name="lname"]').val();
            var email = $('input[name="email"]').val();
            if(first_name == '' || last_name == '' || email == ''){
                notyError("Please fill all required fields first.");
                return;
            }
            if(!validateEmail(email)){
                notyError(email+" is not valid.");
                $('input[name="email"]').parent().addClass('has-error');
                return;
            }

            $.ajax({
                url: '<?php echo base_url(); ?>admin/account_setup/create_provider',
                data: self.serialize(),
                method: "POST",
                dataType: "json"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                if(data.bool){
                    notySuccess(data.message);
                    $('input[name="fname"]').val('');
                    $('input[name="lname"]').val('');
                    $('input[name="email"]').val('');
                    $('#create_modal').modal('toggle');
                    setTimeout(function(){
                        location.reload();
                    }, 3000);
                } else {
                    notyError(data.message);
                }
            });

            return false;
        });
        $('.create_model_btn').click(function(){
            $('.select2').select2();
        });
        $(document).on('click', '.provider_deactivate', function (e) {
            e.preventDefault();
            var self = $(this);
            var id = $(this).data('userid');
            $.ajax({
                url: '<?php echo base_url(); ?>admin/account_setup/deactivate_provider',
                data: {userid: id},
                method: "POST"
//            dataType: "json"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                self.removeClass('provider_deactivate')
                    .removeClass('btn-danger')
                    .addClass('provider_activate')
                    .addClass('btn-success')
                    .html('<i class="fa fa-refresh" aria-hidden="true"></i>');
                self.attr('title', 'Activate');
                self.parent().parent().parent().children('.is_active').html('Inactive');
                notySuccess('You Successfully activate this provider.');
            });
        });
        $(document).on('click', '.provider_activate', function (e) {
            e.preventDefault();
            var self = $(this);
            var id = $(this).data('userid');
            $.ajax({
                url: '<?php echo base_url(); ?>admin/account_setup/activate_provider',
                data: {userid: id},
                method: "POST"
//            dataType: "json"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                self.removeClass('provider_activate')
                    .removeClass('btn-success')
                    .addClass('provider_deactivate')
                    .addClass('btn-danger')
                    .html('<i class="fa fa-refresh" aria-hidden="true"></i>');
                self.attr('title', 'Deactivate');
                self.parent().parent().parent().children('.is_active').html('Active');
                notySuccess('You Successfully deactivate this provider.');
            });
        });
        $(document).on('click', '.provider_remove', function (e) {
            e.preventDefault();
            var self = $(this);
            var userid = $(this).data('userid');
            var providerName = $(this).data('name');
            notyMessage('Are you sure that you want to DELETE <strong>' + providerName + '</strong>', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            notySuccess('Please wait......');
                            removeProvider(userid, self);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });

            return false;
        });
    });
</script>