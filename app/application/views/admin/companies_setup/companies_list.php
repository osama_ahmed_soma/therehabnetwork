<section class="content-header">
    <h1>
        Companies List <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#create_modal">Create Company</a>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Companies Setup</li>
        <li class="active"> Add Company</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table id="doctors_all" class="table table-bordered table-hover tablesorter customTableSort">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Company</th>
                            <th>Url</th>
                            <th># of Doctors</th>
                            <th>Action(s)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($companies)): ?>
                            <?php foreach ($companies as $company): ?>
                                <tr id="company_<?php echo $company->id; ?>">
                                    <td><?php echo $company->id; ?></td>
                                    <td class="company_name"><?php echo $company->name; ?></td>
                                    <td class="company_code">
                                        <a class="btn btn-sm btn-primary url_company" title="Get Registration Url" href="#"
                                            data-company_code="<?php echo $company->code; ?>"
                                            data-toggle="modal" data-target="#url_modal">
                                            <i class="fa fa-user"></i>
                                        </a>
                                    </td>
                                    <td><?php if($company->number_of_doctors > 0): ?><a target="_blank" href="<?php echo base_url(); ?>admin/account_setup/provider_list/<?php echo $company->code; ?>" class="btn btn-primary"><?php echo $company->number_of_doctors; ?></a><?php else: echo $company->number_of_doctors; endif; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-primary edit_company" title="Edit" href="#"
                                               data-company_code="<?php echo $company->code; ?>"
                                               data-company_name="<?php echo $company->name; ?>"
                                               data-company_id="<?php echo $company->id; ?>"
                                               data-toggle="modal" data-target="#edit_modal">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="delete btn btn-sm btn-danger company_remove"
                                               data-companyid="<?php echo $company->id; ?>" title="Delete">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="7">There is no Company.</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Company</th>
                            <th>Url</th>
                            <th># of Doctors</th>
                            <th>Action(s)</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <?php echo (isset($pagination_link)) ? $pagination_link : '' ; ?>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', ['class' => 'create_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Company</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="company_name">Add New Company</label>
                                <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Type New Company" required />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="company_code">Add New Company Code</label>
                                <input type="text" class="form-control" name="company_code" id="company_code" placeholder="Type New Company Code" required />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', ['class' => 'edit_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Company</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="company_name">Edit Company</label>
                                <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Edit Here" required />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="company_code">Edit Company Code</label>
                                <input type="text" class="form-control" name="company_code" id="company_code" placeholder="Edit Here" required />
                            </div>
                        </div>
                        <input type="hidden" name="company_id" id="company_id" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="url_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Company Registration Url</h4>
            </div>
            <div class="modal-body text-center">
                <span class="url"></span>
            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Edit</button>
            </div>-->
        </div>
    </div>
</div>
<script>
    function removeCompany(id, self){
        $.ajax({
            url: '<?php echo base_url(); ?>admin/company/remove_company',
            data: {companyid: id},
            method: "POST"
        }).error(function () {

        }).done(function () {

        }).success(function (data) {
            self.parent().parent().parent().remove();
            notySuccess('You Successfully delete company.');
        });
    }
    $(document).ready(function(){
        $('.create_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/company/add_company_ajax',
                data: $(this).serialize(),
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                var result = JSON.parse(data);
                if(result.bool){
                    $('.create_form input[name="company_name"]').val('');
                    $('.create_form input[name="company_code"]').val('');
                    $('#create_modal').modal('hide');
                    notySuccess(result.message);
                } else {
                    notyError(result.message);
                }
            });
            return false;
        });
        $('.edit_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/company/edit_company_ajax',
                data: $(this).serialize(),
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                var result = JSON.parse(data);
                if(result.bool){
                    $('.edit_form input[name="company_name"]').val('');
                    $('.edit_form input[name="company_code"]').val('');
                    $('.edit_form input[name="company_id"]').val('');
                    $('#edit_modal').modal('hide');
                    notySuccess(result.message);
                    $('#company_'+result.company_id+' .company_name').html(result.company_name);
                    $('#company_'+result.company_id+' .company_code a').attr('data-company_code', result.company_code);
                    $('#company_'+result.company_id+' a.edit_company').attr('data-company_name', result.company_name);
                    $('#company_'+result.company_id+' a.edit_company').attr('data-company_code', result.company_code);
                } else {
                    notyError(result.message);
                }
            });
            return false;
        });
        $('.edit_company').click(function(e){
//            e.preventDefault();
            $('.edit_form #company_name').val($(this).attr('data-company_name'));
            $('.edit_form #company_code').val($(this).attr('data-company_code'));
            $('.edit_form #company_id').val($(this).attr('data-company_id'));
        });
        $('.company_remove').click(function(e){
            e.preventDefault();
            var self = $(this);
            var companyid = $(this).data('companyid');
            notyMessage('Are you sure that you want to DELETE', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            notySuccess('Please wait......');
                            removeCompany(companyid, self);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });
        $('.url_company').click(function(e){
            $('.url').text('<?php echo base_url(); ?>register/doctor/'+$(this).attr('data-company_code'));
        });
    });
</script>