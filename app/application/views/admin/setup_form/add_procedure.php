<section class="content-header">
    <h1>
        Procedures List <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#create_modal">Create Procedure</a>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Setup Form</li>
        <li class="active"> Add Procedure</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table id="doctors_all" class="table table-bordered table-hover tablesorter customTableSort">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Procedure</th>
                            <th>Code</th>
                            <th>Action(s)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($procedures)): ?>
                            <?php foreach ($procedures as $procedure): ?>
                                <tr id="procedure_<?php echo $procedure->id; ?>">
                                    <td><?php echo $procedure->id; ?></td>
                                    <td class="procedure_name"><?php echo $procedure->name; ?></td>
                                    <td class="procedure_code"><?php echo $procedure->code; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-primary edit_procedure" title="Edit" href="#"
                                               data-procedure_name="<?php echo $procedure->name; ?>"
                                               data-procedure_code="<?php echo $procedure->code; ?>"
                                               data-procedure_id="<?php echo $procedure->id; ?>"
                                               data-toggle="modal" data-target="#edit_modal">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="delete btn btn-sm btn-danger procedure_remove"
                                               data-procedureid="<?php echo $procedure->id; ?>" title="Delete">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="7">There is no Procedure.</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Procedure</th>
                            <th>Code</th>
                            <th>Action(s)</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <?php echo (isset($pagination_link)) ? $pagination_link : '' ; ?>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', ['class' => 'create_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Procedure</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="procedure_name">Add New Procedure</label>
                                <input type="text" class="form-control" name="procedure_name" id="procedure_name" placeholder="Type New Procedure" required />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="procedure_code">Add New Procedure Code</label>
                                <input type="text" class="form-control" name="procedure_code" id="procedure_code" placeholder="Type New Procedure Code" required />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', ['class' => 'edit_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Procedure</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="procedure_name">Edit Procedure</label>
                                <input type="text" class="form-control" name="procedure_name" id="procedure_name" placeholder="Edit Here" required />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="procedure_code">Edit Procedure Code</label>
                                <input type="text" class="form-control" name="procedure_code" id="procedure_code" placeholder="Edit Here" required />
                            </div>
                        </div>
                        <input type="hidden" name="procedure_id" id="procedure_id" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function removeProcedure(id, self){
        $.ajax({
            url: '<?php echo base_url(); ?>admin/setup_form/remove_procedure',
            data: {procedureid: id},
            method: "POST"
        }).error(function () {

        }).done(function () {

        }).success(function (data) {
            self.parent().parent().parent().remove();
            notySuccess('You Successfully delete procedure.');
        });
    }
    $(document).ready(function(){
        $('.create_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/setup_form/add_procedure_ajax',
                data: $(this).serialize(),
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                var result = JSON.parse(data);
                if(result.bool){
                    $('.create_form input[name="procedure_name"]').val('');
                    $('.create_form input[name="procedure_code"]').val('');
                    $('#create_modal').modal('hide');
                    notySuccess(result.message);
                } else {
                    notyError(result.message);
                }
            });
            return false;
        });
        $('.edit_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/setup_form/edit_procedure_ajax',
                data: $(this).serialize(),
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                var result = JSON.parse(data);
                if(result.bool){
                    $('.edit_form input[name="procedure_name"]').val('');
                    $('.edit_form input[name="procedure_code"]').val('');
                    $('.edit_form input[name="procedure_id"]').val('');
                    $('#edit_modal').modal('hide');
                    notySuccess(result.message);
                    $('#procedure_'+result.procedure_id+' .procedure_name').html(result.procedure_name);
                    $('#procedure_'+result.procedure_id+' .procedure_code').html(result.procedure_code);
                    $('#procedure_'+result.procedure_id+' a.edit_procedure')
                        .attr('data-procedure_name', result.procedure_name)
                        .attr('data-procedure_code', result.procedure_code);
                } else {
                    notyError(result.message);
                }
            });
            return false;
        });
        $('.edit_procedure').click(function(e){
            $('.edit_form #procedure_name').val($(this).attr('data-procedure_name'));
            $('.edit_form #procedure_code').val($(this).attr('data-procedure_code'));
            $('.edit_form #procedure_id').val($(this).attr('data-procedure_id'));
        });
        $('.procedure_remove').click(function(e){
            e.preventDefault();
            var self = $(this);
            var procedureid = $(this).data('procedureid');
            notyMessage('Are you sure that you want to DELETE', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            notySuccess('Please wait......');
                            removeProcedure(procedureid, self);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });
    });
</script>