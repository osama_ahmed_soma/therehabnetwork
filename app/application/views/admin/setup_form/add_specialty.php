<section class="content-header">
    <h1>
        Specialties List <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#create_modal">Create Specialty</a>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Setup Form</li>
        <li class="active"> Add Specialty</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table id="doctors_all" class="table table-bordered table-hover tablesorter customTableSort">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Specialty</th>
                            <th>Action(s)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($specialties)): ?>
                            <?php foreach ($specialties as $specialty_id => $specialty_name): ?>
                                <tr id="specialty_<?php echo $specialty_id; ?>">
                                    <td><?php echo $specialty_id; ?></td>
                                    <td class="specialty_name"><?php echo $specialty_name; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-primary edit_specialty" title="Edit" href="#"
                                               data-specialty_name="<?php echo $specialty_name; ?>"
                                               data-specialty_id="<?php echo $specialty_id; ?>"
                                               data-toggle="modal" data-target="#edit_modal">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="delete btn btn-sm btn-danger specialty_remove"
                                               data-specialtyid="<?php echo $specialty_id; ?>" title="Delete">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="7">There is no Specialty.</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Specialty</th>
                            <th>Action(s)</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <?php echo (isset($pagination_link)) ? $pagination_link : '' ; ?>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', ['class' => 'create_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Specialty</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="specialty_name">Add New Specialty</label>
                        <input type="text" class="form-control" name="specialty_name" id="specialty_name" placeholder="Type New Specialty" required />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', ['class' => 'edit_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Specialty</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="specialty_name">Edit Specialty</label>
                        <input type="text" class="form-control" name="specialty_name" id="specialty_name" placeholder="Edit Here" required />
                        <input type="hidden" name="specialty_id" id="specialty_id" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function removeSpecialty(id, self){
        $.ajax({
            url: '<?php echo base_url(); ?>admin/setup_form/remove_specialty',
            data: {specialtyid: id},
            method: "POST"
        }).error(function () {

        }).done(function () {

        }).success(function (data) {
            self.parent().parent().parent().remove();
            notySuccess('You Successfully delete specialty.');
        });
    }
    $(document).ready(function(){
        $('.create_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/setup_form/add_specialty_ajax',
                data: $(this).serialize(),
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                var result = JSON.parse(data);
                if(result.bool){
                    $('.create_form input[name="specialty_name"]').val('');
                    $('#create_modal').modal('hide');
                    notySuccess(result.message);
                } else {
                    notyError(result.message);
                }
            });
            return false;
        });
        $('.edit_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/setup_form/edit_specialty_ajax',
                data: $(this).serialize(),
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                var result = JSON.parse(data);
                if(result.bool){
                    $('.edit_form input[name="specialty_name"]').val('');
                    $('.edit_form input[name="specialty_id"]').val('');
                    $('#edit_modal').modal('hide');
                    notySuccess(result.message);
                    $('#specialty_'+result.specialty_id+' .specialty_name').html(result.specialty_name);
                    $('#specialty_'+result.specialty_id+' a.edit_specialty').attr('data-specialty_name', result.specialty_name);
                } else {
                    notyError(result.message);
                }
            });
            return false;
        });
        $('.edit_specialty').click(function(e){
            $('.edit_form #specialty_name').val($(this).attr('data-specialty_name'));
            $('.edit_form #specialty_id').val($(this).attr('data-specialty_id'));
        });
        $('.specialty_remove').click(function(e){
            e.preventDefault();
            var self = $(this);
            var specialtyid = $(this).data('specialtyid');
            notyMessage('Are you sure that you want to DELETE', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            notySuccess('Please wait......');
                            removeSpecialty(specialtyid, self);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });
    });
</script>