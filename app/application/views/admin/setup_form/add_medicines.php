<section class="content-header">
    <h1>
        Medicines List <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#create_modal">Create Medicine</a>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Setup Form</li>
        <li class="active"> Add Medicines</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table id="doctors_all" class="table table-bordered table-hover tablesorter customTableSort">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Medicine</th>
                            <th>Action(s)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($medicines)): ?>
                            <?php foreach ($medicines as $medicine): ?>
                                <tr id="medicine_<?php echo $medicine->drug_id; ?>">
                                    <td><?php echo $medicine->drug_id; ?></td>
                                    <td class="medicine_name"><?php echo $medicine->BrandName; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-primary edit_medicine" title="Edit" href="#"
                                               data-medicine_name="<?php echo $medicine->BrandName; ?>"
                                               data-medicine_id="<?php echo $medicine->drug_id; ?>"
                                               data-toggle="modal" data-target="#edit_modal">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="delete btn btn-sm btn-danger medicine_remove"
                                               data-medicineid="<?php echo $medicine->drug_id; ?>" title="Delete">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="7">There is no Medicine.</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Medicine</th>
                            <th>Action(s)</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <?php echo (isset($pagination_link)) ? $pagination_link : '' ; ?>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', ['class' => 'create_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Medicine</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="medicine_name">Add New Medicine</label>
                        <input type="text" class="form-control" name="medicine_name" id="medicine_name" placeholder="Type New Medicine" required />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', ['class' => 'edit_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Medicine</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="medicine_name">Edit Medicine</label>
                        <input type="text" class="form-control" name="medicine_name" id="medicine_name" placeholder="Edit Here" required />
                        <input type="hidden" name="medicine_id" id="medicine_id" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function removeMedicine(id, self){
        $.ajax({
            url: '<?php echo base_url(); ?>admin/setup_form/remove_medicine',
            data: {medicineid: id},
            method: "POST"
        }).error(function () {

        }).done(function () {

        }).success(function (data) {
            self.parent().parent().parent().remove();
            notySuccess('You Successfully delete medicine.');
        });
    }
    $(document).ready(function(){
        $('.create_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/setup_form/add_medicines_ajax',
                data: $(this).serialize(),
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                var result = JSON.parse(data);
                if(result.bool){
                    $('.create_form input[name="medicine_name"]').val('');
                    $('#create_modal').modal('hide');
                    notySuccess(result.message);
                } else {
                    notyError(result.message);
                }
            });
            return false;
        });
        $('.edit_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/setup_form/edit_medicine_ajax',
                data: $(this).serialize(),
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                var result = JSON.parse(data);
                if(result.bool){
                    $('.edit_form input[name="medicine_name"]').val('');
                    $('.edit_form input[name="medicine_id"]').val('');
                    $('#edit_modal').modal('hide');
                    notySuccess(result.message);
                    $('#medicine_'+result.medicine_id+' .medicine_name').html(result.medicine_name);
                    $('#medicine_'+result.medicine_id+' a.edit_medicine').attr('data-medicine_name', result.medicine_name);
                } else {
                    notyError(result.message);
                }
            });
            return false;
        });
        $('.edit_medicine').click(function(e){
            $('.edit_form #medicine_name').val($(this).attr('data-medicine_name'));
            $('.edit_form #medicine_id').val($(this).attr('data-medicine_id'));
        });
        $('.medicine_remove').click(function(e){
            e.preventDefault();
            var self = $(this);
            var medicineid = $(this).data('medicineid');
            notyMessage('Are you sure that you want to DELETE', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            notySuccess('Please wait......');
                            removeMedicine(medicineid, self);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });
    });
</script>