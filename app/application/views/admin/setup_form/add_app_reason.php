<section class="content-header">
    <h1>
        Reasons List <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#create_modal">Create Reason</a>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Setup Form</li>
        <li class="active"> Add App Reason</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table id="doctors_all" class="table table-bordered table-hover tablesorter customTableSort">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Reason</th>
                            <th>Action(s)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($reasons)): ?>
                            <?php foreach ($reasons as $reason): ?>
                                <tr id="reason_<?php echo $reason->id; ?>">
                                    <td><?php echo $reason->id; ?></td>
                                    <td class="reason_name"><?php echo $reason->name; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-primary edit_reason" title="Edit" href="#"
                                               data-reason_name="<?php echo $reason->name; ?>"
                                               data-reason_id="<?php echo $reason->id; ?>"
                                               data-toggle="modal" data-target="#edit_modal">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="delete btn btn-sm btn-danger reason_remove"
                                               data-reasonid="<?php echo $reason->id; ?>" title="Delete">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="7">There is no Reasons.</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Reason</th>
                            <th>Action(s)</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <?php echo (isset($pagination_link)) ? $pagination_link : '' ; ?>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', ['class' => 'create_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Reason</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="reason_name">Add New Reason</label>
                        <input type="text" class="form-control" name="reason_name" id="reason_name" placeholder="Type New Reason" required />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', ['class' => 'edit_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Reason</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="reason_name">Edit Reason</label>
                        <input type="text" class="form-control" name="reason_name" id="reason_name" placeholder="Edit Here" required />
                        <input type="hidden" name="reason_id" id="reason_id" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function removeReason(id, self){
        $.ajax({
            url: '<?php echo base_url(); ?>admin/setup_form/remove_reason',
            data: {reasonid: id},
            method: "POST"
        }).error(function () {

        }).done(function () {

        }).success(function (data) {
            self.parent().parent().parent().remove();
            notySuccess('You Successfully delete reason.');
        });
    }
    $(document).ready(function(){
        $('.create_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/setup_form/add_app_reason_ajax',
                data: $(this).serialize(),
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                var result = JSON.parse(data);
                if(result.bool){
                    $('.create_form input[name="reason_name"]').val('');
                    $('#create_modal').modal('hide');
                    notySuccess(result.message);
                } else {
                    notyError(result.message);
                }
            });
            return false;
        });
        $('.edit_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/setup_form/edit_app_reason_ajax',
                data: $(this).serialize(),
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                var result = JSON.parse(data);
                if(result.bool){
                    $('.edit_form input[name="reason_name"]').val('');
                    $('.edit_form input[name="reason_id"]').val('');
                    $('#edit_modal').modal('hide');
                    notySuccess(result.message);
                    $('#reason_'+result.reason_id+' .reason_name').html(result.reason_name);
                    $('#reason_'+result.reason_id+' a.edit_reason').attr('data-reason_name', result.reason_name);
                } else {
                    notyError(result.message);
                }
            });
            return false;
        });
        $('.edit_reason').click(function(e){
//            e.preventDefault();
            $('.edit_form #reason_name').val($(this).attr('data-reason_name'));
            $('.edit_form #reason_id').val($(this).attr('data-reason_id'));
        });
        $('.reason_remove').click(function(e){
            e.preventDefault();
            var self = $(this);
            var reasonid = $(this).data('reasonid');
            notyMessage('Are you sure that you want to DELETE', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            notySuccess('Please wait......');
                            removeReason(reasonid, self);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });
    });
</script>
