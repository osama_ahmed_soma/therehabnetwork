<section class="content-header">
    <h1>
        Diagnosis List <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#create_modal">Create Diagnosis</a>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Setup Form</li>
        <li class="active"> Add Diagnosis</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table id="doctors_all" class="table table-bordered table-hover tablesorter customTableSort">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Diagnosis</th>
                            <th>Code</th>
                            <th>Action(s)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($diagnoses)): ?>
                            <?php foreach ($diagnoses as $diagnosis): ?>
                                <tr id="diagnosis_<?php echo $diagnosis->id; ?>">
                                    <td><?php echo $diagnosis->id; ?></td>
                                    <td class="diagnosis_name"><?php echo $diagnosis->name; ?></td>
                                    <td class="diagnosis_code"><?php echo $diagnosis->code; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-primary edit_diagnosis" title="Edit" href="#"
                                               data-diagnosis_name="<?php echo $diagnosis->name; ?>"
                                               data-diagnosis_code="<?php echo $diagnosis->code; ?>"
                                               data-diagnosis_id="<?php echo $diagnosis->id; ?>"
                                               data-toggle="modal" data-target="#edit_modal">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="delete btn btn-sm btn-danger diagnosis_remove"
                                               data-diagnosisid="<?php echo $diagnosis->id; ?>" title="Delete">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="7">There is no Diagnosis.</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Diagnosis</th>
                            <th>Code</th>
                            <th>Action(s)</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <?php echo (isset($pagination_link)) ? $pagination_link : '' ; ?>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', ['class' => 'create_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Diagnosis</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="diagnosis_name">Add New Diagnosis</label>
                                <input type="text" class="form-control" name="diagnosis_name" id="diagnosis_name" placeholder="Type New Dianosis" required />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="diagnosis_code">Add New Diagnosis Code</label>
                                <input type="text" class="form-control" name="diagnosis_code" id="diagnosis_code" placeholder="Type New Diagnosis Code" required />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', ['class' => 'edit_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Diagnosis</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="diagnosis_name">Edit Diagnosis</label>
                                <input type="text" class="form-control" name="diagnosis_name" id="diagnosis_name" placeholder="Edit Here" required />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="diagnosis_code">Edit Diagnosis Code</label>
                                <input type="text" class="form-control" name="diagnosis_code" id="diagnosis_code" placeholder="Edit Here" required />
                            </div>
                        </div>
                        <input type="hidden" name="diagnosis_id" id="diagnosis_id" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function removeDiagnosis(id, self){
        $.ajax({
            url: '<?php echo base_url(); ?>admin/setup_form/remove_diagnosis',
            data: {diagnosisid: id},
            method: "POST"
        }).error(function () {

        }).done(function () {

        }).success(function (data) {
            self.parent().parent().parent().remove();
            notySuccess('You Successfully delete diagnosis.');
        });
    }
    $(document).ready(function(){
        $('.create_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/setup_form/add_diagnosis_ajax',
                data: $(this).serialize(),
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                var result = JSON.parse(data);
                if(result.bool){
                    $('.create_form input[name="diagnosis_name"]').val('');
                    $('.create_form input[name="diagnosis_code"]').val('');
                    $('#create_modal').modal('hide');
                    notySuccess(result.message);
                } else {
                    notyError(result.message);
                }
            });
            return false;
        });
        $('.edit_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/setup_form/edit_diagnosis_ajax',
                data: $(this).serialize(),
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                var result = JSON.parse(data);
                if(result.bool){
                    $('.edit_form input[name="diagnosis_name"]').val('');
                    $('.edit_form input[name="diagnosis_code"]').val('');
                    $('.edit_form input[name="diagnosis_id"]').val('');
                    $('#edit_modal').modal('hide');
                    notySuccess(result.message);
                    $('#diagnosis_'+result.diagnosis_id+' .diagnosis_name').html(result.diagnosis_name);
                    $('#diagnosis_'+result.diagnosis_id+' .diagnosis_code').html(result.diagnosis_code);
                    $('#diagnosis_'+result.diagnosis_id+' a.edit_diagnosis')
                        .attr('data-diagnosis_name', result.diagnosis_name)
                        .attr('data-diagnosis_code', result.diagnosis_code);
                } else {
                    notyError(result.message);
                }
            });
            return false;
        });
        $('.edit_diagnosis').click(function(e){
            $('.edit_form #diagnosis_name').val($(this).attr('data-diagnosis_name'));
            $('.edit_form #diagnosis_code').val($(this).attr('data-diagnosis_code'));
            $('.edit_form #diagnosis_id').val($(this).attr('data-diagnosis_id'));
        });
        $('.diagnosis_remove').click(function(e){
            e.preventDefault();
            var self = $(this);
            var diagnosisid = $(this).data('diagnosisid');
            notyMessage('Are you sure that you want to DELETE', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            notySuccess('Please wait......');
                            removeDiagnosis(diagnosisid, self);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });
    });
</script>