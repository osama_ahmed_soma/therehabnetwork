<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) == 'login' ): ?>
        </div><!-- /.login-box -->

        <!-- jQuery 2.1.4 -->
        <script src="<?php echo base_url(); ?>template_file/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="<?php echo base_url(); ?>template_file/AdminLTE/bootstrap/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url(); ?>template_file/AdminLTE/plugins/iCheck/icheck.min.js"></script>
        <script>
            jQuery(function ($) {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
<?php else: ?>
            </div><!-- /.content-wrapper -->
            <?php $this->view('admin/footer_content'); ?>
            <?php $this->view('admin/right_menu'); ?>
        </div><!-- ./wrapper -->

        <!-- Bootstrap 3.3.5 -->
        <script src="<?php echo base_url(); ?>template_file/AdminLTE/bootstrap/js/bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>template_file/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>template_file/AdminLTE/plugins/fastclick/fastclick.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>template_file/AdminLTE/dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>template_file/AdminLTE/dist/js/demo.js"></script>

        <script src="<?php echo base_url(); ?>template_file/js/jquery.noty.packaged.min.js"></script>

        <script src="<?php echo base_url(); ?>template_file/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

        <script src="<?php echo base_url(); ?>template_file/AdminLTE/plugins/bootstrap-formhelpers/bootstrap-formhelpers.min.js"></script>

        <script src="<?php echo base_url(); ?>template_file/AdminLTE/plugins/bootstrap-formvalidation/formValidation.min.js"></script>
        <script src="<?php echo base_url(); ?>template_file/AdminLTE/plugins/bootstrap-formvalidation/formValidation_bootstrap.min.js"></script>

        <script src="<?php echo base_url(); ?>template_file/AdminLTE/plugins/select2/select2.full.min.js"></script>

        <script src="<?php echo base_url(); ?>template_file/js/lib/bootstrap-select/bootstrap-select.min.js"></script>

        <script src="<?php echo base_url(); ?>template_file/js/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

        <script src="<?php echo base_url(); ?>template_file/AdminLTE/js/custom.js"></script>

<?php if( isset($customScript) && is_array($customScript)) {
    foreach ( $customScript as $script ) { ?>
        <script src="<?php echo $script; ?>"></script>
    <?php }
} ?>
<?php endif; ?>


<?php
if (isset($GLOBALS['captcha_required']) && $GLOBALS['captcha_required'] === true) {
?>
	<script src='https://www.google.com/recaptcha/api.js'></script>
<?php
}
?>
<script>
	<?php if (config_item('csrf_protection')) { ?>
	csrf_hash = '<?php echo $this->security->get_csrf_hash(); ?>';
	<?php } ?>
	var toType = function(obj) {
		return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
	}
    $(document).ready(function(){
		
		$.ajaxSetup({
			cache: false,
		});

		$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
			<?php if (config_item('csrf_protection')) { ?>
			if (options.type.toLowerCase() === "post") {
				options.data = options.data || "";
				if (toType(options.data) == 'string') {
					options.data += options.data?"&":"";
					options.data += "<?php echo $this->security->get_csrf_token_name(); ?>=" + csrf_hash;
				} else if (toType(options.data) == 'object') {
					options.data.appends('<?php echo $this->security->get_csrf_token_name(); ?>', csrf_hash);
				}
			}
			<?php } ?>
		});
		
		
        <?php if ($errorMessage = $this->session->flashdata('error')) { ?>
        notyError("<?php echo $errorMessage?>");
        <?php } ?>

        <?php if ($successMessage = $this->session->flashdata('register_success')) { ?>
        notySuccess("<?php echo $successMessage?>");
        <?php } ?>
        $(".customTableSort").tablesorter();
    });
</script>

    </body>
</html>