<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?php echo $doctors_count; ?></h3>
                    <p><?php echo ($doctors_count > 1) ? 'Doctors' : 'Doctor' ; ?></p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="<?php echo base_url(); ?>admin/account_setup/provider_list" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?php echo $patients_count; ?></h3>
                    <p><?php echo ($patients_count > 1) ? 'Patients' : 'Patient' ; ?></p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="<?php echo base_url(); ?>admin/account_setup/patient_list" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?php echo $companies_count; ?></h3>
                    <p><?php echo ($companies_count > 1) ? 'Companies' : 'Company' ; ?></p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="<?php echo base_url(); ?>admin/company/companies_list" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?php echo $appointments_count; ?></h3>
                    <p><?php echo ($appointments_count > 1) ? 'Appointments' : 'Appointment' ; ?></p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>admin/appointment/all_appointment" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

<!--        <div class="col-lg-3 col-xs-6">-->
<!--            <div class="small-box bg-yellow">-->
<!--                <div class="inner">-->
<!--                    <h3>--><?php //echo $admins_count; ?><!--</h3>-->
<!--                    <p>--><?php //echo ($admins_count > 1) ? 'Administrators' : 'Administrator' ; ?><!--</p>-->
<!--                </div>-->
<!--                <div class="icon">-->
<!--                    <i class="ion ion-person-add"></i>-->
<!--                </div>-->
<!--                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
<!--            </div>-->
<!--        </div>-->

<!--        <div class="col-lg-3 col-xs-6">-->
<!--            <div class="small-box bg-red">-->
<!--                <div class="inner">-->
<!--                    <h3>65</h3>-->
<!--                    <p>Unique Visitors</p>-->
<!--                </div>-->
<!--                <div class="icon">-->
<!--                    <i class="ion ion-pie-graph"></i>-->
<!--                </div>-->
<!--                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
<!--            </div>-->
<!--        </div>-->
    </div><!-- /.row -->


</section><!-- /.content -->