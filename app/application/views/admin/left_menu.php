<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
<!--                <img src="--><?php //echo get_gravatar($this->session->userdata('admin_sess')['email'], 160) ?><!--" class="img-circle" alt="User Image">-->
                <img src="<?php echo default_image($this->session->userdata('admin_sess')['picture']); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata('admin_sess')['firstname'] . ' ' . $this->session->userdata('admin_sess')['lastname']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'company' &&  in_array($this->uri->segment(3), array('companies_list') ) ) { echo 'active '; } ?>treeview">
                <a href="#">
                    <i class="fa fa-user-md"></i> <span>Companies Setup</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'company' && $this->uri->segment(3) == 'companies_list' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/company/companies_list"><i class="fa fa-users"></i> Companies List</a></li>
                </ul>
            </li>

            <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'appointment' &&  in_array($this->uri->segment(3), array('all_appointment', 'appointment_today', 'pending_appointment', 'cancelled_appointment') ) ) { echo 'active '; } ?>treeview">
                <a href="#">
                    <i class="fa fa-user-md"></i> <span>Appointment List</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'appointment' && $this->uri->segment(3) == 'all_appointment' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/appointment/all_appointment"><i class="fa fa-users"></i> All Appointment List</a></li>
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'appointment' && $this->uri->segment(3) == 'appointment_today' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/appointment/appointment_today"><i class="fa fa-users"></i> Today Appointment List</a></li>
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'appointment' && $this->uri->segment(3) == 'pending_appointment' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/appointment/pending_appointment"><i class="fa fa-users"></i> Pending Appointment List</a></li>
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'appointment' && $this->uri->segment(3) == 'cancelled_appointment' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/appointment/cancelled_appointment"><i class="fa fa-users"></i> Cancelled Appointment List</a></li>
                </ul>
            </li>

            <li class="<?php if ($this->uri->segment(2) === 'account_setup' &&  in_array($this->uri->segment(3), array('provider_list', 'patient_list') ) ) { echo 'active '; } ?>treeview">
                <a href="#">
                    <i class="fa fa-user-md"></i> <span>Account Setup</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($this->uri->segment(2) === 'account_setup' && $this->uri->segment(3) == 'provider_list' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/account_setup/provider_list"><i class="fa fa-users"></i> Provider List</a></li>
                    <li class="<?php if ($this->uri->segment(2) === 'account_setup' && $this->uri->segment(3) == 'patient_list' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/account_setup/patient_list"><i class="fa fa-users"></i> Patient List</a></li>
                </ul>
            </li>

<!--            <li class="<?php if ($this->uri->segment(1) === 'admin' &&  in_array($this->uri->segment(2), array('payment_report', 'new_patient_reports', 'new_providers_reports', 'provider_payment_report') ) ) { echo 'active '; } ?>treeview">
                <a href="#">
                    <i class="fa fa-user-md"></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) == 'payment_report' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/payment_report"><i class="fa fa-users"></i> Payment Report</a></li>
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) == 'new_patient_reports' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/new_patient_reports"><i class="fa fa-users"></i> New Patients Reports</a></li>
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) == 'new_providers_reports' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/new_providers_reports"><i class="fa fa-users"></i> New Providers Reports</a></li>
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) == 'provider_payment_report' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/provider_payment_report"><i class="fa fa-users"></i> Provider Wise Payment Report</a></li>
                </ul>
            </li>-->

<!--            <li class="<?php if ($this->uri->segment(1) === 'admin' &&  in_array($this->uri->segment(2), array('provider_log', 'patient_log') ) ) { echo 'active '; } ?>treeview">
                <a href="#">
                    <i class="fa fa-user-md"></i> <span>Logs</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) == 'provider_log' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/provider_log"><i class="fa fa-users"></i> Provider Log</a></li>
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) == 'patient_log' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/patient_log"><i class="fa fa-users"></i> Patient Log</a></li>
                </ul>
            </li>-->

            <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'setup_form' &&  in_array($this->uri->segment(3), array('add_app_reason', 'add_medicines', 'add_procedure', 'add_diagnosis', 'add_specialty') ) ) { echo 'active '; } ?>treeview">
                <a href="#">
                    <i class="fa fa-user-md"></i> <span>Setup Form</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'setup_form' && $this->uri->segment(3) == 'add_app_reason' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/setup_form/add_app_reason"><i class="fa fa-users"></i> Add App Reason</a></li>
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'setup_form' && $this->uri->segment(3) == 'add_medicines' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/setup_form/add_medicines"><i class="fa fa-users"></i> Add Medicines</a></li>
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'setup_form' && $this->uri->segment(3) == 'add_procedure' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/setup_form/add_procedure"><i class="fa fa-users"></i> Add Procedure</a></li>
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'setup_form' && $this->uri->segment(3) == 'add_diagnosis' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/setup_form/add_diagnosis"><i class="fa fa-users"></i> Add Diagnosis</a></li>
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'setup_form' && $this->uri->segment(3) == 'add_specialty' ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/setup_form/add_specialty"><i class="fa fa-users"></i> Add Specialty</a></li>
                </ul>
            </li>

<!--            <li class="<?php if ($this->uri->segment(1) === 'admin' &&  in_array($this->uri->segment(2), array('online_patient_list') ) ) { echo 'active '; } ?>">
                <a href="<?php echo base_url() ?>admin/online_patient_list">
                    <i class="fa fa-user-md"></i> <span>Online Patient List</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>-->

<!--            <li class="<?php if ($this->uri->segment(1) === 'admin' &&  in_array($this->uri->segment(2), array('online_provider_list') ) ) { echo 'active '; } ?>">
                <a href="<?php echo base_url() ?>admin/online_provider_list">
                    <i class="fa fa-user-md"></i> <span>Online Provider List</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>-->

<!--            <li class="<?php if ($this->uri->segment(1) === 'admin' &&  in_array($this->uri->segment(2), array('messages') ) ) { echo 'active '; } ?>">
                <a href="<?php echo base_url() ?>admin/messages">
                    <i class="fa fa-user-md"></i> <span>Messages</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>-->

<!--            <li class="<?php if ($this->uri->segment(1) === 'admin' &&  in_array($this->uri->segment(2), array('consult_in_progress') ) ) { echo 'active '; } ?>">
                <a href="<?php echo base_url() ?>admin/consult_in_progress">
                    <i class="fa fa-user-md"></i> <span>Consultations In Progress</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>-->

<!--            <li class="<?php if ($this->uri->segment(1) === 'admin' &&  in_array($this->uri->segment(2), array('on_call_provider_list') ) ) { echo 'active '; } ?>">
                <a href="<?php echo base_url() ?>admin/on_call_provider_list">
                    <i class="fa fa-user-md"></i> <span>On Call Providers List</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>-->

<!--            <li class="<?php if ($this->uri->segment(1) === 'admin' &&  in_array($this->uri->segment(2), array('payment_reversal') ) ) { echo 'active '; } ?>">
                <a href="<?php echo base_url() ?>admin/payment_reversal">
                    <i class="fa fa-user-md"></i> <span>Payment Reversal</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>-->

<!--            <li class="<?php if ($this->uri->segment(1) === 'admin' &&  in_array($this->uri->segment(2), array('free_consultation_minutes') ) ) { echo 'active '; } ?>">
                <a href="<?php echo base_url() ?>admin/free_consultation_minutes">
                    <i class="fa fa-user-md"></i> <span>Free Consultation Minutes</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>-->

			<li class="<?php if ($this->uri->segment(1) === 'admin' &&  in_array($this->uri->segment(2), array('invite_doctors') ) ) { echo 'active '; } ?>">
                <a href="<?php echo base_url() ?>admin/invite_doctors">
                    <i class="fa fa-user-md"></i> <span>Invite Doctors</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>
			<li class="<?php if ($this->uri->segment(1) === 'admin' &&  in_array($this->uri->segment(2), array('invite_patients') ) ) { echo 'active '; } ?>">
                <a href="<?php echo base_url() ?>admin/invite_patients">
                    <i class="fa fa-user-md"></i> <span>Invite Patients</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>
			<li class="<?php if ($this->uri->segment(1) === 'admin' &&  in_array($this->uri->segment(2), array('timezones') ) ) { echo 'active '; } ?>">
                <a href="<?php echo base_url() ?>admin/timezones">
                    <i class="fa fa-clock-o"></i> <span>Timezones</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>
			
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>