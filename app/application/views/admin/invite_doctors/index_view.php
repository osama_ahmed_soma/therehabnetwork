<section class="content-header">
    <h1>
        Invite Doctors
        <a href="<?php echo base_url(); ?>Doctors_Sample_Import_CSV.csv" class="btn btn-primary">Download Sample CSV</a>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li class="active">Invite Doctors</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
							<?php if ($this->session->flashdata('csv_import_success')) { ?>
								<p class="alert alert-success"><?php echo $this->session->flashdata('csv_import_success'); ?></p>
							<?php } ?>
							<?php if ($this->session->flashdata('csv_import_error')) { ?>
								<p class="alert alert-danger"><?php echo $this->session->flashdata('csv_import_error'); ?></p>
							<?php } ?>
                            <?php echo form_open_multipart('admin/invite_doctors/import'); ?>
								<div class="row">
									<div class="col-md-6">
										<fieldset class="form-group">
											<label class="form-label semibold">Select CSV File to Import</label>
											<input type="file" class="form-control" name="file" accept=".csv" required="true">
										</fieldset>
										<input type="submit" class="btn btn-primary" value="Import" />
									</div>
								</div>
							
							</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>