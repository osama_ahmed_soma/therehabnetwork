<section class="content-header">
    <h1>
        All Appointments <?php echo $all_appointments_count; ?>
        <?php echo form_open('', ['class' => 'form-inline search_form', 'style' => 'display: inline;']); ?>
            <fieldset class="form-group">
                <input type="text" name="date_search" class="form-control" title="Search By Date" placeholder="Search By Date Time">
            </fieldset>
            <fieldset class="form-group">
                <input type="text" name="provider_search" class="form-control" title="Search By Provider" placeholder="Search By Provider">
            </fieldset>
            <fieldset class="form-group">
                <input type="text" name="patient_search" class="form-control" title="Search By Patient" placeholder="Search By Patient">
            </fieldset>
            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
        </form>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Appointment List</li>
        <li class="active"> All Appointment List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table id="doctors_all" class="table table-bordered table-hover tablesorter customTableSort">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Symptom/Problem</th>
                            <th>Provider</th>
                            <th>Patient</th>
                            <th>Payment</th>
                            <th>Status</th>
                            <th>Action(s)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($all_appointments)): ?>
                            <?php foreach ($all_appointments as $appointment): ?>
                                <tr>
                                    <td><?php echo date("jS F Y", $appointment->start_date_stamp); ?></td>
                                    <td><?php echo date('g:i A', $appointment->start_date_stamp); ?>
                                        - <?php echo date('g:i A', $appointment->end_date_stamp); ?></td>
                                    <td><?php echo $appointment->reason_name; ?></td>
                                    <td><?php echo $appointment->doctor_firstname; ?> <?php echo $appointment->doctor_lastname; ?></td>
                                    <td><?php echo $appointment->patient_firstname; ?> <?php echo $appointment->patient_lastname; ?></td>
                                    <td>$<?php echo $appointment->amount; ?></td>
                                    <td>
                                        <?php
                                        if ($appointment->status == 1) {
                                            echo 'Pending';
                                        } else if ($appointment->status == 3) {
                                            echo 'Payed';
                                        } else if ($appointment->status == 4) {
                                            echo 'Completed';
                                        } else if ($appointment->status == 5) {
                                            echo 'Cancelled';
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-primary" title="Edit" href="#">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="delete btn btn-sm btn-danger all_appointment_remove"
                                               data-appointmentid="<?php echo $appointment->id; ?>" title="Delete">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="7">There is no Appointment.</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Symptom/Problem</th>
                            <th>Provider</th>
                            <th>Patient</th>
                            <th>Payment</th>
                            <th>Status</th>
                            <th>Action(s)</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <?php echo (isset($pagination_link)) ? $pagination_link : '' ; ?>
            </div>
        </div>
    </div>
</section>
<script>
    function removeAllAppointment(id, self) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/appointment/remove_all_appointment',
            data: {appointmentid: id},
            method: "POST"
        }).error(function () {

        }).done(function () {

        }).success(function (data) {
            self.parent().parent().parent().remove();
            notySuccess('You Successfully delete appointment.');
        });
    }
    $(document).ready(function () {
        $('input[name="date_search"]').datetimepicker({
            viewMode: 'days',
            showClear: true,
            useCurrent: false,
            format: 'MM/DD/YYYY HH:mm A'
        });
        $('.search_form').submit(function(e){
            e.preventDefault();
            var self = $(this);
            var date_search = $('input[name="date_search"]').val();
            var provider_search = $('input[name="provider_search"]').val();
            var patient_search = $('input[name="patient_search"]').val();
            if(date_search == '' && provider_search == '' && patient_search == ''){
                notyError("Please fill at least one field.");
                return;
            }
            $.ajax({
                url: '<?php echo base_url(); ?>admin/appointment/search_all_appointments',
                data: self.serialize(),
                method: "POST",
                dataType: "json"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                if(data.bool){
                    $('input[name="date_search"]').val('');
                    $('input[name="provider_search"]').val('');
                    $('input[name="patient_search"]').val('');
                    notySuccess(data.all_appointments.length+" all appointments found.");
                    $('.pagination').hide();
                    var html = '';
                    for(var i = 0; i < data.all_appointments.length; i++){
                        html += '<tr>' +
                            '<td>'+ data.all_appointments[i].start_date_stamp_date +'</td>' +
                            '<td>'+ data.all_appointments[i].start_date_stamp_time +' '+ data.all_appointments[i].end_date_stamp_time +'</td>' +
                            '<td>'+ data.all_appointments[i].reason_name +'</td>' +
                            '<td>'+ data.all_appointments[i].doctor_firstname +' '+ data.all_appointments[i].doctor_lastname +'</td>' +
                            '<td>'+ data.all_appointments[i].patient_firstname +' '+ data.all_appointments[i].patient_lastname +'</td>' +
                            '<td>$'+ data.all_appointments[i].amount +'</td>' +
                            '<td>';
                            if (data.all_appointments[i].status == 1) {
                                html += 'Pending';
                            } else if (data.all_appointments[i].status == 3) {
                                html += 'Payed';
                            } else if (data.all_appointments[i].status == 4) {
                                html += 'Completed';
                            } else if (data.all_appointments[i].status == 5) {
                                html += 'Cancelled';
                            }
                            html += '</td>' +
                            '<td>' +
                            '<div class="btn-group">' +
                            '<a class="btn btn-sm btn-primary" title="Edit" href="#">' +
                            '<i class="fa fa-edit"></i>' +
                            '</a>' +
                            '<a class="delete btn btn-sm btn-danger all_appointment_remove"' +
                            'data-appointmentid="'+ data.all_appointments[i].id +'" title="Delete">' +
                            '<i class="fa fa-remove"></i>' +
                            '</a>' +
                            '</div>' +
                            '</td>' +
                            '</tr>';
                    }
                    $('#doctors_all tbody').html(html);
                    $('.customTableSort').trigger('update');
                } else {
                    notyError("No appointment found.");
                }
            });
            return false;
        });
        $(document).on('click', '.all_appointment_remove', function (e) {
            e.preventDefault();
            var self = $(this);
            var appointmentid = $(this).data('appointmentid');
            notyMessage('Are you sure that you want to DELETE', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            notySuccess('Please wait......');
                            removeAllAppointment(appointmentid, self);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });
    });
</script>