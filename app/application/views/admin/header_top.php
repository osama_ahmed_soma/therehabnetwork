<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url() ?>admin/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>M</b>VD</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>MyVirtual</b>Doctor</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
<!--                        <img src="--><?php //echo get_gravatar($this->session->userdata('admin_sess')['email'], 160) ?><!--" class="user-image" alt="User Image">-->
                        <img src="<?php echo default_image($this->session->userdata('admin_sess')['picture']); ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo $this->session->userdata('admin_sess')['firstname'] . ' ' . $this->session->userdata('admin_sess')['lastname']; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo default_image($this->session->userdata('admin_sess')['picture']); ?>" class="img-circle" alt="User Image">
                            <p>
                                <?php echo $this->session->userdata('admin_sess')['firstname'] . ' ' . $this->session->userdata('admin_sess')['lastname']; ?>
                                <small>Member since <?php date('M Y'); ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo base_url(); ?>admin/profile" class="btn btn-primary btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo base_url(); ?>admin/logout" class="btn btn-primary btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>