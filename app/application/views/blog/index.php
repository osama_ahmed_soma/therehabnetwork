<div class="page-content">
    <div class="container-fluid">
        <section class="blog">
            <!--items-->
            <?php
            if (is_array($all_posts) && count($all_posts) > 0) {
                foreach ($all_posts as $post) {

                    ?>
                    <div class="blog_area">
                        <div class="row">
                            <div class="col-lg-4 col-sm-4">
                                <div class="singBimg">
                                    <img class="img-responsive" src="<?php echo base_url() . $post->blog_image; ?>"
                                         alt="blog_image">
                                    <div class="singDate blogDate">
                                        <?php echo $post->post_date; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-sm-8 blog_text">
                                <h2>
                                    <a href="<?php echo base_url(); ?>blog/post/<?php echo $post->blogpost_id; ?>"><?php echo $post->post_title; ?></a>
                                </h2>
                                <div class="blogMeta">
                                    <span>by </span><a href="#">Dr. Serri L.D</a>
                                    <a href="#"><i class="fa fa-stethoscope"></i>Pediatrics</a>
                                    <a href="#"><i class="fa fa-graduation-cap"></i>MD</a>
                                </div>
                                <p>
                                    <?php echo substr($post->post_content, 0, 250); ?>
                                </p>
                                <a class="read_more"
                                   href="<?php echo base_url(); ?>blog/post/<?php echo $post->blogpost_id; ?>">Read
                                    More</a>
                                <div class="clearfix"></div>
                                <!--                            <input class="pinned_blog_id" type="hidden" value="<?php// echo $post->blogpost_id;
                                ?>">
                                <button id="btnChng" class="btn btn-primary pinbtn">                                    
                                    <i class="font-icon fa fa-thumb-tack"></i>
                                    <a class="btn_pinned" href="#">Pin Blog</a>
                                </button>
                                <button class="btn btn-inline btn-primary-outline btnShare" type="button">
                                    <i class="font-icon fa fa-share-alt"></i>
                                    <a class="btn_share" href="#">Share</a>
                                </button>-->
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } else {
                echo 'Not Found';
            }
            ?>
            <!--close item-->
        </section>
    </div>
</div>


