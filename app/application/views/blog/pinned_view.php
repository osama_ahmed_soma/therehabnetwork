<div class="page-content">
    <div class="container-fluid">
        <section class="blog">
            <!--items-->
            <?php
            if (is_array($pinned_one) && count($pinned_one) > 0) {
                foreach ($pinned_one as $post_pin) {

                    ?>
                    <div class="blog_area">
                        <div class="row">
                            <div class="col-lg-4 col-sm-4">
                                <div class="singBimg">
                                    <img class="img-responsive" src="<?php echo base_url() . $post_pin->blog_image; ?>"
                                         alt="blog_image">
                                    <div class="singDate blogDate">
                                        <?php echo $post_pin->post_date; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-sm-8 blog_text pinnedView">
                                <h2>
                                    <a href="<?php echo base_url(); ?>blog/pinnedpost/<?php echo $post_pin->blogpost_id; ?>"><?php echo $post_pin->post_title; ?></a>
                                </h2>
                                <div class="blogMeta">
                                    <span>by </span><a href="#">Dr. Serri L.D</a>
                                    <a href="#"><i class="fa fa-stethoscope"></i>Pediatrics</a>
                                    <a href="#"><i class="fa fa-graduation-cap"></i>MD</a>
                                </div>
                                <p>
                                    <?php echo substr($post_pin->post_content, 0, 350); ?>
                                </p>
                                <a class="read_more"
                                   href="<?php echo base_url(); ?>blog/pinnedpost/<?php echo $post_pin->blogpost_id; ?>">Read
                                    More</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } else {
                echo '<span class="pError">There is no Data Stored, Please pin your blog! &#128539;</span>';
            }
            ?>
            <!--close item-->
        </section>
    </div>
</div>


