<div class="page-content">
    <div class="container-fluid">
        <section class="single_blog">
            <!--items-->
            <div class="single_blgarea">
                <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        <?php
                        if (is_array($one_blog) && count($one_blog) > 0):
                            foreach ($one_blog as $blog):
                                ?>
                                <div class="singBimg">
                                    <img class="img-responsive" src="<?php echo base_url() . $blog->blog_image; ?>"
                                         alt="singleImage">
                                    <div class="singDate">
                                        <?php echo $blog->post_date; ?>
                                    </div>
                                </div>
                                <h2><?php echo $blog->post_title; ?></h2>
                                <div class="blogMeta singLe">
                                    <span>by </span><a href="#">Dr. Serri L.D</a>
                                    <a href="#"><i class="fa fa-stethoscope"></i>Pediatrics</a>
                                    <a href="#"><i class="fa fa-graduation-cap"></i>MD</a>
                                </div>
                                <div class="sig_desc">
                                    <?php echo $blog->post_content; ?>
                                    <div class="clearfix commOn_marign"></div>
                                </div>
                                <input type="hidden" class="pinned_blog_id" value="<?php echo $blog->blogpost_id; ?>">
                                <button class="btn btn-primary pinbtn btSg1" type="button">
                                    <i class="font-icon fa fa-thumb-tack"></i>
                                    <a class="btn_pinned" href="#">Pin Blog</a>
                                </button>
                                <button class="btn btn-primary unpined">
                                    <i class="font-icon fa fa-thumb-tack"></i>
                                    <a class="btn_pinned" href="#">Unpin Blog</a>
                                </button>
                                <!--                            <button class="btn btn-primary pinbtn btSg2" type="button">
                                                                <i class="font-icon fa fa-share-alt"></i>
                                                                <a class="btn_pinned" href="#">Share</a>
                                                            </button> -->
                                <div class="comment_count">
                                    <h3><?php echo count($comments); ?> comments</h3>
                                </div>
                                <div class="singleComments">
                                    <?php
                                    if (is_array($comments) && count($comments) > 0) {
                                        foreach ($comments as $comment) {
                                            ?>
                                            <div class="blog_authorIn">
                                                <img alt=""
                                                     src="<?php echo base_url(); ?>template_file/img/photo-64-2.jpg">
                                                <h5 class="bauthorTitle"><a
                                                        href="#"><?php echo $comment->user_name; ?></a></h5>
                                                <h4 class="metaDate"><?php echo $comment->time; ?></h4>
                                                <div class="clearfix"></div>
                                                <p><?php echo $comment->comment; ?></p>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
								<?php echo form_open('#', ['class' => 'SigcontactForm']); ?>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="hidden" class="sig_post_id"
                                                   value="<?php echo $blog->blogpost_id; ?>">
                                            <textarea class="commtBox" name="sig_comment" placeholder="Add a comment..."
                                                      required></textarea>
                                        </div>
                                        <div class="col-lg-12">
                                            <button id="postComt" class="btn btn-primary commentSubmit" type="submit">
                                                Post Comment
                                            </button>
                                            <span id="notifiy-bar"></span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </form>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>    

