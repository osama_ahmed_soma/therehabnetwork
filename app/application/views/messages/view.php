	<section class="message_reply clearfix">
    	<?php if ( isset($message) && is_object($message) ):  ?>
    	<div class="mr_msg">
        	<div class="mr_msg_img"><img src="<?php echo default_image($message->sender_image_url); ?>" alt=""></div>
        	<div class="mr_msg_name">
            	<div class="mr_msg_name_title">
					<?php echo $message->sender_name; ?>
					<span><?php echo 'To: ' . $message->receiver_name ?>	</span>
					<span><?php echo 'Subject: ' . $message->subject ?>	</span>
                </div>
                <div class="mr_msg_clock"><i class="fa fa-clock-o"></i> <?php echo date('m/d/Y h:i a ', $message->added); ?></div>
            </div>
            <div class="mr_msg_message"><span> Message:</span> <?php echo $message->message; ?></div>
        </div>
         <?php else: ?>
                <div class="alert alert-danger" role="alert">
                    <strong>Error!</strong> Invalid message selected. Click <a href="<?php echo base_url() . 'messages/'; ?>" class="alert-link">this</a> link to go back to messages.
                </div>
            <?php endif; ?>
    </section>
    
    
    <section class="message_reply clearfix">
    	 <?php if ( isset( $replies ) && is_array( $replies ) && !empty( $replies ) ): ?>
                <?php
                $count = 0;
                foreach ($replies as $reply) { ?>
    	<div class="mr_msg replies clearfix">
        	<div class="mr_reply">
            	<?php echo str_repeat('<i class="fa fa-reply"></i>', ++$count)  ?>
            </div>
            <div class="mr_msg_inner clearfix">
                <div class="mr_msg_img"><img src="<?php echo default_image($message->sender_image_url); ?>" alt=""></div>
                <div class="mr_msg_name">
                    <div class="mr_msg_name_title">
                        <?php echo $reply->sender_name; ?>
						<span><?php echo 'To: ' . $reply->receiver_name ?>	</span>
                        <span><?php echo 'Subject: ' . $reply->subject ?>	</span>
                    </div>
                    <div class="mr_msg_clock"><i class="fa fa-clock-o"></i> <?php echo date('m/d/Y h:i a', $reply->added); ?></div>
                </div>
                <div class="mr_msg_message"><span> Message:</span> <?php echo $reply->message; ?></div>
            </div>
        </div>
           <?php } ?>
            <?php endif; ?>
    </section>

    <?php if ( isset($message, $page) && is_object($message) && $page != 'drafts' ):  ?>
        <div class="container-fluid">
            <br /> <hr />
            <!--<a href="<?php echo base_url() . 'messages/reply/' . $message->id; ?>" class="btn btn-primary pull-left">Reply</a>-->
			<button class="btn btn-primary" data-toggle="modal" data-target="#replyModal"><i class="fa fa-mail-reply"></i> Reply</button>
        </div>
	
	<div class="modal fade" id="replyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button> 
				<div class="row setup-content" id="step-1">
					<div class="col-xs-12">
						<div class="col-md-12">
							<?php echo form_open('messages/reply/' . $message->id, array('id' => 'reply_form')); ?>
							<h3 class="text-center head-appoint"><i class="fa fa-mail-reply"></i> Reply</h3>
							<div id="ajax_errors">
							</div>
							<div class="form-group">
								<label for="formGroupSubject">Subject:</label>
								<input type="text" class="form-control" id="formGroupSubject" placeholder="Subject" name="subject" value="<?php echo set_value('subject', $message->subject ); ?>">
							</div>
							<div class="form-group">
								<label for="formGroupMessage">Message:</label>
								<textarea name="message" class="form-control formGroupMessage" placeholder="Message"><?php echo set_value('message'); ?></textarea>
							</div>
							<div class="form-group">
							<!--<div class="col-md-12">-->
								<button type="button" class="btn btn-primary resetButton">Cancel</button>
								<input type="submit" class="btn btn-primary" value="Send" name="send" />
							</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>  
		</div>
	</div>
		<script type="text/javascript">
			jQuery(function ($) {
				//$("#formGroupRecipients").select2();
				$('.formGroupMessage').wysihtml5();
				$('.resetButton').click(function () {
					$('#replyModal').modal('hide');
				});

				$('#reply_form').submit(function (e) {
					e.preventDefault();
					form = $(this);

					formdata = form.serialize();

					console.log(formdata);

					form.find('#ajax_errors').hide(200);
					form.find('#ajax_errors').html('');

//										return false;
					$.ajax({
						method: form.attr('method'),
						url: form.attr('action'),
						data: formdata,
						dataType: 'json',
					}).success(function (response) {
//											console.log(response)
						if (response.redirect) {
							window.location.href = response.redirect;
						} else {
							form.find('#ajax_errors').html(response.errors);
							form.find('#ajax_errors').show(200);
						}
					});
				});

			});
		</script>

	<?php elseif( isset($message, $page) && is_object($message) && $page == 'drafts' ):  ?>

		<div class="container-fluid">
			<br /> <hr />
			<button class="btn btn-primary" data-toggle="modal" data-target="#replyModal"><i class="fa fa-mail-reply"></i> Send</button>
		</div>

		<div class="modal fade" id="replyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
					<div class="row setup-content" id="step-1">
						<div class="col-xs-12">
							<div class="col-md-12">
								<?php echo form_open('messages/send_draft/' . $message->id, array('id' => 'reply_form')); ?>
								<h3 class="text-center head-appoint"><i class="fa fa-mail-reply"></i> Compose</h3>
								<div id="ajax_errors">
								</div>
								<div class="form-group">
									<label for="formGroupSubject">Subject:</label>
									<input type="text" class="form-control" id="formGroupSubject" placeholder="Subject" name="subject" value="<?php echo set_value('subject', $message->subject ); ?>">
								</div>
								<div class="form-group">
									<label for="formGroupMessage">Message:</label>
									<textarea name="message" class="form-control formGroupMessage" placeholder="Message"><?php echo set_value('message', $message->message); ?></textarea>
								</div>
								<div class="form-group">
									<!--<div class="col-md-12">-->
									<button type="button" class="btn btn-primary resetButton">Cancel</button>
									<input type="submit" class="btn btn-primary" value="Send" name="send" />
								</div>
								<?php echo form_close(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			jQuery(function ($) {
				//$("#formGroupRecipients").select2();
				$('.formGroupMessage').wysihtml5();
				$('.resetButton').click(function () {
					$('#replyModal').modal('hide');
				});

				$('#reply_form').submit(function (e) {
					e.preventDefault();
					form = $(this);

					formdata = form.serialize();

					console.log(formdata);

					form.find('#ajax_errors').hide(200);
					form.find('#ajax_errors').html('');

//										return false;
					$.ajax({
						method: form.attr('method'),
						url: form.attr('action'),
						data: formdata,
						dataType: 'json',
					}).success(function (response) {
//											console.log(response)
						if (response.redirect) {
							window.location.href = response.redirect;
						} else {
							form.find('#ajax_errors').html(response.errors);
							form.find('#ajax_errors').show(200);
						}
					});
				});

			});
		</script>
    <?php endif; ?>