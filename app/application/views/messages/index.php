<div role="tabpanel" class="tab-pane fade in <?php if ($this->uri->segment(2) == 'index' || $this->uri->segment(2) == '' ) echo "active"; ?>" id="tabs-1-tab-1">
    <!--<header class="box-typical-header">
        <div class="tbl-row">
            <div class="tbl-cell tbl-cell-title">
                <h3>My Appointments</h3>
            </div>
        </div>
    </header>-->
    <section class="message_section">
    	<div class="ms_layout_settings clearfix">
        	 <?php
            if ( isset($messages) && !empty($messages) ):
                foreach ( $messages as $message) { ?>
        	<div id="row_<?php echo $message->id; ?>" class="ms_msg<?php if ( $message->opened == 0 ) echo ' odd '; ?> clearfix">
            	<div class="ms_msg_img">
                	<a href="<?php echo base_url().$this->session->userdata('type'); ?>/<?php echo (($this->session->userdata('type') == 'doctor') ? 'patientinfo' : 'doctorinfo'); ?>/<?php echo $message->from_userid; ?>"><img src="<?php echo default_image($message->image_url); ?>" alt=""></a>
                </div>
            	<div class="ms_msg_name">
                	<a href="<?php echo base_url().$this->session->userdata('type'); ?>/<?php echo (($this->session->userdata('type') == 'doctor') ? 'patientinfo' : 'doctorinfo'); ?>/<?php echo $message->from_userid; ?>"><?php echo $message->firstname . ' ' . $message->lastname; ?></a>
<!--                	<span>--><?php //echo $message->city != '' ?  $message->city . ', ' : ''; echo $message->code; ?><!--</span>-->
                </div>
            	<div class="ms_msg_message">
                	<a href="<?php echo base_url() . 'messages/view/' . $message->id; ?>?page=inbox"><?php echo $message->subject; ?></a>
                	<span><?php echo word_limiter( strip_tags( $message->message), 10 ) ?></span>
                </div>
                <div class="ms_msg_date"><?php echo date('j M ', $message->added); ?></div>
                <div class="ms_msg_del"><a href="#" class="deleteMessage" data-id="<?php echo $message->id; ?>" data-title="<?php echo $message->subject; ?>" title="Delete <?php echo $message->subject; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>
            </div>
             <?php }

            else: ?>
               <div class="ms_msg clearfix">No Message Found.</div>
            <?php endif; ?>
             <?php if( isset($pagination_link) ):
            echo $pagination_link;
        endif;
        ?>
        </div>
    </section>
 
    <?php /*?><div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Subject</th>
                <th>Date</th>
                <th>From</th>
                <th>Trash</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ( isset($messages) && !empty($messages) ):
                foreach ( $messages as $message) { ?>
                    <tr class="<?php if ( $message->opened == 0 ) echo 'bold'; ?>" id="row_<?php echo $message->id; ?>">
                        <td><a href="<?php echo base_url() . 'messages/view/' . $message->id; ?>"><?php echo $message->subject; ?></a></td>
                        <td><?php echo date('Y-m-d h:i:s a', $message->added); ?></td>
                        <td><?php echo $message->firstname . ' ' . $message->lastname; ?></td>
                        <td style="text-align: center"><a href="#" class="deleteMessage" data-id="<?php echo $message->id; ?>" data-title="<?php echo $message->subject; ?>" title="Delete <?php echo $message->subject; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                    </tr>
                <?php }

            else: ?>
                <tr><td colspan="4">No Message Found.</td></tr>
            <?php endif; ?>
            </tbody>
        </table>
        <?php if( isset($pagination_link) ):
            echo $pagination_link;
        endif;
        ?>
    </div>
<?php */?>
</div><!--.tab-pane-->