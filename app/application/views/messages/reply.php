<div class="page-content">
    <div class="container-fluid">
		<button class="btn btn-primary" data-toggle="modal" data-target="#composeModal"><i class="fa fa-pencil-square"></i> Compose</button>
        <br /> <hr />
    </div>

    <div class="container-fluid">
        <?php if ( isset( $message ) && is_object( $message ) ): ?>

        <?php echo validation_errors(); ?>

        <?php echo form_open('messages/reply/' . $message->id); ?>

        <div class="form-group">
            <label for="formGroupSubject">Subject</label>
            <input type="text" class="form-control" id="formGroupSubject" placeholder="Subject" name="subject" value="<?php echo set_value('subject', $message->subject); ?>">
        </div>

        <div class="form-group">
            <label for="formGroupMessage">Message</label>
            <textarea name="message" class="form-control" id="formGroupMessage" placeholder="Message"><?php echo set_value('message'); ?></textarea>
        </div>

        <button type="button" class="btn btn-primary" id="resetButton">Cancel</button>
        <input type="submit" class="btn btn-primary" value="Send" name="send"/>


        <?php echo form_close(); ?>
        <?php else: ?>

        <?php endif; ?>
    </div>
</div>

<script type="text/javascript">
    jQuery(function ($) {
        $('#formGroupMessage').wysihtml5();
        $('#resetButton').click(function () {
            var url = '<?php echo base_url() . 'messages/'; ?>';
            window.location = url;
        });
    });
</script>