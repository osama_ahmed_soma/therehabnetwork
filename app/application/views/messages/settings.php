<div role="tabpanel" class="tab-pane fade <?php if ($this->uri->segment(2) == 'settings' ) echo "in active"; ?>" id="tabs-1-tab-4">
    <!--<header class="box-typical-header">
        <div class="tbl-row">
            <div class="tbl-cell tbl-cell-title">
                <h3>My Appointments</h3>
            </div>
        </div>
    </header>-->

    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Serial</th>
                <th>Doctor</th>
                <th>Date</th>
                <th>Time</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>01</td>
                <td>John Smith</td>
                <td>June 01</td>
                <td>5PM-6PM</td>
            </tr>
            </tbody>
        </table>
    </div>
</div><!--.tab-pane-->