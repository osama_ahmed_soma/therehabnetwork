</div><!--.tab-content-->
</section><!--.tabs-section-->
</div>
</div>
<style type="text/css" rel="stylesheet">
    tr.bold > td {
        font-weight: bold;
    }
    a.deleteMessage {
        text-decoration: none;
    }
</style>
<script type="text/javascript">
    jQuery(function($){
        $('.deleteMessage').click(function (e) {
            e.preventDefault();
			
            var message_id = $(this).data('id');
            var message_title = $(this).data('title');
            var msg = 'Are you sure that you want to DELETE <strong>' + message_title + '</strong>';
            notyConfirmWithParam( msg, moveMessageToTrash, message_id );

        });

        $('.restoreMessage').click(function(e) {
            e.preventDefault();

            var message_id = $(this).data('id');
            var message_title = $(this).data('title');
            var msg = 'Are you sure that you want to RESTORE <strong>' + message_title + '</strong>';
            notyConfirmWithParam( msg, restoreMessage, message_id );

        });

        $('.deleteMessagePermanent').click(function(e){
            e.preventDefault();

            var message_id = $(this).data('id');
            var message_title = $(this).data('title');
            var msg = 'Are you sure that you want to Permanently DELETE <strong>' + message_title + '</strong>';
            notyConfirmWithParam( msg, permanentDeleteMessage, message_id );


        });

        function permanentDeleteMessage(message_id) {
            //ajax
            $.ajax({
                url: "<?php echo base_url() . 'messages/ajaxPermanentDeleteMessage/'; ?>" + message_id,
                //data: {process_subdomain: 1},
                method: "POST",
                dataType: "json"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                if ( data.success && data.message ) {
                    //show success message
                    notySuccess(data.message);
                    //remove row
                    $('#row_' + message_id).remove();
                }
                else if ( data.failed || data.error) {
                    notyError(data.message);
                }
            });
        }

        function restoreMessage(message_id) {
            //ajax
            $.ajax({
                url: "<?php echo base_url() . 'messages/ajaxRestoreMessage/'; ?>" + message_id,
                //data: {process_subdomain: 1},
                method: "POST",
                dataType: "json"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                if ( data.success && data.message ) {
                    //show success message
                    notySuccess(data.message);
                    //remove row
                    $('#row_' + message_id).remove();
                }
                else if ( data.failed || data.error) {
                    notyError(data.message);
                }
            });
        }

        function moveMessageToTrash(message_id) {
            //ajax
            $.ajax({
                url: "<?php echo base_url() . 'messages/ajaxMoveMessageToTrash/'; ?>" + message_id,
                //data: {process_subdomain: 1},
                method: "POST",
                dataType: "json"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                if ( data.success && data.message ) {
                    //show success message
                    notySuccess(data.message);
                    //remove row
                    $('#row_' + message_id).remove();
                }
                else if ( data.failed || data.error) {
                    notyError(data.message);
                }
            });
        }

        function notyConfirmWithParam(message, onSuccessCallback, param) {
            var params = {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Ok',
                        onClick: function($noty) {
                            $noty.close();
                            onSuccessCallback(param);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'Cancel',
                        onClick: function($noty) {
                            $noty.close();
                        }
                    }
                ]
            };

            notyMessage(message, 'warning', params);
        }
    });
</script>