<div class="page-content  clearfix">

    <!-- Top Compose and search bar -->
    <div class="container-fluid">
        <section class="navi-top clearfix">
                    <div class="navi-heading">
                        <h2>Message</h2>
                    </div>
                    <div class="btn-section pull-left">
						<button class="btn btn-primary" data-toggle="modal" data-target="#composeModal"><i class="fa fa-pencil-square"></i> Compose</button>
                    </div>
                    <div class="search pull-right">
                    <?php echo form_open('messages', array('class' => 'form-inline pull-xs-right', 'method' => 'GET')); ?>
                        <input type="text" class="form-control" name="search" value="<?php echo set_value('search', isset($search_text) ? $search_text : ''); ?>" />
                        <button class="btn btn-outline-success" type="submit">Search</button>
                    </form>
                </div>
                </section>
        
                
            </div>
            <section class="message-tabs clearfix">
    	<div class="tabs-ul pull-left">
        	<ul>
            	<li>
                <a class="nav-link <?php if ($this->uri->segment(2) == 'index' || $this->uri->segment(2) == '' ) echo "active"; ?>" href="<?php echo base_url() . 'messages/'; ?>">
                    <span class="nav-link-in">
                        <?php if( isset($search) && $search == '1') { echo 'Search Result'; } else { echo 'Inbox'; } ?>
                    </span>
                </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if ($this->uri->segment(2) == 'sent' ) echo "active"; ?>" href="<?php echo base_url() . 'messages/sent'; ?>">
                        <span class="nav-link-in">
                            Sent
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if ($this->uri->segment(2) == 'drafts' ) echo "active"; ?>" href="<?php echo base_url() . 'messages/drafts'; ?>">
                        <span class="nav-link-in">
                            Drafts
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if ($this->uri->segment(2) == 'trash' ) echo "active"; ?>" href="<?php echo base_url() . 'messages/trash'; ?>">
                        <span class="nav-link-in">
                            Trash
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="tab-cont pull-right">
        	1 - 10 of 21 Message
        </div>
    </section>
         <section class="message_send clearfix">    
        <?php echo validation_errors(); ?>

        <?php echo form_open('messages/compose'); ?>
            <?php if ( isset($message) ) { ?>
                <input type="hidden" name="message_id" value="<?php echo $message->id; ?>">
            <?php } ?>
            <div class="form-group">
                <label for="formGroupRecipients">Recipients:</label>
                <select placeholder="Recipients" name="recipients" id="formGroupRecipients" class="form-control">
                    <?php
                    if ( isset( $recipient_list ) && is_array( $recipient_list ) && !empty( $recipient_list ) ) {
                        foreach ( $recipient_list as $recipient ) {
                            echo "<option value='{$recipient->id}'" .  set_select('recipients', $recipient->id, true) .   ">{$recipient->firstname} {$recipient->lastname}</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="formGroupSubject">Subject:</label>
                <input type="text" class="form-control" id="formGroupSubject" placeholder="Subject" name="subject" value="<?php echo set_value('subject', isset($message) ? $message->subject : '' ); ?>">
            </div>

            <div class="form-group">
                <label for="formGroupMessage">Message:</label>
                <textarea name="message" class="form-control" id="formGroupMessage" placeholder="Message"><?php echo set_value('message', isset($message) ? $message->message : ''); ?></textarea>
            </div>

            <button type="button" class="btn btn-primary pull-right" id="resetButton">Cancel</button>
            <input type="submit" class="btn btn-primary pull-right" value="Save" name="save" />
            <input type="submit" class="btn btn-primary pull-right" value="Send" name="send" />




        <?php echo form_close(); ?>
    </section>
</div>
<script type="text/javascript">
    jQuery(function($) {
        //$("#formGroupRecipients").select2();
        $('#formGroupMessage').wysihtml5();
        $('#resetButton').click(function() {
            var url = '<?php echo base_url() . 'messages/'; ?>';
            window.location = url;
        });
    });
</script>