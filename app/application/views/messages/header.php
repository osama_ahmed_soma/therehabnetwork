<div class="page-content">

    <!-- Top Compose and search bar -->
    <div class="container-fluid">
		<section class="navi-top clearfix">
			<div class="navi-heading">
				<h2>Message</h2>
            </div>
            <div class="btn-section pull-left">
				<button class="btn btn-primary" data-toggle="modal" data-target="#composeModal"><i class="fa fa-pencil-square"></i> Compose</button>
            </div>
            <div class="search pull-right">
				<?php echo form_open('messages/' . ($this->uri->segment(3) == null ? $this->uri->segment(2) : ''), array('class' => 'form-inline pull-xs-right', 'method' => 'GET')); ?>
                <input type="text" class="form-control" name="search" value="<?php echo set_value('search', isset($search_text) ? $search_text : ''); ?>" />
                <button class="btn btn-primary" type="submit">Search</button>
                <a class="btn btn-primary" href="<?php echo base_url('messages/' . $this->uri->segment(2)); ?>">Reset</a>
				</form>
			</div>
        </section>


    </div>


	<!-- Large modal -->
	<div class="modal fade bs-example-modal-lg" id="composeModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button> 
					<h3><i class="fa fa-pencil-square"></i> Compose</h3>
				</div>
				<?php echo form_open('messages/compose', array('id' => 'compose_form')); ?>
				<div class="modal-body">
				<!-- Top Compose and search bar -->
				<div id="ajax_errors">
					<?php // echo validation_errors(); ?>
				</div>

				<?php if (isset($message)) { ?>
					<input type="hidden" name="message_id" value="<?php echo $message->id; ?>">
				<?php } ?>
				<div class="form-group">
					<label for="formGroupRecipients">Recipients:</label>
					<select name="recipients" id="formGroupRecipients" class="form-control chosen-select" >
						<?php
						if (isset($recipient_list) && is_array($recipient_list) && !empty($recipient_list)) {
							foreach ($recipient_list as $recipient) {
								echo "<option value='{$recipient->id}'" . set_select('recipients', $recipient->id) . ">{$recipient->firstname} {$recipient->lastname}</option>";
							}
						}
						?>
					</select>
					<!--<input placeholder="Recipients" name="recipients" id="recipients_autocomplete" class="form-control" value="" />-->
				</div>
				<div class="form-group">
					<label for="formGroupSubject">Subject:</label>
					<input type="text" class="form-control" id="formGroupSubject" placeholder="Subject" name="subject" value="<?php echo set_value('subject', isset($message) ? $message->subject : '' ); ?>">
				</div>

				<div class="form-group">
					<label for="formGroupMessage">Message:</label>
					<textarea name="message" class="form-control" id="formGroupMessage" placeholder="Message"><?php echo set_value('message', isset($message) ? $message->message : ''); ?></textarea>
				</div>
				</div>
				<div class="modal-footer">
					<div class="form-group pull-right">
						<input type="submit" class="btn btn-primary" value="Save" name="save" />
						<input type="submit" class="btn btn-primary" value="Send" name="send" />
						<button type="button" class="btn btn-primary" id="resetButton">Cancel</button>
					</div>
				</div>
				<script type="text/javascript">
					jQuery(function ($) {
						$('#formGroupMessage').wysihtml5();
						$('#resetButton').click(function () {
							$('#composeModal').modal('hide');
						});
						value_of_save = '';
						$('#compose_form input[name=save]').click(function (e) {
							e.preventDefault();
							value_of_save = $(this).val();
							$('#compose_form').submit();
						});
						$('#compose_form input[name=send]').click(function (e) {
							e.preventDefault();
							value_of_save = $(this).val();
							$('#compose_form').submit();
						});
						$('#compose_form').submit(function (e) {
							e.preventDefault();
							form = $(this);
							formdata = form.serialize();
							formdata += "&save=" + encodeURIComponent(value_of_save);
//							console.log(formdata);
							form.find('#ajax_errors').hide(200);
							form.find('#ajax_errors').html('');
							$.ajax({
								method: form.attr('method'),
								url: form.attr('action'),
								data: formdata,
								dataType: 'json',
							}).success(function (response) {
//											console.log(response)
								if (response.redirect) {
									window.location.href = response.redirect;
								} else {
									form.find('#ajax_errors').html(response.errors);
									form.find('#ajax_errors').show(200);
								}
							});
						});
						
//						$("#formGroupRecipients").select2({
//							tags: true,
//							tokenSeparators: [',', ' '],
//							placeholder: "Recipients"
//						});


//var recipients = new Bloodhound({
//      datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
//      queryTokenizer: Bloodhound.tokenizers.whitespace,
////      limit: 10,
//      prefetch: {
//        url: '<?php echo base_url('messages/recipients'); ?>',
//        filter: function (list) {
//			console.log(list);
//          return $.map(list, function (name) {
//            return { name: name };
//          });
//        }
//      }
//    });
//
//    recipients.initialize();
//
//    var tagApi = jQuery("#recipients_autocomplete").tagsManager({
////      prefilled: ["Angola", "Laos", "Nepal"],
//	  onlyTagList: true
//    });
//    jQuery("#recipients_autocomplete").typeahead(null, {
//      name: 'recipients',
//      displayKey: 'name',
//      source: recipients.ttAdapter()
//    }).on('typeahead:selected', function (e, d) {
//        tagApi.tagsManager("pushTag", d.name);
//    });

					});
				</script>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>


	<!--	<div class="modal fade" id="composeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button> 
					<div class="row setup-content" id="step-1">
						<div class="col-xs-12">
							<div class="col-md-12">
	<?php echo form_open('messages/compose', array('id' => 'compose_form')); ?>
								<h3 class="text-center head-appoint"><i class="fa fa-pencil-square"></i> Compose</h3>
	
	
								 Top Compose and search bar 
								<div id="ajax_errors">
	<?php // echo validation_errors(); ?>
								</div>
	
	<?php if (isset($message)) { ?>
										<input type="hidden" name="message_id" value="<?php echo $message->id; ?>">
	<?php } ?>
								<div class="form-group">
									<label for="formGroupRecipients">Recipients:</label>
									<select placeholder="Recipients" name="recipients" id="formGroupRecipients" class="form-control">
	<?php
	if (isset($recipient_list) && is_array($recipient_list) && !empty($recipient_list)) {
		foreach ($recipient_list as $recipient) {
			echo "<option value='{$recipient->id}'" . set_select('recipients', $recipient->id, true) . ">{$recipient->firstname} {$recipient->lastname}</option>";
		}
	}
	?>
									</select>
								</div>
								<div class="form-group">
									<label for="formGroupSubject">Subject:</label>
									<input type="text" class="form-control" id="formGroupSubject" placeholder="Subject" name="subject" value="<?php echo set_value('subject', isset($message) ? $message->subject : '' ); ?>">
								</div>
	
								<div class="form-group">
									<label for="formGroupMessage">Message:</label>
									<textarea name="message" class="form-control" id="formGroupMessage" placeholder="Message"><?php echo set_value('message', isset($message) ? $message->message : ''); ?></textarea>
								</div>
								<div class="form-group pull-right">
									<input type="submit" class="btn btn-primary" value="Save" name="save" />
									<input type="submit" class="btn btn-primary" value="Send" name="send" />
									<button type="button" class="btn btn-primary" id="resetButton">Cancel</button>
								</div>
								<script type="text/javascript">
									jQuery(function ($) {
										//$("#formGroupRecipients").select2();
										$('#formGroupMessage').wysihtml5();
										$('#resetButton').click(function () {
											$('#composeModal').modal('hide');
										});
										
										value_of_save = '';
										$('#compose_form input[name=save]').click(function(e){
											e.preventDefault();
											value_of_save = $(this).val();
											$('#compose_form').submit();
										});
										$('#compose_form input[name=send]').click(function(e){
											e.preventDefault();
											value_of_save = $(this).val();
											$('#compose_form').submit();
										});
	
										$('#compose_form').submit(function (e) {
											e.preventDefault();
											form = $(this);
											
											formdata = form.serialize();
	//										formdata.push({ name: 'save', value: value_of_save });
											
											formdata += "&save=" + encodeURIComponent(value_of_save);
											
											console.log(formdata);
											
											form.find('#ajax_errors').hide(200);
											form.find('#ajax_errors').html('');
											
	//										return false;
											$.ajax({
												method: form.attr('method'),
												url: form.attr('action'),
												data: formdata,
												dataType: 'json',
											}).success(function (response) {
	//											console.log(response)
												if (response.redirect) {
													window.location.href = response.redirect;
												} else {
													form.find('#ajax_errors').html(response.errors);
													form.find('#ajax_errors').show(200);
												}
											});
										});
	
									});
								</script>
	<?php echo form_close(); ?>
							</div>
						</div>
					</div>
				</div>  
			</div>
		</div>-->




	<section class="message-tabs clearfix">
		<div class="tabs-ul pull-left">
			<ul>
				<li>
					<a class="nav-link <?php if ($this->uri->segment(2) == 'index' || $this->uri->segment(2) == '' || ( isset($page) && $page == 'inbox' )) echo "active"; ?>" href="<?php echo base_url() . 'messages/'; ?>">
						<span class="nav-link-in">
							<?php // if( isset($search) && $search == '1') { echo 'Search Result'; } else { echo 'Inbox'; } ?>
							Inbox <?php echo (!empty($search_text) && ($this->uri->segment(2) == 'index' || $this->uri->segment(2) == '' )) ? '(Search Result)' : ''; ?>
							<?php echo isset($index_count) ? " ($index_count) " : ''; ?>
						</span>
					</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if ($this->uri->segment(2) == 'sent' || ( isset($page) && $page == 'sent' )) echo "active"; ?>" href="<?php echo base_url() . 'messages/sent'; ?>">
                        <span class="nav-link-in">
                            Sent <?php echo!empty($search_text) && $this->uri->segment(2) == 'sent' ? '(Search Result)' : ''; ?>
							<?php echo isset($sent_count) ? " ($sent_count) " : ''; ?>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if ($this->uri->segment(2) == 'drafts'  || ( isset($page) &&  $page == 'drafts' )) echo "active"; ?>" href="<?php echo base_url() . 'messages/drafts'; ?>">
                        <span class="nav-link-in">
                            Drafts <?php echo!empty($search_text) && $this->uri->segment(2) == 'drafts' ? '(Search Result)' : ''; ?>
							<?php echo isset($drafts_count) ? " ($drafts_count) " : ''; ?>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if ($this->uri->segment(2) == 'trash'  || ( isset($page) && $page == 'trash' )) echo "active"; ?>" href="<?php echo base_url() . 'messages/trash'; ?>">
                        <span class="nav-link-in">
                            Trash <?php echo!empty($search_text) && $this->uri->segment(2) == 'trash' ? '(Search Result)' : ''; ?>
							<?php echo isset($trash_count) ? " ($trash_count) " : ''; ?>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <!--<div class="tab-cont pull-right">
			1 - 10 of 21 Message
        </div>-->
    </section>

	<div class="tab-content">
