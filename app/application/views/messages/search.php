<div role="tabpanel" class="tab-pane fade in <?php if ($this->uri->segment(2) == 'index' || $this->uri->segment(2) == '' ) echo "active"; ?>" id="tabs-1-tab-1">
    <section class="message_section">
        <div class="ms_layout_settings clearfix">
            <?php
            if ( isset($messages) && !empty($messages) ):
                foreach ( $messages as $message) { ?>
                    <div id="row_<?php echo $message->id; ?>" class="ms_msg<?php if ( $message->opened == 0 ) echo ' odd '; ?> clearfix">
                        <div class="ms_msg_img">
                            <a href="<?php echo base_url() . 'messages/view/' . $message->id; ?>"><img src="<?php echo base_url() . $message->from_image_url; ?>" alt=""></a>
                        </div>
                        <div class="ms_msg_name">
                            <a href="<?php echo base_url() . 'messages/view/' . $message->id; ?>"><?php echo $message->from_firstname . ' ' . $message->from_lastname; ?></a>
                            <span><?php echo $message->from_city != '' ?  $message->from_city . ', ' : ''; echo $message->from_code; ?></span>
                        </div>
                        <div class="ms_msg_img">
                            <a href="<?php echo base_url() . 'messages/view/' . $message->id; ?>"><img src="<?php echo base_url() . $message->to_image_url; ?>" alt=""></a>
                        </div>
                        <div class="ms_msg_name">
                            <a href="<?php echo base_url() . 'messages/view/' . $message->id; ?>"><?php echo $message->to_firstname . ' ' . $message->to_lastname; ?></a>
                            <span><?php echo $message->to_city != '' ?  $message->to_city . ', ' : ''; echo $message->to_code; ?></span>
                        </div>
                        <div class="ms_msg_message">
                            <a href="<?php echo base_url() . 'messages/view/' . $message->id; ?>"><?php echo $message->subject; ?></a>
                            <span><?php echo word_limiter( strip_tags( $message->message), 10 ) ?></span>
                        </div>
                        <div class="ms_msg_date"><?php echo date('j M ', $message->added); ?></div>
                        <div class="ms_msg_del"><a href="#" class="deleteMessage" data-id="<?php echo $message->id; ?>" data-title="<?php echo $message->subject; ?>" title="Delete <?php echo $message->subject; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>
                    </div>
                <?php }

            else: ?>
                <tr><td colspan="4">No Message Found.</td></tr>
            <?php endif; ?>
            <?php if( isset($pagination_link) ):
                echo $pagination_link;
            endif;
            ?>
        </div>
    </section>
    <?php /*
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Subject</th>
                <th>Date</th>
                <th>From</th>
                <th>To</th>
                <th>Trash</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ( isset($messages) && !empty($messages) ):
                foreach ( $messages as $message) { ?>
                    <tr class="<?php if ( $message->opened == 0 && $message->to_userid == $this->session->userdata('userid') ) echo 'bold'; ?>" id="row_<?php echo $message->id; ?>">
                        <td><a href="<?php echo base_url() . 'messages/view/' . $message->id; ?>"><?php echo $message->subject; ?></a></td>
                        <td><?php echo date('Y-m-d h:i:s a', $message->added); ?></td>
                        <td><?php echo $message->from_username; ?></td>
                        <td><?php echo $message->to_username; ?></td>
                        <td style="text-align: center"><a href="#" class="deleteMessage" data-id="<?php echo $message->id; ?>" data-title="<?php echo $message->subject; ?>" title="Delete <?php echo $message->subject; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                    </tr>
                <?php }

            else: ?>
                <tr><td colspan="5">No Message Found.</td></tr>
            <?php endif; ?>
            </tbody>
        </table>
        <?php if( isset($pagination_link) ):
            echo $pagination_link;
        endif;
        ?>
    </div>
    */ ?>
</div><!--.tab-pane-->