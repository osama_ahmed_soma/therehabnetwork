<?php

/*

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */

?>
<!DOCTYPE html>
<html ng-app="">
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title>The Rehab Network</title>
    <link rel="icon" href="<?php echo base_url(); ?>template_file/image/fav.png" type="image/ico" sizes="16x16">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template_file/css/custom.css">

    <?php if ($this->session->userdata('logged_in')) { ?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>template_file/css/dashboard_style.css" type="text/css" media="screen" />
    <?php }else {?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>template_file/css/style.css" type="text/css" media="screen" />
    <?php }?>
</head>

<body class=" <?php if ($this->session->userdata('logged_in')) { ?>dashboard <?php }else { echo 'login'; }?>">



