



<div class="mobile-menu-left-overlay"></div>

<nav class="side-menu">

    <div class="single-box-providerz">

        <div class="our-doctor">





            <div class="pic">

                <img id="navigation_profile_image" src="<?php echo default_image($this->session->userdata('image')); ?>" alt="profile img" style="min-width: 90%; min-height: 100px;">

            </div>
            <div class="team_prof">
                <div class="member-name-area">
                    <i class="fa fa-user"></i> <span class="online"></span>
                </div>
                <h3 class="names"><?php echo $this->session->userdata('full_name') ?></h3>
                <div id="doctor_profile_completion_notification_container" class="profile_bar_section">
                    <?php echo getError()['message']; ?>
<!--                    Your profile is <strong>75%</strong> complete-->
<!--                    <div class="progress progress-striped active">-->
<!--                        <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0"-->
<!--                             aria-valuemax="100" style="width: 75%; background-color: #f00;">-->
<!--                            <span class="sr-only">75% Complete</span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    Please complete <a href="#"><strong>My Health</strong></a> questions-->
                </div>
            </div>

        </div>

    </div>



    <ul class="side-menu-list">

        <li class="brown">

	            <span>

	                <i class="fa fa-clock-o"></i>

                    <a class="lbl link" href="<?php echo base_url(); ?>doctor/consultations">Consultations</a>

                </span>

        </li>

        <li class="brown">

	            <span>

	                <i class="fa fa-calendar"></i>

	                <a href="<?php echo base_url(); ?>doctor/" class="lbl link">My Patients</a>

	            </span>

        </li>

        <?php /*?>

        <li class="brown">

                    <span>

                        <i class="font-icon font-icon-clock"></i>

                        <a class="lbl link">Waiting Room</a>

                    </span>

        </li><?php */?>

        <li class="brown <?php if ($this->uri->segment(1) == 'messages') echo "active_nav"; ?>">

                    <span>

                        <i class="fa fa-envelope"></i>

                        <a class="lbl link" href="<?php echo site_url("/messages/"); ?>">Messages

                            <?php

                            $unopened_message_count = getUnopenedMessageCount();

                            if ( $unopened_message_count > 0 ) {

                                echo '<span class="label label-pill label-danger pull-right">' . $unopened_message_count .'</span>';

                            }

                            ?>

                        </a>

                    </span>

        </li>

        <li class="brown">

	            <span>

	                <i class="fa fa-calendar"></i>

	                <a href="<?php echo base_url(); ?>doctor/calendar" class="lbl link">My Calendar</a>

	            </span>

        </li>
        <li class="brown">

	            <span>

	                <i class="fa fa-file-text"></i>

	                <a href="<?php echo base_url(); ?>doctor/records" class="lbl link">My Files</a>

	            </span>

        </li>


        <?php /*?> <li class="brown <?php if ($this->uri->segment(2) == 'calendar') echo "active_nav"; ?>">

                    <span>

                        <i class="font-icon font-icon-calend"></i>

                        <a class="lbl link " href="<?php echo site_url("/doctor/calendar"); ?>">My Calendar</a>

                    </span>

        </li>

        <li class="brown <?php if ($this->uri->segment(2) == 'records') echo "active_nav"; ?>">

                    <span>

                        <i class="font-icon fa fa-file-text-o"></i>

                        <a href="<?php echo site_url('/doctor/records'); ?>" class="lbl link">My Documents</a>

                    </span>

        </li>

        <li class="brown <?php if ($this->uri->segment(2) == 'myfinance') echo "active_nav"; ?>">

                    <span>

                        <i class="font-icon fa fa-file-text-o"></i>

                        <a href="<?php echo base_url(); ?>doctor/myfinance" class="lbl link">Finance</a>

                    </span>

        </li>
<?php */?>




    </ul>

</nav><!--.side-menu-->
<!--<div class="my_custom_error_alert_on_profile_completion">-->
<?php //echo (!getError()['boolean']) ? getError()['message'] : '' ; ?>
<!--</div>-->
