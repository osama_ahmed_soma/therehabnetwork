<header class="header">
    <div class="top_header">
        <div class="container">
            <div class="col-sm-6">
                <div class="row">
                    <ul>
                        <li><a href="#"> <i class="fa fa-envelope-o"></i> Info@therehab.com</a></li>
                        <li class="dark"><a href="#"> <i class="fa fa-phone"></i> 888.958.2885</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 no-pad">
                <div class="row">
                    <ul class="right">
                        <li class="active"><a href="#"> <i class="fa fa-lock"></i>  LOG IN</a></li>
                        <li><a href="#"> Enterprise</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="main_header">
        <div class="container">
            <div class="logo"><a href="#"> <img src="<?php echo base_url(); ?>template_file/images/logo.jpg" alt="" /></a></div>
            <div class="nav">
                <ul>
                    <li><a href="#"> For Providers </a> </li>
                    <li><a href="#"> For Consumers</a> </li>
                    <li class="active"><a href="#">Find a Provider</a> </li>
                    <li><a href="#"> Enterprise</a> </li>
                    <li><a href="#"> Pricing</a> </li>
                </ul>
            </div>
        </div>
    </div>
</header>



<div class="cnt">
    <div class="container">
        <?php echo form_open('password/change', ['class'=>'sign-box']); ?>
        <div class="login_area clearfix">
            <div class="la-top clearfix">
                <h2 class="pull-left">Set New Password</h2>
                <div class="pull-right la-logo"><img src="<?php echo base_url(); ?>template_file/images/white-logo.png" class="img-responsive" /> </div>
            </div>
            <div class="form-area" id="login_page">
                <form>

                    <div class="text-danger"><?php echo validation_errors(); ?></div>

                    <p class="text-danger"><?php if (isset($message) && $message != '') echo $message; ?></p>

                    <?php if ($this->session->flashdata('error')) { ?>
                        <p class="text-danger"><?php echo $this->session->flashdata('error'); ?></p>
                    <?php } ?>

                    <?php if ($this->session->flashdata('captcha_error')) { ?>
                        <p class="text-danger"><?php echo $this->session->flashdata('captcha_error'); ?></p>
                    <?php } ?>

                    <div class="inpt-area"> <input type="text" class="form-control" name="email" placeholder="E-Mail" value="<?php echo set_value('email'); ?>" /></div>
                    <div class="inpt-area">  <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo set_value('password'); ?>" /></div>
                    <div class="inpt-area">  <input type="password" class="form-control" name="passconf" placeholder="Confirm Password" value="<?php echo set_value('passconf'); ?>" /></div>






                    <div class="inpt-area"> <input class="form-control" type="submit" value="Set New Password" /></div>
                    <?php if ($GLOBALS['captcha_required']) { ?>
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="<?php echo CAPTCHA_SITE_KEY; ?>"></div>
                        </div>
                    <?php } ?>

                    <input type="hidden" class="form-control" name="token" value="<?php echo $token; ?>" />
                    <p class="sign-note text-left">New to our website? <a href="<?php echo base_url("login"); ?>">Sign up</a></p>
                </form>
            </div>
        </div>
    </div>
</div>
