<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="page-center page-doctor-signin"><!-- 'page-doctor-signin' class added-->
    <div class="page-center-in">
        <div class="container-fluid" id="login_page">
            <center>
                <img src="<?php echo base_url(); ?>template_file/img/logo_patients_login.png" alt="">
            </center>
			<?php echo form_open('login/doctor', ['class'=>'sign-box']); ?>
                <header class="sign-title">Doctors Login</header>
                <?php echo validation_errors(); ?>
                <p><?php if (isset($message) && $message != '') echo $message; ?></p>
                <p><?php echo $this->session->userdata('email'); ?></p>
                <div class="form-group">
                    <input type="text" class="form-control" name="email" placeholder="E-Mail"/>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password"/>
                </div>
                <div class="form-group">
                    <div class="checkbox float-left">
                        <input type="checkbox" id="signed-in"/>
                        <label for="signed-in">Keep me signed in</label>
                    </div>
                    <div class="float-right reset">
                        <a href="reset-password.html">Reset Password</a>
                    </div>
                </div>
                <input type="hidden" name="type" value="doctor">
                <button type="submit" class="btn btn-inline">Login</button>
                <p class="sign-note">New to our website? <a href="sign-up.html">Sign up</a></p>
                <!--<button type="button" class="close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            </form>
        </div>
    </div>
</div><!--.page-center-->