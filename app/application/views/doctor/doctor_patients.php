<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="page-content">
    <div class="container-fluid">
        <section class="navi-top clearfix">
            <div class="navi-heading">
                <h2>My Patients</h2>
            </div>

        </section>

    </div>
    <script>
        function removePatientFromFavourite(patientID, obj){
            var str = "<?php echo site_url('/doctor/remove_from_favorite'); ?>";
            var d_id = <?php echo $this->session->userdata("userid"); ?>;
            var p_id = patientID;
            $.ajax({
                type: "POST",
                url: str,
                data: {p_id: p_id, d_id: d_id},
                dataType: "json",
                success: function (data) {
                    notySuccess('Removed');
                    $(obj).parent().parent().parent().remove();
                    if(document.getElementsByClassName("doctors_home_area")[0].childElementCount <= 0){
                        var child = document.getElementsByClassName("doctors_home_area")[0];
                        child.parentNode.removeChild(child);
                        $('.no_provider_selecter').html('<div class="col-md-12"><h4>You have no patients!</h4></div>');
                    }
                }
            });
        }
        function removePatientFromFavouriteApproval(patientID, obj){
            var params = {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function($noty) {
                            $noty.close();
                            removePatientFromFavourite(patientID, obj);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function($noty) {
                            $noty.close();
                        }
                    }
                ]
            };
            notyMessage('Are you sure you want to remove this patient?', 'warning', params);
        }
    </script>
    <section class="myprovder_page clearfix">
        <div class="container-fluid no_provider_selecter">
            <?php /*echo "<pre>"; print_r($doctors_patients['general_info']); die(); */ ?>
            <?php if (is_array($doctors_patients['general_info'])): ?>
                <div class="doctors_home_area">
                    <?php foreach ($doctors_patients['general_info'] as $doctors_patient): ?>
                        <div class="col-lg-3 col-md-4 col-sm-6 single-box">
                            <div class="our-doctor">
                                <div class="btn-close"><a href="#" onclick="removePatientFromFavouriteApproval(<?php echo $doctors_patient->user_id; ?>, this)"><i class="fa fa-close"></i></a></div>
                                <div class="pic">
                                    <a href="<?php echo base_url(); ?>doctor/patientinfo/<?php echo $doctors_patient->user_id; ?>">
                                        <img src="<?php echo ($doctors_patient->image_url) ? base_url() . $doctors_patient->image_url : base_url('template_file/img/default_icon.jpeg'); ?>" />
                                        <?php /*?><div class="pic-img" style="background:url() no-repeat;"></div><?php */?>
                                    </a>
                                </div>
                                <div class="team_prof">
                                    <h3 class="names"><?php echo $doctors_patient->firstname . " " . $doctors_patient->lastname; ?></h3>
                                    <!--<div class="rating">
                                                <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                                            </div>
                                            <div class="connection-meter online-sec">
                                                <div class="indicator">
                                                    &nbsp;
                                                </div>
                                                <span>online</span>
                                            </div>
                                            <div class="connection-meter">
                                                <i class="font-icon fa fa-stethoscope" aria-hidden="true"></i>
                                                <?php /*$special = searchFor($provider->userid, $providers['specialization'], 'speciality'); */ ?>
                                                <?php /*if (is_array($special)): */ ?>
                                                    <?php /*$i = 0;
                                                    $count = count($special); */ ?>
                                                    <?php /*foreach ($special as $value): */ ?>
                                                        <?php /*if ($i == ($count - 1)): */ ?>
                                                            <span><?php /*echo $value; */ ?></span>
                                                        <?php /*else: */ ?>
                                                            <span><?php /*echo $value; */ ?>, </span>
                                                        <?php /*endif; */ ?>
                                                        <?php /*$i++; */ ?>
                                                    <?php /*endforeach; */ ?>
                                                <?php /*else: */ ?>
                                                    <span>N/A</span>
                                                <?php /*endif; */ ?>
                                            </div>
                                            <div class="connection-meter">
                                                <i class="font-icon fa fa-graduation-cap" aria-hidden="true"></i>
                                                <?php /*$degree = searchFor($provider->userid, $providers['degree'], 'name'); */ ?>
                                                <?php /*if (is_array($degree) && count($degree) > 0): */ ?>
                                                    <?php /*$i = 0;
                                                    $count = count($degree); */ ?>
                                                    <?php /*foreach ($degree as $value): */ ?>
                                                        <?php /*if ($i == ($count - 1)): */ ?>
                                                            <span><?php /*echo $value; */ ?></span>
                                                        <?php /*else: */ ?>
                                                            <span><?php /*echo $value; */ ?>, </span>
                                                        <?php /*endif; */ ?>
                                                        <?php /*$i++; */ ?>
                                                    <?php /*endforeach; */ ?>
                                                <?php /*else: */ ?>
                                                    <span>N/A</span>
                                                <?php /*endif; */ ?>
                                            </div>
                                            <div class="connection-meter">
                                                <span class="glyphicon glyphicon-map-marker"></span>
                                                <span><?php /*echo ($provider->location) ? $provider->location : 'N/A'; */ ?></span>
                                            </div>-->
                                </div>
                                <div class="view_profile">
                                    <a href="<?php echo base_url(); ?>doctor/patientinfo/<?php echo $doctors_patient->user_id; ?>"
                                       class="btn btn-inline btn-primary">View Profile</a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php else: ?>
                <div class="col-md-12">
                    <h4>You have no patients!</h4>
                </div>
            <?php endif; ?>
        </div>

    </section>
</div>


<!--<div class="page-content dp-new">
    <div class="container-fluid">


        <div class="row">
            <div class="doctors_home_area">
                <?php /*if (is_array($doctors_patients['general_info'])) {
                    foreach ($doctors_patients['general_info'] as $doctors_patient) {
                        */ ?>

                        <div class="col-xs-15-image col-sm-15-image col-md-15-image patients_block">
                            <div><img src="<?php /*echo base_url() . $doctors_patient->image_url; */ ?>" height="150px;"
                                      width="150px;"></div>
                            <h4><?php /*echo $doctors_patient->firstname . " " . $doctors_patient->lastname; */ ?></h4>
                            <a class="btn btn-primary"
                               href="<?php /*echo base_url(); */ ?>doctor/patientinfo/<?php /*echo $doctors_patient->user_id; */ ?>">View
                                Detail</a>
                        </div>

                    <?php /*}
                } else {
                    */ ?>
                    <div class="col-xs-15-image col-sm-15-image col-md-15-image patients_block">
                        <h4>You have no patients!</h4>
                    </div>


                    <?php
/*                } */ ?>

            </div>
        </div>
    </div>
</div>-->
