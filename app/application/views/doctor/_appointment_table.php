<?php /** @var array $appointments */?>
<?php /** @var $isPast */?>
<div class="table-responsive">
    <table class="table table-bordered table-hover">
<!--        <thead><tr>-->
<!--            <th><i class="fa fa-calendar" aria-hidden="true"></i> Date</th>-->
<!--            <th><i class="fa fa-user" aria-hidden="true"></i> Patient</th>-->
<!--            <th><i class="fa fa-stethoscope" aria-hidden="true"></i> Symptom/Problem</th>-->
<!--            <th><i class="fa fa-clock-o" aria-hidden="true"></i> Time</th>-->
<!--            <th><i class="fa fa-spinner" aria-hidden="true"></i> Duration</th>-->
<!--            <th><i class="fa fa-usd" aria-hidden="true"></i> Payment</th>-->
<!--            <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Action</th>-->
<!--        </tr></thead>-->
        <thead><tr>
            <th><i class="fa fa-calendar" aria-hidden="true"></i> Date</th>
            <th><i class="fa fa-clock-o" aria-hidden="true"></i> Time</th>
            <th><i class="fa fa-user" aria-hidden="true"></i> Patient</th>
            <th><i class="fa fa-stethoscope" aria-hidden="true"></i> Symptom/Problem</th>
            <?php if($type == 'past' || $type == 'canceled'): ?>
            <th><i class="fa fa-spinner" aria-hidden="true"></i> Duration</th>
            <?php endif; ?>
            <!--<th><i class="fa fa-usd" aria-hidden="true"></i> Payment</th>-->
            <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Action</th>
        </tr></thead>
        <tbody id="<?php echo $id.'_render'; ?>">
        <?php /*if (count($appointments)) { */?><!--
            <?php /*foreach ($appointments as $appointment) { */?>
                <tr>
                    <td><?php /*echo date("jS F Y", strtotime($appointment->start_date)); */?></td>
                    <td><?php /*echo $appointment->firstname . ' ' . $appointment->lastname; */?></td>
                    <td><?php /*echo $appointment->reason; */?></td>
                    <td><?php /*echo date('g:i A', strtotime($appointment->start_time)); */?>
                        -<?php /*echo date('g:i A', strtotime($appointment->end_time)); */?></td>
                    <td><?php /*echo gmdate("H:i:s", $appointment->duration); */?></td>
                    <td>$100</td>
                    <td><a class="btn btn-inline<?php /*echo $isPast ? ' disabled' : ''*/?>" href="<?php /*echo base_url(); */?>appointment/start?aid=<?php /*echo $appointment->appointment_id; */?>">Start</a></td>
                </tr>
            <?php /*} */?>
        <?php /*} else { */?>
            <tr>
                <td colspan="7">There are no appointments found</td>
            </tr>
        --><?php /*} */?>
        </tbody>
    </table>
</div>