<?php
function searchFor($id, $array, $type)
{
    $result = array();
    foreach ($array as $val) {
        if(is_object($val)){
            if($val->userid == $id){
                $result[] = $val->$type;
            }
        }
    }
    return $result;
}

?>
<div class="results">
<?php foreach ($doctors as $doctor) { ?>

    <div class="provider-list clearfix col-md-6">
        <div class="prls-img">
            <a href="<?php echo base_url() . "patient/doctorinfo/" . $doctor->userid; ?>"> <img src="<?php echo default_image($doctor->image_url); ?>" alt="profile img" /></a>
        </div>
        <div class="prls-details ">
            <div class="offline"></div>
            <h2><?php echo $doctor->firstname . ' ' . $doctor->lastname?></h2>
            <p><span><i class="fa fa-stethoscope"></i>
                    <?php $special = searchFor($doctor->userid, (array) $doctor->specials, 'speciality'); ?>
                    <?php if (is_array($special)): ?>
                        <?php $i = 0;
                        $count_special = count($special); ?>
                        <?php if($count_special > 0): ?>
                            <?php foreach ($special as $value): ?>
                                <?php if ($i == ($count_special - 1)): ?>
                                    <span><?php echo $value; ?></span>
                                <?php else: ?>
                                    <span><?php echo $value; ?>, </span>
                                <?php endif; ?>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <span>N/A</span>
                        <?php endif; ?>
                    <?php else: ?>
                        <span>N/A</span>
                    <?php endif; ?>
                </span> <span><i class="fa fa-mortar-board"></i> <?php echo ($doctor->location) ? $doctor->location : 'N/A'; ?></span></p>
            <p><i class="fa fa-dollar"></i> <strong>Self-Pay Rates: </strong>30 Minutes ($45) /  60 Minutes ($70)</p>
            <p>I am a very good providers with a lot of degree and so much. I have taken my MBBS degree from Melibourne. Read More
                International Medical Institute on Medicine. <a href="#">Read More</a> </p>
            <div class="prls-btn">
                <a href="<?php echo base_url() . "patient/doctorinfo/" . $doctor->userid; ?>" class="btn btn-primary"><i class="fa fa-search"></i> View Full Profile</a>
                <a href="#" class="btn btn-default"><i class="fa fa-clock-o"></i> Session</a>
            </div>
        </div>
    </div>

    <div class="col-lg-15 col-md-4 col-sm-6 single-box hidden">
        <div class="our-doctor">
            <div class="pic">
                <a href="<?php echo base_url() . "patient/doctorinfo/" . $doctor->userid; ?>">
                    <img src="<?php echo default_image($doctor->image_url); ?>" alt="profile img">
                </a>
            </div>
            <div class="team_prof">
                <h3 class="names"><?php echo $doctor->firstname . ' ' . $doctor->lastname?></h3>
                <div class="rating">
                    <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                </div>
                <div class="connection-meter text-center">
                    <i class="font-icon fa fa-stethoscope" aria-hidden="true"></i>
                    <?php $special = searchFor($doctor->userid, (array) $doctor->specials, 'speciality'); ?>
                    <?php if (is_array($special)): ?>
                        <?php $i = 0;
                        $count_special = count($special); ?>
                        <?php if($count_special > 0): ?>
                            <?php foreach ($special as $value): ?>
                                <?php if ($i == ($count_special - 1)): ?>
                                    <span><?php echo $value; ?></span>
                                <?php else: ?>
                                    <span><?php echo $value; ?>, </span>
                                <?php endif; ?>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <span>N/A</span>
                        <?php endif; ?>
                    <?php else: ?>
                        <span>N/A</span>
                    <?php endif; ?>
                </div>
                <div class="connection-meter hidden">
                    <span class="glyphicon glyphicon-map-marker"></span>
                    <span>
                        <?php echo ($doctor->location) ? $doctor->location : 'N/A'; ?>
                    </span>
                </div>
            </div>
            <div class="view_profile">
                <a href="<?php echo base_url() . "patient/doctorinfo/" . $doctor->userid; ?>" class="btn btn-inline btn-primary">View Profile</a>
            </div>
        </div>
    </div>
<?php } ?>
</div>
<div class="clearfix"></div>
<span class="doctor_search_result_bottom_text">Can't find your doctor? Click <a href="" data-toggle="modal" data-target="#invite_doctor_after_search_modal">here</a> to invite him/her to My Virtual Doctor</span>
