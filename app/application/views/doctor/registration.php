<?php /** @var array $states */ ?>

<?php /** @var array $specialties */ ?>

<?php /** @var array $languages */ ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>../css/style.css">
<!--<link rel="stylesheet" href="http://198.57.243.182/~myvirtualdoc/css/style.css">-->

<div class="top-header clearfix">
    <div class="pull-left web-logo"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>template_file/img/logo_patients_login.png" alt=""></a></div>
    <div class="pull-right top-hd-phone"><i class="fa fa-phone"></i> Inquire by Phone: <strong><a href="tel:1-855-80">1-855-80-DOCTOR</a></strong>
    </div>
</div>
<div class="log-back regstr-page">
	<div class="container">
		<div class="logfrom-main">
			<h1>Become a Provider</h1>
			<?php if ($successMessage = $this->session->flashdata('register_doctor_success')): ?>
                <h2 class="simple pra lf-para text-center" style="color:#fff;">Thank you for signing up to My Virtual Doctor</h2>
            <?php elseif ($errorMessage = $this->session->flashdata('register_doctor_error')): ?>
                <h2 class="simple pra lf-para text-center" style="color:#fff;">There is an error.</h2>
            <?php else: ?>
                <h2 class="simple pra lf-para text-center" style="color:#fff;">Discover just how simple the switch to telemedicine can be. </h2>
            <?php endif; ?>
			<div class="logfrom-left">
            	<div class="lf-laptop-rig"><iframe width="560" height="315" src="https://www.youtube.com/embed/4ro3NFAfCJ4" frameborder="0" allowfullscreen></iframe></div>

			</div>
			<div class="signup-right bbm-modal__topbar">
				<h2 class="text-center">Get Started with a <br />Risk-Free 90-Day Trial</h2>
				
				<?php if ($this->session->flashdata('captcha_error')) { ?>
                <p class="text-danger"><?php echo $this->session->flashdata('captcha_error'); ?></p>
				<?php } ?>
				
                 <?php if ($successMessage = $this->session->flashdata('register_doctor_success')): ?>
                    <h1 class="text-center"><i class="fa fa-check" style="font-size: 26px;background-color: #5cb85c;border-radius: 50%;padding: 10px;color: #fff;" aria-hidden="true"></i></h1>
                    <p class="text-center"><?php echo $successMessage; ?></p>
                <?php elseif ($errorMessage = $this->session->flashdata('register_doctor_error')): ?>
                    <h1 class="text-center"><i class="fa fa-times" style="font-size: 26px;background-color: #5cb85c;border-radius: 50%;padding: 10px;color: #fff;" aria-hidden="true"></i></h1>
                    <p class="text-center"><?php echo $errorMessage; ?></p>
                <?php else: ?>
                  


					<?php echo form_open('register/doctorCreate', ['autocomplete' => 'off', 'id'=>'form-doctor-signup']); ?>
						
                        <input type="hidden" name="company_id_ref" value="<?php echo $company_id_ref; ?>">
                        <div class="become-provider clearfix">
                            <div class="become-provider-left clearfix">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="fname" placeholder="First Name" value="<?php echo set_value('fname') ?>" >
                                </div>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="lname" placeholder="Last Name" value="<?php echo set_value('lname') ?>">
                                </div>
              
                                 <div class="col-md-12">
									 <input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo set_value('email') ?>" >
                                </div>
								<?php if ($GLOBALS['captcha_required']) { ?>
                                <div class="col-md-12">
									<div class="g-recaptcha" data-sitekey="<?php echo CAPTCHA_SITE_KEY; ?>"></div>
									<br>
								</div>
								<?php } ?>
                                 <div class="col-md-12 Submit">
                                    <fieldset class="form-group">
                                        <input type="hidden" value="1" name="save">
                                        <button type="submit" name="submit" id="submit" value="Submit" class="btn btn-info sing login btn-block" style="font-size: 23px; height: 54px;    font-weight: 600;    text-transform: uppercase;">
                                            Submit
                                        </button>
                                    </fieldset>
                                </div>
                            </div>
                           
                        </div>
                    </form>
				
<?php endif; ?>


					
		</div>

	</div>
</div>





<?php /*?>


<div class="page-content registration_content">

    <div class="container-fluid">


        <section class="become-provider-main col-md-10" style="    display: table;    margin: 0 auto;    float: none;">
            <div class="container">
                <?php if ($successMessage = $this->session->flashdata('register_doctor_success')): ?>
                    <h1 class="text-center"><i class="fa fa-check" style="font-size: 26px;background-color: #5cb85c;border-radius: 50%;padding: 10px;color: #fff;" aria-hidden="true"></i></h1>
                    <p class="text-center"><?php echo $successMessage; ?></p>
                <?php elseif ($errorMessage = $this->session->flashdata('register_doctor_error')): ?>
                    <h1 class="text-center"><i class="fa fa-times" style="font-size: 26px;background-color: #5cb85c;border-radius: 50%;padding: 10px;color: #fff;" aria-hidden="true"></i></h1>
                    <p class="text-center"><?php echo $errorMessage; ?></p>
                <?php else: ?>
                    <h1>Get Started with a Risk-Free 90-Day Trial</h1>


                    <form autocomplete="off" method="post" enctype="multipart/form-data" id="form-doctor-signup" action="<?php echo base_url(); ?>register/doctorCreate">
                        <input type="hidden" name="company_id_ref" value="<?php echo $company_id_ref; ?>">
                        <div class="become-provider clearfix">
                            <div class="become-provider-left col-lg-7 col-md-12 row clearfix">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="fname" placeholder="First Name">
                                </div>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="lname" placeholder="Last Name">
                                </div>
                                <!--<div class="col-md-6">
                                    <div class="dropdown">

                                        <select class="btn btn-primary dropdown-toggle select2" name="p_title">
                                            <option value="">Professional Title</option>
                                            <option value="Mr.">Mr.</option>
                                            <option value="Ms.">Ms.</option>
                                            <option value="Mrs.">Mrs.</option>
                                            <option value="Dr.">Dr.</option>
                                            <option value="D.O.">D.O.</option>
                                            <option value="Ph.D.">Ph.D.</option>

                                        </select>
                                    </div>
                                </div>-->
<!--                                <div class="col-md-6">-->
<!--                                    <div class="dropdown">-->
<!---->
<!--                                        <select class="btn btn-primary dropdown-toggle select2" name="specialty">-->
<!---->
<!--                                            <option value="">Primary Specialty</option>-->
<!---->
<!--                                            --><?php //foreach ($specialties as $index => $specialty) { ?>
<!---->
<!--                                                <option value="--><?php //echo $index.':'.$specialty; ?><!--">--><?php //echo $specialty; ?><!--</option>-->
<!---->
<!--                                            --><?php //} ?>
<!---->
<!--                                            <option value="-1">Other</option>-->
<!---->
<!--                                        </select>-->
<!--                                        <input type="text" class="form-control" name="otherSpecialty" id="otherSpecialty"-->
<!--                                               style="display: none" placeholder="Other specialty">-->
<!--                                    </div>-->
<!--                                </div>-->
                                <!--<div class="col-md-12">
                                <div class="dropdown">

                                    <select name="practicing" class="select2 btn btn-primary dropdown-toggle">

                                        <option value="">Practicing Since</option>

                                        <?php /*$years = range(date('Y'), 1960); */?>

                                        <?php /*foreach ($years as $year) { */?>

                                            <option value="<?php /*echo $year; */?>"><?php /*echo $year; */?></option>

                                        <?php /*} */?>

                                    </select>
                              <?php /*?>  </div>
                            </div>--><?php */?>
<!--                                <div class="col-md-12 Board-Certified-main">-->
<!--                                    <div class="col-md-6 pull-left  row">-->
<!--                                        <h4>Board-Certified: </h4>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-6 Board-Certified-but pull-right row">-->
<!---->
<!--                                        <div class="btn-group pull-right" data-toggle="buttons">-->
<!--                                            <label class="btn btn-default active" id="healthconditions-yes">Yes-->
<!--                                                <input type="radio" name="board_certified" value="1" id="board_certified" autocomplete="off" chacked="">-->
<!--                                            </label>-->
<!---->
<!--                                            <label class="btn btn-default" id="healthconditions-no">No-->
<!--                                                <input type="radio" name="board_certified" value="0" id="board_certified" autocomplete="off">-->
<!--                                            </label>-->
<!--                                        </div>-->
<!---->
<!---->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <hr>-->
<!--                                <div class="col-md-12">-->
<!--                                    <input type="text" class="form-control" name="years_in_practic" id="years_in_practic"-->
<!--                                           placeholder="Years in Practice" style="">-->
<!---->
<!--                                </div>-->
                             <?php /*?>   <div class="col-md-12">
                                    <input type="email" class="form-control" name="email" placeholder="Email">
                                </div><?php */?>
<!--                                <div class="col-md-6">-->
<!--                                    <input type="text" class="form-control" name="phone"-->
<!--                                           placeholder="Preferred Phone Number">-->
<!--                                </div>-->
<!--                                <div class="col-md-6">-->
<!--                                    <div class="dropdown">-->
<!---->
<!--                                        <select name="state" class="select2 btn btn-primary dropdown-toggle">-->
<!---->
<!--                                            <option value="">Primary State License</option>-->
<!---->
<!--                                            --><?php //foreach ($states as $index => $name) { ?>
<!---->
<!--                                                <option value="--><?php //echo $index.':'.$name; ?><!--">--><?php //echo $name; ?><!--</option>-->
<!---->
<!--                                            --><?php //} ?>
<!---->
<!--                                        </select>-->
<!---->
<!---->
<!--                                    </div>-->
<!--                                </div>-->

<!--                                <div class="col-md-6">-->
<!---->
<!--                                    <fieldset class="form-group">-->
<!---->
<!---->
<!--                                        <select name="other_licenses[]" class="selectpicker" multiple="multiple"-->
<!--                                                title="Other Licenses">-->
<!---->
<!--                                            --><?php //foreach ($states as $index => $name) { ?>
<!---->
<!--                                                <option value="--><?php //echo $index.':'.$name; ?><!--">--><?php //echo $name; ?><!--</option>-->
<!---->
<!--                                            --><?php //} ?>
<!---->
<!--                                            <option value="-1">Other</option>-->
<!---->
<!--                                        </select>-->
<!---->
<!--                                        <input type="text" class="form-control" name="otherLanguage" id="otherLanguage"-->
<!---->
<!--                                               style="display: none" placeholder="Other Licenses">-->
<!---->
<!---->
<!--                                    </fieldset>-->
<!---->
<!--                                </div>-->

                                <!--language area-->
<!--                                <div class="col-md-12">-->
<!---->
<!--                                    <fieldset class="form-group">-->
<!--                                        <select name="language[]" class="selectpicker form-control" multiple="multiple"-->
<!--                                                title="Languages">-->
<!--                                            --><?php //foreach ($languages as $index => $language) { ?>
<!--                                                <option value="--><?php //echo $index.':'.$language; ?><!--">--><?php //echo $language; ?><!--</option>-->
<!--                                            --><?php //} ?>
<!---->
<!--                                        </select>-->
<!---->
<!---->
<!--                                    </fieldset>-->
<!---->
<!--                                </div>-->

<!--                                <hr>-->
                               <?php /*?> <div class="col-md-6 col-md-offset-3 Submit">
                                    <fieldset class="form-group">
                                        <input type="hidden" value="1" name="save">
                                        <button type="submit" name="submit" id="submit" value="Submit" class="btn btn-info" style="margin: 0px; width: 100%;">
                                            Submit
                                        </button>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="become-provider-right col-lg-5 col-md-12">
                                <div class="inquire-phone col-md-12">
                                    <i class="fa fa-phone"></i>
                                    <h5>Inquire by Phone</h5>
                                    <h2>1-855-80-DOCTOR</h2>
                                    <hr>
                                    <img src="/img/inquire-phone.png" class="inqurie-img">
                                </div>
                            </div>
                        </div>
                    </form>
                <?php endif; ?>
            </div>
        </section>


    </div>

</div>
<?php */?>

<script>

    $(document).ready(function () {

        var $form = $("#form-doctor-signup");

        var $language = $form.find('[name=language]');

        $language.on("change", function (e) {

            var values = $(this).val();

            var index = values.indexOf("-1");

            if (index > -1) {

                if (values.length > 1) {

                    values.splice(index, 1);

                    $(this).val(values).trigger("change");

                }


                $("#otherLanguage").show().focus();


            }

        });


        var $specialty = $form.find('[name=specialty]');

        $specialty.on("change", function (e) {

            var values = $(this).val();

            var index = values.indexOf("-1");

            if (index > -1) {


                $("#otherSpecialty").show().focus();


            }

        });


        $form.submit(function () {


            $form.find('.form-group').removeClass('has-danger');


            var $firstName = $form.find('[name=fname]');

            var $lastName = $form.find('[name=lname]');

            var $p_title = $form.find('[name=p_title]');

            var $email = $form.find('[name=email]');

//            var $practicing = $form.find('[name=practicing]');

            var $npi = $form.find('[name=npi]');

            var $stateId = $form.find('[name=state]');


            if ($firstName.val() == "") {

                $firstName.parent().addClass('has-danger');

                notyError("First name is empty");


                return false;

            }


            if ($lastName.val() == "") {

                $lastName.parent().addClass('has-danger');

                notyError("Last name is empty");


                return false;

            }


            if ($email.val() == "") {

                $email.parent().addClass('has-danger');

                notyError("Email is empty");


                return false;

            }


            /*if ($practicing.val() == "") {

                $practicing.parent().addClass('has-danger');

                notyError("Practicing Since is empty");


                return false;

            }*/


            //if ($npi.val() == "") {

            //$npi.parent().addClass('has-danger');

            // notyError("NPI Number is empty");


            //return false;

            //}


            //if ($npi.val().length != 10) {

            //$npi.parent().addClass('has-danger');

            //notyError("NPI Number is wrong");


            // return false;

            //}


            if ($stateId.val() == "") {

                $stateId.parent().addClass('has-danger');

                notyError("Primary State License is empty");


                return false;

            }

        });

    });

</script>
<script>

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip({
            placement: 'top'
        });
    });


</script>

