</div><!--.tab-content-->
</section><!--.tabs-section-->
</div>
</div>
<script type="text/javascript">
    jQuery(function($){
        <?php if ($errorMessage = $this->session->flashdata('message')) { ?>
            notySuccess("<?php echo $errorMessage?>");
        <?php } ?>
        function notyConfirmWithParam(message, onSuccessCallback, param) {
            var params = {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Ok',
                        onClick: function($noty) {
                            $noty.close();
                            onSuccessCallback(param);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'Cancel',
                        onClick: function($noty) {
                            $noty.close();
                        }
                    }
                ]
            };

            notyMessage(message, 'warning', params);
        }
    });
</script>