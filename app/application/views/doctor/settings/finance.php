<div class="page-content"><!--added class 'page-doctor-info'-->
    <div class="container-fluid">
        <section class="tabs-section">
            <div class="tabs-section-nav tabs-section-nav-icons">
                <div class="tbl">
                    <ul class="nav" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#tabs-1-tab-1" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Payment Rate
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabs-1-tab-2" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                    Stripe Information
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div><!--.tabs-section-nav-->

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-1">
					<?php echo form_open('doctor/update_finance'); ?>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">My rate of payment per
                                            time-slot (30 mins)</label>
                                    </fieldset>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <fieldset class="form-group" id="doctor_payment_rate_amount_fieldset">
                                        <input name="payment_rate" class="form-control doctor_award_title"
                                               placeholder="Per time slot rate"
                                               value="<?php echo $doctor->payment_rate; ?>" type="text">
                                    </fieldset>
                                </div>
                                <div class="col-lg-2 col-md-2">
                                    <output><h6>USD per 30 mins</h6></output>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <center>
                                        <input value="payment" type="hidden" name="payment">
                                        <button type="submit" class="btn btn-primary btn-block">
                                            Save
                                        </button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!--.tab-pane-->

                <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-2">
					<?php echo form_open('doctor/update_finance'); ?>
                        <div class="card-block">
                            <div class="row">
                                <div class="condition condition-form">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold" for="exampleInput">Stripe API
                                                    Key</label>
                                                <input class="form-control doctor_award_title"
                                                       placeholder="Stripe API Key" name="stripe_one"
                                                       value="<?php echo $doctor->stripe_one; ?>" type="text">
                                            </fieldset>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold" for="exampleInput">Stripe Account
                                                    Token</label>
                                                <input class="form-control doctor_award_title" name="stripe_two"
                                                       placeholder="Stripe Account Token"
                                                       value="<?php echo $doctor->stripe_two; ?>" type="text">
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <div class="row">
                                <div class="col-md-2 col-lg- col-md-offset-4 col-lg-offset-4">
                                    <center>
                                        <input value="stripe" type="hidden" name="stripe">
                                        <button type="submit" class="btn btn-primary btn-block ">Save
                                        </button>
                                    </center>
                                </div>
                                <div class="col-md-2 col-lg-2">
                                    <center>
                                        <button type="reset" class="btn btn-primary-outline btn-block cancel">Cancel
                                        </button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!--.tab-pane-->

            </div><!--.tab-content-->
        </section><!--.tabs-section-->
    </div>
</div>

<script type="text/javascript">
    var addButtons = document.querySelectorAll('.condition-clear .add');
    var cancelButtons = document.querySelectorAll('.condition-form .cancel');
    var addDcButtons = document.querySelectorAll(".condition-form button.btn.btn-primary.btn-inline:not(.cancel)");

    for (i = 0; i < addButtons.length; i++) {
        addButtons[i].addEventListener('click', function () {
            this.style.display = 'none';
            this.parentNode.parentNode.parentNode.nextElementSibling.style.display = 'block';
        }, false)
    }
    for (i = 0; i < cancelButtons.length; i++) {
        cancelButtons[i].addEventListener('click', function () {
            this.parentNode.parentNode.parentNode.parentNode.querySelector('.add').removeAttribute('style');
            this.parentNode.parentNode.parentNode.removeAttribute('style');
        }, false)
    }
</script>
