<div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-1">
	<?php echo form_open('doctor/update_finance'); ?>
        <div class="card-block">
            <fieldset id="current-plan">
                <dl>
                    <dt class="plan-desc-account"></dt>
                    <dd class="plan-name-account">
                        We hope you are enjoying your Preferred plan.
                        <span class="change-plan-link"><a class="action-initiate" href="/change_plan/9026">Change Plan</a></span>
                    </dd>
                </dl>
                <dl>
                    <dt class="plan-billing"></dt>
                    <dd class="plan-exp">Next bill date: September  9, 2016</dd>
                </dl>
            </fieldset>
        </div>
    </form>
</div><!--.tab-pane-->