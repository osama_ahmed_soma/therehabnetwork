<div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-2">
    <?php echo form_open('doctor/stripe'); ?>
    <div class="card-block">
        <div class="row">
            <?php echo validation_errors(); ?>
            <div class="col-lg-6 col-md-6">
                <fieldset class="form-group">
                    <label class="form-label semibold" for="exampleInput">Stripe Secret Key</label>
                    <input class="form-control doctor_award_title"
                           placeholder="Stripe Secret Key" name="stripe_one"
                           value="<?php echo set_value('stripe_one', $doctor->stripe_one); ?>" type="text">
                </fieldset>
            </div>
            <div class="col-lg-6 col-md-6">
                <fieldset class="form-group">
                    <label class="form-label semibold" for="exampleInput">Stripe Publishable Key</label>
                    <input class="form-control doctor_award_title" name="stripe_two"
                           placeholder="Publishable Key"
                           value="<?php echo set_value('stripe_two', $doctor->stripe_two); ?>" type="text">
                </fieldset>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 col-lg-4 text-left">
                <span style="text-align: center;">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#myModal">Where to find this information?</button>
                </span>
            </div>
            <div class="col-md-8 col-lg-8 text-right">
                <span style="text-align: center;">
                    <input value="stripe" type="hidden" name="stripe">
                    <button type="submit" class="btn btn-primary  ">Save</button>
                </span>
                <span style="text-align: center;">
                    <button type="reset" class="btn btn-primary-outline	 cancel">Cancel</button>
                </span>
            </div>
        </div>
    </div>
    </form>
</div><!--.tab-pane-->
<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                style="color: #fff; opacity: 1; text-shadow: none;"><span
                aria-hidden="true">&times;</span></button>
        <img class="modal-content" src="<?php echo base_url() ?>template_file/img/stripe-instructions.png"
             style="width: 100%; background-color: transparent !important; border: 0px !important;">
    </div>
</div>