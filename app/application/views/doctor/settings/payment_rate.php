<div role="tabpanel" class="tab-pane fade in active page-doctor-info" id="tabs-1-tab-2">
    <section class="upcoming-sessions">
        <div class="card-block">
            <div class="condition condition-clear"><!--added class 'condition-clear'-->
                <div class="row">
                    <span style="text-align: center;">
                        <div class="registration-header">
                            <h4>Consultation Types</h4>
                        </div>
                    </span>
                    <table class="table table-hover doctor_consultation_type_table">
                        <thead>
                        <tr>
                            <th>Duration</th>
                            <th>Rate ($/consultation)</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php if(count($consultation_types) > 0): ?>
                                <?php foreach($consultation_types as $consultation_type): ?>
                                    <tr class="consultation_type_id_<?php echo $consultation_type->id; ?>">
                                        <td class="duration"><?php echo $consultation_type->duration; ?> minutes</td>
                                        <td class="rate">$<?php echo $consultation_type->rate; ?></td>
                                        <td class="table-photo">
                                            <a href=""
                                               data-toggle="modal" data-target="#edit_modal"
                                               data-consultation_typeID="<?php echo $consultation_type->id; ?>"
                                               data-consultation_durationID="<?php echo $consultation_type->duration_id; ?>"
                                               data-consultation_rate="<?php echo $consultation_type->rate; ?>"
                                               class="tabledit-delete-button btn btn-sm btn-default doctor_consultation_type_edit"
                                               style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;"><span
                                                    class="glyphicon glyphicon-pencil"
                                                    data-toggle="tooltip" data-placement="top" title="Edit Consultation Type"></span></a>
                                            <a href=""
                                               data-consultation_typeID="<?php echo $consultation_type->id; ?>"
                                               class="tabledit-delete-button btn btn-sm btn-default doctor_consultation_type_remove"
                                               style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;"><span
                                                    class="glyphicon glyphicon-trash"
                                                    data-toggle="tooltip" data-placement="top" title="Remove Consultation Type"></span></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                            <tr id="no_consultation_type_row">
                                <td colspan="7" style="text-align: left;">
                                    No Consultation Type Found..
                                </td>
                            </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="condition condition-clear">
                <div class="row">
                    <div class="text-right">
                        <button class="add btn btn-primary" id="add-type">Add</button>
                    </div>
                </div>
            </div>

            <div class="condition condition-form" id="form-add-type">
                <div class="row">
                    <div class="col-md-6">
                        <label class="form-label semibold">Duration</label>
                        <select name="duration" class="form-control select2 duration" id="select2duration">
                            <?php foreach ($durations as $duration): ?>
                                <option value="<?php echo $duration->id; ?>"><?php echo $duration->time; ?> Minutes
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="form-label semibold">Rate ($/consultation)</label>
                        <input name="payment_rate" class="form-control doctor_award_title rate"
                               placeholder="Per time slot rate"
                               value="" type="text">
                    </div>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <div class="text-right">

                        <button type="button" class="btn btn-primary btn-inline add_doctor_consultation_type">Save
                        </button>
                        <button type="button" class="btn btn-primary-outline btn-inline cancel">Cancel</button>
                    </div>
                </div>
            </div>
        </div><!--.tab-pane-->
    </section>
</div>
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form name="edit_consultation_type_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Consultation</h4>
                </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-label semibold">Duration</label>
                                <select name="duration_edit" class="form-control select2 duration">
                                    <?php foreach ($durations as $duration): ?>
                                        <option value="<?php echo $duration->id; ?>"><?php echo $duration->time; ?> Minutes
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label semibold">Rate ($/consultation)</label>
                                <input name="payment_rate_edit" class="form-control doctor_award_title rate"
                                       placeholder="Per time slot rate"
                                       value="" type="text">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="consultation_type_id_edit" value="" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    var durations = <?php echo json_encode($durations); ?>;
    function removeThings(obj, url, message){
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>'+url,
            data: {},
            success: function () {
                obj.remove();
                notySuccess(message);
                if($('.doctor_consultation_type_table tbody > tr').length == 0){
                    $('.doctor_consultation_type_table tbody').html('<tr id="no_consultation_type_row">' +
                        '<td colspan="7" style="text-align: left;">' +
                            'No Consultation Type Found..' +
                        '</td>' +
                    '</tr>');
                }
            }
        });
    }
    $(document).ready(function () {
        $('form[name="edit_consultation_type_form"]').submit(function(e){
            e.preventDefault();
            var str = "<?php echo base_url(); ?>doctor/edit_consultation_type";
            var doctor_durationID_edit = $('[name="duration_edit"]').val();
            var duration_text = '';
            var durations = <?php echo json_encode($durations); ?>;
            for (var i = 0; i <= durations.length; i++){
                if(durations[i].id == doctor_durationID_edit){
                    duration_text = durations[i].time;
                    break;
                }
            }
            var doctor_rate_edit = $('[name="payment_rate_edit"]').val();
            var consultation_type_id_edit = $('[name="consultation_type_id_edit"]').val();
            if (doctor_durationID_edit == '' || doctor_rate_edit == '') {
                notyError('Please Fill all fields first');
                return;
            }
            $.ajax({
                type: "POST",
                url: str,
                data: {
                    consultation_type_id: consultation_type_id_edit,
                    payment_rate: doctor_rate_edit,
                    duration: doctor_durationID_edit
                },
                dataType: "json",
                success: function(data){
                    if(!data.bool){
                        notyError(data.message);
                        return;
                    }
                    notySuccess(data.message);
                    $('#edit_modal').modal('hide');
                    $('tr.consultation_type_id_'+consultation_type_id_edit+' td.duration')
                        .text(data.duration_in_minutes+' minutes');
                    $('tr.consultation_type_id_'+consultation_type_id_edit+' td.rate')
                        .text('$'+doctor_rate_edit);
                    $('tr.consultation_type_id_'+consultation_type_id_edit+' a.doctor_consultation_type_edit')
                        .attr('data-consultation_durationID', doctor_durationID_edit)
                        .attr('data-consultation_rate', doctor_rate_edit);
                }
            });
        });
        $(document).on('click', '.doctor_consultation_type_edit', function(e){
            e.preventDefault();
            $('[name="consultation_type_id_edit"]').val($(this).attr('data-consultation_typeID'));
            $('[name="payment_rate_edit"]').val($(this).attr('data-consultation_rate'));
            $('[name="duration_edit"]').select2('val', $(this).attr('data-consultation_durationID'));
        });
        $(document).on('click', '.doctor_consultation_type_remove', function(e){
            e.preventDefault();
            var self = $(this);
            var consultation_typeID = self.attr('data-consultation_typeID');
            notyMessage('You really want to delete this consultation type', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            removeThings(self.parent().parent(), 'doctor/remove_consultation_type/' + consultation_typeID, 'Consultation type Deleted');
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });
        $(".add_doctor_consultation_type").click(function (e) {
            e.preventDefault();
            var str = "<?php echo base_url(); ?>doctor/add_consultation_type";
            var doctor_duration = $('#select2duration').val();
            var duration_text = '';

            if ( $.isArray(durations) ) {
                for (var i = 0; i < durations.length; i++){
                    if(durations[i].id == doctor_duration){
                        duration_text = durations[i].time;
                        break;
                    }
                }
            }

            var doctor_rate = $('input[name="payment_rate"]').val();

            if (doctor_duration == '' || doctor_rate == '') {
                notyError('Please Fill all fields first');
            }
            else {
                $.ajax({
                    type: "POST",
                    url: str,
                    data: {
                        payment_rate: doctor_rate,
                        duration: doctor_duration
                    },
                    dataType: "json",
                    success: function (data) {
                        if(data.bool){
                            notySuccess(data.message);
                            $('.rate').val('');
                            $('#form-add-type').hide();
                            $('#add-type').show();
                            $('#no_consultation_type_row').remove();
                            $('.doctor_consultation_type_table tbody').append('<tr class="consultation_type_id_'+data.consultation_type_id+'">'+
                                '<td class="duration">'+duration_text+' minutes</td>' +
                                '<td class="rate">$'+doctor_rate+'</td>' +
                                '<td class="table-photo">' +
                                    '<a href=""' +
                                        'data-toggle="modal" data-target="#edit_modal"' +
                                        'data-consultation_typeID="'+data.consultation_type_id+'"' +
                                        'data-consultation_durationID="'+doctor_duration+'"' +
                                        'data-consultation_rate="'+doctor_rate+'"' +
                                        'class="tabledit-delete-button btn btn-sm btn-default doctor_consultation_type_edit"' +
                                        'style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;">' +
                                        '<span class="glyphicon glyphicon-pencil"' +
                                            'data-toggle="tooltip" data-placement="top" title="Edit Consultation Type"></span>' +
                                    '</a> ' +
                                    '<a href="" ' +
                                        'data-consultation_typeID="'+data.consultation_type_id+'"' +
                                        'class="tabledit-delete-button btn btn-sm btn-default doctor_consultation_type_remove"' +
                                        'style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;">' +
                                        '<span class="glyphicon glyphicon-trash"' +
                                            'data-toggle="tooltip" data-placement="top" title="Remove Consultation Type"></span>' +
                                    '</a>' +
                                '</td>' +
                            '</tr>');
//                            $.ajax({
//                                type: "POST",
//                                url: '<?php //echo base_url(); ?>//doctor/profile_completion_status',
//                                data: {},
//                                dataType: "json",
//                                success: function (response) {
//                                    if(response.boolean){
//                                        $('.my_custom_error_alert_on_profile_completion').empty();
//                                    } else {
//                                        $('.my_custom_error_alert_on_profile_completion').html(response.message);
//                                    }
//                                }
//                            });
                            profile_completion_error();
                        } else {
                            notyError(data.message);
                        }
                    }
                });
            }
        });
    });
    document.querySelectorAll('.condition-clear .add')[0].addEventListener('click', function () {
        this.style.display = 'none';
        this.parentNode.parentNode.parentNode.nextElementSibling.style.display = 'block';
    }, false);
    document.querySelectorAll('.condition-form .cancel')[0].addEventListener('click', function () {
        this.parentNode.parentNode.parentNode.parentNode.querySelector('.add').removeAttribute('style');
        this.parentNode.parentNode.parentNode.removeAttribute('style');
    }, false);
</script>