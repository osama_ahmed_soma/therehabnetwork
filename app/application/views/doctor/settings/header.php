<div class="page-content">
    <div class="container-fluid">
    	<section class="navi-top clearfix">
			<div class="navi-heading">
				<h2>Settings</h2>
            </div>
        </section>
        <section class="message-tabs clearfix">
            <div class="tabs-ul pull-left">
                <ul>
                    <!--<li>
                       <a class="nav-link <?php /*if ($this->uri->segment(2) == 'settings' ) echo "active"; */?>" href="<?php /*echo base_url() . 'doctor/settings'; */?>">
                            <span class="nav-link-in">
                                Main
                            </span>
                            </a>
                    </li>-->
                    <li class="nav-item">
                       <a class="nav-link <?php if ($this->uri->segment(2) == 'change_password' ) echo "active"; ?>" href="<?php echo base_url() . 'doctor/change_password'; ?>">
                                <span class="nav-link-in">
                                    Change Password
                                </span>
                            </a>
                    </li>
                    <li class="nav-item">
                       <a class="nav-link <?php if ($this->uri->segment(2) == 'payment_rate' ) echo "active"; ?>" href="<?php echo base_url() . 'doctor/payment_rate'; ?>">
                                <span class="nav-link-in">
                                    Consultation Types
                                </span>
                            </a>
                    </li>
                    <li class="nav-item">
                       <a class="nav-link <?php if ($this->uri->segment(2) == 'stripe' ) echo "active"; ?>" href="<?php echo base_url() . 'doctor/stripe'; ?>">
                                <span class="nav-link-in">
                                    Stripe Information
                                </span>
                            </a>
                    </li>
                    <li class="nav-item">
                       <a class="nav-link <?php if ($this->uri->segment(2) == 'waive_fee' ) echo "active"; ?>" href="<?php echo base_url() . 'doctor/waive_fee'; ?>">
                                <span class="nav-link-in">
                                    Patient Billing
                                </span>
                            </a>
                    </li>
                </ul>
            </div>
        </section>
        <?php /*?><section class="tabs-section">
            <div class="tabs-section-nav tabs-section-nav-icons">
                <div class="tbl">
                    <ul class="nav" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link <?php if ($this->uri->segment(2) == 'settings' ) echo "active"; ?>" href="<?php echo base_url() . 'doctor/settings'; ?>">
                            <span class="nav-link-in">
                                Main
                            </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($this->uri->segment(2) == 'change_password' ) echo "active"; ?>" href="<?php echo base_url() . 'doctor/change_password'; ?>">
                                <span class="nav-link-in">
                                    Change Password
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($this->uri->segment(2) == 'payment_rate' ) echo "active"; ?>" href="<?php echo base_url() . 'doctor/payment_rate'; ?>">
                                <span class="nav-link-in">
                                    Payment Rate
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($this->uri->segment(2) == 'stripe' ) echo "active"; ?>" href="<?php echo base_url() . 'doctor/stripe'; ?>">
                                <span class="nav-link-in">
                                    Stripe Information
                                </span>
                            </a>
                        </li>
                        <!--
                        <li class="nav-item">
                            <a class="nav-link <?php if ($this->uri->segment(2) == 'settings' ) echo "active"; ?>" href="<?php echo base_url() . 'messages/settings'; ?>">
                                <span class="nav-link-in">
                                    Settings
                                </span>
                            </a>
                        </li>
                        -->
                    </ul>
                </div>
            </div><!--.tabs-section-nav--><?php */?>
            <div class="tab-content">