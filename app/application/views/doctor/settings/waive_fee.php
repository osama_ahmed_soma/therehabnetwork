<div role="tabpanel" class="tab-pane fade in active page-doctor-info" id="tabs-1-tab-2">
    <section class="upcoming-sessions">
        <div class="card-block">
            <div class="condition condition-clear"><!--added class 'condition-clear'-->
                <div class="row">
                    <span style="text-align: center;">
                        <div class="registration-header">
                            <h4>Patient Billing</h4>
                            <p class="text-muted">This section allows you to waive consultation fee by turning the billing off for certain patients</p>
                        </div>
                    </span>
                    <table class="table table-hover doctor_consultation_type_table">
                        <thead>
                        <tr>
                            <th>Patient</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="waive_fee_patient_list">
                            <?php if(count($waive_fee_patients) > 0): ?>
                                <?php foreach($waive_fee_patients as $waive_fee_patient): ?>
                                    <tr>
                                        <td><?php echo $waive_fee_patient->patient_firstname; ?> <?php echo $waive_fee_patient->patient_lastname; ?></td>
                                        <td>
                                            <a href=""
                                                data-waive_fee_id="<?php echo $waive_fee_patient->id; ?>"
                                                class="tabledit-delete-button btn btn-sm btn-default waive_fee_patient_remove"
                                                style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                            <tr id="no_waive_fee_patient_row">
                                <td colspan="7" style="text-align: left;">
                                    No patient found..
                                </td>
                            <tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="condition condition-clear">
                <div class="row">
                    <div class="text-right">
                        <button class="add btn btn-primary" id="add-type">Add</button>
                    </div>
                </div>
            </div>

            <div class="condition condition-form" id="form-add-type">
                <div class="row">
                    <div class="col-md-12">
                        <label class="form-label semibold">Patients</label>
                        <select name="patient_id" class="form-control select2">
                            <?php foreach ($doctors_patients['general_info'] as $doctors_patient): ?>
                                <option value="<?php echo $doctors_patient->user_id; ?>"><?php echo $doctors_patient->firstname; ?> <?php echo $doctors_patient->lastname; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <div class="text-right">

                        <button type="button" class="btn btn-primary btn-inline add_doctor_waive_fee_patient">Save
                        </button>
                        <button type="button" class="btn btn-primary-outline btn-inline cancel">Cancel</button>
                    </div>
                </div>
            </div>
        </div><!--.tab-pane-->
    </section>
</div>
<script>
    function removeThings(obj, url, message){
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>'+url,
            data: {},
            dataType: "json",
            success: function (data) {
                if(!data.bool){
                    notyError('Patient is not removed. Try again.');
                    return;
                }
                obj.remove();
                notySuccess(message);
            }
        });
    }
    $(document).ready(function(){
        $(document).on('click', '.waive_fee_patient_remove', function(e){
            e.preventDefault();
            var self = $(this);
            var waive_fee_id = self.attr('data-waive_fee_id');
            notyMessage('You really want to delete this patient', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            removeThings(self.parent().parent(), 'doctor/remove_waive_fee/' + waive_fee_id, 'Patient Deleted');
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });
        $(document).on('click', '.add_doctor_waive_fee_patient', function(e){
            e.preventDefault();
            var patient_id = $('select[name="patient_id"]').val();
            if(patient_id == ''){
                notyError('You have to select something. Then you can add.');
                return;
            }
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>doctor/add_patient_waive_fee',
                data: {p_id: patient_id},
                dataType: "json",
                success: function (data) {
                    if(data.bool){
                        notySuccess(data.message);
                        $('#no_waive_fee_patient_row').remove();
                        $('#waive_fee_patient_list').append('<tr>' +
                            '<td>'+data.patient.firstname+' '+data.patient.lastname+'</td>' +
                            '<td>' +
                                '<a href=""' +
                                    'data-waive_fee_id="'+data.waive_fee_id+'"' +
                                    'class="tabledit-delete-button btn btn-sm btn-default waive_fee_patient_remove"' +
                                    'style="float: none;background: transparent;color: #adb7be !important;border: none;padding: 0;">' +
                                    '<span class="glyphicon glyphicon-trash"></span>' +
                                '</a>' +
                            '</td>' +
                        '</tr>');
                        document.querySelectorAll('.condition-form .cancel')[0].parentNode.parentNode.parentNode.parentNode.querySelector('.add').removeAttribute('style');
                        document.querySelectorAll('.condition-form .cancel')[0].parentNode.parentNode.parentNode.removeAttribute('style');
                    } else {
                        notyError(data.message);
                    }
                }
            });
            return false;
        });
    });
    document.querySelectorAll('.condition-clear .add')[0].addEventListener('click', function () {
        this.style.display = 'none';
        this.parentNode.parentNode.parentNode.nextElementSibling.style.display = 'block';
    }, false);
    document.querySelectorAll('.condition-form .cancel')[0].addEventListener('click', function () {
        this.parentNode.parentNode.parentNode.parentNode.querySelector('.add').removeAttribute('style');
        this.parentNode.parentNode.parentNode.removeAttribute('style');
    }, false);
</script>