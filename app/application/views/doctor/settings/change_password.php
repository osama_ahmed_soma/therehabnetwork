<div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-2">
            <section class="card">
                <div class="card-block">
                    <?php echo validation_errors(); ?>

                    <?php echo form_open('doctor/change_password'); ?>
                    <div class="row">
                        <span style="text-align: center;">
                            <div class="registration-header">
                                <h4> Change Your Password</h4>
                            </div>
                        </span>
                        <div class="col-lg-3 col-md-3 col-sm-12">
                            <fieldset class="form-group has-feedback">
                                <label class="form-label semibold">Current Password <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="password" class="form-control toggle_pass" name="current_password"
                                           value="<?php echo set_value('current_password'); ?>"
                                           placeholder="Current Password">
                                    <span class="input-group-addon show_hide_password" style="cursor: pointer;" title="Show Password">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </span>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">New Password <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="password" class="form-control toggle_pass" name="password"
                                           value="<?php echo set_value('password'); ?>"
                                           placeholder="New Password">
                                    <span class="input-group-addon show_hide_password" style="cursor: pointer;" title="Show Password">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </span>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">New Password Again <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="password" class="form-control toggle_pass" name="passconf"
                                       value="<?php echo set_value('passconf'); ?>"
                                       placeholder="New Password Again">
                                    <span class="input-group-addon show_hide_password" style="cursor: pointer;" title="Show Password">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </span>
                                </div>
                            </fieldset>
                        </div>
                        <div
                            class="col-md-2 col-lg-2 col-md-offset-1 col-lg-offset-1 col-sm-12">
                            	<label class="form-label semibold">&nbsp;</label>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Change</button>
                        </div>
                    </div>

                    </form>
                </div>
            </section>
</div>