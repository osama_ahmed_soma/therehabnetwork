<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="page-center">
    <div class="page-center-in">
        <div class="container-fluid">
			<?php echo form_open('login/login_auth', ['class' => 'sign-box']); ?>
                <div class="sign-avatar">
                    <img src="<?php echo base_url(); ?>template_file/img/avatar-sign.png" alt="">
                </div>
                <header class="sign-title">Sign In</header>
                <?php echo validation_errors(); ?>
                <p><?php if (isset($message) && $message != '') echo $message; ?></p>
                <p><?php echo $this->session->userdata('email'); ?></p>
				
				<?php if ($this->session->flashdata('captcha_error')) { ?>
                <p class="text-danger"><?php echo $this->session->flashdata('captcha_error'); ?></p>
				<?php } ?>
				
                <div class="form-group">
                    <input type="text" class="form-control" name="email" placeholder="E-Mail"/>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password"/>
                </div>
                <div class="form-group">
                    <div class="checkbox float-left">
                        <input type="checkbox" id="signed-in"/>
                        <label for="signed-in">Keep me signed in</label>
                    </div>
                    <div class="float-right reset">
                        <a href="reset-password.html">Reset Password</a>
                    </div>
                </div>
				<?php if ($GLOBALS['captcha_required']) { ?>
					<div class="form-group">
						<div class="g-recaptcha" data-sitekey="<?php echo CAPTCHA_SITE_KEY; ?>"></div>
					</div>
				<?php } ?>
                <button type="submit" class="btn btn-rounded">Sign in</button>
                <p class="sign-note">New to our website? <a href="sign-up.html">Sign up</a></p>
                <!--<button type="button" class="close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            </form>
        </div>
    </div>
</div><!--.page-center-->