<style>
    .tablesorter th.header{
        cursor: pointer;
    }
</style>
<div class="<?php if($is_show): ?>page-content<?php endif; ?> profile-patient" >
<section class="upcoming-sessions">
        <div class="card-block">
           <!--added class 'condition-clear'-->
                
                    <span style="text-align: center;">
                        <div class="registration-header-pt-section clearfix">
                            <h4><?php if($is_show): ?>My Patients<?php else: ?>Patient Billing<?php endif; ?></h4>
                            <div class="search pull-right">
                                <form name="search_form" class="form-inline pull-xs-right">
                                    <input type="text" class="form-control" name="search" value="" />
                                    <button class="btn btn-primary" type="submit">Search</button>
                                    <a class="btn btn-primary reset_search_form" href="">Reset</a>
                                </form>
                            </div>
                        </div>
                    </span>
                    
        
        <?php if(!$is_show): ?><p class="text-muted col-lg-12 col-md-12 pull-left">This section allows you to waive consultation fee by turning the billing off for certain patients</p><?php endif; ?>

					
    
        <section class="myprovder_page clearfix upcoming-sessions" style="margin-top: 10px;">
        	 
                <div class="tab-content">
                
                <div role="tabpanel" class="tab-pane fade in active" id="scheduled">
                
                
                <div class="table-responsive">
                <table class="table table-bordered table-hover tablesorter">
                
                <thead><tr>
                <th><i class="fa fa-image" aria-hidden="true"></i> Client</th>
                <th><i class="fa fa-user" aria-hidden="true"></i> Name</th>
                <th><i class="fa fa-phone" aria-hidden="true"></i> Phone Number</th>
                <th><i class="fa fa-calendar" aria-hidden="true"></i> Date of birth</th>
                <th><i class="fa fa-calendar-o" aria-hidden="true"></i> Age</th>
                <th><i class="fa fa-calendar-o" aria-hidden="true"></i> Billing</th>
                <?php if($is_show): ?><th></th><?php endif; ?>
                </tr></thead>
				<tbody id="patients_listing">
					<?php if (!is_array($doctors_patients['general_info'] || count($doctors_patients['general_info']) == 0)) { ?>
					<tr>
						<td colspan="7"><span class="pull-left">You currently do not have any patients in your circle. <a href="#" class="invite_patients_link">Click here to invite patients</a></span></td>
					</tr>
					<?php } ?>
				</tbody>
                </table>
                </div>
                </div>
                
                <div id="patients"></div>
                </div>
             
        </section>    
    </div>
      </section></div>	
<?php if($is_show): ?>
<!-- Large modal -->
<div class="modal fade bs-example-modal-lg" id="composeModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
                <h3><i class="fa fa-pencil-square"></i> Compose</h3>
            </div>
            <?php echo form_open('messages/compose', array('id' => 'compose_form')); ?>
            <div class="modal-body">
                <div id="ajax_errors"></div>
                <!-- Top Compose and search bar -->
                <div class="form-group">
                    <label for="formGroupRecipients">Recipients: <strong class="recipient_full_name"></strong></label>
                    <input type="hidden" name="recipients" value="" id="formGroupRecipients" />
                </div>
                <div class="form-group">
                    <label for="formGroupSubject">Subject:</label>
                    <input type="text" class="form-control" id="formGroupSubject" placeholder="Subject" name="subject" value="<?php echo set_value('subject', isset($message) ? $message->subject : '' ); ?>">
                </div>

                <div class="form-group">
                    <label for="formGroupMessage">Message:</label>
                    <textarea name="message" class="form-control" id="formGroupMessage" placeholder="Message"><?php echo set_value('message', isset($message) ? $message->message : ''); ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group pull-right">
                    <input type="submit" class="btn btn-primary" value="Save" name="save" />
                    <input type="submit" class="btn btn-primary" value="Send" name="send" />
                    <button type="button" class="btn btn-primary" id="resetButton">Cancel</button>
                </div>
            </div>
            <script type="text/javascript">
                jQuery(function ($) {
                    $('#formGroupMessage').wysihtml5();
                    $('#resetButton').click(function () {
                        $('#composeModal').modal('hide');
                    });
                    value_of_save = '';
                    $('#compose_form input[name=save]').click(function (e) {
                        e.preventDefault();
                        value_of_save = $(this).val();
                        $('#compose_form').submit();
                    });
                    $('#compose_form input[name=send]').click(function (e) {
                        e.preventDefault();
                        value_of_save = $(this).val();
                        $('#compose_form').submit();
                    });
                    $('#compose_form').submit(function (e) {
                        e.preventDefault();
                        form = $(this);
                        formdata = form.serialize();
                        formdata += "&save=" + encodeURIComponent(value_of_save);
                        form.find('#ajax_errors').hide(200);
                        form.find('#ajax_errors').html('');
                        $.ajax({
                            method: form.attr('method'),
                            url: form.attr('action'),
                            data: formdata,
                            dataType: 'json',
                        }).success(function (response) {
                            if (response.redirect) {
                                window.location.href = response.redirect;
                            } else {
                                form.find('#ajax_errors').html(response.errors);
                                form.find('#ajax_errors').show(200);
                            }
                        });
                    });
                });
            </script>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<?php endif; ?>
<script>
    function pagination_func(pagination_data){
        $('#patients').pagination({
            dataSource: pagination_data,
            pageSize: 20,
            showPageNumbers: false,
            showNavigator: true,
            callback: function (data, pagination) {
                // template method of yourself
                var html;
                if (data.length > 0){
                    for (var i = 0; i < data.length; i++){
//                        on = red diable
//                        off = blue enable
                        html += '<tr>' +
                            '<td><a href="<?php echo base_url(); ?>doctor/patientinfo/'+data[i].user_id+'" style="background:url('+data[i].image_url+'); display: block; border: 0px;" width="90%"class="preview" id="preview_patient"></a></td>' +
                            '<td><a href="<?php echo base_url(); ?>doctor/patientinfo/'+data[i].user_id+'" style="border: 0px;">'+data[i].firstname+' '+data[i].lastname+'</a></td>' +
                            '<td>'+data[i].phone_mobile+'</td>' +
                            '<td>'+data[i].dob+'</td>' +
                            '<td>'+data[i].age+'</td>' +
                            '<td class="is_waived"><input type="checkbox" data-is_notify="0" data-is_notify_success="0" data-switch-set="state" name="is_waived" '+ ( data[i].is_waived == 'On' ? 'checked' : '' ) +' '+ ( data[i].waive_fee_id != '' ? 'data-waive_fee_id="'+data[i].waive_fee_id+'"' : '' ) +'  data-is_enable="'+ data[i].is_waived +'" data-doctorid="<?php echo $this->session->userdata('userid'); ?>" data-patientid="'+data[i].user_id+'"></td>';
                            <?php if($is_show): ?>html += '<td>' +
                            '<a class="btn btn-inline btn-primary remove_from_favorite_doctor hidden" data-doctorid="<?php echo $this->session->userdata('userid'); ?>" data-patientid="'+data[i].user_id+'" title="Remove From Favorite" href="" style="background-color: #072c54;">' +
                            '<i class="fa fa-star" style="color: #ffc800;"></i>' +
                            '</a>' +
                            '<a class="btn btn-inline btn-primary compose_modal_init" data-recipient_id="'+data[i].user_id+'" data-recipient_full_name="'+data[i].firstname+' '+data[i].lastname+'" data-toggle="modal" data-target="#composeModal">' +
                            '<i class="fa fa-comments"></i> Message' +
                            '</a> ' +
                            '<a class="btn btn-inline btn-danger " href="<?php echo base_url(); ?>doctor/patientinfo/'+data[i].user_id+'">' +
                            '<i class="fa fa-tasks"></i> Profile' +
                            '</a>' +
                            '</td>';<?php endif; ?>
                            html += '</tr>';
                    }
                } else {
                    html += '<tr>' +
                        '<td colspan="7">You have no patients!</td>' +
                        '</tr>';
                }
                $('#patients_listing').html(html);
                $('.tablesorter').tablesorter({
                    headers: {
                        0: {
                            sorter: false
                        },
                        5: {
                            sorter: false
                        }<?php if($is_show): ?>,
                        6: {
                            sorter: false
                        }<?php endif; ?>
                    }
                });
                $('[name="is_waived"]').bootstrapSwitch({
                    size: 'mini',
                    onSwitchChange: function(event, state){
                        var self = $(this);
                        var doctorid = self.attr('data-doctorid');
                        var patientid = self.attr('data-patientid');
                        if(self.attr('data-is_enable') == 'On'){
                            if(self.attr('data-is_notify_success') == 0){
                                notyMessage('Disable billing for patient?', 'warning', {
                                    buttons: [
                                        {
                                            addClass: 'btn btn-primary',
                                            text: 'Yes',
                                            onClick: function ($noty) {
                                                $noty.close();
                                                self.attr('data-is_enable', 'Off');
                                                self.bootstrapSwitch(self.data("switch-set"), false);
                                                $.ajax({
                                                    method: 'POST',
                                                    url: '<?php echo base_url(); ?>doctor/add_patient_waive_fee',
                                                    data: {p_id: patientid},
                                                    dataType: 'json',
                                                }).success(function (data) {
                                                    if(data.bool){
                                                        notySuccess(data.message);
                                                        self.attr('data-waive_fee_id', data.waive_fee_id);
                                                    } else {
                                                        self.attr('data-is_enable', 'On');
                                                        self.bootstrapSwitch(self.data("switch-set"), true);
                                                        notyError(data.message);
                                                    }
                                                });
                                            }
                                        },
                                        {
                                            addClass: 'btn btn-danger',
                                            text: 'No',
                                            onClick: function ($noty) {
                                                $noty.close();
                                                self.attr('data-is_enable', 'On');
                                                self.bootstrapSwitch(self.data("switch-set"), true);
                                            }
                                        }
                                    ]
                                });
                                self.attr('data-is_notify_success', 1);
                            } else {
                                self.attr('data-is_notify_success', 0);
                            }
                        } else {
                            if(self.attr('data-is_notify') == 0) {
                                notyMessage('Enable billing for patient?', 'warning', {
                                    buttons: [
                                        {
                                            addClass: 'btn btn-primary',
                                            text: 'Yes',
                                            onClick: function ($noty) {
                                                $noty.close();
                                                self.attr('data-is_notify', 0);
                                                self.attr('data-is_enable', 'On');
                                                self.bootstrapSwitch(self.data("switch-set"), true);
                                                $.ajax({
                                                    method: 'POST',
                                                    url: '<?php echo base_url(); ?>doctor/remove_waive_fee/' + self.attr('data-waive_fee_id'),
                                                    data: {},
                                                    dataType: 'json',
                                                }).success(function (data) {
                                                    if (data.bool) {
                                                        notySuccess('Billing has been enabled for the selected patient');
                                                    } else {
                                                        self.attr('data-is_enable', 'Off');
                                                        self.bootstrapSwitch(self.data("switch-set"), false);
                                                        self.attr('data-waive_fee_id', '');
                                                        notyError('Billing is not enable for the selected patient. Try again.');
                                                    }
                                                });
                                            }
                                        },
                                        {
                                            addClass: 'btn btn-danger',
                                            text: 'No',
                                            onClick: function ($noty) {
                                                $noty.close();
                                                self.attr('data-is_enable', 'Off');
                                                self.bootstrapSwitch(self.data("switch-set"), false);
                                            }
                                        }
                                    ]
                                });
                                self.attr('data-is_notify', 1);
                            } else {
                                self.attr('data-is_notify', 0);
                            }
                        }
                    }
                });
            }
        });
    }
    $(document).ready(function(){
        <?php if($is_show): ?>$(document).on('click', '.compose_modal_init', function(){
            var self = $(this);
            $('input[name="recipients"]').val(self.attr('data-recipient_id'));
            $('.recipient_full_name').text(self.attr('data-recipient_full_name'));
        });<?php endif; ?>
        $('form[name="search_form"]').submit(function(e){
            e.preventDefault();
            var search_input = $('[name="search"]').val();
            if(search_input == ''){
                notyError('Please fill search field first.');
                return;
            }
            $.ajax({
                method: 'GET',
                url: '<?php echo base_url(); ?>doctor/search_doctor_patients/' + search_input,
                data: {},
                dataType: 'json',
            }).success(function (data) {
                if (!data.bool) {
                    notyError(data.message);
                    return;
                }
                notySuccess(data.message);
                pagination_func(data.doctors_patients.general_info);
                $('.tablesorter').trigger('update');
            });
            $('[name="search"]').val('');
        });
        $('.reset_search_form').click(function(e){
            e.preventDefault();
            $.ajax({
                method: 'GET',
                url: '<?php echo base_url(); ?>doctor/search_doctor_patients/all',
                data: {},
                dataType: 'json',
            }).success(function (data) {
                if (!data.bool) {
                    notyError(data.message);
                    return;
                }
                notySuccess(data.message);
                pagination_func(data.doctors_patients.general_info);
                $('.tablesorter').trigger('update');
            });
            $('[name="search"]').val('');
        });
        pagination_func(<?php echo json_encode($doctors_patients['general_info']); ?>);
    });
</script>
