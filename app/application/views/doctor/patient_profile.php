<script type="application/javascript">
    function paginationHtmlRendering(data, type){
        /*console.log(data);
         return;*/
        var html;
        if (typeof data !== 'undefined' && data.length > 0){
            for (var i = 1; i <= data.length; i++){
                html += '<tr>' +
                    '<td>'+i+'</td>' +
                    '<td>'+data[i-1].firstname+' '+data[i-1].lastname+'</td>' +
                    '<td>'+data[i-1].start_date+'</td>' +
                    '<td>'+data[i-1].start_time+'-'+data[i-1].end_time+'</td>' +
                    '<td>'+data[i-1].primary_symptom+'</td>' +
                    '<td>Audio</td>' +
                    '<td>Yes</td>' +
                    '<td>$'+data[i-1].amount+'</td>' +
                    '</tr>';
            }
        } else {
            html += '<tr> \
                <td colspan="7">No Appointment Found</td> \
            </tr>';
        }
        return html;
    }
</script>
<div class="page-content">
    <div class="container-fluid">
        <section class="navi-top clearfix">
            <div class="navi-heading">
                <h2>Patient Profile</h2>
                <a href="<?php echo base_url(); ?>/doctor" class="btn btn-inline btn-primary pull-right">back</a>
            </div>
        </section>

        <section class="message-tabs clearfix">
            <div class="tabs-ul pull-left">
                <ul>
                    <li class="nav-item">
                        <a class="nav-link active" href="#tabs-1-tab-1" role="tab" data-toggle="tab">
                            <span class="nav-link-in">Profile</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="#tabs-1-tab-2" role="tab" data-toggle="tab">
                            <span class="nav-link-in">Scheduled</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="#tabs-1-tab-3" role="tab" data-toggle="tab">
                            <span class="nav-link-in">Past</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="#tabs-1-tab-4" role="tab" data-toggle="tab">
                            <span class="nav-link-in">Files</span>
                        </a>
                    </li>
                    <!--<li class="nav-item">
                       <a class="nav-link " href="#tabs-1-tab-5" role="tab" data-toggle="tab">
                           <span class="nav-link-in">Notes</span>
                       </a>
                   </li>-->
                </ul>
            </div>

        </section>


        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-1">
                <div class="card-block">
                    <?php if (is_array($patients)) {
                        foreach ($patients as $users) { ?>

                            <div class="col-md-3 dc-pro-img">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img src="<?php echo default_image($users->image_url); ?>" alt="" width="100%" id="preview_patient" />
                                    </div>
                                    <div class="col-md-12 dc-pro-nam">
                                        <h3 style="font-weight: 600"><?php echo $users->firstname . " " . $users->lastname; ?></h3>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-9 table-dc-pro">
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <label class="form-label semibold">Age</label>
                                    </div>
                                    <?php
                                    $from = new DateTime($users->dob);
                                    $to = new DateTime('today');
                                    //echo $from->diff($to)->y;
                                    ?>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <output class=""><?php echo (($from->diff($to)->y == 0) ? 'N/A' : $from->diff($to)->y.' years'); ?></output>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <label class="form-label semibold"><?php /*?>Gender<?php */?></label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <output class=""><?php //echo $users->gender; ?></output>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <label class="form-label semibold">DOB</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <output class=""><?php echo ($users->dob) ? $users->dob : 'No Information Available' ; ?></output>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <label class="form-label semibold"><?php /*?>Ethnicity<?php */?></label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <output class=""><?php //echo $users->ethnicity; ?></output>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <label class="form-label semibold">Height</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <output
                                            class=""><?php echo ($users->height_ft || $users->height_inch) ? $users->height_ft . " ft " . $users->height_inch . " inch" : 'No Information Available' ; ?></output>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <label class="form-label semibold"><?php /*?>Eye Color<?php */?></label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <output class=""><?php //echo $users->eye_color; ?></output>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <label class="form-label semibold">Weight</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <output class=""><?php echo ($users->weight) ? $users->weight . " lbs" : 'No Information Available' ; ?></output>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <label class="form-label semibold"><?php /*?>Hair Color<?php */?></label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <output class=""><?php //echo $users->hair_color; ?></output>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <label class="form-label semibold">Gender<?php /*?>Bloog Type<?php */?></label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <output class=""><?php echo genderFullWord($users->gender); ?><?php //echo $users->blood_type; ?></output>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <label class="form-label semibold"></label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <output class=""></output>
                                    </div>
                                </div>
                            </div>

                        <?php }
                    } ?>
                </div>

                <div class="card-block medical-section-dc">

                    <div class="row">

                        <div class="col-md-3">

                            <label class="form-label semibold">Medical Conditions</label>
                            <ul style="list-style: none">
                                <?php
                                if(!empty($healths)){
                                    if (is_array($healths)) {
                                        foreach ($healths as $health) { ?>
                                            <li><?php echo $health->condition_term; ?></li>
                                        <?php }
                                    }
                                } else {
                                    echo 'No Information Available';
                                }
                                ?>
                            </ul>
                        </div>

                        <div class="col-md-3">
                            <label class="form-label semibold">Medication</label>
                            <ul style="list-style: none">
                                <?php
                                if(!empty($medications)){
                                    if (is_array($medications)) {
                                        foreach ($medications as $medication) { ?>
                                            <li><?php echo $medication->condition_term; ?></li>
                                        <?php }
                                    }
                                } else {
                                    echo 'No Information Available';
                                }

                                ?>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <label class="form-label semibold">Allergies</label>
                            <ul style="list-style: none">
                                <?php
                                if(!empty($allergies)){
                                    if (is_array($allergies)) {
                                        foreach ($allergies as $allergy) { ?>
                                            <li><?php echo $allergy->condition_term; ?></li>
                                        <?php }
                                    }
                                } else {
                                    echo 'No Information Available';
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <label class="form-label semibold">Surgeries</label>
                            <ul style="list-style: none">
                                <?php
                                if(!empty($surgeries)){
                                    if (is_array($surgeries)) {
                                        foreach ($surgeries as $surgery) { ?>
                                            <li><?php echo $surgery->condition_term; ?></li>
                                        <?php }
                                    }
                                } else {
                                    echo 'No Information Available';
                                }
                                ?>
                            </ul>
                        </div>

                    </div>

                </div>

            </div><!--.tab-pane-->

            <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-2">
                <!--<header class="box-typical-header">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-title">
                            <h3>My Appointments</h3>
                        </div>
                    </div>
                </header>-->

                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Serial</th>
                            <th>
                                Doctor
                            </th>
                            <th>
                                Date
                            </th>
                            <th>Time</th>
                            <th>Symptom/Problem</th>
                            <th>Consultation</th>
                            <th>Concent To Treat</th>
                            <th>Payment</th>
                        </tr>
                        </thead>
                        <tbody id="futureAppointments_render">
                        <?php if(count($futureAppointments) > 0): ?>
                            <?php $i = 0; ?>
                            <?php foreach ($futureAppointments as $appointment):
                                $futureAppointments[$i]->start_date = date("jS F Y", ($appointment->start_date_stamp));
                                $futureAppointments[$i]->start_time = date('g:i A', ($appointment->start_date_stamp));
                                $futureAppointments[$i]->end_time = date('g:i A', ($appointment->end_date_stamp));
                                $i++;
                            endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="8">No Appointment Found</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <div id="futureAppointments"></div>
                    <script>
                        $(document).ready(function () {
                            $('#futureAppointments').pagination({
//                                dataSource: [1, 2, 3, 4, 5, 6, 7, ... , 50],
                                dataSource: <?php echo json_encode($futureAppointments); ?>,
                                pageSize: 20,
                                showPageNumbers: false,
                                showNavigator: true,
                                callback: function (data, pagination) {
                                    // template method of yourself
                                    var html = paginationHtmlRendering(data, 'future');
                                    $('#futureAppointments_render').html(html);
                                }
                            });
                        });
                    </script>
                </div>
            </div><!--.tab-pane-->

            <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-3">
                <!--<header class="box-typical-header">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-title">
                            <h3>My Appointments</h3>
                        </div>
                    </div>
                </header>-->

                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Serial</th>
                            <th>
                                Doctor
                            </th>
                            <th>
                                Date
                            </th>
                            <th>Time</th>
                            <th>Symptom/Problem</th>
                            <th>Consultation</th>
                            <th>Concent To Treat</th>
                            <th>Payment</th>
                        </tr>
                        </thead>
                        <tbody id="pastAppointments_render">
                        <?php if(count($pastAppointments) > 0): ?>
                            <?php $i = 0; ?>
                            <?php foreach ($pastAppointments as $appointment):
                                $pastAppointments[$i]->start_date = date("jS F Y", ($appointment->start_date_stamp));
                                $pastAppointments[$i]->start_time = date('g:i A', ($appointment->start_date_stamp));
                                $pastAppointments[$i]->end_time = date('g:i A', ($appointment->end_date_stamp));
                                $i++;
                            endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="8">No Appointment Found</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <div id="pastAppointments"></div>
                    <script>
                        $(document).ready(function () {
                            $('#pastAppointments').pagination({
                                dataSource: <?php echo json_encode($pastAppointments); ?>,
                                pageSize: 20,
                                hello: 'world',
                                showPageNumbers: false,
                                showNavigator: true,
                                callback: function (data, pagination) {
                                    // template method of yourself
                                    var html = paginationHtmlRendering(data, 'past');
                                    $('#pastAppointments_render').html(html);
                                }
                            });
                        });
                    </script>
                </div>

            </div><!--.tab-pane-->

            <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-4">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
							<tr>
								<th><i class="fa fa-check" aria-hidden="true"></i> File Name</th>
								<!--<th><i class="fa fa-file" aria-hidden="true"></i> Filename</th>-->
								<th><i class="fa fa-server" aria-hidden="true"></i> Size</th>
								<th><i class="fa fa-upload" aria-hidden="true"></i> Shared Date</th>
								<th class="text-center"><i class="fa fa-thumb-tack" aria-hidden="true"></i> Action</th>
							</tr>
                        </thead>
                        <tbody>
                        <?php
                        if (count($patient_files) > 0) {
                            foreach ($patient_files as $file) {
//								var_debug($file);
								$type = ($file->filetype) ? $file->filetype : pathinfo($file->filename, PATHINFO_EXTENSION);
								$icons = $this->config->item('file_icons');
								?>
								<tr data-to-delete="<?php echo $file->id; ?>">
									<td><i class="<?php echo (isset($icons[$type])) ? $icons[$type] : $icons['default']; ?>"></i> <a title="Download" target="_blank" href="<?php echo base_url('file/download_document/' . $file->id . '/' . $patient_id); ?>"> <span class="title"><?php echo $file->title; ?></span></a></td>
									<!--<td><?php echo $file->filename; ?></td>-->
									<td><?php echo humanFileSize($file->filesize); ?></td>
									<td class="text-left"><?php echo date('m/d/Y', strtotime($file->date_time)); ?></td>
									<td class="text-center">
										<a title="Download" class="btn btn-primary" target="_blank" href="<?php echo base_url('file/download_document/' . $file->id . '/' . $patient_id); ?>"><span><i class="fa fa-download"></i></span></a>
									</td>
								</tr>
                                <?php
                            }
                        } else { ?>
								<tr><td colspan="3">No files shared</td></tr>
						<?php } ?>
                        </tbody>
                    </table>
                </div>
            </div><!--.tab-pane-->

            <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-5">
                <header class="box-typical-header">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-title">
                            <h36>Notes to be done later:</h36>
                        </div>
                    </div>
                </header>

                <!--<div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                Upload History
                            </th>
                            <th>
                                Upload Date
                            </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><a href="#">amended-catalog.xls</a></td>
                            <td class="text-center">July 22</td>
                            <td class="text-center"><a href="#"><span><i class="fa fa-times"></i></span></a></td>
                        </tr>
                        <tr>
                            <td><a href="#">amended-catalog.xls</a></td>
                            <td class="text-center">July 22</td>
                            <td class="text-center"><a href="#"><span><i class="fa fa-times"></i></span></a></td>
                        </tr>
                        <tr>
                            <td><a href="#">amended-catalog.xls</a></td>
                            <td class="text-center">July 22</td>
                            <td class="text-center"><a href="#"><span><i class="fa fa-times"></i></span></a></td>
                        </tr>
                        <tr>
                            <td><a href="#">amended-catalog.xls</a></td>
                            <td class="text-center">July 22</td>
                            <td class="text-center"><a href="#"><span><i class="fa fa-times"></i></span></a></td>
                        </tr>
                        <tr>
                            <td><a href="#">amended-catalog.xls</a></td>
                            <td class="text-center">July 22</td>
                            <td class="text-center"><a href="#"><span><i class="fa fa-times"></i></span></a></td>
                        </tr>
                        <tr>
                            <td><a href="#">amended-catalog.xls</a></td>
                            <td class="text-center">July 22</td>
                            <td class="text-center"><a href="#"><span><i class="fa fa-times"></i></span></a></td>
                        </tr>
                        <tr>
                            <td><a href="#">amended-catalog.xls</a></td>
                            <td class="text-center">July 22</td>
                            <td class="text-center"><a href="#"><span><i class="fa fa-times"></i></span></a></td>
                        </tr>
                        <tr>
                            <td><a href="#">amended-catalog.xls</a></td>
                            <td class="text-center">July 22</td>
                            <td class="text-center"><a href="#"><span><i class="fa fa-times"></i></span></a></td>
                        </tr>
                        <tr>
                            <td><a href="#">amended-catalog.xls</a></td>
                            <td class="text-center">July 22</td>
                            <td class="text-center"><a href="#"><span><i class="fa fa-times"></i></span></a></td>
                        </tr>
                        </tbody>
                    </table>-->
            </div>

        </div><!--.tab-pane-->
    </div><!--.tab-content-->
    </section><!--.tabs-section-->
</div>
</div>

