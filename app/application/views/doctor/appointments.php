<?php /** @var array $pastAppointments */ ?>

<?php /** @var array $futureAppointments */ ?>

<?php
/*echo "<pre>";
print_r($pastAppointments);
print_r($futureAppointments);
print_r($patients);
print_r($reasons);
die();*/
?>
<!-- Syed Testing -->
<div class="page-content">

    <div class="container-fluid">
        <div class="home_welcome">Welcome to My Virtual Doctor
            <strong><?php echo $this->session->userdata('full_name') ?>!</strong></div>


        <section class="navi-top">
            <!--<div class="navi-heading">
                <h2>Consultations</h2>
            </div>-->
            <div class="btn-section text-right hidden">
                <a class="btn btn-danger" data-toggle="modal" data-target="#doctorMakeAppointment"><i
                        class="fa fa-clock-o"></i> Schedule</a>
                <a class="btn btn-success"><i class="fa fa-users"></i> Invite</a>
            </div>
        </section>
        <!--        --><?php //if ( !empty($futureAppointments) ): ?>
        <section class="upcoming-sessions">
            <div class="navi-heading">
                <h2><i class="fa fa-paper-plane" aria-hidden="true"></i> Upcoming Consultations</h2>
            </div>
            <strong>
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="scheduled">

                        <?php

                        /*$array = $futureAppointments;

                        $futureAppointments = [];
                        $pendingAppointments = [];
                        if (count($array)) {
                            foreach ($array as $appointment) {
                                if($appointment->app_status == 1){
                                    $pendingAppointments[] = $appointment;
                                } else {
                                    $futureAppointments[] = $appointment;
                                }
                            }

                        }*/
                        if (count($futureAppointments)) {
                            $i = 0;
                            foreach ($futureAppointments as $appointment) {
                                $futureAppointments[$i]->start_date = date("F jS, Y", $appointment->start_date_stamp);
                                $futureAppointments[$i]->start_time = date('g:i A', $appointment->start_date_stamp);
                                $futureAppointments[$i]->end_time = date('g:i A', $appointment->end_date_stamp);

                                $dteStart = new DateTime(date('Y-m-d H:i:s', $appointment->start_date_stamp));
                                $dteEnd = new DateTime(date('Y-m-d H:i:s', $appointment->end_date_stamp));
                                $dteDiff = $dteStart->diff($dteEnd);

                                $i++;
                            }

                        }


                        ?>

                        <?php $this->view('doctor/_appointment_table', [

                            'appointments' => $futureAppointments,

                            'id' => 'futureAppointments',

                            'type' => 'future',

                            'isPast' => false

                        ]); ?>

                    </div><!--.tab-pane-->

                    <!--<div id="futureAppointments" class="pagination-section pull-right">
                        <div class="show-num">Showing 1 to 10 of 57 entries</div>
                        <div class="pagination-slide">
                            <div class="next"><i class="fa fa-caret-left" aria-hidden="true"></i></div>
                            <div class="prev"><i class="fa fa-caret-right" aria-hidden="true"></i></div>
                        </div>
                    </div>-->

                    <div id="futureAppointments"></div>


                </div><!--.tab-content--></strong>
        </section>
        <!--        --><?php //endif; ?>
        <?php if (!empty($pendingAppointments)): ?>
            <section class="upcoming-sessions">
                <div class="navi-heading">
                    <h2><i class="fa fa-refresh" aria-hidden="true"></i> Pending Consultations</h2>
                </div>
                <strong>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="scheduled">
                            <?php
                            /*echo "<pre>";
                            print_r($pendingAppointments);
                            die();*/

                            if (count($pendingAppointments)) {
                                $i = 0;
                                foreach ($pendingAppointments as $appointment) {
                                    $pendingAppointments[$i]->start_date = date("F jS, Y", $appointment->start_date_stamp);
                                    $pendingAppointments[$i]->start_time = date('g:i A', $appointment->start_date_stamp);
                                    $pendingAppointments[$i]->end_time = date('g:i A', $appointment->end_date_stamp);

                                    $dteStart = new DateTime(date('Y-m-d H:i:s', $appointment->start_date_stamp));
                                    $dteEnd = new DateTime(date('Y-m-d H:i:s', $appointment->end_date_stamp));
                                    $dteDiff = $dteStart->diff($dteEnd);
                                    $pendingAppointments[$i]->duration = $dteDiff->format("%H:%I:%S");

                                    $i++;
                                }

                            }

                            /*if (count($pendingAppointments)) {
                                $i = 0;
                                foreach ($pendingAppointments as $appointment) {
                                    $pendingAppointments[$i]->start_date = date("jS F Y", $appointment->start_date_stamp);
                                    $pendingAppointments[$i]->start_time = date('g:i A', $appointment->start_date_stamp);
                                    $pendingAppointments[$i]->end_time = date('g:i A', $appointment->end_date_stamp);

                                    $dteStart = new DateTime(date('Y-m-d H:i:s', $appointment->start_date_stamp));
                                    $dteEnd   = new DateTime(date('Y-m-d H:i:s', $appointment->end_date_stamp));
                                    $dteDiff  = $dteStart->diff($dteEnd);
                                    $pendingAppointments[$i]->duration = $dteDiff->format("%H:%I:%S");

                                    $pendingAppointments[$i]->id = $appointment->appointment_id;
                                    $i++;
                                }

                            }*/

                            ?>
                            <?php $this->view('doctor/_appointment_table', [

                                'appointments' => $pendingAppointments,

                                'id' => 'pendingAppointments',

                                'type' => 'pending',

                                'isPast' => false,

                            ]); ?>
                        </div>
                        <div id="pendingAppointments"></div>
                    </div>
                </strong>
            </section>
        <?php endif; ?>
        <?php if (!empty($canceledAppointments)): ?>
            <section class="upcoming-sessions">
                <div class="navi-heading">
                    <h2><i class="fa fa-refresh" aria-hidden="true"></i> Cancelled Consultations</h2>
                </div>
                <strong>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="scheduled">
                            <?php
                            if (count($canceledAppointments)) {
                                $i = 0;
                                foreach ($canceledAppointments as $appointment) {
                                    $canceledAppointments[$i]->start_date = date("F jS, Y", $appointment->start_date_stamp);
                                    $canceledAppointments[$i]->start_time = date('g:i A', $appointment->start_date_stamp);
                                    $canceledAppointments[$i]->end_time = date('g:i A', $appointment->end_date_stamp);

                                    $dteStart = new DateTime(date('Y-m-d H:i:s', $appointment->start_date_stamp));
                                    $dteEnd = new DateTime(date('Y-m-d H:i:s', $appointment->end_date_stamp));
                                    $dteDiff = $dteStart->diff($dteEnd);
                                    $canceledAppointments[$i]->duration = $dteDiff->format("%H:%I:%S");

                                    $i++;
                                }

                            }

                            ?>
                            <?php $this->view('doctor/_appointment_table', [

                                'appointments' => $canceledAppointments,

                                'id' => 'canceledAppointments',

                                'type' => 'canceled',

                                'isPast' => false,

                            ]); ?>
                        </div>
                        <div id="canceledAppointments"></div>
                    </div>
                </strong>
            </section>
        <?php endif; ?>
        <?php if (!empty($pastAppointments)): ?>
            <section class="upcoming-sessions">
                <div class="navi-heading">
                    <h2><i class="fa fa-reply" aria-hidden="true"></i> Past Consultations</h2>
                </div>
                <strong>
                    <div class="tab-content">


                        <div role="tabpanel" class="tab-pane fade in active" id="past">

                            <?php

                            if (count($pastAppointments)) {
                                $i = 0;
                                foreach ($pastAppointments as $appointment) {
                                    $pastAppointments[$i]->start_date = date("F jS, Y", $appointment->start_date_stamp);
                                    $pastAppointments[$i]->start_time = date('g:i A', $appointment->start_date_stamp);
                                    $pastAppointments[$i]->end_time = date('g:i A', $appointment->end_date_stamp);

                                    /*$dteStart = new DateTime(date('Y-m-d H:i:s', $appointment->start_date_stamp));
                                    $dteEnd   = new DateTime(date('Y-m-d H:i:s', $appointment->end_date_stamp));
                                    $dteDiff  = $dteStart->diff($dteEnd);
                                    $pastAppointments[$i]->duration = $dteDiff->format("%H:%I:%S");*/
                                    $pastAppointments[$i]->duration = gmdate("H:i:s", $appointment->duration);

                                    $i++;
                                }

                            }

                            ?>

                            <?php $this->view('doctor/_appointment_table', [

                                'appointments' => $pastAppointments,

                                'id' => 'pastAppointments',

                                'type' => 'past',

                                'isPast' => true,

                            ]); ?>

                        </div><!--.tab-pane-->

                        <!--<div id="pastAppointments" class="pagination-section pull-right">
                            <div class="show-num">Showing 1 to 10 of 57 entries</div>
                            <div class="pagination-slide">
                                <div class="next"><i class="fa fa-caret-left" aria-hidden="true"></i></div>
                                <div class="prev"><i class="fa fa-caret-right" aria-hidden="true"></i></div>
                            </div>
                        </div>-->

                        <div id="pastAppointments"></div>

                    </div><!--.tab-content--></strong>
            </section>
        <?php endif; ?>
        <?php /*?> <section class="tabs-section hidden">

            <div class="tabs-section-nav tabs-section-nav-icons">

                <div class="tbl">

                    <ul class="nav" role="tablist">

                        <li class="nav-item">

                            <a class="nav-link active" href="#scheduled" role="tab" data-toggle="tab">

                                <span class="nav-link-in">Scheduled</span>

                            </a>

                        </li>

                        <li class="nav-item">

                            <a class="nav-link" href="#past" role="tab" data-toggle="tab">

                                <span class="nav-link-in">Past</span>

                            </a>

                        </li>



                    </ul>

                </div>

            </div><!--.tabs-section-nav-->



            <div class="tab-content">

                <div role="tabpanel" class="tab-pane fade in active" id="scheduled">

                    <?php $this->view('doctor/_appointment_table', [

                        'appointments' => $futureAppointments,

                        'isPast' => false,

                    ]); ?>

                </div><!--.tab-pane-->



                <div role="tabpanel" class="tab-pane fade" id="past">

                    <?php $this->view('doctor/_appointment_table', [

                        'appointments' => $pastAppointments,

                        'isPast' => true,

                    ]); ?>

                </div><!--.tab-pane-->

            </div><!--.tab-content-->
        </section><!--.tabs-section--><?php */ ?>

    </div>

</div>
</div>


<script>

    jQuery(document).ready(function ($) {

        function paginationHtmlRendering(data, type) {
            var html;
            for (var i = 0; i < data.length; i++) {
                var reason_name = data[i].reason_name;
                if (data[i].reason_id == '13') {
                    reason_name = data[i].reason;
                }
                html += '<tr> \
                <td>' + data[i].start_date + '</td> \
                <td>' + data[i].start_time + '-' + data[i].end_time + '</td> \
                <td>' + data[i].firstname + ' ' + data[i].lastname + '</td> \
                <td>' + reason_name + '</td>';
                if (type == 'canceled' || type == 'past') {
                    html += '<td>' + data[i].duration + '</td>';
                }
                html += '<td>';
                if (type == 'future') {
//                    html += '<a class="btn btn-inline btn-success request_to_start_consult_page" data-appointment_id="'+data[i].appointment_id+'" href="" >Start</a>';
                    html += '<a class="btn btn-inline btn-success" data-appointment_id="' + data[i].appointment_id + '" href="<?php echo base_url(); ?>/appointment/start?aid=' + data[i].appointment_id + '" >Start</a>';
                }
                if (type == 'pending') {
                    html += '<button class="btn btn-inline disabled" disabled="disabled" >Pending Approval</button>';
                }
                if (type == 'canceled') {
                    html += '<button class="btn btn-inline doctor_reschedule_patient_appointment" data-id="' + data[i].id + '">Reschedule</button>';
                }
                if (type == 'past') {
                    html += '<a class="btn btn-inline" href="<?php echo base_url(); ?>appointment/report?aid=' + data[i].appointment_id + '" >Report</a>';
                }
                html += '</td> \
                </tr > ';
            }
            return html;
        }

        $('#futureAppointments').pagination({
            dataSource: <?php echo json_encode($futureAppointments); ?>,
            pageSize: 5,
            showPageNumbers: false,
            showNavigator: true,
            beforeRender: function () {
                $('#futureAppointments_render').html('<tr>' +
                    '<td colspan="7" style="text-align: left;">There are no upcoming appointments</td>' +
                    '</tr>');
                return false;
            },
            callback: function (data, pagination) {
                // template method of yourself
                var html = paginationHtmlRendering(data, 'future');
                $('#futureAppointments_render').html(html);
            }
        });
        <?php if(!empty($pendingAppointments)): ?>
        $('#pendingAppointments').pagination({
            dataSource: <?php echo json_encode($pendingAppointments); ?>,
            pageSize: 5,
            showPageNumbers: false,
            showNavigator: true,
            callback: function (data, pagination) {
                // template method of yourself
                var html = paginationHtmlRendering(data, 'pending');
                $('#pendingAppointments_render').html(html);
            }
        });
        <?php endif; ?>
        <?php if(!empty($canceledAppointments)): ?>
        $('#canceledAppointments').pagination({
            dataSource: <?php echo json_encode($canceledAppointments); ?>,
            pageSize: 5,
            showPageNumbers: false,
            showNavigator: true,
            callback: function (data, pagination) {
                // template method of yourself
                var html = paginationHtmlRendering(data, 'canceled');
                $('#canceledAppointments_render').html(html);
            }
        });
        <?php endif; ?>
        <?php if(!empty($pastAppointments)): ?>
        $('#pastAppointments').pagination({
            dataSource: <?php echo json_encode($pastAppointments); ?>,
            pageSize: 5,
            showPageNumbers: false,
            showNavigator: true,
            callback: function (data, pagination) {
                // template method of yourself
                var html = paginationHtmlRendering(data, 'past');
                $('#pastAppointments_render').html(html);
            }
        });
        <?php endif; ?>

    });
</script>