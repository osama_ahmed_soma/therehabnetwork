<?php /** @var array $states */ ?>
<?php /** @var array $specialties */ ?>
<?php /** @var array $languages */ ?>
<div class="page-content registration_content">
    <div class="container-fluid">


		<?php echo form_open_multipart('register/doctorCreate', ['autocomplete' => 'off', 'id' => 'form-doctor-signup']); ?>
			
            <div class="registration-header">
                <img src="<?php echo base_url(); ?>template_file/img/logo-my_virtual_doctor.png">
                <h4>Let’s Get Started - Create An Account</h4>
            </div>
            <section class="card">

                <div class="card-block">
                    <div class="row">

                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">First Name</label>
                                        <input  type="text" class="form-control" name="fname"
                                               placeholder="First Name">
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Last Name</label>
                                        <input type="text" class="form-control" name="lname"
                                               placeholder="Last Name">
                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Primary Specialty</label>

                                        <select name="specialty" class="select2">
                                            <option value="">Select Specialty</option>
                                            <?php foreach ($specialties as $index => $specialty) { ?>
                                                <option value="<?php echo $index; ?>"><?php echo $specialty; ?></option>
                                            <?php } ?>
                                            <option value="-1">Other</option>
                                        </select>

                                        <input type="text" class="form-control" name="otherSpecialty" id="otherSpecialty"
                                               style="display: none" placeholder="Other specialty">

                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Practicing Since</label>

                                        <select name="practicing" class="select2">
                                            <option value="">Select a Year</option>
                                            <?php $years = range(date('Y'), 1960); ?>
                                            <?php foreach ($years as $year) { ?>
                                                <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                            <?php } ?>
                                        </select>

                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Email</label>
                                        <input type="email" class="form-control" name="email" placeholder="Email">
                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Languages Spoken</label>

                                        <select name="language[]" class="select2" multiple="multiple"
                                                placeholder="Select Language">
                                            <?php foreach ($languages as $index => $language) { ?>
                                                <option value="<?php echo $index; ?>"><?php echo $language; ?></option>
                                            <?php } ?>
                                            <option value="-1">Other</option>
                                        </select>
                                        <input type="text" class="form-control" name="otherLanguage" id="otherLanguage"
                                               style="display: none" placeholder="Other language">

                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">NPI Number</label>
                                        <input type="text" class="form-control" name="npi"
                                               placeholder="NPI Number">
                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Primary State License</label>

                                        <select name="state_id" class="select2">
                                            <option value="">Select a State</option>
                                            <?php foreach ($states as $index => $name) { ?>
                                                <option value="<?php echo $index; ?>"><?php echo $name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Other Licenses</label>
                                        <input type="text" class="form-control" name="otherLicenses"
                                               placeholder="Other Licenses">
                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Phone Number</label>
                                        <input type="text" class="form-control" name="phone"
                                               placeholder="Phone Number">
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4 col-md-offset-4 col-lg-offset-4 col-sm-offset-4">
                    <input type="hidden" value="1" name="save">
                    <input type="submit" class="btn btn-primary btn-lg btn-block" value="Create Account">
                </div>
            </div><br/>
        </form>
    </div>
</div>

<script>
    $(document).ready(function () {
        var $form = $("#form-doctor-signup");
        var $language = $form.find('[name=language]');
        $language.on("change", function (e) {
            var values = $(this).val();
            var index = values.indexOf("-1");
            if (index > -1) {
                if (values.length > 1) {
                    values.splice(index, 1);
                    $(this).val(values).trigger("change");
                }

                $("#otherLanguage").show().focus();

            }
        });

        var $specialty = $form.find('[name=specialty]');
        $specialty.on("change", function (e) {
            var values = $(this).val();
            var index = values.indexOf("-1");
            if (index > -1) {

                $("#otherSpecialty").show().focus();

            }
        });

        $form.submit(function () {

            $form.find('.form-group').removeClass('has-danger');

            var $firstName = $form.find('[name=fname]');
            var $lastName = $form.find('[name=lname]');
            var $email = $form.find('[name=email]');
            var $practicing = $form.find('[name=practicing]');
            var $npi = $form.find('[name=npi]');
            var $stateId = $form.find('[name=state_id]');

            if ($firstName.val() == "") {
                $firstName.parent().addClass('has-danger');
                notyError("First name is empty");

                return false;
            }

            if ($lastName.val() == "") {
                $lastName.parent().addClass('has-danger');
                notyError("Last name is empty");

                return false;
            }

            if ($email.val() == "") {
                $email.parent().addClass('has-danger');
                notyError("Email is empty");

                return false;
            }

            if ($practicing.val() == "") {
                $practicing.parent().addClass('has-danger');
                notyError("Practicing Since is empty");

                return false;
            }

            if ($npi.val() == "") {
                $npi.parent().addClass('has-danger');
                notyError("NPI Number is empty");

                return false;
            }

            if ($npi.val().length != 10) {
                $npi.parent().addClass('has-danger');
                notyError("NPI Number is wrong");

                return false;
            }

            if ($stateId.val() == "") {
                $stateId.parent().addClass('has-danger');
                notyError("Primary State License is empty");

                return false;
            }
        });
    });
</script>

