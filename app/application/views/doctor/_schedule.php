<div class="modal fade" id="doctorMakeAppointment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
            <div class="stepwizard col-md-offset-3">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                        <p>Appointment Details</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p>Confirmation</p>
                    </div>
                </div>
            </div>

			<?php echo form_open('', ['role' => 'form', 'class'=>'createAppoint', 'id'=>'payment-form']); ?>
                <input type="hidden" value="<?php echo isset($appointment) ? $appointment->patients_id : ''; ?>" name="pid" id="patient_id" />
                <div class="row setup-content" id="step-1">
                    <div class="col-xs-12">
                        <div class="col-md-12">
                            <h3 class="text-center head-appoint"><i class="fa fa-calendar"></i> Create New Appointment</h3>
                            <div class="modal-body">
                                <div class="col-md-6 left-app">

                                    <?php if (isset($patients) && !empty($patients)): ?>

                                    <div class="doc-img">
                                        <div class="di-user unselect di-user-main"><i class="fa fa-user"></i></div>
                                        <div class="di-detail di-detail-main">Select a Patient<span>For this appointment</span></div>
                                        <div class="more-doc"><a data-toggle="collapse" id="patient_collapse" data-parent="#accordion1" href="#collapseOne"> <i class="fa fa-angle-down" aria-hidden="true"></i></a></div>

                                    </div>
                                    <div class="list-app clearfix panel-collapse collapse" id="collapseOne">
                                        <div class="add-appointment">Who is this appointment for?</div>
                                        <div class="ap-inner clearfix">
                                            <div class="ap-in-search"><input type="text" placeholder="Search" class="form-control search_patient" /></div>
                                            <div id="patient_listing">
                                                <?php
                                                if ( is_array($patients) ) {
                                                    foreach ($patients as $patient) { ?>
                                                        <div class="doc-img">
                                                            <div class="di-user"><img src="<?php echo default_image($patient->image_url); ?>" /></div>
                                                            <div class="di-detail select_patient"><a href="#" data-image_url="<?php echo default_image($patient->image_url); ?>" data-patient_id="<?php echo $patient->user_id; ?>" data-patient_name="<?php echo $patient->firstname . ' ' . $patient->lastname; ?>" data-patient_details="<?php echo $patient->city; echo isset($patient->code) ? ', ' . $patient->code : ''; ?>"><?php echo $patient->firstname . ' ' . $patient->lastname; ?></a> <span><?php echo $patient->city; echo isset($patient->code) ? ', ' . $patient->code : ''; ?></span></div>
                                                        </div>

                                                        <?php

                                                    }
                                                }

                                                ?>
                                            </div>
                                        </div>
                                    </div>

                                    <?php elseif( isset($patient) ): ?>
                                        <div class="doc-img">
                                            <div class="di-user unselect di-user-main">
                                                <?php echo '<img src="' . default_image($patient->image_url) . '">'; ?>
                                            </div>
                                            <div class="di-detail di-detail-main">
                                                <h6><?php echo $patient->firstname . ' ' . $patient->lastname;?> <span><?php echo $patient->city; echo isset($patient->code) ? ', ' . $patient->code : ''; ?></span></h6>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <div class="appointment-time">
                                        <!-- <div class="pt-head"><i class="fa fa-clock-o"></i> when</div> -->
                                        <div class="">
                                            <div class="pt-cnt picker">
                                                <div class="pt-calendar clearfix">
                                                    <div class="pt-cl-head clearfix">
                                                        <div class="pt-month">
                                                            <div class='input-group date' id='doctorTimings'>
                                                                <input type='text' class="form-control" value="<?php echo date('m/d/Y'); ?>" />
                                                                <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pt-cl-cnt clearfix picker-slots" id="picker_slots"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 summarized pt-cnt" style="display: none;">

                                                <h5 style="display: inline-block;" class="pull-left">When</h5>
                                                <a id="appointment-sum_edit" style="display: inline-block;" class="pull-right"><h5>Edit</h5></a>
                                                <hr style="clear: both;">
                                                <h2 class="edit_time"></h2>
                                                <h6 class="edit_date">Monday, June 6, 2016</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 right-app right-app-from">

                                    <?php if ( !isset($reschedule_nopayment) && isset($consultation_types) && is_array($consultation_types) && !empty($consultation_types) ): ?>
                                        <div class="form-group" id="consultation_type_div">
                                            <label class="control-label">Consultation Type</label>
                                            <?php if( !isset($consultation_type_data) && isset($appointment) ) { ?>
                                                <p>30 minutes : $<?php echo $appointment->amount; ?></p>
                                            <?php } else { ?>
                                                <select name="consultation_type" id="consultation_type" class="select2 form-control" <?php echo isset($consultation_type_data) && is_object($consultation_type_data) && isset($consultation_type_data->id) ? 'disabled="disabled"' : ''; ?>>
                                                    <?php foreach($consultation_types as $consultation_type): ?>
                                                        <option value="<?php echo $consultation_type->id; ?>" <?php echo isset($consultation_type_data) && is_object($consultation_type_data) && isset($consultation_type_data->id) && $consultation_type->id == $consultation_type_data->id ? 'selected="selected"' : ''; ?>><?php echo $consultation_type->duration; ?> minutes : $<?php echo $consultation_type->rate; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            <?php } ?>
                                        </div>
                                    <?php endif; ?>

                                    <div class="form-group ">
                                        <label class="control-label">Reasons for Appointment</label>
                                        <select name="reason_id" id="reason_id" class="select2 form-control">
                                            <option value="">Primary Concern/Reasons</option>
                                            <?php if( is_array($reasons) && !empty($reasons) ) foreach ($reasons as $reason) { ?>
                                                <option value="<?php echo $reason->id; ?>" <?php echo isset($appointment) && $appointment->reason_id == $reason->id ? 'selected="selected"' : ''; ?>><?php echo $reason->name; ?></option>
                                            <?php } ?>
                                        </select>
                                        <?php  if ( isset($appointment) && $appointment->reason != '' ): ?>
                                            <input type="text" placeholder="Please enter other reason here" name="reason" id="reason_textbox" value="<?php echo $appointment->reason; ?>" class="form-control" />
                                        <?php  else: ?>
                                            <input type="text" placeholder="Please enter other reason here" name="reason" value="" id="reason_textbox" style="display:none;" class="form-control" />
                                        <?php  endif; ?>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Additional Notes</label>
                                        <textarea name="appointment_notes" id="appointment_notes" placeholder="Notes" class="form-control" ></textarea>
                                    </div>

                                </div>
                                <div class="col-md-12 next-btn">
                                    <button class="btn btn-primary nextBtn btn-lg pull-right doctor_step2_next" type="button" >Schedule Appointment</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row setup-content" id="step-2">
                    <div class="col-xs-12 ">
                        <div class="col-md-12">
                            <h3 class="text-center head-appoint"><i class="fa fa-check" aria-hidden="true"></i> Appointment Request Sent</h3>
                            <div class="col-md-12 text-center">
                                <?php if ( isset($reschedule_nopayment) && $reschedule_nopayment == 'yes' ): ?>
                                <p>
                                    Appointment Has been rescheduled successfully.
                                </p>
                                <?php else: ?>
                                <p id="nopayment_message" style="display: none;">
                                    Appointment has been scheduled and now awaiting confirmation at the patient's end. Once that is complete the calendar will automatically be updated.
                                </p>
                                <p id="withpayment_message">Appointment has been scheduled and now awaiting confirmation and payment at the patient's end. Once that is complete the calendar will automatically be updated.</p>
                                <?php endif; ?>
                            </div>



                            <div class="col-md-12 next-btn">
                                <button class="btn btn-primary nextBtn btn-lg doctor_finish_step" type="button" data-dismiss="modal">Finish</button>
                            </div>

                        </div>
                    </div> </div>

            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(function($){
        step_2_ok = false;
        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

        allWells.hide();
        //we don't need top navigation click

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });


        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

            if ( step_2_ok === false ) {
                isValid = false;
            }

            if (isValid) {
                nextStepWizard.removeAttr('disabled').trigger('click');
            }

        });

        $('div.setup-panel div a.btn-primary').trigger('click');

        $('#reason_id').change(function(){
            //$("#first").select2('val');
            var val = $(this).val();
            if ( val == 13 ) {
                $('#reason_textbox').css('display', 'block');
            } else {
                $('#reason_textbox').css('display', 'none');
            }
        });

        //initialize bootstrap datetime picker
        $('#doctorTimings').datetimepicker({
            viewMode: 'days',
            useCurrent: false,
            minDate: '<?php echo date('m/d/Y', time()); ?>',
            format: 'MM/DD/YYYY',
            <?php
            if(isset($doctor_available_time) && is_array($doctor_available_time)) { ?>
            enabledDates: <?php echo json_encode($doctor_available_time); ?>
            <?php
            }
            ?>
        });


        function loadAvailableTiming(id, s_date, s_month) {
            //disable next and prev button
            //1. disable next and previous button
            $('.step2_next').prop('disabled', true);

            //2. show loading state
            var str = '<div class="alert alert-info" role="alert"><i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom"></i> \
                <span class="sr-only">Loading...</span> Getting available timings...</div>';
            $('.picker-slots').html(str);


            //$('.picker-slots').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i> Loading....');

            $.ajax({
                method: "POST",
                url: "<?php echo base_url(); ?>appointment/ajax_get_doctors_available_time",
                data: {id: id, select_date: s_date, select_month: s_month},
                dataType: "html",
                //async: true,
            }).success(function (data, status) {
                //alert(data);
                $(".picker-slots").html(data);
            }).done(function(){
                $('.step2_next').prop('disabled', false);
            });
        }


        var id = '<?php echo $this->session->userdata('userid'); ?>';
        var s_date = '<?php echo date("Y-m-d"); ?>';
        var m_date = '<?php echo date("m");  ?>';
        loadAvailableTiming( id, s_date, m_date );


        //now do .picker-slots changes
        $('.picker-slots').on('click', "button.picker-slots-button", function (e) {
            e.preventDefault();

            var stm = $(this).attr("data-start_time");
            var etm = $(this).attr("data-end_time");
            var d = $(this).attr("data-fulldate");

            var start_date = $(this).data('start_date');
            var end_date = $(this).data('end_date');
            var available_time_id = $(this).data('available');

            $(".edit_time").text(stm + "-" + etm);
            $(".edit_date").html(d + "<input type='hidden' class='selected_date' name='avail' value='" + start_date + "-" + end_date + "'><input type='hidden' name='avaialble_time_id' value='"+ available_time_id  +"'>");
            $(".picker").hide("fast");
            $(".summarized").show("fast");

            return false;
        });



        //get date change event
        $("#doctorTimings").on("dp.change", function (e) {
            date_time = e.date.toObject();
            step_2_ok = false;
            //now grab all plans for this date
            var month = date_time.months + 1;
            if ( month < 10 ) {
                month = '0' + month.toString();
            }
            var day = date_time.date;
            if ( day < 10 ) {
                day = '0' + day.toString();
            }
            var id = '<?php echo $this->session->userdata('userid'); ?>';
            var s_date = date_time.years +  '-' +  month + '-' + day;
            var m_date = month;
            loadAvailableTiming( id, s_date, m_date );

        });
        <?php if( ! isset($appointment)): ?>
        $("#appointment-sum_edit").click(function () {
            $(".picker").show("fast");
            $(".summarized").hide("fast");
            $(".edit_date").html("");
        });


        //search for patients
        var bind_to = ':input.search_patient';
        $('#doctorMakeAppointment').off('keyup', bind_to);
        $('#doctorMakeAppointment').on('keyup', bind_to, function(event) {
            //alert($(this).val().length);
            step_2_ok = false;
            var th = $(this);
            var length = th.val().length;
            var text = th.val();
            if ( length > 2  ) {
                //search patient by name via ajax
                get_patient_ajax('search', text);
            }
            else if ( length == 0 || length == '' ) {
                //get latest four patient
                get_patient_ajax('four', text);
            }
        });

        function get_patient_ajax( type, text ) {
            if ( type == 'four' ) {
                //get latest four
                var url = '<?php echo base_url() . 'PatientAjax/ajax_get_doctor_patients';  ?>';
                var data = {};

            } else {
                //get all
                var url = '<?php echo base_url() . 'PatientAjax/ajax_search_doctor_patients' ?>';
                var data = {search: text};
            }

            //2. show loading state
            var str = '<i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom"></i> \
                <span class="sr-only">Searching...</span> Searching for patient. Please wait...';
            $('#patient_listing').html(str);


            //$('.picker-slots').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i> Loading....');

            $.ajax({
                method: "POST",
                url: url,
                data: data,
                dataType: "html",
                //async: true,
            }).success(function (response, status) {
                //alert(data);
                $("#patient_listing").html(response);
            }).done(function(){

            });
        }

        //select a patient
        //
        $('#doctorMakeAppointment').on('click', '.select_patient', function(e) {
            e.preventDefault();

            step_2_ok = false;

            var th = $(this).children('a');
            var image_url = th.data('image_url');
            var name = th.data('patient_name');
            var pid = th.data('patient_id');
            var image = '<img src="'+ image_url +'" />';
            var details = '<span>' + th.data('patient_details') + '</span>';

            $('#patient_id').val(pid);
            $('.di-user-main').html(image).removeClass('unselect');
            $('.di-detail-main').html(name + details);
            $('#patient_collapse').trigger('click');


            //call ajax to checked patient biling waived or not
            $.ajax({
                method: "POST",
                url: '<?php echo base_url() . 'PatientAjax/ajax_check_doctor_waived/' ?>' + pid,
                dataType: "json",
                //async: true,
            }).success(function (response, status) {

                //fee_waived
                if ( response.waived && response.waived > 0 ) {
                    $('#nopayment_message').show();
                    $('#withpayment_message').hide();
                    $('#consultation_type_div').hide();
                }
                else {
                    $('#nopayment_message').hide();
                    $('#withpayment_message').show();
                    $('#consultation_type_div').show();
                }

            }).done(function(){

            });






        });
        <?php endif; ?>

        //now add new appointment
        $('#doctorMakeAppointment').on('click', '.doctor_step2_next', function(e) {
            //check for valid inputs
            e.preventDefault();

            if ( step_2_ok == true ) {
                return false;
            }

            var th = $(this);

            //check stripe is configured or not
            <?php if ( isset($doctor_stripe) && ($doctor_stripe->stripe_one == '' || $doctor_stripe->stripe_two == '') ) { ?>
            notyError('Payment method is not configured yet. Please visit Settings -> Stripe Information tab to setup your stripe api keys.');
            return false;
            <?php } ?>


            var patient_id =   $('#patient_id').val() ;

            if ( patient_id <= 0  || patient_id == undefined  ) {
                notyError('Please select a patient first.');
                return false;
            }

            //check reason
            var appdt = $(".selected_date").val();

            if ( appdt == undefined || appdt == '') {
                notyError('Please select appointment time.');
                return false;
            }

            if ( $('#reason_id').val() == '' ) {
                notyError('Please select a reasons for appointment.');
                return false;
            }

            if ( $('#reason_id').val() == '13' &&  $('#reason_textbox').val() == '' ) {
                notyError('Please enter other reasons for appointment.');
                return false;
            }

            var url = '<?php

                if ( isset($appointment) ) {
                    echo base_url() . 'appointment/ajax_doctor_edit_appointment/' . $appointment->id;
                } else {
                    echo base_url() . 'appointment/ajax_doctor_create_new';
                }

                ?>';

            //now everything is ok, send ajax request to make appointment
            th.html('Please wait....');
            th.prop('disabled', true);
            //ajax call
            $.ajax({
                url: url,
                //data: $.param(data),
                data:  $('#payment-form').serializeArray(),
                method: "POST",
                dataType: "json"
            }).error(function (err) {
                //enable buttons
                //notyError(err.responseText);
                console.log('Error: ', err);

            }).done(function () {
                th.html('Schedule Appointment');
                th.prop('disabled', false);

            }).success(function (data) {

                if ( data.success ) {
                    step_2_ok = true;

                    if( data.message ) {
                        $.each(data.message, function(index, msg){
                            notySuccess(msg)
                        });
                    }


                    //reset form
                    resetForm($('#payment-form'));

                    $('.doctor_step2_next').trigger('click');

                    setTimeout(function(){
                        var url = '<?php echo base_url() . 'doctor/consultations'; ?>';
                        window.location = url;
                        //location.reload();
                    }, 5000);


                } else if ( data.error ) {
                    $.each(data.error, function(index, msg) {
                        notyError(msg)
                    });
                }
            }, 'json');


        });

        function resetForm($form) {
            $form.find('input:text, input:hidden, input:password, input:file, select, textarea').val('');
            $form.find('input:radio, input:checkbox')
                .removeAttr('checked').removeAttr('selected');

            //reset patient list
            $('#patient_listing').html('');
        }
    });
</script>