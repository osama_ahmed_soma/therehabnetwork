<div class="page-content page-doctor-update_profile">
    <div class="container-fluid">
        <!---->
        <!--        <header class="card-header">-->
        <!--            User Profile-->
        <!--        </header>-->


        <?php
        if (is_array($doctors)) {
            foreach ($doctors as $doctor) {
                $home = $doctor->phone_home;
                $mobile = $doctor->phone_mobile;
                $business = $doctor->phone_business;
                ?>
                <form method="post" enctype="multipart/form-data">

                    <section class="card">

                        <center><?php echo validation_errors(); ?>
                            <?php echo $this->session->flashdata('message'); ?>
                        </center>
                        <div class="card-block">
                            <div class="row">


                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col-md-12" style="min-height: 0;">
                                            <h5>Upload Your Image</h5>
                                        </div>
                                        <div class="col-md-12">
                                            <img src="<?php echo base_url() . $doctor->image_url; ?>" width="90%"
                                                 class="preview" id="preview_profile">
                                        </div>
                                        <div class="col-md-12">
                                            <label class="btn btn-default btn-file">
                                                <span><i class="fa fa-file"></i></span>Replace Image
                                                <input id="imgInpProfile" onchange="loadFileProfile(event);"
                                                       type="file" class="btn btn-primary btn-block"
                                                       name="doctor_image">
                                                <input type="hidden" name="doctor_image_db"
                                                       value="<?php echo $doctor->image_url; ?>">
                                            </label>
                                        </div>

                                    </div>
                                </div>


                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">First Name</label>
                                                <input type="text" class="form-control" name="fname"
                                                       value="<?php echo $doctor->firstname; ?>">

                                            </fieldset>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Last Name</label>
                                                <input type="text" class="form-control" name="lname"
                                                       placeholder="Last Name"
                                                       value="<?php echo $doctor->lastname; ?>">

                                            </fieldset>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Email</label>
                                                <input type="text" class="form-control" name="email" placeholder="Email"
                                                       value="<?php echo $doctor->email; ?>">
                                            </fieldset>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Gender</label>
                                                <select name="gender" class="select2">
                                                    <option value="">Select Gender</option>
                                                    <?php
                                                    $genders = array(
                                                        'Male' => 'Male', 'Female' => 'Female', 'Other' => 'Other'
                                                    );
                                                    foreach ($genders as $key_gender => $value_gender) {

                                                        ?>
                                                        <option
                                                            value="<?php echo $value_gender; ?>"
                                                            <?php echo $doctor->gender == $value_gender ? 'selected' : '' ?>>
                                                            <?php echo $key_gender; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Date of Birth</label>

                                                <div class='input-group date'>
                                                    <input id="daterange_dob"
                                                           class="form-control daterange_picker" name="dob"
                                                           value="<?php echo $doctor->dob; ?>"
                                                           type="text" placeholder="Date Of Birth">
                                                    <span class="input-group-addon">
                                           <i class="font-icon font-icon-calend"></i>
                                        </span>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Time Zone</label>
                                                <select name="timezone" class="select2">
                                                    <?php $timezones = array(
                                                        'Hawaii-Alleuitan Time Zone UTC -10:00' => 'Hawaii-Alleuitan Time Zone UTC -10:00',
                                                        'Alaska Time Zone UTC -09:00' => 'Alaska Time Zone UTC -09:00',
                                                        'Pacific Time Zone UTC -8:00' => 'Pacific Time Zone UTC -8:00',
                                                        'Mountain Time Zone UTC -07:00' => 'Mountain Time Zone UTC -07:00',
                                                        'Central Time Zone UTC -06:00' => 'Central Time Zone UTC -06:00',
                                                        'Eastern Time Zone UTC -05:00' => 'Eastern Time Zone UTC -05:00',
                                                        'Atlantic Time Zone UTC -04:00' => 'Atlantic Time Zone UTC -04:00'
                                                    );

                                                    ?>
                                                    <option value="">Select Timezone</option>
													<?php foreach (tz_list() as $zoneValue) { ?>
														<option value="<?php echo $zoneValue['zone']; ?>" <?php echo ($doctor->timezone == $zoneValue['zone']) ? 'selected' : '' ?>>
															<?php echo $zoneValue['zone']; ?></option>
													<?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">State</label>
                                                <select name="state" id="state" class="select2">
                                                    <option value="">Select States</option>
                                                    <?php
                                                    $us_state_names = array(
                                                        'ALABAMA' => 'ALABAMA', 'ALASKA' => 'ALASKA', 'ARIZONA' => 'ARIZONA', 'ARKANSAS' => 'ARKANSAS', 'CALIFORNIA' => 'CALIFORNIA', 'COLORADO' => 'COLORADO', 'CONNECTICUT' => 'CONNECTICUT', 'DELAWARE' => 'DELAWARE', 'FLORIDA' => 'FLORIDA', 'GEORGIA' => 'GEORGIA', 'HAWAII' => 'HAWAII', 'IDAHO' => 'IDAHO', 'ILLINOIS' => 'ILLINOIS', 'INDIANA' => 'INDIANA', 'IOWA' => 'IOWA', 'KANSAS' => 'KANSAS', 'KENTUCKY' => 'KENTUCKY', 'LOUISIANA' => 'LOUISIANA', 'MAINE' => 'MAINE', 'MARYLAND' => 'MARYLAND', 'MASSACHUSETTS' => 'MASSACHUSETTS', 'MICHIGAN' => 'MICHIGAN', 'MINNESOTA' => 'MINNESOTA', 'MISSISSIPPI' => 'MISSISSIPPI', 'MISSOURI' => 'MISSOURI', 'MONTANA' => 'MONTANA', 'NEBRASKA' => 'NEBRASKA', 'NEVADA' => 'NEVADA', 'NEW HAMPSHIRE' => 'NEW HAMPSHIRE', 'NEW JERSEY' => 'NEW JERSEY', 'NEW MEXICO' => 'NEW MEXICO', 'NEW YORK' => 'NEW YORK', 'NORTH CAROLINA' => 'NORTH CAROLINA', 'NORTH DAKOTA' => 'NORTH DAKOTA', 'OHIO' => 'OHIO', 'OKLAHOMA' => 'OKLAHOMA', 'OREGON' => 'OREGON', 'PENNSYLVANIA' => 'PENNSYLVANIA', 'RHODE ISLAND' => 'RHODE ISLAND', 'SOUTH CAROLINA' => 'SOUTH CAROLINA', 'SOUTH DAKOTA' => 'SOUTH DAKOTA', 'TENNESSEE' => 'TENNESSEE', 'TEXAS' => 'TEXAS', 'UTAH' => 'UTAH', 'VERMONT' => 'VERMONT', 'VIRGINIA' => 'VIRGINIA', 'WASHINGTON' => 'WASHINGTON', 'WEST VIRGINIA' => 'WEST VIRGINIA', 'WISCONSIN' => 'WISCONSIN', 'WYOMING' => 'WYOMING'
                                                    );
                                                    foreach ($us_state_names as $key_state => $value_state) {

                                                        ?>
                                                        <option
                                                            value="<?php echo $value_state; ?>" <?php echo $doctor->state == $value_state ? 'selected' : '' ?>>
                                                            <?php echo $key_state; ?></option>
                                                        <?php
                                                    }
                                                    ?>

                                                </select>

                                            </fieldset>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">City</label>
                                                <input type="text" class="form-control" name="city"
                                                       value="<?php echo $doctor->city; ?>"
                                                       placeholder="City">

                                            </fieldset>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Zip</label>
                                                <input type="text" class="form-control" name="zip"
                                                       value="<?php echo $doctor->zip; ?>"
                                                       placeholder="Zip">

                                            </fieldset>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Phone-Office</label>
                                                <input type="text" class="form-control" name="phone-office"
                                                       value="<?php echo $doctor->phone_home; ?>"
                                                       placeholder="Phone-Office">

                                            </fieldset>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Phone-Mobile</label>
                                                <input type="text" class="form-control" name="phone-mobile"
                                                       value="<?php echo $doctor->phone_mobile; ?>"
                                                       placeholder="Phone-Mobile">

                                            </fieldset>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <fieldset class="form-group">
                                                <label class="form-label semibold">Fax</label>
                                                <input type="text" class="form-control" name="fax"
                                                       value="<?php echo $doctor->fax; ?>"
                                                       placeholder="Fax">
                                            </fieldset>
                                        </div>


                                    </div>
                                </div>
                            </div>


                        </div>
                    </section>

                    <section class="card">
                        <div class="card-block">

                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Pre-Medical Education</label>
                                        <input type="text" class="form-control" name="pre_education"
                                               value="<?php echo $doctor->pre_medical_education; ?>"
                                               placeholder="Pre-Medical Education">

                                    </fieldset>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Practicing Since</label>

                                        <div class='input-group date'>
                                            <input id="daterange_pricing" class="form-control daterange_picker"
                                                   name="practicing"
                                                   value="<?php echo $doctor->practicing_since; ?>"
                                                   type="text" placeholder="Practicing Since">
                                            <!--                                        <input class="form-control condition_date" name="practicing" value="" type="text" placeholder="Practicing Since">-->
                                            <span class="input-group-addon">
                                            <i class="font-icon font-icon-calend"></i>
                                        </span>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Residency</label>
                                        <input type="text" class="form-control" name="residency"
                                               value="<?php echo $doctor->residency; ?>"
                                               placeholder="Residency">

                                    </fieldset>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Department</label>
                                        <input type="text" class="form-control" name="dept"
                                               value="<?php echo $doctor->department; ?>"
                                               placeholder="Department">

                                    </fieldset>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Specialty</label>
                                        <input type="text" class="form-control" name="specialty"
                                               value="<?php echo $doctor->specialty; ?>"
                                               placeholder="Specialty">

                                    </fieldset>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Sub Specialty</label>
                                        <input type="text" class="form-control" name="s_specialty"
                                               value="<?php echo $doctor->sub_specialty; ?>"
                                               placeholder="Sub Specialty">

                                    </fieldset>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Medical License</label>
                                        <input type="text" class="form-control" name="m_license"
                                               value="<?php echo $doctor->npi; ?>"
                                               placeholder="Medical License">

                                    </fieldset>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">State License</label>
                                        <input type="text" class="form-control" name="s_license"
                                               value="<?php echo $doctor->state_license; ?>"
                                               placeholder="State License">

                                    </fieldset>
                                </div>
                            </div>

                        </div>
                    </section>

                    <!--                    <div class="row">-->
                    <!--                        <div class="col-md-4 col-lg-4 col-sm-4 col-md-offset-4 col-lg-offset-4 col-sm-offset-4">-->
                    <!--                            <input type="hidden" value="1" name="save">-->
                    <!---->
                    <!--                        </div>-->
                    <!--                    </div>-->


                    <center>
                        <input type="hidden" value="1" name="save">
                        <button type="submit" class="btn btn-inline">Save</button>
                        <button type="button" class="btn btn-inline cancel">Cancel</button>
                    </center>
                </form>
                <?php
            }
        } else {
            ?>
            <form method="post">

                <section class="card">

                    <center><?php echo validation_errors(); ?>
                        <?php echo $this->session->flashdata('message'); ?>
                    </center>
                    <div class="card-block">
                        <div class="row">


                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5>Upload Your Image</h5>
                                    </div>
                                    <div class="col-md-12">
                                        <img src="" width="90%" class="preview" id="preview_profile">
                                    </div>
                                    <div class="col-md-12">
                                        <label class="btn btn-default btn-file">
                                            <span><i class="fa fa-file"></i></span>Replace Image
                                            <input id="imgInpProfile" onchange="loadFileProfile(event);"
                                                   type="file" class="btn btn-primary btn-block">
                                        </label>
                                    </div>

                                </div>


                            </div>


                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">First Name</label>
                                            <input type="text" class="form-control" name="fname"
                                                   value="">

                                        </fieldset>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">Last Name</label>
                                            <input type="text" class="form-control" name="lname"
                                                   placeholder="Last Name"
                                                   value="">

                                        </fieldset>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">Email</label>
                                            <input type="text" class="form-control" name="email" placeholder="Email"
                                                   value="">
                                        </fieldset>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">Gender</label>
                                            <select name="gender" class="select2">
                                                <option value="">Select Gender</option>
                                                <?php
                                                $genders = array(
                                                    'Male' => 'Male', 'Female' => 'Female'
                                                );
                                                foreach ($genders as $key_gender => $value_gender) {

                                                    ?>
                                                    <option
                                                        value="<?php echo $value_gender; ?>"><?php echo $key_gender; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">Date of Birth</label>

                                            <div class='input-group date'>
                                                <input id="daterange_dob" class="form-control" name="dob" value=""
                                                       type="text" placeholder="Date Of Birth">
                                                <!--                                        <input class="form-control condition_date" name="dob" value="" type="text" placeholder="Date Of Birth">-->
                                                <span class="input-group-addon">
                                           <i class="font-icon font-icon-calend"></i>
                                        </span>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">Time Zone</label>
                                            <select name="timezone" class="select2">
                                                <?php $timezones = array(
                                                    'Hawaii-Alleuitan Time Zone UTC -10:00' => 'Hawaii-Alleuitan Time Zone UTC -10:00',
                                                    'Alaska Time Zone UTC -09:00' => 'Alaska Time Zone UTC -09:00',
                                                    'Pacific Time Zone UTC -8:00' => 'Pacific Time Zone UTC -8:00',
                                                    'Mountain Time Zone UTC -07:00' => 'Mountain Time Zone UTC -07:00',
                                                    'Central Time Zone UTC -06:00' => 'Central Time Zone UTC -06:00',
                                                    'Eastern Time Zone UTC -05:00' => 'Eastern Time Zone UTC -05:00',
                                                    'Atlantic Time Zone UTC -04:00' => 'Atlantic Time Zone UTC -04:00'
                                                );

                                                ?>
                                                <option value="">Select Timezone</option>
                                                <?php foreach (tz_list() as $zoneValue) { ?>
												<option value="<?php echo $zoneValue['zone']; ?>">
													<?php echo $zoneValue['zone']; ?></option>
												<?php } ?>
                                            </select>
                                        </fieldset>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">State</label>
                                            <select name="state" id="state" class="select2">
                                                <option value="">Select States</option>
                                                <?php
                                                $us_state_names = array(
                                                    'ALABAMA' => 'ALABAMA', 'ALASKA' => 'ALASKA', 'ARIZONA' => 'ARIZONA', 'ARKANSAS' => 'ARKANSAS', 'CALIFORNIA' => 'CALIFORNIA', 'COLORADO' => 'COLORADO', 'CONNECTICUT' => 'CONNECTICUT', 'DELAWARE' => 'DELAWARE', 'FLORIDA' => 'FLORIDA', 'GEORGIA' => 'GEORGIA', 'HAWAII' => 'HAWAII', 'IDAHO' => 'IDAHO', 'ILLINOIS' => 'ILLINOIS', 'INDIANA' => 'INDIANA', 'IOWA' => 'IOWA', 'KANSAS' => 'KANSAS', 'KENTUCKY' => 'KENTUCKY', 'LOUISIANA' => 'LOUISIANA', 'MAINE' => 'MAINE', 'MARYLAND' => 'MARYLAND', 'MASSACHUSETTS' => 'MASSACHUSETTS', 'MICHIGAN' => 'MICHIGAN', 'MINNESOTA' => 'MINNESOTA', 'MISSISSIPPI' => 'MISSISSIPPI', 'MISSOURI' => 'MISSOURI', 'MONTANA' => 'MONTANA', 'NEBRASKA' => 'NEBRASKA', 'NEVADA' => 'NEVADA', 'NEW HAMPSHIRE' => 'NEW HAMPSHIRE', 'NEW JERSEY' => 'NEW JERSEY', 'NEW MEXICO' => 'NEW MEXICO', 'NEW YORK' => 'NEW YORK', 'NORTH CAROLINA' => 'NORTH CAROLINA', 'NORTH DAKOTA' => 'NORTH DAKOTA', 'OHIO' => 'OHIO', 'OKLAHOMA' => 'OKLAHOMA', 'OREGON' => 'OREGON', 'PENNSYLVANIA' => 'PENNSYLVANIA', 'RHODE ISLAND' => 'RHODE ISLAND', 'SOUTH CAROLINA' => 'SOUTH CAROLINA', 'SOUTH DAKOTA' => 'SOUTH DAKOTA', 'TENNESSEE' => 'TENNESSEE', 'TEXAS' => 'TEXAS', 'UTAH' => 'UTAH', 'VERMONT' => 'VERMONT', 'VIRGINIA' => 'VIRGINIA', 'WASHINGTON' => 'WASHINGTON', 'WEST VIRGINIA' => 'WEST VIRGINIA', 'WISCONSIN' => 'WISCONSIN', 'WYOMING' => 'WYOMING'
                                                );
                                                foreach ($us_state_names as $key_state => $value_state) {

                                                    ?>
                                                    <option
                                                        value="<?php echo $value_state; ?>"><?php echo $key_state; ?></option>
                                                    <?php
                                                }
                                                ?>

                                            </select>

                                        </fieldset>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">City</label>
                                            <input type="text" class="form-control" name="city" value=""
                                                   placeholder="City">

                                        </fieldset>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">Zip</label>
                                            <input type="text" class="form-control" name="zip" value=""
                                                   placeholder="Zip">

                                        </fieldset>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">Phone-Office</label>
                                            <input type="text" class="form-control" name="phone-office" value=""
                                                   placeholder="Phone-Office">

                                        </fieldset>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">Phone-Mobile</label>
                                            <input type="text" class="form-control" name="phone-mobile"
                                                   placeholder="Phone-Mobile">

                                        </fieldset>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">Fax</label>
                                            <input type="text" class="form-control" name="fax" value=""
                                                   placeholder="Fax">
                                        </fieldset>
                                    </div>


                                </div>
                            </div>
                        </div>


                    </div>
                </section>

                <section class="card">
                    <div class="card-block">

                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <fieldset class="form-group">
                                    <label class="form-label semibold">Pre-Medical Education</label>
                                    <input type="text" class="form-control" name="pre_education" value=""
                                           placeholder="Pre-Medical Education">

                                </fieldset>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <fieldset class="form-group">
                                    <label class="form-label semibold">Practicing Since</label>

                                    <div class='input-group date'>
                                        <input id="daterange_pricing" class="form-control" name="practicing"
                                               value=""
                                               type="text" placeholder="Practicing Since">
                                        <!--                                        <input class="form-control condition_date" name="practicing" value="" type="text" placeholder="Practicing Since">-->
                                        <span class="input-group-addon">
                                            <i class="font-icon font-icon-calend"></i>
                                        </span>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <fieldset class="form-group">
                                    <label class="form-label semibold">Residency</label>
                                    <input type="text" class="form-control" name="residency" value=""
                                           placeholder="Residency">

                                </fieldset>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <fieldset class="form-group">
                                    <label class="form-label semibold">Department</label>
                                    <input type="text" class="form-control" name="dept" value=""
                                           placeholder="Department">

                                </fieldset>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <fieldset class="form-group">
                                    <label class="form-label semibold">Specialty</label>
                                    <input type="text" class="form-control" name="specialty" value=""
                                           placeholder="Specialty">

                                </fieldset>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <fieldset class="form-group">
                                    <label class="form-label semibold">Sub Specialty</label>
                                    <input type="text" class="form-control" name="s_specialty" value=""
                                           placeholder="Sub Specialty">

                                </fieldset>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <fieldset class="form-group">
                                    <label class="form-label semibold">Medical License</label>
                                    <input type="text" class="form-control" name="m_license" value=""
                                           placeholder="Medical License">

                                </fieldset>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <fieldset class="form-group">
                                    <label class="form-label semibold">State License</label>
                                    <input type="text" class="form-control" name="s_license" value=""
                                           placeholder="State License">

                                </fieldset>
                            </div>
                        </div>

                    </div>
                </section>
                <center>
                    <input type="hidden" value="1" name="save">
                    <button type="submit" class="btn btn-inline">Save</button>
                    <button type="button" class="btn btn-inline cancel">Cancel</button>
                </center>
            </form>
            <?php
        }
        ?>
    </div>
</div>
