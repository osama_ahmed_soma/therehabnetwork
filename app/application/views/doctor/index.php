<?php /** @var CI_Loader $this */?>
<div class="page-content view-doctor-profile">
    <section class="doctor-profile-section clearfix">
        <div class="dps-detail clearfix">
            <div class="dps-dt-img"> <img src="<?php echo default_image($doctor->image_url); ?>" width="90%" class="preview" id="preview_patient"></div>
            <div class="dps-dt-cnt">
                <div class="dps-dt-client-name">
					<span>Dr. <?php echo $doctor->firstname . " " . $doctor->lastname; ?></span>
					<span class="dps-dt-btn add_and_remove_buttons_prepend_area">
						<a class="btn btn-primary-outline" href="<?php echo site_url('/doctor/update_profile'); ?>"  style="margin-top: 0px;">Edit Profile</a>
					</span>
				</div>
                <div class="dps-dt-list-edu">
                    <ul>
                        <li> <i class="font-icon fa fa-stethoscope" aria-hidden="true"></i><?php
                            if($specials != 'No result found'){
                                if (is_array($specials)) {
									$last = end($specials);
                                    foreach ($specials as $special) {
										echo $special->speciality;
										 if ($last != $special) {
											 echo  ', ';
										 }
                                    }
									unset($last);
                                }
                            } else {
                                echo 'N/A';
                            }
                            ?> </li>
                        <li> <i class="font-icon fa fa-graduation-cap" aria-hidden="true"></i> <?php
                            if($educations != 'No result found') {
                                if (is_array($educations)) {
									$last = end($educations);
                                    foreach ($educations as $education) {
										 echo $education->name;
										 if ($last != $education) {
											 echo  ', ';
										 }
                                    }
									unset($last);
                                }
                            } else {
                                echo 'N/A';
                            }
                            ?></li>
                        <li><span class="glyphicon glyphicon-map-marker"></span><span><?php echo ($doctor->city) ? $doctor->city : 'N/A'; ?></span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="dps-about">
            <div class="dps-ab-txt">
                <h4>About Me</h4>
                <?php echo ($doctor->about) ? $doctor->about : 'No information available'; ?>
            </div>
        </div>
    </section>
    <div class="container-fluid">
        <section class="dsp-detail-list clearfix">
            <div class="col-md-6">
                <div class="dsp-dl-column">
                    <div class="dsp-dl-cl-name colspan" data-att="myspecialities">Specialties</div>
                    <div class="dso-dl-cl-txt colspan_show" data-id="myspecialities" style="display:block;">
                        <?php
                        if(count($specials) > 0 && $specials != 'No result found'){
                            $i = 0;
                            if (is_array($specials)) {
                                foreach ($specials as $special) {
                                    $i = $i + 1;
                                    echo $i . ". " . $special->speciality . "<br>";
                                }
                            }
                        } else {
                            echo '<span>No information available</span>';
                        }
                        ?>

                    </div>
                </div>
                <div class="dsp-dl-column">
                    <div class="dsp-dl-cl-name colspan" data-att="education">Education </div>
                    <div class="dso-dl-cl-txt colspan_show" data-id="education" style="display:block;">
                        <?php if(count($educations) > 0 && $educations != 'No result found'): ?>
                        <ul class="exp-timeline">
                            <?php
                            if (is_array($educations)) {
//								var_debug($educations);
                                foreach ($educations as $education) {
                                    ?>
                                    <li class="exp-timeline-item">
                                        <div class="dot"></div>
                                        <div class="tbl">
                                            <div class="tbl-row">
                                                <div class="tbl-cell">
                                                    <div
                                                        class="exp-timeline-range"><?php echo $education->from_year . " - " . $education->to_year; ?></div>
                                                    <div
                                                        class="exp-timeline-status"><?php echo $education->name; ?></div>
                                                    <div
                                                        class="exp-timeline-location"><?php echo $education->institute; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                        <?php else: ?>
                            <span>No information available</span>
                        <?php endif; ?>
                    </div>
                </div>
                <!--<div class="dsp-dl-column">
                    <div class="dsp-dl-cl-name colspan" data-att="experience">Experience </div>
                    <div class="dso-dl-cl-txt colspan_show" data-id="experience" style="display:block;">
                        <ul class="exp-timeline">
                            <?php
/*                            if (is_array($experiences)) {
                                foreach ($experiences as $experience) {
                                    */?>
                                    <li class="exp-timeline-item">
                                        <div class="dot"></div>
                                        <div class="tbl">
                                            <div class="tbl-row">
                                                <div class="tbl-cell">
                                                    <div
                                                        class="exp-timeline-range"><?php /*echo $experience->from_year . " - " . $experience->to_year; */?></div>
                                                    <div
                                                        class="exp-timeline-status"><?php /*echo $experience->name; */?></div>
                                                    <div
                                                        class="exp-timeline-location"><?php /*echo $experience->institute; */?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
/*                                }
                            }
                            */?>
                        </ul>
                    </div>
                </div>-->

            </div>
            <div class="col-md-6">
                <div class="dsp-dl-column">
                    <div class="dsp-dl-cl-name colspan" data-att="awards">Awards </div>
                    <div class="dso-dl-cl-txt colspan_show" data-id="awards" style="display:block;">
                        <?php if(count($awards) > 0): ?>
                        <ul class="exp-timeline">
                            <?php
                            if (is_array($awards)) {
                                foreach ($awards as $award) {
                                    ?>
                                    <li class="exp-timeline-item">
                                        <div class="dot"></div>
                                        <div class="tbl">
                                            <div class="tbl-row">
                                                <div class="tbl-cell">
                                                    <div
                                                        class="exp-timeline-range"><?php echo $award->year; ?></div>
                                                    <div class="exp-timeline-location"><?php echo $award->name; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                        <?php else: ?>
                            <span>No information available</span>
                        <?php endif; ?>
                    </div>
                </div>
                <!--<div class="dsp-dl-column">
                    <div class="dsp-dl-cl-name colspan" data-att="professional">Professional Membership or Societies </div>
                    <div class="dso-dl-cl-txt colspan_show" data-id="professional" style="display:block;">
                        <p>
                            <?php
/*                            $i = 0;
                            if (is_array($memberships)) {
                                foreach ($memberships as $membership) {
                                    $i = $i + 1;
                                    echo $i . ". " . $membership->membership_post . " of " . $membership->society_name . "<br>";
                                }
                            }
                            */?>
                        </p>

                    </div>
                </div>-->

                <div class="dsp-dl-column">
                    <div class="dsp-dl-cl-name colspan" data-att="licenced">Licensed States </div>
                    <div class="dso-dl-cl-txt colspan_show" data-id="licenced" style="display:block;">
                   		<span>
                            <?php
                            if(count($licenced_states) > 0 && $licenced_states != 'No result found'){
                                $i = 0;
                                if (is_array($licenced_states)) {
                                    foreach ($licenced_states as $licenced_state) {
                                        $i = $i + 1;
                                        echo $i . ". " . $licenced_state->state_name . "<br>";
                                    }
                                }
                            } else {
                                echo 'No information available';
                            }
                            ?>
                        </span>

                    </div>
                </div>
                <div class="dsp-dl-column">
                    <div class="dsp-dl-cl-name colspan" data-att="language">Language(s) Spoken</div>
                    <div class="dso-dl-cl-txt colspan_show" data-id="language" style="display:block;">
                   		<span>
                            <?php
                            if(count($languages) > 0){
                                $i = 0;
                                if (is_array($languages)) {
                                    foreach ($languages as $language) {
                                        $i = $i + 1;
                                        echo $i . ". " . $language->Language_name . "<br>";
                                    }
                                }
                            } else {
                                echo 'No information available';
                            }
                            ?>
                        </span>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $('.colspan').click(function(){
            $data = $(this).attr('data-att');
            $('.colspan_show[data-id='+$data+']').toggle('slow');
        });
    </script>

    <div class="container-fluid hidden">



        <?php
        if (is_object($doctor)) {
            ?>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_info">

                    <div class="row">
                        <div class="col-md-3">
                            <img src="<?php echo base_url() . $doctor->image_url; ?>" width="90%" class="preview" id="preview_patient">
                        </div>

                        <div class="col-md-6">

                            <h4>Dr. <?php echo $doctor->firstname . " " . $doctor->lastname; ?></h4>
                            <div class="connection-meter">
                                <div class="indicator">
                                    &nbsp;
                                </div>
                                <span>online</span>
                            </div>
                            <br>
                            <div class="connection-meter">
                                <p>
                                    <i class="font-icon fa fa-stethoscope" aria-hidden="true"></i>
                                    <span>
                                            <?php
                                            $i = 0;
                                            if (is_array($specials)) {
                                                foreach ($specials as $special) {
                                                    $i = $i + 1;
                                                    if ($i < sizeof($specials))
                                                        $str = ",";
                                                    else
                                                        $str = "";
                                                    echo $special->speciality . $str;
                                                }
                                            }
                                            ?>
                                        </span>
                                </p>
                            </div>

                            <div class="connection-meter">
                                <p>
                                    <i class="font-icon fa fa-graduation-cap" aria-hidden="true"></i>
                                    <span>
                                            <?php
                                            $i = 0;
                                            if (is_array($degrees)) {
                                                foreach ($degrees as $degree) {
                                                    $i = $i + 1;
                                                    if ($i < sizeof($degrees))
                                                        $str = ",";
                                                    else
                                                        $str = "";
                                                    echo $degree->name . $str;
                                                }
                                            }
                                            ?>
                                        </span>
                                </p>
                            </div>

                            <div class="connection-meter">
                                <p><span class="glyphicon glyphicon-map-marker"></span><span><?php echo $doctor->location; ?></span>
                                </p>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <br>

                            <div class="row">
                                <div class="col-md-12">
                                    <button id="btn-book-appointment" class="btn btn-primary btn-block" type="submit">Book Appointment</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">

                                <div class="col-md-12">
                                    <button id="btn-contact-message" class="btn btn-primary-outline btn-block" type="submit">Contact </button>
                                </div>

                            </div>
                            <br>
                            <?php
                            if ($fav) {
                                ?>
                                <div class="row">
                                    <div class="col-md-12" doctor_fav_add_remove>
                                        <a class="btn btn-primary-outline btn-block remove_from_favorite" href="#"
                                           data-doctorid="<?php echo $doctor->userid; ?>"
                                           data-patientid="<?php echo $this->session->userdata('userid'); ?>">
                                            Remove from Favorite</a>
                                    </div>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="row">
                                    <div class="col-md-12 doctor_fav_add_remove">
                                        <a class="btn btn-primary-outline btn-block add_to_favorite" href="#"
                                           data-patientid="<?php echo $this->session->userdata('userid'); ?>"
                                           data-doctorid="<?php echo $doctor->userid; ?>">Add to Favorite</a>
                                    </div>
                                </div>

                                <?php
                            }
                            ?>

                        </div>
                    </div>
                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>About me</h4>
                    <?php echo $doctor->about; ?>
                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>My Specialities</h4>
                    <p>

                    </p>
                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>Education</h4>
                    <ul class="exp-timeline">
                        <?php
                        if (is_array($educations)) {
                            foreach ($educations as $education) {
                                ?>
                                <li class="exp-timeline-item">
                                    <div class="dot"></div>
                                    <div class="tbl">
                                        <div class="tbl-row">
                                            <div class="tbl-cell">
                                                <div
                                                    class="exp-timeline-range"><?php echo $education->from_year . " - " . $education->to_year; ?></div>
                                                <div
                                                    class="exp-timeline-status"><?php echo $education->name; ?></div>
                                                <div
                                                    class="exp-timeline-location"><?php echo $education->institute; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>

                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>Experience</h4>
                    <ul class="exp-timeline">
                        <?php
                        if (is_array($experiences)) {
                            foreach ($experiences as $experience) {
                                ?>
                                <li class="exp-timeline-item">
                                    <div class="dot"></div>
                                    <div class="tbl">
                                        <div class="tbl-row">
                                            <div class="tbl-cell">
                                                <div
                                                    class="exp-timeline-range"><?php echo $experience->from_year . " - " . $experience->to_year; ?></div>
                                                <div
                                                    class="exp-timeline-status"><?php echo $experience->name; ?></div>
                                                <div
                                                    class="exp-timeline-location"><?php echo $experience->institute; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>

                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>Awards</h4>
                    <ul class="exp-timeline">
                        <?php
                        if (is_array($awards)) {
                            foreach ($awards as $award) {
                                ?>
                                <li class="exp-timeline-item">
                                    <div class="dot"></div>
                                    <div class="tbl">
                                        <div class="tbl-row">
                                            <div class="tbl-cell">
                                                <div
                                                    class="exp-timeline-range"><?php echo $award->year; ?></div>
                                                <div class="exp-timeline-location"><?php echo $award->name; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>

                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>Professional Membership or Societies</h4>
                    <p>
                        <?php
                        $i = 0;
                        if (is_array($memberships)) {
                            foreach ($memberships as $membership) {
                                $i = $i + 1;
                                echo $i . ". " . $membership->membership_post . " of " . $membership->society_name . "<br>";
                            }
                        }
                        ?>
                    </p>
                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>Licenced States</h4>
                    <span>
                            <?php
                            $i = 0;
                            if (is_array($licenced_states)) {
                                foreach ($licenced_states as $licenced_state) {
                                    $i = $i + 1;
                                    if ($i < sizeof($licenced_states))
                                        $str = ",";
                                    else
                                        $str = "";
                                    echo $licenced_state->licenced_state_name . $str;
                                }
                            }
                            ?>
                        </span>
                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>Language Expertise</h4>
                    <span>
                            <?php
                            $i = 0;
                            if (is_array($languages)) {
                                foreach ($languages as $language) {

                                    $i = $i + 1;
                                    if ($i < sizeof($languages))
                                        $str = ",";
                                    else
                                        $str = "";
                                    echo $language->name . $str;
                                }
                            }
                            ?>
                        </span>
                </article>
            </section>
            <?php
        }
        ?>
    </div>
    <!-- <a class="btn btn-danger" data-toggle="modal" data-target="#patientMakeAppointment"><i class="fa fa-clock-o"></i> Schedule</a> -->
</div>


<?php $this->view('appointment/_popup_create2', [

    'patient' => $patient,
    'doctor' => $doctor,
    'reasons' => $reasons,
    'id' => $id,

]); ?>

</div>

</div>