<p><?php echo $firstname?> <?php echo $lastname?>,</p>

<p>Thank you for registering to join My Virtual Doctor!</p>

<p>An implementation specialist will follow up with you within 24hrs to schedule your complimentary 30 minute
    training session. In the meantime, please login to your account and complete the set up process.</p>

<p>Please don't hesitate to contact us at 1-855-80-DOCTOR or at info@myvirtualdoctor.com for additional assistance.</p>
<br/>

<p>Thank you,</p>
<p>My Virtual Doctor</p>