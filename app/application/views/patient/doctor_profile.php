<div class="page-content">
    <div class="container-fluid">

        <section class="card">

            <div class="card-block">
                <div class="row">
                    <div class="col-md-3">
                        <img src="<?php echo base_url() . $doctor->image_url; ?>" width="90%"
                             class="preview" id="preview_patient">
                    </div>

                    <div class="col-md-6">

                        <h4>Dr. <?php echo $doctor->firstname . " " . $doctor->lastname; ?></h4>
                        <div class="connection-meter">
                            <div class="indicator">
                                &nbsp;
                            </div>
                            <span>online</span>
                        </div>
                        <br>
                        <div class="connection-meter">
                            <i class="font-icon fa fa-stethoscope" aria-hidden="true"></i>
                            <span>
                                            Pediatrics Allergist
                                        </span>
                        </div>
                        <br>
                        <div class="connection-meter">
                            <i class="font-icon fa fa-graduation-cap" aria-hidden="true"></i>
                            <span>
                                          MD. FCPS, FRCS (USA)
                                        </span>
                        </div>
                        <br>
                        <div class="connection-meter">
                            <span class="glyphicon glyphicon-map-marker"></span>
                            <span>
                                             Conejo Children Medical College Hospital,
                                        </span>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-block" type="submit">Book Appointment</button>
                            </div>
                        </div>
                        <br>
                        <div class="row">

                            <div class="col-md-12">
                                <button class="btn btn-primary-outline btn-block" type="submit">Contact</button>
                            </div>

                        </div>
                        <br>
                        <div class="row">

                            <div class="col-md-12">
                                <button class="btn btn-primary-outline btn-block" type="submit">Add to Favorite</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="card">
            <div class="card-block">

                <div class="row">
                    <div class="col-md-12">
                        <h4>Overview</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pretium convallis elementum.
                            Duis lacinia vel sem vel consequat. Fusce et cursus lectus. Fusce orci lectus, mollis non
                            nibh in, egestas finibus mi. Praesent sed mauris sed nulla interdum consectetur in at eros.
                            Donec vehicula ornare nisi, a fermentum elit viverra eget. Aenean hendrerit ultricies nulla,
                            eget congue lorem tincidunt et. Mauris nec tellus ipsum. Maecenas eget quam euismod,
                            fermentum mauris eget, consectetur ante. Etiam vitae ligula diam. Duis condimentum quis
                            velit at venenatis. Donec ut convallis ex.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pretium convallis elementum.
                            Duis lacinia vel sem vel consequat. Fusce et cursus lectus. Fusce orci lectus, mollis non
                            nibh in, egestas finibus mi. Praesent sed mauris sed nulla interdum consectetur in at eros.
                            Donec vehicula ornare nisi, a fermentum elit viverra eget. Aenean hendrerit ultricies nulla,
                            eget congue lorem tincidunt et. Mauris nec tellus ipsum. Maecenas eget quam euismod,
                            fermentum mauris eget, consectetur ante. Etiam vitae ligula diam. Duis condimentum quis
                            velit at venenatis. Donec ut convallis ex.</p>
                    </div>
                </div>

            </div>
        </section>
    </div>
</div>