<?php /** @var CI_Loader $this */ ?>
<div class="page-content view-doctor-profile">
    <section class="doctor-profile-section clearfix">
        <div class="dps-detail clearfix">
            <div class="dps-dt-img"><img src="<?php echo default_image($doctor->image_url); ?>" width="90%"
                                         class="preview" id="preview_patient"></div>
            <div class="dps-dt-cnt">
                <div class="dps-dt-client-name">Dr. <?php echo $doctor->firstname . " " . $doctor->lastname; ?></div>
                <div class="dps-dt-list-edu">
                    <ul>
                        <li><i class="font-icon fa fa-stethoscope" aria-hidden="true"></i><?php
                            if ($specials != 'No result found') {
                                $i = 0;
                                if (is_array($specials)) {
                                    foreach ($specials as $special) {
                                        $i = $i + 1;
                                        if ($i < sizeof($specials))
                                            $str = ",";
                                        else
                                            $str = "";
                                        echo $special->speciality . $str;
                                    }
                                }
                            } else {
                                echo 'N/A';
                            }
                            ?> </li>
                        <li><i class="font-icon fa fa-graduation-cap" aria-hidden="true"></i> <?php
                            if ($degrees != 'No result found') {
                                $i = 0;
                                if (is_array($degrees)) {
                                    foreach ($degrees as $degree) {
                                        $i = $i + 1;
                                        if ($i < sizeof($degrees))
                                            $str = ",";
                                        else
                                            $str = "";
                                        echo $degree->name . $str;
                                    }
                                }
                            } else {
                                echo 'N/A';
                            }
                            ?></li>
                        <li><span
                                class="glyphicon glyphicon-map-marker"></span><span><?php echo ($doctor->location) ? $doctor->location : 'N/A'; ?></span>
                        </li>
                    </ul>
                </div>
                <div class="dps-dt-btn add_and_remove_buttons_prepend_area">
                    <button class="btn btn-primary patient_appointment_for_doctor_single" data-id="<?php echo $id; ?>"
                            type="submit"><i class="fa fa-clock-o"></i> Book Appointment
                    </button>
                    <button id="btn-contact-message" class="btn btn-primary-outline compose_modal_init"
                            data-recipient_id="<?php echo $doctor->userid; ?>"
                            data-recipient_full_name="<?php echo $doctor->firstname . " " . $doctor->lastname; ?>"
                            data-toggle="modal"
                            data-target="#contacts" type="submit"><i class="fa fa-envelope"></i> Message
                    </button>
                    <?php
                    if ($fav) {
                        ?>
                        <span class="doctor_fav_add_remove">
                                            <button class="btn btn-primary-outline remove_from_favorite" href="#"
                                                    data-doctorid="<?php echo $doctor->userid; ?>"
                                                    data-patientid="<?php echo $this->session->userdata('userid'); ?>"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="Click to remove to My Physicians list." style="color: #fff !important;">
<!--                                               <i class="fa fa-star"></i>--> <i class="font-icon fa fa-stethoscope"></i> Remove from My Physicians
                                            </button>
                                        </span>
                        <?php
                    } else {
                        ?>
                        <span class="doctor_fav_add_remove">
                                            <button class="btn btn-primary-outline add_to_favorite" href="#"
                                                    data-patientid="<?php echo $this->session->userdata('userid'); ?>"
                                                    data-doctorid="<?php echo $doctor->userid; ?>"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="Click to add to My Physicians list.">
<!--                                                <i class="fa fa-star-o"></i>-->
                                                    <i class="font-icon fa fa-stethoscope"></i> Add To My Physicians
                                            </button>
                                        </span>

                        <?php
                    }
                    ?>


                </div>
            </div>
        </div>
        <div class="dps-about">
            <div class="dps-ab-txt">
                <h4>About Me</h4>
                <?php echo ($doctor->about) ? $doctor->about : 'No information available'; ?>
            </div>
        </div>
    </section>
    <div class="container-fluid">
        <section class="dsp-detail-list clearfix">
            <div class="col-md-6">
                <div class="dsp-dl-column">
                    <div class="dsp-dl-cl-name colspan" data-att="myspecialities">Specialties</div>
                    <div class="dso-dl-cl-txt colspan_show" data-id="myspecialities" style="display:block;">
                        <?php
                        if (count($specials) > 0 && $specials != 'No result found') {
                            $i = 0;
                            if (is_array($specials)) {
                                foreach ($specials as $special) {
                                    $i = $i + 1;
                                    echo $i . ". " . $special->speciality . "<br>";
                                }
                            }
                        } else {
                            echo '<span>No information available</span>';
                        }
                        ?>

                    </div>
                </div>
                <div class="dsp-dl-column">
                    <div class="dsp-dl-cl-name colspan" data-att="education">Education</div>
                    <div class="dso-dl-cl-txt colspan_show" data-id="education" style="display:block;">
                        <?php if (count($educations) > 0 && $educations != 'No result found'): ?>
                            <ul class="exp-timeline">
                                <?php
                                if (is_array($educations)) {
                                    foreach ($educations as $education) {
                                        ?>
                                        <li class="exp-timeline-item">
                                            <div class="dot"></div>
                                            <div class="tbl">
                                                <div class="tbl-row">
                                                    <div class="tbl-cell">
                                                        <div
                                                            class="exp-timeline-range"><?php echo $education->from_year . " - " . $education->to_year; ?></div>
                                                        <div
                                                            class="exp-timeline-status"><?php echo $education->name; ?></div>
                                                        <div
                                                            class="exp-timeline-location"><?php echo $education->institute; ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        <?php else: ?>
                            <span>No information available</span>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="dsp-dl-column">
                    <div class="dsp-dl-cl-name colspan" data-att="awards">Awards</div>
                    <div class="dso-dl-cl-txt colspan_show" data-id="awards" style="display:block;">
                        <?php if (count($awards) > 0): ?>
                            <ul class="exp-timeline">
                                <?php
                                if (is_array($awards)) {
                                    foreach ($awards as $award) {
                                        ?>
                                        <li class="exp-timeline-item">
                                            <div class="dot"></div>
                                            <div class="tbl">
                                                <div class="tbl-row">
                                                    <div class="tbl-cell">
                                                        <div
                                                            class="exp-timeline-range"><?php echo $award->year; ?></div>
                                                        <div
                                                            class="exp-timeline-location"><?php echo $award->name; ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        <?php else: ?>
                            <span>No information available</span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="dsp-dl-column">
                    <div class="dsp-dl-cl-name colspan" data-att="licenced">Licensed States</div>
                    <div class="dso-dl-cl-txt colspan_show" data-id="licenced" style="display:block;">
                   		<span>
                            <?php
                            if (count($licenced_states) > 0 && $licenced_states != 'No result found') {
                                $i = 0;
                                if (is_array($licenced_states)) {
                                    foreach ($licenced_states as $licenced_state) {
                                        $i = $i + 1;
                                        echo $i . ". " . $licenced_state->state_name . "<br>";
                                    }
                                }
                            } else {
                                echo 'No information available';
                            }
                            ?>
                        </span>

                    </div>
                </div>
                <div class="dsp-dl-column">
                    <div class="dsp-dl-cl-name colspan" data-att="language">Language(s) Spoken</div>
                    <div class="dso-dl-cl-txt colspan_show" data-id="language" style="display:block;">
                   		<span>
                            <?php
                            if (count($languages) > 0) {
                                $i = 0;
                                if (is_array($languages)) {
                                    foreach ($languages as $language) {

                                        $i = $i + 1;
                                        if ($i < sizeof($languages))
                                            $str = ",";
                                        else
                                            $str = "";
                                        echo $language->name . $str;
                                    }
                                }
                            } else {
                                echo 'No information available';
                            }
                            ?>
                        </span>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $('.colspan').click(function () {
            $data = $(this).attr('data-att');
            $('.colspan_show[data-id=' + $data + ']').toggle('slow');
        });
    </script>

    <div class="container-fluid hidden">


        <?php
        if (is_object($doctor)) {
            ?>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_info">

                    <div class="row">
                        <div class="col-md-3">
                            <img src="<?php echo default_image($doctor->image_url); ?>" width="90%" class="preview"
                                 id="preview_patient">
                        </div>

                        <div class="col-md-6">

                            <h4>Dr. <?php echo $doctor->firstname . " " . $doctor->lastname; ?></h4>
                            <div class="connection-meter">
                                <div class="indicator">
                                    &nbsp;
                                </div>
                                <span>online</span>
                            </div>
                            <br>
                            <div class="connection-meter">
                                <p>
                                    <i class="font-icon fa fa-stethoscope" aria-hidden="true"></i>
                                    <span>
                                            <?php
                                            $i = 0;
                                            if (is_array($specials)) {
                                                foreach ($specials as $special) {
                                                    $i = $i + 1;
                                                    if ($i < sizeof($specials))
                                                        $str = ",";
                                                    else
                                                        $str = "";
                                                    echo $special->speciality . $str;
                                                }
                                            }
                                            ?>
                                        </span>
                                </p>
                            </div>

                            <div class="connection-meter">
                                <p>
                                    <i class="font-icon fa fa-graduation-cap" aria-hidden="true"></i>
                                    <span>
                                            <?php
                                            $i = 0;
                                            if (is_array($degrees)) {
                                                foreach ($degrees as $degree) {
                                                    $i = $i + 1;
                                                    if ($i < sizeof($degrees))
                                                        $str = ",";
                                                    else
                                                        $str = "";
                                                    echo $degree->name . $str;
                                                }
                                            }
                                            ?>
                                        </span>
                                </p>
                            </div>

                            <div class="connection-meter">
                                <p><span
                                        class="glyphicon glyphicon-map-marker"></span><span><?php echo $doctor->location; ?></span>
                                </p>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <br>

                            <div class="row">
                                <div class="col-md-12">
                                    <button id="btn-book-appointment" class="btn btn-primary btn-block" type="submit">
                                        Book Appointment
                                    </button>
                                </div>
                            </div>
                            <br>
                            <div class="row">

                                <div class="col-md-12">
                                    <button id="btn-contact-message" class="btn btn-primary-outline btn-block"
                                            type="submit">Contact
                                    </button>
                                </div>

                            </div>
                            <br>
                            <?php
                            if ($fav) {
                                ?>
                                <div class="row">
                                    <div class="col-md-12" doctor_fav_add_remove>
                                        <a class="btn btn-primary-outline btn-block remove_from_favorite" href="#"
                                           data-doctorid="<?php echo $doctor->userid; ?>"
                                           data-patientid="<?php echo $this->session->userdata('userid'); ?>">
                                            <i class="fa fa-star"></i> Remove from Favorite</a>
                                    </div>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="row">
                                    <div class="col-md-12 doctor_fav_add_remove">
                                        <a class="btn btn-primary-outline btn-block add_to_favorite" href="#"
                                           data-patientid="<?php echo $this->session->userdata('userid'); ?>"
                                           data-doctorid="<?php echo $doctor->userid; ?>"><i class="fa fa-star-o"></i>
                                            Add to Favorite</a>
                                    </div>
                                </div>

                                <?php
                            }
                            ?>

                        </div>
                    </div>
                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>About me</h4>
                    <?php echo $doctor->about; ?>
                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>My Specialities</h4>
                    <p>

                    </p>
                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>Education</h4>
                    <ul class="exp-timeline">
                        <?php
                        if (is_array($educations)) {
                            foreach ($educations as $education) {
                                ?>
                                <li class="exp-timeline-item">
                                    <div class="dot"></div>
                                    <div class="tbl">
                                        <div class="tbl-row">
                                            <div class="tbl-cell">
                                                <div
                                                    class="exp-timeline-range"><?php echo $education->from_year . " - " . $education->to_year; ?></div>
                                                <div
                                                    class="exp-timeline-status"><?php echo $education->name; ?></div>
                                                <div
                                                    class="exp-timeline-location"><?php echo $education->institute; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>

                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>Experience</h4>
                    <ul class="exp-timeline">
                        <?php
                        if (is_array($experiences)) {
                            foreach ($experiences as $experience) {
                                ?>
                                <li class="exp-timeline-item">
                                    <div class="dot"></div>
                                    <div class="tbl">
                                        <div class="tbl-row">
                                            <div class="tbl-cell">
                                                <div
                                                    class="exp-timeline-range"><?php echo $experience->from_year . " - " . $experience->to_year; ?></div>
                                                <div
                                                    class="exp-timeline-status"><?php echo $experience->name; ?></div>
                                                <div
                                                    class="exp-timeline-location"><?php echo $experience->institute; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>

                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>Awards</h4>
                    <ul class="exp-timeline">
                        <?php
                        if (is_array($awards)) {
                            foreach ($awards as $award) {
                                ?>
                                <li class="exp-timeline-item">
                                    <div class="dot"></div>
                                    <div class="tbl">
                                        <div class="tbl-row">
                                            <div class="tbl-cell">
                                                <div
                                                    class="exp-timeline-range"><?php echo $award->year; ?></div>
                                                <div class="exp-timeline-location"><?php echo $award->name; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>

                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>Professional Membership or Societies</h4>
                    <p>
                        <?php
                        $i = 0;
                        if (is_array($memberships)) {
                            foreach ($memberships as $membership) {
                                $i = $i + 1;
                                echo $i . ". " . $membership->membership_post . " of " . $membership->society_name . "<br>";
                            }
                        }
                        ?>
                    </p>
                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>Licenced States</h4>
                    <span>
                            <?php
                            $i = 0;
                            if (is_array($licenced_states)) {
                                foreach ($licenced_states as $licenced_state) {
                                    $i = $i + 1;
                                    if ($i < sizeof($licenced_states))
                                        $str = ",";
                                    else
                                        $str = "";
                                    echo $licenced_state->licenced_state_name . $str;
                                }
                            }
                            ?>
                        </span>
                </article>
            </section>

            <section class="box-typical doctors_profile">
                <article class="profile-info-item doc_profile_simple_text">
                    <h4>Language Expertise</h4>
                    <span>
                            <?php
                            $i = 0;
                            if (is_array($languages)) {
                                foreach ($languages as $language) {

                                    $i = $i + 1;
                                    if ($i < sizeof($languages))
                                        $str = ",";
                                    else
                                        $str = "";
                                    echo $language->name . $str;
                                }
                            }
                            ?>
                        </span>
                </article>
            </section>
            <?php
        }
        ?>
    </div>


    <!--Contact Form -->
    <?php
    if (is_object($doctor)) {
        ?>
        <div class="modal fade bs-example-modal-lg" id="contacts" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
                        <h3><i class="fa fa-pencil-square"></i> Compose</h3>
                    </div>
                    <?php echo form_open('messages/compose', array('id' => 'compose_form')); ?>
                    <div class="modal-body">
                        <div id="ajax_errors"></div>
                        <!-- Top Compose and search bar -->
                        <div class="form-group">
                            <label for="formGroupRecipients">Recipient: <strong class="recipient_full_name"></strong></label>
                            <input type="hidden" name="recipients" value="" id="formGroupRecipients" />
                        </div>
                        <div class="form-group">
                            <label for="formGroupSubject">Subject:</label>
                            <input type="text" class="form-control" id="formGroupSubject" placeholder="Subject" name="subject" value="<?php echo set_value('subject', isset($message) ? $message->subject : '' ); ?>">
                        </div>

                        <div class="form-group">
                            <label for="formGroupMessage">Message:</label>
                            <textarea name="message" class="form-control" id="formGroupMessage" placeholder="Message"><?php echo set_value('message', isset($message) ? $message->message : ''); ?></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="form-group pull-right">
                            <input type="submit" class="btn btn-primary" value="Save" name="save" />
                            <input type="submit" class="btn btn-primary" value="Send" name="send" />
                            <button type="button" class="btn btn-primary" id="resetButton">Cancel</button>
                        </div>
                    </div>
                    <script type="text/javascript">
                        jQuery(function ($) {
                            $('#formGroupMessage').wysihtml5();
                            $('#resetButton').click(function () {
                                $('#contacts').modal('hide');
                            });
                            value_of_save = '';
                            $('#compose_form input[name=save]').click(function (e) {
                                e.preventDefault();
                                value_of_save = $(this).val();
                                $('#compose_form').submit();
                            });
                            $('#compose_form input[name=send]').click(function (e) {
                                e.preventDefault();
                                value_of_save = $(this).val();
                                $('#compose_form').submit();
                            });
                            $('#compose_form').submit(function (e) {
                                e.preventDefault();
                                form = $(this);
                                formdata = form.serialize();
                                formdata += "&save=" + encodeURIComponent(value_of_save);
                                form.find('#ajax_errors').hide(200);
                                form.find('#ajax_errors').html('');
                                $.ajax({
                                    method: form.attr('method'),
                                    url: form.attr('action'),
                                    data: formdata,
                                    dataType: 'json',
                                }).success(function (response) {
                                    if (response.redirect) {
                                        window.location.href = response.redirect;
                                    } else {
                                        form.find('#ajax_errors').html(response.errors);
                                        form.find('#ajax_errors').show(200);
                                    }
                                });
                            });
                        });
                    </script>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
<!--        <div class="modal fade bs-example-modal-lg" id="contacts" tabindex="-1" role="dialog"-->
<!--             aria-labelledby="contacts">-->
<!--            <div class="modal-dialog modal-lg" role="document">-->
<!--                <div class="modal-content">-->
<!--                    <div class="modal-header">-->
<!--                        <button type="button" class="close contact_modal_close" data-dismiss="modal" aria-label="">-->
<!--                            <span>×</span></button>-->
<!--                        <h4 class="text-center" style="margin:0px;"><i class="fa fa-envelope"></i> Contact with Doctor-->
<!--                        </h4>-->
<!--                    </div>-->
<!--                    <div class="modal-body">-->
<!--                        <form id="contact">-->
<!--                            <div class="contact-popup-1">-->
<!--                                <div class="wrapper clearfix">-->
<!---->
<!--                                    <div class="container-fluid">-->
<!--                                        <div class="row">-->
<!--                                            <div class="col-sm-12 notes">-->
<!--                                                <h5 class="error_messages"></h5>-->
<!--                                                <textarea rows="10" name="message_text" class="form-control"-->
<!--                                                          id="message_text"-->
<!--                                                          placeholder="Message Text"></textarea>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <br/>-->
<!--                                    <div class="col-md-12 next-btn">-->
<!--                                        <input type="hidden" id="receiver_id" name="receiver_id"-->
<!--                                               value="--><?php //echo $doctor->userid; ?><!--">-->
<!--                                        <button id="popup-contact-submit" type="button"-->
<!--                                                class="btn btn-primary pull-right nextBtn ">Submit-->
<!--                                        </button>-->
<!--                                        <button id="popup-contact-cancel" data-dismiss="modal" type="button"-->
<!--                                                class="cancel btn btn-primary pull-right nextBtn ">Cancel-->
<!--                                        </button>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </form>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <?php
    }
    ?>
</div>

</div>

</div>
<script>
    $(document).ready(function(){
        $(document).on('click', '.compose_modal_init', function(){
            var self = $(this);
            $('input[name="recipients"]').val(self.attr('data-recipient_id'));
            $('.recipient_full_name').text(self.attr('data-recipient_full_name'));
        });
    });
</script>

