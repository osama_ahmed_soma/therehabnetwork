<?php /** @var array $appointments */?>
<?php /** @var $isPast */?>
<div class="table-responsive">
    <table class="table table-bordered table-hover">
        <thead><tr>
            <th><i class="fa fa-calendar" aria-hidden="true"></i> Date</th>
            <th><i class="fa fa-clock-o" aria-hidden="true"></i> Time</th>
            <th><i class="fa fa-user" aria-hidden="true"></i> Provider</th>
            <th><i class="fa fa-stethoscope" aria-hidden="true"></i> Symptom/Problem</th>
            <?php if ($type == 'past') { ?>
                <th><i class="fa fa-spinner" aria-hidden="true"></i> Duration</th>
            <?php } ?>
            <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Action</th>
        </tr></thead>
        <tbody id="<?php echo $id.'_render'; ?>">
            <?php if(count($appointments) == 0): ?>
                <tr>
                    <td colspan="7" style="text-align: left;"><?php if($is_physician): ?>You have no appointments, click <a href="" class="open_schedule_appintment_patient">here</a> to schedule an appointment<?php else: ?>You have no appointment. Click <a href="<?php echo base_url(); ?>patient">here</a> to find a doctor and schedule an appointment.<?php endif; ?></td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
</div>