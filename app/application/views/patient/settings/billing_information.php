<?php
/**
 * Created by PhpStorm.
 * User: osama
 * Date: 9/5/2016
 * Time: 7:55 AM
 */
?>
<div id="billing" class="tab-pane fade in" style="display: block;">

    <script src="<?php echo base_url(); ?>template_file/js/jquery.maskedinput.min.js"></script>

    <form method="post" id="form-billing">
        <section class="card">
            <div class="card-block">
				
                <div id="row-list-card" class="row" <?php echo (is_null($card)) ? 'style="display:none;"' : ''?>>

                </div>

                <div id="row-add-card" class="row" <?php echo (!is_null($card)) ? 'style="display:none;"' : ''?>>
                   <span style="text-align: center;">
                                    <div class="registration-header">
                                        <h4> Add new card</h4>
                                    </div>
                                </span>

                    <div class="col-md-12">
                        <div class="col-sm-4">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Name of card</label>
                                <input type="text" class="form-control" autocomplete="off" name="card-name" id="card-name" style="width: 312px;">
                            </fieldset>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-4 card-form">
                            <div class="card-row">
                                <label>Number</label>
                                <input type="text" size="20" class="card-number" id="card-number" placeholder="•••• •••• •••• ••••">
                            </div>
                            <div class="card-row">
                                <div class="column">
                                    <label>Expiry</label>
                                    <input type="text" size="2" class="card-exp" id="card-exp" placeholder="MM / YY">
                                </div>
                                <div class="column">
                                    <label class="card-form-label-cvc">CVC</label>
                                    <input type="text" size="4" class="card-cvc" id="card-cvc" placeholder="•••">
                                </div>
                            </div>
                            <div class="card-row card-form-submit-row">
                                <button type="submit" class="glowing btn btn-inline btn-success" id="add-card-button">Add Card</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>

    </form>

    <script type="text/template" id="template-list-card">
        <div class="col-md-12">
            <h5>Your saved cards</h5>
            <ul style="list-style: disc;padding-left: 10px;">
                <li>%name% <a href="#" id="card-remove" title="Remove card" data-cardid="%cardid%"><i class="fa fa-times" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </script>

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script>

        function showCardList(name, cardId) {
            var template = $("#template-list-card").html();
            template = template
                .replace(/%name%/g, name)
                .replace(/%cardid%/g, cardId);

            $("#row-list-card").html(template).show();
            $("#row-add-card").hide();
        }
        $(document).ready(function() {

            <?php if (!is_null($card)) { ?>
            showCardList("<?php echo $card->name;?>", <?php echo $card->id;?>);
            <?php } ?>

            Stripe.setPublishableKey('<?php echo $stripePublishableKey; ?>');

            $("#card-number").mask("9999 9999 9999 9999",{placeholder:" "});
            $("#card-exp").mask("99/99",{placeholder:" "});


            var $form = $('#form-billing');

            $("#add-card-button").on('click', function() {
                $form.submit();

                return false;
            });

            $(document).on('click', "#card-remove", function(e) {
                e.preventDefault();

                var $this = $(this);
                notyConfirm("Are you sure want to delete card?", function() {

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>patient/deleteCard",
                        data: {
                            card_id: $this.data('cardid')
                        },
                        dataType: "text",
                        success: function () {
                            $("#row-add-card").show();
                            $("#row-list-card").hide();
                            notySuccess("Card deleted");
                        },
                        error: function() {
                            notyError("Error");
                        }
                    });
                });
            });

            $form.submit(function(e) {

                var cardName = $("#card-name").val();
                if (cardName == "") {
                    notyError("Your card name is empty");

                    return false;
                }

                var cardNumber = $("#card-number").val().replace(/ /g, "");
                if (cardNumber == "") {
                    notyError("Your card number is empty");

                    return false;
                }

                var cardExpire = $("#card-exp").val();
                if (cardExpire == "") {
                    notyError("Your card's expiration is invalid");

                    return false;
                }

                var expData = cardExpire.split('/');
                if (expData.length != 2) {
                    notyError("Your card's expiration is invalid");

                    return false;
                }

                var cardCvc = $("#card-cvc").val();

                Stripe.card.createToken({
                    number: cardNumber,
                    exp_month: expData[0],
                    exp_year: expData[1],
                    cvc: cardCvc
                }, function(status, response) {
                    if (status != 200) {
                        if (response.error && response.error.message) {
                            notyError(response.error.message);

                            return;
                        }

                        console.log(status, response);
                        notyError("Could not add card");

                        return;
                    }

                    var token = response.token;

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>patient/addCard",
                        data: {
                            token: token,
                            card_name: cardName
                        },
                        dataType: "text",
                        success: function (cardResponse) {

                            showCardList(cardName, cardResponse);

                            notySuccess("Card added");
                        },
                        error: function() {
                            notyError("Saving error");
                        }
                    });
                });

                return false;
            });
        });
    </script>
</div>