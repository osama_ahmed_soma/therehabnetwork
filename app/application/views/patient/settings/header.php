<div class="page-content">
    <div class="container-fluid">
    	<section class="navi-top clearfix">
			<div class="navi-heading">
				<h2>Settings</h2>
            </div>
        </section>
        <section class="message-tabs clearfix">
            <div class="tabs-ul pull-left">
                <ul>
                    <li class="nav-item">
                         <a class="nav-link <?php if ($this->uri->segment(2) == 'change_password' ) echo "active"; ?>" href="<?php echo base_url() . 'patient/change_password'; ?>">
                            <span class="nav-link-in">
                                Change Password
                            </span>
                            </a>
                    </li>
<!--                    <li class="nav-item">
                       <a class="nav-link <?php if ($this->uri->segment(2) == 'billing_information' ) echo "active"; ?>" href="<?php echo base_url() . 'patient/billing_information'; ?>">
                                <span class="nav-link-in">
                                    Billing Information
                                </span>
                            </a>
                    </li>-->
                    <li class="nav-item">
                       <a class="nav-link <?php if ($this->uri->segment(2) == 'family_members' ) echo "active"; ?>" href="<?php echo base_url() . 'patient/family_members'; ?>">
                                <span class="nav-link-in">
                                    Family Members
                                </span>
                            </a>
                    </li>
                    <li class="nav-item">
                       <a class="nav-link <?php if ($this->uri->segment(2) == 'primary_physician' ) echo "active"; ?>" href="<?php echo base_url() . 'patient/primary_physician'; ?>">
                                <span class="nav-link-in">
                                    Primary Physician
                                </span>
                            </a>
                    </li>
                </ul>
            </div>
        </section>
        <?php /*?><section class="tabs-section">
            <div class="tabs-section-nav tabs-section-nav-icons">
                <div class="tbl">
                    <ul class="nav" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link <?php if ($this->uri->segment(2) == 'change_password' ) echo "active"; ?>" href="<?php echo base_url() . 'patient/change_password'; ?>">
                            <span class="nav-link-in">
                                Change Password
                            </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($this->uri->segment(2) == 'billing_information' ) echo "active"; ?>" href="<?php echo base_url() . 'patient/billing_information'; ?>">
                                <span class="nav-link-in">
                                    Billing Information
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($this->uri->segment(2) == 'family_members' ) echo "active"; ?>" href="<?php echo base_url() . 'patient/family_members'; ?>">
                                <span class="nav-link-in">
                                    Family Members
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div><!--.tabs-section-nav--><?php */?>
            <div class="tab-content">