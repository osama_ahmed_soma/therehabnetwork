	<?php
/**
 * Created by PhpStorm.
 * User: osama
 * Date: 9/5/2016
 * Time: 8:06 AM
 */
?>
<?php /** @var array $familyMembers */?>

<form method="post" id="form-add-family">
    <section class="card">
        <div class="card-block">
            <div id="family_listing_container" class="row">
            <span style="text-align: center;">
                                <div class="registration-header">
                                    <h4> Your family</h4>
                                </div>
                            </span>
                <div class="col-md-12">
                    <ul id="family_listing" class="list-group" style="list-style: disc;padding-left: 10px;">
                        <?php if (count($familyMembers) > 0): ?>
                            <?php foreach ($familyMembers as $member) { ?>
                                <li class="list-group-item member_<?php echo $member->user_id; ?>">
                                    <span
                                        class="full_name"><?php echo $member->firstname . ' ' . $member->lastname ?></span>
                                    <div class="pull-right">
                                        <a data-toggle="modal" data-target="#family_edit_modal"
                                           class="edit_family_member_modal"
                                           data-first_name="<?php echo $member->firstname; ?>"
                                           data-last_name="<?php echo $member->lastname; ?>"
                                           data-email="<?php echo $member->user_email; ?>"
                                           data-userid="<?php echo $member->user_id; ?>">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a class="delete_family_member" data-userid="<?php echo $member->user_id; ?>">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </div>
                                </li>
                            <?php } ?>
                        <?php else: ?>
                            <li class="list-group-item no_family_member">
                                    <span
                                        class="">No Family members added</span>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <br/>
        </div>
    </section>


    <div class="row">
        <div class="col-md-2 col-md-offset-5">
            <button type="button" class="btn btn-inline btn-success" data-toggle="modal" data-target="#family_add_modal">Add new family member</button>
        </div>
    </div>

</form>

<div class="modal fade" id="family_edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', ['class' => 'edit_form']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Member</h4>
                </div>
                <div class="modal-body">
                    <fieldset class="form-group">
                        <label class="form-label semibold">First Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="fname_edit">
                    </fieldset>
                    <fieldset class="form-group">
                        <label class="form-label semibold">Last Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="lname_edit">
                    </fieldset>
                    <fieldset class="form-group">
                        <label class="form-label semibold">Email <span class="text-danger">*</span></label>
                        <input type="email" class="form-control"name="email_edit">
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="user_id" value="" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="family_add_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="add_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Member</h4>
                </div>
                <div class="modal-body">
                    <fieldset class="form-group">
                        <label class="form-label semibold">First Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="fname">
                    </fieldset>
                    <fieldset class="form-group">
                        <label class="form-label semibold">Last Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="lname">
                    </fieldset>
                    <fieldset class="form-group">
                        <label class="form-label semibold">Email <span class="text-danger">*</span></label>
                        <input type="email" class="form-control" name="email">
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="user_id" value="" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    function removeFamilyMember(userID, str, self){
        $.ajax({
            type: "POST",
            url: str,
            data: {
                id: userID
            },
            success: function () {
                $('.member_'+userID).remove();
                notySuccess('Removed');
                if($('#family_listing li').length == 0){
//                    $('#family_listing_container').hide('slow');
                    $('#family_listing').html('<li class="list-group-item no_family_member">' +
                        '<span class="">No Family members added</span>' +
                    '</li>');
                }
            },
            error: function() {
                notyError("Error");
            }
        });
    }
    $(document).ready(function() {
        $(document).on('click', '.edit_family_member_modal', function(){
            var self = $(this);
            $('input[name="fname_edit"]').val(self.attr('data-first_name'));
            $('input[name="lname_edit"]').val(self.attr('data-last_name'));
            $('input[name="email_edit"]').val(self.attr('data-email'));
            $('input[name="user_id"]').val(self.attr('data-userid'));
        });

        $(document).on('click', '.delete_family_member', function(){
            var self = this;
            var userID = $(self).data('userid');
            var str = '<?php echo base_url(); ?>patient/removeFamilyMember';
            notyMessage('Are you sure that you want to DELETE', 'warning', {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            removeFamilyMember(userID, str, self);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
            return false;
        });

        $('form.edit_form').submit(function(e){
            e.preventDefault();
            var self = this;
            if($('input[name="fname_edit"]').val() == ''){
                notyError('First name is empty');
                return false;
            }
            if($('input[name="lname_edit"]').val() == ''){
                notyError('Last name is empty');
                return false;
            }
            if($('input[name="email_edit"]').val() == ''){
                notyError('Email is empty');
                return false;
            }
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>patient/edit_family_member',
                data: $(self).serialize(),
                dataType: "json",
                success: function (data) {
                    if(data.bool){
                        $('.no_family_member').remove('slow');
                        notySuccess(data.message);
                        $('#family_edit_modal').modal('hide');
                        $('.member_'+$('input[name="user_id"]').val()+'').empty().html('<span class="full_name">'+ $('input[name="fname_edit"]').val()+' '+$('input[name="lname_edit"]').val() +'</spam>' +
                            '<div class="pull-right">' +
                            '<a data-toggle="modal" data-target="#family_edit_modal" class="edit_family_member_modal" data-first_name="'+$('input[name="fname_edit"]').val()+'" data-last_name="'+$('input[name="lname_edit"]').val()+'" data-email="'+$('input[name="email_edit"]').val()+'" data-userid="'+$('input[name="user_id"]').val()+'">' +
                            '<i class="fa fa-pencil"></i>' +
                            '</a> ' +
                            '<a class="delete_family_member" data-userid="'+$('input[name="user_id"]').val()+'">' +
                            '<i class="fa fa-trash-o"></i>' +
                            '</a>' +
                            '</div>');
                    } else {
                        notyError(data.message);
                    }
                },
                error: function() {
                    notyError("Error in request.");
                }
            });
            return false;
        });


        var $form = $('.add_form');
        $form.submit(function(e) {
            //e.preventDefault();

            $form.find('.form-group').removeClass('has-danger');

            var $firstName = $form.find('[name=fname]');
            var $lastName = $form.find('[name=lname]');
            var $email = $form.find('[name=email]');

            if ($firstName.val() == "") {
                $firstName.parent().addClass('has-danger');
                notyError('First name is empty');

                return false;
            }

            if ($lastName.val() == "") {
                $lastName.parent().addClass('has-danger');
                notyError('Last name is empty');

                return false;
            }

            if ($email.val() == "") {
                $email.parent().addClass('has-danger');
                notyError('Email is empty');

                return false;
            }
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>patient/addFamilyMember',
                data: $form.serialize(),
                dataType: "json",
                success: function (data) {
                    if(data.bool){
                        notySuccess(data.message);
                        $('ul.list-group').append('<li class="list-group-item member_'+data.user_id+'">' +
                            '<span class="full_name">'+ $firstName.val() +' '+ $lastName.val() +'</spam>' +
                            '<div class="pull-right">' +
                                '<a data-toggle="modal" data-target="#family_edit_modal" class="edit_family_member_modal" data-first_name="'+$firstName.val()+'" data-last_name="'+$lastName.val()+'" data-email="'+$email.val()+'" data-userid="'+data.user_id+'">' +
                                    '<i class="fa fa-pencil"></i>' +
                                '</a> ' +
                                '<a class="delete_family_member" data-userid="'+data.user_id+'">' +
                                    '<i class="fa fa-trash-o"></i>' +
                                '</a>' +
                            '</div>' +
                        '</li>');
                        $firstName.val('');
                        $lastName.val('');
                        $email.val('');
                        $('#family_add_modal').modal('hide');
                        $('#family_listing_container').show('slow');
                    } else {
                        notyError(data.message);
                    }
                },
                error: function() {

                }
            });
            return false;
        });
    });
</script>