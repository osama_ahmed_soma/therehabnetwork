<div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-2">
    <section class="card">
        <div class="card-block">
            <?php echo validation_errors(); ?>
			<?php echo form_open('', ['name' => 'primary_physician_form']); ?>
                <div class="row">
                    <span style="text-align: center;">
                        <div class="registration-header">
                            <h4>Primary Physician</h4>
                        </div>
                    </span>
                    <?php if (count($primary_physician) == 0): ?>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">First Name <span
                                        class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="first_name"
                                       value="<?php echo set_value('first_name'); ?>"
                                       placeholder="First Name">
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Last Name <span
                                        class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="last_name"
                                       value="<?php echo set_value('last_name'); ?>"
                                       placeholder="Last Name">
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Email <span
                                        class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="email"
                                       value="<?php echo set_value('email'); ?>"
                                       placeholder="Email">
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Phone Number <span
                                        class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="phone_number"
                                       value="<?php echo set_value('phone_number'); ?>"
                                       placeholder="Phone Number">
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">City <span
                                        class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="city"
                                       value="<?php echo set_value('city'); ?>"
                                       placeholder="City">
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">State</label>
                                <select name="state" id="state" class="select2">
                                    <option value="">Select States</option>
                                    <?php foreach ($states as $key_state => $value_state) { ?>
                                        <option
                                            value="<?php echo $key_state; ?>">
                                            <?php echo $value_state; ?></option>
                                    <?php } ?>
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Zip</label>
                                <input type="text" class="form-control" name="zip"
                                       value="<?php echo set_value('zip'); ?>"
                                       placeholder="Zip">
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <input type="hidden" name="is_update"
                                   value="0"/>
                            <label class="form-label semibold">&nbsp;</label>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Add Primary Physician</button>
                        </div>
                    <?php else: ?>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">First Name <span
                                        class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="first_name"
                                       value="<?php echo $primary_physician->firstname; ?>"
                                       placeholder="First Name">
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Last Name <span
                                        class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="last_name"
                                       value="<?php echo $primary_physician->lastname; ?>"
                                       placeholder="Last Name">
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Email <span
                                        class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="email"
                                       value="<?php echo $primary_physician->user_email; ?>"
                                       placeholder="Email">
                                <input type="hidden" class="form-control" name="old_email"
                                       value="<?php echo $primary_physician->user_email; ?>">
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Phone Number <span
                                        class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="phone_number"
                                       value="<?php echo $primary_physician->phone_mobile; ?>"
                                       placeholder="Phone Number">
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">City <span
                                        class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="city"
                                       value="<?php echo $primary_physician->city; ?>"
                                       placeholder="City">
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">State</label>
                                <select name="state" id="state" class="select2">
                                    <option value="">Select States</option>
                                    <?php foreach ($states as $key_state => $value_state) { ?>
                                        <option
                                            value="<?php echo $key_state; ?>" <?php echo $primary_physician->state_id == $key_state ? 'selected' : '' ?>>
                                            <?php echo $value_state; ?></option>
                                    <?php } ?>
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold">Zip</label>
                                <input type="text" class="form-control" name="zip"
                                       value="<?php echo $primary_physician->zip; ?>"
                                       placeholder="Zip">
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <input type="hidden" name="is_update"
                                   value="1"/>
                            <input type="hidden" name="user_id"
                                   value="<?php echo $primary_physician->userid; ?>" />
                            <label class="form-label semibold">&nbsp;</label>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Update Primary Physician</button>
                        </div>
                    <?php endif; ?>
                </div>

            </form>
        </div>
    </section>
</div>
<script type="text/javascript">
    jQuery(function ($) {
        function notyConfirmWithParam(message, onSuccessCallback, param) {
            var params = {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Ok',
                        onClick: function ($noty) {
                            $noty.close();
                            onSuccessCallback(param);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'Cancel',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            };

            notyMessage(message, 'warning', params);
        }
    });
</script>