<div class="page-content">
    <div class="container-fluid">
        <section class="box-typical">
            <header class="box-typical-header">
                <div class="tbl-row">
                    <div class="tbl-cell tbl-cell-title">
                        <h3>My Appointments</h3>
                    </div>
                </div>
            </header>
            <?php //print_r($appointments); ?>
            <div class="box-typical-body">
                <?php
                if ($appointments) {
                    if (is_array($appointments)) {
                        ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>
                                        Doctor
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>Time</th>
                                    <th>Symptom/Problem</th>
                                    <th>Concent To Treat</th>
                                    <th>Payment</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($appointments as $appointment) {
                                    ?>
                                    <tr>
                                        <td><?php echo $appointment->firstname . ' ' . $appointment->lastname; ?></td>
                                        <td><?php echo date("jS F Y", strtotime($appointment->start_date)); ?></td>
                                        <td><?php echo date('g:i A', strtotime($appointment->start_time)); ?>
                                            -<?php echo date('g:i A', strtotime($appointment->end_time)); ?></td>
                                        <td><?php echo $appointment->reason; ?></td>
                                        <td>Yes</td>
                                        <td>$100</td>
                                        <td><a class="btn btn-inline disabled">Start</a></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <?php
                    } else {
                        echo "No Appointments Found";
                    }
                }

                ?>
            </div><!--.box-typical-body-->
        </section><!--.box-typical-->
    </div>
</div>