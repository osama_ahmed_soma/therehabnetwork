<?php /** @var array $pastAppointments */?>
<?php /** @var array $futureAppointments */?>
<script>
    function paginationHtmlRendering(data, type){
        var html;
        console.log(typeof data);
        console.log(data.length);
        console.log(type);
        if (typeof data !== 'undefined' && data.length > 0){
            for (var i = 0; i < data.length; i++){
                var reason_name = data[i].primary_symptom;
                if ( data[i].reason_id == '13' ) {
                    reason_name = data[i].reason;
                }
                html += '<tr>' +
                    '<td>'+data[i].start_date+'</td>' +
                    '<td>'+data[i].start_time+'-'+data[i].end_time+'</td>' +
                    '<td>'+data[i].firstname+' '+data[i].lastname+'</td>' +
                    '<td>'+reason_name+'</td>';
                if(type == 'past'){
                    html += '<td>'+data[i].duration+'</td>';
                }
                html += '<td>' +
                    '<div class="btn-group ">';
                if(type == 'future') {
                    html += '<a class="btn btn-success request_to_start_consult_page" data-appointment_id="'+data[i].appointment_id+'" href="<?php echo base_url(); ?>appointment/start?aid='+data[i].appointment_id+'">Start</a>';
                }
                if(type == 'pending'){
                    html += '<a class="btn  btn-danger pending_appoint_for_patient" data-id="'+data[i].id+'" href="">Approve</a>';
                }
                if(type == 'canceled'){
                    html += '<button class="btn disabled" disabled="disabled">Pending Reschedule</button>';
                }
                if(type == 'past'){
                    html += '<a class="btn" href="<?php echo base_url(); ?>appointment/report?aid='+data[i].appointment_id+'">Report</a>';
                }
                html += '</div>' +
                    '</td>' +
                    '</tr> ';
            }
        } else {
            html += '<tr> \
                <td colspan="7">There are no upcoming appointments</td> \
            </tr>';
        }
        return html;
    }
</script>
<div class="page-content">
    <div class="container-fluid">

		<section class="navi-top">
        	<div class="navi-heading hidden">
            	<h2><a class="btn btn-danger open_schedule_appintment_patient"><i class="fa fa-clock-o"></i> Schedule Appointment</a></h2>
            </div>
            <div class="btn-section hidden">
            	<a class="btn btn-danger"><i class="fa fa-clock-o"></i> Schedule</a>
            	<a class="btn btn-success"><i class="fa fa-users"></i> Invite</a>
            </div>
        </section>
<!--        --><?php //if(!empty($futureAppointments)): ?>
            <section class="upcoming-sessions">
                <div class="navi-heading">
                    <h2><i class="fa fa-paper-plane" aria-hidden="true"></i> Upcoming Appointments</h2>
                </div>
                <strong>
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane fade in active" id="scheduled">

                            <?php
                            if (count($futureAppointments) > 0) {
                                $i = 0;
                                foreach ($futureAppointments as $appointment) {
                                    $futureAppointments[$i]->start_date = date("F jS, Y", ($appointment->start_date_stamp));
                                    $futureAppointments[$i]->start_time = date('g:i A', ($appointment->start_date_stamp));
                                    $futureAppointments[$i]->end_time = date('g:i A', ($appointment->end_date_stamp));
                                    $i++;
                                }
                            }


                            ?>

                            <?php $this->view('patient/_appointment_table', [
                                'appointments' => $futureAppointments,
                                'id' => 'futureAppointments',
                                'type' => 'future',
                                'is_physician' => $is_physician
                            ]); ?>

                        </div><!--.tab-pane-->
                        <div id="futureAppointments"></div>
                        <script>
                            $(document).ready(function () {
                                $('#futureAppointments').pagination({
                                    dataSource: <?php echo json_encode($futureAppointments); ?>,
                                    pageSize: 5,
                                    showPageNumbers: false,
                                    showNavigator: true,
                                    callback: function (data, pagination) {
                                        // template method of yourself
                                        var html = paginationHtmlRendering(data, 'future');
                                        $('#futureAppointments_render').html(html);
                                    }
                                });
                            });
                        </script>


                    </div><!--.tab-content--></strong>
            </section>
<!--        --><?php //endif; ?>
        <?php if ( !empty($pendingAppointments) ): ?>
        <section class="upcoming-sessions">
            <div class="navi-heading">
                <h2><i class="fa fa-reply" aria-hidden="true"></i> Pending Appointments</h2>
            </div>
            <strong>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="scheduled">
                        <?php

                        if (count($pendingAppointments)) {
                            $i = 0;
                            foreach ($pendingAppointments as $appointment) {
                                $pendingAppointments[$i]->start_date = date("F jS, Y", ($appointment->start_date_stamp));
                                $pendingAppointments[$i]->start_time = date('g:i A', ($appointment->start_date_stamp));
                                $pendingAppointments[$i]->end_time = date('g:i A', ($appointment->end_date_stamp));
                                $pendingAppointments[$i]->id = $appointment->appointment_id;
                                $i++;
                            }

                        }

                        ?>
                        <?php /*$this->view('doctor/_appointment_table', [

                            'appointments' => $pendingAppointments,

                            'id' => 'pendingAppointments',

                            'type' => 'pending',

                            'isPast' => false

                        ]); */?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead><tr>
                                    <th><i class="fa fa-calendar" aria-hidden="true"></i> Date</th>
                                    <th><i class="fa fa-clock-o" aria-hidden="true"></i> Time</th>
                                    <th><i class="fa fa-user" aria-hidden="true"></i> Provider</th>
                                    <th><i class="fa fa-stethoscope" aria-hidden="true"></i> Symptom/Problem</th>
                                    <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Action</th>
                                </tr></thead>
                                <tbody id="pendingAppointments_render">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="pendingAppointments"></div>
                    <script>
                        $(document).ready(function () {
                            $('#pendingAppointments').pagination({
                                dataSource: <?php echo json_encode($pendingAppointments); ?>,
                                pageSize: 5,
                                showPageNumbers: false,
                                showNavigator: true,
                                callback: function (data, pagination) {
                                    // template method of yourself
                                    var html = paginationHtmlRendering(data, 'pending');
                                    $('#pendingAppointments_render').html(html);
                                }
                            });
                        });
                    </script>
                </div>
            </strong>
        </section>
        <?php endif; ?>
        <?php if ( !empty($canceledAppointments) ): ?>
            <section class="upcoming-sessions">
                <div class="navi-heading">
                    <h2><i class="fa fa-reply" aria-hidden="true"></i> Cancelled Appointments</h2>
                </div>
                <strong>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="scheduled">
                            <?php

                            if (count($canceledAppointments)) {
                                $i = 0;
                                foreach ($canceledAppointments as $appointment) {
                                    $canceledAppointments[$i]->start_date = date("F jS, Y", ($appointment->start_date_stamp));
                                    $canceledAppointments[$i]->start_time = date('g:i A', ($appointment->start_date_stamp));
                                    $canceledAppointments[$i]->end_time = date('g:i A', ($appointment->end_date_stamp));
                                    $canceledAppointments[$i]->id = $appointment->appointment_id;
                                    $i++;
                                }

                            }

                            ?>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead><tr>
                                        <th><i class="fa fa-calendar" aria-hidden="true"></i> Date</th>
                                        <th><i class="fa fa-clock-o" aria-hidden="true"></i> Time</th>
                                        <th><i class="fa fa-user" aria-hidden="true"></i> Provider</th>
                                        <th><i class="fa fa-stethoscope" aria-hidden="true"></i> Symptom/Problem</th>
                                        <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Action</th>
                                    </tr></thead>
                                    <tbody id="canceledAppointments_render">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="canceledAppointments"></div>
                        <script>
                            $(document).ready(function () {
                                $('#canceledAppointments').pagination({
                                    dataSource: <?php echo json_encode($canceledAppointments); ?>,
                                    pageSize: 5,
                                    showPageNumbers: false,
                                    showNavigator: true,
                                    callback: function (data, pagination) {
                                        // template method of yourself
                                        var html = paginationHtmlRendering(data, 'canceled');
                                        $('#canceledAppointments_render').html(html);
                                    }
                                });
                            });
                        </script>
                    </div>
                </strong>
            </section>
        <?php endif; ?>
        <?php if(!empty($pastAppointments)): ?>
        <section class="upcoming-sessions">
        	<div class="navi-heading">
            	<h2><i class="fa fa-reply" aria-hidden="true"></i> Past Appointments</h2>
            </div>
        	<strong><div class="tab-content">



                <div role="tabpanel" class="tab-pane fade in active" id="past">

                    <?php

                    if (count($pastAppointments)) {
                        $i = 0;
                        foreach ($pastAppointments as $appointment) {
                            $pastAppointments[$i]->start_date = date("F jS, Y", ($appointment->start_date_stamp));
                            $pastAppointments[$i]->start_time = date('g:i A', ($appointment->start_date_stamp));
                            $pastAppointments[$i]->end_time = date('g:i A', ($appointment->end_date_stamp));
                            if (true) {
                                $pastAppointments[$i]->duration = gmdate("H:i:s", $appointment->duration);
                            }
                            $i++;
                        }

                    }

                    ?>

                    	<?php $this->view('patient/_appointment_table', [
                        'appointments' => $pastAppointments,
                        'id' => 'pastAppointments',
                        'type' => 'past',
                        'isPast' => true,
                        'is_physician' => $is_physician
                    ]); ?>

                        </div><!--.tab-pane-->

                        <div id="pastAppointments"></div>

                        <script>
                            $(document).ready(function () {
                                $('#pastAppointments').pagination({
                                    dataSource: <?php echo json_encode($pastAppointments); ?>,
                                    pageSize: 5,
                                    showPageNumbers: false,
                                    showNavigator: true,
                                    callback: function (data, pagination) {
                                        // template method of yourself
                                        var html = paginationHtmlRendering(data, 'past');
                                        $('#pastAppointments_render').html(html);
                                    }
                                });
                            });
                        </script>

                    </div><!--.tab-content--></strong>
            </section>
        <?php endif; ?>
        </div>
    </div>