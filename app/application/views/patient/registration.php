<link rel="stylesheet" href="<?php echo base_url(); ?>../css/style.css">
<!--<link rel="stylesheet" href="http://198.57.243.182/~myvirtualdoc/css/style.css">-->

<div class="top-header clearfix">
    <div class="pull-left web-logo"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>template_file/img/logo_patients_login.png" alt=""></a></div>
    <div class="pull-right top-hd-phone"><i class="fa fa-phone"></i> Inquire by Phone: <strong><a href="tel:1-855-80">1-855-80-DOCTOR</a></strong>
    </div>
</div>
<div class="log-back patient-signup-lckback">
	<div class="container">
		<div class="logfrom-main">
			<h1>
				See Your Doctor Within Minutes</h1>

			<div class="logfrom-left">
				<div class="logfrom-left-back"></div>

			</div>
			<div class="signup-right bbm-modal__topbar">
				<h2 class="text-center">Let's Get Started</h2>
				<p class="text-center" style="    font-size: 22px;">Creating an account is Free!</p>
				<div id="register_form_error"></div>

				<?php /* if ($this->session->flashdata('captcha_error')) { ?>
	                <p class="text-danger"><?php echo $this->session->flashdata('captcha_error'); ?></p>
				<?php } */ ?>

				<?php echo form_open('register/patient_create', ['class' => 'ng-invalid ng-invalid-required ng-dirty', 'id' => 'registrationForm', 'data-role' => 'form']); ?>
				<div>
					<div id="firstname-wrapper" class="form-group has-feedback margin-bottom-sm" style="    width: 49%;    float: left;	">
						<input class="form-control ng-pristine ng-valid" type="text" name="userFname" id="userFname" placeholder="First Name">
					</div>
					<div id="lastname-wrapper" class="form-group has-feedback margin-bottom-sm" style="    width: 50%;    float: left;    margin-right: 0px !important;">
						<input class="form-control ng-pristine ng-valid" type="text" name="userLname" id="userLname" placeholder="Last Name">
					</div>
					<div id="username-wrapper" class="form-group has-feedback margin-bottom-sm">
						<input class="form-control ng-isolate-scope ng-pristine ng-invalid ng-invalid-required ng-valid-email" autocomplete="off" type="text" name="userEmail" id="userEmail" placeholder="Email address">
					</div>
					<?php if ($GLOBALS['captcha_required']) { ?>
						<div class="form-group has-feedback margin-bottom-sm">
							<div class="g-recaptcha" data-sitekey="<?php echo CAPTCHA_SITE_KEY; ?>"></div>
						</div>
					<?php } ?>
					<div id="consent-box" class="form-group has-feedback" style="    margin-bottom: 3px;">
						<span class="required-checkbox consent-needed">
							<label class="cursor"style="font-size: 16px;">I agree to My Virtual Doctor's <input type="checkbox" name="consent" id="consent" class="checkbox cursor ng-isolate-scope ng-pristine ng-invalid ng-invalid-required"> </label>
							<br>
						</span>
						<div>
							<p class="tou-Terms" style="    font-size: 15px !important;"><span><a target="_blank" href="<?php echo base_url(); ?>../termandconditions.html" class="external">Terms of Use</a> and <a target="_blank" href="<?php echo base_url(); ?>../privacypolicy.html" class="external">Privacy Policy</a>	</span>
						</div>
					</div>
				</div>
				<button type="submit" id="btn-login" class="btn btn-info sing login btn-block" style="font-size: 23px; height: 54px;    font-weight: 600;    text-transform: uppercase;">Sign Up </button>

                <!--<h5> Already have an account? <a href="<?php echo base_url() . 'login.html' ?>" class="login">Log In Here   </a>
                <br> Doctor <a href="<?php echo base_url() . 'doctor-login.html' ?>" class="login">Sign in</a></h5> -->
				</form>
				<h5> Already a member?<a href="<?php echo base_url() . 'login' ?>"> Click here to login</a></h5>

				<div class="bbm-modal__bottombar hidden" style="margin-top: 0px;padding: 5px; text-align: center;">
					Forgot password? <a href="<?php echo base_url() . 'password-reset.html' ?>" class="login">Reset your password here   </a>
				</div>
				<div class="bbm-modal__bottombar hidden" style="margin-top: 0px;padding: 5px; text-align: center;">
					New to My Virtual Doctor?   <a class="signup" href="<?php echo base_url() . 'signup.html' ?>">Create a free account now</a>
				</div>
			</div>
		</div>

	</div>
</div>


<script type="text/javascript">
	jQuery(function ($) {

		//reigster_form
		$('#registrationForm').submit(function () {
			var th = $(this);

			//validate form
			var userFname = jQuery.trim($('#userFname').val());
			var userLname = jQuery.trim($('#userLname').val());

			var userEmail = jQuery.trim($('#userEmail').val());

//            var password = jQuery.trim( $('#password').val() );

//            var userTimeZone = jQuery.trim( $('#userTimeZone').val() );

			var errorStr = '';

			if (userFname == '') {
				errorStr += '<p><strong>First Name</strong> field is required.<p>';
			}

			if (userLname == '') {
				errorStr += '<p><strong>Last Name</strong> field is required.</p>';
			}

			if (userEmail == '') {
				errorStr += '<p><strong>Email address</strong> field is required.<p>';
			}

			/*if ( password == '' ) {
			 errorStr += '<p><strong>Password</strong> field is required.</p>';
			 }
			 
			 if ( userTimeZone == '' ) {
			 errorStr += '<p><strong>Time Zone</strong> field is required.</p>';
			 }*/

			if ($('#consent').is(":not(:checked)")) {

				errorStr += '<p>Please agree with <strong>Terms of Use</strong></p>';

			}

			if (errorStr != '') {
				var str = '<div class="alert alert-danger" role="alert">' + errorStr + '</div>';
				$('#register_form_error').html(str);
				return false;
			} else {
				$('#register_form_error').html('');
			}


			var data = $("#registrationForm").serializeArray();

			th.find('button').attr('disabled', true);

			$.ajax({
				url: th.attr('action'),
				data: data,
				method: "POST",
				dataType: "json"

			}).error(function (data) {
				data = JSON.parse(data.responseText);
				console.log(data)
				th.find('button').removeAttr('disabled');
				
				grecaptcha.reset();

				data.msg = '<div class="alert alert-danger alert-dismissible" role="alert">' + data.msg + '</div>';

				$('#register_form_error').html(data.msg);

			}).done(function () {
				th.find('button').removeAttr('disabled');
			}).success(function (data) {
				//data = JQuery.parseJSON(data);
				if (data.status == 'success') {
					var str = "";
					str += '<div class="alert alert-success alert-dismissible" role="alert"><div style="width: 80px; height: 80px; margin: 0px auto;"><img src="<?php echo base_url(); ?>../img/1471994079_Tick_Mark_Dark.png" style="position: relative;width: 80px;margin: 0px;padding: 0px;" /></div> \<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\<strong>Success!</strong> ' + data.msg + '</div>';
					$('#register_form_error').html(str);
					setTimeout(function () {
						$("#registrationForm").hide();
//                        window.location = '<?php //echo base_url();    ?>//patient/profile-settings.html';
					}, 3000);
				}
			}, 'json');

			return false;
		});
	});
	var toType = function(obj) {
			return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
		  }
</script>