<div class="homelogin">
    <div class="top-header clearfix">
        <div class="pull-left web-logo"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>template_file/img/logo_patients_login.png" alt=""></a></div>
        <div class="pull-right top-hd-phone"><i class="fa fa-phone"></i> Inquire by Phone: <strong><a href="tel:1-855-80">1-855-80-DOCTOR</a></strong></div>
    </div>
    
    <div class="page-content registration_content">
        <div class="container-fluid">
            <div class="registration-header">
                <img src="<?php echo base_url(); ?>template_file/img/logo-my_virtual_doctor.png">
    
            </div>
            <center><?php echo validation_errors(); ?>
                <?php echo $this->session->flashdata('message'); ?>
            </center>
            <div class="card-block">
                <div class="row">
                </div>
            </div>
				<?php echo form_open('register/patient'); ?>
				
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xl-6 col-md-offset-3 col-lg-offset-3">
                        <section class="card">
                            <div class="card-block">
                                <div class="row">
                                    <center>
                                        <div class="registration-header">
                                            <h4> Let’s Get Started - Create An Account</h4>
                                            <p> Finding the best telehealth care has never been easier.</p>
                                            <p> Join Us FREE!</p>
                                        </div>
                                    </center>
                                    <!--<hr style="margin: 10px 0">-->
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">Email Address</label>
                                            <input type="text" class="form-control" name="email" value=""
                                                   placeholder="Email Address">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <fieldset class="form-group">
                                            <label class="form-label semibold">Create Password</label>
                                            <input type="password" class="form-control" name="password" value=""
                                                   placeholder="Password">
                                        </fieldset>
                                    </div>
                                    <div
                                        class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
                                        <input type="hidden" value="1" name="save">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block">Create Account
                                        </button>
                                    </div>
                                    <!--<div class="checkbox">
                                        <label class="checkbox-inline"><input type="checkbox">By signing up, I agree to My Virtual Doctor <a
                                                href="#">Terms of Use</a> and <a href="#">Privacy Policy</a></label>
                                    </div>-->
                                </div>
                            </div>
                            <div class="row">
                                <!--<div class="col-md-4 col-lg-4 col-sm-4 col-md-offset-4 col-lg-offset-4 col-sm-offset-4">
                                    <input type="hidden" value="1" name="save">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Create Account</button>
                                </div>-->
                                <div class="checkbox ">
                                    <label class="checkbox-inline"><input type="checkbox">By signing up, I agree to My
                                        Virtual Doctor
                                        <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a></label>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </form>
    
            <!--<div class="row">
                <div class="checkbox">
                    <label class="checkbox-inline"><input type="checkbox">By signing up, I agree to My Virtual Doctor <a
                            href="#">Terms of Use</a> and <a href="#">Privacy Policy</a></label>
                </div>
                <div class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
    
                </div>
            </div>-->
            <div class="row">
                <div class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
                    <label class="">Already have an account? <a href="<?php echo site_url("/login"); ?>">Login
                            here!</a></label>
                </div>
            </div>
        </div>
    </div>
</div>    

