<div class="page-content">
    <div class="container-fluid">
        <section class="tabs-section">
            <div class="tabs-section-nav tabs-section-nav-icons">
                <div class="tbl">
                    <ul class="nav" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link <?php if ($this->uri->segment(2) == 'emailpassword' ) echo "active"; ?>" href="<?php echo base_url() . 'change/emailpassword'; ?>">
                                <span class="nav-link-in">
                                    Change Password
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div><!--.tabs-section-nav-->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-2">
                    <section class="card">
                        <div class="card-block">
                            <?php echo validation_errors(); ?>
                            <?php echo $this->session->flashdata('message'); ?>

                            <?php echo form_open('change/emailpassword'); ?>
                            <div class="row">
                                <span style="text-align: center;">
                                    <div class="registration-header">
                                        <h4> Change Your Password</h4>
                                    </div>
                                </span>
                                <!--<hr style="margin: 10px 0">-->
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Enter Password</label>
                                        <input type="password" class="form-control" name="password"
                                               value="<?php echo set_value('password'); ?>"
                                               placeholder="Password">
                                    </fieldset>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Retype Password</label>
                                        <input type="password" class="form-control" name="passconf"
                                               value="<?php echo set_value('passconf'); ?>"
                                               placeholder="Confirm Password">
                                    </fieldset>
                                </div>
                                <div
                                    class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Change</button>
                                </div>
                            </div>

                            </form>
                        </div>
                    </section>
                </div>
            </div><!--.tab-content-->
        </section><!--.tabs-section-->
    </div>
</div>
<script type="text/javascript">
    jQuery(function($){
        function notyConfirmWithParam(message, onSuccessCallback, param) {
            var params = {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Ok',
                        onClick: function($noty) {
                            $noty.close();
                            onSuccessCallback(param);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'Cancel',
                        onClick: function($noty) {
                            $noty.close();
                        }
                    }
                ]
            };

            notyMessage(message, 'warning', params);
        }
    });
</script>

<!--<div class="page-content registration_content">
    <div class="container-fluid">
        <div class="registration-header">
            <img src="<?php /*echo base_url(); */?>template_file/img/logo-my_virtual_doctor.png">

        </div>
        <center><?php /*echo validation_errors(); */?>
            <?php /*echo $this->session->flashdata('message'); */?>
        </center>
        <div class="card-block">
            <div class="row">
            </div>
        </div>
        <form method="post">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 col-xl-6 col-md-offset-3 col-lg-offset-3">
                    <section class="card">
                        <div class="card-block">
                            <div class="row">
                                <center>
                                    <div class="registration-header">
                                        <h4> Change Your Email - Password</h4>
                                    </div>
                                </center>

                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Enter Email</label>
                                        <input type="text" class="form-control" name="email"
                                               value="<?php /*echo $user->user_email; */?>"
                                               placeholder="Email Address">
                                    </fieldset>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold">Enter Password</label>
                                        <input type="password" class="form-control" name="password"
                                               value="<?php /*echo $user->user_password; */?>"
                                               placeholder="Password">
                                    </fieldset>
                                </div>
                                <div
                                    class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
                                    <input type="hidden" value="1" name="save">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </form>
    </div>
</div>-->

