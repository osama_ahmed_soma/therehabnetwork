<?php
function searchFor($id, $array, $type)
{
    $result = array();
    foreach ($array as $val) {
        if (is_array($val)) {
            foreach ($val as $v) {
                if ($v->userid == $id) {
                    $result[] = $v->$type;
                }
            }
        }
    }
    return $result;
}

?>
<div class="page-content">

    <div class="container-fluid">
        <section class="navi-top clearfix">
            <div class="navi-heading">
                <h2>My Physicians</h2>
            </div>

        </section>
    </div>
    <script>
        function removeDoctorFromFavourite(prividerID, obj){
            var str = "<?php echo site_url('/patient/remove_from_favorite'); ?>";
            var d_id = prividerID;
            var p_id = <?php echo $this->session->userdata("userid"); ?>;
            $.ajax({
                type: "POST",
                url: str,
                data: {p_id: p_id, d_id: d_id},
                dataType: "json",
                success: function (data) {
                    notySuccess('Removed');
                    $(obj).parent().parent().parent().remove();
                    if(document.getElementsByClassName("doctors_home_area")[0].childElementCount <= 0){
                        var child = document.getElementsByClassName("doctors_home_area")[0];
                        child.parentNode.removeChild(child);
                        $('.no_provider_selecter').html('<div class="col-md-12"><h4>You are currently not connected to any doctors. Click <a href="<?php echo base_url(); ?>patient">here</a> to find a doctor!</h4></div>');
                    }
                }
            });
        }
        function removeDoctorFromFavouriteApproval(providerID, obj){
            var params = {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function($noty) {
                            $noty.close();
                            removeDoctorFromFavourite(providerID, obj);
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function($noty) {
                            $noty.close();
                        }
                    }
                ]
            };
            notyMessage('Are you sure you want to remove this physician?', 'warning', params);
        }
    </script>
    <section class="myprovder_page clearfix">
        <div class="container-fluid no_provider_selecter">

            <?php if (is_array($providers['general_info'])): ?>
                <div class="doctors_home_area">
                    <?php foreach ($providers['general_info'] as $provider): ?>
                        <div class="col-lg-3 col-md-4 col-sm-6 single-box">
                            <div class="our-doctor">
                                <div class="btn-close"><a href="javascript:void(0)"
                                                          onclick="removeDoctorFromFavouriteApproval(<?php echo $provider->userid; ?>, this)"><i
                                            class="fa fa-close"></i></a></div>
                                <div class="pic">
                                    <a href="<?php echo site_url('/patient/doctorinfo/' . $provider->userid); ?>">
                                        <img src="<?php echo default_image($provider->image_url); ?>" />
                                        <?php /*?><div class="pic-img"
                                             style="background:url(<?php echo default_image($provider->image_url); ?>) no-repeat;"></div><?php */?>
                                    </a>

                                </div>
                                <div class="team_prof">
                                    <h3 class="names"><?php echo "Dr. " . $provider->firstname . " " . $provider->lastname; ?></h3>
                                    <div class="rating">
                                        <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                                    </div>
                                    <div class="connection-meter text-center">
                                        <i class="font-icon fa fa-stethoscope" aria-hidden="true"></i>
                                        <?php $special = searchFor($provider->userid, $providers['specialization'], 'speciality'); ?>
                                        <?php if (is_array($special) && count($special) > 0): ?>
                                            <?php $i = 0;
                                            $count = count($special); ?>
                                            <?php foreach ($special as $value): ?>
                                                <?php if ($i == ($count - 1)): ?>
                                                    <span><?php echo $value; ?></span>
                                                <?php else: ?>
                                                    <span><?php echo $value; ?>, </span>
                                                <?php endif; ?>
                                                <?php $i++; ?>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <span>N/A</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="view_profile">
                                    <a href="<?php echo site_url('/patient/doctorinfo/' . $provider->userid); ?>"
                                       class="btn btn-inline btn-primary">View Profile</a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php else: ?>
                <div class="col-md-12">
                    <h4>You are currently not connected to any doctors. Click <a href="<?php echo base_url(); ?>patient">here</a> to find a doctor!</h4>
                </div>
            <?php endif; ?>
        </div>
    </section>
</div>