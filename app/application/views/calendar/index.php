<?php /** @var $zones */ ?>
<?php /** @var array $times */ ?>

<div class="page-content">
    <div class="container-fluid">
        	  <header class="box-typical-header">
            <div class="tbl-row">
                <div class="tbl-cell tbl-cell-title">
                    <h3>My Calendar</h3>
                </div>
            </div>
        </header>
		<div class="legend">
        	<button type="button" class="btn btn-inline btn-primary pull-left" data-toggle="modal" onclick="fc_popover_time_change(false)" data-target="#myModal">Add Available Time Slot</button>
        
			<div class="aval"><i class="fa fa-circle" aria-hidden="true"></i> <span>Availability Time</span></div>
			<div class="pend"><i class="fa fa-circle" aria-hidden="true"></i>  <span>Pending Appointment (Awaiting approval)</span></div>
			<div class="conf"><i class="fa fa-circle" aria-hidden="true"></i> <span>Confirmed Session</span></div>
		</div>
        <div class="box-typical">
            <div class="calendar-page">
                <div class="calendar-page-content">
                    <div class="calendar-page-title">My Schedules</div>
                    <div class="calendar-page-content-in">

                        <div class="row">
                            <div class="col-md-12" style="margin-top: 100px;margin-bottom: 20px;">

                                <?php $this->view('calendar/_add_time_slot_wo_timezone', ['times' => $times]); ?>

                            </div>

                        </div>
                        <div class="extra"></div>
                        <div id='calendar'></div>

                    </div><!--.calendar-page-content-in-->
                </div><!--.calendar-page-content-->
            </div><!--.calendar-page-->
        </div><!--.box-typical-->

    </div>
</div>

<script type="text/template" id="popupCreateAvail">
    <div class="fc-popover click">
        <div class="fc-header">Set available time
            <button type="button" class="cl"><i class="font-icon-close-2"></i></button>
        </div>
        <div class="success" style="color: green; background-color: #fff; text-align: center; padding: 10px;"></div>
        <div class="error" style="color: #f00; background-color: #fff; text-align: center; padding: 10px;"></div>
        <form id="ClickPopUp_custom_form" class="ClickPopUp_custom_form">
            <div class="fc-body edit-event" style="display: block;">
                <div class="form-group">
                    <div class="input-group">

                        <select name="start_time" class="select2" onchange="fc_popover_time_change(true)">
                            <?php
                            foreach ($times as $timeKey => $timeValue) {
                                echo "<option value='" . $timeKey . "'>" . $timeValue . "</option>";
                            }
                            ?>
                        </select>
                        <span class="input-group-addon"><i class="font-icon font-icon-clock"></i></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <select name="end_time" class="select2" onchange="fc_popover_time_change(true)">
                            <?php
                            foreach ($times as $timeKey => $timeValue) {
                                echo "<option value='" . $timeKey . "'>" . $timeValue . "</option>";
                            }
                            ?>
                        </select>
                        <span class="input-group-addon"><i class="font-icon font-icon-clock"></i></span>
                    </div>
                </div>
                <div class="text-center">
                    <input class="form-control start_date" name="start_date" value="" type="hidden">
                    <input class="form-control end_date" name="end_date" value="" type="hidden">
                    <button type="button" class="btn btn-rounded btn-sm onclickPopUpSaveButton" onclick="onClickPopUpFunc()">Save</button>
                    <button type="button" class="btn btn-rounded btn-sm btn-default remove-popover">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</script>

<script type="text/template" id="calendarPopup">
    <div class="fc-popover click">
        <div class="fc-header">
            <button type="button" class="cl"><i class="font-icon-close-2"></i></button>
        </div>
        <div class="fc-body main-screen">
            <p>
            </p>
            <p class="color-blue-grey">Name Surname Patient<br/>Surgey ACL left knee</p>
            <ul class="actions">
                <li><a href="#">More details</a></li>
                <li><a href="#" class="fc-event-action-edit">Edit event</a></li>
                <li><a href="#" class="fc-event-action-remove">Remove</a></li>
            </ul>
        </div>
        <div class="fc-body remove-confirm">
            <p>Are you sure to remove event?</p>
            <div class="text-center">
                <button type="button" class="btn btn-rounded btn-sm">Yes</button>
                <button type="button" class="btn btn-rounded btn-sm btn-default remove-popover">No</button>
            </div>
        </div>
        <div class="fc-body edit-event">
            <p>Edit event</p>
            <div class="form-group">
                <div class="input-group date datetimepicker">
                    <input type="text" class="form-control"/>
                    <span class="input-group-addon"><i class="font-icon font-icon-calend"></i></span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group date datetimepicker-2">
                    <input type="text" class="form-control"/>
                    <span class="input-group-addon"><i class="font-icon font-icon-clock"></i></span>
                </div>
            </div>
            <div class="form-group">
                <textarea class="form-control" rows="2">Name Surname Patient Surgey ACL left knee</textarea>
            </div>
            <div class="text-center">
                <button type="button" class="btn btn-rounded btn-sm" >Save</button>
                <button type="button" class="btn btn-rounded btn-sm btn-default remove-popover">Cancel</button>
            </div>
        </div>
    </div>
</script>

<script type="text/javascript">

    function close_fc_popover(){

    }

    function fc_popover_time_change(condition){
        if(condition){
            $('.fc-popover').find('div.error').html('');
            var startTime = $('.fc-popover').find('select[name=start_time]').val();
            var endTime = $('.fc-popover').find('select[name=end_time]').val();
            var total_start_time_in_sec = (startTime.split(":")[1] * 60) + (startTime.split(":")[0] * 3600);
            var total_end_time_in_sec = (endTime.split(":")[1] * 60) + (endTime.split(":")[0] * 3600);
            if(total_end_time_in_sec <= (total_start_time_in_sec + 1799)){
//            error
//            console.log("your end time is less than start time");
                $('.fc-popover').find('div.error').html("Your end time must be 30 minutes greater than your start time.");
                $('.fc-popover').find('button.onclickPopUpSaveButton').addClass('disabled').prop('disabled', true);
            } else {
//            true
//            console.log("your end time is greater and not equal to start time good to go.");
                $('.fc-popover').find('div.error').html('');
                $('.fc-popover').find('button.onclickPopUpSaveButton').removeClass('disabled').prop('disabled', false);
            }
        } else {
            $('.calendar_avail').find('div.error').html('');
            var startTime = $('.calendar_avail').find('select[name=start_time]').val();
            var endTime = $('.calendar_avail').find('select[name=end_time]').val();
            var total_start_time_in_sec = (startTime.split(":")[1] * 60) + (startTime.split(":")[0] * 3600);
            var total_end_time_in_sec = (endTime.split(":")[1] * 60) + (endTime.split(":")[0] * 3600);
            if(total_end_time_in_sec <= (total_start_time_in_sec + 1799)){
                $('.calendar_avail').find('div.error').html('<div class="col-md-12 text-danger text-center">Your end time must be 30 minutes greater than your start time.</div>');
                $('.calendar_avail').find('button.add_availability').addClass('disabled').prop('disabled', true);
            } else {
                $('.calendar_avail').find('div.error').html('');
                $('.calendar_avail').find('button.add_availability').removeClass('disabled').prop('disabled', false);
            }
        }


    }

    function removeAllPopovers() {
        $('.fc-popover.click').remove();
    }

    function initPopoverCloseButton() {
        $('.fc-popover.click .cl, .fc-popover.click .remove-popover').click(function () {
            $('.fc-popover.click').remove();
            $('.fc-event').removeClass('event-clicked');

            if (fullcalendar) {
                fullcalendar.fullCalendar( 'unselect' );
            }
        });
    }

    var clickEvent;
    var fullcalendar;
    var dateClicked;
    function onClickPopUpFunc(){

        var str = "<?php echo base_url(); ?>availability/add_new";
        var f = $("#ClickPopUp_custom_form").serialize();
        $.ajax({
            type: "POST",
            url: str,
            data: f,
            success: function (data) {
                removeAllPopovers();
                var result = JSON.parse(data);
                if(result.bool){
                    notySuccess(result.message);
                } else {
                    notyError(result.message);
                }
                $('#calendar').fullCalendar('refetchEvents');
            }
        });
    }

    function onClickPopUpUpdate(){
        var str = "<?php echo base_url(); ?>availability/update";
        var f = $("#updateForm").serialize();
        $.ajax({
            type: "POST",
            url: str,
            data: f,
            //dataType: "text",
            success: function (data) {
                $('.success').html(data);
//                setTimeout(function(){ location.reload(); }, 1500);
            }
        });
    }

    function remove_available_time(id){
        removeAllPopovers();
        var str = "<?php echo base_url(); ?>availability/remove";
        if(id != ''){
            $.ajax({
                type: "POST",
                url: str,
                data: "id="+id,
                success: function (data) {
                    notySuccess('Availability Deleted');
                    $('#calendar').fullCalendar('refetchEvents');
                }
            });
        }
    }

    $(document).ready(function () {


        fullcalendar = $('#calendar').fullCalendar({
            header: {
                left: '',
                center: 'prev, title, next',
                right: 'agendaDay,agendaTwoDay,agendaWeek,month'
            },
            slotDuration: "00:30:01",
            buttonIcons: {
                prev: 'font-icon font-icon-arrow-left',
                next: 'font-icon font-icon-arrow-right',
                prevYear: 'font-icon font-icon-arrow-left',
                nextYear: 'font-icon font-icon-arrow-right'
            },
            editable: false,
            selectable: true,
            unselectAuto: false,
            now: <?php echo json_encode($now) ?>,
            eventLimit: 4, // allow "more" link when too many events
            events: "<?php echo base_url(); ?>availability/get_available",
            eventRender: function(event, element, view) {

				element.find(".fc-time").html(moment(event.start).format("hh:mm A"));
	
                if(event.class == 'appointed pending'){
                    $(element).css({
                        'background-color': 'rgb(167, 18, 18)'

                    });
                }
                if(event.class == 'appointed'){
                    $(element).css({
                        'background-color': 'rgb(11, 125, 19)'

                    });
                }

            },
            viewRender: function (event, element, view) {
//				console.log(event);
//				console.log(element.html());
//				console.log(view);
                if (!("ontouchstart" in document.documentElement)) {
                    $('.fc-scroller').jScrollPane({
                        autoReinitialise: true,
                        autoReinitialiseDelay: 100
                    });
                }

                removeAllPopovers();
            },
            select: function(start, end, jsEvent, view, resource) {

                var start_date, end_date, start_time, end_time;
                start_date = start.format('YYYY-MM-DD');
                start_time = start.format('HH:mm');
                end_date = end.format('YYYY-MM-DD');
                end_time = end.add(30, "minutes").format('HH:mm');
                if(view.name == 'month'){
                    end_date = end.subtract(1, "days").format('YYYY-MM-DD');
                }

                clickEvent = jsEvent;

                removeAllPopovers();
                $('body').append($('#popupCreateAvail').html());

                $('.fc-popover').find('select[name=start_time]').val(start_time);//.trigger('change.select2');
                $('.fc-popover').find('select[name=end_time]').val(end_time);//.trigger('change.select2');
                $('.fc-popover').find('input[name=start_date]').val(start_date);
                $('.fc-popover').find('input[name=end_date]').val(end_date);

                $(".select2").select2();

                $('.fc-popover.click').css({
                    left: clickEvent.pageX,
                    top: clickEvent.pageY
                });

                fc_popover_time_change(true);

                // Close buttons
                initPopoverCloseButton();
            },
            eventMouseout: function(event, jsEvent, view) {
                $('.fc-event').removeClass('event-clicked');
            },
            eventClick: function (calEvent, jsEvent, view) {

                var eventEl = $(this);

                // Add and remove event border class
                if (!$(this).hasClass('event-clicked')) {
                    $('.fc-event').removeClass('event-clicked');

                    $(this).addClass('event-clicked');
                }

                // Remove old popover
                removeAllPopovers();

                // Add popover
                var html = '';
                html += '<div class="fc-popover click">' +
                    '<div class="fc-header">Available Time<button type="button" class="cl"><i class="font-icon-close-2"></i></button>' +
                    '</div>' +
                    '<div class="fc-body main-screen">' +
                    '<div class="row"><div class="col-md-10">' +
                        calEvent.innerText +
                    '</div>' +
                    '</div>' +
                    '<div class="row"><div class="col-md-10">' +
                    moment(calEvent.start).format('hh:mma') + ' - ' + moment(calEvent.end).format('hh:mma') +
                    ' <?php // echo date('T'); ?></div>' +
                    '<!--<div class="col-md-2"> <button type="button" class="btn btn-success fc-event-action-edit" style="padding: 5px;line-height: 1;"><i class="glyphicon glyphicon-edit"></i></button></div>-->';
                        if(calEvent.class == 'not_appointed') {
                            html += '<div class="col-md-2"> <button type="button" class="btn btn-danger fc-event-action-remove" style="padding: 5px;line-height: 1;"><i class="glyphicon glyphicon-remove"></i></button></div></div>';
                        }
                    html += '</div>' +
                    '<div class="fc-body remove-confirm">' +
                    '<p>Are you sure to remove event?</p>' +
                    '<div class="text-center">' +
                    '<input type="hidden" id="remove_time_id" value="'+calEvent.id+'">' +
                    '<button type="button" class="btn btn-rounded btn-sm" onclick="remove_available_time('+calEvent.id+')">Yes</button>' +
                    '<button type="button" class="btn btn-rounded btn-sm btn-default remove-popover">No</button>' +
                    '</div>' +
                    '</div>' +
                    '<div class="fc-body edit-event">' +
                    '<p>Edit event</p>' +
                    '<div class="success" style="color: green; background-color: #fff; text-align: center; padding: 10px;"></div>' +
                    '<div class="error" style="color: #f00; background-color: #fff; text-align: center; padding: 10px;"></div>' +
                    '<form id="updateForm">' +
                    '<div class="form-group">' +
                    /*'<div class="input-group date datetimepicker">' +
                     '<input type="text" class="form-control" />' +
                     '<span class="input-group-addon"><i class="font-icon font-icon-calend"></i></span>' +
                     '</div>' +*/
                    '<div class="input-group">' +
                    '<select name="start_time" class="select2" onchange="fc_popover_time_change(true)">';
                    <?php
                    foreach ($times as $timeKey => $timeValue):
                    ?>
                        if(moment(calEvent.start).format('HH:mm') == '<?php echo $timeKey; ?>'){
                            html += '<option value="<?php echo $timeKey; ?>" selected><?php echo $timeValue; ?></option>';
                        } else {
                            html += '<option value="<?php echo $timeKey; ?>"><?php echo $timeValue; ?></option>';
                        }
                    <?php
                    endforeach;
                    ?>
                html += '</select>' +
                    '<span class="input-group-addon"><i class="font-icon font-icon-clock"></i></span>' +
                    '</div>' +
                    '</div>' +
                    /*'<div class="form-group">' +
                    '<div class="input-group date datetimepicker-2">' +
                    '<input type="text" class="form-control" />' +
                    '<span class="input-group-addon"><i class="font-icon font-icon-clock"></i></span>' +
                    '</div>' +
                    '</div>' +*/
                    '<div class="form-group">' +
                    '<div class="input-group">' +
                    '<select name="end_time" class="select2" onchange="fc_popover_time_change(true)">';
                        <?php
                        foreach ($times as $timeKey => $timeValue):
                        ?>
                            if(moment(calEvent.end).format('HH:mm') == '<?php echo $timeKey; ?>'){
                                html += '<option value="<?php echo $timeKey; ?>" selected><?php echo $timeValue; ?></option>';
                            } else {
                                html += '<option value="<?php echo $timeKey; ?>"><?php echo $timeValue; ?></option>';
                            }
                        <?php
                            endforeach;
                        ?>
                    html += '</select>' +
                        '<span class="input-group-addon"><i class="font-icon font-icon-clock"></i></span>' +
                        '</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<textarea class="form-control" rows="2">Name Surname Patient Surgey ACL left knee</textarea>' +
                    '</div>' +
                    '<div class="text-center">' +
                        '<input type="hidden" name="start_date" value="'+moment(calEvent.start).format('YYYY-MM-DD')+'">' +
                        '<input type="hidden" name="end_date" value="'+moment(calEvent.end).format('YYYY-MM-DD')+'">' +
                        '<input type="hidden" name="id" value="'+calEvent.id+'">' +
                    '<button type="button" class="btn btn-rounded btn-sm onclickPopUpSaveButton" onclick="onClickPopUpUpdate()">Save</button>' +
                    '<button type="button" class="btn btn-rounded btn-sm btn-default remove-popover">Cancel</button>' +
                    '</div>' +
                        '</form>' +
                    '</div>' +
                    '</div>';
                $('body').append(html);

                // Datepicker init
                $('.fc-popover.click .datetimepicker').datetimepicker({
                    widgetPositioning: {
                        horizontal: 'right'
                    }
                });

                $('.fc-popover.click .datetimepicker-2').datetimepicker({
                    widgetPositioning: {
                        horizontal: 'right'
                    },
                    format: 'LT',
                    debug: true
                });


                // Position popover
                function posPopover() {
                    $('.fc-popover.click').css({
                        left: eventEl.offset().left + eventEl.outerWidth() / 2,
                        top: eventEl.offset().top + eventEl.outerHeight()
                    });
                }

                posPopover();

                $('.fc-scroller, .calendar-page-content, body').scroll(function () {
                    posPopover();
                });

                $(window).resize(function () {
                    posPopover();
                });



                // Actions link
                $('.fc-event-action-edit').click(function (e) {
                    e.preventDefault();

                    $('.fc-popover.click .main-screen').hide();
                    $('.fc-popover.click .edit-event').show();
                });

                $('.fc-event-action-remove').click(function (e) {
                    e.preventDefault();

                    $('.fc-popover.click .main-screen').hide();
                    $('.fc-popover.click .remove-confirm').show();
                });

                initPopoverCloseButton();
            },
            dayClick: function(date, jsEvent, view) {
                dateClicked = date.format();
            }
        });

        $('#calendar').fullCalendar('option', 'timezone', '<?php echo $time_zone; ?>');



        $("#repeat-toggle").on("change", function() {
           console.log($(this).prop("checked"));
        });





        $(".add_availability").click(function (e) {
            e.preventDefault();
            $(this).prop('disabled', true);
            var str = "<?php echo base_url(); ?>availability/add_new";
            var f = $(".calendar_avail").serialize();
            /*console.log(f);*/
            $.ajax({
                type: "POST",
                url: str,
                data: f,
                //dataType: "text",
                success: function (data) {
                    removeAllPopovers();
                    var result = JSON.parse(data);
                    if(result.bool){
                        notySuccess(result.message);
                    } else {
                        notyError(result.message);
                    }
                    $('#calendar').fullCalendar('refetchEvents');
                }
            });
        });
    });
</script>

