<?php /** @var array $times */?>

<div class="modal fade"
     id="myModal"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal"
                        aria-label="Close">
                    <i class="font-icon-close-2"></i>
                </button>
                <h4 class="modal-title" id="myModalLabel">Set Availability for
                    appointment</h4>
            </div>
            <form class="calendar_avail">
                <div class="modal-body">
                    <div class="row">
                        <div class="start_time">
                            <div class="col-md-2">
                                Start Time:
                            </div>
                            <div class="col-md-4">
                                <div class="input-group date datetimepicker-1">
                                    <input class="form-control start_date"
                                           name="start_date" value="" type="text"
                                    >
                                    <span class="input-group-addon">
                                                                        <i class="font-icon font-icon-calend"></i>
                                                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <select name="start_time" class="select2">
                                        <?php
                                            foreach ($times as $timeKey => $timeValue) {
                                                echo "<option value='" . $timeKey . "'>" . $timeValue . "</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select name="start_timezone" class="select2">
                                        <?php foreach ($zones as $zoneKey => $zoneValue) { ?>
                                            <option value=<?php echo $zoneKey ?>><?php echo $zoneValue ?></option>";
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="end_time">
                            <div class="col-md-2">
                                End Time:
                            </div>
                            <div class="col-md-4">
                                <div class="input-group date datetimepicker-1">
                                    <input class="form-control start_date"
                                           name="end_date" value="" type="text"
                                           placeholder="date">
                                    <span class="input-group-addon">
                                                                        <i class="font-icon font-icon-calend"></i>
                                                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <select name="end_time" class="select2" placeholder="time">
                                        <?php
                                            foreach ($times as $timeKey => $timeValue) {
                                                echo "<option value='" . $timeKey . "'>" . $timeValue . "</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select name="end_timezone" class="select2">
                                        <?php foreach ($zones as $zoneKey => $zoneValue) { ?>
                                            <option value=<?php echo $zoneKey ?>><?php echo $zoneValue ?></option>";
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row repeat-row">
                        <div class="repeat-box">
                            <div class="checkbox-bird">
                                <input type="checkbox" name="repeat" value="1" id="repeat-toggle"><label
                                    for="repeat-toggle">Repeat</label>
                            </div>
                            <div class="toggle-box" style="display: none;">
                                <div class="row">
                                    <span>Repeats</span>
                                    <span class="select2-wrapper">
                                                                        <select name="repeat_type"
                                                                                class="select2 repeat-type"
                                                                                ng-model="type">
                                                                            <option value="day" selected="selected">Daily</option>
                                                                            <option value="week">Weekly</option>
                                                                            <option value="month">Monthly</option>
                                                                            <!--                                                                            <option value="year">Yearly</option>-->
                                                                        </select>
                                                                    </span>
                                    <span>Repeats every</span>
                                    <input type="number" name="repeat_frequency"
                                           value="1" placeholder="0">
                                    <label id="repeat-count">{{type}}</label>
                                </div>
                                <div class="row" ng-show="type=='week'">
                                    <span>Repeats on</span>
                                    <div class="checkbox-bird"><input type="checkbox"
                                                                      id="repeat-sun"
                                                                      name="day[]"
                                                                      value="sunday"><label
                                            for="repeat-sun">Sun</label></div>
                                    <div class="checkbox-bird"><input type="checkbox"
                                                                      id="repeat-mon"
                                                                      name="day[]"
                                                                      value="monday"><label
                                            for="repeat-mon">Mon</label></div>
                                    <div class="checkbox-bird"><input type="checkbox"
                                                                      id="repeat-tue"
                                                                      name="day[]"
                                                                      value="tuesday"><label
                                            for="repeat-tue">Tue</label></div>
                                    <div class="checkbox-bird"><input type="checkbox"
                                                                      id="repeat-wed"
                                                                      name="day[]"
                                                                      value="wednesday"><label
                                            for="repeat-wed">Wed</label></div>
                                    <div class="checkbox-bird"><input type="checkbox"
                                                                      id="repeat-thu"
                                                                      name="day[]"
                                                                      value="thursday"><label
                                            for="repeat-thu">Thu</label></div>
                                    <div class="checkbox-bird"><input type="checkbox"
                                                                      id="repeat-fri"
                                                                      name="day[]"
                                                                      value="friday"><label
                                            for="repeat-fri">Fri</label></div>
                                    <div class="checkbox-bird"><input type="checkbox"
                                                                      id="repeat-sat"
                                                                      name="day[]"
                                                                      value="saturday"><label
                                            for="repeat-sat">Sat</label></div>

                                </div>
                                <div class="row">
                                    <span>Ends</span>
                                    <!--<div class="checkbox-detailed">
                                        <input type="radio" name="repeat-end" id="repeat-end-never" value="never" checked="">
                                        <label for="repeat-end-never">
                                            <span class="checkbox-detailed-tbl">
                                                <span class="checkbox-detailed-cell">Never</span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="checkbox-detailed">
                                        <input type="radio" name="repeat-end" id="repeat-end-on" value="on">
                                        <label for="repeat-end-on">
                                            <span class="checkbox-detailed-tbl">
                                                <span class="checkbox-detailed-cell">On</span>
                                            </span>
                                        </label>
                                    </div>-->
                                    <div class="date-input-wrapper">
                                        <div class="input-group date datetimepicker-1">
                                            <input class="form-control" name="end_loop"
                                                   value="" type="text" placeholder="">
                                            <span class="input-group-addon">
                                                                                <i class="font-icon font-icon-calend"></i>
                                                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="row">
                                    <span>Summary</span>
                                    <span id="repeat-summary">Every <span id="repeat-summary-count"></span><span id="repeat-summary-type">day</span><span id="repeat-summary-weekdays"></span></span>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--<button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button>-->
                    <button type="button" class="btn btn-rounded btn-primary add_availability">Add Availability
                    </button>
                </div>
            </form>

        </div>
    </div>
</div><!--.modal-->