<?php
if (count($records) > 0 && is_array($records)) {
	foreach ($records as $rec) {
//		var_dump($rec);
		?>
		<tr class="table-row no-records no-records-delete" data-to-delete="<?php echo $rec->id; ?>">
			<!--<td	class="table-cell"><?php echo (empty($rec->title)) ? $rec->filename : $rec->title; ?></td>-->
			<td class="table-cell"><?php echo $rec->shared_with; ?></td>
			<td class="table-cell"><?php echo date('m/d/Y', strtotime($rec->date_time)); ?></td>
			<td class="table-cell">
				<a class="delete_shared" data-rowid="<?php echo $rec->id; ?>"><i class="fa fa-trash-o"></i></a>
			</td>
		</tr>
		<?php
	}
} else {
	?>
	<tr>
		<td colspan="4"><span class="text-left pull-left">Shared with no one</span></td>
	</tr>
	<?php
}
?>