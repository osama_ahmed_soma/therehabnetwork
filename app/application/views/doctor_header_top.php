<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<header class="site-header app-design">
    <div class="container-fluid">
        <div class="row">
            <div class=" top-logo-section">
                <a href="<?php echo base_url(); ?>" class="top-logo">
                    <img class="hidden-md-down" src="<?php echo base_url(); ?>template_file/img/logo.png" alt="">
                    <img class="hidden-lg-up" src="<?php echo base_url(); ?>template_file/img/logo.png" alt="">
                </a>
                <button class="hamburger hamburger--htla">
                    <span>toggle menu</span>
                </button>
            </div>
            <div class="top-menu-left-head">
                <div class="site-header-content-in">
                    <div class="site-header-shown">
						<div id="time_note" class="dropdown dropdown-notification" style="display: none; margin-right: 10px; margin-top: 6px;" data-toggle="tooltip" data-placement="bottom" title=""><i class="fa fa-clock-o" aria-hidden="true" style="color: #fff !important;"></i> Video Consultation Starts in <span></span> minutes!</div>
                        <div class="dropdown dropdown-notification schedule-btn">
                            <a class="btn btn-danger pending_appoint_for_doctor"><i class="fa fa-clock-o"></i> Schedule Appointment</a>
                            <a class="btn btn-success" id="invite_patients"><i class="fa fa-users"></i> Invite Patients</a>
                        </div>
                        <div class="dropdown dropdown-notification notif">
                            <a href="#"
                               class="header-alarm dropdown-toggle active"

                               data-toggle="dropdown"
                               aria-haspopup="true"
                               aria-expanded="false">
                                <i class="fa fa-bell"></i>
                                <?php
                                $unopened_notification_count = getUnreadNotificationCount();
                                if ( $unopened_notification_count > 0 ) {
                                    echo '&nbsp;<span class="label label-pill label-danger c_notification">' . $unopened_notification_count .'</span>';
                                }
                                ?>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-notif"
                                 aria-labelledby="dd-notification">
                                  <button type="button" class="close close_btn_after_read_all" data-dismiss="modal" aria-label=""><span>×</span></button>
                                <div class="dropdown-menu-notif-header">
                                    Notifications
                                    
                                </div>
                                <div class="dropdown-menu-notif-list">
                                    <?php
                                    $unread_notification = getUnreadNotitication();
                                    if ( is_array($unread_notification) && $unopened_notification_count > 0 ) {
                                        foreach ( $unread_notification as $notification ) { ?>
                                            <div class="dropdown-menu-notif-item">
                                                <div class="photo">
                                                    <img src="<?php echo base_url() . $notification->image_url; ?>" alt="">
                                                </div>
                                                <div class="dot"></div>
                                                <?php echo $notification->message; ?>
                                                <div class="color-blue-grey-lighter">
                                                    <?php echo date('m/d/Y h:i a', $notification->added); ?>
                                                </div>
                                            </div>
                                            <?php

                                        }
                                    } else { ?>
                                        <div class="dropdown-menu-notif-item text-center clearfix">
                                            <!--<div class="dot"></div>-->
                                            No unread notification found.
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="dropdown-menu-notif-more clearfix">
                                	<?php
                                    if ( $unopened_notification_count > 0 ) {
                                        echo '&nbsp;<span class="label label-pill label-danger c_notification">' . $unopened_notification_count .'</span>';
                                        echo '<a href="#" id="dd-notification" class="pull-left">Mark as read</a>';
                                    }
                                    ?>
                                
                                    <a href="<?php echo base_url() . 'notification'; ?>" class="pull-right">View more</a>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown dropdown-notification messages">
                            <a href="<?php echo base_url() . 'messages'; ?>"
                               class="header-alarm dropdown-toggle active"
                               id="dd-messages"
                               data-toggle="dropdown"
                               aria-haspopup="true"
                               aria-expanded="false">
                                <i class="fa fa-envelope"></i>
                                <?php
                                $unopened_message_count = getUnopenedMessageCount();
                                if ( $unopened_message_count > 0 ) {
                                    echo '&nbsp;<span class="label label-pill label-danger">' . $unopened_message_count .'</span>';
                                }
                                ?>
                            </a>
                            <?php /*
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-messages"
                             aria-labelledby="dd-messages">
                            <div class="dropdown-menu-messages-header">
                                <ul class="nav" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active"
                                           data-toggle="tab"
                                           href="#tab-incoming"
                                           role="tab">
                                            Inbox
                                            <?php
                                            $unopened_message_count = getUnopenedMessageCount();
                                            if ( $unopened_message_count > 0 ) {
                                                echo '&nbsp;<span class="label label-pill label-danger pull-right">' . $unopened_message_count .'</span>';
                                            }
                                            ?>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link"
                                           data-toggle="tab"
                                           href="#tab-outgoing"
                                           role="tab">Outbox</a>
                                    </li>
                                </ul>
                                <!--<button type="button" class="create">
                                    <i class="font-icon font-icon-pen-square"></i>
                                </button>-->
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-incoming" role="tabpanel">
                                    <div class="dropdown-menu-messages-list">
                                        <a href="#" class="mess-item">
                                            <span class="avatar-preview avatar-preview-32"><img
                                                    src="<?php echo base_url(); ?>template_file/img/photo-64-2.jpg"
                                                    alt=""></span>
                                            <span class="mess-item-name">Tim Collins</span>
                                            <span class="mess-item-txt">Morgan was bothering about something!</span>
                                        </a>
                                        <a href="#" class="mess-item">
                                            <span class="avatar-preview avatar-preview-32"><img
                                                    src="<?php echo base_url(); ?>template_file/img/avatar-2-64.png"
                                                    alt=""></span>
                                            <span class="mess-item-name">Christian Burton</span>
                                            <span class="mess-item-txt">Morgan was bothering about something! Morgan was bothering about something.</span>
                                        </a>
                                        <a href="#" class="mess-item">
                                            <span class="avatar-preview avatar-preview-32"><img
                                                    src="<?php echo base_url(); ?>template_file/img/photo-64-2.jpg"
                                                    alt=""></span>
                                            <span class="mess-item-name">Tim Collins</span>
                                            <span class="mess-item-txt">Morgan was bothering about something!</span>
                                        </a>
                                        <a href="#" class="mess-item">
                                            <span class="avatar-preview avatar-preview-32"><img
                                                    src="<?php echo base_url(); ?>template_file/img/avatar-2-64.png"
                                                    alt=""></span>
                                            <span class="mess-item-name">Christian Burton</span>
                                            <span class="mess-item-txt">Morgan was bothering about something...</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-outgoing" role="tabpanel">
                                    <div class="dropdown-menu-messages-list">
                                        <a href="#" class="mess-item">
                                            <span class="avatar-preview avatar-preview-32"><img
                                                    src="<?php echo base_url(); ?>template_file/img/avatar-2-64.png"
                                                    alt=""></span>
                                            <span class="mess-item-name">Christian Burton</span>
                                            <span class="mess-item-txt">Morgan was bothering about something! Morgan was bothering about something...</span>
                                        </a>
                                        <a href="#" class="mess-item">
                                            <span class="avatar-preview avatar-preview-32"><img
                                                    src="<?php echo base_url(); ?>template_file/img/photo-64-2.jpg"
                                                    alt=""></span>
                                            <span class="mess-item-name">Tim Collins</span>
                                            <span class="mess-item-txt">Morgan was bothering about something! Morgan was bothering about something.</span>
                                        </a>
                                        <a href="#" class="mess-item">
                                            <span class="avatar-preview avatar-preview-32"><img
                                                    src="<?php echo base_url(); ?>template_file/img/avatar-2-64.png"
                                                    alt=""></span>
                                            <span class="mess-item-name">Christian Burtons</span>
                                            <span class="mess-item-txt">Morgan was bothering about something!</span>
                                        </a>
                                        <a href="#" class="mess-item">
                                            <span class="avatar-preview avatar-preview-32"><img
                                                    src="<?php echo base_url(); ?>template_file/img/photo-64-2.jpg"
                                                    alt=""></span>
                                            <span class="mess-item-name">Tim Collins</span>
                                            <span class="mess-item-txt">Morgan was bothering about something!</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-menu-notif-more">
                                <a href="#">See more</a>
                            </div>
                        </div>
                        */ ?>
                        </div>

                        <div class="dropdown user-menu">
                            <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                <?php if (count($this->session->userdata('image')) > 0) {
                                    ?>
                                    <img id="header_profile_image_icon" src="<?php echo default_image($this->session->userdata('image')); ?>" alt="">
                                    <?php
                                } else {
                                    ?>
                                    <img id="header_profile_image_icon" src="<?php echo base_url(); ?>template_file/img/avatar-2-64.png" alt="">
                                    <?php
                                } ?>
                                <span class="header_user_fname">
                                <?php
                                echo explode(' ', trim($this->session->userdata('full_name')))[0];
                                ?>
                                    </span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                                <a class="dropdown-item" href="<?php echo base_url(); ?>doctor/profile"><span
                                        class="font-icon glyphicon glyphicon-user"></span>Profile</a>
                                <a class="dropdown-item" href="<?php echo site_url('/doctor/change_password'); ?>"><span
                                        class="font-icon glyphicon glyphicon-cog"></span>Settings</a>
                                <a class="dropdown-item" href="mailto:support@myvirtualdoctor.com"><span
                                        class="font-icon glyphicon glyphicon-question-sign"></span>Support Center</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?php echo site_url('/login/logout'); ?>"><span
                                        class="font-icon glyphicon glyphicon-question-sign"></span>Logout</a>
                            </div>
                        </div>

                        <button type="button" class="burger-right">
                            <i class="font-icon-menu-addl"></i>
                        </button>
                    </div><!--.site-header-shown-->

                </div><!--site-header-content-in-->
            </div><!--.site-header-content-->
        </div>
    </div><!--.container-fluid-->
</header><!--.site-header-->