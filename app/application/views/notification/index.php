<div class="page-content">

    <!-- Top Compose and search bar -->
    <div class="container-fluid">
        <section class="navi-top clearfix">
            <div class="navi-heading">
                <h2>Notifications</h2>
                <a href="#" id="dd-notification" class="btn btn-primary pull-right" >Mark all as read</a>
            </div>
        </section>
    </div>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-1">

            <section class="message_section">
                <div class="ms_layout_settings clearfix">
                    <?php
                    if ( isset($notifications) && !empty($notifications) ):
                        foreach ( $notifications as $notification) { ?>
                            <div class="ms_msg<?php if ( $notification->view == 0 ) { echo ' odd '; } ?> clearfix">
                                <div class="ms_msg_img">
<!--                                    <a href="#"><img src="--><?php //echo base_url() . $notification->image_url; ?><!--" alt=""></a>-->
                                    <a href="#"><img src="<?php echo default_image($notification->image_url); ?>" alt=""></a>
                                </div>
                                <div class="ms_msg_name">
                                    <a href="#"><?php echo $notification->firstname . ' ' . $notification->lastname; ?></a>
                                </div>
                                <div style="width:48%; float: left; line-height: 24px; margin-right: 5px;">
                                    <span><?php echo $this->security->xss_clean($notification->message); ?></span>
                                </div>
                                <div style="width:20%; float: left; line-height: 24px;"><?php echo date('m/d/Y h:i a ', $notification->added); ?></div>
                                <div style="width:5%; float: left; line-height: 24px;">
                                    <!--<a class="mark_as_read pull-right" data-id="<?php /*echo $notification->id; */?>" href="#">
                                        <i aria-hidden="true" class="fa fa-thumbs-o-up"></i>
                                    </a>-->
                                    <a href="#" class="notification_delete" data-id="<?php echo $notification->id; ?>" title="Delete Notification">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        <?php }

                    else: ?>
                        <div class="ms_msg clearfix">No Notification Found.</div>
                    <?php endif; ?>
                    <?php if( isset($pagination_link) ):
                        echo $pagination_link;
                    endif;
                    ?>
                </div>
            </section>
        </div><!--.tab-pane-->
    </div>
</div>

<script>
    jQuery(function($) {
        $(document.body).on('click', '.mark_as_read', function (e) {
            e.preventDefault();
            var th = $(this);
            $.ajax({
                method: "POST",
                url: "<?php echo base_url(); ?>notification/readNotification/" + th.data('id'),
                data: {},
                dataType: "html",
                //async: true,
            }).success(function (data, status) {
                location.reload();
            }).done(function () {

            });
        });

        $(document.body).on('click', '.notification_delete', function (e) {
            e.preventDefault();
            var th = $(this);
            $.ajax({
                method: "POST",
                url: "<?php echo base_url(); ?>notification/deleteNotification/" + th.data('id'),
                data: {},
                dataType: "html",
                //async: true,
            }).success(function (data, status) {
                location.reload();
            }).done(function () {

            });
        });

    });
</script>
