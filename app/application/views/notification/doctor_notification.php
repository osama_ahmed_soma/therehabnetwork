<script type="text/javascript">
    var interval;
    var found_appointment = false;
    var aid = false;
    var appointment_info = false;
    var is_delayed = false;
    var is_action = false;
    var modal_shown = false;
    var redirect = false;
    var duration = minutes = seconds = timer = 0;
    function startTimer(duration, display, url = false) {
        var timer = duration, minutes, seconds;
        interval = setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.text(minutes + ":" + seconds);

            if (minutes == 0 && seconds == 0) {
                clearInterval(interval);
                if(!is_action){
                    $('#cancel_submit_btn').triggerHandler('click');
                }
            }

            if (minutes == 0 && seconds == 0 && redirect == true && url !== false) {
                $('#time_note').css('background-color', '#46c35f');
                $('#time_note').html('<i class="fa fa-clock-o" aria-hidden="true" style="color: #fff !important;"></i><a href="<?php echo base_url(); ?>appointment/start?aid='+aid+'" style="color: #fff !important;">Your consultation is now ready. Click here to start.</a>');
//                window.location = url;
            }

            if (--timer < 0) {
                timer = 0;
            }
        }, 1000);
    }
    function poll() {
        setTimeout(function () {
            show_loader = false;
            if (found_appointment === false) {
                $.ajax({
                    url: "<?php echo base_url($this->session->userdata('type') . '/upcoming_appointment'); ?>",
                    success: function (data) {
                        if (!$.isEmptyObject(data.appointment_info)) {
                            aid = data.appointment_info.id;
                            appointment_info = data.appointment_info;
                            is_delayed = data.is_delayed;
                            is_action = data.is_action;
                            found_appointment = true;
                            if (is_action) {
                                $('#appointment_Modal').modal('hide');
                                $('#time_note').show(200);
                                redirect = true;
                            } else {
                                $('#appointment_Modal').modal('show');
                            }
                            $('#minutes_in_modal').text(Math.floor(data.diff / 60));
                            var display = $('#time_note span, #time_redirect_counter');
                            $('#time_note').attr('title', data.appointment_info.hover_text);
                            startTimer(data.diff, display, '<?php echo base_url(); ?>appointment/start?aid=' + aid);
                            return;
                        }
                        poll();
                    }, dataType: "json"
                });
            }
        }, 5000);
    }
    jQuery(function ($) {
        poll();
        $('#accept_btn').click(function (e) {
            e.preventDefault();
            $('#appointment_Modal').modal('hide');
            $('#time_note').show(200);
            redirect = true;
            is_action = true;
            $.ajax({
                method: "POST",
                url: "<?php echo base_url(); ?>doctor/appointment_event_trigger",
                data: {
                    id: aid,
                    action_type: 'accepted',
                    reason: '',
                    stamp: ''
                },
                dataType: "json",
            }).success(function (data, status) {
                if (data.status == 'success') {

                }
            });
        });

        $('#cancel_btn').click(function (e) {
            e.preventDefault();
            $('#reason_for_cancel').show(200);
            $('#reason_for_delay').hide(200);
        });

        $('#wait15min_btn').click(function (e) {
            e.preventDefault();
            $('#reason_for_delay').show(200);
            $('#reason_for_cancel').hide(200);
        });

        $('#cancel_submit_btn').click(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "<?php echo base_url(); ?>doctor/appointment_event_trigger",
                data: {
                    id: aid,
                    action_type: 'cancelled',
                    reason: $('#reason_for_cancel textarea[name=reason_for_cancel]').val(),
                    stamp: ''
                },
                dataType: "json",
            }).success(function (data, status) {
                if (data.status == 'success') {
                    $('#cancel_btn, #accept_btn, #wait15min_btn, #reason_for_cancel').hide(200);
                    $('#appointment_Modal .modal-body p').html(data.msg);
                    $('#appointment_Modal .modal-title').text('Cancelled');
                    clearInterval(interval);
                    found_appointment = false;
                    redirect = false;
                    setTimeout(function () {
                        $('#appointment_Modal').modal('hide');
                        $('#reason_for_cancel textarea[name=reason_for_cancel]').val('');
                        poll();
                    }, 3000);
                }
            });
        });

        $('#wait15min_submit_btn').click(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "<?php echo base_url(); ?>doctor/appointment_event_trigger",
                data: {
                    id: aid,
                    action_type: 'delayed',
                    reason: $('#reason_for_delay textarea[name=reason_for_delay]').val(),
                    stamp: appointment_info.start_date_stamp
                },
                dataType: "json",
            }).success(function (data, status) {
                if (data.status == 'success') {
                    $('#cancel_btn, #accept_btn, #wait15min_btn, #reason_for_cancel, #reason_for_delay').hide(200);
                    $('#appointment_Modal .modal-body p').html(data.msg);
                    $('#appointment_Modal .modal-title').text('Delayed');
                    clearInterval(interval);
                    found_appointment = false;
                    redirect = false;
                    setTimeout(function () {
                        $('#appointment_Modal').modal('hide');
                        $('#reason_for_delay textarea[name=reason_for_delay]').val('');
                        poll();
                    }, 3000);
                }
            });
        });

        $('#appointment_Modal').on('hidden.bs.modal', function () {
            if (redirect == false && found_appointment == true) {
                clearInterval(interval);
                found_appointment = false;
                poll();
            }
        })
    });


</script>
<div class="modal fade bs-example-modal-sm" id="appointment_Modal" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Ready?</h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-md-12">
                    <div class="row">
                        <div class="form-group text-center">
                            <p> Next consultation starts in <span id="minutes_in_modal">5</span> minutes. </p>
                            <div id="time_redirect_counter" style="display: none;"></div>
                        </div>
                    </div>
                    <div class="row" id="reason_for_cancel" style="display: none;">
                        <div class="form-group">
                            <textarea class="form-control" name="reason_for_cancel"
                                      placeholder="Enter reason of cancel (Optional)"></textarea>
                            <button type="button" class="btn btn-default" id="cancel_submit_btn">Submit</button>
                        </div>
                    </div>
                    <div class="row" id="reason_for_delay" style="display: none;">
                        <div class="form-group">
                            <textarea class="form-control" name="reason_for_delay"
                                      placeholder="Enter reason for delay (Optional)"></textarea>
                            <button type="button" class="btn btn-default" id="wait15min_submit_btn">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer ">
                <div class="appointment_Modal_btn">
                    <button type="button" class="btn btn-primary" id="wait15min_btn">Wait 15 minutes</button>
                    <button type="button" class="btn btn-success " id="accept_btn">Accept</button>
                    <button type="button" class="btn btn-danger " id="cancel_btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>