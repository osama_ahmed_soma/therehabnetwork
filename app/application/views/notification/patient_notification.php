<script type="text/javascript">
    var found_appointment = false;
    var aid = false;
    var appointment_info = false;
    var is_delayed = false;
    var is_action = false;
    var show_loader = false;
    var modal_shown = false;
    var is_cancelled = false;
    function check_is_doctor_ready(redirect) {
        show_loader = false;
        setTimeout(function () {
            $.ajax({
                method: "POST",
                url: "<?php echo base_url($this->session->userdata('type') . '/is_doctor_ready_for_consultation'); ?>/" + aid,
                data: {},
                dataType: "json",
            }).success(function (data, status) {
                if (data.bool) {
                    window.location = redirect;
                } else {
                    check_is_doctor_ready(redirect);
                }
            }).done(function () {

            });
        }, 1500);
    }
    function startTimer(duration, display, redirect = false) {
        var timer = duration, minutes, seconds;
        interval = setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.text(minutes + ":" + seconds);

            if (minutes == 0 && seconds <= 30) {
                if (modal_shown == false) {
                    clearInterval(interval);
                    modal_shown = true;
                    $('#appointment_redirect_Modal').modal('show');
                    check_is_doctor_ready(redirect);
                    clearInterval(interval);
                }
            }
            if (minutes == 0 && seconds == 0) {
                clearInterval(interval);
            }

            if (--timer < 0) {
                timer = 0;
            }
        }, 1000);
    }
    function check_appoinment_status(duration) {
        setTimeout(function () {
            show_loader = false;
            $.ajax({
                method: "POST",
                url: "<?php echo base_url($this->session->userdata('type') . '/check_appointment_log_status'); ?>",
                data: {
                    id: aid,
                    stamp: appointment_info.start_date_stamp
                },
                success: function (data) {
                    if (data.status == 'nothing') {
                        console.log('nothing, check status again');
                        check_appoinment_status();
                    } else {
                        clearInterval(interval);
                        if (data.status === 'cancelled') {
                            $('#time_note').html(data.msg).attr('title', data.reason).show();
                            $('#appointment_redirect_Modal').modal('hide');
                            found_appointment = false;
                            aid = false;
                            appointment_info = false;
                            is_delayed = false;
                            is_action = false;
                            show_loader = false;
                            modal_shown = false;
                            is_cancelled = true;
                            poll();
                        } else if (data.status === 'delayed') {
                            $('#appointment_redirect_Modal').modal('hide');
                            $('#time_note').html('<i class="fa fa-clock-o" aria-hidden="true" style="color: #fff !important;"></i> Video Consultation Starts in <span></span> minutes! <yo style="color: #bd1e60;">Consultation has been delayed 15 minutes by doctor.</yo>').attr('title', data.reason).show(200);
                            display = $('#time_note span, #time_redirect_counter');
                            startTimer(data.diff, display, '<?php echo base_url(); ?>appointment/start?aid=' + aid);
                        }
                    }

                },
                dataType: "json"
            });
        }, 3000);
    }
    function poll() {
        setTimeout(function () {
            show_loader = false;
            if (found_appointment === false) {
                $.ajax({
                    url: "<?php echo base_url($this->session->userdata('type') . '/upcoming_appointment'); ?>",
                    success: function (data) {
                        if (!$.isEmptyObject(data.appointment_info)) {
                            aid = data.appointment_info.id;
                            appointment_info = data.appointment_info;
                            is_delayed = data.is_delayed;
                            is_action = data.is_action;
                            found_appointment = true;
                            $('#time_note').html('<i class="fa fa-clock-o" aria-hidden="true" style="color: #fff !important;"></i> Video Consultation Starts in <span></span> minutes!').attr('title', data.appointment_info.hover_text).show(200);
                            var display = $('#time_note span, #time_redirect_counter');
                            if (!is_action) {
                                check_appoinment_status(data.diff);
                            }
                            startTimer(data.diff, display, '<?php echo base_url(); ?>appointment/start?aid=' + aid);
                            return;
                        }
                        if (data.knocking_data.is_doctor_in_video_page) {
                            if(data.knocking_data.is_consultation_start){
                                aid = data.knocking_data.appointment_id;
                                $.ajax({
                                    method: "GET",
                                    url: "<?php echo base_url($this->session->userdata('type') . '/update_appointment_timing'); ?>/" + aid,
                                    data: {},
//                                    dataType: "json",
                                }).success(function (data, status) {
                                    check_is_doctor_ready('<?php echo base_url(); ?>appointment/start?aid=' + aid);
                                }).done(function () {

                                });
                                return;
                            }
                            if(!isPatientApprovedKnocking){
                                $('#time_note').addClass('knocking_notification').html('Your doctor is already in consultation room. Do you want to knock and start the consultation now? <a class="btn btn-success knocking_patient_approved" href="" data-knocking_id="' + data.knocking_data.knocking_id + '" data-appointment_id="' + data.knocking_data.appointment_id + '">Yes</a>').attr('title', '').show(200);
                            }
                        } else {
                            if ($('#time_note').hasClass('knocking_notification')) {
                                $('#time_note').removeClass('knocking_notification').html('').hide(200);
                            }
                        }
                        poll();
                    },
                    dataType: "json"
                });
            }
        }, 5000);
    }
    jQuery(function ($) {
        poll();
    });
</script>
<div class="modal fade bs-example-modal-ls" id="appointment_redirect_Modal" tabindex="-1" role="dialog"
     data-backdrop="static" data-keyboard="false" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-ls" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <img src="<?php echo base_url(); ?>template_file/img/animated-Timer.gif"
                     style="margin: 0px auto; width: 100px; height: 100px; border-radius: 500px; border: 4px solid #CCC;"><br/>
                <h4 style="margin-bottom: 0px;">Please Wait.</h4>
                <span class="text-muted">Your consultation will begin momentarily.</span>
            </div>
        </div>
    </div>
</div>