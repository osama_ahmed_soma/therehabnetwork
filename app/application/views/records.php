<style>
	#documents_table td span.title {
		cursor: pointer;
	}
	#documents_table td span.title:hover {
		text-decoration: underline;
	}
</style>
<div class="page-content">
    <div class="container-fluid">
		<section class="navi-top clearfix">
			<div class="navi-heading">
				<?php // if ($this->session->userdata('type') == 'doctor') { ?>
				<h2>My Files</h2>
				<?php // } else { ?>
				<!--<h2>My Secured Files</h2>-->
				<?php // } ?>
            </div>
			<div class="search pull-right">
				<?php echo form_open($this->uri->segment(1) .'/records', array('class' => 'form-inline pull-xs-right', 'method' => 'GET')); ?>
                <input type="text" class="form-control" name="search" value="<?php echo set_value('search', isset($search_text) ? $search_text : ''); ?>" />
                <button class="btn btn-outline-success" type="submit">Search</button>
                <a class="btn btn" href="<?php echo base_url($this->uri->segment(1) . '/records'); ?>">Reset</a>
				<?php echo form_close(); ?>
			</div>
        </section>
		<section class="upcoming-sessions">
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active">
					<div class="table-responsive">
						<table id="documents_table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th><i class="fa fa-check" aria-hidden="true"></i> File Name</th>
									<!--<th><i class="fa fa-file" aria-hidden="true"></i> Filename</th>-->
									<th><i class="fa fa-server" aria-hidden="true"></i> Size</th>
									<th><i class="fa fa-upload" aria-hidden="true"></i> Upload Date</th>
									<th class="text-center"><i class="fa fa-thumb-tack" aria-hidden="true"></i> Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if (is_array($documents) && count($documents) > 0) {
									foreach ($documents as $file) {
										$type = ($file->filetype) ? $file->filetype : pathinfo($file->filename, PATHINFO_EXTENSION);
										$icons = $this->config->item('file_icons');
										?>
										<tr data-to-delete="<?php echo $file->id; ?>">
											<td><i class="<?php echo (isset($icons[$type])) ? $icons[$type] : $icons['default']; ?>"></i> <span class="title"><?php echo $file->title; ?></span></td>
											<!--<td><?php echo $file->filename; ?></td>-->
											<td><?php echo humanFileSize($file->filesize); ?></td>
											<td class="text-left"><?php echo date('m/d/Y', strtotime($file->date_time)); ?></td>
											<td class="text-center">
												<a title="Download" class="btn btn-primary" target="_blank" href="<?php echo base_url('file/download_document/' . $file->id); ?>"><span><i class="fa fa-download"></i></span></a>
												<button data-file_id="<?php echo $file->id ?>" class="btn btn-success open_edit" title="Rename" ><span><i class="fa fa-edit"></i></span></button>
												<a title="Share" class="btn btn-default share_modal_btn" data-title="<?php echo $file->title; ?>" file_id="<?php echo $file->id; ?>" href="#" ><span><i class="fa fa-share"></i></span></a>
												<a title="Delete" href="#" class="delete_file_link btn btn-danger" data-file_id="<?php echo $file->id ?>"><span><i class="fa fa-trash"></i></span></a>
											</td>
										</tr>
										<?php
									}
								} else {
								?>
										<tr class="no_docs">
											<td colspan="4"><span class="text-left pull-left">No documents found</span></td>
										</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php
						if(isset($pagination_link)) {
							echo $pagination_link;
						}
						?>
					</div>
				</div>
			</div>
		</section>
		<section class="upcoming-sessions">
			<div class="navi-heading">
                <h2><i class="fa fa-paper-plane" aria-hidden="true"></i> Upload New Document</h2>
            </div>
            <div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="scheduled">
					<div class="condition-form text-left">
						<span id="files"></span>
						<div class="col-md-12">
							<?php
							echo form_open_multipart('upload/document', array('class' => 'dropzone', 'id' => 'my-awesome-dropzone'));
							?>
							<div class="fallback">
								<input name="file" type="file" multiple />
							</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		</section>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" id="share_with_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
				<h4><i class="fa fa-share"></i> Sharing "<em></em>"</h4>
			</div>
			<div class="modal-body">
				<section class="upcoming-sessions">
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active">
							<div class="table-responsive">
								<table class="table" id="shared_with_table">
									<thead>
										<tr class="table-row tbl-header">
											<!--<th class="table-cell" >Document Title</th>-->
											<th class="table-cell" >Shared with</th>
											<th class="table-cell" >Date Time</th>
											<th class="table-cell" >Delete</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</section>
				<hr>
				<div class="row">
					<div class="col-md-3">
						Share with <?php
						if ($this->session->userdata('type') == 'doctor') {
							echo 'patient: ';
						} else {
							echo 'doctor: ';
						}
						?>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<select name="shared_to_id" id="shared_to_id" class="form-control select2" >
								<?php
								if (isset($recipient_list) && is_array($recipient_list) && !empty($recipient_list)) {
									foreach ($recipient_list as $recipient) {
										echo "<option value='{$recipient->id}'" . set_select('recipients', $recipient->id) . ">{$recipient->firstname} {$recipient->lastname}</option>";
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<button type="button" id="share_btn" class="btn btn-info btn-block pull-right">Share</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>template_file/js/dropzone.js"></script>
<script>
	Dropzone.autoDiscover = false;
	//	errors = null;
	$(document).ready(function () {

		var myDropzone = new Dropzone($("#my-awesome-dropzone").get(0), {
			maxFilesize: "10",
			addRemoveLinks: true
		});

		myDropzone.on("queuecomplete", function (file) {
//			location.reload();
			
			myDropzone.removeAllFiles();
			
			//			if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
			//		        alert('Your action, Refresh your page here. ');
			//			}
			//			alert('queuecomplete: ' + errors);
			//			if (errors === null) {
			//				alert('reload');
			//			} else {
			//				$('.errors').html(errors);
			//			}
		});
		
		myDropzone.on("error", function(file, message) {
//			console.log(file);
			console.log(message);
//			if (message !== '') {
//				alert(message)
//				errors = message;
//			}
		});
		files_to_be_removed = [];
		myDropzone.on("complete", function(file,xhr,formData) {
//			console.log(file)
//			console.log(xhr)
//			console.log(formData)
		});
		
		myDropzone.on("sending", function(file,xhr,formData) {
			<?php if (config_item('csrf_protection')) { ?>
			$('input[name=<?php echo $this->security->get_csrf_token_name(); ?>]').val(csrf_hash);
			<?php } ?>
		});

		myDropzone.on("success", function (file, xhr) {
			error = null;
//			console.log('success');
//			console.log(file);
			
			xhr = JSON.parse(xhr);
//			console.log(xhr);

//			files_to_be_removed.push(file);

			<?php if (config_item('csrf_protection')) { ?>
			csrf_hash = xhr.csrf_hash;
			<?php } ?>
			
			$('#documents_table tbody .no_docs').remove();
			$('#documents_table tbody').append('<tr data-to-delete="'+xhr.file_info.id+'">\n\
								<td><i class="'+xhr.file_info.icon_class+'"></i> <div class="input-group">\n\
	  <input type="text" required name="title" placeholder="Untitled File" class="form-control title_input_'+xhr.file_info.id+'" value="" />\n\
      <span class="input-group-btn">\n\
        <button class="btn btn-default update_btn_'+xhr.file_info.id+'" type="button">Update</button>\n\
      </span>\n\
    </div></td>\n\
								<!--<td>'+xhr.file_info.filename+'</td>-->\n\
								<td>'+xhr.file_info.filesize+'</td>\n\
								<td class="text-left">'+xhr.file_info.date_time+'</td>\n\
								<td class="text-center">\n\
									<a title="Download" class="btn btn-primary" target="_blank" href="<?php echo base_url('file/download_document/'); ?>/'+xhr.file_info.id+'"><span><i class="fa fa-download"></i></span></a>\n\
									<button data-file_id="'+xhr.file_info.id+'" class="btn btn-success open_edit" disabled="disabled" title="Rename" ><span><i class="fa fa-edit"></i></span></button>\n\
									<a title="Share" class="btn btn-default share_modal_btn" data-title="'+xhr.file_info.title+'" file_id="'+xhr.file_info.id+'" href="#"><span><i class="fa fa-share"></i></span></a>\n\
									<a title="Delete" href="#" class="delete_file_link btn btn-danger" data-file_id="'+xhr.file_info.id+'"><span><i class="fa fa-trash"></i></span></a>\n\
								</td>\n\
							</tr>');
//			$('#documents_table tbody tr > td:last-child').trigger('dblclick');

			$(document).on('click', '.update_btn_'+xhr.file_info.id, function(e) {
				e.stopPropagation();
//				alert('newly hellow');
//				console.log($(this).parents('tr').html());
//				console.log($(this).parents('td').html());
				currentEle = $(this).parents('td');
				title_input = currentEle.find('.title_input_'+xhr.file_info.id);
				value = title_input.val();
				id = currentEle.parents('tr').attr('data-to-delete');
//				alert(id)
				$.ajax({
					type: "POST",
					url: "<?php echo base_url('file/update/'); ?>"+ id,
					dataType: "json",
					data: { title: value },
					success: function (data) {
						if (data.status == 'success') {
							editing = false;
							$(currentEle).find('.input-group, .title').remove();
							$(currentEle).html(currentEle.html() + '<span class="title">'+data.file_info.title+'</span>');
//							$('.open_edit').attr('disabled', false);
							currentEle.parents('tr').find('td:last-child').find('.open_edit').attr('disabled', false);
							
//							remove_from_uploader(id, myDropzone);
							notySuccess(data.msg)
						} else {
							notyError(data.msg)
						}
					}
				});
			});
			
			$(".title_input_"+xhr.file_info.id).keyup(function (event) {
				if (event.keyCode == 13) {
					
					currentEle = $(this).parents('td');
					title_input = $(this);
					value = title_input.val();
					id = currentEle.parents('tr').attr('data-to-delete');
//					alert(id)
//					alert(value)
					$.ajax({
						type: "POST",
						url: "<?php echo base_url('file/update/'); ?>"+ id,
						dataType: "json",
						data: { title: value },
						success: function (data) {
							if (data.status == 'success') {
								editing = false;
								$(currentEle).find('.input-group').remove();
								$(currentEle).html(currentEle.html() + ' <span class="title">'+data.file_info.title+'</span>');
//								$('.open_edit').attr('disabled', false);
								currentEle.parents('tr').find('td:last-child').find('.open_edit').attr('disabled', false);
								
//								remove_from_uploader(id, myDropzone);
								notySuccess(data.msg)
							} else {
								notyError(data.msg)
							}
						}
					});
				}
			});

		});

		myDropzone.on("removedfile", function (file) {
//			console.log('removedfile');
//			console.log(file);
		});

//		myDropzone.on("complete", function(file) {
//			console.log('complete');
////			myDropzone.removeFile(file);
//			console.log(file);
//		});

		file_id = '';
		$(document).on("click", '.share_modal_btn', function (e) {
			e.preventDefault();
			file_id = $(this).attr('file_id');
			title = $(this).attr('data-title');
			$('#share_with_modal').modal('show');
			$('#share_with_modal .modal-header > h4 > em').html(title);
			$.ajax({
				type: "GET",
				url: "<?php echo base_url('file/shared_with/'); ?>"+ file_id,
				dataType: "html",
				success: function (response) {
					$('#shared_with_table tbody').html(response);
				}

			});
		});

		$("#share_btn").click(function (e) {
			e.preventDefault();
			var shared_to_id = $("#shared_to_id").val();
			if (shared_to_id === '') {
				notyError("Select person from dropdown");
			} else {
				$.ajax({
					type: "GET",
					url: "<?php echo base_url('file/share/'); ?>"+ shared_to_id + '/' + file_id,
					dataType: "json",
					success: function (data) {
						notySuccess(data.msg)
//						html = '';
//						html += '<tr class="table-row no-records no-records-delete" data-to-delete="99">\n\
//			<td class="table-cell"></td>\n\
//			<td class="table-cell">John smith</td>\n\
//			<td class="table-cell">2016-09-02 02:50:04</td>\n\
//			<td class="table-cell">\n\
//				<a class="deleteMessage" data-id="99" data-rowid="99"><i class="fa fa-trash-o"></i></a>\n\
//			</td>\n\
//		</tr>';
//						$("#shared_with_table tbody").append(html);

						$.ajax({
							type: "GET",
							url: "<?php echo base_url('file/shared_with/'); ?>"+ file_id,
							dataType: "html",
							success: function (response) {
								$('#shared_with_table tbody').html(response);
							}

						});

					}
				});
			}
		});

		$(document).on("click", '.delete_shared', function (e) {
			e.preventDefault();
			var item_id = $(this).data('rowid');
			var msg = 'Are you sure that you want to delete this share?';
			notyConfirmWithParam(msg, function moveMessageToTrash(obj) {
				console.log(obj);
				//ajax
				$('tr[data-to-delete=' + obj.itemID + ']').remove();
				$.ajax({
					method: "GET",
					url: '<?php echo base_url(); ?>file/delete_shared/' + obj.itemID,
					dataType: "json",
					success: function (data) {
						if (data.status === 'success') {
							notySuccess(data.msg);
						} else {
							notyError(data.msg);
						}
					}
				});
			}, {
				itemID: item_id,
			});

		});

		$(document).on("click", '.delete_file_link', function (e) {
			e.preventDefault();
			var item_id = $(this).attr('data-file_id');
			var msg = 'Are you sure that you want to delete?';
			notyConfirmWithParam(msg, function moveMessageToTrash(obj) {
				console.log(obj);
				//ajax
				$.ajax({
					method: "GET",
					url: '<?php echo base_url(); ?>file/delete/' + obj.itemID,
					dataType: "json",
					success: function (data) {
						if (data.status === 'success') {
							$('tr[data-to-delete=' + obj.itemID + ']').remove();
							notySuccess(data.msg);
						} else {
							notyError(data.msg);
						}
					}
				});
			}, {
				itemID: item_id,
			});

		});
		
		focused = false;
		editing = false;
		currentEle = null;
		last_element = null;
		$(document).on('dblclick', "#documents_table tbody tr > td:first-child", function (e) {
			e.stopPropagation();
			currentEle = $(this);
			console.log(currentEle)
			
			currentEle.parents('tr').find('td:last-child').find('.open_edit').attr('disabled', true);
			if (focused == false && editing === true) {
				notyError('Cannot edit more than one at once');
//				notyError(last_element.html());
				last_element.find('.open_edit').attr('disabled', true);
				last_element.find('input').focus();
				last_element.find('input').fadeOut(100);
				last_element.find('input').fadeIn(100);
				return false;
			}
			last_element = currentEle;
			value = $(this).find('.title').html();
//			console.log(value)
			if (focused == false) {
//				alert($(this).find('.title').length)
				editing = true;
				
				updateVal(currentEle, value);
			}
		});
		
		$(document).on('click', "span.title", function (e) {
			e.stopPropagation();
			
			if (focused == true) {
				return false;
			}
			
			console.log($(this).parents('tr').find('td:last-child').find('a:first-of-type').attr('href'));
			window.open($(this).parents('tr').find('td:last-child').find('a:first-of-type').attr('href'));
			
			
//			return true;
		});

		$(document).on('click', ".open_edit", function (e) {
			e.stopPropagation();
			currentEle = $(this).parents('tr').find('td:first-child');
			
			console.log(currentEle);
//			return false;
			
			if (focused == false && editing === true) {
				notyError('Cannot edit more than one at once');
//				notyError(last_element.html());
				last_element.find('input').focus();
				last_element.find('input').fadeOut(100);
				last_element.find('input').fadeIn(100);
				return false;
			}
			last_element = currentEle;
			value = $(this).parents('tr').find('td:first-child').find('.title').html();
//			console.log(value)
			if (focused == false) {
				$(this).attr('disabled', true);
//				alert($(this).find('.title').length)
				editing = true;
				updateVal(currentEle, value);
			}
		});

//		$(document).on('focusin', ".title_input", function (e) {
//		   e.stopPropagation();
//		   focused = true;
//		   console.log('focus in')
//		   console.log($(this).val())
//		   
////		   currentEle = $(this).parents('td');
////		   console.log(currentEle)
////		   var value = $(this).html();
////		   if (focused == false) {
////				updateVal(currentEle, value);
////			}
//		});
//		$(document).on('focusout', ".title_input", function (e) {
//		   e.stopPropagation();
//		   console.log('focus out')
//		   focused = false;
//		});
		function updateVal(currentEle, value) {
			id = currentEle.parents('tr').attr('data-to-delete');
//			console.log(value)
//			$(currentEle).find('.title').html('<input type="text" required name="title" placeholder="Enter title of the document" class="form-control title_input_'+id+'" value="' + value + '" />');
			$(currentEle).find('.title').html('<div class="input-group">\n\
<input type="text" required name="title" placeholder="Enter title of the document" class="form-control title_input_'+id+'" value="' + value + '" />\n\
      <span class="input-group-btn">\n\
        <button class="btn btn-default update_btn_'+id+'" type="button">Update</button>\n\
      </span>\n\
    </div>');
		  
//		  $("update_btn_"+id).unbind('click');

//console.log('loggging events' );
//console.log( $(".update_btn_"+id).data('events') );

			$('.update_btn_'+id).click(function(e) {
				e.stopPropagation();
//				alert('old hellow');
//				console.log($(this).parents('tr').html());
//				console.log($(this).parents('td').html());
				currentEle = $(this).parents('td');
				title_input = currentEle.find('.title_input_'+id);
				value = title_input.val();
				id = currentEle.parents('tr').attr('data-to-delete');
//				alert(id)
				$.ajax({
					type: "POST",
					url: "<?php echo base_url('file/update/'); ?>"+ id,
					dataType: "json",
					data: { title: value },
					success: function (data) {
						if (data.status == 'success') {
							editing = false;
							$(currentEle).find('.input-group, .title').remove();
							$(currentEle).html(currentEle.html() + '<span class="title">'+data.file_info.title+'</span></a>');
//							$('.open_edit').attr('disabled', false);
							currentEle.parents('tr').find('td:last-child').find('.open_edit').attr('disabled', false);
							
//							remove_from_uploader(id, myDropzone);
							notySuccess(data.msg)
						} else {
							notyError(data.msg)
						}
					}
				});
			});


			$(document).on('focusin', ".title_input_"+id, function (e) {
				e.stopPropagation();
				focused = true;
//				console.log('focus in')
//				console.log($(this).val())
			});
			$(document).on('focusout', ".title_input_"+id, function (e) {
				e.stopPropagation();
//				console.log('focus out')
				focused = false;
			});

			$(".title_input_"+id).focus();
			
//			$('#documents_table').on('keyup', ".title_input", function (event) {
			$(".title_input_"+id).keyup(function (event) {
				if (event.keyCode == 13) {
					$(currentEle).find('.title').html($.trim($(this).val()));
					$.ajax({
						type: "POST",
						url: "<?php echo base_url('file/update/'); ?>"+ id,
						dataType: "json",
						data: { title: $(currentEle).find('.title').html() },
						success: function (data) {
							if (data.status == 'success') {
								editing = false;
								$(currentEle).find('.input-group').remove();
//								$('.open_edit').attr('disabled', false);
								currentEle.parents('tr').find('td:last-child').find('.open_edit').attr('disabled', false);
								
//								remove_from_uploader(id, myDropzone);
								notySuccess(data.msg)
							} else {
								notyError(data.msg)
							}
						}
					});
				}
			});

//			$(document).click(function () { // you can use $('html')
//				console.log('danger lcik')
//				  $(currentEle).html($.trim($(".title_input").val()));
//			});
		}


	});
	function notyConfirmWithParam(message, onSuccessCallback, param) {
		var params = {
			buttons: [
				{
					addClass: 'btn btn-primary',
					text: 'Ok',
					onClick: function ($noty) {
						$noty.close();
						onSuccessCallback(param);
					}
				},
				{
					addClass: 'btn btn-danger',
					text: 'Cancel',
					onClick: function ($noty) {
						$noty.close();
					}
				}
			]
		};

		notyMessage(message, 'warning', params);
	}
	
	function remove_from_uploader(id, myDropzone) {
//		console.log(files_to_be_removed);
		
		console.log(myDropzone.files.length)
		console.log(files_to_be_removed.length)
		
		myDropzone.files.forEach(function(item, index, object) {
			xhr = JSON.parse(item.xhr.responseText);
		//    console.log(item);
		//    console.log(index);
		//    console.log(object);

			files_to_be_removed.forEach(function(item__, index__, object__) {
		//		console.log(item__)
		//		console.log(index__)
		//		console.log(object__)

		//		console.log(typeof id)
		//		console.log(typeof xhr.file_info.id)

				if (xhr.file_info.id === id) {
					console.log(xhr.file_info.id + '===' +id)
					myDropzone.removeFile(item)
					object__.splice(index__, 1);
					object.splice(index, 1);
				}
			});

		});
		console.log(myDropzone.files.length)
		console.log(files_to_be_removed.length)
//		console.log(files_to_be_removed);
	}
</script>