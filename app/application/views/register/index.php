<div class="top-header clearfix">
    <div class="pull-left web-logo"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>/template_file/img/logo_patients_login.png" alt=""></a></div>
    <div class="pull-right top-hd-phone"><i class="fa fa-phone"></i> Inquire by Phone: <strong><a href="tel:1-855-80">1-855-80-DOCTOR</a></strong></div>
</div>

<div class="page-center-m"><!-- 'page-patient-login' class added-->



        <div class="container-fluid" id="login_page">
			<div class="pt-box-form">
                <div class="patient_box">
                    <div class="pb_head">Are you a Patient?</div>
                    <div class="pb_main">
                        
                        <div class="pb_main_img">
                            <img src="<?php echo base_url(); ?>/template_file/img/patient_profile.png" alt="">
                        </div>
                        <div class="pb_main_txt">
                            <p>Why wait weeks for an appointment or waste time in waiting rooms when you can get easy access to your favorite physicians on demand and on your schedule wherever you are. It’s time to get better and get back to living life doing the things you love. Plus, you save on healthcare costs!</p>
                            <a href="<?php echo base_url(); ?>register/patient"><div class="pb_main_btn">
                                Create a FREE account now. <span>See a doctor in minutes…</span>
                            </div></a>
                        </div>
                    </div>
                </div>
                <div class="provider_box">
                    <div class="pb_head">Are you a Provider?</div>
                    <div class="pbx_main">
                        
                        <div class="pbx_main_img">
                            <img src="<?php echo base_url(); ?>/template_file/img/provider.png" alt="">
                        </div>
                        <div class="pbx_main_txt">
                            <p>Unlock untapped revenue paths and improve patient care with turnkey telemedicine software and a chronic care management program that extends your reach and enables you to serve the ever-growing needs of patients.  Whether you combine our solutions or use them individually, gaining patients and coordinating care has never been easier!</p>
                            <a href="<?php echo base_url(); ?>register/doctor"><div class="pbx_main_btn">
                                Grow your practice the easy way. <span>Start a FREE 90-day trial…</span>
                            </div></a>
                        </div>
                    </div>
                </div>
            </div>

         

        </div>

    

</div><!--.page-center-->