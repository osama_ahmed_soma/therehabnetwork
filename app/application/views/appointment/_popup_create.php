<?php /** @var array $patients */?>
<?php /** @var array $doctors */?>
<?php /** @var array $reasons */?>

<div class="appointment-overlay" id="appointment_overlay">

    <?php  if ( isset($error) && is_array($error)): ?>
    <div class="appointment-popup-1">
        <i class="fa fa-times-circle-o close-appointment" style="position: absolute;right: 0px;cursor: pointer;">&nbsp;</i>
            <div class="wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <?php
                        foreach ( $error as $err ) {
                            echo "<div class='alert alert-danger' role='alert'>{$err}</div>";
                        }
                        ?>
                    </div>
                </div>
            </div>
    </div>


    <?php else: ?>

    <form class="createAppoint" id="payment-form">
        <div class="appointment-popup-1">
            <i class="fa fa-times-circle-o close-appointment" style="position: absolute;right: 0px;cursor: pointer;">&nbsp;</i>
            <div class="wrapper">
                <h4>Create New Appointment</h4>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3 appointment-patient">
                            <h5>Patient</h5>
                            <div class="media">

                                    <input type="hidden" value="<?php echo $patient->user_id; ?>" name="pid">
                                    <div class="media-left">
                                        <?php
                                        echo '<img src="' . base_url() . $patient->image_url . '">';
                                        ?>

                                    </div>
                                    <div class="media-body">
                                        <?php
                                        echo '<h6>' . $patient->firstname . ' ' . $patient->lastname . '</h6>';
                                        ?>
                                    </div>

                            </div>
                        </div>
                        <div class="col-sm-3 appointment-doctor">
                            <h5>Doctor</h5>
                            <div class="media">
                                <?php
                                if ( is_object($doctor) ) {
                                    ?>
                                    <input type="hidden" value="<?php echo $doctor->userid; ?>" name="did">
                                    <div class="media-left">
                                        <?php
                                        echo '<img src="' . base_url() . $doctor->image_url . '">';
                                        ?>

                                    </div>
                                    <div class="media-body">
                                        <?php
                                        echo '<h6>' . $doctor->firstname . ' ' . $doctor->lastname . '</h6>';
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6 appointment-reason">
                            <h5><label for="reason_id">Reasons for Appointment</label></h5>
                            <select name="reason_id" id="reason_id" class="select2">
                                <option value="">Primary Concern/Reasons</option>
                                <?php if( is_array($reasons) && !empty($reasons) ) foreach ($reasons as $reason) { ?>
                                    <option value="<?php echo $reason->id; ?>" <?php echo isset($appointment) && $appointment->reason_id == $reason->id ? 'selected="selected"' : ''; ?>><?php echo $reason->name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">

                            <?php  if ( isset($appointment) ) { ?>
                                <div class="col-sm-6 summarized">
                                    <input type="hidden" class="selected_date" name="avail" value="<?php echo $appointment->start_date_stamp . '-' . $appointment->end_date_stamp; ?>">
                                    <input type="hidden" name="avaialble_time_id" value="<?php echo $appointment->available_id; ?>">
                                    <h5>When</h5>
                                    <a id="appointment-sum_edit"><h5></h5></a>
                                    <hr>
                                    <h2 class="edit_time"><?php echo date('h:i A', $appointment->start_date_stamp) . '-' . date('h:i A', $appointment->end_date_stamp); ?></h2>
                                    <h6 class="edit_date"><?php  echo date('l, F d Y', $appointment->start_date_stamp) ?></h6>
                                </div>

                            <?php
                            }
                            else {
                                ?>
                                <div class="col-sm-6 picker">
                                    <h5>When</h5>

                                    <div class="picker-wrapper">
                                        <div class="picker-nav">
                                            <img class="picker-nav-left"
                                                 src="<?php echo base_url() ?>/template_file/img/doctor-profile/nav-l.png">
                                    <span id="picker-nav-date" data-doctor="<?php echo $id; ?>"
                                          data-date="<?php echo date("m-d-Y"); ?>"
                                          data-month='<?php echo date("m"); ?>'><?php echo date("d M,Y") ?></span>
                                            <img class="picker-nav-right"
                                                 src="<?php echo base_url() ?>/template_file/img/doctor-profile/nav-r.png">
                                        </div>
                                        <div id="slot-20160715" class="picker-slots"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 summarized" style="display: none;">

                                    <h5>When</h5>
                                    <a id="appointment-sum_edit"><h5>Edit</h5></a>
                                    <hr>
                                    <h2 class="edit_time"></h2>
                                    <h6 class="edit_date">Monday, June 6, 2016</h6>
                                </div>
                                <?php
                            }
                        ?>

                        <div class="col-sm-6 notes">
                            <h5>Additional Notes</h5>
                            <textarea name="appointment_notes" id="appointment-notes"
                                      placeholder="Notes"></textarea>
                        </div>
                    </div>
                </div>
                <button id="popup-1-cancel" class="cancel">Cancel</button>
                <button id="popup-1-next" class="proceed step1">Next</button>
            </div>
        </div>


        <div class="appointment-popup-2">
            <div class="wrapper">
                <h1>Consent to treat</h1>
                <div class="concent_text" style="max-height: 350px; overflow: scroll;">
                    <p><strong>Terms and Conditions</strong></p>
                    <p>These Terms and Conditions define the obligations of My Virtual Doctor, LLC, its authorized agents and me, the service subscriber, and they establish the basic rules of safe and fair use of My Virtual Doctor, LLC services.  My Virtual Doctor, LLC and its authorized agents reserve the right to immediately and without advance notice terminate the service and deny access to individuals who do not abide by the Terms and Conditions</p>
                    <p>By using the My Virtual Doctor, LLC public Website, the My Virtual Doctor, LLC secure Website and the My Virtual Doctor, LLC telemedicine service, I signify my acceptance of the My Virtual Doctor, LLC services Terms and Conditions and software provider End User Agreement.  If I do not accept the My Virtual Doctor, LLC services Terms and Conditions and software provider End User Agreement I should not use this service.  If My Virtual Doctor, LLC or software provider changes the Terms and Conditions or End User Agreement, they will post those changes prominently.  My continued use of the services and Websites following the posting of changes to these terms will mean I accept those changes.  Changes to the Terms and Conditions and End User Agreement will become effective immediately upon posting on teh My Virtual Doctor, LLC Websites and shall supersede all prior versions of the Terms and Conditions and End User Agreement unless otherwise noted.</p>
                    <p><strong>Privacy and Security</strong></p>
                    <p>My Virtual Doctor, LLC considers the privacy of my health information to be one of the most important elements in our relationship with me.  My Virtual Doctor, LLC's responsibility to maintain the confidentiality of my health information is one that they take very seriously.  I accept My Virtual Doctor, LLC Privacy and Security Policy.</p>
                    <p>I understand that it is extremely important that I keep my password to access My Virtual Doctor, LLC completely confidential.  If at any time I feel that the confidentiality of my password has been compromised, I will change it by going to the Password link on the My Virtual Doctor, LLC website.  I understand that My Virtual Doctor, LLC or their authorized vendors and agents take no responsibility for and disclaim any and all liability or consequential damages arising from a breach of health record confidentiality resulting from my sharing or losing my password.  If My Virtual Doctor, LLC or their authorized vendors and agents discovers that I have inappropriately shared my password with another person, or that I have misused or abused my online access privileges in any way, my participation may be discontinued without prior notice.</p>
                    <p><strong>Use of My Virtual Doctor, LLC for Health Care Services</strong></p>
                    <p>My registration authorizes me to use My Virtual Doctor, LLC services as provided in this agreement.  It is my duty to be truthful and accurate with all the information I enter into or upload to the system.  I acknowledge that I understand that any misrepresentations about the patient's condition may result in serious harm to me or others.  The law requires that every medical diagnostic or treatment encounter be documented.  The documentation of consultation encounters with My Virtual Doctor, LLC is maintained in an electronic health record (EHR).  I may also use my EHR to store other important medical information pertaining to my current health, medical condition and my health history.  While my account with My Virtual Doctor, LLC is active and in good standing, I will have unlimited access to my medical information stored in my EHR.</p>
                    <p>By accepting this Agreement, I am granted a non-transferable license subject to the terms of this Agreement to use My Virtual Doctor, LLC services.  In order to be valid, my account myst contain certain required true, correct and verifiable information about my identity and medical history.  In order to maintain access to My Virtual Doctor, LLC services, and in order for My Virtual Doctor, LLC to provide me important information regarding my medical treatment and my health, it is my responsibility to update my personal account information and to notify My Virtual Doctor, LLC of any changes in my home address, e-mail address, telephone number, or guardian or emergency contact.  My failure to do so may result in interruption of service or My Virtual Doctor, LLC's inability to deliver to me important time sensitive information about my medical condition, medications, laboratory and diagnostic test results.  I may update my personal information by accessing my registration information through the My Virtual Doctor, LLC Website by loggin in with my username and password.</p>
                    <p>My Virtual Doctor, LLC's telemedicine consults are provided by clinicians dedicated to the safe and effective, evidence-based practice of telemedicine.  I choose to enter into a clinician-patient relationship with My Virtual Doctor, LLC's clinicians.  I agree to have my medical history and other diagnostic and medical documentation review by one of My Virtual Doctor, LLC clinicians.  I acknowledge that My Virtual Doct, LLC clinicians do not prescribe DEA controlled substances, non-therapeutic drugs and certain other drugs which may be harmful because of their potential for abuse.  My Virtual Doctor, LLC's clinicians reserve the right to deny care for potential misuse of services.  My Virtual Doctor, LLC operates subject to state regulation and may not be available in certain states.</p>
                    <p>For individuals who are under age 18, a parent or legal guardian must accept this Agreement on his or her behalf.  I agree at all times not to falsify or misrepresent my identity or my authority to act on behalf on another person.  I also agree to not attempt or facilitate to attack or undermine the security of the integrity of the systems or networks of My Virtual Doctor, LLC, the software provider or any of its authorized agents or affiliates.</p>
                    <p>I understand that My Virtual Doctor, LLC should never be used for urgent matters.  Therefore, for all urgent matters that I believe may immediately affect my health or well-being, I will, without delay, go to the emergency department of a local hospital, and/or dial 911.</p>
                    <p>My Virtual Doctor, LLC makes the consultation report available for you to review to ensure that relevant signs and symptoms of patients presenting complaint are accurately documented and that you understand the treatment decision and instructions issued by the My Virtual Doctor, LLC health care provider.  I am advised to immediately contact My Virtual Doctor, LLC if I disagree with or do not understand the contents of the consultation report or the instructions issued by the treating My Virtual Doctor, LLC health care provider.</p>
                    <p>I understand that My Virtual Doctor, LLC clinicians or staff may send me messages.  These messages may contain information that is important to my health and medical care.  It is my responsibility to monitor these messages.  By entering my valid and functional e-mail address and mobile phone number, I have enabled My Virtual Doctor, LLC to notify me of messages sent to my My Virtual Doctor, LLC inbox.  I will update my e-mail address on My Virtual Doctor, LLC as needed.  I agree not to hold My Virtual Doctor, LLC or its authorized vendors and agents liable for any loss, injury or claims of any kind resulting from My Virtual Doctor, LLC messages that I fail to read in a timely manner.  I understand that contents of any message may be stored in my own permanent health record.  I agree that all communication will be in regard to my own health condition(s).  I understand that asking for advice on behalf of another person could potentially be harmful and is a violation of the My Virtual Doctor, LLC Terms and Conditions.  My Virtual Doctor, LLC and its clinicians do not assume any responsibility for health information or services used by persons other than the primary account holder.</p>
                    <p><strong>Deactivation of Account</strong></p>
                    <p>I understand that my account may be deactivated upon my request or at the discretion of My Virtual Doctor, LLC for failure to meet these Terms and Conditions</p>
                    <p><strong>Disclaimer</strong></p>
                    <p>I understand that my account may not be available to me at all times due to unanticipated system failures, back-up procedures, maintenance, or other causes beyond the control of the My Virtual Doctor, LLC or its authorized vendors and agents.  Access is provided on an "as-is as-is available" basis and My Virtual Doctor, LLC or its authorized vendors and agents do not guarantee that I will be able to access my account at all times</p>
                    <p>I understand that My Virtual Doctor, LLC or its authorized vendors and agents take no responsibility for and disclaim any and all liability arising from any inaccuracies or defects in software, communication lines, the virtual private network, the Internet or my Internet Service Provider (ISP), access system, computer hardware or software, or any other service or device that I use to access my account.</p>
                    <p>I understand taht the health care services rendered by My Virtual Doctor, LLC's clinicians are subject to their discretion and professional judgement.  I understand that My Virtual Doctor, LLC operates subject to state regulation and may not be available in certain states.</p>
                    <p><strong>Surveys</strong></p>
                    <p>I understand that from time to time I may be asked to complete patient satisfaction surveys.  My Virtual Doctor, LLC or its software provider, vendors, and agents may analyze information submitted via these surveys as part of descriptive (demographic) studies and reports.  In such cases all of my personal indentifying information will be removed.</p>
                    <p>If any provision or provisions of this Agreement shall be held to be invalid, illegal or unenforceable, the validity, legality and enforceability of the remaining provisions shall not be affected thereby.</p>
                    <p>It is understood that no delay or omission in exercising any right or rememdy identified herin shall constitute a waiver of such right or remedy, and shall not be construed as a bar to or a waiver of any such right or remedy on any other occasion.</p>
                    <p>My Virtual Doctor, LLC and its authorized agents and I agree to comply with all applicable laws and regulations of governmental bodies or agencies in performance of our respective obligations under this Agreement.</p>
                    <p> </p>
                    <p><strong>Consent for Treatment, Statement of Financial Responsibility/Assignment of Benefits</strong></p>
                    <p><strong>Patient Consent for Treatment</strong></p>
                    <p>As the patient and primary user for this telemedicine virtual consultation, I voluntarily give my permission to the health care providers of My Virtual Doctor, LLC and such assistants and other health care providers as they may deem necessary to provide medical services to me.  I understand by signing this form, I am authorizing them to treat me for as long as I seek care from My Virtual Doctor, LLC providers, or until I withdraw my consent in writing</p>
                    <p>Guardian of Patient Consent for Treatment</p>
                    <p>As the legal guardin or healthcare conservator of the patient for which this telemedicine consultations is being scheduled, I give my permission to the health care providers of My Virtual Doctor, LLC and such assistants and other health care providers as they may deem necessary to provide medical services to me.  I understand by signing this form, I am authorizing them to treat me for as long as I seek care from My Virtual Doctor, LLC providers, or until I withdraw my consent in writing.</p>
                    <p><strong>Statement of Financial Responsibility/Assignment of Benefits</strong></p>
                    <p>In consideration of our organization advancing credit to me for my health care and services, I hereby irrevocably assign and transfer to My Virtual Doctor, LLC and treating Clinicians all benefits and payments now due and payable or to become due and payable to me under and self-insurance program, under and third-party actions against any other person or entity, or under any other benefit plan or program (hereafter referred to as Benefits) for this or any other period of care.</p>
                    <p>I understand and acknowledge that this assignment does not relieve me of my financial responsibility for all My Virtual Doctor, LLC charges and treating Clinician charges incurred by me or anyone on my behalf, and I hereby accept such responsibility, including but not limited to payment of those fees and charges not directly reimbursed to My Virtual Doctor, LLC and treating Clinicians by any Benefit plan or program.  Furthermore, I agree to pay all costs of collection, reasonable attorney's fees and court costs incurred in enforcing this payment obligation.</p>
                    <p><strong>Authorization to Process Claims and Release Information</strong></p>
                    <p>I authorize My Virtual Doctor, LLc and the Clinician, caregivers and/or professional corporations that render services to me to process claims for payment by my insurance carrier on my behalf for covered services provided to me by My Virtual Doctor, LLC.  I authorize the release of necessary information, including medical information, regarding medical services rendered during this consultation and treatment or any related services or claim, to my insurance carrier(s), including any managed care plan or other payor, past and/or present employer(s), Medicare, CHAMPUS/TRICARE, authorized private review entities and/or utilization review entitles acting on behalf of such insurance carrier(s), payers, managed care plans and/or employer(s), the billing agents and collection agents or attorneys of My Virtual Doctor, LLC and/or the physicians, caregivers and/or professional corporations, my employers Workers Compensation carrier, and, as applicable, the Social Security Administration, the Health Care Financing Administration, the Peer Review Organization acting on behalf of the federal government and/or any other federal or state agency for the purpose(s) of satisfying charges billed and/or facilitating utilization review and/or otherwise complying with the obligations of state or federal law.  Authorization is hereby granted to release health record data and/or copies to my attending and/or admitting healthcare professional and/or any consulting healthcare professional and/or any healthcare professional I may be referred to for follow up care.  I further authorize My Virtual Doctor, LLC and any other healthcare provider or professional rendering services to me to obtain from any source medical history, examinations, diagnoses, treatments and other health or insurance authorization information for the purpose(s) of satisfying charges billed and/or facilitating utilization review, provideing medical treatment and/or the evaluation of such treatment, and/or otherwise complying with the obligations of federal law.  A photocopy of this Authorization may be honored.</p>
                    <p><strong>Medicare Patients Certification, Authorization to Release Information, Request</strong></p>
                    <p>I certify that the information given by my in applying for payment under Title XVIII of the Social Security Act is correct.  I authorize any holder of medical or other information about me to release to the Social Security Administration or its intermediaries or carriers any information needed for this or a RELATED Medicare claim.  I request that payment of authorized benefits be made on my behalf.</p>
                </div>

                <div class="checkbox">
                    <label for="acknowledge_check">
                        <input type="checkbox" value="" id="acknowledge_check" style="visibility:visible;" />
                        I acknowledge and agree to the Terms and Conditions and Consent to Treatment stated above.
                    </label>
                </div>

                <button id="popup-2-cancel" class="cancel">Back</button>
                <button id="popup-2-next" class="proceed">Next</button>

            </div>
        </div>

        <div class="appointment-popup-3">
            <div class="wrapper">
                <h1>Payment</h1>
                <div id="payment_error"></div>
                <div class="concent_text" style="max-height: 400px; overflow: scroll; padding: 0px 10px;">
                    <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td width="25%">Name on Card</td>
                        <td><input type="text" value="<?php echo is_object($patient) ?  $patient->firstname . ' ' . $patient->lastname : ''; ?>" class="form-control" id="name_on_card" name="name_on_card" id="card-holder-name" placeholder="Card Holder's Name"></td>
                    </tr>
                    <tr>
                        <td width="25%">Card Number</td>
                        <td> <input type="text" size="20" data-stripe="number" class="form-control" placeholder="Debit/Credit Card Number"></td>
                    </tr>
                    <tr>
                        <td width="25%">Expiration Date</td>
                        <td><div class="col-xs-6 row">
                                <select class="form-control col-sm-2" data-stripe="exp_month">
                                    <option value="01">Jan (01)</option>
                                    <option value="02">Feb (02)</option>
                                    <option value="03">Mar (03)</option>
                                    <option value="04">Apr (04)</option>
                                    <option value="05">May (05)</option>
                                    <option value="06">June (06)</option>
                                    <option value="07">July (07)</option>
                                    <option value="08">Aug (08)</option>
                                    <option value="09">Sep (09)</option>
                                    <option value="10">Oct (10)</option>
                                    <option value="11">Nov (11)</option>
                                    <option value="12">Dec (12)</option>
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <select class="form-control" data-stripe="exp_year">
                                    <?php
                                    $year1 = date('y');
                                    $year2 = date('Y');
                                    for( $i = 0 ; $i < 25; $i++ ) {
                                        echo "<option value='$year1'>$year2</option>";
                                        $year1++;
                                        $year2++;
                                    }
                                    ?>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="25%">Card CVV</td>
                        <td><input type="text" class="form-control" size="4" data-stripe="cvc" placeholder="Security Code"></td>
                    </tr>

                    </tbody>
                </table>

                    <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td width="25%"><strong>Billing Information</strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="25%">Address 1</td>
                        <td><input type="text" class="form-control" id="address_1" name="address_1" value="<?php echo is_object($patient) ? $patient->address : ''; ?>" placeholder="Address 1"></td>
                    </tr>
                    <tr>
                        <td width="25%">Address 2</td>
                        <td> <input type="text" class="form-control" id="address_2" name="address_2" value="" placeholder="Address 2"></td>
                    </tr>
                    <tr>
                        <td width="25%">Country</td>
                        <td><input type="text" class="form-control" value="<?php echo is_object($patient) ? $patient->country : ''; ?>" id="country" name="country" placeholder="Country"></td>
                    </tr>
                    <tr>
                        <td width="25%">State/Province</td>
                        <td><input type="text" class="form-control" value="" name="state" id="userState" placeholder="State/Province"></td>
                    </tr>
                    <tr>
                        <td width="25%">City</td>
                        <td><input type="text" class="form-control" value="<?php echo is_object($patient) ? $patient->city : ''; ?>" id="city" name="city" placeholder="City"></td>
                    </tr>
                    <tr>
                        <td width="25%">Zip/Postal Code</td>
                        <td><input type="text" class="form-control" id="userZip" name="zip" value="<?php echo is_object($patient) ? $patient->zip : ''; ?>" placeholder="Zip/Postal Code"></td>
                    </tr>
                    <tr>
                        <td width="25%">Phone</td>
                        <td><input type="text" class="form-control" id="userPhone" name="phone" placeholder="Phone" value="<?php echo is_object($patient) ? $patient->phone_home : ''; ?>"></td>
                    </tr>

                    </tbody>
                </table>
                </div>
                <button id="popup-3-cancel" class="cancel">Back</button>
                <button id="popup-3-next" class="proceed app_create">Make Payment</button>

            </div>
        </div>

        <div class="appointment-popup-4">
            <div class="wrapper">
                <h4 class="consult-step-heading">Thank you! Your appointment has been scheduled.</h4><br  />
                <p class="text-center  consult-calender">The doctor will confirm the appointment date once the payment has been processed. Once the appointment is confirmed, please make sure to show up at least 10 minutes before the selected date/time. The appointment will also show up under your Dashboard > Confirmed Appointments page for future reference.</p>
                <button id="popup-4-next" class="proceed">Ok</button>
            </div>
        </div>

    </form>





</div>

<script>
    $(document).ready(function () {
        var final_done = false;
        Stripe.setPublishableKey('<?php echo $doctor->stripe_two; ?>');

        <?php  if( !isset($appointment) ):  ?>

        function loadAvailableTiming(id, s_date, s_month) {
            //disable next and prev button
            var picker_nav_left = $('.picker-nav-left');
            var picker_nav_right = $('.picker-nav-right');
            picker_nav_left.css('cursor', 'not-allowed').removeClass('picker-nav-left');
            picker_nav_right.css('cursor', 'not-allowed').removeClass('picker-nav-right');
            $('.picker-slots').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i> Loading....');

            $.ajax({
                method: "POST",
                url: "<?php echo base_url(); ?>appointment/ajax_get_doctors_available_time",
                data: {id: id, select_date: s_date, select_month: s_month},
                dataType: "html",
                //async: true,
            }).success(function (data, status) {
                //alert(data);
                $(".picker-slots").html(data);
            }).done(function(){
                picker_nav_left.css('cursor', 'pointer').addClass('picker-nav-left');
                picker_nav_right.css('cursor', 'pointer').addClass('picker-nav-right');
            });
        }
        var id = '<?php echo $id; ?>';
        var s_date = '<?php echo date("Y-m-d"); ?>';
        var m_date = '<?php echo date("m");  ?>';
        loadAvailableTiming( id, s_date, m_date );

        $('.createAppoint').on('click', '.picker-nav-right', function(){
            //$(".picker-nav-right").click(function () {

            $('.picker-nav-left').css('cursor', 'pointer');

            var months = new Array();
            months[1] = "Jan";
            months[2] = "Feb";
            months[3] = "Mar";
            months[4] = "Apr";
            months[5] = "May";
            months[6] = "Jun";
            months[7] = "Jul";
            months[8] = "Aug";
            months[9] = "Sep";
            months[10] = "Oct";
            months[11] = "Nov";
            months[12] = "Dec";
            var span_date = $("#picker-nav-date").text();
            var d = Date.parse(span_date);
            d = new Date(d);

            var currentDate = new Date(d.getTime() + 24 * 60 * 60 * 1000);
            var day = currentDate.getDate();
            var month = currentDate.getMonth() + 1;
            var year = currentDate.getFullYear();
            $("#picker-nav-date").text(day + " " + months[month] + "," + year);
            if ( day < 10) {
                day = '0' + day;
            }
            $("#picker-nav-date").attr("data-date", year + '-' + ("0" + month).slice(-2) + '-' + day);
            $("#picker-nav-date").attr("data-month", ("0" + month).slice(-2));

            //call ajax
            var s_date = $("#picker-nav-date").attr("data-date");
            var s_month = $("#picker-nav-date").attr("data-month");
            var id = $("#picker-nav-date").attr("data-doctor");

            loadAvailableTiming(id, s_date, s_month);
        });

        $('.createAppoint').on('click', '.picker-nav-left', function(){
            //$(".picker-nav-left").click(function () {
            var months = new Array();
            months[1] = "Jan";
            months[2] = "Feb";
            months[3] = "Mar";
            months[4] = "Apr";
            months[5] = "May";
            months[6] = "Jun";
            months[7] = "Jul";
            months[8] = "Aug";
            months[9] = "Sep";
            months[10] = "Oct";
            months[11] = "Nov";
            months[12] = "Dec";
            var span_date = $("#picker-nav-date").text();
            var d = Date.parse(span_date);
            d = new Date(d);

            var today = new Date(Date.parse('<?php echo date('Y-m-d'); ?>')); //today
            var currentDate = new Date(d.getTime() - 24 * 60 * 60 * 1000);

            //check for previous date

            if( d.getTime() < today.getTime()  ) {
                $('.picker-nav-left').css('cursor', 'not-allowed');
                return false;
            }


            var day = currentDate.getDate();
            var month = currentDate.getMonth() + 1;
            var year = currentDate.getFullYear();
            $("#picker-nav-date").text(day + " " + months[month] + "," + year);
            if ( day < 10) {
                day = '0' + day;
            }
            $("#picker-nav-date").attr("data-date", year + '-' + ("0" + month).slice(-2) + '-' + day);
            $("#picker-nav-date").attr("data-month", ("0" + month).slice(-2));

            //ajax call start from here
            var s_date = $("#picker-nav-date").attr("data-date");
            var s_month = $("#picker-nav-date").attr("data-month");
            var id = $("#picker-nav-date").attr("data-doctor");

            loadAvailableTiming(id, s_date, s_month);
        });

        $('.picker-slots').on('click', "button.picker-slots-button", function (e) {
            e.preventDefault();
            var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            var month = new Array();
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";
            var stm = $(this).attr("data-start_time");
            var etm = $(this).attr("data-end_time");
            var d = $(this).attr("data-date");
            var d = new Date(d);
            var dt = d.getDate();
            var m = month[d.getMonth()];
            var day = days[d.getDay()];
            var y = d.getFullYear();
            var start_date = $(this).data('start_date');
            var end_date = $(this).data('end_date');
            var available_time_id = $(this).data('available');

            $(".edit_time").text(stm + "-" + etm);
            $(".edit_date").html(day + ", " + m + " " + dt + " " + y + "<input type='hidden' class='selected_date' name='avail' value='" + start_date + "-" + end_date + "'><input type='hidden' name='avaialble_time_id' value='"+ available_time_id  +"'>");
            $(".picker").hide("fast");
            $(".summarized").show("fast");

            return false;
        });
        $("#appointment-sum_edit").click(function () {
            $(".picker").show("fast");
            $(".summarized").hide("fast");
            $(".edit_date").html("");
        });


        <?php endif; ?>

        $('#appointment_overlay').on('click', '#popup-2-cancel', function(e){
        //$("#popup-2-cancel").click(function () {
            e.preventDefault();
            $(".appointment-popup-2").slideUp("fast");
            $(".appointment-popup-1").show("fast");
        });

        $('#appointment_overlay').on('click', '#popup-3-cancel', function( e ){
        //$("#popup-3-cancel").click(function () {
            e.preventDefault();
            $(".appointment-popup-3").slideUp("fast");
            $(".appointment-popup-2").show("fast");
        });

        $('#appointment_overlay').on('click', '#popup-1-next', function(e){
        //$("#popup-1-next").click(function (e) {
            e.preventDefault();
            var appdt = $(".selected_date").val();
            var rs = $("#reason_id").val();
            if (rs == '' || appdt == undefined) {
                var text = 'Select select appointment time.';
                if ( rs == '' ) {
                    text = 'Please select a reasons for Appointment.'
                }
                noty({
                    text: text,
                    theme: 'relax',
                    type: 'error',
                    layout: 'topRight',
                    timeout: 10 * 1000,

                    animation: {
                        open: 'animated bounceInRight',
                        close: 'animated bounceOutRight'
                    }
                });
            }
            else {
                $(".appointment-popup-1").slideUp("fast");
                $(".appointment-popup-2").show("fast");
            }
        });

        $('#appointment_overlay').on('click', '#popup-2-next', function(e){
        //$("#popup-2-next").click(function (e) {
            //concent to treat

            e.preventDefault();

            if( $('#acknowledge_check').prop("checked") == false ) {
                var str = '<strong><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error</strong> Please acknowledge and agree to the Terms and Conditions and Consent to Treatment stated below.';
                notyError(str);
            } else {
                $(".appointment-popup-2").slideUp("fast");
                $(".appointment-popup-3").show("fast");
            }
        });


        $('#appointment_overlay').on('click', '#popup-3-next', function(e){
            //$("#popup-3-next").click(function () {
            e.preventDefault();

            if ( final_done == true ) {
                $(".appointment-popup-3").slideUp("fast");
                $(".appointment-popup-4").show("fast");
            }

            return false;

        });

        $('#appointment_overlay').on('click', '#popup-3-next', function(e){
        //$('#popup-3-next').click(function(e){
            e.preventDefault();

            if ( final_done === true ) {
                return;
            }

            //disable buttons
            $('#popup-3-cancel').prop('disabled', true);
            $('#popup-3-next').prop('disabled', true);

            //show modal with wait message
            var str = '<div class="row" role="alert"><i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> \
                <span class="sr-only">Loading...</span> Please wait...</div>';
            $('#payment_error').html(str).show();

            ////now get stripe token
            var $form = $('#payment-form');
            Stripe.card.createToken($form, stripeResponseHandler);
        });

        function stripeResponseHandler(status, response) {
            // Grab the form:
            var $form = $('#payment-form');
            $('#payment_error').html('');

            if (response.error) { // Problem!

                // Show the errors on the form:
                notyError(response.error.message);
                //$('#payment_error').html('<div class="alert alert-danger" role="alert">' + response.error.message + '</div>').show();
                //enable buttons
                $('#popup-3-cancel').prop('disabled', false);
                $('#popup-3-next').prop('disabled', false);

            } else { // Token was created!

                // Get the token ID:
                var token = response.id;
                var data = $('#payment-form').serializeArray(); // convert form to array
                data.push({name: "stripeToken", value: token});

                <?php
                if ( isset($appointment) ) { ?>

                data.push({name: "appointment_id", value: '<?php echo $appointment->id; ?>'});
                <?php
                }
                ?>

                //show modal with processing message
                var str = '<div class="row"><i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> \
                <span class="sr-only">Loading...</span> Please wait, while we are processing your request...</div>';
                $('#payment_error').html(str).show();

                var url = '<?php

                if ( isset($appointment) ) {
                    echo base_url() . 'appointment/ajax_edit_appointment';
                } else {
                    echo base_url() . 'appointment/ajax_create_new';
                }

                ?>';

                //ajax call
                $.ajax({
                    url: url,
                    //data: $.param(data),
                    data: data,
                    method: "POST",
                    dataType: "json"
                }).error(function (err) {
                    //enable buttons
                    notyError(err.responseText);

                    $('#popup-3-cancel').prop('disabled', false);
                    $('#popup-3-next').prop('disabled', false);
                    console.log('Error: ', err);

                }).done(function () {
                    $('#popup-3-next').prop('disabled', false);

                }).success(function (data) {
                    //console.log(data);
                    if ( data.success ) {
                        final_done = true;
                        $('#payment_error').html('').hide();

                        if( data.message ) {
                            $.each(data.message, function(index, msg){
                                notySuccess(msg);
                            });
                        }

                        //reset form
                        resetForm($('#payment-form'));

                        $('#popup-3-next').trigger('click');



                    } else if ( data.error ) {
                        $.each(data.error, function(index, msg) {
                            notyError(msg)
                        });
                    }
                }, 'json');
            }
        }

        function resetForm($form) {
            $form.find('input:text, input:hidden, input:password, input:file, select, textarea').val('');
            $form.find('input:radio, input:checkbox')
                .removeAttr('checked').removeAttr('selected');
        }


        $('#appointment_overlay').on('click', '#popup-4-next', function(e){
            //$("#popup-4-next").click(function () {
            e.preventDefault();
            $(".appointment-popup-3").hide("fast");
            $(".appointment-overlay").hide("fast");
        });

        $('#appointment_overlay').on('click', '#popup-1-cancel', function(e){
        //$("#popup-1-cancel").click(function () {
            e.preventDefault();
            $(".appointment-popup-1").hide("fast");
            $(".appointment-overlay").hide("fast");
        });
    });
</script>
<?php endif;  //isset($error) end here ?>