<div class="modal fade paient-appointment-bf" id="patientMakeAppointment" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>


            <?php if (isset($error) && is_array($error)): ?>
                <div class="appointment-popup-1">
                    <div class="wrapper">
                        <div class="container-fluid">
                            <div class="row" style="text-align: center; padding: 20px;">
                                <?php
                                foreach ($error as $err) {
                                    echo "<div class='alert alert-danger' role='alert'>{$err}</div>";
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>


            <?php else: ?>


                <div class="stepwizard">

                    <div class="stepwizard-row setup-panel">

                        <div class="stepwizard-step">

                            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>

                            <p>Appointment Details</p>

                        </div>

                        <div class="stepwizard-step">

                            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>

                            <p>Consent to treat</p>

                        </div>

                        <div class="stepwizard-step">

                            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>

                            <p>Payment</p>

                        </div>

                        <div class="stepwizard-step">

                            <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>

                            <p>Confirmation</p>

                        </div>

                    </div>

                </div>

					<?php echo form_open('', ['id' => 'payment-form', 'role' => 'form']); ?>
					
                    <input type="hidden" name="fee_waived" id="fee_waived" value="<?php echo isset($billing_waived) ? $billing_waived : '0'; ?>"/>
                    <div class="row setup-content" id="step-1">

                        <div class="col-xs-12">

                            <div class="col-md-12">

                                <h3 class="text-center head-appoint"><i class="fa fa-calendar"></i> Create New
                                    Appointment</h3>

                                <div class="col-md-6 left-app">
                                    <div class="doc-img">

                                        <div class="col-md-6 col-xs-12 ">

                                            <input type="hidden" value="<?php echo $patient->user_id; ?>" name="pid">
                                            <div class="di-user ">
                                                <?php echo '<img src="' . default_image($patient->image_url) . '">'; ?>
                                            </div>
                                            <div class="di-detail">
                                                <h6><?php echo $patient->firstname . ' ' . $patient->lastname; ?>
                                                    <span><?php echo $patient->city;
                                                        echo isset($patient->code) ? ', ' . $patient->code : ''; ?></span>
                                                </h6>
                                            </div>


                                        </div>

                                        <?php if (isset($doctors) && !empty($doctors)): ?>
                                            <div class="col-md-6 col-xs-12 ">
                                                <input type="hidden" value="" name="did" id="doctor_id"/>
                                                <input type="hidden" value="" id="did_stripe"/>
                                                <div class="">
                                                    <div class="di-user unselect di-user-main"><i
                                                            class="fa fa-user"></i></div>
                                                    <div class="di-detail di-detail-main">Select a Doctor</div>
                                                    <div class="more-doc"><a data-toggle="collapse"
                                                                             id="patient_collapse"
                                                                             data-parent="#accordion1"
                                                                             href="#collapseOne"> <i
                                                                class="fa fa-angle-down" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-app clearfix panel-collapse collapse" id="collapseOne">
                                                <div class="add-appointment">Who is this appointment for?</div>
                                                <div class="ap-inner clearfix">
                                                    <div class="ap-in-search"><input type="text" placeholder="Search"
                                                                                     class="form-control search_patient"/>
                                                    </div>
                                                    <div id="patient_listing">
                                                        <?php
                                                        if (is_array($doctors)) {
                                                            foreach ($doctors as $doc) { ?>
                                                                <div class="doc-img">
                                                                    <div class="di-user"><img
                                                                            src="<?php echo default_image($doc->image_url); ?>"/>
                                                                    </div>
                                                                    <div class="di-detail select_patient"><a href="#"
                                                                                                             data-image_url="<?php echo default_image($doc->image_url); ?>"
                                                                                                             data-patient_id="<?php echo $doc->userid; ?>"
                                                                                                             data-did_stripe="<?php echo $doc->stripe_two; ?>"
                                                                                                             data-patient_name="<?php echo $doc->firstname . ' ' . $doc->lastname; ?>"
                                                                                                             data-patient_details="<?php echo $doc->city;
                                                                                                             echo isset($doc->code) ? ', ' . $doc->code : ''; ?>"><?php echo $doc->firstname . ' ' . $doc->lastname; ?></a>
                                                                        <span><?php echo $doc->city;
                                                                            echo isset($doc->code) ? ', ' . $doc->code : ''; ?></span>
                                                                    </div>
                                                                </div>

                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php else: ?>

                                            <div class="col-md-6">
                                                <?php
                                                if (isset($doctor) && is_object($doctor)) {
                                                    ?>
                                                    <input type="hidden" value="<?php echo $doctor->userid; ?>"
                                                           name="did" id="doctor_id">
                                                    <input type="hidden" value="<?php echo $doctor->stripe_two; ?>"
                                                           id="did_stripe"/>
                                                    <div class="di-user ">
                                                        <?php
                                                        echo '<img src="' . default_image($doctor->image_url) . '">';
                                                        ?>

                                                    </div>
                                                    <div class="di-detail">
                                                        <h6><?php echo $doctor->firstname . ' ' . $doctor->lastname; ?>
                                                            <span><?php echo $doctor->city;
                                                                echo isset($doctor->code) ? ', ' . $doctor->code : ''; ?></span>
                                                        </h6>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>


                                    <div class="appointment-time">
                                        <?php if (isset($appointment)) { ?>
                                            <div class="col-sm-6 summarized pt-cnt">
                                                <input type="hidden" class="selected_date" name="avail"
                                                       value="<?php echo $appointment->start_date_stamp . '-' . $appointment->end_date_stamp; ?>">
                                                <input type="hidden" name="avaialble_time_id"
                                                       value="<?php echo $appointment->available_id; ?>">
                                                <h5 style="display: inline-block;" class="pull-left">When</h5>
                                                <a id="" style="display: inline-block;" class="pull-right"><h5></h5></a>
                                                <hr style="clear: both;">
                                                <h2 class="edit_time"><?php echo date('h:i A', $appointment->start_date_stamp) . '-' . date('h:i A', $appointment->end_date_stamp); ?></h2>
                                                <h6 class="edit_date"><?php echo date('l, F d Y', $appointment->start_date_stamp) ?></h6>
                                            </div>
                                            <?php
                                        } else {
                                            ?>
                                            <!-- <div class="pt-head"><i class="fa fa-clock-o"></i> when</div>-->
                                            <div class="">
                                                <div class="pt-cnt picker">
                                                    <div class="pt-calendar clearfix">
                                                        <div class="pt-cl-head clearfix">
                                                            <div class="pt-month">
                                                                <div class='input-group date' id='doctorTimings'>
                                                                    <input type='text' class="form-control"
                                                                           id="doctorTimings_field"
                                                                           value="<?php echo date('m/d/Y'); ?>"/>
                                                                    <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="pt-cl-cnt clearfix picker-slots"
                                                             id="picker_slots"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 summarized pt-cnt" style="display: none;">

                                                    <h5 style="display: inline-block;" class="pull-left">When</h5>
                                                    <a id="appointment-sum_edit" style="display: inline-block;"
                                                       class="pull-right"><h5>Edit</h5></a>
                                                    <hr style="clear: both;">
                                                    <h2 class="edit_time"></h2>
                                                    <h6 class="edit_date">Monday, June 6, 2016</h6>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>

                                </div>

                                <div class="col-md-6 right-app right-app-from">
                                    <?php if (!isset($billing_waived)) { ?>
                                    <div class="form-group" id="billing_waived_div">
                                        <label class="control-label">Consultation Type</label>
                                        <?php if (isset($consultation_types) && is_array($consultation_types) && !empty($consultation_types) ): ?>
                                            <?php if( !isset($consultation_type_data) && isset($appointment) ) { ?>
                                                <p>30 minutes : $<?php echo $appointment->amount; ?></p>
                                            <?php } elseif(isset($consultation_type_data)) { ?>
                                                <input type="text" class="form-control" value="<?php echo $consultation_type_data->duration; ?> minutes : $<?php echo $consultation_type_data->rate; ?>" readonly="readonly" />
                                            <?php } else { ?>
                                                <select name="consultation_type" id="consultation_type" class="select2 form-control" <?php echo isset($consultation_type_data) && is_object($consultation_type_data) && isset($consultation_type_data->id) ? 'readonly="readonly"' : ''; ?>>
                                                    <?php foreach($consultation_types as $consultation_type): ?>
                                                        <option value="<?php echo $consultation_type->id; ?>" <?php echo isset($consultation_type_data) && is_object($consultation_type_data) && isset($consultation_type_data->id) && $consultation_type->id == $consultation_type_data->id ? 'selected="selected"' : ''; ?>><?php echo $consultation_type->duration; ?> minutes : $<?php echo $consultation_type->rate; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                        <?php } else: ?>
                                            <!-- show empty select for now, after selecting doctor it will be populated -->
                                            <select name="consultation_type" id="consultation_type" class="select2 form-control"></select>

                                        <?php endif; ?>
                                    </div>
                                    <?php } ?>
                                    <div class="form-group ">

                                        <label class="control-label">Reasons for Appointment</label>

                                        <select name="reason_id" id="reason_id" class="select2 form-control">
                                            <option value="">Primary Concern/Reasons</option>
                                            <?php if (isset($reasons) && is_array($reasons) && !empty($reasons)) foreach ($reasons as $reason) { ?>
                                                <option
                                                    value="<?php echo $reason->id; ?>" <?php echo isset($appointment) && $appointment->reason_id == $reason->id ? 'selected="selected"' : ''; ?>><?php echo $reason->name; ?></option>
                                            <?php } ?>
                                        </select>
                                        <?php if (isset($appointment) && $appointment->reason != ''): ?>
                                            <input type="text" placeholder="Please enter other reason here"
                                                   name="reason" id="reason_textbox"
                                                   value="<?php echo $appointment->reason; ?>" class="form-control"/>
                                        <?php else: ?>
                                            <input type="text" placeholder="Please enter other reason here"
                                                   name="reason" value="" id="reason_textbox" style="display:none;"
                                                   class="form-control"/>
                                        <?php endif; ?>
                                    </div>

                                    <!--<div class="add-concern">

                                        <a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i>  Add a Concern</a>

                                    </div> -->

                                    <div class="form-group">

                                        <label class="control-label">Additional Notes</label>

                                        <textarea name="appointment_notes" rows="4" id="appointment-notes"
                                                  class="form-control"
                                                  placeholder="Notes"><?php echo isset($appointment) ? $appointment->notes : ''; ?></textarea>

                                    </div>


                                </div>

                                <div class="col-md-12 next-btn">

                                    <button class="btn btn-primary nextBtn btn-lg pull-right step_1_next" type="button">
                                        Next
                                    </button>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="row setup-content" id="step-2">

                        <div class="col-xs-12 ">

                            <div class="col-md-12 ">

                                <h3 class="text-center head-appoint"><i class="fa fa-stethoscope"
                                                                        aria-hidden="true"></i> Consent to treat</h3>

                                <div class="col-md-12 concent-to-treat clearfix ">

                                    <p><strong>Terms and Conditions</strong></p>
                                    <p>These Terms and Conditions define the obligations of My Virtual Doctor, LLC, its
                                        authorized agents and me, the service subscriber, and they establish the basic
                                        rules of safe and fair use of My Virtual Doctor, LLC services.  My Virtual
                                        Doctor, LLC and its authorized agents reserve the right to immediately and
                                        without advance notice terminate the service and deny access to individuals who
                                        do not abide by the Terms and Conditions</p>
                                    <p>By using the My Virtual Doctor, LLC public Website, the My Virtual Doctor, LLC
                                        secure Website and the My Virtual Doctor, LLC telemedicine service, I signify my
                                        acceptance of the My Virtual Doctor, LLC services Terms and Conditions and
                                        software provider End User Agreement.  If I do not accept the My Virtual Doctor,
                                        LLC services Terms and Conditions and software provider End User Agreement I
                                        should not use this service.  If My Virtual Doctor, LLC or software provider
                                        changes the Terms and Conditions or End User Agreement, they will post those
                                        changes prominently.  My continued use of the services and Websites following
                                        the posting of changes to these terms will mean I accept those changes.  Changes
                                        to the Terms and Conditions and End User Agreement will become effective
                                        immediately upon posting on teh My Virtual Doctor, LLC Websites and shall
                                        supersede all prior versions of the Terms and Conditions and End User Agreement
                                        unless otherwise noted.</p>
                                    <p><strong>Privacy and Security</strong></p>
                                    <p>My Virtual Doctor, LLC considers the privacy of my health information to be one
                                        of the most important elements in our relationship with me.  My Virtual Doctor,
                                        LLC's responsibility to maintain the confidentiality of my health information is
                                        one that they take very seriously.  I accept My Virtual Doctor, LLC Privacy and
                                        Security Policy.</p>
                                    <p>I understand that it is extremely important that I keep my password to access My
                                        Virtual Doctor, LLC completely confidential.  If at any time I feel that the
                                        confidentiality of my password has been compromised, I will change it by going
                                        to the Password link on the My Virtual Doctor, LLC website.  I understand that
                                        My Virtual Doctor, LLC or their authorized vendors and agents take no
                                        responsibility for and disclaim any and all liability or consequential damages
                                        arising from a breach of health record confidentiality resulting from my sharing
                                        or losing my password.  If My Virtual Doctor, LLC or their authorized vendors
                                        and agents discovers that I have inappropriately shared my password with another
                                        person, or that I have misused or abused my online access privileges in any way,
                                        my participation may be discontinued without prior notice.</p>
                                    <p><strong>Use of My Virtual Doctor, LLC for Health Care Services</strong></p>
                                    <p>My registration authorizes me to use My Virtual Doctor, LLC services as provided
                                        in this agreement.  It is my duty to be truthful and accurate with all the
                                        information I enter into or upload to the system.  I acknowledge that I
                                        understand that any misrepresentations about the patient's condition may result
                                        in serious harm to me or others.  The law requires that every medical diagnostic
                                        or treatment encounter be documented.  The documentation of consultation
                                        encounters with My Virtual Doctor, LLC is maintained in an electronic health
                                        record (EHR).  I may also use my EHR to store other important medical
                                        information pertaining to my current health, medical condition and my health
                                        history.  While my account with My Virtual Doctor, LLC is active and in good
                                        standing, I will have unlimited access to my medical information stored in my
                                        EHR.</p>
                                    <p>By accepting this Agreement, I am granted a non-transferable license subject to
                                        the terms of this Agreement to use My Virtual Doctor, LLC services.  In order to
                                        be valid, my account myst contain certain required true, correct and verifiable
                                        information about my identity and medical history.  In order to maintain access
                                        to My Virtual Doctor, LLC services, and in order for My Virtual Doctor, LLC to
                                        provide me important information regarding my medical treatment and my health,
                                        it is my responsibility to update my personal account information and to notify
                                        My Virtual Doctor, LLC of any changes in my home address, e-mail address,
                                        telephone number, or guardian or emergency contact.  My failure to do so may
                                        result in interruption of service or My Virtual Doctor, LLC's inability to
                                        deliver to me important time sensitive information about my medical condition,
                                        medications, laboratory and diagnostic test results.  I may update my personal
                                        information by accessing my registration information through the My Virtual
                                        Doctor, LLC Website by loggin in with my username and password.</p>
                                    <p>My Virtual Doctor, LLC's telemedicine consults are provided by clinicians
                                        dedicated to the safe and effective, evidence-based practice of telemedicine.  I
                                        choose to enter into a clinician-patient relationship with My Virtual Doctor,
                                        LLC's clinicians.  I agree to have my medical history and other diagnostic and
                                        medical documentation review by one of My Virtual Doctor, LLC clinicians.  I
                                        acknowledge that My Virtual Doct, LLC clinicians do not prescribe DEA controlled
                                        substances, non-therapeutic drugs and certain other drugs which may be harmful
                                        because of their potential for abuse.  My Virtual Doctor, LLC's clinicians
                                        reserve the right to deny care for potential misuse of services.  My Virtual
                                        Doctor, LLC operates subject to state regulation and may not be available in
                                        certain states.</p>
                                    <p>For individuals who are under age 18, a parent or legal guardian must accept this
                                        Agreement on his or her behalf.  I agree at all times not to falsify or
                                        misrepresent my identity or my authority to act on behalf on another person.  I
                                        also agree to not attempt or facilitate to attack or undermine the security of
                                        the integrity of the systems or networks of My Virtual Doctor, LLC, the software
                                        provider or any of its authorized agents or affiliates.</p>
                                    <p>I understand that My Virtual Doctor, LLC should never be used for urgent matters.
                                         Therefore, for all urgent matters that I believe may immediately affect my
                                        health or well-being, I will, without delay, go to the emergency department of a
                                        local hospital, and/or dial 911.</p>
                                    <p>My Virtual Doctor, LLC makes the consultation report available for you to review
                                        to ensure that relevant signs and symptoms of patients presenting complaint are
                                        accurately documented and that you understand the treatment decision and
                                        instructions issued by the My Virtual Doctor, LLC health care provider.  I am
                                        advised to immediately contact My Virtual Doctor, LLC if I disagree with or do
                                        not understand the contents of the consultation report or the instructions
                                        issued by the treating My Virtual Doctor, LLC health care provider.</p>
                                    <p>I understand that My Virtual Doctor, LLC clinicians or staff may send me
                                        messages.  These messages may contain information that is important to my health
                                        and medical care.  It is my responsibility to monitor these messages.  By
                                        entering my valid and functional e-mail address and mobile phone number, I have
                                        enabled My Virtual Doctor, LLC to notify me of messages sent to my My Virtual
                                        Doctor, LLC inbox.  I will update my e-mail address on My Virtual Doctor, LLC as
                                        needed.  I agree not to hold My Virtual Doctor, LLC or its authorized vendors
                                        and agents liable for any loss, injury or claims of any kind resulting from My
                                        Virtual Doctor, LLC messages that I fail to read in a timely manner.  I
                                        understand that contents of any message may be stored in my own permanent health
                                        record.  I agree that all communication will be in regard to my own health
                                        condition(s).  I understand that asking for advice on behalf of another person
                                        could potentially be harmful and is a violation of the My Virtual Doctor, LLC
                                        Terms and Conditions.  My Virtual Doctor, LLC and its clinicians do not assume
                                        any responsibility for health information or services used by persons other than
                                        the primary account holder.</p>
                                    <p><strong>Deactivation of Account</strong></p>
                                    <p>I understand that my account may be deactivated upon my request or at the
                                        discretion of My Virtual Doctor, LLC for failure to meet these Terms and
                                        Conditions</p>
                                    <p><strong>Disclaimer</strong></p>
                                    <p>I understand that my account may not be available to me at all times due to
                                        unanticipated system failures, back-up procedures, maintenance, or other causes
                                        beyond the control of the My Virtual Doctor, LLC or its authorized vendors and
                                        agents.  Access is provided on an "as-is as-is available" basis and My Virtual
                                        Doctor, LLC or its authorized vendors and agents do not guarantee that I will be
                                        able to access my account at all times</p>
                                    <p>I understand that My Virtual Doctor, LLC or its authorized vendors and agents
                                        take no responsibility for and disclaim any and all liability arising from any
                                        inaccuracies or defects in software, communication lines, the virtual private
                                        network, the Internet or my Internet Service Provider (ISP), access system,
                                        computer hardware or software, or any other service or device that I use to
                                        access my account.</p>
                                    <p>I understand taht the health care services rendered by My Virtual Doctor, LLC's
                                        clinicians are subject to their discretion and professional judgement.  I
                                        understand that My Virtual Doctor, LLC operates subject to state regulation and
                                        may not be available in certain states.</p>
                                    <p><strong>Surveys</strong></p>
                                    <p>I understand that from time to time I may be asked to complete patient
                                        satisfaction surveys.  My Virtual Doctor, LLC or its software provider, vendors,
                                        and agents may analyze information submitted via these surveys as part of
                                        descriptive (demographic) studies and reports.  In such cases all of my personal
                                        indentifying information will be removed.</p>
                                    <p>If any provision or provisions of this Agreement shall be held to be invalid,
                                        illegal or unenforceable, the validity, legality and enforceability of the
                                        remaining provisions shall not be affected thereby.</p>
                                    <p>It is understood that no delay or omission in exercising any right or rememdy
                                        identified herin shall constitute a waiver of such right or remedy, and shall
                                        not be construed as a bar to or a waiver of any such right or remedy on any
                                        other occasion.</p>
                                    <p>My Virtual Doctor, LLC and its authorized agents and I agree to comply with all
                                        applicable laws and regulations of governmental bodies or agencies
                                        in performance of our respective obligations under this Agreement.</p>
                                    <p> </p>
                                    <p><strong>Consent for Treatment, Statement of Financial Responsibility/Assignment
                                            of Benefits</strong></p>
                                    <p><strong>Patient Consent for Treatment</strong></p>
                                    <p>As the patient and primary user for this telemedicine virtual consultation, I
                                        voluntarily give my permission to the health care providers of My Virtual
                                        Doctor, LLC and such assistants and other health care providers as they may deem
                                        necessary to provide medical services to me.  I understand by signing this form,
                                        I am authorizing them to treat me for as long as I seek care from My Virtual
                                        Doctor, LLC providers, or until I withdraw my consent in writing</p>
                                    <p>Guardian of Patient Consent for Treatment</p>
                                    <p>As the legal guardin or healthcare conservator of the patient for which this
                                        telemedicine consultations is being scheduled, I give my permission to the
                                        health care providers of My Virtual Doctor, LLC and such assistants and other
                                        health care providers as they may deem necessary to provide medical services to
                                        me.  I understand by signing this form, I am authorizing them to treat me for as
                                        long as I seek care from My Virtual Doctor, LLC providers, or until I withdraw
                                        my consent in writing.</p>
                                    <p><strong>Statement of Financial Responsibility/Assignment of Benefits</strong></p>
                                    <p>In consideration of our organization advancing credit to me for my health care
                                        and services, I hereby irrevocably assign and transfer to My Virtual Doctor, LLC
                                        and treating Clinicians all benefits and payments now due and payable or to
                                        become due and payable to me under and self-insurance program, under and
                                        third-party actions against any other person or entity, or under any other
                                        benefit plan or program (hereafter referred to as Benefits) for this or any
                                        other period of care.</p>
                                    <p>I understand and acknowledge that this assignment does not relieve me of my
                                        financial responsibility for all My Virtual Doctor, LLC charges and treating
                                        Clinician charges incurred by me or anyone on my behalf, and I hereby accept
                                        such responsibility, including but not limited to payment of those fees and
                                        charges not directly reimbursed to My Virtual Doctor, LLC and treating
                                        Clinicians by any Benefit plan or program.  Furthermore, I agree to pay all
                                        costs of collection, reasonable attorney's fees and court costs incurred in
                                        enforcing this payment obligation.</p>
                                    <p><strong>Authorization to Process Claims and Release Information</strong></p>
                                    <p>I authorize My Virtual Doctor, LLc and the Clinician, caregivers and/or
                                        professional corporations that render services to me to process claims for
                                        payment by my insurance carrier on my behalf for covered services provided to me
                                        by My Virtual Doctor, LLC.  I authorize the release of necessary information,
                                        including medical information, regarding medical services rendered during this
                                        consultation and treatment or any related services or claim, to my insurance
                                        carrier(s), including any managed care plan or other payor, past and/or present
                                        employer(s), Medicare, CHAMPUS/TRICARE, authorized private review entities
                                        and/or utilization review entitles acting on behalf of such insurance
                                        carrier(s), payers, managed care plans and/or employer(s), the billing agents
                                        and collection agents or attorneys of My Virtual Doctor, LLC and/or the
                                        physicians, caregivers and/or professional corporations, my employers Workers
                                        Compensation carrier, and, as applicable, the Social Security Administration,
                                        the Health Care Financing Administration, the Peer Review Organization acting on
                                        behalf of the federal government and/or any other federal or state agency for
                                        the purpose(s) of satisfying charges billed and/or facilitating utilization
                                        review and/or otherwise complying with the obligations of state or federal law.
                                         Authorization is hereby granted to release health record data and/or copies to
                                        my attending and/or admitting healthcare professional and/or any consulting
                                        healthcare professional and/or any healthcare professional I may be referred to
                                        for follow up care.  I further authorize My Virtual Doctor, LLC and any other
                                        healthcare provider or professional rendering services to me to obtain from any
                                        source medical history, examinations, diagnoses, treatments and other health or
                                        insurance authorization information for the purpose(s) of satisfying charges
                                        billed and/or facilitating utilization review, provideing medical treatment
                                        and/or the evaluation of such treatment, and/or otherwise complying with the
                                        obligations of federal law.  A photocopy of this Authorization may be
                                        honored.</p>
                                    <p><strong>Medicare Patients Certification, Authorization to Release Information,
                                            Request</strong></p>
                                    <p>I certify that the information given by my in applying for payment under Title
                                        XVIII of the Social Security Act is correct.  I authorize any holder of medical
                                        or other information about me to release to the Social Security Administration
                                        or its intermediaries or carriers any information needed for this or a RELATED
                                        Medicare claim.  I request that payment of authorized benefits be made on my
                                        behalf.</p>
                                </div>
                                <div class="check-app-bpx">
                                    <label for="acknowledge_check">
                                        <input type="checkbox" value="" id="acknowledge_check"
                                               style="visibility:visible;"/>
                                        I acknowledge and agree to the Terms and Conditions and Consent to Treatment
                                        stated above.
                                    </label>
                                </div>


                            </div>


                            <div class="col-md-12">
                                <div class="col-md-12 next-btn">

                                    <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Back</button>
                                    <button class="btn btn-primary nextBtn btn-lg pull-right step_2_next" type="button">
                                        Next
                                    </button>

                                </div>

                            </div>

                        </div>

                    </div>
                    <div class="row setup-content" id="step-3">

                        <div class="col-xs-12 payment-account-pop">
                            <h3 class="text-center head-appoint"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                Payment</h3>
                            <div class="clearfix">
                                <div id="payment_error" style="text-align: center;"></div>
                                <div id="no_payment_div" style="<?php if (isset($billing_waived)) { echo 'display:block;'; } else { echo 'display:none;'; }?>">
                                    <div class="col-md-12">
                                        <div class="clearfix">
                                            <p>The doctor has waived the billing for this consultation, please click the button below to proceed.</p>
                                        </div>
                                    </div>
                                </div>
                                <div id="payment_div" style="<?php if (isset($billing_waived)) { echo 'display:none;'; }?>">
                                    <div class="col-md-12">
                                        <div class="clearfix hidden">
                                            <h2>Account Information </h2>
                                            <div class="form-group col-md-6">
                                                <input type="text"
                                                       value="<?php echo is_object($patient) ? $patient->firstname . ' ' . $patient->lastname : ''; ?>"
                                                       class="form-control" id="name_on_card" name="name_on_card"
                                                       id="card-holder-name" placeholder="Card Holder's Name">
                                            </div>
                                            <div class="form-group  col-md-6">
                                                <input type="text" class="form-control" id="address_1" name="address_1"
                                                       value="<?php echo is_object($patient) ? $patient->address : ''; ?>"
                                                       placeholder="Address 1">

                                            </div>

                                            <div class="form-group  col-md-6">
                                                <input type="text" class="form-control" id="address_2" name="address_2"
                                                       value="" placeholder="Address 2">

                                            </div>
                                            <div class="form-group  col-md-3">
                                                <input type="text" class="form-control"
                                                       value="<?php echo is_object($patient) ? $patient->country : ''; ?>"
                                                       id="country" name="country" placeholder="Country">

                                            </div>
                                            <div class="form-group  col-md-3">
                                                <input type="text" class="form-control" value="" name="state"
                                                       id="userState" placeholder="State/Province">

                                            </div>
                                            <div class="form-group  col-md-4">
                                                <input type="text" class="form-control"
                                                       value="<?php echo is_object($patient) ? $patient->city : ''; ?>"
                                                       id="city" name="city" placeholder="City">

                                            </div>
                                            <div class="form-group  col-md-4">
                                                <input type="text" class="form-control" id="userZip" name="zip"
                                                       value="<?php echo is_object($patient) ? $patient->zip : ''; ?>"
                                                       placeholder="Zip/Postal Code">

                                            </div>
                                            <div class="form-group  col-md-4">
                                                <input type="text" class="form-control" id="userPhone" name="phone"
                                                       placeholder="Phone"
                                                       value="<?php echo is_object($patient) ? $patient->phone_home : ''; ?>">

                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <h2>Billing Information </h2>

                                            <div class="form-group col-md-6">
                                                <input type="text" size="20" data-stripe="number" class="form-control"
                                                       placeholder="Debit/Credit Card Number">
                                            </div>
                                            <div class="form-group  col-md-6">
                                                <input type="text" class="form-control" size="4" data-stripe="cvc"
                                                       placeholder="Security Code">

                                            </div>
                                            <div class="form-group col-md-6">

                                                <select class="form-control col-sm-2" data-stripe="exp_month">
                                                    <option value="">Expiration Month</option>
                                                    <option value="01">Jan (01)</option>
                                                    <option value="02">Feb (02)</option>
                                                    <option value="03">Mar (03)</option>
                                                    <option value="04">Apr (04)</option>
                                                    <option value="05">May (05)</option>
                                                    <option value="06">June (06)</option>
                                                    <option value="07">July (07)</option>
                                                    <option value="08">Aug (08)</option>
                                                    <option value="09">Sep (09)</option>
                                                    <option value="10">Oct (10)</option>
                                                    <option value="11">Nov (11)</option>
                                                    <option value="12">Dec (12)</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <select class="form-control" data-stripe="exp_year">
                                                    <option value="">Expiration Year</option>
                                                    <?php
                                                    $year1 = date('y');
                                                    $year2 = date('Y');
                                                    for ($i = 0; $i < 25; $i++) {
                                                        echo "<option value='$year1'>$year2</option>";
                                                        $year1++;
                                                        $year2++;
                                                    }
                                                    ?>
                                                </select>
                                            </div>


                                        </div>

                                        <!--<div class="concent_text" style="max-height: 400px; overflow: scroll; padding: 0px 10px;">-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12 next-btn">
                                    <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Back</button>
                                    <button id="popup-3-next"
                                            class="btn btn-primary nextBtn btn-lg pull-right step_3_next">Proceed
                                    </button>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="row setup-content" id="step-4">

                        <div class="col-xs-12 ">
                            <h3 class="text-center head-appoint"><i class="fa fa-check" aria-hidden="true"></i>Thank
                                you! Your appointment has been scheduled.</h3>
                            <div class="col-xs-12 ">
                                <p class="text-center  consult-calender">Thank you for scheduling the appointment.
                                    Your appointment is confirmed and your doctor has been notified. Please show up at least 5 minutes before the appointment date/time.The appointment will also show up under your Dashboard > Confirmed Appointments page for reference.</p>

                            </div>

                            <div class="col-md-12">
                                <div class="col-md-12 next-btn">
                                    <a href="<?php echo base_url(); ?>patient/appointments" id="popup-4-next" class="btn btn-primary nextBtn btn-lg pull-right proceed">
                                        Ok
                                    </a>

                                </div>

                            </div>
                        </div>
                    </div>


                </form>


            <?php endif; ?>

        </div>

    </div>

</div>

<script>

    jQuery(document).ready(function ($) {
        //Stripe.setPublishableKey($('#did_stripe').val());
        next_step_ok = false;
        step_1_ok = false;
        step_2_ok = false;
        step_3_ok = false;

        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');
        allPrevBtn = $('.prevBtn');


        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allPrevBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");
            $(".form-group").removeClass("has-error");
            prevStepWizard.removeAttr('disabled').trigger('click');
        });
        $('div.setup-panel div a.btn-primary').trigger('click');


        $('#reason_id').change(function () {
            //$("#first").select2('val');
            var val = $(this).val();
            if (val == 13) {
                $('#reason_textbox').css('display', 'block');
            } else {
                $('#reason_textbox').css('display', 'none');
            }
        });

        <?php if ( isset($appointment) ) { ?>
        $('select').val('<?php echo $appointment->reason_id; ?>').trigger('change');
        <?php } ?>

        //initialize bootstrap datetime picker
        $('#doctorTimings').datetimepicker({
            viewMode: 'days',
            showClear: true,
            useCurrent: false,
            minDate: '<?php echo date('m/d/Y', time()); ?>',
            format: 'MM/DD/YYYY',
            <?php
            if(isset($doctor_available_time) && is_array($doctor_available_time)) { ?>
            enabledDates: <?php echo json_encode($doctor_available_time); ?>
            <?php
            }
            ?>
        });


        function loadAvailableTiming(id, s_date, s_month) {
            //disable next and prev button
            //1. disable next and previous button
            $('.step2_next').prop('disabled', true);

            //2. show loading state
            var str = '<div class="alert alert-info" role="alert"><i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom"></i> \
                <span class="sr-only">Loading...</span> Getting available timings...</div>';
            $('.picker-slots').html(str);


            //$('.picker-slots').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i> Loading....');

            $.ajax({
                method: "POST",
                url: "<?php echo base_url(); ?>appointment/ajax_get_doctors_available_time",
                data: {id: id, select_date: s_date, select_month: s_month},
                dataType: "html",
                //async: true,
            }).success(function (data, status) {
                //alert(data);
                $(".picker-slots").html(data);
            }).done(function () {
                $('.step2_next').prop('disabled', false);
            });
        }

        <?php if ( !isset($appointment) ): ?>
        var id = $('#doctor_id').val();
        var s_date = '<?php echo date("Y-m-d"); ?>';
        var m_date = '<?php echo date("m");  ?>';
        loadAvailableTiming(id, s_date, m_date);
        <?php endif; ?>

        //now do .picker-slots changes
        $('.picker-slots').on('click', "button.picker-slots-button", function (e) {
            e.preventDefault();

            var stm = $(this).attr("data-start_time");
            var etm = $(this).attr("data-end_time");
            var d = $(this).attr("data-fulldate");
            var start_date = $(this).data('start_date');
            var end_date = $(this).data('end_date');
            var available_time_id = $(this).data('available');

            $(".edit_time").text(stm + "-" + etm);
            $(".edit_date").html(d + "<input type='hidden' class='selected_date' name='avail' value='" + start_date + "-" + end_date + "'><input type='hidden' name='avaialble_time_id' value='" + available_time_id + "'>");
            $(".picker").hide("fast");
            $(".summarized").show("fast");

            return false;
        });


        //get date change event
        $("#doctorTimings").on("dp.change", function (e) {
            if (!e.date) {
                return;
            }
            date_time = e.date.toObject();
            step_1_ok = false;
            next_step_ok = false;
            //now grab all plans for this date
            var month = date_time.months + 1;
            if (month < 10) {
                month = '0' + month.toString();
            }
            var day = date_time.date;
            if (day < 10) {
                day = '0' + day.toString();
            }
            var id = $('#doctor_id').val();
            var s_date = date_time.years + '-' + month + '-' + day;
            var m_date = month;
            loadAvailableTiming(id, s_date, m_date);

        });

        $("#appointment-sum_edit").click(function () {
            $(".picker").show("fast");
            $(".summarized").hide("fast");
            $(".edit_date").html("");
        });

        <?php  if( isset($doctors) && !empty($doctors) ): ?>
        //search for patients
        var bind_to = ':input.search_patient';
        $('#patientMakeAppointment').off('keyup', bind_to);
        $('#patientMakeAppointment').on('keyup', bind_to, function (event) {
            //alert($(this).val().length);
            step_1_ok = false;
            var th = $(this);
            var length = th.val().length;
            var text = th.val();
            if (length > 2) {
                //search patient by name via ajax
                get_patient_ajax('search', text);
            }
            else if (length == 0 || length == '') {
                //get latest four patient
                get_patient_ajax('four', text);
            }
        });

        function get_patient_ajax(type, text) {
            if (type == 'four') {
                //get latest four
                var url = '<?php echo base_url() . 'PatientAjax/ajax_get_patient_doctors';  ?>';
                var data = {};

            } else {
                //get all
                var url = '<?php echo base_url() . 'PatientAjax/ajax_search_patient_doctors' ?>';
                var data = {search: text};
            }

            //2. show loading state
            var str = '<i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom"></i> \
                <span class="sr-only">Searching...</span> Searching for doctor. Please wait...';
            $('#patient_listing').html(str);


            //$('.picker-slots').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i> Loading....');

            $.ajax({
                method: "POST",
                url: url,
                data: data,
                dataType: "html",
                //async: true,
            }).success(function (response, status) {
                //alert(data);
                $("#patient_listing").html(response);
            }).done(function () {

            });
        }

        //select a patient
        //
        $('#patientMakeAppointment').on('click', '.select_patient', function (e) {
            e.preventDefault();

            step_1_ok = false;

            var th = $(this).children('a');
            var image_url = th.data('image_url');
            var name = th.data('patient_name');
            var pid = th.data('patient_id');
            var did_stripe = th.data('did_stripe');
            var image = '<img src="' + image_url + '" />';
            var details = '<span>' + th.data('patient_details') + '</span>';

            $("#doctorTimings").trigger('dp.change');
            $('#doctor_id').val(pid);
            $('#did_stripe').val(did_stripe);
            $('.di-user-main').html(image).removeClass('unselect');
            $('.di-detail-main').html(name + details);
            $('#patient_collapse').trigger('click');

            //Stripe.setPublishableKey(did_stripe);

            //get doctor available times
            //#picker_slots
            var str = '<div class="alert alert-info" role="alert"><i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom"></i> \
                <span class="sr-only">Loading...</span> Getting available timings...</div>';
            $('.picker-slots').html(str);

            $.ajax({
                method: "POST",
                url: '<?php echo base_url() . 'PatientAjax/ajax_get_doctor_available_times/' ?>' + pid,
                dataType: "html",
                //async: true,
            }).success(function (response, status) {

                var dates = jQuery.parseJSON(response);

                //destroy old datetime picker
                $('#doctorTimings').data("DateTimePicker").destroy();

                //initialize bootstrap datetime picker
                $('#doctorTimings').datetimepicker({
                    viewMode: 'days',
                    showClear: true,
                    useCurrent: false,
                    minDate: '<?php echo date('m/d/Y', time()); ?>',
                    format: 'MM/DD/YYYY',
                    enabledDates: dates
                });

            }).done(function () {
                $('.picker-slots').html('');
            });


            //check billing waived or not
            //call ajax
            $.ajax({
                method: "POST",
                url: '<?php echo base_url() . 'PatientAjax/ajax_check_patient_waived/' ?>' + pid,
                dataType: "json",
                //async: true,
            }).success(function (response, status) {

                //fee_waived
                if (response.waived && response.waived > 0) {
                    $('#fee_waived').val(response.waived);
                    $('#no_payment_div').show();
                    $('#payment_div').hide();
                    $('#popup-3-next').val('Next');
                    $('#billing_waived_div').hide();
                }
                else {
                    $('#fee_waived').val(0);
                    $('#no_payment_div').hide();
                    $('#payment_div').show();
                    $('#popup-3-next').val('Proceed');
                    $('#billing_waived_div').show();
                    get_doctor_consultation_type(pid);
                }

            }).done(function () {

            });

        });

        function get_doctor_consultation_type(pid) {

            //remove option
            $('#consultation_type').find('option').remove();

            //get available Consultation Type
            $.ajax({
                method: "POST",
                url: '<?php echo base_url() . 'PatientAjax/ajax_get_doctor_consultation_types/' ?>' + pid,
                dataType: "json",
                //async: true,
            }).success(function (response, status) {

                //var dates = jQuery.parseJSON(response);
                if( response.data ) {

                    $.each(response.data, function(index, value){
                        var val1 = value.id;
                        var val2 = value.duration + ' minutes : $' + value.rate;
                        $('#consultation_type')
                            .append($("<option></option>")
                                .attr("value",val1)
                                .text(val2));
                    });
                }

            }).done(function () {
                $('.picker-slots').html('');
            });
        }

        <?php endif; ?>

        //now add new appointment
        $('#patientMakeAppointment').on('click', '.step_1_next', function (e) {
            //check for valid inputs
            e.preventDefault();
            if (step_1_ok == true) {
                return;
            }

            //check for doctor
            var doctor_id = $('#doctor_id').val();
            if (doctor_id <= 0 || doctor_id == undefined) {
                notyError('Please select a doctor first.');
                return false;
            }

            //check doctor stripe setup
            var doctor_stripe = $('#did_stripe').val();
            console.log(doctor_stripe);
            if (doctor_stripe == '') {
                notyError('The selected doctor has not configured his payment settings. Please select another doctor or notify your doctor to setup his payment information.');
                return false;
            }
            else {
                //set stripe api key for doctor
                Stripe.setPublishableKey(doctor_stripe);
            }

            //check consultation type
            <?php if (!isset($appointment)): ?>
            var consultation_type = $('#consultation_type').val();
            var fee_waived = $('#fee_waived').val();

            if ( fee_waived == '0' && (consultation_type == '' || consultation_type == null) ) {
                notyError('The selected doctor has not defined a consultation type. Please select another doctor or notify your doctor to setup his consultation type.');
                return false;
            }
            <?php endif; ?>


            //check reason
            var appdt = $(".selected_date").val();

            if (appdt == undefined || appdt == '') {
                notyError('Please select appointment time.');
                return false;
            }

            if ($('#reason_id').val() == '') {
                notyError('Please select a reasons for appointment.');
                return false;
            }

            if ($('#reason_id').val() == '13' && $('#reason_textbox').val() == '') {
                notyError('Please enter other reasons for appointment.');
                return false;
            }

            //now everything is ok, send ajax request to make appointment
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a")
            nextStepWizard.removeAttr('disabled').trigger('click');

        });


        $('#patientMakeAppointment').on('click', '.step_2_next', function (e) {
            //$("#popup-2-next").click(function (e) {
            //concent to treat

            e.preventDefault();


            if (step_2_ok == true) {
                return;
            }

            next_step_ok = false;
            step_2_ok = false;

            var th = $(this);


            if ($('#acknowledge_check').prop("checked") == false) {
                var str = 'Please acknowledge and agree to the Terms and Conditions and Consent to Treatment stated below.';
                notyError(str);
            } else {
                var curStep = th.closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a")
                nextStepWizard.removeAttr('disabled').trigger('click');

            }

            /*

            //get doctor waived
            var doctor_id = $('#doctor_id').val();

            //call ajax
            $.ajax({
                method: "POST",
                url: '<?php echo base_url() . 'PatientAjax/ajax_check_patient_waived/' ?>' + doctor_id,
                dataType: "json",
                //async: true,
            }).success(function (response, status) {

                //fee_waived
                if (response.waived && response.waived > 0) {
                    $('#fee_waived').val(response.waived);
                    $('#no_payment_div').show();
                    $('#payment_div').hide();
                    $('#popup-3-next').val('Next');
                    $('#billing_waived_div').hide();
                }
                else {
                    $('#fee_waived').val(0);
                    $('#no_payment_div').hide();
                    $('#payment_div').show();
                    $('#popup-3-next').val('Proceed');
                    $('#billing_waived_div').show();
                }

            }).done(function () {
                if ($('#acknowledge_check').prop("checked") == false) {
                    var str = 'Please acknowledge and agree to the Terms and Conditions and Consent to Treatment stated below.';
                    notyError(str);
                } else {
                    var curStep = th.closest(".setup-content"),
                        curStepBtn = curStep.attr("id"),
                        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a")
                    nextStepWizard.removeAttr('disabled').trigger('click');

                }
            });
            */
        });

        $('#patientMakeAppointment').on('click', '.step_3_next', function (e) {
            //$('#popup-3-next').click(function(e){
            e.preventDefault();

            var fee_waived = $('#fee_waived').val();

            if (next_step_ok === true) {
                return;
            }
            //disable buttons
            $('.step_3_next').prop('disabled', true);

            //show modal with wait message
            var str = '<div class="row" role="alert"><i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> \
                <span class="sr-only">Loading...</span> Please wait...</div>';
            $('#payment_error').html(str).show();


            if (fee_waived > 0) {
                //call ajax and make the database entry
                var url = '<?php

                    if (isset($appointment)) {
                        echo base_url() . 'appointment/ajax_edit_appointment';
                    } else {
                        echo base_url() . 'appointment/ajax_create_new';
                    }

                    ?>';

                var data = $('#payment-form').serializeArray(); // convert form to array
                data.push({name: "no_token", value: 'yes'});

                <?php
                if ( isset($appointment) ) { ?>

                data.push({name: "appointment_id", value: '<?php echo $appointment->id; ?>'});
                <?php
                }
                ?>

                $.ajax({
                    url: url,
                    data: data,
                    method: "POST",
                    dataType: "json"
                }).error(function (err) {
                    //enable buttons
                    notyError(err.responseText);

                    $('#popup-3-cancel').prop('disabled', false);
                    $('#popup-3-next').prop('disabled', false);
                    console.log('Error: ', err);

                }).done(function () {
                    $('#popup-3-next').prop('disabled', false);

                }).success(function (data) {
                    //console.log(data);
                    if (data.success) {
                        final_done = true;
                        $('#payment_error').html('').hide();

                        if (data.message) {
                            $.each(data.message, function (index, msg) {
                                notySuccess(msg);
                            });
                        }

                        //reset form
                        resetForm($('#payment-form'));

                        var curStep = $('.step_3_next').closest(".setup-content"),
                            curStepBtn = curStep.attr("id"),
                            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a")
                        nextStepWizard.removeAttr('disabled').trigger('click');

                        setTimeout(function () {
//                            location.reload();
                            var url = '<?php echo base_url() . 'patient/appointments'; ?>';
                            window.location = url;
                        }, 10000);


                    } else if (data.error) {
                        $.each(data.error, function (index, msg) {
                            notyError(msg)
                        });
                    }
                }, 'json');
            }
            else {
                ////now get stripe token
                console.log('here at stripe payment processing');
                var $form = $('#payment-form');
                Stripe.card.createToken($form, stripeResponseHandler);
            }

        });

        function stripeResponseHandler(status, response) {
            // Grab the form:
            var $form = $('#payment-form');
            $('#payment_error').html('');

            if (response.error) { // Problem!

                // Show the errors on the form:
                notyError(response.error.message);
                //$('#payment_error').html('<div class="alert alert-danger" role="alert">' + response.error.message + '</div>').show();
                //enable buttons
                $('#popup-3-cancel').prop('disabled', false);
                $('#popup-3-next').prop('disabled', false);

            } else { // Token was created!

                // Get the token ID:
                var token = response.id;
                var data = $('#payment-form').serializeArray(); // convert form to array
                data.push({name: "stripeToken", value: token});

                <?php
                if ( isset($appointment) ) { ?>

                data.push({name: "appointment_id", value: '<?php echo $appointment->id; ?>'});
                <?php
                }
                ?>

                //show modal with processing message
                var str = '<div class="row"><i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> \
                <span class="sr-only">Loading...</span> Please wait, while we are processing your request...</div>';
                $('#payment_error').html(str).show();

                var url = '<?php

                    if (isset($appointment)) {
                        echo base_url() . 'appointment/ajax_edit_appointment';
                    } else {
                        echo base_url() . 'appointment/ajax_create_new';
                    }

                    ?>';

                //ajax call
                $.ajax({
                    url: url,
                    //data: $.param(data),
                    data: data,
                    method: "POST",
                    dataType: "json"
                }).error(function (err) {
                    //enable buttons
                    notyError(err.responseText);

                    $('#popup-3-cancel').prop('disabled', false);
                    $('#popup-3-next').prop('disabled', false);
                    console.log('Error: ', err);

                }).done(function () {
                    $('#popup-3-next').prop('disabled', false);

                }).success(function (data) {
                    //console.log(data);
                    if (data.success) {
                        final_done = true;
                        $('#payment_error').html('').hide();

                        if (data.message) {
                            $.each(data.message, function (index, msg) {
                                notySuccess(msg);
                            });
                        }

                        //reset form
                        resetForm($('#payment-form'));

                        var curStep = $('.step_3_next').closest(".setup-content"),
                            curStepBtn = curStep.attr("id"),
                            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a")
                        nextStepWizard.removeAttr('disabled').trigger('click');

                        setTimeout(function () {
                            var url = '<?php echo base_url() . 'patient/appointments'; ?>';
                            window.location = url;
                            //location.reload();
                        }, 10000);


                    } else if (data.error) {
                        $.each(data.error, function (index, msg) {
                            notyError(msg)
                        });
                    }
                }, 'json');
            }
        }


        function resetForm($form) {
            $form.find('input:text, input:hidden, input:password, input:file, select, textarea').val('');
            $form.find('input:radio, input:checkbox')
                .removeAttr('checked').removeAttr('selected');

            //reset patient list
            $('#patient_listing').html('');
        }

    });
</script>

<script type="text/javascript">
    $(function () {
        $('#doctorTimings_field').datetimepicker({
            viewMode: 'days',
            format: 'MM/DD/YYYY'
        });
    });
</script>