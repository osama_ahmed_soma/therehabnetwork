<h1>My Virtual Doctor Consultation Report</h1>
<hr style="margin-top: 0px"/>

<table>
    <tr>
        <td style="width: 50%">
            <div>
                <div><strong>Patient: <?php echo $appointment->firstname . ' ' .$appointment->lastname; ?></strong></div>
                <div><?php echo $appointment->address; ?></div>
                <div>Home: <?php echo $appointment->phone_home ? $appointment->phone_home : 'N/A' ?></div>
                <div>Phone: <?php echo $appointment->phone_mobile; ?></div>
            </div>
        </td>
        <td valign="top">
            <div class="col-md-6">
                <div><strong>Encounter Date:</strong> <?php echo date("jS F, Y g:i A", strtotime($appointment->start_date)); ?></div>
                <div><strong>Clinician:</strong> <?php echo $doctor->firstname . ' ' . $doctor->lastname ?></div>
                <div><strong>Consultation Duration:</strong> <?php echo gmdate("H:i:s", $appointment->duration); ?></div>
            </div>
        </td>
    </tr>
</table>

<h4>Patient Statistic</h4>
<hr style="margin-top: 0px"/>
<table width="80%">
    <tr>
        <td style="padding-bottom: 20px; width: 50%"><strong>Age: </strong><span><?php echo $patient->age?> Years</span></td>
        <td style="padding-bottom: 20px; width: 50%"><strong>Gender: </strong><span><?php echo $patient->gender == 1 ? 'Male' : 'Female' ?></span></td>
    </tr>
    <tr>
        <td style="padding-bottom: 20px; width: 50%"><strong>Height: </strong><span><?php echo $patient->height_ft ?>ft <?php echo $patient->height_inch ?>in</span></td>
        <td style="padding-bottom: 20px; width: 50%"><strong>Weight: </strong><span><?php echo $patient->weight ?>lbs</span></td>
    </tr>
</table>

<h4>Primary Concern</h4>
<hr style="margin-top: 0px"/>
<div><?php echo $appointment->reason ?></div>

<h4>Secondary Concern</h4>
<hr style="margin-top: 0px"/>
<div><?php echo $appointment->notes ? $appointment->notes : 'N/A'; ?></div>

<h4>Subjective</h4>
<hr style="margin-top: 0px"/>
<div><?php echo $appointment->subjective ? $appointment->subjective : 'N/A' ?></div>

<h4>Objective</h4>
<hr style="margin-top: 0px"/>
<div><?php echo $appointment->objective ? $appointment->objective : 'N/A' ?></div>

<h4>Assessment</h4>
<hr style="margin-top: 0px"/>
<div><?php echo $appointment->assessment ? $appointment->assessment : 'N/A' ?></div>

<h4>Plan</h4>
<hr style="margin-top: 0px"/>
<div><?php echo $appointment->plan ? $appointment->plan : 'N/A' ?></div>