<?php /** @var $patient */?>
<?php /** @var $appointment */?>
<?php /** @var array $doctorFiles */?>
<?php /** @var array $patientFiles */?>

<script src="<?php echo base_url(); ?>template_file/js/lib/tinymce/tinymce.min.js"></script>
<div class="patient-data__userinfo">
    <div class="userpic userpic--patient-data userpic--white-border">
        <img src="<?php echo default_image($patient->image_url); ?>" alt="<?php echo $patient->firstname ?>" title="<?php echo $patient->firstname ?>">
    </div>
    <div class="patient-data__name">
        <span class="patient-data__firstname"><?php echo $patient->firstname ?></span>
        <span class="patient-data__lastname"><?php echo $patient->lastname ?></span>
    </div>
    <div class="patient-data__secondary-info" >
        <span class="patient-data__gender"><?php echo $patient->gender ?></span>
        <span class="patient-data__age"><?php echo $patient->age ?></span>
        <span class="patient-data__phone"><?php echo $patient->phone_mobile?></span>
    </div>
</div>

<div class="k-tabstrip-wrapper"><div class="consult-screen__consultation-tabs k-widget k-header k-tabstrip k-floatwrap k-tabstrip-top k-tabstrip-scrollable" id="clinician-consultation-tabs" data-role="tabstrip" tabindex="0" role="tablist">

        <ul class="k-tabstrip-items k-reset" role="tablist"><li class="intakeel k-state-active k-item k-tab-on-top k-state-default k-first" role="tab" data-target="clinician-consultation-tabs-1" data-toggle="tab"><span class="k-loading k-complete"></span>
                <span class="k-link"><span class="icon icon_intake"></span><span class="text">Intake Form</span></span>

            </li><li class="userprofileel k-item k-state-default" role="tab" data-target="clinician-consultation-tabs-2" data-toggle="tab"><span class="k-loading k-complete"></span>
                <span class="k-link"><span class="icon icon_user"></span><span class="text">Profile</span></span>

            </li><li class="sopatab k-state-disabled k-item"  role="tab" data-target="clinician-consultation-tabs-4"><span class="k-loading k-complete"></span>
                <span class="k-link"><span class="icon icon_note"></span><span class="text">S.O.A.P.</span></span>

            </li><li class="filestab k-item"  role="tab" data-target="clinician-consultation-tabs-5"><span class="k-loading k-complete"></span>
                <span class="k-link"><span class="icon icon_archive"></span><span class="text">Files</span></span>

            </li></ul>

        <div class="patient-data patient-data__intake k-content k-state-active" id="clinician-consultation-tabs-1" role="tabpanel" aria-expanded="true" style="display: block;">
            <ul class="patient-data__list">
                <li>
                    <h3>Primary Concern</h3>
                    <p><?php echo $appointment->reason_id == '13' ?  $appointment->reason : $appointment->reason_name; ?></p>
                </li>
                <li>
                    <h3>Secondary Concern</h3>
                    <p><?php echo $appointment->notes ? $appointment->notes : 'N/A'; ?></p>
                </li>
            </ul>

            <div class="patient-data__info-block">
                <h2 class="patient-data__title patient-data__title--large">Medical History</h2>

                <h3 class="patient-data__subtitle patient-data__subtitle--consult">Chronic Conditions</h3>
                <ul class="patient-data__list patient-data__list--consult" style="display: none;"></ul>

                <ul class="patient-data__list patient-data__list--consult patient-data__list--empty">
                    <?php if (count($patient->health)) { ?>
                        <?php foreach ($patient->health as $health) { ?>
                            <li><?php echo $health->condition_term ?></li>
                        <?php } ?>
                    <?php } else { ?>
                        <li>None Reported</li>
                    <?php } ?>
                </ul>

                <div style="display: none;">
                    <h2 class="patient-data__subtitle patient-data__subtitle--consult">Birth History</h2>
                    <ul class="patient-data__list patient-data__list--consult">
                        <li>
                            <div class="patient-data__list-title">Was the child born at full term?</div>

                            <div class="patient-data__list-description" data-template="yesno-template" data-bind="source:intakeForm.isChildBornFullTerm">

                                No

                            </div>
                        </li>
                        <li>
                            <div class="patient-data__list-title">Was the child born vaginally?</div>

                            <div class="patient-data__list-description" data-template="yesno-template" data-bind="source:intakeForm.isChildBornVaginally">

                                No

                            </div>
                        </li>
                        <li>
                            <div class="patient-data__list-title">Was the child born discharged with the mother?</div>

                            <div class="patient-data__list-description" data-template="yesno-template" data-bind="source:intakeForm.isChildDischargeMother">

                                No

                            </div>
                        </li>
                        <li>
                            <div class="patient-data__list-title">Was the child's vaccination up-to-date?</div>

                            <div class="patient-data__list-description" data-template="yesno-template" data-bind="source:intakeForm.isVaccinationUpToDate">

                                No

                            </div>
                        </li>
                    </ul>
                </div>

                <h2 class="patient-data__subtitle patient-data__subtitle--consult">Surgeries</h2>
                <ul class="patient-data__list patient-data__list--consult" data-template="intakeItem_surgery" data-bind="source:intakeForm.priorSurgeories,visible:isPriorSurgeoriesAvailable" style="display: none;"></ul>

                <ul class="patient-data__list patient-data__list--consult patient-data__list--empty">
                    <?php if (count($patient->surgeries)) { ?>
                        <?php foreach ($patient->surgeries as $surgery) { ?>
                            <li><?php echo $surgery->condition_term ?></li>
                        <?php } ?>
                    <?php } else { ?>
                        <li>None Reported</li>
                    <?php } ?>
                </ul>

                <h2 class="patient-data__subtitle patient-data__subtitle--consult">Medication Allergies</h2>
                <ul class="patient-data__list patient-data__list--consult" style="display: none;"></ul>

                <ul class="patient-data__list patient-data__list--consult patient-data__list--empty">
                    <?php if (count($patient->allergies)) { ?>
                        <?php foreach ($patient->allergies as $surgery) { ?>
                            <li><?php echo $surgery->condition_term ?></li>
                        <?php } ?>
                    <?php } else { ?>
                        <li>None Reported</li>
                    <?php } ?>
                </ul>

                <h2 class="patient-data__subtitle patient-data__subtitle--consult">Current Medications</h2>

                <ol class="patient-data__list patient-data__list--consult patient-data__list--empty">
                    <?php if (count($patient->medication)) { ?>
                        <?php foreach ($patient->medication as $surgery) { ?>
                            <li><?php echo $surgery->condition_term ?></li>
                        <?php } ?>
                    <?php } else { ?>
                        <li>None Reported</li>
                    <?php } ?>
                </ol>
            </div>
        </div>

        <div class="patient-data patient-data__patient-profile k-content" id="clinician-consultation-tabs-2" role="tabpanel" aria-hidden="true" aria-expanded="false" >
            <div class="patient-data__info-block patient-data__info-block--small-block">
                <div class="patient-data__header patient-data__header--patient-details">Age</div>
                <div class="patient-data__icon">
                    <span class="icon_hash"></span>
                </div>
                <div class="patient-data__data">
                    <span>
                        <div data-bind="html:patientInfomation.age"><?php echo $patient->age?></div>

                        <div class="dob">DOB: <span><?php echo $patient->dob?></span></div>
                    </span>
                </div>
            </div>
            <div class="patient-data__info-block patient-data__info-block--small-block">
                <div class="patient-data__header patient-data__header--patient-details">Gender</div>
                <div class="patient-data__icon">
                    <span class="icon_<?php echo $patient->gender == 'M' ? 'male' : 'female' ?>"></span>
                </div>
                <div class="patient-data__data"><span data-bind="html:patientInfomation.gender"><?php echo $patient->gender; ?></span></div>
            </div>
            <div class="patient-data__info-block patient-data__info-block--small-block">
                <div class="patient-data__header patient-data__header--patient-details">Height</div>
                <div class="patient-data__icon">
                    <span class="icon_height"></span>
                </div>
                <div class="patient-data__data"><span><?php echo $patient->height_ft ?>ft <?php echo $patient->height_inch ?>in</span></div>
            </div>
            <div class="patient-data__info-block patient-data__info-block--small-block">
                <div class="patient-data__header patient-data__header--patient-details">Weight</div>
                <div class="patient-data__icon">
                    <span class="icon_weight"></span>
                </div>
                <div class="patient-data__data"><span><?php echo $patient->weight ?>lbs</span></div>
            </div>
            <!--<div class="patient-data__info-block patient-data__info-block--small-block">
                <div class="patient-data__header patient-data__header--patient-details">Eye Color</div>
                <div class="patient-data__icon">
                    <span class="icon_eye"></span>
                </div>
                <div class="patient-data__data"><span><?php /*echo $patient->eye_color*/?></span></div>
            </div>-->
            <!--<div class="patient-data__info-block patient-data__info-block--small-block">
                <div class="patient-data__header patient-data__header--patient-details">Hair Color</div>
                <div class="patient-data__icon">
                    <span class="icon_hair"></span>
                </div>
                <div class="patient-data__data"><span><?php /*echo $patient->hair_color*/?></span></div>
            </div>-->
            <!--<div class="patient-data__info-block patient-data__info-block--small-block">
                <div class="patient-data__header patient-data__header--patient-details">Ethnicity</div>
                <div class="patient-data__icon">
                    <span class="icon_globe"></span>
                </div>
                <div class="patient-data__data"><span><?php /*echo $patient->ethnicity; */?></span></div>
            </div>-->
            <!--<div class="patient-data__info-block patient-data__info-block--small-block">
                <div class="patient-data__header patient-data__header--patient-details">Blood Type</div>
                <div class="patient-data__icon">
                    <span class="icon_drop"></span>
                </div>
                <div class="patient-data__data"><span><?php /*echo $patient->blood_type*/?></span></div>
            </div>-->

            <div class="patient-data__info-block">
                <div class="patient-data__title patient-data__title--large">Contact Info</div>

                <div class="patient-data__info-block--large-block">
                    <dl class="details-list">
                        <dt class="details-list__title">Address:</dt>
                        <dd class="details-list__description"><address><?php echo $patient->address ?>,
                                <?php echo $patient->city?>, USA</address></dd>

                        <dt class="details-list__title">Home Phone:</dt>
                        <dd class="details-list__description"><?php echo $patient->phone_home ?></dd>

                        <dt class="details-list__title">Cell Phone:</dt>
                        <dd class="details-list__description"><?php echo $patient->phone_mobile ?></dd>

                        <dt class="details-list__title details-list__title--small">Email:</dt>
                        <dd class="details-list__description details-list__description--large"><a href="mailto:<?php echo $patient->email ?>"><?php echo $patient->email ?></a></dd>
                    </dl>
                </div>
            </div>
        </div>


        <div class="patient-data patient-data__message k-content" id="clinician-consultation-tabs-3" role="tabpanel">


            <section id="chatCont" class="chat chat--admin" style="max-width:99.59%;">

                <div class="chat__messages-list">
                    <div class="chat__messages-container js-chat-container" id="msgCont">

                    </div>
                </div>

                <div class="chat__controls chat__controls--upload">
                    <div class="chat__input">
                        <button type="button" class="chat__upload">
                            <span class="icon_arrow-with-circle-up"></span>
                        </button>

                        <input type="text" id="chatMessageCtl" data-bind="onEnterKey: onKeyUp" placeholder="Type a message here." class="field js-message">
                    </div>
                </div>

            </section>
        </div>


        <div class="patient-data patient-data__soap k-content" id="clinician-consultation-tabs-4" role="tabpanel" aria-hidden="true" aria-expanded="false">
            <div class="patient-data__info-block">
                <div class="patient-data__title patient-data__title--large">Soap Note</div>

                <form id="soapnotes-form">
                    <div id="soap-s" class="patient-data__soap-content">
                        <h4>Subjective</h4>
                        <textarea id="redactor-subjective" name="subjective" class="k-editor"><?php echo $appointment->subjective;?></textarea>

                        <div class="intake-data">
                            <h4>Primary Concern:</h4>
                            <p id="primaryConcern"><?php echo $appointment->reason?></p>
                            <h4>Secondary Concern:</h4>
                            <p id="SecondaryConcern"><?php echo $appointment->notes ? $appointment->notes : 'N/A'; ?></p>
                        </div>
                        <div id="soap-o" class="patient-data__soap-content">
                            <h4>Objective</h4>
                            <textarea id="redactor-objective" name="objective" class="k-editor"><?php echo $appointment->objective;?></textarea>

                            <div class="intake-data">
                                <h4>Medication Allergies:</h4>
                                <p id="MedicationAllergies">
                                    <?php if (count($patient->allergies)) { ?>
                                        <?php foreach ($patient->allergies as $surgery) { ?>
                                            <?php echo $surgery->condition_term ?>,
                                        <?php } ?>
                                    <?php } else { ?>
                                        None
                                    <?php } ?>
                                </p>
                                <h4>Current Medications:</h4>
<!--                                <p id="CurrentMedications" data-bind="html:intakeForm.takingMedicationsInfo">None</p>-->
                                <p id="CurrentMedications" data-bind="html:intakeForm.takingMedicationsInfo">
                                    <?php if (count($patient->medication)) { ?>
                                        <?php foreach ($patient->medication as $surgery) { ?>
                                            <?php echo $surgery->condition_term ?>,
                                        <?php } ?>
                                    <?php } else { ?>
                                        None
                                    <?php } ?>
                                </p>
                            </div>
                        </div>
                        <div id="soap-a" class="patient-data__soap-content">
                            <h4>Assessment</h4>
                            <textarea id="redactor-assessment" name="assessment" class="k-editor"><?php echo $appointment->assessment;?></textarea>
                        </div>
                        <div id="soap-p" class="patient-data__soap-content">
                            <h4>Plan</h4>
                            <textarea id="redactor-plan" name="plan" class="k-editor"><?php echo $appointment->plan;?></textarea>
                        </div>

                        <button id="btnGo" class="btn btn-primary button__brand"><span>Submit</span></button>
                    </div>
                </form>
            </div>

        </div>

        <div class="patient-data patient-data__files k-content" id="clinician-consultation-tabs-5" role="tabpanel" aria-hidden="true" aria-expanded="false">
            <div id="fileSharingSection"><div id="file-sharing">

                    <div id="splitter">
                        <div class="pane-2 patientFiles overlay" id="bottomFiles" style="min-height: 470px;position:relative;">

<!--                            <div class="heading">
                                <span class="icon_archive"></span>
                                <span class="filesinfo">
                                    <span>Patient Files</span>
                                </span>
                                <span class="uploadFiles icon_upload-cloud" id="bottomUploadFiles">Upload Files</span>
                            </div>-->

                            <div class="k-widget k-upload k-header k-upload-empty" style="display: none;">
                                <div class="k-dropzone icon_upload-cloud">
                                    <div class="k-button k-upload-button">
                                        <input name="cloudfilesPatient" id="cloudfilesPatient" type="file" data-role="upload" multiple autocomplete="off">
                                        <span>Select files...</span>
                                    </div>
                                    <em>drop files here to upload</em>
                                </div>
                            </div>

                            <ul class="breadcrumbs" data-template="breadcrumb-template" data-bind="source: parents">
                                <li data-bind="attr: {class: breadCrumbColor}" class="last">
                                    <a href="javascript:void(0)" class="last">Patient Home</a>
                                </li>
                            </ul>
                            <div class="scrollable-area patientArea" style="height: 400px; position: relative; width: 100% !important; overflow: auto;">
                                <?php if (count($patientFiles)) { ?>
                                    <table id="bottomTable" class=" table table-bordered table-hover" cellspacing="0" cellpadding="0" data-role="draggable">
                                        <thead>
                                        <tr style="background-color: #EAEAEA;">
                                            <th>File Name</th>
                                            <th>Shared Date</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead><tbody>

                                        <?php foreach ($patientFiles as $patientFile) {
											$type = pathinfo($patientFile->filename, PATHINFO_EXTENSION);
											$icons = $this->config->item('file_icons');
											?>
                                            <tr >
                                                <td><i class="<?php echo (isset($icons[$type])) ? $icons[$type] : $icons['default']; ?>"></i> <a title="Download" target="_blank" href="<?php echo base_url('file/download_document/' . $patientFile->id . '/' . $patient->id); ?>"><?php echo $patientFile->title; ?></td>
                                                <td><?php echo date('m/d/Y', strtotime($patientFile->date_time))?></td>
                                                </td>
                                                <td class="action text-center">
													<a title="Download" class="btn btn-sm btn-primary" target="_blank" href="<?php echo base_url('file/download_document/' . $patientFile->id . '/' . $patient->id); ?>"><span><i class="fa fa-download"></i></span></a>
                                                </td> 
                                           </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                <?php } else { ?>
                                    <div class="no-data" style="text-align: center; font-weight: bold;">This folder is empty.</div>
                                <?php } ?>
                            </div>
                            <div class="listViewColumn" style="display: none;">
                                <div class="listView"> </div>
                                <div class="pager k-pager-wrap"> </div>
                            </div><!-- / .listViewColumn -->
                        </div><!-- / .pane-2 -->

                        <div id="topFiles" class="pane-1 clinicianFiles overlay" style="min-height: 470px;position:relative;">
                            <div class="heading">
                                <section id="filesInfo">
                                    <span class="icon_archive"></span>
                                    <span class="filesinfo">
                                        <span class="big">My Files</span>
                                    </span>
                                    <!--<span class="uploadFiles icon_upload-cloud" id="topUploadFiles">Upload Files</span>-->
                                </section>
                            </div>

                            <div class="k-widget k-upload k-header k-upload-empty" style="display: none;"><div class="k-dropzone icon_upload-cloud">
                                <div class="k-button k-upload-button">
                                    <input name="cloudfilesClinician" id="cloudfilesClinician" type="file" data-role="upload" multiple autocomplete="off">
                                    <span>Select files...</span>
                                </div>
                                <em>drop files here to upload</em>
                            </div></div>

                            <ul class="breadcrumbs" data-template="breadcrumb-template" data-bind="source: parents">
                                <li class="last">
                                    <a href="javascript:void(0)" data-bind="text: name, click: drill, attr: {class: breadCrumbColor}" class="last">Home</a>
                                </li>
                            </ul>
                            <div class="scrollable-area myArea" style="height: 400px; position: relative; width: 100% !important;  overflow: auto; ">

                                <?php if (count($doctorFiles)) { ?>
                                    <table id="topTable" class="filesTable" cellpadding="0" cellspacing="0" data-role="draggable">
                                        <thead>
                                        <tr style="background-color: #EAEAEA;">
                                            <th class="bulkAction" style="text-align: center; width: 80px;">
                                                <ul class="actions" style="display: none">
                                                    <li>
                                                        <span class="toggle" id="bulkMenuTop" data-bind="click: openTopBatchMenu"></span>
                                                        <ul id="menuTop" style="left: -5px;">
                                                            <li><span class="icon_eye"></span>View File</li>
                                                            <li><span class="icon_download"></span>Download File</li>
                                                            <li><span class="icon_link"></span>Public Link</li>
                                                            <li><span class="icon_info2"></span>File Properties</li>
                                                            <li><span>content</span>Copy/Move</li>
                                                            <li><span class="icon_new-folder"></span>Add Folder</li>
                                                            <li><span class="icon_trash"></span>Delete File</li>
                                                            <li><span class="icon_attachment"></span>Attach to Consult</li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <span class="actions-title" style="display: none">Actions</span>
                                            </th>
                                            <th class="name topName"><span class="" data-parameter="topName" data-bind="click: sort">File Name</span></th><!-- sort active ascending -->
                                            <th class="date topDate"><span class="" data-parameter="topDate" data-bind="click: sort">Uploaded</span></th>
                                            <th class="tags"><span>Tags</span></th>
                                            <th class="actions"></th>
                                        </tr>
                                        </thead>
                                        <tbody data-template="file-list-template">


                                        <?php foreach ($doctorFiles as $doctorFile) { ?>

                                            <tr class="trFile" >
                                                <td>
                                                    <div class="" style="text-align: center;margin-left:0px" >
                                                        <img  alt="" src="https://myvirtualdoctor.connectedcare.md/images/filetypes/png/jpg.png" title="">
                                                    </div>
                                                </td>
                                                <td class="name name-wrap"><?php echo $doctorFile->filename ?></td>
                                                <td class="date"><?php echo $doctorFile->date_time ?></td>
                                                <td class="tag">
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <ul class="ulT" data-template="tag-li-template" data-bind="source: firstTwoTags"></ul>
                                                            </td>
                                                            <td style="padding-right:5px !important;">
                                                                <div class="nav2">
                                                                    <ul data-bind="visible: isTag2ToolTipVisible" style="display: none;">
                                                                        <li>
                                                                            <a class="fa" data-bind="text: tag2ToolTipNumber" href="javascript:void(0);">..0</a>
                                                                            <ul data-template="tag-li-tooltip-template" data-bind="source: tootTipTags"></ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="action" style="text-align: center;">
                                                    <ul class="actions">
                                                        <li>
                                                            <a href="javascript:void(0)" class="toggle icon_chevron-down" data-bind="click: openMenu"></a>
                                                            <ul class="ulMenu" data-bind="attr: {id: menuId}" id="menu_2437">
                                                                <li data-bind="visible: displayViewFile" data-parameter="display" style="display: none;"><span class="icon_eye"></span>View File</li>
                                                                <li data-bind="click: download, visible: displayDownload"><span class="icon_download"></span>Download File</li>
                                                                <li data-bind="click: share, visible: displaySend"><span class="icon_link"></span>Public Link</li>
                                                                <li data-bind="click: properties, visible: displayProperties"><span class="icon_info2"></span><span data-bind="text: displayTitle">File</span> Properties</li>
                                                                <li data-bind="click: copy, visible: displayCopy" data-parameter="top"><span class="icon_copy"></span>Copy To</li>
                                                                <li data-bind="click: mkdir, visible: displayAddFolder" style="display: none;"><span class="icon_new-folder"></span>Add Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFolder" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFile" data-parameter="top"><span class="icon_trash"></span>Delete File</li>
                                                                <li data-bind="click: attachToConsult, visible: displayAttachConsult" class="attached" style="display: none;"><span class="icon_attachment"></span>Attach to Consult</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                        <?php } ?>


                                        </tbody>
                                    </table>
                                <?php } else { ?>

                                    <div class="no-data" data-bind="visible: isEmpty" style="text-align: center; font-weight: bold; display: none;">This folder is empty.</div>
                                <?php } ?>

                            </div>
                            <div class="listViewColumn" style="display: none;">
                                <div class="listView"> </div>
                                <div class="pager k-pager-wrap"> </div>
                            </div><!-- / .listViewColumn -->

                            <script id="breadcrumb-template" type="text/x-kendo-template">
                                <li data-bind="attr: {class: breadCrumbColor}">
                                    <a href="javascript:void(0)" data-bind="text: name, click: drill, attr: {class: breadCrumbColor}"></a>
                                    <a data-bind="click: mkdir ,attr: {class: addFolderColor}"><span data-bind="attr:{class: addFolderColor}" title="Add Folder" style="line-height:31px"></span></a>
                                </li>
                            </script>

                            <script id="file-list-template" type="text/x-kendo-template">
                                <tr class='trFile' data-bind="attr:{ id:id}">
                                    <td data-bind="click: drill, events: { dblclick: download }">
                                        <div class='bulkWrap ' style='text-align: center;margin-left:0px' data-bind="attr: { class: Paperclip }">
                                            <!-- input type="checkbox" -->
                                            <img data-bind="attr: { src: fileIcon, title: fileType }" alt="">
                                        </div>
                                    </td>
                                    <td class='name name-wrap' data-bind="text: name, click: drill, events: { dblclick: download }"></td>
                                    <td class='date' data-bind="text: dateModifiedShort, click: drill, events: { dblclick: download }"></td>
                                    <td class='tag'>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <ul class="ulT" data-template="tag-li-template" data-bind="source: firstTwoTags"></ul>
                                                </td>
                                                <td style="padding-right:5px !important;">
                                                    <div class="nav2">
                                                        <ul data-bind="visible: isTag2ToolTipVisible">
                                                            <li>
                                                                <a class="fa" data-bind="text: tag2ToolTipNumber" href="javascript:void(0);"></a>
                                                                <ul data-template="tag-li-tooltip-template" data-bind="source: tootTipTags"></ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class='action' style='text-align: center;'>
                                        <ul class="actions">
                                            <li>
                                                <a href="javascript:void(0)" class="toggle icon_chevron-down" data-bind="click: openMenu"></a>
                                                <ul class="ulMenu" data-bind="attr: {id: menuId}">
                                                    <li data-bind="visible: displayViewFile" data-parameter="display"><span class="icon_eye"></span>View File</li>
                                                    <li data-bind="click: download, visible: displayDownload"><span class="icon_download"></span>Download File</li>
                                                    <li data-bind="click: share, visible: displaySend"><span class="icon_link"></span>Public Link</li>
                                                    <li data-bind="click: properties, visible: displayProperties"><span class="icon_info2"></span><span data-bind="text: displayTitle"></span> Properties</li>
                                                    <li data-bind="click: copy, visible: displayCopy" data-parameter="top"><span class="icon_copy"></span>Copy To</li>
                                                    <li data-bind="click: mkdir, visible: displayAddFolder"><span class="icon_new-folder"></span>Add Folder</li>
                                                    <li data-bind="click: delete, visible: displayDeleteFolder" data-parameter="top"><span class="icon_trash"></span>Delete Folder</li>
                                                    <li data-bind="click: delete, visible: displayDeleteFile" data-parameter="top"><span class="icon_trash"></span>Delete File</li>
                                                    <li data-bind="click: attachToConsult, visible: displayAttachConsult" class="attached"><span class="icon_attachment"></span>Attach to Consult</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </script>

                            <script id="tag-li-template" class="tag-li-template" type="text/x-kendo-template">
                                <li style="cursor:pointer;" class="liT" data-tag-tagtext="#= tagId #" data-bind="text: tag, events:{click: searchTag}"></li>
                            </script>

                            <script id="tag-li-tooltip-template" class="tag-li-tooltip-template" type="text/x-kendo-template">
                                <li><a href="javascript:void(0);" data-tag-tagtext="#= tagId #" data-bind="text: tag, events:{click: searchTag}" ></a></li>
                            </script>

                            <script type="text/x-kendo-template" id="tag-property-template" class="tag-property-template">
                                <li><span style="cursor:pointer;" data-tag-tagtext="#= tag #" data-bind="text: tag"></span>
                                    <a href="javascript:void(0);" class="remove" title="Remove" data-bind="click: removeTag" data-tag-id="#= tagFileId #">x</a>
                                </li>
                            </script>

                        </div><!-- / .pane-1 -->
                    </div> <!-- / #splitter -->











                    <script type="text/javascript">
                        $(function() {
                            $("#txtTag").keypress(function(e) {
                                var key = e.which ? e.which : e.keyCode;
                                if(key == 44){
                                    var btn = ".btn-ok-addTag";
                                    $(""+btn+"").focus();
                                    $(""+btn+"").click();

                                    event.preventDefault();
                                    return false;
                                }
                            });

                            $(".search-files").keyup(function (e) {
                                app.snapFileService.viewModel.search(e);
                            });

                            $(".search-files-bottom").keyup(function (e) {
                                app.snapFileService.bottomViewModel.search(e);
                            });

                            //Event fired when clearing text input on IE with clear icon
                            $('input[type="text"]').bind("mouseup", function (event) {
                                var $input = $(this);
                                var oldValue = $input.val();
                                if (oldValue == "") {
                                    return;
                                }
                                setTimeout(function () {
                                    var newValue = $input.val();
                                    if (newValue == "") {
                                        var enterEvent = $.Event("keyup");
                                        enterEvent.which = 13;
                                        $input.trigger(enterEvent);
                                    }
                                }, 1);
                            });

                            $("body").click(function(){
                                $(".ulMenu").hide();
                            });

                            // when the page is end .menu() open to top
                            $(".myArea .trFile").click(function(e){
                                e.stopPropagation();
                                var posY = $(this).offset().top;
                                var PY = 200, TP = -155;
                                var visibleMenuCount = $('ul.ulMenu > li:visible').length;


                                if( $(".attached").is(':visible')) {// || (snap.clinicialAppointment == true  && app.snapFileService.consultStatus == 1)){
                                    TP=-160;
                                    PY=225
                                }
                                if(! $(".ulMenu").hasClass("pic_menu")){
                                    PY=340,
                                        TP=-71
                                }

                                if ((e.pageY - posY) > PY && visibleMenuCount > 0) {
                                    $(".ulMenu").css("top",TP);
                                }else{
                                    $(".ulMenu").css("top",20);
                                }

                                $(this).find('.ulMenu').show();
                            });

                            $(".patientArea").click(function(e){

                                var posY = $(this).offset().top;
                                var PY=240,TP=-124;
                                var visibleMenuCount = $('ul.ulMenu > li:visible').length;

                                if($(".attached").is(':visible')){
                                    TP=-155;
                                }

                                if(! $(".ulMenu").hasClass("pic_menu")){
                                    PY=340,
                                        TP=-35
                                }

                                if ((e.pageY - posY) > PY && visibleMenuCount>0) {
                                    $(".ulMenu").css("top",TP);
                                }else{
                                    $(".ulMenu").css("top",20);
                                }
                            });

                        });
                    </script>
                </div><!-- / #file-sharing --></div>
        </div>
    </div></div>


<script>
    $(document).ready(function() {

        tinymce.init({
            content_css: '<?php echo base_url(); ?>template_file/css/tinymce.css',
            height: 190,
            format: 'html',
            selector: 'textarea.k-editor',
            toolbar: 'styleselect removeformat | bold italic underline | alignleft aligncenter alignright | bullist numlist outdent indent ',
            block_formats: 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3',
            menubar: false,
            statusbar: false
        });

        $('#btnGo').on('click', function(e) {
            e.preventDefault();

            tinymce.triggerSave();

            var data = {
                subjective: tinyMCE.get("redactor-subjective").getContent(),
                objective: tinyMCE.get("redactor-objective").getContent(),
                assessment: tinyMCE.get("redactor-assessment").getContent(),
                plan: tinyMCE.get("redactor-plan").getContent(),
                id: <?php echo $appointment->id ?>
            };

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>appointment/saveSoap",
                data: data,
                dataType: "text",
                success: function (data) {
                    if (data) {
                        notySuccess("Saved Successfully.");
                    }
                    else {
                        notyError("Save Fail.");
                    }
                },
                error: function() {
                    notyError("Save Fail.");
                }
            });

            console.log(data);
        });

        var $items = $('.k-item');
        $items.on('click', function() {
            var $this = $(this);
            if ($this.hasClass('disabled')) {
                return;
            }

            $('.k-content').hide();

            $items.removeClass('k-state-active');
            $this.addClass('k-state-active');

            var target = $(this).data('target');
            $('#'+target).show();
        });

    });
</script>