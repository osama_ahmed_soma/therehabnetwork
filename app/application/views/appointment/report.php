<?php /** @var $apiKey */?>
<?php /** @var $token */?>
<?php /** @var $isDoctor */?>
<?php /** @var $patient */?>
<?php /** @var $appointment */?>

<div class="page-content  view-doctor-profile">
    <section class="doctor-profile-section clearfix">
        <div class="dps-top-head">
            <h3>My Virtual Doctor Consultation Report</h3>
            <div class="pull-right"><a href="<?php echo site_url("/appointment/reportPdf?aid=" . $appointment->appointment_id); ?>">Download as <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></div>
        </div>
        <div class="dps-detail clearfix">
            <div class="dps-dt-img"> <img src="<?php echo default_image($patient->image_url); ?>" width="90%" class="preview" id="preview_patient"></div>
            <div class="dps-dt-cnt">
                <div class="dps-dt-client-name"><?php echo $appointment->firstname . ' ' .$appointment->lastname; ?></div>
                <div class="dps-dt-list-edu">
                    <div class="col-md-6"><ul>
                            <li><i class="fa fa-map"></i> <?php echo $appointment->address ? $appointment->address : 'N/A'; ?></li>
                            <li><i class="fa fa-fax"></i> <?php echo $appointment->phone_home ? $appointment->phone_home : 'N/A'; ?></li>
                            <li><i class="fa fa-phone"></i>  <?php echo $appointment->phone_mobile ? $appointment->phone_mobile : 'N/A'; ?></li>
                        </ul>
                    </div>
                    <div class="col-md-6"><ul>
                            <li><strong>Encounter Date:</strong> <i class="fa fa-clock-o"></i><?php echo date("jS F, Y g:i A", strtotime($appointment->start_date)); ?></li>
                            <li><strong>Clinician:</strong> <i class="fa fa-user"></i><?php echo $doctor->firstname . ' ' . $doctor->lastname ?></li>
                            <li><strong>Consultation Duration:</strong> <i class="fa fa-spinner"></i><?php echo gmdate("H:i:s", $appointment->duration); ?></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

    </section>
    <div class="container-fluid">
        <section class="dsp-detail-list clearfix">
            <div class="col-md-6">
                <div class="dsp-dl-column clearfix">
                    <div class="dsp-dl-cl-name colspan" data-att="myspecialities">Patient Statistic</div>
                    <div class="dso-dl-cl-txt colspan_show clearfix" data-id="myspecialities" style="display:block;">
                        <div class="col-md-6" style="padding:15px 0"><strong>Age: </strong><span style=""><?php echo (($patient->age > 0) ? $patient->age.' Years' : 'N/A'); ?></span></div>
                        <div class="col-md-6" style="padding:15px 0"><strong>Gender: </strong><span style=""><?php echo $patient->gender == 1 ? 'Male' : 'Female' ?></span></div>
                        <div class="col-md-6" style="padding:15px 0"><strong>Height: </strong><span style=""><?php echo $patient->height_ft ?>ft <?php echo $patient->height_inch ?>in</span></div>
                        <div class="col-md-6" style="padding:15px 0"><strong>Weight: </strong><span style=""><?php echo $patient->weight ?>lbs</span></div>
                    </div>
                </div>
                <div class="dsp-dl-column clearfix">
                    <div class="dsp-dl-cl-name colspan" data-att="myspecialities">Primary Concern</div>
                    <div class="dso-dl-cl-txt colspan_show clearfix" data-id="myspecialities" style="display:block;">
                        <?php echo $appointment->reason_id == '13' ?  $appointment->reason : $appointment->reason_name; ?>
                    </div>
                </div>
                <div class="dsp-dl-column clearfix">
                    <div class="dsp-dl-cl-name colspan" data-att="myspecialities">Secondary Concern</div>
                    <div class="dso-dl-cl-txt colspan_show clearfix" data-id="myspecialities" style="display:block;">
                        <?php echo $appointment->notes ? $appointment->notes : 'N/A'; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="dsp-dl-column clearfix">
                    <div class="dsp-dl-cl-name colspan" data-att="myspecialities">Subjective</div>
                    <div class="dso-dl-cl-txt colspan_show clearfix" data-id="myspecialities" style="display:block;">
                        <?php echo $appointment->subjective ? $appointment->subjective : 'N/A' ?>
                    </div>
                </div>
                <div class="dsp-dl-column clearfix">
                    <div class="dsp-dl-cl-name colspan" data-att="myspecialities">Objective</div>
                    <div class="dso-dl-cl-txt colspan_show clearfix" data-id="myspecialities" style="display:block;">
                        <?php echo $appointment->objective ? $appointment->objective : 'N/A' ?>
                    </div>
                </div>
                <div class="dsp-dl-column clearfix">
                    <div class="dsp-dl-cl-name colspan" data-att="myspecialities">Assessment</div>
                    <div class="dso-dl-cl-txt colspan_show clearfix" data-id="myspecialities" style="display:block;">
                        <?php echo $appointment->assessment ? $appointment->assessment : 'N/A' ?>
                    </div>
                </div>
                <div class="dsp-dl-column clearfix">
                    <div class="dsp-dl-cl-name colspan" data-att="myspecialities">Plan</div>
                    <div class="dso-dl-cl-txt colspan_show clearfix" data-id="myspecialities" style="display:block;">
                        <?php echo $appointment->plan ? $appointment->plan : 'N/A'; ?>
                    </div>
                </div>
            </div>
        </section>
    </div>

</div>
