<?php ?>
<div class="consult-screen">

    <div class="consult-screen__screen">
        <div class="networktest">
            <div class="center">
                <div class="title">Please wait while we are preparing your consultation.</div>
                <div class="title">Testing Video Requirement</div>
                <!--                <div id="testAnvilConnection" class="testblock green">-->
                <!--                    <div class="">API Server Test</div>-->
                <!--                    <section class="details"> Connect to the OpenTok API Servers </section> <span class="progress circle">100%</span>-->
                <!--                </div>-->
                <!--                <div id="testTurnConnection" class="testblock green">-->
                <!--                    <div>Mesh Turn Server Test</div>-->
                <!--                    <section class="details">Mesh calls with relay server fallback </section> <span class="progress circle">100%</span>-->
                <!--                </div>-->
                <div id="resultInfo" class="testblock red">
                    <div>Hardware Access</div>
                    <section class="details">Permission is denied to access the camera. Please change permission setting
                        for camera access
                    </section>
                    <span class="progress circle">100%</span>
                </div>
                <div id="userDisconnected" class="testblock" style="display: none;">
                    <div>User disconnected</div>
                    <section class="details">Video connection lost. Please wait.</section>
                </div>
            </div>
        </div>

        <div class="videodisconnect-msg" style="display: none;">
            <div class="center">
                <div class="title">Video connection lost. Please wait.</div>
            </div>
        </div>
        <div id="toolbar" class="annotationtoolbar" style="display: none;">
            <ul id="OT_toolbar" class="OT_panel">
                <li id="OT_capture" data-col="Capture" style="border: none; cursor: pointer;"></li>
                <li id="OT_clear" data-col="Clear" style="border: none; cursor: pointer;"></li>
                <li id="OT_line_width" data-type="group" data-col="Line Width"
                    style="border: none; cursor: pointer;"></li>
                <li id="OT_colors" class="OT_color" data-type="group" data-col="Colors"
                    style="color: rgb(26, 188, 156); border: none; cursor: pointer;"></li>
                <li id="OT_shapes" data-type="group" data-col="Shapes" style="border: none; cursor: pointer;"></li>
                <li id="OT_line" data-col="Line" style="border: none; cursor: pointer;"></li>
                <li id="OT_pen" data-col="Pen" style="border: none; cursor: pointer;"></li>
            </ul>
            <div class="color-picker" style="visibility: hidden; background-color: rgba(0, 0, 0, 0.701961);">
                <div class="color-choice" data-col="#1abc9c"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(26, 188, 156);"></div>
                <div class="color-choice" data-col="#2ecc71"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(46, 204, 113);"></div>
                <div class="color-choice" data-col="#3498db"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(52, 152, 219);"></div>
                <div class="color-choice" data-col="#9b59b6"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(155, 89, 182);"></div>
                <div class="color-choice" data-col="#34495e"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(52, 73, 94);"></div>
                <div class="color-choice" data-col="#16a085"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(22, 160, 133);"></div>
                <div class="color-choice" data-col="#27ae60"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(39, 174, 96);"></div>
                <div class="color-choice" data-col="#2980b9"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(41, 128, 185);"></div>
                <div class="color-choice" data-col="#8e44ad"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(142, 68, 173);"></div>
                <div class="color-choice" data-col="#2c3e50"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(44, 62, 80);"></div>
                <div class="color-choice" data-col="#f1c40f"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(241, 196, 15);"></div>
                <div class="color-choice" data-col="#e67e22"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(230, 126, 34);"></div>
                <div class="color-choice" data-col="#e74c3c"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(231, 76, 60);"></div>
                <div class="color-choice" data-col="#ecf0f1"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(236, 240, 241);"></div>
                <div class="color-choice" data-col="#95a5a6"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(149, 165, 166);"></div>
                <div class="color-choice" data-col="#f39c12"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(243, 156, 18);"></div>
                <div class="color-choice" data-col="#d35400"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(211, 84, 0);"></div>
                <div class="color-choice" data-col="#c0392b"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(192, 57, 43);"></div>
                <div class="color-choice" data-col="#bdc3c7"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(189, 195, 199);"></div>
                <div class="color-choice" data-col="#7f8c8d"
                     style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(127, 140, 141);"></div>
            </div>
        </div>
        <div class="pluginPlaceholder" id="pluginPlaceholder">

        </div>

        <div class="imagedrawer__container" data-bind="class:{is-active:openSnapImageHolder}">
            <a class="imagedrawer__prev-btn" href="#" data-bind="click:snapImagePrev"><span
                    class="icon_chevron-small-left"></span></a>

            <div id="snapImageholder" class="imagedrawer__snapimagecollection km-widget km-scroll-wrapper"
                 data-role="scroller" style="overflow: hidden;">
                <div class="km-scroll-header"></div>
                <div class="km-scroll-container" style="transform-origin: left top 0px;">
                    <div class="imagedrawer__photos" data-template="imageHolder"
                         data-bind="source:snapImageCollection"></div>
                </div>
                <div class="km-touch-scrollbar km-horizontal-scrollbar"
                     style="transform-origin: left top 0px; display: none; width: 814px;"></div>
                <div class="km-touch-scrollbar km-vertical-scrollbar"
                     style="transform-origin: left top 0px; display: none; height: 84px;"></div>
            </div>
            <a class="imagedrawer__next-btn" href="#" data-bind="click:snapImageNext"><span
                    class="icon_chevron-small-right"></span></a>

        </div>
        <div class="consult-screen__feed-wrapper"></div>
    </div>
</div>

<div class="consult-screen__horizontal-menu">
    <ul class="hide-mobile consult-options__horizontal" style="width: 100%">
        <li title="Cycle self view mode" class="is-active">
            <span class="consult-options__icon icon_self_view"></span>
            <span class="consult-options__text">Self View</span>
        </li>
        <li title="Mute Camera" id="muteCamera">
            <span class="consult-options__icon icon_video_camera"></span>
            <span class="consult-options__text">Video</span>
        </li>
        <li title="Mute Microphone" id="muteMic">
            <span class="consult-options__icon icon_mic"></span>
            <span class="consult-options__text">Mic</span>
        </li>
        <li title="Mute Speaker" id="muteSpeaker">
            <span class="consult-options__icon icon_speaker"></span>
            <span class="consult-options__text">Speakers</span>
        </li>
        <li title="Fullscreen View" id="fullScreen">
            <span class="consult-options__icon icon_resize-full-screen"></span>
            <span class="consult-options__text">Full Screen</span>
        </li>
        <li>
            <span class="consult-options__text consult-options__text--timer"
                  style="padding-top: 10px; font-size: 1em;"><?php echo($appointment->duration ? $appointment->durationReadable : '00:00') ?></span>
        </li>
        <li title="Start Consult" class="consult-btn">
            <?php if ($isDoctor): if ($appointment->is_doctor_ready != 2): ?>
                <span <?php if ($appointment->is_doctor_ready == 0): ?>
                    data-type="start_consult_container"
                <?php elseif ($appointment->is_doctor_ready == 1): ?>
                    data-type="end_consult_container"
                <?php endif; ?>
                    data-toggle="tooltip" data-placement="top"
                    title="<?php if ($appointment->is_doctor_ready == 0): ?>Start<?php elseif ($appointment->is_doctor_ready == 1): ?>End<?php endif; ?> Consult"
                    class="<?php if ($appointment->is_doctor_ready == 0): ?>green<?php endif; ?> consult-options__icon consult-options__icon--call icon_phone start_end_consult_btn start_end_consult_tooltip">
            </span>
            <?php endif;
            endif; ?>
        </li>
        <!--        -->
        <?php //if($isDoctor): if($appointment->is_doctor_ready == 2): ?><!--<li title="Exit Video Screen" class="consult-btn" style="display: none;">-->
        <!--            <span class=" consult-options__icon consult-options__icon--call exit_video_screen_btn exit_video_screen_tooltip" data-toggle="tooltip" data-placement="top" title="Exit Video Screen"><i class="fa fa-arrow-left" aria-hidden="true"></i></span>-->
        <!--        </li>--><?php //endif; endif; ?>
    </ul>

</div>