<?php /** @var $apiKey */ ?>
<?php /** @var $token */ ?>
<?php /** @var $isDoctor */ ?>
<?php /** @var $patient */ ?>
<?php /** @var $appointment */ ?>
<?php /** @var array $doctorFiles */ ?>
<?php /** @var array $patientFiles */ ?>

<style>
    .app-design {
        position: relative !important;
    }

    .green {
        background: #46C35F !important;
    }
</style>

<script src="https://static.opentok.com/v2/js/opentok.js" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>template_file/css/appointment_new.css">
<script charset="utf-8">
    var apiKey = '<?php echo $apiKey ?>';
    var sessionId = '<?php echo $appointment->tokbox_session; ?>';
    var token = '<?php echo $token; ?>';
    var publisher, subscriber;
    var publisherResized = false;
    var clientConnected = false;
    var publisherConnected = false;

    var totalSecond = <?php echo $appointment->duration;?>;

    var publisherOptions = {
        insertMode: 'append',
        showControls: false,
        resolution: '1280x720',
        width: "100%",
        height: "100%"
    };

    function publisherResize() {
        if (publisher && publisher.element && !publisherResized) {
            publisher.element.style["z-index"] = 999;
            publisher.element.style.width = "100px";
            publisher.element.style.height = "100px";

            publisherResized = true;
        }
    }

    function pad(val) {
        var valString = val + "";
        if (valString.length < 2) {
            return "0" + valString;
        }
        else {
            return valString;
        }
    }

    var intervaler;

    //fired when publisher and subscriber are online
    function onTranslationStart() {

        publisherResize();

        $('.consult-options__icon--call').addClass('is-active');

        intervaler = setInterval(function () {
            totalSecond++;

            var seconds = pad(totalSecond % 60);
            var minutes = pad(parseInt(totalSecond / 60));

//            $(".consult-options__text--timer").text(minutes + ":" + seconds);
        }, 1000);

        beginSession();
    }

    function onTranslationStop() {
        clearInterval(intervaler);
        endSession();
    }

    function beginSession() {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>appointment/beginSession",
            data: {
                aid: <?php echo $appointment->id ?>
            },
            dataType: "text"
        });
    }

    function endSession() {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>appointment/endSession",
            data: {
                aid: <?php echo $appointment->id ?>
            },
            dataType: "text"
        });
    }


    var session = OT.initSession(apiKey, sessionId)
    //new user connected
        .on('streamCreated', function (event) {
            console.log("Got new client ", event);

            var options = {width: '100%', height: "100%", showControls: false, insertMode: 'append'};
            subscriber = session.subscribe(event.stream, 'pluginPlaceholder', options);

            clientConnected = true;

            if (publisherConnected) {
                onTranslationStart();
            }

            publisherResize();
        })
        .on('streamDestroyed', function (event) {
            clientConnected = false;
            $("#userDisconnected").show();
            onTranslationStop();

            console.log("Client disconnected ", event);
        })
        .on('sessionDisconnected', function (event) {
            console.log(event);
        })
        .connect(token, function (error) {
            if (error) {
                console.log("Error connecting: ", error.code, error.message);
            } else {
                console.log("Connected to the session.");
            }

            publisher = OT.initPublisher('pluginPlaceholder', publisherOptions, function (error) {
                if (error) {
                    console.log('Publisher not initialized.');
                }
            });
            publisher.on({
                accessAllowed: function (event) {
                    $("#resultInfo").hide();
                }
            });
            session.publish(publisher, function (error) {
                if (error) {
                    console.log("Publishing error ", error);

                    $(".pluginPlaceholder").hide();

                    return;
                }

                publisherConnected = true;

                if (clientConnected) {
                    onTranslationStart();
                }
            });

        });


    jQuery(document).ready(function ($) {
        $('.start_end_consult_tooltip').tooltip('show');
        $('.start_end_consult_tooltip').on('hidden.bs.tooltip', function () {
            $('.start_end_consult_tooltip').tooltip('show');
        });
        $('.exit_video_screen_tooltip').tooltip('show');
        $('.exit_video_screen_tooltip').on('hidden.bs.tooltip', function () {
            $('.exit_video_screen_tooltip').tooltip('show');
        });
        $("#muteCamera").on('click', function () {
            $this = $(this);
            $children = $this.children().first();

            $this.toggleClass('is-active');

            //camera is enabled
            if ($children.hasClass('icon_video_camera')) {
                $children.removeClass('icon_video_camera');
                $children.addClass('icon_video_camera_mute');

                publisher.publishVideo(false);
            }
            else {
                $children.addClass('icon_video_camera');
                $children.removeClass('icon_video_camera_mute');

                publisher.publishVideo(true);
            }
        });

        $("#muteMic").on('click', function () {
            $this = $(this);
            $children = $this.children().first();

            $this.toggleClass('is-active');

            //mic is enabled
            if ($children.hasClass('icon_mic')) {
                $children.removeClass('icon_mic');
                $children.addClass('icon_mic_mute');

                publisher.publishAudio(false);
            }
            else {
                $children.addClass('icon_mic');
                $children.removeClass('icon_mic_mute');

                publisher.publishAudio(true);
            }
        });

        $("#muteSpeaker").on('click', function () {
            $this = $(this);
            $children = $this.children().first();

            $this.toggleClass('is-active');

            //speaker is enabled
            if ($children.hasClass('icon_speaker')) {
                $children.removeClass('icon_speaker');
                $children.addClass('icon_speaker_mute');

                if (subscriber) {
                    subscriber.setAudioVolume(0);
                }
            }
            else {
                $children.addClass('icon_speaker');
                $children.removeClass('icon_speaker_mute');

                if (subscriber) {
                    subscriber.setAudioVolume(100);
                }
            }
        });

        $("#fullScreen").on('click', function () {

            // if already full screen; exit
            // else go fullscreen
            if (
                document.fullscreenElement ||
                document.webkitFullscreenElement ||
                document.mozFullScreenElement ||
                document.msFullscreenElement
            ) {

                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                } else if (document.msExitFullscreen) {
                    document.msExitFullscreen();
                }
            } else {
                element = $('#appointment-container').get(0);

                if (element.requestFullscreen) {
                    element.requestFullscreen();
                } else if (element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if (element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                } else if (element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
            }
        });

        $(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange MSFullscreenChange', function (e) {
            $this = $("#fullScreen");
            $children = $this.children().first();

            $this.toggleClass('is-active');

            //full screen
            if (document.mozFullScreenElement) {
                $children.removeClass('icon_resize-full-screen');
                $children.addClass('icon_resize-100');
            }
            else {
                $children.addClass('icon_resize-full-screen');
                $children.removeClass('icon_resize-100');
            }
        });

        $(document.body).on('click', '.consult-options__icon--call', function () {
            var th = $(this);
            //if( th.hasClass('.is-active')) {
            //show confirm modal

            /*var msg = 'End This Call?';
             notyConfirm2(msg, endCallRedirect);*/
            //}
        });
        var startEndVideoButton;
        $(document.body).on('click', '.start_end_consult_btn', function () {
            var self = $(this);
            startEndVideoButton = self;
            if (self.attr('data-type') == 'start_consult_container') {
                self.hide();
                $('.start_end_consult_tooltip').tooltip('hide');
                self.parent().attr('style', 'cursor: default;').append('<img data-selector="custom_loading_on_button" src="https://i.stack.imgur.com/FhHRx.gif" class="" style="margin-top: 13px;">');
//                self.removeClass('start_end_consult_btn');
                $.ajax({
                    method: "POST",
                    url: "<?php echo base_url(); ?>doctor/start_consult/<?php echo $appointment->id; ?>",
                    data: {},
                    dataType: "json",
                }).success(function (data, status) {
                    setTimeout(function(){
                        self.parent().attr('style', 'cursor: pointer;');
//                        $('img[data-selector="custom_loading_on_button"]').remove();
//                        self.show();
                        if (!data.bool) {
                            notyError(data.message);
                            self.attr('data-type', 'start_consult_container').addClass('green');
                            self.attr('title', 'Start Consult');
                            $('.start_end_consult_tooltip').tooltip('hide')
                                .attr('data-original-title', 'Start Consult')
                                .tooltip('show');
                        }/* else {
                            self.attr('data-type', 'end_consult_container').removeClass('green');
                            self.attr('title', 'End Consult');
                            $('.start_end_consult_tooltip').tooltip('hide')
                                .attr('data-original-title', 'End Consult')
                                .tooltip('show');
                        }*/
                    }, 1500);
                }).done(function () {

                });
                return;
            }
            if (self.attr('data-type') == 'end_consult_container') {
                var msg = '<strong>Confirmation</strong> You currently have a consultation progress. Are you sure you want to end this consultation';
                notyConfirm2(msg, endCallRedirect);
                return;
            }
            if (self.attr('data-type') == 'back_consult_container') {
                endCallRedirect2();
                return;
            }
        });

        <?php if($this->session->userdata('type') == 'doctor'): ?>
        check_start_button();
        if($('.start_end_consult_btn').attr('data-type') != 'end_consult_container'){
            $('.start_end_consult_btn').attr('data-type', 'back_consult_container');
            $('.start_end_consult_btn').removeClass('icon_phone');
            $('.start_end_consult_btn').addClass('icon_back');
            $('.start_end_consult_btn').attr('title', 'Exit Video Screen');
            $('.start_end_consult_tooltip').tooltip('hide')
                .attr('data-original-title', 'Exit Video Screen')
                .tooltip('show');
        }
//        $('.start_end_consult_btn').hide('slow');
        function check_start_button(){
            $.ajax({
                method: "GET",
                url: "<?php echo base_url(); ?>appointment/request_to_start_consult_btn/<?php echo $appointment->id; ?>/true",
                data: {},
                dataType: "json",
            }).success(function (data, status) {
                if(data.show_button || data.patient_approved){
                    if(data.patient_approved){
                        $('#time_note').addClass('knocking_notification').html('Your patient is knocking and is available to start the consultation. Please click the "Call" button to start now').attr('title', '').show(200);
                    }
//                    $('.start_end_consult_btn').show('slow');
                    if($('.start_end_consult_btn').attr('data-type') != 'end_consult_container'){
                        $('.start_end_consult_btn').attr('data-type', 'start_consult_container');
                        $('.start_end_consult_btn').removeClass('icon_back');
                        $('.start_end_consult_btn').addClass('icon_phone');
                        $('.start_end_consult_btn').attr('title', 'Start Consult');
                        $('.start_end_consult_tooltip').tooltip('hide')
                            .attr('data-original-title', 'Start Consult')
                            .tooltip('show');
                    }
                    check_start_timeFrom_patient();
                    return;
                }
                setTimeout(function(){
                    check_start_button();
                }, 1500);
            });
        }
        <?php else: ?>
        check_start_timeFrom_patient();
        <?php endif; ?>
        function call_timer(time){
            var minutes = Math.floor(time / 60),
                seconds = time - minutes * 60,
                time_format = ("0" + minutes).slice(-2)+':'+("0" + seconds).slice(-2);
            $('.consult-options__text--timer').text(time_format);
        }

        var start_call_duration_timer_interval_object;

        function check_start_timeFrom_patient(){
            show_loader = false;
            setTimeout(function(){
                $.ajax({
                    method: "POST",
                    url: "<?php echo base_url(); ?>appointment/get_consultation_start_time/<?php echo $appointment->id; ?>",
                    data: {},
                    dataType: "json",
                }).success(function (data, status) {
                    if(!data.bool){
                        check_start_timeFrom_patient();
                        return;
                    }
                    call_timer(data.seconds);
                    start_call_duration_timer_interval_object = setInterval(function(){
                        data.seconds++
                        call_timer(data.seconds);
                    }, 1000);
                    <?php if($this->session->userdata('type') == 'doctor'): ?>
                    $('img[data-selector="custom_loading_on_button"]').remove();
                    startEndVideoButton.show();
                    startEndVideoButton.attr('data-type', 'end_consult_container').removeClass('green');
                    startEndVideoButton.attr('title', 'End Consult');
                    $('.start_end_consult_tooltip').tooltip('hide')
                        .attr('data-original-title', 'End Consult')
                        .tooltip('show');
                    if($('#time_note').hasClass('knocking_notification')){
                        $('#time_note').removeClass('knocking_notification').html('').hide(200);
                    }
                    <?php endif; ?>
                });
            }, 3000);
        }

        function endCallRedirect() {
            $.ajax({
                method: "POST",
                url: "<?php echo base_url(); ?>doctor/end_consult/<?php echo $appointment->id; ?>",
                data: {},
            }).success(function (data, status) {
                $('.start_end_consult_btn').removeClass('icon_phone');
                $('.start_end_consult_btn').addClass('icon_back');

                $('.start_end_consult_btn').attr('data-type', 'back_consult_container').removeClass('green');
                $('.start_end_consult_btn').attr('title', 'Exit Video Screen');
                $('.start_end_consult_tooltip').tooltip('hide')
                    .attr('data-original-title', 'Exit Video Screen')
                    .tooltip('show');
//                $('.start_end_consult_btn').parent().hide(200);
//                $('.start_end_consult_tooltip').tooltip('disable');
//                $('.exit_video_screen_btn').parent().show('slow');
                clearInterval(start_call_duration_timer_interval_object);
                var msg = 'Do you want to continue with current consultation notes?';
                notyConfirm3(msg, endCallRedirect2);
            }).done(function () {

            });
        }

        function endCallRedirect2() {
            //redirect user to consultation window
            var url = '<?php echo $this->session->userdata('type') == 'doctor' ? base_url() . 'doctor/consultations' : base_url() . 'patient/appointments'; ?>';
            window.location = url;
        }

        function notyConfirm2(message, onSuccessCallback) {
            var params = {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                            onSuccessCallback();
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            };

            notyMessage(message, 'warning', params);
        }

        function notyConfirm3(message, onSuccessCallback) {
            var params = {
                buttons: [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Yes',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'No',
                        onClick: function ($noty) {
                            $noty.close();
                            onSuccessCallback();
                        }
                    }
                ]
            };

            notyMessage(message, 'warning', params);
        }

        $('.exit_video_screen_btn').click(function(e){
            e.preventDefault();
            window.location = '<?php echo base_url(); ?>';
        });


    });

</script>
<?php if (!$isDoctor) { ?>
    <script>
        function recurv() {
            show_loader = false;
            setTimeout(function(){
                $.ajax({
                    method: "POST",
                    url: "<?php echo base_url(); ?>patient/is_doctor_end_consultation/<?php echo $appointment->id; ?>",
                    data: {},
                    dataType: "json",
                }).success(function (data, status) {
                    if (data.bool) {
//                notySuccess('The consultation is now over. The doctor will prepare a report for you shortly so make sure to check back for this appointment under “Past Appointments” section on your <a href="<?php //echo base_url(); ?>//patient/appointments">“My Appointments”</a> page.');
                        $('#patient_modal_for_redirect').modal('show');
                        setTimeout(function () {
                            window.location = "<?php echo base_url(); ?>patient/appointments";
                        }, 5000);
                        return;
                    }
                    recurv();
                }).done(function () {

                });
            }, 3000);
        }
        $(document).ready(function () {
            recurv();
            <?php if(count($appointment_log) > 0): ?>
                <?php if(empty($appointment_log[0]->session_start_stamp)): ?>
            $.ajax({
                method: "GET",
                url: "<?php echo base_url(); ?>patient/add_start_session_time/<?php echo $appointment_log[0]->id; ?>",
                data: {},
                dataType: "json",
            }).success(function (data, status) {

            });
                <?php endif; ?>
            <?php endif; ?>
        });
    </script>
<?php } ?>
<div class="modal fade bs-example-modal-ls" id="patient_modal_for_redirect" tabindex="-1" role="dialog"
     data-backdrop="static" data-keyboard="false" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-ls" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <p>The consultation is now over. The doctor will prepare a report for you shortly so make sure to check
                    back for this appointment under “Past Appointments” section on your <a
                        href="<?php echo base_url(); ?>patient/appointments">“My Appointments”</a> page.</p>
            </div>
        </div>
    </div>
</div>
<div style="padding-top: 0px; clear: both; display: table; width: 100%">
    <section style="overflow: hidden; width:<?php echo $isDoctor ? '75' : '100' ?>%; float: left;"
             id="appointment-container" class="<?php if ($isDoctor) {
        echo 'doctor';
    } else {
        echo 'patient';
    } ?>">
        <?php $this->view('appointment/_video', [
            'appointment' => $appointment,
            'isDoctor' => $isDoctor
        ]); ?>
    </section>

    <?php if ($isDoctor) { ?>
        <section style="width: 25%; float: left;" id="patientContainer">
            <?php $this->view('appointment/_patient_info', [
                'patient' => $patient,
                'appointment' => $appointment,
                'doctorFiles' => $doctorFiles,
                'patientFiles' => $patientFiles,
            ]); ?>
        </section>
    <?php } ?>

</div>
<div class="footer-start-page">