<link rel="stylesheet" href="<?php echo base_url(); ?>template_file/css/appointment.css">

<div class="rowz" style="padding-top: 80px; clear: both; display: table">
    <div class="container-fluidz">
        <section id="SECTION_1">
            <div id="DIV_2">
                <div id="DIV_131">
                    <div id="DIV_132">
                        <div id="DIV_133">
                            <div id="DIV_134">
                                Please wait while we are preparing your consultation.
                            </div>
                            <div id="DIV_135">
                                Testing Video Requirement
                            </div>
                            <div id="DIV_136">
                                <div id="DIV_137">
                                    API Server Test
                                </div>
                                <section id="SECTION_138">
                                    Connect to the OpenTok API Servers
                                </section>
                                <span id="SPAN_139">100%</span>
                            </div>
                            <div id="DIV_140">
                                <div id="DIV_141">
                                    Mesh Turn Server Test
                                </div>
                                <section id="SECTION_142">
                                    Mesh calls with relay server fallback
                                </section>
                                <span id="SPAN_143">100%</span>
                            </div>
                            <div id="DIV_144">
                                <div id="DIV_145">
                                    Hardware Access
                                </div>
                                <section id="SECTION_146">
                                    Permission is denied to access the camera. Please change permission setting for
                                    camera
                                    access
                                </section>
                                <span id="SPAN_147">100%</span>
                            </div>
                        </div>
                    </div>
                    <div id="DIV_148">
                        <div id="DIV_149">
                            <div id="DIV_150">
                                Video connection lost. Please wait.
                            </div>
                        </div>
                    </div>
                    <div id="DIV_151">
                        <ul id="UL_152">
                            <li id="LI_153">
                            </li>
                            <li id="LI_154">
                            </li>
                            <li id="LI_155">
                            </li>
                            <li id="LI_156">
                            </li>
                            <li id="LI_157">
                            </li>
                            <li id="LI_158">
                            </li>
                            <li id="LI_159">
                            </li>
                        </ul>
                        <div id="DIV_160">
                            <div id="DIV_161">
                            </div>
                            <div id="DIV_162">
                            </div>
                            <div id="DIV_163">
                            </div>
                            <div id="DIV_164">
                            </div>
                            <div id="DIV_165">
                            </div>
                            <div id="DIV_166">
                            </div>
                            <div id="DIV_167">
                            </div>
                            <div id="DIV_168">
                            </div>
                            <div id="DIV_169">
                            </div>
                            <div id="DIV_170">
                            </div>
                            <div id="DIV_171">
                            </div>
                            <div id="DIV_172">
                            </div>
                            <div id="DIV_173">
                            </div>
                            <div id="DIV_174">
                            </div>
                            <div id="DIV_175">
                            </div>
                            <div id="DIV_176">
                            </div>
                            <div id="DIV_177">
                            </div>
                            <div id="DIV_178">
                            </div>
                            <div id="DIV_179">
                            </div>
                            <div id="DIV_180">
                            </div>
                        </div>
                    </div>
                    <div id="DIV_181">
                    </div>
                    <div id="DIV_182">
                        <a href="#" id="A_183"><span id="SPAN_184"></span></a>
                        <div id="DIV_185">
                            <div id="DIV_186">
                            </div>
                            <div id="DIV_187">
                                <div id="DIV_188">
                                </div>
                            </div>
                            <div id="DIV_189">
                            </div>
                            <div id="DIV_190">
                            </div>
                        </div>
                        <a href="#" id="A_191"><span id="SPAN_192"></span></a>
                    </div>
                    <div id="DIV_193">
                    </div> <!--
			<div id="snapImageholder">
				<div>
					<div>
						<span></span><br /> Discard
					</div><img src="images/none.jpg" alt='' />
				</div>
				<div>
					<div>
						<span></span><br /> Discard
					</div><img src="images/none.jpg" alt='' />
				</div>
			</div> -->
                </div>
            </div>
            <div id="DIV_215">
                <ul id="UL_216">
                    <li id="LI_217">
                        <span id="SPAN_218"></span> <span id="SPAN_219">Controls</span>
                    </li>
                    <li id="LI_220">
                        <span id="SPAN_221"></span> <span id="SPAN_222">Images</span>
                    </li>
                    <li id="LI_223">
                        <span id="SPAN_224"></span>
                    </li>
                    <li id="LI_225">
                        <span id="SPAN_226">00:00</span> <span id="SPAN_227">Session</span>
                    </li>
                    <li id="LI_228">
                        <span id="SPAN_229"></span> <span id="SPAN_230">Info</span>
                    </li>
                </ul>
<!--                <ul id="UL_231">-->
<!--                    <li id="LI_232">-->
<!--                        <span id="SPAN_233"></span> <span id="SPAN_234">Settings</span>-->
<!--                    </li>-->
<!--                    <li id="LI_235">-->
<!--                        <span id="SPAN_236"></span> <span id="SPAN_237">Share</span>-->
<!--                    </li>-->
<!--                    <li id="LI_238">-->
<!--                        <span id="SPAN_239"></span> <span id="SPAN_240">Participants</span>-->
<!--                    </li>-->
<!--                </ul>-->
                <ul id="UL_241">
                    <li id="LI_242">
                        <span id="SPAN_243"></span> <span id="SPAN_244">Images</span>
                    </li>
                    <li id="LI_245">
                        <span id="SPAN_246"></span> <span id="SPAN_247">Kubi</span>
                    </li>
                    <li id="LI_248">
                        <span id="SPAN_249"></span> <span id="SPAN_250">Self View</span>
                    </li>
                    <li id="LI_251">
                        <span id="SPAN_252"></span> <span id="SPAN_253">Video</span>
                    </li>
                    <li id="LI_254">
                        <span id="SPAN_255"></span> <span id="SPAN_256">Mic</span>
                    </li>
                    <li id="LI_257">
                        <span id="SPAN_258"></span> <span id="SPAN_259">Speakers</span>
                    </li>
                    <li id="LI_260">
                        <span id="SPAN_261"></span> <span id="SPAN_262">Full Screen</span>
                    </li>
                    <li id="LI_263">
                        <span id="SPAN_264">00:00</span> <span id="SPAN_265">Session</span>
                    </li>
                    <li id="LI_266">
                        <span id="SPAN_267"></span>
                    </li>
                </ul>
            </div>
        </section>
    </div>
</div>
