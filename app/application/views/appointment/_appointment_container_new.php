<link rel="stylesheet" href="<?php echo base_url(); ?>template_file/css/appointment_new.css">

<div style="padding-top: 80px; clear: both; display: table; width: 100%">
    <section style="width:75%; float: left;" id="appointment-container">
        <div class="consult-screen">
            <div class="hide-desktop consult-screen__vertical-menu">
                <ul class="consult-options__vertical consult-options__vertical--top">
                    <li data-id="activeShare">
                        <span class="consult-options__icon icon_share_screen"></span>
                        <span class="consult-options__text">Share</span>
                    </li>
                    <li data-id="activeParticipants active" data-bind="class:{active:activeParticipants},click:selectVideoTab" class="">
                        <span class="consult-options__icon icon_users"></span>
                        <span class="consult-options__text">Participants</span>
                    </li>
                    <li data-id="activeSetting" data-bind="click:selectVideoTab">
                        <span class="consult-options__icon icon_cog"></span>
                        <span class="consult-options__text">Settings</span>
                    </li>
                </ul>

                <ul class="consult-options__vertical consult-options__vertical--bottom">
                    <li data-bind="click:showSelfView" title="Cycle self view mode">
                        <span class="consult-options__icon icon_selfview-on"></span>
                        <span class="consult-options__text">Self View</span>
                    </li>
                    <li title="Mute Video" data-bind="click:muteVideo">
                        <span class="consult-options__icon icon_video-camera_mute" data-bind="class:{icon_video-camera :isVideoUnMute,icon_video-camera_mute :isVideoMute}"></span>
                        <span class="consult-options__text">Video</span>
                    </li>
                    <li title="Mute Speaker" data-bind="click:muteVoice">
                        <span class="consult-options__icon icon_speaker" data-bind="class:{icon_speaker :isUnMute,icon_speaker_mute :isMute}"></span>
                        <span class="consult-options__text">Speakers</span>
                    </li>
                    <li title="Mute Microphone" data-bind="click:muteMicrophone">
                        <span class="consult-options__icon icon_mic"></span>
                        <span class="consult-options__text">Mic</span>
                    </li>
                </ul>
            </div>

            <div class="consult-screen__screen">
                <div class="networktest">
                    <div class="center">
                        <div class="title">Please wait while we are preparing your consultation. </div>
                        <div class="title">Testing Video Requirement </div>
                        <div id="testAnvilConnection" class="testblock green">
                            <div class="">API Server Test</div>
                            <section class="details"> Connect to the OpenTok API Servers </section> <span class="progress circle">100%</span>
                        </div>
                        <div id="testTurnConnection" class="testblock green">
                            <div>Mesh Turn Server Test</div>
                            <section class="details">Mesh calls with relay server fallback </section> <span class="progress circle">100%</span>
                        </div>
                        <div id="resultInfo" class="testblock red">
                            <div>Hardware Access</div>
                            <section class="details">Permission is denied to access the camera. Please change permission setting for camera access</section> <span class="progress circle">100%</span>
                        </div>
                    </div>
                </div>
                <div class="videodisconnect-msg" style="display: none;">
                    <div class="center">
                        <div class="title">Video connection lost. Please wait. </div>
                    </div>
                </div>
                <div id="toolbar" class="annotationtoolbar" style="display: none;"><ul id="OT_toolbar" class="OT_panel"><li id="OT_capture" data-col="Capture" style="border: none; cursor: pointer;"></li><li id="OT_clear" data-col="Clear" style="border: none; cursor: pointer;"></li><li id="OT_line_width" data-type="group" data-col="Line Width" style="border: none; cursor: pointer;"></li><li id="OT_colors" class="OT_color" data-type="group" data-col="Colors" style="color: rgb(26, 188, 156); border: none; cursor: pointer;"></li><li id="OT_shapes" data-type="group" data-col="Shapes" style="border: none; cursor: pointer;"></li><li id="OT_line" data-col="Line" style="border: none; cursor: pointer;"></li><li id="OT_pen" data-col="Pen" style="border: none; cursor: pointer;"></li></ul><div class="color-picker" style="visibility: hidden; background-color: rgba(0, 0, 0, 0.701961);"><div class="color-choice" data-col="#1abc9c" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(26, 188, 156);"></div><div class="color-choice" data-col="#2ecc71" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(46, 204, 113);"></div><div class="color-choice" data-col="#3498db" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(52, 152, 219);"></div><div class="color-choice" data-col="#9b59b6" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(155, 89, 182);"></div><div class="color-choice" data-col="#34495e" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(52, 73, 94);"></div><div class="color-choice" data-col="#16a085" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(22, 160, 133);"></div><div class="color-choice" data-col="#27ae60" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(39, 174, 96);"></div><div class="color-choice" data-col="#2980b9" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(41, 128, 185);"></div><div class="color-choice" data-col="#8e44ad" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(142, 68, 173);"></div><div class="color-choice" data-col="#2c3e50" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(44, 62, 80);"></div><div class="color-choice" data-col="#f1c40f" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(241, 196, 15);"></div><div class="color-choice" data-col="#e67e22" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(230, 126, 34);"></div><div class="color-choice" data-col="#e74c3c" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(231, 76, 60);"></div><div class="color-choice" data-col="#ecf0f1" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(236, 240, 241);"></div><div class="color-choice" data-col="#95a5a6" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(149, 165, 166);"></div><div class="color-choice" data-col="#f39c12" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(243, 156, 18);"></div><div class="color-choice" data-col="#d35400" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(211, 84, 0);"></div><div class="color-choice" data-col="#c0392b" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(192, 57, 43);"></div><div class="color-choice" data-col="#bdc3c7" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(189, 195, 199);"></div><div class="color-choice" data-col="#7f8c8d" style="width: 30px; height: 30px; margin: 5px; cursor: pointer; border-radius: 100%; opacity: 0.7; background-color: rgb(127, 140, 141);"></div></div></div>
                <div class="pluginPlaceholder" id="pluginPlaceholder">

                </div>

                <div class="imagedrawer__container" data-bind="class:{is-active:openSnapImageHolder}">
                    <a class="imagedrawer__prev-btn" href="#" data-bind="click:snapImagePrev"><span class="icon_chevron-small-left"></span></a>

                    <div id="snapImageholder" class="imagedrawer__snapimagecollection km-widget km-scroll-wrapper" data-role="scroller" style="overflow: hidden;"><div class="km-scroll-header"></div><div class="km-scroll-container" style="transform-origin: left top 0px;">
                            <div class="imagedrawer__photos" data-template="imageHolder" data-bind="source:snapImageCollection"></div>
                        </div><div class="km-touch-scrollbar km-horizontal-scrollbar" style="transform-origin: left top 0px; display: none; width: 814px;"></div><div class="km-touch-scrollbar km-vertical-scrollbar" style="transform-origin: left top 0px; display: none; height: 84px;"></div></div>
                    <a class="imagedrawer__next-btn" href="#" data-bind="click:snapImageNext"><span class="icon_chevron-small-right"></span></a>

                </div>
                <div class="consult-screen__feed-wrapper"></div>
                <!--
                  <div id="snapImageholder" class="snapimagecollection" data-bind="class:{is-active:openSnapImageHolder}"
                      data-source="snapImageCollection"
                      data-template="imageHolder"
                      data-content-height="120px"
                      data-enable-pager="false">

                     <div class="snap-img-item">
                         <div class="snapimg-close" data-bind="click: closeSnapImage">
                             <span class="icon_circle-with-cross"></span><br />
                             Discard
                         </div>
                         <img class="snapimg" src="images/none.jpg" />

                     </div>
                     <div class="snap-img-item">
                         <div class="snapimg-close" data-bind="click: closeSnapImage">
                             <span class="icon_circle-with-cross"></span><br />
                             Discard
                         </div>
                         <img class="snapimg" src="images/none.jpg" />

                     </div>
                 </div>
                    -->

            </div>

            <div class="consult-screen__content--settings" data-bind="class:{is-active:activeSetting}">
                <div class="hide-desktop consult-settings__close" data-id="activeSetting" data-bind="click:selectVideoTab">Close <span class="icon_cross"></span></div>
                <div class="consult-settings__title">Settings</div>

                <div class="consult-settings__options">
                    <div class="consult-settings__sub-title">Camera:</div>

                    <span title="" style="width: 96%;" class="k-widget k-dropdown k-header" unselectable="on" role="listbox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-owns="" aria-disabled="false" aria-readonly="false"><span unselectable="on" class="k-dropdown-wrap k-state-default"><span unselectable="on" class="k-input"></span><span unselectable="on" class="k-select"><span unselectable="on" class="k-icon k-i-arrow-s">select</span></span></span><input data-placeholder="Select..." style="width: 96%; display: none;" data-role="dropdownlist" data-auto-bind="false" data-text-field="label" data-value-field="value" data-bind="source: videoSourceList,value: selectedVideoSource, events: { change: onVideoChange,open:onVideoComboOpen }"></span>

                </div>
                <div class="consult-settings__options">
                    <div class="consult-settings__sub-title">Microphone:</div>
                    <div>
                        <span title="" style="width: 96%;" class="k-widget k-dropdown k-header" unselectable="on" role="listbox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-owns="" aria-disabled="false" aria-readonly="false"><span unselectable="on" class="k-dropdown-wrap k-state-default"><span unselectable="on" class="k-input"></span><span unselectable="on" class="k-select"><span unselectable="on" class="k-icon k-i-arrow-s">select</span></span></span><input data-placeholder="Select..." style="width: 96%; display: none;" data-role="dropdownlist" data-auto-bind="false" data-text-field="label" data-value-field="value" data-bind="source: audioSourceList,value: selectedAudioSource, events: { change: onAudioChange,open:onAudioComboOpen }"></span>
                    </div>
                </div>
            </div>

        </div>



        <div class="consult-screen__horizontal-menu">


            <ul class="hide-mobile consult-options__horizontal" data-bind="visible : hasVideoInititlized">
                <li title="Cycle self view mode" class="is-active">
                    <span class="consult-options__icon icon_self_view"></span>
                    <span class="consult-options__text">Self View</span>
                </li>
                <li title="Mute Camera" data-bind="click:muteVideo, class:{is-active:isVideoBtn}" class="is-active">
                    <span class="consult-options__icon icon_video_camera_mute" data-bind="class:{icon_video_camera :isVideoUnMute,icon_video_camera_mute :isVideoMute}"></span>
                    <span class="consult-options__text">Video</span>
                </li>
                <li data-bind="click:muteMicrophone, class:{is-active:isMicrophoneBtn}" title="Mute Microphone">
                    <span class="consult-options__icon icon_mic icon_microphone" data-bind="class:{icon_microphone_mute :isMuteMicrophone,icon_microphone :isUnMuteMicrophone}"></span>
                    <span class="consult-options__text">Mic</span>
                </li>
                <li title="Mute Speaker">
                    <span class="consult-options__icon icon_speaker"></span>
                    <span class="consult-options__text">Speakers</span>
                </li>
                <li data-bind="click:onFullScreen" title="Fullscreen View">
                    <span class="consult-options__icon icon_resize-full-screen"></span>
                    <span class="consult-options__text">Full Screen</span>
                </li>
                <li>
                    <span class="consult-options__text consult-options__text--timer" data-bind="html:sessionTime">00:00</span>
                    <span class="consult-options__text">Session</span>
                </li>
                <li title="Start Consult" class="consult-btn" data-bind="click:startSession, attr: {title: consultButtonTitle }">
                    <span class="consult-options__icon consult-options__icon--call icon_phone" data-bind="class:{is-active:isStarted}"></span>
                </li>
            </ul>

        </div>

    </section>

    <section style="width: 25%; float: left;" id="patientContainer">
        <div class="patient-data__userinfo">
            <div class="userpic userpic--patient-data userpic--white-border">
                <img src="https://myvirtualdoctor.connectedcare.md/api/v2.1/images/b41b3c4c-4095-49be-9ebd-5cb75a58330f" alt="Phil" data-bind="attr: { src: profileImage, alt: profileFirstName, title: profileFirstName }" title="Phil">
            </div>
            <div class="patient-data__name">
                <span class="patient-data__firstname" data-bind="text:profileFirstName">Phil</span>
                <span class="patient-data__lastname" data-bind="text:profileLastName">Vasta</span>
            </div>
            <div class="patient-data__secondary-info" data-bind="invisible:isGuestUser">
                <span class="patient-data__gender" data-bind="html:patientInfomation.gender">M</span>
                <span class="patient-data__age" data-bind="text:patientInfomation.age">41</span>
                <span class="patient-data__phone"><a href="tel:+1561-289-9988" data-bind="html:patientInfomation.mobilePhone,attr:{href:patientMobilePhoneLink}">(561) 289-9988</a></span>
            </div>

            <a class="hide-desktop close" href="#" data-bind="click:toggleMobileInfoPanel">Close</a>
        </div>

        <div class="k-tabstrip-wrapper"><div class="consult-screen__consultation-tabs k-widget k-header k-tabstrip k-floatwrap k-tabstrip-top k-tabstrip-scrollable" id="clinician-consultation-tabs" data-role="tabstrip" tabindex="0" role="tablist">

                <ul class="k-tabstrip-items k-reset"><li class="intakeel k-state-active k-item k-tab-on-top k-state-default k-first" data-bind="visible:showIntakeForm" role="tab" aria-selected="true" aria-controls="clinician-consultation-tabs-1"><span class="k-loading k-complete"></span>
                        <span class="k-link"><span class="icon icon_intake"></span><span class="text">Intake Form</span></span>

                    </li><li class="userprofileel k-item k-state-default" role="tab" aria-controls="clinician-consultation-tabs-2"><span class="k-loading k-complete"></span>
                        <span class="k-link"><span class="icon icon_user"></span><span class="text">Profile</span></span>

                    </li><li class="chattab k-state-disabled disabled k-item" role="tab" aria-controls="clinician-consultation-tabs-3"><span class="k-loading k-complete"></span>
                        <span class="k-link"><span class="message-flag hide">3</span><span class="icon icon_chat"></span><span class="text">Message</span></span>


                    </li><li class="sopatab k-state-disabled disabled k-item" data-bind="visible:showSoap" role="tab" aria-controls="clinician-consultation-tabs-4"><span class="k-loading k-complete"></span>
                        <span class="k-link"><span class="icon icon_note"></span><span class="text">S.O.A.P.</span></span>

                    </li><li class="filestab k-item" data-bind="visible:showFiles" role="tab" aria-controls="clinician-consultation-tabs-5"><span class="k-loading k-complete"></span>
                        <span class="k-link"><span class="icon icon_archive"></span><span class="text">Files</span></span>

                    </li><li class="ePrescribe k-state-disabled disabled k-item k-last" data-bind="visible:showRx" role="tab" aria-controls="clinician-consultation-tabs-6"><span class="k-loading k-complete"></span>
                        <span class="k-link"><span class="icon icon_rx"></span><span class="text">e-Prescribe</span></span>

                    </li></ul>

                <div class="patient-data patient-data__intake k-content k-state-active" id="clinician-consultation-tabs-1" role="tabpanel" aria-expanded="true" style="display: block;">
                    <ul class="patient-data__list">
                        <li>
                            <h3>Primary Concern</h3>
                            <p data-bind="html:consultationInfomation.primaryConsern">Diarrhea or Constipation</p>
                        </li>
                        <li>
                            <h3>Secondary Concern</h3>
                            <p data-bind="html:consultationInfomation.secondaryConsern">N/A</p>
                        </li>
                    </ul>

                    <div class="patient-data__info-block">
                        <h2 class="patient-data__title patient-data__title--large">Medical History</h2>

                        <h3 class="patient-data__subtitle patient-data__subtitle--consult">Chronic Conditions</h3>
                        <ul class="patient-data__list patient-data__list--consult" data-template="chronictemplate" data-bind="source:intakeForm.medicalCondition,visible:isMedicalConditionAvailable" style="display: none;"></ul>

                        <ul class="patient-data__list patient-data__list--consult patient-data__list--empty" data-bind="invisible:isMedicalConditionAvailable">
                            <li>None Reported</li>
                        </ul>

                        <div data-bind="visible:intakeForm.isOneYearBelowChild" style="display: none;">
                            <h2 class="patient-data__subtitle patient-data__subtitle--consult">Birth History</h2>
                            <ul class="patient-data__list patient-data__list--consult">
                                <li>
                                    <div class="patient-data__list-title">Was the child born at full term?</div>

                                    <div class="patient-data__list-description" data-template="yesno-template" data-bind="source:intakeForm.isChildBornFullTerm">

                                        No

                                    </div>
                                </li>
                                <li>
                                    <div class="patient-data__list-title">Was the child born vaginally?</div>

                                    <div class="patient-data__list-description" data-template="yesno-template" data-bind="source:intakeForm.isChildBornVaginally">

                                        No

                                    </div>
                                </li>
                                <li>
                                    <div class="patient-data__list-title">Was the child born discharged with the mother?</div>

                                    <div class="patient-data__list-description" data-template="yesno-template" data-bind="source:intakeForm.isChildDischargeMother">

                                        No

                                    </div>
                                </li>
                                <li>
                                    <div class="patient-data__list-title">Was the child's vaccination up-to-date?</div>

                                    <div class="patient-data__list-description" data-template="yesno-template" data-bind="source:intakeForm.isVaccinationUpToDate">

                                        No

                                    </div>
                                </li>
                            </ul>
                        </div>

                        <h2 class="patient-data__subtitle patient-data__subtitle--consult">Surgeries</h2>
                        <ul class="patient-data__list patient-data__list--consult" data-template="intakeItem_surgery" data-bind="source:intakeForm.priorSurgeories,visible:isPriorSurgeoriesAvailable" style="display: none;"></ul>

                        <ul class="patient-data__list patient-data__list--consult patient-data__list--empty" data-bind="invisible:isPriorSurgeoriesAvailable">
                            <li>None Reported</li>
                        </ul>

                        <h2 class="patient-data__subtitle patient-data__subtitle--consult">Medication Allergies</h2>
                        <ul class="patient-data__list patient-data__list--consult" data-template="chronictemplate" data-bind="source:intakeForm.medicationAllergies,visible:isMedicationAllergiesAvailable" style="display: none;"></ul>

                        <ul class="patient-data__list patient-data__list--consult patient-data__list--empty" data-bind="invisible:isMedicationAllergiesAvailable">
                            <li>None Reported</li>
                        </ul>

                        <h2 class="patient-data__subtitle patient-data__subtitle--consult">Current Medications</h2>
                        <ol class="patient-data__list patient-data__list--consult" data-template="chronictemplate" data-bind="source:intakeForm.takingMedications,visible:isTakingMedicationsAvailable" style="display: none;"></ol>

                        <ol class="patient-data__list patient-data__list--consult patient-data__list--empty" data-bind="invisible:isTakingMedicationsAvailable">
                            <li>None Reported</li>
                        </ol>
                    </div>
                </div>

                <div class="patient-data patient-data__patient-profile k-content" id="clinician-consultation-tabs-2" role="tabpanel" aria-hidden="true" aria-expanded="false">
                    <div class="patient-data__info-block patient-data__info-block--small-block">
                        <div class="patient-data__header patient-data__header--patient-details">Age</div>
                        <div class="patient-data__icon">
                            <span class="icon_hash"></span>
                        </div>
                        <div class="patient-data__data">
                    <span>
                        <div data-bind="html:patientInfomation.age">41</div>

                        <div class="dob">DOB: <span data-bind="html:patientInfomation.dobInfo">2/17/1975</span></div>
                    </span>
                        </div>
                    </div>
                    <div class="patient-data__info-block patient-data__info-block--small-block">
                        <div class="patient-data__header patient-data__header--patient-details">Gender</div>
                        <div class="patient-data__icon">
                            <span class="icon_female"></span>
                        </div>
                        <div class="patient-data__data"><span data-bind="html:patientInfomation.gender">M</span></div>
                    </div>
                    <div class="patient-data__info-block patient-data__info-block--small-block">
                        <div class="patient-data__header patient-data__header--patient-details">Height</div>
                        <div class="patient-data__icon">
                            <span class="icon_height"></span>
                        </div>
                        <div class="patient-data__data"><span data-bind="html:formatHeight">6ft 0in</span></div>
                    </div>
                    <div class="patient-data__info-block patient-data__info-block--small-block">
                        <div class="patient-data__header patient-data__header--patient-details">Weight</div>
                        <div class="patient-data__icon">
                            <span class="icon_weight"></span>
                        </div>
                        <div class="patient-data__data"><span data-bind="html:formatWeight">220lbs</span></div>
                    </div>
                    <div class="patient-data__info-block patient-data__info-block--small-block">
                        <div class="patient-data__header patient-data__header--patient-details">Eye Color</div>
                        <div class="patient-data__icon">
                            <span class="icon_eye"></span>
                        </div>
                        <div class="patient-data__data"><span data-bind="html:patientInfomation.eyeColor">N/A</span></div>
                    </div>
                    <div class="patient-data__info-block patient-data__info-block--small-block">
                        <div class="patient-data__header patient-data__header--patient-details">Hair Color</div>
                        <div class="patient-data__icon">
                            <span class="icon_hair"></span>
                        </div>
                        <div class="patient-data__data"><span data-bind="html:patientInfomation.hairColor">Brown</span></div>
                    </div>
                    <div class="patient-data__info-block patient-data__info-block--small-block">
                        <div class="patient-data__header patient-data__header--patient-details">Ethnicity</div>
                        <div class="patient-data__icon">
                            <span class="icon_globe"></span>
                        </div>
                        <div class="patient-data__data"><span data-bind="html:patientInfomation.ethnicity">N/A</span></div>
                    </div>
                    <div class="patient-data__info-block patient-data__info-block--small-block">
                        <div class="patient-data__header patient-data__header--patient-details">Blood Type</div>
                        <div class="patient-data__icon">
                            <span class="icon_drop"></span>
                        </div>
                        <div class="patient-data__data"><span data-bind="html:patientInfomation.bloodType">N/A</span></div>
                    </div>

                    <div class="patient-data__info-block">
                        <div class="patient-data__title patient-data__title--large">Contact Info</div>

                        <div class="patient-data__info-block--large-block">
                            <dl class="details-list">
                                <dt class="details-list__title">Address:</dt>
                                <dd class="details-list__description"><address data-bind="html:patientInfomation.address">3851 Landings Dr,
                                        Boca Raton, FL 33496, USA</address></dd>

                                <dt class="details-list__title">Home Phone:</dt>
                                <dd class="details-list__description"><a href="tel:N/A" data-rel="external" data-bind="html:patientInfomation.homePhone,attr:{href:patientHomePhoneLink}">N/A</a></dd>

                                <dt class="details-list__title">Cell Phone:</dt>
                                <dd class="details-list__description"><a href="tel:+1561-289-9988" data-rel="external" data-bind="html:patientInfomation.mobilePhone,attr:{href:patientMobilePhoneLink}">(561) 289-9988</a></dd>

                                <dt class="details-list__title details-list__title--small">Email:</dt>
                                <dd class="details-list__description details-list__description--large"><a href="mailto:phil@myvirtualdoctor.com" data-bind="html:patientInfomation.email,attr:{href:patientEmailLink}">phil@myvirtualdoctor.com</a></dd>
                            </dl>
                        </div>
                    </div>

                    <div class="patient-data__info-block">
                        <h2 class="patient-data__title patient-data__title--large">Medical History</h2>

                        <h3 class="patient-data__subtitle patient-data__subtitle--consult">Chronic Conditions</h3>
                        <ul class="patient-data__list patient-data__list--consult" data-template="chronictemplate" data-bind="source:intakeForm.medicalCondition,visible:isMedicalConditionAvailable" style="display: none;"></ul>

                        <ul class="patient-data__list patient-data__list--consult patient-data__list--empty" data-bind="invisible:isMedicalConditionAvailable">
                            <li>None Reported</li>
                        </ul>

                        <h2 class="patient-data__subtitle patient-data__subtitle--consult">Surgeries</h2>
                        <ul class="patient-data__list patient-data__list--consult" data-template="intakeItem_surgery" data-bind="source:intakeForm.priorSurgeories,visible:isPriorSurgeoriesAvailable" style="display: none;"></ul>

                        <ul class="patient-data__list patient-data__list--consult patient-data__list--empty" data-bind="invisible:isMedicationAllergiesAvailable">
                            <li>None Reported</li>
                        </ul>

                        <h2 class="patient-data__subtitle patient-data__subtitle--consult">Medication Allergies</h2>
                        <ul class="patient-data__list patient-data__list--consult" data-template="chronictemplate" data-bind="source:intakeForm.medicationAllergies,visible:isMedicationAllergiesAvailable" style="display: none;"></ul>

                        <ul class="patient-data__list patient-data__list--consult patient-data__list--empty" data-bind="invisible:isMedicationAllergiesAvailable">
                            <li>None Reported</li>
                        </ul>

                        <h2 class="patient-data__subtitle patient-data__subtitle--consult">Current Medications</h2>
                        <ol class="patient-data__list patient-data__list--consult" data-template="chronictemplate" data-bind="source:intakeForm.takingMedications,visible:isTakingMedicationsAvailable" style="display: none;"></ol>

                        <ol class="patient-data__list patient-data__list--consult patient-data__list--empty" data-bind="invisible:isTakingMedicationsAvailable">
                            <li>None Reported</li>
                        </ol>
                    </div>

                    <div class="patient-data__info-block">
                        <div class="patient-data__title patient-data__title--large">Care Info</div>
                    </div>
                    <div class="patient-data__header patient-data__header--dark-bg">Caregivers</div>
                    <div data-template="caregivers" data-bind="source:depedentInformation.data">
                        <div class="patient-data__info-block patient-data__info-block--small-block" data-patientid="5505">
                            <div class="userpic userpic--patient-date-large userpic--white-border">
                                <img src="/images/default-user.jpg">
                            </div>
                            <div class="patient-data__name patient-data__name--physician">Pauly Vasta</div>
                        </div>

                    </div>

                    <div class="patient-data__header patient-data__header--dark-bg">Specialist Physician(s)</div>
                    <div class="patient-data__info-block patient-data__info-block--small-block">
                        <div class="userpic userpic--patient-date-large userpic--white-border">
                            <img src="/images/Patient-Male.gif">
                        </div>
                        <div class="patient-data__name patient-data__name--physician" data-bind="html:patientInfomation.physicianSpecialist">N/A</div>
                    </div>

                    <div class="patient-data__header patient-data__header--dark-bg">Preferred Physician</div>
                    <div class="patient-data__info-block patient-data__info-block--small-block">
                        <div class="userpic userpic--patient-date-large userpic--white-border">
                            <img src="/images/Patient-Male.gif">
                        </div>
                        <div class="patient-data__name patient-data__name--physician" data-bind="html:patientInfomation.primaryPhysician">N/A</div>
                    </div>

                    <div class="patient-data__header patient-data__header--dark-bg">Preferred Pharmacy</div>
                    <div class="patient-data__info-block patient-data__info-block--large-block">
                        <h3 data-bind="html:patientInfomation.preferedPharmacy" class="patient-data__block-title">N/A</h3>
                        <address class="patient-data__block-description" data-bind="html:patientInfomation.pharmacyContact">N/A</address>

                        <div id="map-canvas" class="patient-data__map-canvas"></div>
                    </div>
                </div>


                <div class="patient-data patient-data__message k-content" id="clinician-consultation-tabs-3" role="tabpanel" aria-hidden="true" aria-expanded="false">


                    <section id="chatCont" class="chat chat--admin" style="max-width:99.59%;">

                        <div class="chat__messages-list">
                            <div class="chat__messages-container js-chat-container" id="msgCont">

                            </div>
                        </div>

                        <div class="chat__controls chat__controls--upload">
                            <div class="chat__input">
                                <button type="button" class="chat__upload">
                                    <span class="icon_arrow-with-circle-up"></span>
                                </button>

                                <input type="text" id="chatMessageCtl" data-bind="onEnterKey: onKeyUp" placeholder="Type a message here." class="field js-message">
                            </div>
                        </div>

                    </section>
                </div>


                <div class="patient-data patient-data__soap k-content" id="clinician-consultation-tabs-4" role="tabpanel" aria-hidden="true" aria-expanded="false">
                    <div class="patient-data__info-block">
                        <div class="patient-data__title patient-data__title--large">Soap Note</div>

                        <form id="soapnotes-form">
                            <div id="soap-s" class="patient-data__soap-content">
                                <h4>Subjective</h4>
                                <table cellspacing="4" cellpadding="0" class="k-widget k-editor k-header k-editor-widget" role="presentation"><tbody><tr role="presentation"><td class="k-editor-toolbar-wrap" role="presentation"><ul class="k-editor-toolbar" role="toolbar" aria-controls="soap-field-s" data-role="editortoolbar"><li class="k-tool-group" role="presentation"><span class="k-editor-dropdown k-group-start k-group-end"><span title="Format" style="width: 110px;" class="k-widget k-dropdown k-header k-editor-widget" unselectable="on" role="listbox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-owns="" aria-disabled="false" aria-readonly="false" aria-busy="false"><span unselectable="on" class="k-dropdown-wrap k-state-default"><span unselectable="on" class="k-input">Format</span><span unselectable="on" class="k-select"><span unselectable="on" class="k-icon k-i-arrow-s">select</span></span></span><select title="Format" class="k-formatting k-decorated" data-role="selectbox" style="width: 110px; display: none;" unselectable="on"><option value="p">Paragraph</option><option value="blockquote">Quotation</option><option value="h1">Heading 1</option><option value="h2">Heading 2</option><option value="h3">Heading 3</option><option value="h4">Heading 4</option><option value="h5">Heading 5</option><option value="h6">Heading 6</option></select></span></span></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Bold"><span unselectable="on" class="k-tool-icon k-bold"></span><span class="k-tool-text">Bold</span></a><a href="" role="button" class="k-tool" unselectable="on" title="Italic"><span unselectable="on" class="k-tool-icon k-italic"></span><span class="k-tool-text">Italic</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Underline"><span unselectable="on" class="k-tool-icon k-underline"></span><span class="k-tool-text">Underline</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Align text left"><span unselectable="on" class="k-tool-icon k-justifyLeft"></span><span class="k-tool-text">Justify Left</span></a><a href="" role="button" class="k-tool" unselectable="on" title="Center text"><span unselectable="on" class="k-tool-icon k-justifyCenter"></span><span class="k-tool-text">Justify Center</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Align text right"><span unselectable="on" class="k-tool-icon k-justifyRight"></span><span class="k-tool-text">Justify Right</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Insert unordered list"><span unselectable="on" class="k-tool-icon k-insertUnorderedList"></span><span class="k-tool-text">Insert unordered list</span></a><a href="" role="button" class="k-tool" unselectable="on" title="Insert ordered list"><span unselectable="on" class="k-tool-icon k-insertOrderedList"></span><span class="k-tool-text">Insert ordered list</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Indent"><span unselectable="on" class="k-tool-icon k-indent"></span><span class="k-tool-text">Indent</span></a><a href="" role="button" class="k-tool k-group-end k-state-disabled" unselectable="on" title="Outdent"><span unselectable="on" class="k-tool-icon k-outdent"></span><span class="k-tool-text">Outdent</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Insert hyperlink"><span unselectable="on" class="k-tool-icon k-createLink"></span><span class="k-tool-text">Create Link</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Remove hyperlink"><span unselectable="on" class="k-tool-icon k-unlink"></span><span class="k-tool-text">Remove Link</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Insert image"><span unselectable="on" class="k-tool-icon k-insertImage"></span><span class="k-tool-text">Insert Image</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start k-group-end" data-popup="" unselectable="on" title="Create table"><span unselectable="on" class="k-tool-icon k-createTable"></span><span class="k-tool-text">Create table</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add column on the left"><span unselectable="on" class="k-tool-icon k-addColumnLeft"></span><span class="k-tool-text">Add column on the left</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add column on the right"><span unselectable="on" class="k-tool-icon k-addColumnRight"></span><span class="k-tool-text">Add column on the right</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add row above"><span unselectable="on" class="k-tool-icon k-addRowAbove"></span><span class="k-tool-text">Add row above</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add row below"><span unselectable="on" class="k-tool-icon k-addRowBelow"></span><span class="k-tool-text">Add row below</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Delete row"><span unselectable="on" class="k-tool-icon k-deleteRow"></span><span class="k-tool-text">Delete row</span></a><a href="" role="button" class="k-tool k-group-end k-state-disabled" unselectable="on" title="Delete column"><span unselectable="on" class="k-tool-icon k-deleteColumn"></span><span class="k-tool-text">Delete column</span></a></li></ul></td></tr><tr><td class="k-editable-area"><iframe title="Editable area. Press F10 for toolbar." frameborder="0" class="k-content" src='javascript:""'></iframe><textarea id="soap-field-s" data-bind="value:soapNote.subjective" data-role="editor" autocomplete="off" class="k-content k-raw-content" style="display: none;"></textarea></td></tr></tbody></table>
                                <div class="intake-data">
                                    <h4>Primary Concern:</h4>
                                    <p id="primaryConcern" data-bind="html:consultationInfomation.primaryConsern">Diarrhea or Constipation</p>
                                    <h4>Secondary Concern:</h4>
                                    <p id="SecondaryConcern" data-bind="html:consultationInfomation.secondaryConsern">N/A</p>
                                </div>
                                <div id="soap-o" class="patient-data__soap-content">
                                    <h4>Objective</h4>
                                    <table cellspacing="4" cellpadding="0" class="k-widget k-editor k-header k-editor-widget" role="presentation"><tbody><tr role="presentation"><td class="k-editor-toolbar-wrap" role="presentation"><ul class="k-editor-toolbar" role="toolbar" aria-controls="soap-field-o" data-role="editortoolbar"><li class="k-tool-group" role="presentation"><span class="k-editor-dropdown k-group-start k-group-end"><span title="Format" style="width: 110px;" class="k-widget k-dropdown k-header k-editor-widget" unselectable="on" role="listbox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-owns="" aria-disabled="false" aria-readonly="false" aria-busy="false"><span unselectable="on" class="k-dropdown-wrap k-state-default"><span unselectable="on" class="k-input">Format</span><span unselectable="on" class="k-select"><span unselectable="on" class="k-icon k-i-arrow-s">select</span></span></span><select title="Format" class="k-formatting k-decorated" data-role="selectbox" style="width: 110px; display: none;" unselectable="on"><option value="p">Paragraph</option><option value="blockquote">Quotation</option><option value="h1">Heading 1</option><option value="h2">Heading 2</option><option value="h3">Heading 3</option><option value="h4">Heading 4</option><option value="h5">Heading 5</option><option value="h6">Heading 6</option></select></span></span></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Bold"><span unselectable="on" class="k-tool-icon k-bold"></span><span class="k-tool-text">Bold</span></a><a href="" role="button" class="k-tool" unselectable="on" title="Italic"><span unselectable="on" class="k-tool-icon k-italic"></span><span class="k-tool-text">Italic</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Underline"><span unselectable="on" class="k-tool-icon k-underline"></span><span class="k-tool-text">Underline</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Align text left"><span unselectable="on" class="k-tool-icon k-justifyLeft"></span><span class="k-tool-text">Justify Left</span></a><a href="" role="button" class="k-tool" unselectable="on" title="Center text"><span unselectable="on" class="k-tool-icon k-justifyCenter"></span><span class="k-tool-text">Justify Center</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Align text right"><span unselectable="on" class="k-tool-icon k-justifyRight"></span><span class="k-tool-text">Justify Right</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Insert unordered list"><span unselectable="on" class="k-tool-icon k-insertUnorderedList"></span><span class="k-tool-text">Insert unordered list</span></a><a href="" role="button" class="k-tool" unselectable="on" title="Insert ordered list"><span unselectable="on" class="k-tool-icon k-insertOrderedList"></span><span class="k-tool-text">Insert ordered list</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Indent"><span unselectable="on" class="k-tool-icon k-indent"></span><span class="k-tool-text">Indent</span></a><a href="" role="button" class="k-tool k-group-end k-state-disabled" unselectable="on" title="Outdent"><span unselectable="on" class="k-tool-icon k-outdent"></span><span class="k-tool-text">Outdent</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Insert hyperlink"><span unselectable="on" class="k-tool-icon k-createLink"></span><span class="k-tool-text">Create Link</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Remove hyperlink"><span unselectable="on" class="k-tool-icon k-unlink"></span><span class="k-tool-text">Remove Link</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Insert image"><span unselectable="on" class="k-tool-icon k-insertImage"></span><span class="k-tool-text">Insert Image</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start k-group-end" data-popup="" unselectable="on" title="Create table"><span unselectable="on" class="k-tool-icon k-createTable"></span><span class="k-tool-text">Create table</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add column on the left"><span unselectable="on" class="k-tool-icon k-addColumnLeft"></span><span class="k-tool-text">Add column on the left</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add column on the right"><span unselectable="on" class="k-tool-icon k-addColumnRight"></span><span class="k-tool-text">Add column on the right</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add row above"><span unselectable="on" class="k-tool-icon k-addRowAbove"></span><span class="k-tool-text">Add row above</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add row below"><span unselectable="on" class="k-tool-icon k-addRowBelow"></span><span class="k-tool-text">Add row below</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Delete row"><span unselectable="on" class="k-tool-icon k-deleteRow"></span><span class="k-tool-text">Delete row</span></a><a href="" role="button" class="k-tool k-group-end k-state-disabled" unselectable="on" title="Delete column"><span unselectable="on" class="k-tool-icon k-deleteColumn"></span><span class="k-tool-text">Delete column</span></a></li></ul></td></tr><tr><td class="k-editable-area"><iframe title="Editable area. Press F10 for toolbar." frameborder="0" class="k-content" src='javascript:""'></iframe><textarea id="soap-field-o" data-bind="value:soapNote.objective" data-role="editor" autocomplete="off" class="k-content k-raw-content" style="display: none;"></textarea></td></tr></tbody></table>
                                    <div class="intake-data">
                                        <h4>Medication Allergies:</h4>
                                        <p id="MedicationAllergies" data-bind="html:intakeForm.medicationAllergiesInfo">None</p>
                                        <h4>Current Medications:</h4>
                                        <p id="CurrentMedications" data-bind="html:intakeForm.takingMedicationsInfo">None</p>
                                    </div>
                                </div>
                                <div id="soap-a" class="patient-data__soap-content">
                                    <h4>Assessment</h4>
                                    <table cellspacing="4" cellpadding="0" class="k-widget k-editor k-header k-editor-widget" role="presentation"><tbody><tr role="presentation"><td class="k-editor-toolbar-wrap" role="presentation"><ul class="k-editor-toolbar" role="toolbar" aria-controls="soap-field-a" data-role="editortoolbar"><li class="k-tool-group" role="presentation"><span class="k-editor-dropdown k-group-start k-group-end"><span title="Format" style="width: 110px;" class="k-widget k-dropdown k-header k-editor-widget" unselectable="on" role="listbox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-owns="" aria-disabled="false" aria-readonly="false" aria-busy="false"><span unselectable="on" class="k-dropdown-wrap k-state-default"><span unselectable="on" class="k-input">Format</span><span unselectable="on" class="k-select"><span unselectable="on" class="k-icon k-i-arrow-s">select</span></span></span><select title="Format" class="k-formatting k-decorated" data-role="selectbox" style="width: 110px; display: none;" unselectable="on"><option value="p">Paragraph</option><option value="blockquote">Quotation</option><option value="h1">Heading 1</option><option value="h2">Heading 2</option><option value="h3">Heading 3</option><option value="h4">Heading 4</option><option value="h5">Heading 5</option><option value="h6">Heading 6</option></select></span></span></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Bold"><span unselectable="on" class="k-tool-icon k-bold"></span><span class="k-tool-text">Bold</span></a><a href="" role="button" class="k-tool" unselectable="on" title="Italic"><span unselectable="on" class="k-tool-icon k-italic"></span><span class="k-tool-text">Italic</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Underline"><span unselectable="on" class="k-tool-icon k-underline"></span><span class="k-tool-text">Underline</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Align text left"><span unselectable="on" class="k-tool-icon k-justifyLeft"></span><span class="k-tool-text">Justify Left</span></a><a href="" role="button" class="k-tool" unselectable="on" title="Center text"><span unselectable="on" class="k-tool-icon k-justifyCenter"></span><span class="k-tool-text">Justify Center</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Align text right"><span unselectable="on" class="k-tool-icon k-justifyRight"></span><span class="k-tool-text">Justify Right</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Insert unordered list"><span unselectable="on" class="k-tool-icon k-insertUnorderedList"></span><span class="k-tool-text">Insert unordered list</span></a><a href="" role="button" class="k-tool" unselectable="on" title="Insert ordered list"><span unselectable="on" class="k-tool-icon k-insertOrderedList"></span><span class="k-tool-text">Insert ordered list</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Indent"><span unselectable="on" class="k-tool-icon k-indent"></span><span class="k-tool-text">Indent</span></a><a href="" role="button" class="k-tool k-group-end k-state-disabled" unselectable="on" title="Outdent"><span unselectable="on" class="k-tool-icon k-outdent"></span><span class="k-tool-text">Outdent</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Insert hyperlink"><span unselectable="on" class="k-tool-icon k-createLink"></span><span class="k-tool-text">Create Link</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Remove hyperlink"><span unselectable="on" class="k-tool-icon k-unlink"></span><span class="k-tool-text">Remove Link</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Insert image"><span unselectable="on" class="k-tool-icon k-insertImage"></span><span class="k-tool-text">Insert Image</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start k-group-end" data-popup="" unselectable="on" title="Create table"><span unselectable="on" class="k-tool-icon k-createTable"></span><span class="k-tool-text">Create table</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add column on the left"><span unselectable="on" class="k-tool-icon k-addColumnLeft"></span><span class="k-tool-text">Add column on the left</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add column on the right"><span unselectable="on" class="k-tool-icon k-addColumnRight"></span><span class="k-tool-text">Add column on the right</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add row above"><span unselectable="on" class="k-tool-icon k-addRowAbove"></span><span class="k-tool-text">Add row above</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add row below"><span unselectable="on" class="k-tool-icon k-addRowBelow"></span><span class="k-tool-text">Add row below</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Delete row"><span unselectable="on" class="k-tool-icon k-deleteRow"></span><span class="k-tool-text">Delete row</span></a><a href="" role="button" class="k-tool k-group-end k-state-disabled" unselectable="on" title="Delete column"><span unselectable="on" class="k-tool-icon k-deleteColumn"></span><span class="k-tool-text">Delete column</span></a></li></ul></td></tr><tr><td class="k-editable-area"><iframe title="Editable area. Press F10 for toolbar." frameborder="0" class="k-content" src='javascript:""'></iframe><textarea id="soap-field-a" data-bind="value:soapNote.assessment" data-role="editor" autocomplete="off" class="k-content k-raw-content" style="display: none;"></textarea></td></tr></tbody></table>
                                </div>
                                <div id="soap-p" class="patient-data__soap-content">
                                    <h4>Plan</h4>
                                    <table cellspacing="4" cellpadding="0" class="k-widget k-editor k-header k-editor-widget" role="presentation"><tbody><tr role="presentation"><td class="k-editor-toolbar-wrap" role="presentation"><ul class="k-editor-toolbar" role="toolbar" aria-controls="soap-field-p" data-role="editortoolbar"><li class="k-tool-group" role="presentation"><span class="k-editor-dropdown k-group-start k-group-end"><span title="Format" style="width: 110px;" class="k-widget k-dropdown k-header k-editor-widget" unselectable="on" role="listbox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-owns="" aria-disabled="false" aria-readonly="false" aria-busy="false"><span unselectable="on" class="k-dropdown-wrap k-state-default"><span unselectable="on" class="k-input">Format</span><span unselectable="on" class="k-select"><span unselectable="on" class="k-icon k-i-arrow-s">select</span></span></span><select title="Format" class="k-formatting k-decorated" data-role="selectbox" style="width: 110px; display: none;" unselectable="on"><option value="p">Paragraph</option><option value="blockquote">Quotation</option><option value="h1">Heading 1</option><option value="h2">Heading 2</option><option value="h3">Heading 3</option><option value="h4">Heading 4</option><option value="h5">Heading 5</option><option value="h6">Heading 6</option></select></span></span></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Bold"><span unselectable="on" class="k-tool-icon k-bold"></span><span class="k-tool-text">Bold</span></a><a href="" role="button" class="k-tool" unselectable="on" title="Italic"><span unselectable="on" class="k-tool-icon k-italic"></span><span class="k-tool-text">Italic</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Underline"><span unselectable="on" class="k-tool-icon k-underline"></span><span class="k-tool-text">Underline</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Align text left"><span unselectable="on" class="k-tool-icon k-justifyLeft"></span><span class="k-tool-text">Justify Left</span></a><a href="" role="button" class="k-tool" unselectable="on" title="Center text"><span unselectable="on" class="k-tool-icon k-justifyCenter"></span><span class="k-tool-text">Justify Center</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Align text right"><span unselectable="on" class="k-tool-icon k-justifyRight"></span><span class="k-tool-text">Justify Right</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Insert unordered list"><span unselectable="on" class="k-tool-icon k-insertUnorderedList"></span><span class="k-tool-text">Insert unordered list</span></a><a href="" role="button" class="k-tool" unselectable="on" title="Insert ordered list"><span unselectable="on" class="k-tool-icon k-insertOrderedList"></span><span class="k-tool-text">Insert ordered list</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Indent"><span unselectable="on" class="k-tool-icon k-indent"></span><span class="k-tool-text">Indent</span></a><a href="" role="button" class="k-tool k-group-end k-state-disabled" unselectable="on" title="Outdent"><span unselectable="on" class="k-tool-icon k-outdent"></span><span class="k-tool-text">Outdent</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start" unselectable="on" title="Insert hyperlink"><span unselectable="on" class="k-tool-icon k-createLink"></span><span class="k-tool-text">Create Link</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Remove hyperlink"><span unselectable="on" class="k-tool-icon k-unlink"></span><span class="k-tool-text">Remove Link</span></a><a href="" role="button" class="k-tool k-group-end" unselectable="on" title="Insert image"><span unselectable="on" class="k-tool-icon k-insertImage"></span><span class="k-tool-text">Insert Image</span></a></li><li class="k-tool-group k-button-group" role="presentation"><a href="" role="button" class="k-tool k-group-start k-group-end" data-popup="" unselectable="on" title="Create table"><span unselectable="on" class="k-tool-icon k-createTable"></span><span class="k-tool-text">Create table</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add column on the left"><span unselectable="on" class="k-tool-icon k-addColumnLeft"></span><span class="k-tool-text">Add column on the left</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add column on the right"><span unselectable="on" class="k-tool-icon k-addColumnRight"></span><span class="k-tool-text">Add column on the right</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add row above"><span unselectable="on" class="k-tool-icon k-addRowAbove"></span><span class="k-tool-text">Add row above</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Add row below"><span unselectable="on" class="k-tool-icon k-addRowBelow"></span><span class="k-tool-text">Add row below</span></a><a href="" role="button" class="k-tool k-state-disabled" unselectable="on" title="Delete row"><span unselectable="on" class="k-tool-icon k-deleteRow"></span><span class="k-tool-text">Delete row</span></a><a href="" role="button" class="k-tool k-group-end k-state-disabled" unselectable="on" title="Delete column"><span unselectable="on" class="k-tool-icon k-deleteColumn"></span><span class="k-tool-text">Delete column</span></a></li></ul></td></tr><tr><td class="k-editable-area"><iframe title="Editable area. Press F10 for toolbar." frameborder="0" class="k-content" src='javascript:""'></iframe><textarea id="soap-field-p" data-bind="value:soapNote.plan" data-role="editor" autocomplete="off" class="k-content k-raw-content" style="display: none;"></textarea></td></tr></tbody></table>
                                </div>
                                <div class="patient-data__soap-content" data-bind="visible:hospitalMedicaCodingConfiguration.isPrimaryOn" style="">
                                    <h4><span data-bind="text: hospitalMedicaCodingConfiguration.primaryCodingSystemName">CPT</span> Codes(s)</h4>
                                    <select id="primaryMedCodesSelect" style="width: 98%" tabindex="-1" class="select2-hidden-accessible" aria-hidden="true"></select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 98%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-primaryMedCodesSelect-container"><span class="select2-selection__rendered" id="select2-primaryMedCodesSelect-container"></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                    <p><i>Please enter a full code or keyword</i></p>
                                </div>
                                <div class="patient-data__soap-content" data-bind="visible:hospitalMedicaCodingConfiguration.isSecondaryOn" style="display: none;">
                                    <h4><span data-bind="text: hospitalMedicaCodingConfiguration.secondaryCodingSystemName"></span> Codes(s)</h4>
                                    <select id="secondaryMedCodesSelect" style="width: 98%" multiple="multiple"></select>
                                    <p><i>Please enter a full code or keyword</i></p>
                                </div>
                                <button id="btnGo" class="btn btn-primary button__brand" data-bind="click:onSaveClick"><span>Submit</span></button>
                            </div>
                        </form>
                    </div>

                </div>

                <div class="patient-data patient-data__files k-content" id="clinician-consultation-tabs-5" role="tabpanel" aria-hidden="true" aria-expanded="false">
                    <div id="fileSharingSection"><div id="file-sharing">

                            <div id="splitter">
                                <div class="pane-2 patientFiles overlay" id="bottomFiles" style="min-height: 470px;position:relative;">

                                    <div class="heading">
                                        <span class="icon_archive"></span>
                                        <span class="filesinfo">
                    <span>Patient Files</span>
                    <span class="small" data-bind="visible: isFolderSizeVisible"><span data-bind="text: topFolderSize" style="display: inline;">6.91MB</span> out of <span data-bind="text: maxFolderSize" style="display: inline;">10.00GB</span></span>
                </span>
                                        <span class="uploadFiles icon_upload-cloud" id="bottomUploadFiles" data-bind="visible: isWritable">Upload Files</span>
                                        <span class="searchBox">
                    <input type="text" class="k-input search-files-bottom" placeholder="Search files..." data-bind="events: { change: search }">
                </span>
                                    </div>

                                    <div class="k-widget k-upload k-header k-upload-empty" style="display: none;"><div class="k-dropzone icon_upload-cloud"><div class="k-button k-upload-button"><input name="cloudfilesPatient" id="cloudfilesPatient" type="file" data-role="upload" multiple autocomplete="off"><span>Select files...</span></div><em>drop files here to upload</em></div></div>

                                    <ul class="breadcrumbs" data-template="breadcrumb-template" data-bind="source: parents">
                                        <li data-bind="attr: {class: breadCrumbColor}" class="last">
                                            <a href="javascript:void(0)" data-bind="text: name, click: drill, attr: {class: breadCrumbColor}" class="last">Patient Home</a>
                                            <a data-bind="click: mkdir ,attr: {class: addFolderColor}" class="icon_new-folder breadcrumbs-add-folder"><span data-bind="attr:{class: addFolderColor}" title="Add Folder" style="line-height:31px" class="icon_new-folder breadcrumbs-add-folder"></span></a>
                                        </li>
                                    </ul>
                                    <div class="scrollable-area patientArea" style="height: 400px; position: relative; width: 100% !important; overflow: auto; overflow-y: scroll;">
                                        <table id="bottomTable" class="filesTable" cellspacing="0" cellpadding="0" data-role="draggable">
                                            <thead>
                                            <tr style="background-color: #EAEAEA;">
                                                <th class="bulkActions" style="text-align: center; width: 80px">
                                                    <ul class="actions" style="display: none">
                                                        <li>
                                                            <span class="toggle" id="bottomBulkMenuToggle"></span>
                                                            <ul id="bottomBulkMenu">
                                                                <li><span class="icon_eye"></span>View File</li>
                                                                <li><span class="icon_download"></span>Download File</li>
                                                                <li><span class="icon_link"></span>Public Link</li>
                                                                <li><span class="icon_info2"></span>File Properties</li>
                                                                <li><span class="icon_copy"></span>Copy/Move</li>
                                                                <li><span class="icon_new-folder"></span>Add Folder</li>
                                                                <li><span class="icon_trash"></span>Delete File</li>
                                                                <li><span class="icon_attachment"></span>Attach to Consult</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                    <span class="actions-title" style="display: none">Actions</span>
                                                </th>
                                                <th class="name bottomName"><span class="" data-parameter="bottomName" data-bind="click: sort"><span class="opt">File</span> Name</span></th>
                                                <th class="date bottomDate"><span class="" data-parameter="bottomDate" data-bind="click: sort">Uploaded</span></th>
                                                <th class="tags"><span>Tags</span></th>
                                                <th class="actions"></th>
                                            </tr>
                                            </thead>
                                            <tbody><tr style="text-align: center; font-weight: bold; display: none;" data-bind="visible: isSearchEmpty"><td colspan="5" style="font-size: 15px !important;">No file or folder matched your search</td></tr>
                                            </tbody><tbody data-template="file-list-template" data-bind="source: allFiles, invisible: isEmpty">
                                            <tr class="trFile" data-bind="attr:{ id:id}" id="a7779de3-9279-46a3-a5ed-2ebef631f250">
                                                <td data-bind="click: drill, events: { dblclick: download }">
                                                    <div class="" style="text-align: center;margin-left:0px" data-bind="attr: { class: Paperclip }">
                                                        <!-- input type="checkbox" -->
                                                        <img data-bind="attr: { src: fileIcon, title: fileType }" alt="" src="/images/icon-folder.png" title="folder">
                                                    </div>
                                                </td>
                                                <td class="name name-wrap" data-bind="text: name, click: drill, events: { dblclick: download }">test share</td>
                                                <td class="date" data-bind="text: dateModifiedShort, click: drill, events: { dblclick: download }">06/21/2016</td>
                                                <td class="tag">
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <ul class="ulT" data-template="tag-li-template" data-bind="source: firstTwoTags"></ul>
                                                            </td>
                                                            <td style="padding-right:5px !important;">
                                                                <div class="nav2">
                                                                    <ul data-bind="visible: isTag2ToolTipVisible" style="display: none;">
                                                                        <li>
                                                                            <a class="fa" data-bind="text: tag2ToolTipNumber" href="javascript:void(0);">..0</a>
                                                                            <ul data-template="tag-li-tooltip-template" data-bind="source: tootTipTags"></ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="action" style="text-align: center;">
                                                    <ul class="actions">
                                                        <li>
                                                            <a href="javascript:void(0)" class="toggle icon_chevron-down" data-bind="click: openMenu"></a>
                                                            <ul class="ulMenu" data-bind="attr: {id: menuId}" id="menu_a7779de3-9279-46a3-a5ed-2ebef631f250">
                                                                <li data-bind="visible: displayViewFile" data-parameter="display" style="display: none;"><span class="icon_eye"></span>View File</li>
                                                                <li data-bind="click: download, visible: displayDownload" style="display: none;"><span class="icon_download"></span>Download File</li>
                                                                <li data-bind="click: share, visible: displaySend" style="display: none;"><span class="icon_link"></span>Public Link</li>
                                                                <li data-bind="click: properties, visible: displayProperties"><span class="icon_info2"></span><span data-bind="text: displayTitle">Folder</span> Properties</li>
                                                                <li data-bind="click: copy, visible: displayCopy" data-parameter="top" style="display: none;"><span class="icon_copy"></span>Copy To</li>
                                                                <li data-bind="click: mkdir, visible: displayAddFolder"><span class="icon_new-folder"></span>Add Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFolder" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFile" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete File</li>
                                                                <li data-bind="click: attachToConsult, visible: displayAttachConsult" class="attached" style="display: none;"><span class="icon_attachment"></span>Attach to Consult</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr class="trFile" data-bind="attr:{ id:id}" id="2431">
                                                <td data-bind="click: drill, events: { dblclick: download }">
                                                    <div class="" style="text-align: center;margin-left:0px" data-bind="attr: { class: Paperclip }">
                                                        <!-- input type="checkbox" -->
                                                        <img data-bind="attr: { src: fileIcon, title: fileType }" alt="" src="/images/filetypes/png/jpg.png" title="">
                                                    </div>
                                                </td>
                                                <td class="name name-wrap" data-bind="text: name, click: drill, events: { dblclick: download }">Dr_Sachin_Parikh_N3215_Cr op32_WEB.jpg</td>
                                                <td class="date" data-bind="text: dateModifiedShort, click: drill, events: { dblclick: download }">06/21/2016</td>
                                                <td class="tag">
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <ul class="ulT" data-template="tag-li-template" data-bind="source: firstTwoTags"></ul>
                                                            </td>
                                                            <td style="padding-right:5px !important;">
                                                                <div class="nav2">
                                                                    <ul data-bind="visible: isTag2ToolTipVisible" style="display: none;">
                                                                        <li>
                                                                            <a class="fa" data-bind="text: tag2ToolTipNumber" href="javascript:void(0);">..0</a>
                                                                            <ul data-template="tag-li-tooltip-template" data-bind="source: tootTipTags"></ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="action" style="text-align: center;">
                                                    <ul class="actions">
                                                        <li>
                                                            <a href="javascript:void(0)" class="toggle icon_chevron-down" data-bind="click: openMenu"></a>
                                                            <ul class="ulMenu" data-bind="attr: {id: menuId}" id="menu_2431">
                                                                <li data-bind="visible: displayViewFile" data-parameter="display" style="display: none;"><span class="icon_eye"></span>View File</li>
                                                                <li data-bind="click: download, visible: displayDownload"><span class="icon_download"></span>Download File</li>
                                                                <li data-bind="click: share, visible: displaySend"><span class="icon_link"></span>Public Link</li>
                                                                <li data-bind="click: properties, visible: displayProperties"><span class="icon_info2"></span><span data-bind="text: displayTitle">File</span> Properties</li>
                                                                <li data-bind="click: copy, visible: displayCopy" data-parameter="top"><span class="icon_copy"></span>Copy To</li>
                                                                <li data-bind="click: mkdir, visible: displayAddFolder" style="display: none;"><span class="icon_new-folder"></span>Add Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFolder" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFile" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete File</li>
                                                                <li data-bind="click: attachToConsult, visible: displayAttachConsult" class="attached" style="display: none;"><span class="icon_attachment"></span>Attach to Consult</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr class="trFile" data-bind="attr:{ id:id}" id="2433">
                                                <td data-bind="click: drill, events: { dblclick: download }">
                                                    <div class="" style="text-align: center;margin-left:0px" data-bind="attr: { class: Paperclip }">
                                                        <!-- input type="checkbox" -->
                                                        <img data-bind="attr: { src: fileIcon, title: fileType }" alt="" src="/images/filetypes/png/jpg.png" title="">
                                                    </div>
                                                </td>
                                                <td class="name name-wrap" data-bind="text: name, click: drill, events: { dblclick: download }">phil profile.jpg</td>
                                                <td class="date" data-bind="text: dateModifiedShort, click: drill, events: { dblclick: download }">06/21/2016</td>
                                                <td class="tag">
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <ul class="ulT" data-template="tag-li-template" data-bind="source: firstTwoTags"></ul>
                                                            </td>
                                                            <td style="padding-right:5px !important;">
                                                                <div class="nav2">
                                                                    <ul data-bind="visible: isTag2ToolTipVisible" style="display: none;">
                                                                        <li>
                                                                            <a class="fa" data-bind="text: tag2ToolTipNumber" href="javascript:void(0);">..0</a>
                                                                            <ul data-template="tag-li-tooltip-template" data-bind="source: tootTipTags"></ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="action" style="text-align: center;">
                                                    <ul class="actions">
                                                        <li>
                                                            <a href="javascript:void(0)" class="toggle icon_chevron-down" data-bind="click: openMenu"></a>
                                                            <ul class="ulMenu" data-bind="attr: {id: menuId}" id="menu_2433">
                                                                <li data-bind="visible: displayViewFile" data-parameter="display" style="display: none;"><span class="icon_eye"></span>View File</li>
                                                                <li data-bind="click: download, visible: displayDownload"><span class="icon_download"></span>Download File</li>
                                                                <li data-bind="click: share, visible: displaySend"><span class="icon_link"></span>Public Link</li>
                                                                <li data-bind="click: properties, visible: displayProperties"><span class="icon_info2"></span><span data-bind="text: displayTitle">File</span> Properties</li>
                                                                <li data-bind="click: copy, visible: displayCopy" data-parameter="top"><span class="icon_copy"></span>Copy To</li>
                                                                <li data-bind="click: mkdir, visible: displayAddFolder" style="display: none;"><span class="icon_new-folder"></span>Add Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFolder" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFile" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete File</li>
                                                                <li data-bind="click: attachToConsult, visible: displayAttachConsult" class="attached" style="display: none;"><span class="icon_attachment"></span>Attach to Consult</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr class="trFile" data-bind="attr:{ id:id}" id="2685">
                                                <td data-bind="click: drill, events: { dblclick: download }">
                                                    <div class="" style="text-align: center;margin-left:0px" data-bind="attr: { class: Paperclip }">
                                                        <!-- input type="checkbox" -->
                                                        <img data-bind="attr: { src: fileIcon, title: fileType }" alt="" src="/images/filetypes/png/png.png" title="">
                                                    </div>
                                                </td>
                                                <td class="name name-wrap" data-bind="text: name, click: drill, events: { dblclick: download }">Picture 1.png</td>
                                                <td class="date" data-bind="text: dateModifiedShort, click: drill, events: { dblclick: download }">06/25/2016</td>
                                                <td class="tag">
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <ul class="ulT" data-template="tag-li-template" data-bind="source: firstTwoTags"></ul>
                                                            </td>
                                                            <td style="padding-right:5px !important;">
                                                                <div class="nav2">
                                                                    <ul data-bind="visible: isTag2ToolTipVisible" style="display: none;">
                                                                        <li>
                                                                            <a class="fa" data-bind="text: tag2ToolTipNumber" href="javascript:void(0);">..0</a>
                                                                            <ul data-template="tag-li-tooltip-template" data-bind="source: tootTipTags"></ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="action" style="text-align: center;">
                                                    <ul class="actions">
                                                        <li>
                                                            <a href="javascript:void(0)" class="toggle icon_chevron-down" data-bind="click: openMenu"></a>
                                                            <ul class="ulMenu" data-bind="attr: {id: menuId}" id="menu_2685">
                                                                <li data-bind="visible: displayViewFile" data-parameter="display" style="display: none;"><span class="icon_eye"></span>View File</li>
                                                                <li data-bind="click: download, visible: displayDownload"><span class="icon_download"></span>Download File</li>
                                                                <li data-bind="click: share, visible: displaySend"><span class="icon_link"></span>Public Link</li>
                                                                <li data-bind="click: properties, visible: displayProperties"><span class="icon_info2"></span><span data-bind="text: displayTitle">File</span> Properties</li>
                                                                <li data-bind="click: copy, visible: displayCopy" data-parameter="top"><span class="icon_copy"></span>Copy To</li>
                                                                <li data-bind="click: mkdir, visible: displayAddFolder" style="display: none;"><span class="icon_new-folder"></span>Add Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFolder" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFile" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete File</li>
                                                                <li data-bind="click: attachToConsult, visible: displayAttachConsult" class="attached" style="display: none;"><span class="icon_attachment"></span>Attach to Consult</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr class="trFile" data-bind="attr:{ id:id}" id="2683">
                                                <td data-bind="click: drill, events: { dblclick: download }">
                                                    <div class="" style="text-align: center;margin-left:0px" data-bind="attr: { class: Paperclip }">
                                                        <!-- input type="checkbox" -->
                                                        <img data-bind="attr: { src: fileIcon, title: fileType }" alt="" src="/images/filetypes/png/iso.png" title="">
                                                    </div>
                                                </td>
                                                <td class="name name-wrap" data-bind="text: name, click: drill, events: { dblclick: download }">superman icon - Google Se arch.webarchive</td>
                                                <td class="date" data-bind="text: dateModifiedShort, click: drill, events: { dblclick: download }">06/25/2016</td>
                                                <td class="tag">
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <ul class="ulT" data-template="tag-li-template" data-bind="source: firstTwoTags"></ul>
                                                            </td>
                                                            <td style="padding-right:5px !important;">
                                                                <div class="nav2">
                                                                    <ul data-bind="visible: isTag2ToolTipVisible" style="display: none;">
                                                                        <li>
                                                                            <a class="fa" data-bind="text: tag2ToolTipNumber" href="javascript:void(0);">..0</a>
                                                                            <ul data-template="tag-li-tooltip-template" data-bind="source: tootTipTags"></ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="action" style="text-align: center;">
                                                    <ul class="actions">
                                                        <li>
                                                            <a href="javascript:void(0)" class="toggle icon_chevron-down" data-bind="click: openMenu"></a>
                                                            <ul class="ulMenu" data-bind="attr: {id: menuId}" id="menu_2683">
                                                                <li data-bind="visible: displayViewFile" data-parameter="display" style="display: none;"><span class="icon_eye"></span>View File</li>
                                                                <li data-bind="click: download, visible: displayDownload"><span class="icon_download"></span>Download File</li>
                                                                <li data-bind="click: share, visible: displaySend"><span class="icon_link"></span>Public Link</li>
                                                                <li data-bind="click: properties, visible: displayProperties"><span class="icon_info2"></span><span data-bind="text: displayTitle">File</span> Properties</li>
                                                                <li data-bind="click: copy, visible: displayCopy" data-parameter="top"><span class="icon_copy"></span>Copy To</li>
                                                                <li data-bind="click: mkdir, visible: displayAddFolder" style="display: none;"><span class="icon_new-folder"></span>Add Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFolder" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFile" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete File</li>
                                                                <li data-bind="click: attachToConsult, visible: displayAttachConsult" class="attached" style="display: none;"><span class="icon_attachment"></span>Attach to Consult</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="no-data" data-bind="visible: isEmpty" style="text-align: center; font-weight: bold; display: none;">This folder is empty.</div>
                                    </div>
                                    <div class="listViewColumn" style="display: none;">
                                        <div class="listView"> </div>
                                        <div class="pager k-pager-wrap"> </div>
                                    </div><!-- / .listViewColumn -->
                                </div><!-- / .pane-2 -->

                                <div id="topFiles" class="pane-1 clinicianFiles overlay" style="min-height: 470px;position:relative;">
                                    <div class="heading">
                                        <section id="filesInfo">
                                            <span class="icon_archive"></span>
                                            <span class="filesinfo">
                        <span class="big">My Files</span>
                        <span class="small" data-bind="visible: isFolderSizeVisible"><span data-bind="text: topFolderSize" style="display: inline;">0.02MB</span> out of <span data-bind="text: maxFolderSize" style="display: inline;">10.00GB</span></span>
                    </span>
                                            <!--<span class="uploadFiles icon_upload-cloud" id="topUploadFiles" data-bind="visible: isWritable">Upload Files</span>-->
                                            <span class="searchBox">
                        <input type="text" class="k-input search-files" placeholder="Search files..." data-bind="events: { change: search, keyPress: preventEnter }">
                    </span>
                                        </section>
                                    </div>

                                    <div class="k-widget k-upload k-header k-upload-empty" style="display: none;"><div class="k-dropzone icon_upload-cloud"><div class="k-button k-upload-button"><input name="cloudfilesClinician" id="cloudfilesClinician" type="file" data-role="upload" multiple autocomplete="off"><span>Select files...</span></div><em>drop files here to upload</em></div></div>

                                    <ul class="breadcrumbs" data-template="breadcrumb-template" data-bind="source: parents">
                                        <li data-bind="attr: {class: breadCrumbColor}" class="last">
                                            <a href="javascript:void(0)" data-bind="text: name, click: drill, attr: {class: breadCrumbColor}" class="last">Home</a>
                                            <a data-bind="click: mkdir ,attr: {class: addFolderColor}" class="icon_new-folder breadcrumbs-add-folder"><span data-bind="attr:{class: addFolderColor}" title="Add Folder" style="line-height:31px" class="icon_new-folder breadcrumbs-add-folder"></span></a>
                                        </li>
                                    </ul>
                                    <div class="scrollable-area myArea" style="height: 400px; position: relative; width: 100% !important;  overflow: auto; overflow-y: scroll;">
                                        <table id="topTable" class="filesTable" cellpadding="0" cellspacing="0" data-role="draggable">
                                            <thead>
                                            <tr style="background-color: #EAEAEA;">
                                                <th class="bulkAction" style="text-align: center; width: 80px;">
                                                    <ul class="actions" style="display: none">
                                                        <li>
                                                            <span class="toggle" id="bulkMenuTop" data-bind="click: openTopBatchMenu"></span>
                                                            <ul id="menuTop" style="left: -5px;">
                                                                <li><span class="icon_eye"></span>View File</li>
                                                                <li><span class="icon_download"></span>Download File</li>
                                                                <li><span class="icon_link"></span>Public Link</li>
                                                                <li><span class="icon_info2"></span>File Properties</li>
                                                                <li><span>content</span>Copy/Move</li>
                                                                <li><span class="icon_new-folder"></span>Add Folder</li>
                                                                <li><span class="icon_trash"></span>Delete File</li>
                                                                <li><span class="icon_attachment"></span>Attach to Consult</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                    <span class="actions-title" style="display: none">Actions</span>
                                                </th>
                                                <th class="name topName"><span class="" data-parameter="topName" data-bind="click: sort">File Name</span></th><!-- sort active ascending -->
                                                <th class="date topDate"><span class="" data-parameter="topDate" data-bind="click: sort">Uploaded</span></th>
                                                <th class="tags"><span>Tags</span></th>
                                                <th class="actions"></th>
                                            </tr>
                                            </thead>
                                            <tbody><tr style="text-align: center; font-weight: bold; display: none;" data-bind="visible: isSearchEmpty"><td colspan="5" style="font-size: 15px !important;">No file or folder matched your search</td></tr>
                                            </tbody><tbody data-template="file-list-template" data-bind="source: allFiles, invisible: isEmpty">
                                            <tr class="trFile" data-bind="attr:{ id:id}" id="8d979040-c281-4254-b89a-ad228e2ef4d2">
                                                <td data-bind="click: drill, events: { dblclick: download }">
                                                    <div class="" style="text-align: center;margin-left:0px" data-bind="attr: { class: Paperclip }">
                                                        <!-- input type="checkbox" -->
                                                        <img data-bind="attr: { src: fileIcon, title: fileType }" alt="" src="/images/icon-folder.png" title="folder">
                                                    </div>
                                                </td>
                                                <td class="name name-wrap" data-bind="text: name, click: drill, events: { dblclick: download }">My Virtual Doctor Clinici ans Shared Folder</td>
                                                <td class="date" data-bind="text: dateModifiedShort, click: drill, events: { dblclick: download }">06/01/2016</td>
                                                <td class="tag">
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <ul class="ulT" data-template="tag-li-template" data-bind="source: firstTwoTags"></ul>
                                                            </td>
                                                            <td style="padding-right:5px !important;">
                                                                <div class="nav2">
                                                                    <ul data-bind="visible: isTag2ToolTipVisible" style="display: none;">
                                                                        <li>
                                                                            <a class="fa" data-bind="text: tag2ToolTipNumber" href="javascript:void(0);">..0</a>
                                                                            <ul data-template="tag-li-tooltip-template" data-bind="source: tootTipTags"></ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="action" style="text-align: center;">
                                                    <ul class="actions">
                                                        <li>
                                                            <a href="javascript:void(0)" class="toggle icon_chevron-down" data-bind="click: openMenu"></a>
                                                            <ul class="ulMenu" data-bind="attr: {id: menuId}" id="menu_8d979040-c281-4254-b89a-ad228e2ef4d2">
                                                                <li data-bind="visible: displayViewFile" data-parameter="display" style="display: none;"><span class="icon_eye"></span>View File</li>
                                                                <li data-bind="click: download, visible: displayDownload" style="display: none;"><span class="icon_download"></span>Download File</li>
                                                                <li data-bind="click: share, visible: displaySend" style="display: none;"><span class="icon_link"></span>Public Link</li>
                                                                <li data-bind="click: properties, visible: displayProperties"><span class="icon_info2"></span><span data-bind="text: displayTitle">Folder</span> Properties</li>
                                                                <li data-bind="click: copy, visible: displayCopy" data-parameter="top" style="display: none;"><span class="icon_copy"></span>Copy To</li>
                                                                <li data-bind="click: mkdir, visible: displayAddFolder"><span class="icon_new-folder"></span>Add Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFolder" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFile" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete File</li>
                                                                <li data-bind="click: attachToConsult, visible: displayAttachConsult" class="attached" style="display: none;"><span class="icon_attachment"></span>Attach to Consult</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr class="trFile" data-bind="attr:{ id:id}" id="2437">
                                                <td data-bind="click: drill, events: { dblclick: download }">
                                                    <div class="" style="text-align: center;margin-left:0px" data-bind="attr: { class: Paperclip }">
                                                        <!-- input type="checkbox" -->
                                                        <img data-bind="attr: { src: fileIcon, title: fileType }" alt="" src="/images/filetypes/png/jpg.png" title="">
                                                    </div>
                                                </td>
                                                <td class="name name-wrap" data-bind="text: name, click: drill, events: { dblclick: download }">phil profile.jpg</td>
                                                <td class="date" data-bind="text: dateModifiedShort, click: drill, events: { dblclick: download }">06/21/2016</td>
                                                <td class="tag">
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <ul class="ulT" data-template="tag-li-template" data-bind="source: firstTwoTags"></ul>
                                                            </td>
                                                            <td style="padding-right:5px !important;">
                                                                <div class="nav2">
                                                                    <ul data-bind="visible: isTag2ToolTipVisible" style="display: none;">
                                                                        <li>
                                                                            <a class="fa" data-bind="text: tag2ToolTipNumber" href="javascript:void(0);">..0</a>
                                                                            <ul data-template="tag-li-tooltip-template" data-bind="source: tootTipTags"></ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="action" style="text-align: center;">
                                                    <ul class="actions">
                                                        <li>
                                                            <a href="javascript:void(0)" class="toggle icon_chevron-down" data-bind="click: openMenu"></a>
                                                            <ul class="ulMenu" data-bind="attr: {id: menuId}" id="menu_2437">
                                                                <li data-bind="visible: displayViewFile" data-parameter="display" style="display: none;"><span class="icon_eye"></span>View File</li>
                                                                <li data-bind="click: download, visible: displayDownload"><span class="icon_download"></span>Download File</li>
                                                                <li data-bind="click: share, visible: displaySend"><span class="icon_link"></span>Public Link</li>
                                                                <li data-bind="click: properties, visible: displayProperties"><span class="icon_info2"></span><span data-bind="text: displayTitle">File</span> Properties</li>
                                                                <li data-bind="click: copy, visible: displayCopy" data-parameter="top"><span class="icon_copy"></span>Copy To</li>
                                                                <li data-bind="click: mkdir, visible: displayAddFolder" style="display: none;"><span class="icon_new-folder"></span>Add Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFolder" data-parameter="top" style="display: none;"><span class="icon_trash"></span>Delete Folder</li>
                                                                <li data-bind="click: delete, visible: displayDeleteFile" data-parameter="top"><span class="icon_trash"></span>Delete File</li>
                                                                <li data-bind="click: attachToConsult, visible: displayAttachConsult" class="attached" style="display: none;"><span class="icon_attachment"></span>Attach to Consult</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="no-data" data-bind="visible: isEmpty" style="text-align: center; font-weight: bold; display: none;">This folder is empty.</div>
                                    </div>
                                    <div class="listViewColumn" style="display: none;">
                                        <div class="listView"> </div>
                                        <div class="pager k-pager-wrap"> </div>
                                    </div><!-- / .listViewColumn -->

                                    <script id="breadcrumb-template" type="text/x-kendo-template">
                                        <li data-bind="attr: {class: breadCrumbColor}">
                                            <a href="javascript:void(0)" data-bind="text: name, click: drill, attr: {class: breadCrumbColor}"></a>
                                            <a data-bind="click: mkdir ,attr: {class: addFolderColor}"><span data-bind="attr:{class: addFolderColor}" title="Add Folder" style="line-height:31px"></span></a>
                                        </li>
                                    </script>

                                    <script id="file-list-template" type="text/x-kendo-template">
                                        <tr class='trFile' data-bind="attr:{ id:id}">
                                            <td data-bind="click: drill, events: { dblclick: download }">
                                                <div class='bulkWrap ' style='text-align: center;margin-left:0px' data-bind="attr: { class: Paperclip }">
                                                    <!-- input type="checkbox" -->
                                                    <img data-bind="attr: { src: fileIcon, title: fileType }" alt="">
                                                </div>
                                            </td>
                                            <td class='name name-wrap' data-bind="text: name, click: drill, events: { dblclick: download }"></td>
                                            <td class='date' data-bind="text: dateModifiedShort, click: drill, events: { dblclick: download }"></td>
                                            <td class='tag'>
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <ul class="ulT" data-template="tag-li-template" data-bind="source: firstTwoTags"></ul>
                                                        </td>
                                                        <td style="padding-right:5px !important;">
                                                            <div class="nav2">
                                                                <ul data-bind="visible: isTag2ToolTipVisible">
                                                                    <li>
                                                                        <a class="fa" data-bind="text: tag2ToolTipNumber" href="javascript:void(0);"></a>
                                                                        <ul data-template="tag-li-tooltip-template" data-bind="source: tootTipTags"></ul>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td class='action' style='text-align: center;'>
                                                <ul class="actions">
                                                    <li>
                                                        <a href="javascript:void(0)" class="toggle icon_chevron-down" data-bind="click: openMenu"></a>
                                                        <ul class="ulMenu" data-bind="attr: {id: menuId}">
                                                            <li data-bind="visible: displayViewFile" data-parameter="display"><span class="icon_eye"></span>View File</li>
                                                            <li data-bind="click: download, visible: displayDownload"><span class="icon_download"></span>Download File</li>
                                                            <li data-bind="click: share, visible: displaySend"><span class="icon_link"></span>Public Link</li>
                                                            <li data-bind="click: properties, visible: displayProperties"><span class="icon_info2"></span><span data-bind="text: displayTitle"></span> Properties</li>
                                                            <li data-bind="click: copy, visible: displayCopy" data-parameter="top"><span class="icon_copy"></span>Copy To</li>
                                                            <li data-bind="click: mkdir, visible: displayAddFolder"><span class="icon_new-folder"></span>Add Folder</li>
                                                            <li data-bind="click: delete, visible: displayDeleteFolder" data-parameter="top"><span class="icon_trash"></span>Delete Folder</li>
                                                            <li data-bind="click: delete, visible: displayDeleteFile" data-parameter="top"><span class="icon_trash"></span>Delete File</li>
                                                            <li data-bind="click: attachToConsult, visible: displayAttachConsult" class="attached"><span class="icon_attachment"></span>Attach to Consult</li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </script>

                                    <script id="tag-li-template" class="tag-li-template" type="text/x-kendo-template">
                                        <li style="cursor:pointer;" class="liT" data-tag-tagtext="#= tagId #" data-bind="text: tag, events:{click: searchTag}"></li>
                                    </script>

                                    <script id="tag-li-tooltip-template" class="tag-li-tooltip-template" type="text/x-kendo-template">
                                        <li><a href="javascript:void(0);" data-tag-tagtext="#= tagId #" data-bind="text: tag, events:{click: searchTag}" ></a></li>
                                    </script>

                                    <script type="text/x-kendo-template" id="tag-property-template" class="tag-property-template">
                                        <li><span style="cursor:pointer;" data-tag-tagtext="#= tag #" data-bind="text: tag"></span>
                                            <a href="javascript:void(0);" class="remove" title="Remove" data-bind="click: removeTag" data-tag-id="#= tagFileId #">x</a>
                                        </li>
                                    </script>

                                </div><!-- / .pane-1 -->
                            </div> <!-- / #splitter -->











                            <script type="text/javascript">
                                $(function() {
                                    $("#txtTag").keypress(function(e) {
                                        var key = e.which ? e.which : e.keyCode;
                                        if(key == 44){
                                            var btn = ".btn-ok-addTag";
                                            $(""+btn+"").focus();
                                            $(""+btn+"").click();

                                            event.preventDefault();
                                            return false;
                                        }
                                    });

                                    $(".search-files").keyup(function (e) {
                                        app.snapFileService.viewModel.search(e);
                                    });

                                    $(".search-files-bottom").keyup(function (e) {
                                        app.snapFileService.bottomViewModel.search(e);
                                    });

                                    //Event fired when clearing text input on IE with clear icon
                                    $('input[type="text"]').bind("mouseup", function (event) {
                                        var $input = $(this);
                                        var oldValue = $input.val();
                                        if (oldValue == "") {
                                            return;
                                        }
                                        setTimeout(function () {
                                            var newValue = $input.val();
                                            if (newValue == "") {
                                                var enterEvent = $.Event("keyup");
                                                enterEvent.which = 13;
                                                $input.trigger(enterEvent);
                                            }
                                        }, 1);
                                    });

                                    // when the page is end .menu() open to top
                                    $(".myArea").click(function(e){
                                        var posY = $(this).offset().top;
                                        var PY = 200, TP = -155;
                                        var visibleMenuCount = $('ul.ulMenu > li:visible').length;


                                        if( $(".attached").is(':visible') || (snap.clinicialAppointment == true  && app.snapFileService.consultStatus == 1)){
                                            TP=-160;
                                            PY=225
                                        }
                                        if(! $(".ulMenu").hasClass("pic_menu")){
                                            PY=340,
                                                TP=-71
                                        }

                                        if ((e.pageY - posY) > PY && visibleMenuCount > 0) {
                                            $(".ulMenu").css("top",TP);
                                        }else{
                                            $(".ulMenu").css("top",20);
                                        }
                                    });

                                    $(".patientArea").click(function(e){

                                        var posY = $(this).offset().top;
                                        var PY=240,TP=-124;
                                        var visibleMenuCount = $('ul.ulMenu > li:visible').length;

                                        if($(".attached").is(':visible') || app.snapFileService.consultStatus == 1){
                                            TP=-155;
                                        }

                                        if(! $(".ulMenu").hasClass("pic_menu")){
                                            PY=340,
                                                TP=-35
                                        }

                                        if ((e.pageY - posY) > PY && visibleMenuCount>0) {
                                            $(".ulMenu").css("top",TP);
                                        }else{
                                            $(".ulMenu").css("top",20);
                                        }
                                    });

                                });
                            </script>
                        </div><!-- / #file-sharing --></div>
                </div>
                <div class="patient-data patient-data__eprescribe k-content" id="clinician-consultation-tabs-6" role="tabpanel" aria-hidden="true" aria-expanded="false">
                    <h1>ePrescribe will open in window</h1>
                </div>
            </div></div>

    </section>
</div>
