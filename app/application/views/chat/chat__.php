<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="page-content page-doctor-patient-chat">
    <div class="container-fluid">
        <div class="box-typical chat-container">
            <section class="chat-area">
                <div class="chat-area-in">
                    <div class="chat-area-header">
                        <div class="chat-list-item online">
                            <div class="chat-list-item-photo">
                                <!--									<img src="-->
                                <?php //echo base_url();?><!--template_file/img/photo-64-1.jpg" alt="">-->
                                <img src="<?php echo base_url(); ?>template_file/img/photo-64-1.jpg" alt="">
                            </div>
                            <div class="chat-list-item-name">
                                <span class="chat_partner_name"><?php echo $chat_partner->user_name; ?></span>
                            </div>
                            <div class="chat-list-item-txt writing">Last seen 05 aug 2015 at 18:04</div>
                        </div>
                        <div class="chat-area-header-action">
                            <div class="dropdown dropdown-typical">
                                <a class="dropdown-toggle dropdown-toggle-txt" id="dd-chat-action" data-target="#"
                                   href="http://example.com" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">
                                    <span class="lbl">Actions</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-chat-action">
                                    <a class="dropdown-item no-nowrap" href="#">Delete&nbsp;conversation</a>
                                    <a class="dropdown-item no-nowrap" href="#">Report spam</a>
                                </div>
                            </div>
                        </div>
                    </div><!--.chat-area-header-->

                    <div class="chat-dialog-area" id="chat_dialog_area" style="overflow: auto;">


                    </div><!--.chat-dialog-area-->

                    <div class="chat-area-bottom">
                        <form id="private_message" class="write-message" method="post">
                            <!--								<form id="private_message" class="write-message" method="post" action="-->
                            <?php //echo base_url();?><!--/message/save_messages">-->
                            <div class="avatar">
                                <img src="<?php echo base_url(); ?>template_file/img/photo-64-3.jpg" alt="">
                            </div>
                            <div class="form-group">
                                <!--										<input type="hidden" name="chat_id" id="chat_id" value="-->
                                <?php //echo $chat->chat_id; ?><!--">-->
                                <input type="hidden" name="sender_id" id="sender_id"
                                       value="<?php echo $this->session->userdata('userid'); ?>">
                                <input type="hidden" name="receiver_id" id="receiver_id"
                                       value="<?php echo $chat_partner->id; ?>">
                                <textarea rows="2" class="form-control" placeholder="Type a message" name="message_body"
                                          id="message_body"></textarea>
                            </div>
                            <a class="btn btn-rounded float-left " id="send_message">Send</a>
                            <button type="reset" class="btn btn-rounded btn-default float-left">Clear</button>
                            <div id="message_box"></div>
                            <div class="dropdown dropdown-typical dropup attach">
                                <!--										<a class="dropdown-toggle dropdown-toggle-txt"-->
                                <!--										   id="dd-chat-attach"-->
                                <!--										   data-target="#"-->
                                <!--										   data-toggle="dropdown"-->
                                <!--										   aria-haspopup="true"-->
                                <!--										   aria-expanded="false">-->
                                <!--											<span class="lbl">Attach</span>-->
                                <!--										</a>-->
                                <!--										<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-chat-attach">-->
                                <!--											<a class="dropdown-item" href="#"><i class="font-icon font-icon-cam-photo"></i>Photo</a>-->
                                <!--											<a class="dropdown-item" href="#"><i class="font-icon font-icon-cam-video"></i>Video</a>-->
                                <!--											<a class="dropdown-item" href="#"><i class="font-icon font-icon-sound"></i>Audio</a>-->
                                <!--											<a class="dropdown-item" href="#"><i class="font-icon font-icon-page"></i>Document</a>-->
                                <!--											<a class="dropdown-item" href="#"><i class="font-icon font-icon-earth"></i>Map</a>-->
                            </div>
                    </div>
                    <!--								</form>-->
                </div><!--.chat-area-bottom-->
        </div><!--.chat-area-in-->
        </section><!--.chat-area-->
    </div><!--.chat-container-->

</div><!--.container-fluid-->
</div><!--.page-content-->