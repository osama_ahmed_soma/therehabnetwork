<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>


<div class="page-content page-doctor-patient-message">
    <div class="container-fluid">
        <div class="box-typical chat-container">
            <section class="chat-list">
                <div class="chat-list-search">
                    <input type="text" class="form-control form-control-rounded" placeholder="Search"/>
                </div><!--.chat-list-search-->
                <div class="chat-list-in scrollable-block">

                    <?php foreach ($contact_list as $contact) { ?>
                        <a href="<?php echo base_url(); ?>message/chat/<?php echo $contact->id; ?>">
                            <div class="chat-list-item online chat_partner" id="contact-chat-list">
                                <div class="chat-list-item-photo">
                                    <img src="<?php echo base_url(); ?>template_file/img/photo-64-1.jpg" alt="">
                                </div>
                                <div class="chat-list-item-header">
                                    <div class="chat-list-item-name">
                                        <input type="hidden" name="user_id"
                                               id="contact_user_id" <?php echo $contact->id; ?>
                                        <span class="name"><?php echo $contact->user_name; ?></span>
                                    </div>
                                    <div class="chat-list-item-date"><?php echo date("h:i:s"); ?></div>
                                </div>
                                <div class="chat-list-item-cont">
                                    <div class="chat-list-item-txt writing">
                                        <div class="icon">
                                        </div>
                                    </div>
                                    <!--										<div class="chat-list-item-count">3</div>-->
                                </div>
                            </div>
                        </a>
                    <?php } ?>

                </div><!--.chat-list-in-->
            </section><!--.chat-list-->
        </div><!--.chat-container-->

    </div><!--.container-fluid-->
</div><!--.page-content-->


