<?php

/**
 * Return patient age
 *
 * @param $dob
 * @return int
 */
function getPatientAge($dob)
{
//    $age = DateTime::createFromFormat('m/d/Y', $dob)
//        ->diff(new DateTime('now'))
//        ->y;
//
//    return $age;
    return date_diff(date_create($dob), date_create('today'))->y;
}

function getPendingAppointments($patient_id)
{
    $CI = & get_instance();
    $CI->load->model('patient_model');
    $pendingAppointments = $CI->patient_model->getPendingAppointmentForPatient($patient_id);
    return $pendingAppointments;
}
