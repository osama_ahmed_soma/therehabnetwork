<?php

function error_html($error){
    return '<div class="page-content" style="min-height: auto; margin-bottom: -5%; padding: 0px; background-color: transparent !important;">
        <div class="container-fluid" style="padding: 0px;">
            <div class="alert alert-warning">
                '.$error.'
            </div>
        </div>
    </div>';
}

function profile_completion_html_generator($data = []){
    return 'Your profile is <strong style="color: '.$data['color'].';">'.$data['percent'].'%</strong> complete
            <div class="progress progress-striped active">
                <div class="progress-bar" role="progressbar" aria-valuenow="'.$data['percent'].'" aria-valuemin="0"
                     aria-valuemax="100" style="width: '.$data['percent'].'%; background-color: '.$data['color'].';">
                    <span class="sr-only">'.$data['percent'].'% Complete</span>
                </div>
            </div>
            '.$data['message'];
}

function getError(){
    $CI = & get_instance();
    $type = $CI->session->userdata('type');
    $return = [];
    $return['message'] = '';
    $return['html'] = '';
    $return['type'] = 'none';
    $return['user_type'] = $type;
    $return['boolean'] = false;
    if($type == 'doctor'){
        $CI->load->model('doctor_model');
        $CI->load->model('consultationtype_model');
        $doctor = $CI->doctor_model->get_doctor($CI->session->userdata('userid'));
        if($doctor->firstname == '' || $doctor->lastname == '' || $doctor->email == '' || $doctor->gender == '' || $doctor->dob == '' || $doctor->timezone == '' || $doctor->zip == '' || $doctor->phone_mobile == '' || $doctor->npi == ''){
            $return['message'] = profile_completion_html_generator([
                'percent' => 0,
                'color' => '#f85959',
                'message' => 'Please complete your <a href="'.base_url().'doctor/update_profile" ><strong style="color: #f85959;">Profile</strong></a>'
            ]);
            $return['type'] = 'profile';
            return $return;
        }
        $doctor_specialization = $CI->doctor_model->get_doctors_specialization($CI->session->userdata('userid'));
        if(!is_array($doctor_specialization) && $doctor_specialization == 'No result found'){
            $return['message'] = profile_completion_html_generator([
                'percent' => 25,
                'color' => '#f85959',
                'message' => 'Please add your <a href="'.base_url().'doctor/update_profile/tabs-1-tab-2" ><strong style="color: #f85959;">Specialty</strong></a>'
            ]);
            $return['type'] = 'specialty';
            return $return;
        }
        $doctor_consultation_types = $CI->consultationtype_model->getByDoctorList($CI->session->userdata('userid'));
        if(count($doctor_consultation_types) == 0){
            $return['message'] = profile_completion_html_generator([
                'percent' => 50,
                'color' => '#f37328',
                'message' => 'Please set <a href="'.base_url().'doctor/payment_rate" ><strong style="color: #f37328;">Consultation Type</strong></a>'
            ]);
            $return['type'] = 'consultation_type';
            return $return;
        }
        $doctor_strip_info = $CI->doctor_model->get_doctor_stripe($CI->session->userdata('userid'));
        if($doctor_strip_info->stripe_one == '' || $doctor_strip_info->stripe_two == ''){
            $return['message'] = profile_completion_html_generator([
                'percent' => 75,
                'color' => '#c1ff2a',
                'message' => 'Please add a <a href="'.base_url().'doctor/stripe" ><strong style="color: #c1ff2a;">Payment method</strong></a>'
            ]);
            $return['type'] = 'strip';
            return $return;
        }
        $return['message'] = profile_completion_html_generator([
            'percent' => 100,
            'color' => '#77de62',
            'message' => ''
        ]);
        $return['boolean'] = true;
        return $return;
    } else {
        $CI->load->model('patient_model');
        $patient = $CI->patient_model->getById($CI->session->userdata('userid'));
        if($patient->firstname == '' || $patient->lastname == '' || $patient->user_email == '' || $patient->city == '' || $patient->country == '' || $patient->gender == '' || $patient->timezone == '' || $patient->phone_mobile == '' || $patient->height_ft == '' || $patient->height_inch == '' || $patient->weight == ''){
            $return['message'] = profile_completion_html_generator([
                'percent' => 25,
                'color' => '#f85959',
                'message' => 'Please complete your <a href="'.base_url().'patient/profile" ><strong style="color: #f85959;">Profile</strong></a>'
            ]);
            $return['type'] = 'profile';
            return $return;
        }
//        $CI->load->model('login_model');
//        if(count($CI->login_model->get_health_patient($CI->session->userdata('userid'))) == 0){
//            $return['message'] = profile_completion_html_generator([
//                'percent' => 60,
//                'color' => '#FFA500',
//                'message' => 'Please complete <a href="'.base_url().'patient/myhealth" ><strong style="color: #FFA500;">My Health</strong></a> questions</strong></a>'
//            ]);
//            $return['type'] = 'health';
//            return $return;
//        }
        $CI->load->model('health_model');
        $health_data = $CI->health_model->get([
            'patients_id' => $CI->session->userdata('userid'),
            'type' => 'health',
            '*' => ''
        ]);
        if(!$patient->is_health || ($patient->is_health == 2 && !$health_data)){
            $return['message'] = profile_completion_html_generator([
                'percent' => 60,
                'color' => '#FFA500',
                'message' => 'Please complete <a href="'.base_url().'patient/myhealth" ><strong style="color: #FFA500;">My Health </strong> history</a></strong></a>'
            ]);
            $return['type'] = 'health';
            return $return;
        }
        $medication_data = $CI->health_model->get([
            'patients_id' => $CI->session->userdata('userid'),
            'type' => 'medication',
            '*' => ''
        ]);
        if(!$patient->is_medication || ($patient->is_medication == 2 && !$medication_data)){
            $return['message'] = profile_completion_html_generator([
                'percent' => 70,
                'color' => '#FFA500',
                'message' => 'Please complete <a href="'.base_url().'patient/myhealth" ><strong style="color: #FFA500;">My Health</strong></a> medication</strong></a>'
            ]);
            $return['type'] = 'health';
            return $return;
        }
        $allergies_data = $CI->health_model->get([
            'patients_id' => $CI->session->userdata('userid'),
            'type' => 'allergies',
            '*' => ''
        ]);
        if(!$patient->is_allergies || ($patient->is_allergies == 2 && !$allergies_data)){
            $return['message'] = profile_completion_html_generator([
                'percent' => 80,
                'color' => '#FFA500',
                'message' => 'Please complete <a href="'.base_url().'patient/myhealth" ><strong style="color: #FFA500;">My Health</strong></a> allergies</strong></a>'
            ]);
            $return['type'] = 'health';
            return $return;
        }
        $surgeries_data = $CI->health_model->get([
            'patients_id' => $CI->session->userdata('userid'),
            'type' => 'surgery',
            '*' => ''
        ]);
        if(!$patient->is_surgeries || ($patient->is_surgeries == 2 && !$surgeries_data)){
            $return['message'] = profile_completion_html_generator([
                'percent' => 90,
                'color' => '#FFA500',
                'message' => 'Please complete <a href="'.base_url().'patient/myhealth" ><strong style="color: #FFA500;">My Health</strong></a> surgeries</strong></a>'
            ]);
            $return['type'] = 'health';
            return $return;
        }
//        if($patient->is_surgeries == 1){
//            $return['message'] = profile_completion_html_generator([
//                'percent' => 100,
//                'color' => '#77de62',
//                'message' => ''
//            ]);
////            $return['type'] = 'health';
//            $return['boolean'] = true;
//            return $return;
//        }
        $return['message'] = profile_completion_html_generator([
            'percent' => 100,
            'color' => '#77de62',
            'message' => ''
        ]);
        $return['boolean'] = true;
        return $return;
    }
}