<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (DEBUG) {
	ini_set('html_errors', 1);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	ini_set('log_errors', 1);
	ini_set('track_errors', 1);
	if (extension_loaded('xdebug')) {
		ini_set('xdebug.auto_trace', 1);
//	ini_set('xdebug.cli_color', 0); // don't how to use it
		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_includes', 1);
		ini_set('xdebug.collect_params', '4');
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.collect_vars', 1);
		ini_set('xdebug.coverage_enable', 1);
		ini_set('xdebug.default_enable', 1);
		ini_set('xdebug.dump.COOKIE', 1);
		ini_set('xdebug.dump.ENV', 1);
		ini_set('xdebug.dump.FILES', 1);
		ini_set('xdebug.dump.GET', 1);
		ini_set('xdebug.dump.POST', 1);
		ini_set('xdebug.dump.REQUEST', 1);
		ini_set('xdebug.dump.SESSION', 1);
		ini_set('xdebug.dump.SERVER', 'REQUEST_URI,REMOTE_ADDR,REQUEST_METHOD,PHP_SELF,SCRIPT_NAME');
		ini_set('xdebug.dump_globals', 1);
//	ini_set('xdebug.dump_once', 1);
		ini_set('xdebug.dump_undefined', 1);
		ini_set('xdebug.extended_info', 1);
//	ini_set('xdebug.file_link_format', 1); // dont know how to use it
		ini_set('xdebug.idekey', 'netbeans-xdebug');
//	ini_set('xdebug.max_nesting_level', 5);
		ini_set('xdebug.overload_var_dump', 1);

		ini_set('xdebug.profiler_aggregate', 1);
		ini_set('xdebug.profiler_append', 1);
		ini_set('xdebug.profiler_enable', 1);
		ini_set('xdebug.profiler_enable_trigger', 1);
		ini_set('xdebug.profiler_output_dir', 'xdebug_output_dir');
		ini_set('xdebug.profiler_output_name', 'xdebug.out.%p');

		ini_set('xdebug.remote_autostart', 1);
		ini_set('xdebug.remote_connect_back', 1);
		ini_set('xdebug.remote_cookie_expire_time', 3600);
		ini_set('xdebug.remote_enable', 1);
		ini_set('xdebug.remote_handler', 'dbgp');
		ini_set('xdebug.remote_host', $_SERVER['HTTP_HOST']);
		ini_set('xdebug.remote_log', 'xdebug.log');
		ini_set('xdebug.remote_mode', 'req');
		ini_set('xdebug.remote_port', 9000);
		ini_set('xdebug.scream', 1);
		ini_set('xdebug.show_exception_trace', 1);
		ini_set('xdebug.show_local_vars', 1);
		ini_set('xdebug.show_mem_delta', 1);
		ini_set('xdebug.trace_enable_trigger', 1);
//	ini_set('xdebug.trace_format', 0); // don't how to use it
		ini_set('xdebug.trace_options', 0);
		ini_set('xdebug.trace_output_dir', 'xdebug_trace_output_dir');
		ini_set('xdebug.trace_output_name', 'trace.%c');
		ini_set('xdebug.var_display_max_depth', -1); // 5 is enough i think
		ini_set('xdebug.var_display_max_children', -1);  // 256 is enough i think
		ini_set('xdebug.var_display_max_data', -1);  // 1024 is enough i think

		ini_set('xdebug.force_display_errors', 1);
	}
}

/**
 * MY URL Helper
 *
 * @package		My Extended CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Syed Salman Ali
 * @link		http://salmanpk.com/projects/my_extended_codeigniter
 */

/**
 * function description will be here soon
 *
 * @param	mixed
 * @return	string
 */
function var_debug($var = 'no parameter value provided in var_debug', $title = '', $hide_sidebar = false, $show_everything = false, $return = false) {
	if (DEBUG) {
		$CI = & get_instance();
		if ($return) {
			ob_start();
		}
		if ($CI->input->is_ajax_request() === true) {
			print_r($var);
//			debug_header($var);
			$bt = debug_backtrace();
			$my = 0;
			if (substr($bt[0]['file'], -16) === basename(__FILE__)) {
				$my++;
			}
			echo PHP_EOL . 'File: <strong>' . $bt[$my]['file'] . '</strong> at line number: <strong>' . $bt[$my]['line'] . '</strong>' . PHP_EOL;
		} elseif (is_cli() === true) {
			echo '<-------------------------------------------------------------------------------------------------------------------------------------' . PHP_EOL;
//			echo '*****' . PHP_EOL;
			var_dump($var);
//			echo '*****';
			$bt = debug_backtrace();
			$my = 0;
			if (substr($bt[0]['file'], -16) === basename(__FILE__)) {
				$my++;
			}
			echo 'File: ' . $bt[$my]['file'] . '</strong> at line number: <strong>' . $bt[$my]['line'] . PHP_EOL . '-------------------------------------------------------------------------------------------------------------------------------------/>' . PHP_EOL . PHP_EOL . PHP_EOL;
		} else {
			echo '<link rel="icon" href="' . base_url() . 'images/debug.png" type="image/x-icon" /><div class="mydebugbox" style="border: 1px solid gray; padding: 5px; box-shadow: 0px 0px 10px 2px rgba(119, 119, 119, 0.25);-moz-box-shadow: 0px 0px 10px 2px rgba(119, 119, 119, 0.25);-webkit-box-shadow: 0px 0px 10px 2px rgba(119, 119, 119, 0.25); font-family: Menlo, Monaco, Consolas, \'Courier New\', monospace; font-size: 12px;
background: #fff !important;">';
			echo ($hide_sidebar) ? '<style>.sidebar-offcanvas {display:none !important;}</style>' : '';
			$bt = debug_backtrace();
//			var_dump($bt);
//			var_dump(substr($bt[0]['file'], -16));
//			var_dump(basename(__FILE__));
			$my = 0;
			if (substr($bt[0]['file'], -16) === basename(__FILE__)) {
				$my++;
			}
//			var_dump($bt[$my]['file']);
			echo 'File: <strong>' . $bt[$my]['file'] . '</strong> at line number: <strong>' . $bt[$my]['line'] . '</strong>';
			echo ($title) ? '<h1 style="font-size: 16px; background: #eee; padding: 5px;">' . $title . '</h1>' : '';
			if (extension_loaded('xdebug')) {
				var_dump($var);
				if ($show_everything) {
					var_dump($bt);
				}
			} else {
				echo 'Server is poor, xdebug extension not found! Trying to format debugging output...';
				print_r2($var);
			}
			echo '</div>';
		}

		if ($return) {
			$output = ob_get_clean();
			return $output;
		}
	}
}

#http://php.net/manual/en/function.print-r.php#90759

function print_r_tree($data) {
// capture the output of print_r
	$out = print_r($data, true);

// replace something like '[element] => <newline> (' with <a href="javascript:toggleDisplay('...');">...</a><div id="..." style="display: none;">
	$out = preg_replace('/([ \t]*)(\[[^\]]+\][ \t]*\=\>[ \t]*[a-z0-9 \t_]+)\n[ \t]*\(/iUe', "'\\1<a href=\"javascript:toggleDisplay(\''.(\$id = substr(md5(rand().'\\0'), 0, 7)).'\');\">\\2</a><div id=\"'.\$id.'\" style=\"display: none;\">'", $out);

// replace ')' on its own on a new line (surrounded by whitespace is ok) with '</div>
	$out = preg_replace('/^\s*\)\s*$/m', '</div>', $out);

// print the javascript function toggleDisplay() and then the transformed output
	echo '<script language="Javascript">function toggleDisplay(id) { document.getElementById(id).style.display = (document.getElementById(id).style.display == "block") ? "none" : "block"; }</script>' . "\n$out";
}

//https://phphints.wordpress.com/2008/10/29/print_r-replacement-to-show-array-depth-more-clearly/

function print_r2(&$a, $i = 0) {
//	$colors = array("black", "red", "green", "maroon", "orange", "green", "purple", "yellow");


	if (!is_array($a)) {
		echo '<pre>';

		$type = gettype($a);
//		print_r($a);
		if ($type === 'string') {
			echo '<small>string</small>&nbsp;<span style="color:cc0000;">\'' . htmlspecialchars($a, ENT_QUOTES) . '\'</span>&nbsp;<em>(length=' . strlen($a) . ')</em> <br>';
		} else if ($type === 'boolean') {
			echo '<small>boolean</small>&nbsp;<span style="color:purple;">' . ($a ? 'true' : 'false') . '</span><br>';
		} else {
			echo '<small>' . gettype($a) . '</small>&nbsp;<span style="color:cc0000;">' . $a . '</span>&nbsp;<em>(length=' . strlen($a) . ')</em> <br>';
		}
		echo '</pre>';
		return;
	}

	echo '<pre><span style="color: ' . /* $colors[$i++] . */ '"><strong>array</strong> <em>(size=' . count($a) . ')</em><br>';

	if (!empty($a)) {

		foreach ($a as $k => $v) {

			if (isAssoc($a)) {
				echo "&n bsp;&nbsp;'$k'" . ' => ';
			} else {
				echo "&nbsp;&nbsp;$k" . ' => ';
			}

			if (is_array($v)) {
				print_r2($v, $i);
			} else {
				$type = gettype($a[$k]);
				if ($type === 'string') {
					echo '<small>string</small>&nbsp;<span style="color:cc0000;">\'' . htmlspecialchars($a[$k], ENT_QUOTES) . '\'</span>&nbsp;<em>(length=' . strlen($a[$k]) . ')</em> <br>';
				} else if ($type === 'integer') {
					echo '<small>int</small>&nbsp;<span style="color:#4e9a06;">' . $a[$k] . '</span><br>';
				} else if ($type === 'float' || $type === 'double') {
					echo '<small>' . gettype($a[$k]) . '</small>&nbsp;<span style="color:#f57900;">' . $a[$k] . '</span><br>';
				} else if ($type === 'boolean') {
					echo '<small>boolean</small>&nbsp;<span style="color:cc0000;">' . ($a[$k] ? 'true' : 'false' ) . '</span><br>';
				} else {
					echo '<small>' . gettype($a[$k]) . '</small>&nbsp;<span style="color:cc0000;">' . $a[$k] . '</span>&nbsp;<em>(length=' . strlen($a[$k]) . ')</em> <br>';
				}
			}
		}
	} else {
		echo '&nbsp;&nbsp;<em style="color: #888a85;">empty</em><br>';
	}

	$i--;

	echo '</span></pre>';
}

// http://stackoverflow.com/questions/173400/how-to-check-if-php-array-is-associative-or-sequential
function isAssoc($arr) {
	return array_keys($arr) !== range(0, count($arr) - 1);
}

function print_nice($elem, $max_level = 100, $print_nice_stack = array()) {
	if (is_array($elem) || is_object($elem)) {
		if (in_array($elem, $print_nice_stack, true)) {
			echo "<font color=red>RECURSION</font>";
			return;
		}
		$print_nice_stack[] = $elem;
		if ($max_level < 1) {
			echo "<font color=red>nivel maximo alcanzado</font>";
			return;
		}
		$max_level--;
		echo "<table border=1 cellspacing=0 cellpadding=3 width=100%>";
		if (is_array($elem)) {
			echo '<tr><td colspan=2 style="background-color:#333333;"><strong><font color=white>ARRAY</font></strong></td></tr>';
		} else {
			echo '<tr><td colspan=2 style="background-color:#333333;"><strong>';
//echo '<font color=white>OBJECT Type: '.get_class($elem).'</font></strong></td></tr>';
		}
		$color = 0;
		foreach ($elem as $k => $v) {
			if ($max_level % 2) {
				$rgb = ($color++ % 2) ? "#888888" : "#BBBBBB"

				;
			} else {
				$rgb = ($color++ % 2) ? "#8888BB" : "#BBBBFF";
			}
			echo '<tr><td valign ="top"  style="width:40px;background-color:' . $rgb . ';">';

			echo '<strong>' . $k . "</strong></td><td>";
			print_nice($v, $max_level, $print_nice_stack);
			echo "</td></tr>";
		} echo "</table>";
		return;
	}
	if ($elem === null) {
		echo "<font color=green>NULL</font>";
	} elseif ($elem === 0) {
		echo "0";
	} elseif ($elem === true) {
		echo "<font color=green>TRUE</font>";
	} elseif ($elem === false) {
		echo "<font color=green>FALSE</font>";
	} elseif ($elem === "") {
		echo "<font color=green>EMPTY STRING</font>";
	} else {
		echo str_replace("\n", "<strong><font color=red>*</font></strong><br>\n", $elem);
	}
}

#https://github.com/digitalnature/php-ref
# seems like it has won my heart, i will try it later

function _die($die_keyword = 'DIED') {
	if (DEBUG) {
		$CI = & get_instance();
		$bt = debug_backtrace();
		$my = 0;
		if (substr($bt[0]['file'], -16) === basename(__FILE__)) {
			$my++;
		}
		if (!$CI->input->is_ajax_request() && is_cli() === false) {
			die('<br><div class="mydiebox" style="color: #fff; border: 1px solid green;  padding: 5px; box-shadow: 0px 0px 10px 2px rgba(119, 119, 119, 0.25);
-moz-box-shadow: 0px 0px 10px 2px rgba(119, 119, 119, 0.25);
-webkit-box-shadow: 0px 0px 10px 2px rgba(119, 119, 119, 0.25); font-family: Menlo, Monaco, Consolas, \'Courier New\', monospace; font-size: 12px;  background: /*url(http://cdn.mysitemyway.com/etc-mysitemyway/webtreats/assets/posts/124/thumbs/grunge-stripes-patterns-12.jpg)*/ #cc0000 top center; "><strong>' . $die_keyword . '</strong> at file: <strong>' . $bt[$my]['file'] . '</strong> at line # <strong>' . $bt[$my]['line'] . '</strong></div><br>');
		} elseif (is_cli() === true) {
			die(PHP_EOL . $die_keyword . ' at file: ' . $bt[$my]['file'] . ' at line # ' . $bt[$my]['line'] . PHP_EOL . PHP_EOL);
		} else {
			die(PHP_EOL . $die_keyword . ' at file: ' . $bt[$my]['file'] . ' at line # ' . $bt[$my]['line'] . PHP_EOL . PHP_EOL);
		}
	}
}

// it is not a stable function, 
function debug_header($things) {
	if (DEBUG) {
		if (!is_string($things)) {
			foreach ($things as $key => $value) {
				header('___' . $key . ': => ' . $value);
			}
		} else {
			header('___$things: => ' . $things);
		}
	}
}

function dd($var = 'no parameter value provided in dd', $title = '', $hide_sidebar = false, $show_everything = false, $die_keyword = 'DIED') {
	var_debug($var, $title, $hide_sidebar, $show_everything);
	_die($die_keyword);
}

function d($die_keyword = 'DIED') {
	_die($die_keyword);
}

function _exit($die_keyword = 'EXITED') {
	_die($die_keyword);
}

function kill($die_keyword = 'KILLED') {
	_die($die_keyword);
}

function terminate($die_keyword = 'TERMINATED') {
	_die($die_keyword);
}

function dump($var = 'no parameter value provided in dump', $title = '', $hide_sidebar = false, $show_everything = false) {
	var_debug($var, $title, $hide_sidebar, $show_everything);
}

function dump_debug($input, $collapse = false) {
	if (DEBUG) {
		$recursive = function($data, $level = 0) use (&$recursive, $collapse) {
			global $argv;

			$isTerminal = isset($argv);

			if (!$isTerminal && $level == 0 && !defined("DUMP_DEBUG_SCRIPT")) {
				define("DUMP_DEBUG_SCRIPT", true);

				echo '<script language="Javascript">function toggleDisplay(id) {';
				echo 'var state = document.getElementById("container"+id).style.display;';
				echo 'document.getElementById("container"+id).style.display = state == "inline" ? "none" : "inline";';
				echo 'document.getElementById("plus"+id).style.display = state == "inline" ? "inline" : "none";';
				echo '}</script>' . "\n";
			}

			$type = !is_string($data) && is_callable($data) ? "Callable" : ucfirst(gettype($data));
			$type_data = null;
			$type_color = null;
			$type_length = null;

			switch ($type) {
				case "String":
					$type_color = "green";
					$type_length = strlen($data);
					$type_data = "\"" . htmlentities($data) . "\"";
					break;

				case "Double":
				case "Float":
					$type = "Float";
					$type_color = "#0099c5";
					$type_length = strlen($data);
					$type_data = htmlentities($data);
					break;

				case "Integer":
					$type_color = "red";
					$type_length = strlen($data);
					$type_data = htmlentities($data);
					break;

				case "Boolean":
					$type_color = "#92008d";
					$type_length = strlen($data);
					$type_data = $data ? "TRUE" : "FALSE";
					break;

				case "NULL":
					$type_length = 0;
					break;

				case "Array":
					$type_length = count($data);
			}

			if (in_array($type, array("Object", "Array"))) {
				$notEmpty = false;

				foreach ($data as $key => $value) {
					if (!$notEmpty) {
						$notEmpty = true;

						if ($isTerminal) {
							echo $type . ($type_length !== null ? "(" . $type_length . ")" : "") . "\n";
						} else {
							$id = substr(md5(rand() . ":" . $key . ":" . $level), 0, 8);

							echo "<a href=\"javascript:toggleDisplay('" . $id . "');\" style=\"text-decoration:none\">";
							echo "<span style='color:#666666'>" . $type . ($type_length !== null ? "(" . $type_length . ")" : "") . "</span>";
							echo "</a>";
							echo "<span id=\"plus" . $id . "\" style=\"display: " . ($collapse ? "inline" : "none") . ";\">&nbsp;&#10549;</span>";
							echo "<div id=\"container" . $id . "\" style=\"display: " . ($collapse ? "" : "inline") . ";\">";
							echo "<br />";
						}

						for ($i = 0; $i <= $level; $i++) {
							echo $isTerminal ? "|    " : "<span style='color:black'>|</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
						}

						echo $isTerminal ? "\n" : "<br />";
					}

					for ($i = 0; $i <= $level; $i++) {
						echo $isTerminal ? "|    " : "<span style='color:black'>|</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					}

					echo $isTerminal ? "[" . $key . "] => " : "<span style='color:black'>[" . $key . "]&nbsp;=>&nbsp;</span>";

					call_user_func($recursive, $value, $level + 1);
				}

				if ($notEmpty) {
					for ($i = 0; $i <= $level; $i++) {
						echo $isTerminal ? "|    " : "<span style='color:black'>|</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					}

					if (!$isTerminal) {
						echo "</div>";
					}
				} else {
					echo $isTerminal ?
							$type . ($type_length !== null ? "(" . $type_length . ")" : "") . "  " :
							"<span style='color:#666666'>" . $type . ($type_length !== null ? "(" . $type_length . ")" : "") . "</span>&nbsp;&nbsp;";
				}
			} else {
				echo $isTerminal ?
						$type . ($type_length !== null ? "(" . $type_length . ")" : "") . "  " :
						"<span style='color:#666666'>" . $type . ($type_length !== null ? "(" . $type_length . ")" : "") . "</span>&nbsp;&nbsp;";

				if ($type_data != null) {
					echo $isTerminal ? $type_data : "<span style='color:" . $type_color . "'>" . $type_data . "</span>";
				}
			}

			echo $isTerminal ? "\n" : "<br />";
		};

		call_user_func($recursive, $input);
	}
}

function custom_debug($data, $key = null) {
	$debug_backtrace = debug_backtrace();
//	var_dump($debug_backtrace);
//	die;
//	array_push($GLOBALS['debug_data'], array($debug_backtrace[0]['file'] => $debug_backtrace[0] ) );
//	array_push($GLOBALS['debug_data'], $data);
//	$GLOBALS['debug_data'][$debug_backtrace[0]['file']] = $debug_backtrace[0];
	if ($key) {
		$GLOBALS['debug_data'][$key] = $data;
		return;
	}
	$GLOBALS['debug_data'][basename($debug_backtrace[0]['file']) . ' at line #' . $debug_backtrace[0]['line']] = $data;
}
