<?php
/**
 * Created by PhpStorm.
 * User: ziku
 * Date: 8/14/16
 * Time: 3:45 AM
 */
function getUnopenedMessageCount( $id = null )
{
    $CI = & get_instance();
    if ( $id == null ) {
        $id = $CI->session->userdata('userid');
    }

    //get unopened message count
    $where = array(
        'to_userid' => $id,
        'draft' => '0',
        'recipient_delete' => '0',
        'opened' => '0'
    );
    $CI->db->from('messages')
        ->where($where);

    return $CI->db->count_all_results();
}

function getLatestThreeInbox( $id = null )
{
    $CI = & get_instance();
    if ( $id == null ) {
        $id = $CI->session->userdata('userid');
    }

    $CI->load->model('messages_model');

    return $CI->messages_model->getInbox(3);
}

function getLatestThreeOutbox( $id = null )
{
    $CI = & get_instance();
    if ( $id == null ) {
        $id = $CI->session->userdata('userid');
    }

    $CI->load->model('messages_model');

    return $CI->messages_model->getSentBox(3);
}

function getUnreadNotificationCount( $id = null ) {
    $CI = & get_instance();
    if ( $id === null ) {
        $id = $CI->session->userdata('userid');
    }

    $CI->load->model('notification_model');

    return $CI->notification_model->countUnreadNotifications( $id );
}

function getUnreadNotitication( $id = null, $count = 4 ) {
    $CI = & get_instance();
    if ( $id === null ) {
        $id = $CI->session->userdata('userid');
    }

    $CI->load->model('notification_model');

    return $CI->notification_model->getUnreadNotifications( $id, $count );
}

/*function readNotification($noficationID){
    $CI = & get_instance();
    $CI->load->model('notification_model');
    $CI->notification_model->getUnreadNotifications( $noficationID );
}*/