<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Input extends CI_Input
{

	function ip_address()
	{
		$ip = parent::ip_address();
		
		$cloudflare_ip = $this->get_request_header('HTTP_CF_CONNECTING_IP', true);
		
		if ($cloudflare_ip != $ip) {
			return $cloudflare_ip;
		}
		
		return $ip;
	}

}
