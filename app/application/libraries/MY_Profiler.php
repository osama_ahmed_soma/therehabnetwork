<?php

class MY_Profiler extends CI_Profiler {

	protected $_available_sections = array(
		'debug_data',
		'benchmarks',
		'get',
		'memory_usage',
		'post',
		'uri_string',
		'controller_info',
		'queries',
		'http_headers',
		'session_data',
		'config'
	);

	function __construct($config = array()) {
		parent::__construct($config);
	}

	function run() {
		// hehehe :-)
		if (DEBUG == true && is_cli() === false && $this->CI->input->is_ajax_request() == false) {
			return parent::run();
		}
		return;
	}

	// --------------------------------------------------------------------

	/**
	 * Compile debug data
	 *
	 * @return	string
	 */
	/*
	  function _compile_debug_data() {
	  if (!$GLOBALS['debug_data']) {
	  return;
	  }

	  $output = '<fieldset id="ci_profiler_csession" style="border:1px solid #000;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#eee">';
	  $output .= '<legend style="color:#000;">&nbsp;&nbsp;Debug Data&nbsp;&nbsp;(<span style="cursor: pointer;" onclick="var s=document.getElementById(\'ci_profiler_debug_data\').style;s.display=s.display==\'none\'?\'\':\'none\';this.innerHTML=this.innerHTML==\'' . $this->CI->lang->line('profiler_section_show') . '\'?\'' . $this->CI->lang->line('profiler_section_hide') . '\':\'' . $this->CI->lang->line('profiler_section_show') . '\';">' . $this->CI->lang->line('profiler_section_show') . '</span>)</legend>';
	  $output .= "<table style='width:100%;display:none' id='ci_profiler_debug_data'>";

	  foreach ($GLOBALS['debug_data'] as $key => $val) {
	  if (is_array($val) OR is_object($val)) {
	  $val = print_r($val, TRUE);
	  }

	  $output .= "<tr><td style='padding:5px; vertical-align: top;color:#900;background-color:#ddd;'>" . $key . "&nbsp;&nbsp;</td><td style='padding:5px; color:#000;background-color:#ddd;'>" . htmlspecialchars($val) . "</td></tr>\n";
	  }

	  $output .= '</table>';
	  $output .= "</fieldset>";
	  return $output;
	  } */

	/**
	 * Compile debug data
	 *
	 * @return	string
	 */
	function _compile_debug_data() {
//		I will see comments there for expert debug tracing https://secure.php.net/manual/en/function.debug-backtrace.php
//		ob_start();
//		debug_print_backtrace();
//		$GLOBALS['debug_data'][__LINE__] = ob_get_clean();

		$output = "\n\n";
		$output .= '<fieldset id="ci_profiler_get" style="border:1px solid #337AB7;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#eee">';
		$output .= "\n";
		$output .= '<legend style="color:#337ab7;">&nbsp;&nbsp;Debug Data&nbsp;&nbsp;</legend>';
		$output .= "\n";

		if (!isset($GLOBALS['debug_data']) || count($GLOBALS['debug_data']) == 0) {
			$output .= "<div style='color:#286090;font-weight:normal;padding:4px 0 4px 0'>No Debug Data exists</div>";
		} else {
			$output .= "\n\n<table style='width:100%; border:none'>\n";

			foreach ($GLOBALS['debug_data'] as $key => $val) {
				if (!is_numeric($key)) {
					$key = "'" . $key . "'";
				}

				$output .= "<tr><td style='width:50%;color:#000;background-color:#ddd;padding:5px;vertical-align: top;'>" . $key . "&nbsp;&nbsp; </td><td style='width:50%;padding:5px;color:#cd6e00;font-weight:normal;background-color:#ddd;vertical-align: top;'>";
//				if (is_array($val)) {
//					$output .= "<pre>" . htmlspecialchars(stripslashes(print_r($val, true))) . "</pre>";
//				} else if (is_object($val)) {
//					$output .= "<pre>" . htmlspecialchars(print_r($val, true)) . "</pre>";
//				} else {
//					$output .= htmlspecialchars(stripslashes($val));
//				}
//				$output .= var_debug($val, 'record', false, false, true);

				// my love :-)
				if (extension_loaded('xdebug')) {
					ob_start();
					var_dump($val);
					$output .= ob_get_clean();
				} else {
					ob_start();
					dump_debug($val);
					$output .= ob_get_clean();
				}
				// my love :-)
				
				$output .= "</td></tr>\n";
			}

			$output .= "</table>\n";
		}
		$output .= "</fieldset>";

		return $output;
	}

}
