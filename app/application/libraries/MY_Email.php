<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Email Queue
 *
 * A CodeIgniter library to queue e-mails.
 *
 * @package     CodeIgniter
 * @category    Libraries
 * @author      Thaynã Bruno Moretti
 * @link    http://www.meau.com.br/
 * @license http://www.opensource.org/licenses/mit-license.html
 */
class MY_Email extends CI_Email
{

	// DB table
	private $table_email_queue = 'email_queue';
	// Main controller
	private $main_controller = 'queue_email send_queue';
	// PHP Nohup command line
	private $phpcli = 'nohup php';
	private $expiration = NULL;
	// Status (pending, sending, sent, failed)
	private $status;

	/**
	 * Constructor
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
		
		log_message('debug', 'Email Queue Class Initialized');

		$this->expiration = 60 * 5;
		$this->CI = & get_instance();

		$this->CI->load->database('default');
	}

	public function set_status($status)
	{
		$this->status = $status;
		return $this;
	}

	/**
	 * check_mail_limit
	 *
	 * Check daily limit
	 * @return  mixed
	 */
	public function check_mail_limit()
	{
		$this->CI->db->where('q.status', 'sent');
		$this->CI->db->where('DATE_FORMAT(date, "%Y-%m-%d") =', date("Y-m-d"));
		$query = $this->CI->db->from("{$this->table_email_queue} q");
		$count = $query->count_all_results();
		
//		var_debug($this->CI->db->last_query());
		
		if ($count < config_item('max_email_per_day')) {
			return true;
		}
		return false;
	}

	/**
	 * Get
	 *
	 * Get queue emails.
	 * @return  mixed
	 */
	public function get($limit = NULL, $offset = NULL)
	{
		$count = $this->check_mail_limit();
//		var_debug($count);
		
		if (!$count) {
			return [];
		}
		
		if ($this->status != FALSE) {
			$this->CI->db->where('q.status', $this->status);
		}
		$query = $this->CI->db->get("{$this->table_email_queue} q", $limit, $offset);
		return $query->result();
	}

	/**
	 * Save
	 *
	 * Add queue email to database.
	 * @return  mixed
	 */
	public function send($auto_clear = TRUE, $skip_job = FALSE)
	{
//		var_dump($auto_clear);
//		var_dump($skip_job);

		if ($skip_job === TRUE) {
			return parent::send($auto_clear);
		}

		$date = date("Y-m-d H:i:s");

		$to = is_array($this->_recipients) ? implode(", ", $this->_recipients) : $this->_recipients;
		$cc = implode(", ", $this->_cc_array);
		$bcc = implode(", ", $this->_bcc_array);

		$dbdata = array(
			'to' => $to,
			'cc' => $cc,
			'bcc' => $bcc,
			'message' => $this->_body,
			'headers' => serialize($this->_headers),
			'status' => 'pending',
			'date' => $date
		);

		return $this->CI->db->insert($this->table_email_queue, $dbdata);
	}

	/**
	 * Start process
	 *
	 * Start php process to send emails
	 * @return  mixed
	 */
	public function start_process()
	{
		$filename = FCPATH . 'index.php';
		$exec = shell_exec("{$this->phpcli} {$filename} {$this->main_controller} > /dev/null &");

		return $exec;
	}

	/**
	 * Send queue
	 *
	 * Send queue emails.
	 * @return  void
	 */
	public function send_queue()
	{
		// cron is working fine, now we don't need this
//		$this->CI->db->insert('cron', ['running' => 1]);
//		$insert_id= $this->CI->db->insert_id();
		
		$this->set_status('pending');
		$emails = $this->get(30);

		$this->CI->db->where('status', 'pending');
		$this->CI->db->set('status', 'sending');
		$this->CI->db->set('date', date("Y-m-d H:i:s"));
		$this->CI->db->update($this->table_email_queue);

		foreach ($emails as $email) {
			$recipients = explode(", ", $email->to);

			$cc = !empty($email->cc) ? explode(", ", $email->cc) : array();
			$bcc = !empty($email->bcc) ? explode(", ", $email->bcc) : array();

			$this->_headers = unserialize($email->headers);

			$this->to($recipients);
			$this->cc($cc);
			$this->bcc($bcc);

			$this->message($email->message);

			$debug_headers = '';

			// we want debug headers and we dont want to re-queue it
			if ($this->send(false, true)) {
				$status = 'sent';
			} else {
				$status = 'failed';
				$debug_headers = $this->print_debugger(array('headers'));
			}

			$this->CI->db->where('id', $email->id);

			$this->CI->db->set('debug_headers', $debug_headers);

//			var_dump($debug_headers);
//			var_dump($status);

			$this->CI->db->set('status', $status);
			$this->CI->db->set('date', date("Y-m-d H:i:s"));
			$this->CI->db->update($this->table_email_queue);
		}
		
		// cron is working fine, now we don't need this
//		$next_run_at = date('Y-m-d H:i:s', strtotime('+1 minute'));
//		$this->CI->db->set('next_run_at', $next_run_at);
//		$this->CI->db->set('ran_at', 'NOW()', false);
//		$this->CI->db->set('running', 0);
//		$this->CI->db->where('id', $insert_id);
//		$this->CI->db->update('cron');
	}

	/**
	 * Retry failed emails
	 *
	 * Resend failed or expired emails
	 * @return void 
	 */
	public function retry_queue()
	{
		$expire = (time() - $this->expiration);
		$date_expire = date("Y-m-d H:i:s", $expire);

		$this->CI->db->set('status', 'pending');
		$this->CI->db->where("(date < '{$date_expire}' AND status = 'sending')");
		$this->CI->db->or_where("status = 'failed'");

		$this->CI->db->update($this->table_email_queue);

		$this->set_status('pending');
		$this->send(false, true);

		log_message('debug', 'Email queue retrying...');
	}

}
