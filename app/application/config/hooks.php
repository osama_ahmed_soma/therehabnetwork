<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | Hooks
  | -------------------------------------------------------------------------
  | This file lets you define "hooks" to extend CI without hacking the core
  | files.  Please see the user guide for info:
  |
  |	https://codeigniter.com/user_guide/general/hooks.html
  |
 */

$hook['post_controller_constructor'] = function() {

	$CI = & get_instance();
	$CI->load->database();
	$CI->load->helper('debug');

	if (defined('DEBUG') && DEBUG === true && $CI->input->is_ajax_request() === false && config_item('enable_profiler') === true) {
		$CI->output->enable_profiler(true);
	}

	// my utf8 love
	$CI->db->query("SET character_set_results=utf8");
	$CI->db->query("SET NAMES utf8");

	set_timezone();

	// I have to send new csrf token in response to everyrequest if everytime
	if (config_item('csrf_regenerate')) {
		if ($CI->input->method() == 'post') {
			log_message('debug', 'Sending csrf ..... ' . $CI->uri->uri_string());
			$CI->output->set_header($CI->security->get_csrf_token_name() . ': ' . $CI->security->get_csrf_hash());
//			log_message('debug',$CI->security->get_csrf_token_name() . ': ' . $CI->security->get_csrf_hash());
//			log_message('debug', 'csrf header that is sent: ' . $CI->output->get_header($CI->security->get_csrf_token_name()) . ' ---------- ' . $CI->uri->uri_string());
		}
	}
};

// I have to send new csrf token in response to everyrequest if everytime
//$hook['display_override'] = function() {
//	$CI = & get_instance();
//	if (config_item('csrf_regenerate') == true && $CI->input->method() == 'post') {
//		log_message('debug', 'Sending csrf ..... ' . $CI->uri->uri_string());
//		$CI->output->set_header('X-Custom-Header: SALMAN IS HERE');
//		$CI->output->set_header($CI->security->get_csrf_token_name() . ': ' . $CI->security->get_csrf_hash());
//		log_message('debug', var_export(headers_list(), true));
//	}
//	$CI->output->_display();
//};
