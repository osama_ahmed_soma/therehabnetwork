<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Config extends CI_Config
{

	public function __construct()
	{
		parent::__construct();
	}

	function site_url($uri = '', $protocol = NULL)
	{
		if (filter_input(INPUT_SERVER, 'SERVER_PORT') == 443) {
			$protocol = 'https';
		}
		return parent::site_url($uri, $protocol);
	}
	
	function base_url($uri = '', $protocol = NULL)
	{
		if (filter_input(INPUT_SERVER, 'SERVER_PORT') == 443) {
			$protocol = 'https';
		}
		return parent::base_url($uri, $protocol);
	}

}
