<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class My_Controller
 *
 * @property CI_Session $session
 * @property CI_Input $input*
 * @property CI_Form_validation $form_validation
 * @property Login_model $login_model
 * @property Specialty_Model $specialty_model
 * @property Timezone_Model $timezone_model
 * @property State_Model $state_model
 * @property Appointment_Model $appointment_model
 *
 * @property CI_Config $config
 * @property CI_Upload $upload
 *
 */
class MY_Controller extends CI_Controller {
	public function __construct() {
		parent::__construct();
//		dd($this->session->all_userdata());
	}

	public function profile_completion_status(){
	    echo json_encode(getError());
    }

}

/**
 * Class Auth_Controller
 *
 */
class Auth_Controller extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
//		dd($this->session->all_userdata());
		
        if ( $this->uri->segment(1) === 'admin' ){
            if(!$this->session->userdata('admin_sess')['logged_in']){
                if ( $this->uri->segment(1) === 'admin' ) {
                    if ( $this->uri->segment(2) !== 'login' ) {
                        redirect(base_url() . 'admin/login');
                    }

                } else {
                    redirect('/login');
                }
            }
        } else {
            if (!$this->session->userdata('logged_in')) {
                if ( $this->uri->segment(1) === 'admin' ) {
                    if ( $this->uri->segment(2) !== 'login' ) {
                        redirect(base_url() . 'admin/login');
                    }

                } else {
                    redirect('/login');
                }
                return;
            }
        }
	}

	function patient_access_check() {
		if ($this->session->userdata('type') !== 'patient')
		{
			if (!$this->input->is_ajax_request())
			{
//				echo ' Redirecting...';
//				header( "refresh:5;url=" . base_url('home/doctor') );
				if ( $this->session->userdata('type') === 'doctor' ) {
					redirect('/doctor', 'refresh');
				}
				elseif ( $this->session->userdata('type') === 'admin' ) {
					redirect('/admin/dashboard', 'refresh');
				}
				else {
					$this->session->sess_destroy();
					redirect('/login');
				}
			} else {
				$this->output->set_status_header(403);
				echo 'Access Denied!';
			}
			die;
		}
	}

	function doctor_access_check() {
		if ($this->session->userdata('type') !== 'doctor')
		{
			if (!$this->input->is_ajax_request())
			{
//				echo ' Redirecting...';
//				header( "refresh:5;url=" . base_url('patient') );
				if ( $this->session->userdata('type') == 'patient' ) {
					redirect('/patient', 'refresh');
				}
				elseif ( $this->session->userdata('type') == 'admin' ) {
					redirect('/admin/dashboard', 'refresh');
				}
				else {
					$this->session->sess_destroy();
					redirect('/login');
				}
			}
			else {
				$this->output->set_status_header(403);
				echo 'Access Denied!';
			}
		}
	}

	function admin_access_check() {
		if ( $this->uri->segment(1) === 'admin' )
		{

		    $admin_session_array = $this->session->userdata('admin_sess');

            /*echo "<pre>";
            print_r($admin_session_array);
            die();*/

			if ( $admin_session_array['type'] && $admin_session_array['type'] !== 'admin' )
			{
                $this->session->unset_userdata('admin_sess');
                redirect('/login');
				if (!$this->input->is_ajax_request())
				{
					/*if ( $this->session->userdata('type') == 'doctor' ) {
						redirect('/doctor', 'refresh');
					}
					elseif ( $this->session->userdata('type') == 'patient' ) {
						redirect('/patient', 'refresh');
					}
					else {
						$this->session->sess_destroy();
						redirect('/login');
					}*/
				}
				else {
					$this->output->set_status_header(403);
					echo 'Access Denied!';
				}
			}

		}

	}

}
