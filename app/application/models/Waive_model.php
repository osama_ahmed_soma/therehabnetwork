<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Waive_model extends CI_Model
{

    public function add($data){
        $this->db->insert('waive_fee', $data);
        return $this->db->insert_id();
    }

    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update('waive_fee', $data);
    }

    public function getList($limit = null, $page = null)
    {
        $this->db->order_by('id', 'ASC');
        $this->db->from('waive_fee');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        return $this->db->get()->result();
    }

    public function getList_count(){
        return count($this->getList());
    }

    public function getById($id)
    {
        $this->db->where('id', $id)->limit(1);

        $query = $this->db->get('waive_fee');

        return $query->row();
    }

    public function getByFields($data)
    {
        $this->db->select('waive_fee.*, patient.firstname as patient_firstname, patient.lastname as patient_lastname');
        foreach ($data as $field_value){
            $this->db->where($field_value['field'], $field_value['value']);
        }
        $this->db->join('patient', 'waive_fee.patient_id = patient.user_id');

        return $this->db->get('waive_fee')->result();
    }

    public function check_exists($data)
    {
        foreach ($data as $field_value){
            $this->db->where($field_value['field'], $field_value['value']);
        }
        $query = $this->db->get('waive_fee');
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function remove($id){
        $this->db->delete('waive_fee', [
            'id' => $id
        ]);
    }
}