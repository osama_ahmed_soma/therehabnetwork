<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Drug_model extends CI_Model
{

    public function getAll($limit = null, $page = null){
        $this->db->order_by('drug_id', 'ASC');
        $this->db->from('drug_list');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        return $this->db->get()->result();
    }

    public function getAll_count(){
        return count($this->getAll());
    }

    public function add($medicine){
        $this->db->insert('drug_list', [
            'BrandName' => $medicine
        ]);
        return $this->db->insert_id();
    }

    public function update($id, $data){
        $this->db->where('drug_id', $id);
        $this->db->update('drug_list', $data);
    }

    public function remove($id){
        $this->db->delete('drug_list', [
            'drug_id' => $id
        ]);
    }

    public function check_exists($medicine)
    {
        $this->db->where('BrandName', $medicine);
        $query = $this->db->get('drug_list');
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

}