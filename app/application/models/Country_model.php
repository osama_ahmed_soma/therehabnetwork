<?php


class Country_Model extends CI_Model
{

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function find_all_countries()
    {
        $this->db->select('*');
        $this->db->from('country');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function find_all_states()
    {
        $this->db->select('*');
        $this->db->from('state');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function find_country_state($country_id)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('country', $country_id);
        $query_result = $this->db->get();
        return $query_result->result();
    }

}