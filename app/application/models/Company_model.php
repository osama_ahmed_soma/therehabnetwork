<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Company_Model extends CI_Model
{

    public function add($data){
        $this->db->insert('company', $data);
        return $this->db->insert_id();
    }

    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update('company', $data);
    }

    public function getList($limit = null, $page = null)
    {
        $this->db->order_by('id', 'ASC');
        $this->db->from('company');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        return $this->db->get()->result();
    }

    public function getList_count(){
        return count($this->getList());
    }

    public function getByField($fieldName, $fieldValue)
    {
        $this->db->where($fieldName, $fieldValue);

        $query = $this->db->get('company');

        return $query->row();
    }

    public function getById($id)
    {
        $this->db->where('id', $id)->limit(1);

        $query = $this->db->get('company');

        return $query->row();
    }

    public function check_exists($name, $code)
    {
        $this->db->where('name', $name);
        $this->db->where('code', $code);
        $query = $this->db->get('company');
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function remove($id){
        $this->db->delete('company', [
            'id' => $id
        ]);
    }
}