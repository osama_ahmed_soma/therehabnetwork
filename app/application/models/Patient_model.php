<?php

/**
 * Class Patient_model
 *
 *  @property CI_DB_query_builder|CI_DB_mysql_driver $db
 *
 * @property $id
 * @property $user_id
 * @property $firstname
 * @property $lastname
 * @property $email
 * @property $address
 * @property $country
 * @property $city
 * @property $state_id
 */
class Patient_model extends CI_Model
{
    /**
     * @param int $id
     * @return Patient_model|null
     */

    public function get($data = [])
    {
        $this->db->select(implode(', ', array_keys(array_filter($data, function ($value) {
            return ($value == '');
        }))));
        foreach ($data as $index => $value) {
            if ($value != '') {
                $this->db->where($index, $value);
            }
        }
        return $this->db->get('patient')->result();
    }

    public function get_all_patient($limit = null, $page = null){
        $this->db->select('patient.*, user.time as created_at, user.is_active as active, user.user_email, user.user_password, user.login_date_time as login_date_time, user.is_joined as is_joined');
        $this->db->join('user', 'user.id = patient.user_id');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        return $this->db->get('patient')->result();
    }

    public function get_list_by_name_email_phone($value){
        $this->db->select('patient.*, user.time as created_at, user.is_active as active, user.user_email, user.user_password, user.login_date_time as login_date_time, user.is_joined as is_joined');
        $this->db->join('user', 'user.id = patient.user_id');
        $this->db->or_like('CONCAT( patient.firstname,  \' \', patient.lastname )', $value);
        $this->db->or_like('user.user_email', $value);
        $this->db->or_like('patient.phone_mobile', $value);
        return $this->db->get('patient')->result();
    }

    public function get_all_patient_count(){
        return count($this->get_all_patient());
    }

    public function remove_patient($userid){
        $this->db->delete('patient', array('user_id' => $userid));
        $this->db->delete('user', array('id' => $userid));
    }

    public function getById($id = null)
    {
        if ( null === $id )
            $id = $this->session->userdata("userid");

        $this->db->select('patient.*, user.*');
        $this->db->join('user', 'patient.user_id = user.id');
        $this->db->where('user_id', $id);
        $this->db->limit('1');
        $query = $this->db->get('patient');

        return $query->row();
    }

    public function get_patient($id = null)
    {
        if ( null === $id )
            $id = $this->session->userdata("userid");

        $this->db->join('user', 'patient.user_id =user.id');
        $this->db->where('user_id', $id);
        $this->db->limit('1');
        $query = $this->db->get('patient');

        return $query->row();
    }

    public function update($id, $data)
    {
        $this->db->where('user_id', $id);
        $this->db->update('patient', $data);
    }

    public function getLastCard($userId)
    {
        $this->db->where('user_id', $userId);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('patient_card');

        $rows = $query->result();

        return empty($rows) ? null :reset($rows);
    }

    public function addCard($userId, $name, $customerHash)
    {
        return $this->db->insert('patient_card', [
            'user_id' => $userId,
            'name' => $name,
            'customer_hash' => $customerHash,
        ]);
    }

    public function deleteCardById($userId, $cardId)
    {
        $this->db->where('user_id', $userId);
        $this->db->where('id', $cardId);
        $this->db->delete('patient_card');
    }

    /**
     * @param string $email
     * @param string $password
     * @return int User Id
     */
    public function createUser($email, $password, $parentId = null)
    {
        $data = [
            'user_email' => $email,
            'user_name' => '',
            'user_password' => $password,
            'parent_id' => $parentId,
            'type' => "patient"
        ];
        $this->db->insert('user', $data);

        return $this->db->insert_id();
    }

    public function createFamilyMember($userId, $data)
    {
        $this->db->trans_start();

        //create user
        $childUserId = $this->createUser($data['email'], password(random_string()), $userId);

        unset($data['email']);
        $data['user_id'] = $childUserId;

        //create patient
        $this->createPatient_old($data);

        $this->db->trans_complete();

//        return $this->db->trans_status();
        return $childUserId;
    }

    public function createPatient_old($data)
    {
		$this->db->insert('patient', $data);
        return $this->db->insert_id();
	}

    public function createPatient($userFname, $userLname, $userEmail, $password)
    {
		$this->db->trans_start();
        $userId = $this->createUser($userEmail, $password);
        $this->db->insert('patient', [
            'firstname' => $userFname,
            'lastname' => $userLname,
            'email' => $userEmail,
            'user_id' => $userId,
        ]);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }
	

    public function getFamilyMembers($userId)
    {
        $this->db->join('user', 'patient.user_id =user.id');
        $this->db->where('parent_id', $userId);
        $query = $this->db->get('patient');

        return $query->result();
    }

    public function removeFamilyMember($userID){
        $this->db->set('parent_id', null);
        $this->db->where('id', $userID);
        $this->db->update('user');
    }

    public function getPastAppointmentForPatient($id, $doctorID = null)
    {
        $now = time();
        $this->db->select('appointment.*, appointment.id AS appointment_id, appointment.status as app_status, IF(reason.id = 13, appointment.reason, reason.name) AS primary_symptom, doctor.firstname, doctor.lastname', false);
        //$this->db->join('available_time', 'available_time.id = appointment.available_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('patients_id', $id);
        if($doctorID !== null){
            $this->db->where('doctors_id' , $doctorID);
        }
        $this->db->group_start();
        $this->db->where('appointment.start_date_stamp <', $now );
        $this->db->or_where("appointment.status", 6);
        $this->db->group_end();
        $this->db->where("appointment.status !=", 5);
        $this->db->order_by('appointment.start_date_stamp', 'DESC');
        $query = $this->db->get('appointment');

        return $query->result();
    }

    public function getFutureAppointmentForPatient($id, $doctorID = null)
    {
        $now = date('Y-m-d H:i:s', time());
        $now = time();
        $this->db->select('appointment.*, appointment.id AS appointment_id, appointment.status as app_status, IF(appointment.reason_id = 13, appointment.reason, reason.name) AS primary_symptom, doctor.firstname, doctor.lastname', false);
        //$this->db->join('available_time', 'available_time.id = appointment.available_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('patients_id', $id);
        if($doctorID !== null){
            $this->db->where('doctors_id' , $doctorID);
        }
        $this->db->where("appointment.start_date_stamp >=", $now);
        $this->db->group_start();
        $this->db->where("appointment.status", 2);
        $this->db->or_where("appointment.status", 3);
        $this->db->group_end();
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        $query = $this->db->get('appointment');

        return $query->result();
    }
	
    public function getUpcomingAppointmentForPatient($id, $coming_time, $current_time)
    {
//        $this->db->select('appointment.*, appointment.id AS appointment_id, appointment.status as app_status, IF(appointment.reason_id = 13, appointment.reason, reason.name) AS primary_symptom, doctor.firstname, doctor.lastname', false);
        $this->db->select('appointment.*, appointment.id AS appointment_id, reason.name AS primary_symptom, doctor.firstname, doctor.lastname, appointment.status as app_status');
        $this->db->from('appointment');
        //$this->db->join('available_time', 'available_time.id = appointment.available_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('patients_id', $id);
//        $this->db->where("FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i') <=", $coming_time);
//        $this->db->where("FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i') >=", $current_time);
        $this->db->where("FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') <=", $coming_time);
        $this->db->where("FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') >=", $current_time);

//		$this->db->where("appointment.id NOT IN (SELECT al.appointment_id FROM appointment_log al WHERE al.appointment_id = appointment.id AND (al.status = 'delayed' OR al.status = 'cancelled') )", null, false);
//		$this->db->where("appointment.id NOT IN (SELECT al.appointment_id FROM appointment_log al WHERE al.appointment_id = appointment.id AND (al.status = 'delayed') )", null, false); // this is correct, but may be i will remove it
		
        $this->db->group_start();
        $this->db->where("(appointment.status = 2 OR appointment.status = 3)");
        $this->db->group_end();
        $this->db->order_by('appointment.start_date_stamp', 'DESC');
		$this->db->limit(1);
        $query = $this->db->get();

        return $query->row();
    }

    public function getCurrentAppointmentForDoctor($id, $current_time)
    {
        $this->db->select('appointment.*, appointment.id AS appointment_id, reason.name AS primary_symptom, doctor.firstname, doctor.lastname, appointment.status as app_status');
        $this->db->from('appointment');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('patients_id', $id);
        $this->db->where("FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') <=", $current_time);
        $this->db->where("FROM_UNIXTIME(appointment.end_date_stamp, '%Y-%m-%d %H:%i:%s') >=", $current_time);
        $this->db->group_start();
        $this->db->where("(appointment.status = 2 OR appointment.status = 3)");
        $this->db->group_end();
        $this->db->order_by('appointment.start_date_stamp', 'DESC');
		$this->db->limit(1);
        $query = $this->db->get();

        return $query->row();
    }

    public function getUpcomingAppointment_delayedForDoctor($id, $coming_time, $current_time)
    {
        $this->db->select('appointment.*, appointment.id AS appointment_id, appointment.status as app_status, IF(appointment.reason_id = 13, appointment.reason, reason.name) AS primary_symptom, doctor.firstname, doctor.lastname', false);
        //$this->db->join('available_time', 'available_time.id = appointment.available_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('patients_id', $id);
        $this->db->where("FROM_UNIXTIME(appointment.delay_time, '%Y-%m-%d %H:%i') <=", $coming_time);
        $this->db->where("FROM_UNIXTIME(appointment.delay_time, '%Y-%m-%d %H:%i') >=", $current_time);

//		$this->db->where("appointment.id NOT IN (SELECT al.appointment_id FROM appointment_log al WHERE al.appointment_id = appointment.id AND (al.status = 'delayed' OR al.status = 'cancelled') )", null, false);
//		$this->db->where("appointment.id NOT IN (SELECT al.appointment_id FROM appointment_log al WHERE al.appointment_id = appointment.id AND (al.status = 'delayed') )", null, false); // this is correct, but may be i will remove it

        $this->db->group_start();
        $this->db->where("(appointment.status = 2 OR appointment.status = 3)");
        $this->db->group_end();
        $this->db->order_by('appointment.start_date_stamp', 'DESC');
		$this->db->limit(1);
        $query = $this->db->get('appointment');

        return $query->row();
    }

    public function upcoming_appointment_is_doctor_status($patient_id, $appointment_id, $condition = 1){
        $this->db->where('id', $appointment_id);
        $this->db->where('patients_id', $patient_id);
        $this->db->where('is_doctor_ready', $condition);
        $query = $this->db->get('appointment');
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function getPendingAppointmentForPatient($id)
    {
        $now = time();
        $this->db->select('appointment.*, appointment.id AS appointment_id, appointment.status as app_status, IF(appointment.reason_id = 13, appointment.reason, reason.name) AS primary_symptom, doctor.firstname, doctor.lastname', false);
        //$this->db->join('available_time', 'available_time.id = appointment.available_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('patients_id', $id);
        $this->db->where("appointment.start_date_stamp >=", $now);
        $this->db->where("appointment.status", 1);
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        $query = $this->db->get('appointment');

        return $query->result();
    }

    public function getCanceledAppointmentForPatient($id)
    {
//        $now = time();
        $this->db->select('appointment.*, appointment.id AS appointment_id, appointment.status as app_status, IF(appointment.reason_id = 13, appointment.reason, reason.name) AS primary_symptom, doctor.firstname, doctor.lastname', false);
        //$this->db->join('available_time', 'available_time.id = appointment.available_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('patients_id', $id);
//        $this->db->where("appointment.start_date_stamp >=", $now);
        $this->db->where("appointment.status", 5);
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        $query = $this->db->get('appointment');

        return $query->result();
    }

    public function get_patient_doctors($id = null, $limit = 4)
    {
        if ( null === $id )
            $id = $this->session->userdata('userid');

        $this->db->select('usr.*, st.code');
        $this->db->from('favorite as fav, doctor as usr');
        $this->db->join('state st', 'st.id = usr.state_id', 'left');
        $this->db->where('usr.userid = fav.d_id');
        $this->db->where('fav.p_id', $id);

        $this->db->order_by('usr.firstname', 'ASC');
        if ( is_numeric( $limit ) ) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function search_patient_doctors( $text="", $id = null, $limit = 4)
    {
        if ( null === $id )
            $id = $this->session->userdata('userid');

        $this->db->select('usr.*, st.code');
        $this->db->from('favorite as fav, doctor as usr');
        $this->db->join('state st', 'st.id = usr.state_id', 'left');
        $this->db->where('usr.userid = fav.d_id');
        $this->db->where('fav.p_id', $id);
        $this->db->group_start();
        $this->db->like('usr.firstname', $text);
        $this->db->or_like('usr.lastname', $text);
        $this->db->group_end();

        $this->db->order_by('usr.firstname', 'ASC');
        if ( is_numeric( $limit ) ) {
            $this->db->limit($limit);
        }

        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function patient_check_waive( $doctor_id, $patient_id = null ) {
        if ( null === $patient_id )
            $patient_id = $this->session->userdata('userid');

        return $this->db->select('id')
            ->from('waive_fee')
            ->where('doctor_id', $doctor_id)
            ->where('patient_id', $patient_id)
            ->count_all_results();
    }
}