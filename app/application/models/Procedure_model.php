<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Procedure_model extends CI_Model
{

    public function add($data){
        $this->db->insert('procedure_tbl', $data);
        return $this->db->insert_id();
    }

    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update('procedure_tbl', $data);
    }

    public function getAll($limit = null, $page = null){
        $this->db->order_by('id', 'ASC');
        $this->db->from('procedure_tbl');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        return $this->db->get()->result();
    }

    public function getAll_count(){
        return count($this->getAll());
    }

    public function remove($id){
        $this->db->delete('procedure_tbl', [
            'id' => $id
        ]);
    }

    public function check_exists($name, $code)
    {
        $this->db->where('name', $name);
        $this->db->where('code', $code);
        $query = $this->db->get('procedure_tbl');
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

}