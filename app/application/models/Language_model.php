<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Language_Model extends CI_Model
{
    public function getList()
    {
        $this->db->order_by('name', 'ASC');
        $this->db->from('language');
        $rows = $this->db->get()->result();

        $result = [];
        foreach ($rows as $row) {
            $result[$row->id] = $row->name;
        }

        return $result;
    }

    /**
     * Create new language
     *
     * @param string $name
     * @return int
     */
    public function addLanguage($name)
    {
        $this->db->insert('language', [
            'name' => $name,
        ]);

        return $this->db->insert_id();
    }

    public function getById($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('language');

        $rows = $query->result();

        return empty($rows) ? null : reset($rows);
    }

    public function getByIds($ids)
    {
        if (!count($ids)) {
            return [];
        }

        $this->db->where_in('id', $ids);
        $query = $this->db->get('language');

        $rows = $query->result();

        return $rows;
    }
}