<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Knocking_model extends CI_Model
{

    private function get_by_id($knocking_id)
    {
        $this->db->where('id', $knocking_id);
        return $this->db->get('appointment_knocking_data')->row();
    }

    private function checkExistence($id, $field = 'appointment_id')
    {
        $this->db->where($field, $id);
        $query = $this->db->get('appointment_knocking_data');
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    private function is_doctor_available($id, $field = 'id')
    {
        $this->db->where($field, $id);
        $query = $this->db->get('appointment_knocking_data');
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    private function insert($data)
    {
        $this->db->insert('appointment_knocking_data', $data);
        return $this->db->insert_id();
    }

    public function update()
    {

    }

    private function update_by_appointment_id($data, $isInsert = true)
    {
        $appointment_id = $data['appointment_id'];
        // check appointment exist
        $CI =& get_instance();
        $CI->load->model('appointment_model');
        if ($CI->appointment_model->getById($appointment_id) != null) {
            // check data exists
            if ($this->checkExistence($appointment_id)) {
                // update
                $this->db->where('appointment_id', $appointment_id);
                $this->db->update('appointment_knocking_data', $data);
                return [
                    'message' => 'Knocking data updated successfully.',
                    'status' => 'updated',
                    'bool' => true
                ];
            } else {
                // insert
                if ($isInsert) {
                    $data['appointment_id'] = $appointment_id;
                    $data['added_at'] = date('Y-m-d H:i:s', time());
                    $knocking_id_inserted = $this->insert($data);
                    return [
                        'message' => 'Knocking data inserted successfully.',
                        'status' => 'inserted',
                        'knocking_id_inserted' => $knocking_id_inserted,
                        'bool' => true
                    ];
                }
            }
        } else {
            return [
                'message' => 'Appointment not exist.',
                'bool' => false
            ];
        }
    }

    public function doctor_available($appointment_id)
    {
        return $this->update_by_appointment_id([
            'appointment_id' => $appointment_id,
            'is_doctor_available' => 1
        ]);
    }

    public function doctor_unavailable($doctor_id)
    {
        $CI =& get_instance();
        $CI->load->model('doctor_model');
        $appointments = $CI->doctor_model->getFutureAppointmentForDoctors($doctor_id);
        if ($appointments) {
            foreach ($appointments as $appointment) {
                $this->update_by_appointment_id([
                    'appointment_id' => $appointment->id,
                    'is_doctor_available' => 0,
                    'is_patient_approved' => 0
                ], false);
            }
        }
    }

    public function patient_approved($knocking_id)
    {
        if ($this->checkExistence($knocking_id, 'id')) {
            if ($this->is_doctor_available($knocking_id)) {
                $knocking_data = $this->get_by_id($knocking_id);
                return $this->update_by_appointment_id([
                    'appointment_id' => $knocking_data->appointment_id,
                    'is_patient_approved' => 1
                ]);
            } else {
                return [
                    'message' => 'Doctor is not in consultation page',
                    'bool' => false
                ];
            }
        } else {
            return [
                'message' => 'This knocking data is not exist',
                'bool' => false
            ];
        }
    }

    public function check_doctor_availability($patient_id)
    {
        $CI =& get_instance();
        $CI->load->model('patient_model');
        $appointments = $CI->patient_model->getFutureAppointmentForPatient($patient_id);
        if ($appointments) {
            foreach ($appointments as $appointment) {
                if ($this->checkExistence($appointment->id)) {
                    $this->db->select('id, appointment_id, is_doctor_available');
                    $this->db->where('appointment_id', $appointment->id);
                    $appointment_knocking_data = $this->db->get('appointment_knocking_data')->row();
                    return [
                        'bool' => (($appointment_knocking_data->is_doctor_available == 0) ? false : true),
                        'id' => $appointment_knocking_data->id,
                        'appointment_id' => $appointment_knocking_data->appointment_id
                    ];
                }
            }
        }
        return false;
    }

    public function check_patient_approval($appointment_id)
    {
        if ($this->checkExistence($appointment_id)) {
            $this->db->select('id, is_doctor_available, is_patient_approved');
            $this->db->where('appointment_id', $appointment_id);
            $appointment_knocking_data = $this->db->get('appointment_knocking_data')->row();
            return [
                'bool' => ((($appointment_knocking_data->is_doctor_available == 0) || ($appointment_knocking_data->is_patient_approved == 0)) ? false : true),
                'id' => $appointment_knocking_data->id
            ];
        }
        return false;
    }

}