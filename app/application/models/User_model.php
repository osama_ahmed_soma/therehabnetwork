<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Class Login_model
 *
 * @property CI_Session $session
 * @property CI_DB_query_builder $db
 */
class User_model extends CI_Model
{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_user($id = null)
    {
        if ( null === $id )
            $id = $this->session->userdata("userid");

        $this->db->where('id', $id);
        $this->db->limit('1');
        $query = $this->db->get('user');

        return $query->row();
    }

    public function get_user_field($data = [])
    {
        foreach ($data as $key => $value){
            $this->db->where($key, $value);
        }
        $this->db->limit('1');
        $query = $this->db->get('user');

        return $query->row();
    }

    public function getName($field, $value)
    {
        $this->db->select("user_name");
        $this->db->from('user');
        $this->db->where($field, $value);
        $row = $this->db->get()->row();
        if (!$row) {
            return false;
        }
        return $row->user_name;
    }

    public function getEmail($field, $value){
        $this->db->select("user_email");
        $this->db->from('user');
        $this->db->where($field, $value);
        $row = $this->db->get()->row();
        if (!$row) {
            return false;
        }
        return $row->user_email;
    }

    public function email_exists($email, $user_id = null)
    {
        $email = $this->security->xss_clean($email);

        $this->db->select("*");
        $this->db->from('user');
        $this->db->where('user_email', $email);
        if ($user_id !== null) {
            $this->db->where('id !=', $user_id);
        }
        $row = $this->db->get()->row();
        if (!$row) {
            return false;
        }
        return true;
    }

    public function update($data, $field, $value){
        $this->db->where($field, $value);
        $this->db->update('user', $data);
		return ($this->db->affected_rows() > 0) ? true : false;
    }

    public function changePassword($data, $field, $value)
    {
        $this->db->where($field, $value);
        $this->db->update('user', $data);
    }
	
	public function token_match($token) {
		$this->db->where('token', $token);
		$this->db->where('token_expiry > ', 'NOW()', false); // correct
		$this->db->where('is_active', 1);
		$this->db->from('user');
		$num_res = $this->db->count_all_results();
		if ($num_res == 1) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
}