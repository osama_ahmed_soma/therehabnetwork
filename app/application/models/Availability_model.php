<?php
/**
 * Created by PhpStorm.
 * User: ziku
 * Date: 8/22/16
 * Time: 7:56 PM
 */
class Availability_Model extends CI_Model
{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function check_availability($id, $date, $time)
    {
        $this->db->where('start_date', $date);
        $this->db->where('start_time', $time);
        $this->db->where('userid', $id);
        $query = $this->db->get('available_time');
        if ($query->num_rows() > 0) {
            // If there is a user, then create session data
            /* foreach ($query->result() as $row) {
              $result[] = $row;
              } */
            return false;
        } else {
            return true;
        }
    }

    public function availability_doctors()
    {

        $start_date = $this->input->post('start_date');
        $start_date = new DateTime($start_date);
        $start_date = $start_date->format('Y-m-d');
        $start_time = $this->input->post('start_time');
        $start_timezone = $this->input->post('start_timezone');
        $end_date = $this->input->post('end_date');
        $end_date = new DateTime($end_date);
        $end_date = $end_date->format('Y-m-d');
        $end_time = $this->input->post('end_time');
        $end_timezone = $this->input->post('end_timezone');
        $repeat = $this->input->post('repeat');
        $repeat_type = $this->input->post('repeat_type');
        $repeat_freq = $this->input->post('repeat_frequency');
        $day = $this->input->post('day');
        $endloop = $this->input->post('end_loop');
        $id = $this->session->userdata('userid');
        //return date('Y-m-d', strtotime('+1 week next friday', strtotime($start_date)));
        $day_count = count($day);
        $endloop = strtotime($endloop);
        $start = strtotime($start_date);
        $startday = strtolower(date('l', strtotime($start_date)));
        $startdate = strtotime($start_date);
        //return $day[0];
        //$last_day=date('Y-m-d', strtotime('+'.$repeat_freq.$repeat_type.$last_day[$day_count-1], strtotime($start_date)));
        if ($repeat == '1') {
            if ($repeat_type == "week") {
                while ($endloop >= $start) {
                    for ($i = 0; $i < $day_count; $i++) {
                        if ($day[$i] == $startday) {
                            $new_start_date = date('Y-m-d', strtotime($start_date));
                            $month = date('m', strtotime($start_date));
                        } else {
                            $new_start_date = date('Y-m-d', strtotime("next " . $day[$i], strtotime($start_date)));
                            $month = date('m', strtotime($start_date));
                        }
                        $newstart = strtotime($new_start_date);
                        if ($endloop >= $newstart) {
                            $check_av = $this->check_availability($id, $new_start_date, $start_time . ":00");
                            if ($check_av) {
                                $data = array(
                                    'userid' => $id,
                                    'start_date' => $new_start_date,
                                    'end_date' => $new_start_date,
                                    'start_time' => $start_time,
                                    'end_time' => $end_time,
                                    'start_time_zone' => $start_timezone,
                                    'end_time_zone' => $end_timezone,
                                    'month' => $month,
                                    'status' => 'yes'
                                );
                                $this->db->insert('available_time', $data);
                                //$temp_day = $day[$i];
                            } else {
                                return [
                                    'message' => 'Availability already exists with same date and time',
                                    'bool' => false
                                ];
                            }
                        }
                    }
                    $start_date = date('Y-m-d', strtotime('+' . $repeat_freq . ' ' . $repeat_type, strtotime($start_date)));
                    $end_date = $start_date;
                    $start = strtotime($start_date);
                }
            }
            if ($repeat_type == "month") {
                $new_start_date = $start_date;
                $newstart = strtotime($new_start_date);
                //return $endloop."-------".$start;
//                if($endloop > $newstart){
//                    return "sdfd";
//                }
                do {
                    $month = date('m', strtotime($new_start_date));
                    if ($endloop >= $newstart) {
                        $check_av = $this->check_availability($id, $new_start_date, $start_time . ":00");
                        if ($check_av) {
                            $data = array(
                                'userid' => $id,
                                'start_date' => $new_start_date,
                                'end_date' => $new_start_date,
                                'start_time' => $start_time,
                                'end_time' => $end_time,
                                'start_time_zone' => $start_timezone,
                                'end_time_zone' => $end_timezone,
                                'month' => $month,
                                'status' => 'yes'
                            );
                            $this->db->insert('available_time', $data);
                            //$temp_day = $day[$i];
                        } else {
                            return [
                                'message' => 'Availability already exists with same date and time',
                                'bool' => false
                            ];
                        }
                    }
                    $new_start_date = date('Y-m-d', strtotime('+' . $repeat_freq . ' ' . $repeat_type, strtotime($new_start_date)));
                    $end_date = $new_start_date;
                    $start = strtotime($new_start_date);
                } while ($endloop >= $start);
            }
            if ($repeat_type == "day") {
                $new_start_date = $start_date;
                $newstart = strtotime($new_start_date);
                do {
                    $month = date('m', strtotime($new_start_date));
                    if ($endloop >= $newstart) {
                        $check_av = $this->check_availability($id, $new_start_date, $start_time . ":00");
                        if ($check_av) {
                            $data = array(
                                'userid' => $id,
                                'start_date' => $new_start_date,
                                'end_date' => $new_start_date,
                                'start_time' => $start_time,
                                'end_time' => $end_time,
                                'start_time_zone' => $start_timezone,
                                'end_time_zone' => $end_timezone,
                                'month' => $month,
                                'status' => 'yes'
                            );
                            $this->db->insert('available_time', $data);
                            //$temp_day = $day[$i];
                        } else {
                            return [
                                'message' => 'Availability already exists with same date and time',
                                'bool' => false
                            ];
                        }
                    }
                    $new_start_date = date('Y-m-d', strtotime('+ ' . $repeat_freq . ' day', strtotime($new_start_date)));
                    $end_date = $new_start_date;
                    $start = strtotime($new_start_date);
                } while ($endloop >= $start);
            }
        } else {

            //first count number of days
            $start_date_stamp = strtotime( $start_date );
            $end_date_stamp = strtotime( $end_date );

            if ( $start_date_stamp === $end_date_stamp ) {
                //same day
                $data = array();
                $check_av = $this->check_availability($id, $start_date, $start_time . ":00");
                $month = date('m', strtotime($start_date));
                $sds = strtotime($start_date . ' ' . $start_time . ':00');
                $eds = strtotime($end_date . ' ' . $end_time . ':00');
                if ($check_av) {
                    $data[] = array(
                        'userid' => $id,
                        'start_date' => $start_date,
                        'start_date_stamp' => $sds,
                        'end_date' => $end_date,
                        'end_date_stamp' => $eds,
                        'start_time' => $start_time,
                        'end_time' => $end_time,
                        'start_time_zone' => $start_timezone,
                        'end_time_zone' => $end_timezone,
                        'month' => $month,
                        'status' => 'yes'
                    );
                }
            }
            elseif ( $end_date_stamp > $start_date_stamp ) {
                //multiple day
                $data = array();
                while ( $start_date_stamp <= $end_date_stamp ) {
                    //
                    $str_date = date('Y-m-d', $start_date_stamp);
                    $str_time = $start_time . ':00';
                    $month = date('m', $start_date_stamp);
                    $sds = strtotime($str_date . ' ' . $start_time . ':00');
                    $eds = strtotime($str_date . ' ' . $end_time . ':00');

                    $check_av = $this->check_availability($id, $str_date, $str_time);

                    if ($check_av) {
                        $data[] = array(
                            'userid' => $id,
                            'start_date' => $str_date,
                            'start_date_stamp' => $sds,
                            'end_date' => $str_date,
                            'end_date_stamp' => $eds,
                            'start_time' => $start_time,
                            'end_time' => $end_time,
                            'start_time_zone' => $start_timezone,
                            'end_time_zone' => $end_timezone,
                            'month' => $month,
                            'status' => 'yes'
                        );
                    }

                    $start_date_stamp = strtotime( '+1 day', $start_date_stamp );

                }

            }
//            return $start_date.' '.$end_date;
            if ( is_array($data) && !empty( $data ) && count($data) == 1 ) {

//                $q = $this->db->insert('available_time', $data[0]);
                $q = $this->db->insert_batch('available_time', $data);
                if ($q) {
                    return [
                        'message' => 'Availability Added',
                        'bool' => true
                    ];
                } else {
                    return [
                        'message' => 'Availability Not Added',
                        'bool' => false
                    ];
                }
            } elseif ( is_array($data) && !empty( $data ) && count($data) > 1 ) {

//                return json_encode($data);

                $q = $this->db->insert_batch('available_time', $data);
                if ($q) {
                    return [
                        'message' => 'Availability Added',
                        'bool' => true
                    ];
                } else {
                    return [
                        'message' => 'Availability Not Added',
                        'bool' => false
                    ];
                }
            } else {
                return [
                    'message' => 'Availability already exists with same date and time',
                    'bool' => false
                ];
            }
        }
    }

    public function available_time()
    {
        $id = $this->session->userdata('userid');
        /*$this->db->select('available_time.*');
        $this->db->join('appointment', 'available_time.id = appointment.available_id');
        $this->db->join('doctor', 'available_time.userid = doctor.id');*/


        $this->db->where('userid', $id);
        $query = $this->db->get('available_time');
        $result = array();
        if ($query->num_rows() > 0) {
            // If there is a user, then create session data
            foreach ($query->result() as $row) {
                $result[] = $row;
            }
            return $result;
        } else {
            return false;
        }
    }

    public function getDoctorAppoinmentByAvailabelID($doctorID, $availableID, $start_date_stamp, $end_date_stamp){
        $this->db->select('appointment.id AS appointment_id, appointment.status AS app_status, reason.name AS reason_name, patient.firstname, patient.lastname');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('doctors_id', $doctorID);
        $this->db->where('appointment.available_id', $availableID);
        $this->db->where('appointment.start_date_stamp', $start_date_stamp);
        $this->db->where('appointment.end_date_stamp', $end_date_stamp);
        $query = $this->db->get('appointment');
        return $query->result();

    }

    /*public function getFutureAppointmentForDoctors($id)
    {
        $now = time();
        $this->db->select('appointment.*, appointment.id AS appointment_id, reason.name AS reason_name, patient.firstname, patient.lastname, appointment.status as app_status');
        $this->db->join('available_time', 'available_time.id = appointment.available_id');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('doctors_id', $id);
        $this->db->where("appointment.start_date_stamp >=", $now);
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        $query = $this->db->get('appointment');

        return $query->result();
    }*/

    public function update_time(){
        $start_date = $this->input->post('start_date');
        $start_date = new DateTime($start_date);
        $start_date = $start_date->format('Y-m-d');
        $start_time = $this->input->post('start_time');
        $end_date = $this->input->post('end_date');
        $end_date = new DateTime($end_date);
        $end_date = $end_date->format('Y-m-d');
        $end_time = $this->input->post('end_time');

        $id = $this->session->userdata('userid');

        $data = array();
        $check_av = $this->check_availability($id, $end_date, $end_time . ":00");
        $sds = strtotime($start_date . ' ' . $start_time . ':00');
        $eds = strtotime($end_date . ' ' . $end_time . ':00');

        if ($check_av) {
            $data = array(
                'start_date_stamp' => $sds,
                'end_date_stamp' => $eds,
                'start_time' => $start_time,
                'end_time' => $end_time
            );
        }

        if ( is_array($data) && !empty( $data )){
            $this->db->where('userid', $id);
            $this->db->where('id', $this->input->post('id'));
            $q = $this->db->update('available_time',$data);
            if ($q) {
                return "Availability Updated";
            } else {
                return "Availability Not Updated";
            }
        } else {
            return "Availability already exists with same date and time";
        }
    }

    public function remove_time(){
        $userID = $this->session->userdata('userid');
        $this->db->where('userid', $userID);
        $this->db->where('id', $this->input->post('id'));
        $this->db->delete('available_time');
    }
}