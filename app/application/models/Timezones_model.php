<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Timezones_Model extends CI_Model
{

    public function add($data = [])
    {
        $this->db->insert('timezones', $data);
        return $this->db->insert_id();
    }

    public function update($where = [], $values = [])
    {
        foreach ($where as $index => $value) {
            $this->db->where($index, $value);
        }
        $this->db->update('timezones', $values);
    }

    public function get($data = [])
    {
        $this->db->select(implode(', ', array_keys(array_filter($data, function ($value) {
            return ($value == '');
        }))));
        foreach ($data as $index => $value) {
            if ($value != '') {
                $this->db->where($index, $value);
            }
        }
        $this->db->order_by('order_by', 'ASC');
        return $this->db->get('timezones')->result();
    }

    public function remove($id)
    {
        $this->db->delete('timezones', [
            'id' => $id
        ]);
    }

    public function search($value, $type = '')
    {
        $this->db->select('*');
        if ($type == '') {
            $this->db->or_like([
                'var' => $value,
                'name' => $value
            ]);
        }
        return $this->db->get('timezones')->result();
    }
}