<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Class Login_model
 *
 * @property CI_Session $session
 * @property CI_DB_query_builder $db
 */
class Login_model extends CI_Model
{

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function all_user()
    {
        $query = $this->db->query("select * from user");

        $row = $query->row();

        if (isset($row)) {
            echo $query->num_rows();
            echo $row->user_name;
            echo $row->user_email;
            echo $row->user_password;
        }
    }

    public function get_user($user_id)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('id', $user_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return null;
        }
    }

    public function getLoginUser($email, $password, $isApi = false)
    {
        $email = $this->security->xss_clean($email);
        $password = $this->security->xss_clean($password);

        $this->db->select("*");
        $this->db->from('user');
        $this->db->where('user_email', $email);
//        $this->db->where('user_password', $password);
        $this->db->where('is_active', 1);

        $row = $this->db->get()->row();
//		dd($row);
		
        if (!$row) {
            return false;
        }
		
		if (password_verify($password, $row->user_password)) {
//			dd('Password is valid!');
		} else {
			return false;
		}
		
        $data = array(
            'userid' => $row->id,
            'useremail' => $email,
            'username' => $row->user_name,
            'userpassword' => $row->user_password,
            //'full_name' => $row->firstname . ' ' . $row->lastname,
            'type' => $row->type,
            //'image' => $row->image_url,
            'logged_in' => true,
        );
        if ($row->type == 'patient') {
            $this->db->from('patient');
            $this->db->where('user_id', $row->id);

            $data['patient'] = 'patient';
        }
        else {
            $this->db->from('doctor');
            $this->db->where('userid', $row->id);

            $data['doctor'] = 'doctor';
        }

        $this->db->select("*");
        $row = $this->db->get()->row();
        $data['full_name'] = $row->firstname . ' ' . $row->lastname;
        
		// saving user's timezone in session
		$data['user_timezone'] = $row->timezone;
		
        if ($row->image_url != "") {
            $data['image'] = $row->image_url;
        }
        else {
//            $data['image'] = 'template_file/img/default-user.jpg';
            $data['image'] = 'template_file/only_logo.png';
        }

        if(!$isApi){
            $this->session->set_userdata($data);
            return $data['type'];
        } else {
            return $data;
        }
    }
	
    public function LoginAsUser($id)
    {
        $this->db->select("*");
        $this->db->from('user');
        $this->db->where('id', $id);
        $this->db->where('is_active', 1);
        $row = $this->db->get()->row();
        if (!$row) {
            return false;
        }

        $data = array(
            'userid' => $row->id,
            'useremail' => $row->user_email,
            'username' => $row->user_name,
            'userpassword' => $row->user_password,
            //'full_name' => $row->firstname . ' ' . $row->lastname,
            'type' => $row->type,
            //'image' => $row->image_url,
            'logged_in' => true
        );
        if ($row->type == 'patient') {
            $this->db->from('patient');
            $this->db->where('user_id', $row->id);
            $data['patient'] = 'patient';
        }
        else {
            $this->db->from('doctor');
            $this->db->where('userid', $row->id);
            $data['doctor'] = 'doctor';
        }

        $this->db->select("*");
        $row = $this->db->get()->row();
        $data['full_name'] = $row->firstname . ' ' . $row->lastname;
        
		// saving user's timezone in session
		$data['user_timezone'] = $row->timezone;
		
		if ($row->image_url != "") {
            $data['image'] = $row->image_url;
        }
        else {
//            $data['image'] = 'template_file/img/default-user.jpg';
            $data['image'] = 'template_file/only_logo.png';
        }

        $this->session->set_userdata($data);

        return $data['type'];
    }

    public function update_email_password($user)
    {
        $this->db->where('id', $this->session->userdata('userid'));
        $this->db->update('user', $user);
    }

    public function update_password( $data, $where ) {
        if ( is_array($data) && !empty($data) && is_array($where) && !empty($where) ) {
            $this->db->update('user', $data, $where);

            return $this->db->affected_rows();
        }

        return NULL;
    }

    public function update_doctor_email($email)
    {
        $this->db->set('email', $email);
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->update('doctor');
    }

    public function update_patient_email($email)
    {
        $this->db->set('email', $email);
        $this->db->where('user_id', $this->session->userdata('userid'));
        $this->db->update('patient');
    }

    public function message_send($data)
    {
        $this->db->insert('private_messages', $data);
        return $this->db->insert_id();
    }

    public function insert_health_condition($condition = '', $source = '', $date = '', $dosage = '', $freq = '', $time = '', $severity = '', $reaction = '', $type = '', $ndc = '')
    {
        $id = $this->session->userdata('userid');
        $data = array(
            'type' => $type,
            'condition_term' => $condition,
            'date' => $date,
            'source' => $source,
            'dosage' => $dosage,
            'frequency' => $freq,
            'time' => $time,
            'severity' => $severity,
            'reaction' => $reaction,
            'patients_id' => $id,
            'ndc' => $ndc
        );
        $this->db->insert('health', $data);
        return $this->db->insert_id();
    }

    public function insert_doctor_specialty($specialty = '', $id = null)
    {
        if($id == null){
            $id = $this->session->userdata('userid');
        }
		$data = array(
            'userid' => $id,
            'speciality' => $specialty
        );
        $this->db->insert('doctor_specialization', $data);
        return $this->db->insert_id();
    }

    public function insert_doctor_education($fr_year = '', $to_year = '', $title = '', $university = '')
    {
        $id = $this->session->userdata('userid');
        $data = array(
            'userid' => $id,
            'from_year' => $fr_year,
            'to_year' => $to_year,
            'name' => $title,
            'institute' => $university
        );
        $this->db->insert('doctor_education', $data);
        return $this->db->insert_id();
    }

    public function insert_doctor_award($awrd_title, $award_orga, $year)
    {
        $id = $this->session->userdata('userid');
        $data = array(
            'userid' => $id,
            'name' => $awrd_title,
            'venue' => $award_orga,
            'year' => $year
        );
        $this->db->insert('doctor_award', $data);
        return $this->db->insert_id();
    }

    public function insert_doctor_membership($membership_title = '', $membership_orga = '')
    {
        $id = $this->session->userdata('userid');
        $data = array(
            'userid' => $id,
            'membership_post' => $membership_title,
            'society_name' => $membership_orga
        );
        $this->db->insert('doctor_professional_membership', $data);
        return $this->db->insert_id();
    }


    public function find_users_for_chat($contact_user_id)
    {
        $this->db->select('*');
        $this->db->from('contact_list');
        $this->db->join('user', 'user.id = contact_list.to_contact_person');
        $this->db->where('contact_user', $contact_user_id);
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function find_chat_partner($partner_id)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('id', $partner_id);
        $query_result = $this->db->get();
        return $query_result->row();
    }

    public function find_chat($partner_id)
    {
        $this->db->select('*');
        $this->db->from('chats');
        $this->db->where('created_by', $partner_id);
        $query_result = $this->db->get();
        return $query_result->row();
    }

    public function ajax_message_save($receiver, $sender, $message)
    {
        $data_array = array(
            'to_userid' => $receiver,
            'from_userid' => $sender,
            'messages' => $message
        );
//        $query = "INSERT INTO private_messages (to_userid,from_userid,messages) VALUES (?,?,?)";
//        $this->db->query($query, array($receiver,$sender,$message));
        $this->db->insert('private_messages', $data_array);
        //return $this->db->insert_id();
        //return $this->db->insert_id();
    }

    public function message_save($receiver, $sender, $message)
    {
        $data_array = array(
            'to_userid' => $receiver,
            'from_userid' => $sender,
            'messages' => $message
        );
//            $query = "INSERT INTO private_messages (to_userid,from_userid,messages) VALUES (?,?,?)";
//            $this->db->query($query, array($receiver,$sender,$message));
        $this->db->insert('private_messages', $data_array);
        //return $this->db->insert_id();
        //return $this->db->insert_id();
    }

    public function ajax_getting_chat_messages($sender_id, $reciever_id)
    {
        //$this->db->select("DATE_FORMAT('time_sent', '%D of %M %Y at %H:%i:%s')");
//        $this->db->select('*');
////        $this->db->from('private_messages');
////        $this->db->where('to_userid',$reciever_id);
////        $this->db->where('from_userid',$sender_id);
//        $query_result = $this->db->get();
//        $result = $query_result->result();
//
        $this->db->select('*');
        $this->db->from('private_messages');
        $this->db->where('from_userid', $sender_id);
        $this->db->where('to_userid', $reciever_id);

        $this->db->or_where('from_userid', $reciever_id);
        $this->db->or_where('to_userid', $sender_id);
        $this->db->join('user', 'user.id = private_messages.from_userid');
        $this->db->order_by('time_sent', 'asc');
        $query_result = $this->db->get();
        $result = $query_result->result();

//        $query_string = "SELECT 
//                        pm.`to_userid`, 
//                        pm.messages, pm.`from_userid`, 
//                        DATE_FORMAT(pm.`time_sent`, '%D of %M %Y at %H:%i:%s') AS chat_message_timestamp, 
//                        us.user_name 
//                        FROM `private_messages` pm 
//                        JOIN user us ON pm.`to_userid`= us.id
//                        WHERE pm.from_userid = ?";
//        
//        
////        $query_string = "SELECT 
////                        * 
////                        FROM private_messages";
//        $result = $this->db->query($query_string, $sender_id);
        return $result;
    }

    public function get_doctor_finance($doctor_id)
    {
        $this->db->select('*');
        $this->db->from('doctor');
        $this->db->where('userid', $doctor_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return 'No data found on doctor!';
        }
    }

    public function update_doctor_payment($payment)
    {
        $this->db->set('payment_rate', $payment);
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->update('doctor');
    }

    public function update_doctor_stripe($stripes)
    {
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->update('doctor', $stripes);
    }


    public function insert_doctor_licenced_state($licenced_state)
    {
        $id = $this->session->userdata('userid');
        //$data = array();
        $data = array();

        for ($i = 0; $i < count($licenced_state); $i++) {
            $data[] = array('userid' => $id, 'licenced_state_name' => $licenced_state[$i]);
            //$this->db->insert('doctor_licenced_states', $data);
        }

//            $data = array(
//                'userid' => $id,
//                'licenced_state_name' => $licenced_state
//            );
        foreach ($data as $key => $value) {
            $query = "insert into doctor_licenced_states (userid, licenced_state_name) values ('" . $value['userid'] . "','" . $value['licenced_state_name'] . "')";
            $this->db->query($query);
            //$this->db->insert('doctor_licenced_states', $item);
        }
        //$this->db->insert('doctor_licenced_states', $data);
        return $this->db->insert_id();
    }

    public function get_health($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('health');
        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $result[] = $row;
            }
            return $result;
        } else
            return "No result found";
    }

    public function get_doctor($id)
    {
        $this->db->select('*');
        $this->db->from('doctor');
        $this->db->where('userid', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else
            return "No result found";
    }

    public function get_patient($id)
    {
        $this->db->join('user', 'patient.user_id =user.id');
        $this->db->where('user_id', $id);
        $query = $this->db->get('patient');
        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $result[] = $row;
            }
            return $result;
        } else
            return "No result found";
    }

    public function get_doctors($id)
    {
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor');
        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $result[] = $row;
            }
            return $result;
        } else
            return "No result found";
    }

    public function get_patients()
    {
        $id = $this->session->userdata("userid");

        return $this->db->where('user_id', $id)
            ->get('patient')
            ->row();
    }

    public function get_health_condition($type)
    {
        $this->db->where('type', $type);
        $query = $this->db->get('health');
        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $result[] = $row;
            }
            return $result;
        } else
            return "No result found";
    }

    public function get_drugs(){
        $query = $this->db->get('drug_list');
        $result = array();
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row) {
                $result[] = $row;
            }
            return $result;
        } else {
            return "No result found";
        }
    }

    public function get_patient_health_condition($type, $patient_id, $limit = null, $page = null)
    {
        $this->db->select('*');
        $this->db->from('health');
        $this->db->where('type', $type);
        $this->db->where('patients_id', $patient_id);
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        $query = $this->db->get();

        return $query->result();
    }

    public function get_health_patient($patient_id)
    {
        $this->db->select('*');
        $this->db->from('health');
        $this->db->where('patients_id', $patient_id);
        $query = $this->db->get();

        return $query->result();
    }

    public function delete_condition($cid, $uid)
    {
        $id = $this->session->userdata("userid");
        if ($id == $uid) {
            //$this->db->where('type', $type);
            $this->db->where('id', $cid);
            $query = $this->db->delete('health');
            if ($query) {
                return true;
            } else
                return false;
        }
    }

    public function update_condition($cid, $uid)
    {
        $id = $this->session->userdata("userid");
        if ($id == $uid) {
            //$this->db->where('type', $type);
            $this->db->where('id', $cid);
            $data = array(
                'title' => 'My title',
                'name' => 'My Name',
                'date' => 'My date'
            );
            $query = $this->db->delete('health');
            if ($query) {
                return true;
            } else
                return false;
        }
    }

    public function add_to_fav($pid, $did)
    {
        $data = array(
            'p_id' => $pid,
            'd_id' => $did,
            'status' => "pending"
        );
        $this->db->where('p_id', $pid);
        $this->db->where('d_id', $did);
        $query = $this->db->get('favorite');
        if ($query->num_rows() > 0) {
            return "Already Added to Favorite";
        } else {
            $this->db->set('time', 'NOW()', FALSE);
            $this->db->insert('favorite', $data);
            return $this->db->insert_id();
        }
    }

    public function get_added_favorite($fav_id)
    {
        $this->db->select('*');
        $this->db->from('favorite');
        $this->db->where('id', $fav_id);
        $query_result = $this->db->get();
        if ($query_result->num_rows() > 0) {
            return $query_result->row();
        } else {
            return null;
        }
    }

    public function get_favorite($did)
    {
        $pid = $this->session->userdata('userid');
        $this->db->where('p_id', $pid);
        $this->db->where('d_id', $did);
        $query = $this->db->get('favorite');
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function get_favorite_doctor($pid)
    {
        $did = $this->session->userdata('userid');
        $this->db->where('p_id', $pid);
        $this->db->where('d_id', $did);
        $query = $this->db->get('favorite');
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

//    public function get_providers() {
//        $pid = $this->session->userdata('userid');
//        $this->db->select('*');
//        $this->db->from('favorite');
//        $this->db->where('p_id', $pid);
//        $query = $this->db->get();
//        return $query->result();
//
//
//    }

    public function get_providers()
    {
        $id = $this->session->userdata('userid');
        $this->db->where('p_id', $id);
        $query = $this->db->get('favorite');
        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $special[] = $this->get_doctors_specialization($row->d_id);
                $degree[] = $this->get_doctors_degree($row->d_id);
                $this->db->where('userid', $row->d_id);
                $d_qr = $this->db->get('doctor');
                foreach ($d_qr->result() as $drow) {
                    $result[] = $drow;
                }
                //array_merge($result,$r);
            }
        }
        if (!empty($special) && !empty($degree) && !empty($result)) {
            return array('specialization' => $special, 'degree' => $degree, 'general_info' => $result);
        }
    }

    public function remove_from_fav($did)
    {
        $this->db->where('d_id', $did);
        $query = $this->db->delete('favorite');
        return true;
    }

    public function remove_from_favorite($pid, $did)
    {
        $this->db->where('p_id', $pid);
        $this->db->where('d_id', $did);
        $query = $this->db->delete('favorite');
        return true;
    }

    public function get_event_number($date)
    {
        $this->db->where('date', $date);
        $this->db->where('status', '1');
        $query = $this->db->get('events');
        $i = $this->db->affected_rows();
        return $i;
    }

    public function get_event_title($date)
    {
        $this->db->where('date', $date);
        $this->db->where('status', '1');
        $query = $this->db->get('events');
        $result = array();
        foreach ($query->result() as $row) {
            $result[] = $row;
        }
        return $result;
    }

    public function check_availability($id, $date, $time)
    {
        $this->db->where('start_date', $date);
        $this->db->where('start_time', $time);
        $this->db->where('userid', $id);
        $query = $this->db->get('available_time');
        if ($query->num_rows() > 0) {
            // If there is a user, then create session data
            /* foreach ($query->result() as $row) {
              $result[] = $row;
              } */
            return false;
        } else {
            return true;
        }
    }



    public function availability_doctors()
    {
        $start_date = $this->input->post('start_date');
        $start_time = $this->input->post('start_time');
        $start_timezone = $this->input->post('start_timezone');
        $end_date = $this->input->post('end_date');
        $end_time = $this->input->post('end_time');
        $end_timezone = $this->input->post('end_timezone');
        $repeat = $this->input->post('repeat');
        $repeat_type = $this->input->post('repeat_type');
        $repeat_freq = $this->input->post('repeat_frequency');
        $day = $this->input->post('day');
        $endloop = $this->input->post('end_loop');
        $id = $this->session->userdata('userid');
        //return date('Y-m-d', strtotime('+1 week next friday', strtotime($start_date)));
        $day_count = count($day);
        $endloop = strtotime($endloop);
        $start = strtotime($start_date);
        $startday = strtolower(date('l', strtotime($start_date)));
        $startdate = strtotime($start_date);
        //return $day[0];
        //$last_day=date('Y-m-d', strtotime('+'.$repeat_freq.$repeat_type.$last_day[$day_count-1], strtotime($start_date)));
        if ($repeat == '1') {
            if ($repeat_type == "week") {
                while ($endloop >= $start) {
                    for ($i = 0; $i < $day_count; $i++) {
                        if ($day[$i] == $startday) {
                            $new_start_date = date('Y-m-d', strtotime($start_date));
                            $month = date('m', strtotime($start_date));
                        } else {
                            $new_start_date = date('Y-m-d', strtotime("next " . $day[$i], strtotime($start_date)));
                            $month = date('m', strtotime($start_date));
                        }
                        $newstart = strtotime($new_start_date);
                        if ($endloop >= $newstart) {
                            $check_av = $this->check_availability($id, $new_start_date, $start_time . ":00");
                            if ($check_av) {
                                $data = array(
                                    'userid' => $id,
                                    'start_date' => $new_start_date,
                                    'end_date' => $new_start_date,
                                    'start_time' => $start_time,
                                    'end_time' => $end_time,
                                    'start_time_zone' => $start_timezone,
                                    'end_time_zone' => $end_timezone,
                                    'month' => $month,
                                    'status' => 'yes'
                                );
                                $this->db->insert('available_time', $data);
                                //$temp_day = $day[$i];
                            } else {
                                return "Availability already exists with same date and time";
                            }
                        }
                    }
                    $start_date = date('Y-m-d', strtotime('+' . $repeat_freq . ' ' . $repeat_type, strtotime($start_date)));
                    $end_date = $start_date;
                    $start = strtotime($start_date);
                }
            }
            if ($repeat_type == "month") {
                $new_start_date = $start_date;
                $newstart = strtotime($new_start_date);
                //return $endloop."-------".$start;
//                if($endloop > $newstart){
//                    return "sdfd";
//                }
                do {
                    $month = date('m', strtotime($new_start_date));
                    if ($endloop >= $newstart) {
                        $check_av = $this->check_availability($id, $new_start_date, $start_time . ":00");
                        if ($check_av) {
                            $data = array(
                                'userid' => $id,
                                'start_date' => $new_start_date,
                                'end_date' => $new_start_date,
                                'start_time' => $start_time,
                                'end_time' => $end_time,
                                'start_time_zone' => $start_timezone,
                                'end_time_zone' => $end_timezone,
                                'month' => $month,
                                'status' => 'yes'
                            );
                            $this->db->insert('available_time', $data);
                            //$temp_day = $day[$i];
                        } else {
                            return "Availability already exists with same date and time";
                        }
                    }
                    $new_start_date = date('Y-m-d', strtotime('+' . $repeat_freq . ' ' . $repeat_type, strtotime($new_start_date)));
                    $end_date = $new_start_date;
                    $start = strtotime($new_start_date);
                } while ($endloop >= $start);
            }
            if ($repeat_type == "day") {
                $new_start_date = $start_date;
                $newstart = strtotime($new_start_date);
                do {
                    $month = date('m', strtotime($new_start_date));
                    if ($endloop >= $newstart) {
                        $check_av = $this->check_availability($id, $new_start_date, $start_time . ":00");
                        if ($check_av) {
                            $data = array(
                                'userid' => $id,
                                'start_date' => $new_start_date,
                                'end_date' => $new_start_date,
                                'start_time' => $start_time,
                                'end_time' => $end_time,
                                'start_time_zone' => $start_timezone,
                                'end_time_zone' => $end_timezone,
                                'month' => $month,
                                'status' => 'yes'
                            );
                            $this->db->insert('available_time', $data);
                            //$temp_day = $day[$i];
                        } else {
                            return "Availability already exists with same date and time";
                        }
                    }
                    $new_start_date = date('Y-m-d', strtotime('+ ' . $repeat_freq . ' day', strtotime($new_start_date)));
                    $end_date = $new_start_date;
                    $start = strtotime($new_start_date);
                } while ($endloop >= $start);
            }
        } else {
            $check_av = $this->check_availability($id, $start_date, $start_time . ":00");
            $month = date('m', strtotime($start_date));
            if ($check_av) {
                $data = array(
                    'userid' => $id,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'start_time' => $start_time,
                    'end_time' => $end_time,
                    'start_time_zone' => $start_timezone,
                    'end_time_zone' => $end_timezone,
                    'month' => $month,
                    'status' => 'yes'
                );
                $q = $this->db->insert('available_time', $data);
                if ($q) {
                    return "Availability Added";
                } else {
                    return "Availability Not Added";
                }
            } else {
                return "Availability already exists with same date and time";
            }
        }
    }

    public function available_time()
    {
        $id = $this->session->userdata('userid');
        $this->db->where('userid', $id);
        $query = $this->db->get('available_time');
        $result = array();
        if ($query->num_rows() > 0) {
            // If there is a user, then create session data
            foreach ($query->result() as $row) {
                $result[] = $row;
            }
            return $result;
        } else {
            return false;
        }
    }

    public function edit_speciality($speciality, $speciality_id)
    {
        $this->db->set('speciality', $speciality);
        $this->db->where('id', $speciality_id);
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->update('doctor_specialization');

    }

    public function edit_education($data, $education_id)
    {
        $this->db->where('id', $education_id);
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->update('doctor_education', $data);

    }

    public function edit_award($data, $award_id)
    {
        $this->db->where('id', $award_id);
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->update('doctor_award', $data);

    }

    public function edit_health($condition_id, $condition_type, $data)
    {
        $this->db->where('id', $condition_id);
        $this->db->where('type', $condition_type);
        $this->db->where('patients_id', $this->session->userdata('userid'));
        $this->db->update('health', $data);
    }

    public function edit_membership($data, $doctor_membership_id)
    {
        $this->db->where('membership_id', $doctor_membership_id);
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->update('doctor_professional_membership', $data);
    }

    public function erase_speciality($sp_id)
    {
        $this->db->where('id', $sp_id);
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->delete('doctor_specialization');
    }

    public function erase_education($id)
    {
        $this->db->where('id', $id);
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->delete('doctor_education');
    }

    public function erase_award($id)
    {
        $this->db->where('id', $id);
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->delete('doctor_award');
    }

    public function erase_membership($id)
    {
        $this->db->where('membership_id', $id);
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->delete('doctor_professional_membership');
    }

    public function get_patient_files($id)
    {
        $this->db->select("*");
        $this->db->from("files_upload");
        $this->db->where("user_id", $id);
        $query_result = $this->db->get();
        if ($query_result->num_rows() > 0) {
            return $query_result->result();
        } else {
            return null;
        }
    }

    public function save_appointment()
    {
        $pid = $this->input->post('pid');
        $did = $this->input->post('did');
        $avail = $this->input->post('avail');
        $reason_appointments = $this->input->post('reason_appointments');
        $appointment_notes = $this->input->post('appointment_notes');
        $data = array(
            'patients_id' => $pid,
            'doctors_id' => $did,
            'available_id' => $avail,
            'reason' => $reason_appointments,
            'notes' => $appointment_notes,
            'created_date' => date("Y-m-d H:i:s")
        );
        $this->db->insert('appointment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->reconnect();

        $this->db->where('id', $avail);
        $query = $this->db->update('available_time', array('status' => 'no'));
        return TRUE;
    }

    public function check_existing__appointment()
    {
        $pid = $this->input->post('pid');
        $did = $this->input->post('did');
        $avail = $this->input->post('avail');

        $this->db->where('patients_id', $pid);
        $this->db->where('doctors_id', $did);
        $this->db->where('available_id', $avail);

        $query = $this->db->get('appointment');
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function get_doctors_specialization($id)
    {
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor_specialization');
        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function get_doctors_degree($id)
    {
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor_degree');
        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function get_doctors_patients($id)
    {
        //$id = $this->session->userdata('userid');
        $this->db->where('d_id', $id);
        $query = $this->db->get('favorite');
        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                //$personal_info[] = $this->get_doctors_specialization($row->d_id);
                //$degree[] = $this->get_doctors_degree($row->d_id);
                $this->db->where('user_id', $row->p_id);
                $d_qr = $this->db->get('patient');
                foreach ($d_qr->result() as $prow) {
                    $result[] = $prow;
                }
                //array_merge($result,$r);
            }
        }
        if (!empty($result)) {
            return array('general_info' => $result);
        }
    }


    public function get_doctors_experience($id)
    {
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor_experience');
        $result = array();
        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function get_doctors_available($id, $month, $date)
    {
        //$this->db->group_by('month');
        $this->db->where('userid', $id);
        $this->db->where('month', $month);
        $this->db->where('start_date', $date);
        $this->db->where('status', "yes");
        $query = $this->db->get('available_time');

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function get_doctors_professiona_membership($id)
    {
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor_professional_membership');

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function get_doctors_licenced_states($id)
    {
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor_licenced_states');

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function get_doctors_other_licenced_states($id)
    {
        $this->db->select('state.name AS state_name, state.id AS state_id, doctor_other_licenses.id AS doctor_other_licenses_id');
        $this->db->join('state', 'state.id = doctor_other_licenses.state_id');
        $this->db->where('doctor_other_licenses.userid', $id);
        $query = $this->db->get('doctor_other_licenses');

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function get_doctors_awards($id)
    {
        $this->db->select('*');
        $this->db->from('doctor_award');
        $this->db->where('userid', $id);
        $query_result = $this->db->get();
        if ($query_result->num_rows() > 0) {
            return $query_result->result();
        } else {
            return null;
        }
    }

    public function get_doctors_language($id)
    {
        $this->db->select('doctor_language.*, language.name as Language_name');
        $this->db->join('language', 'doctor_language.language_id = language.id');
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor_language');
        $result = array();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else
            return [];
    }

    public function get_doctors_education($id)
    {
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor_education');
        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function get_appointment_patients($id)
    {
//        $pid = $this->input->post('pid');
//        $did = $this->input->post('did');
        $this->db->join('available_time', 'available_time.id = appointment.available_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->where('patients_id', $id);
        $this->db->order_by('start_date', 'DESC');
//        $this->db->where('doctors_id', $did);
        $query = $this->db->get('appointment');
        if ($query->num_rows() > 0) {
            // If there is a user, then create session data
            foreach ($query->result() as $row) {
                $result[] = $row;
            }
            return $result;
        } else {
            return false;
        }
    }

    //indexing post
    public function get_all_blogposts()
    {
        $query = $this->db->get('blogpost');
        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $result[] = $row;
            }
            return $result;
        } else {
            return "No result found";
        }
    }

    //single post
    public function post($id)
    {
        $this->db->where('blogpost_id', $id);
        $query = $this->db->get('blogpost');
        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $result[] = $row;
            }
            return $result;
        } else {
            return 'No result found';
        }
    }

    //pin blog post
    public function pin_post($post_id)
    {
        $u_id = $this->session->userdata("userid");
        $data = array(
            'userid' => $u_id,
            'post_id' => $post_id
        );
        $result = $this->db->insert('pinned_post', $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //pin updata data
    public function pin_post_updata($updata_id)
    {
        $user_id = $this->session->userdata('userid');
        if ($updata_id != $user_id OR $updata_id == $user_id) {
            $this->db->where('post_id', $updata_id);
            $query = $this->db->delete('pinned_post');
            if ($query) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    //Comment data insert
    public function sinlge_comment_insert($post_id, $comment_id)
    {
        $user_id = $this->session->userdata('userid');
        $data = array(
            'userId' => $user_id,
            'postId' => $post_id,
            'comment' => $comment_id
        );
        $query = $this->db->insert('post_comments', $data);
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //Comment data retrive from db
    public function comment_view($post_id)
    {
        $this->db->select('*');
        $this->db->from('post_comments');
        $this->db->where('postId', $post_id);
        $this->db->join('user', 'user.id = post_comments.userId');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return 'No result found';
        }
    }

    //pinned data retrive from db
    public function pinned_data_view()
    {
        $this->db->select('*');
        $this->db->from('pinned_post');
        $this->db->join('blogpost', 'blogpost.blogpost_id=pinned_post.post_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return 'No result found';
        }
    }

    public function patient_upload_file($filename, $title)
    {
        $id = $this->session->userdata("userid");
        $data = array(
            'filename' => $filename,
            'user_id' => $id,
            'title' => $title
        );
        $this->db->insert('files_upload', $data);
        return $this->db->insert_id();
    }

    public function patient_upload_retrive($pat_id)
    {
        $this->db->select('*');
        $this->db->from('files_upload');
        $this->db->where('user_id', $pat_id);
		$this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    public function doctor_retrive_upload($upID)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id', $upID);
        $this->db->join('files_upload', 'files_upload.user_id=user.id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return 'No result found';
        }
    }

    public function patient_upload_delete($dId)
    {
        $file = $this->image_id_find($dId);
        if (!$this->db->where('id', $dId)->delete('files_upload')) {
            return FALSE;
        }
        unlink('uploads/documents/' . $file->filename);
        return TRUE;
    }

    public function image_id_find($file_id)
    {
        return $this->db->select()
            ->from('files_upload')
            ->where('id', $file_id)
            ->get()
            ->row();
    }
    

}


