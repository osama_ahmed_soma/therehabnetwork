<?php

/**
 * Created by PhpStorm.
 * User: ziku
 * Date: 8/13/16
 * Time: 3:10 AM
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Messages_Model extends CI_Model {

	public $from_userid;
	public $to_userid;
	public $parent;
	public $subject;
	public $message;
	public $attachment;
	public $opened;
	public $draft;
	public $recipient_delete;
	public $sender_delete;
	public $added;

	public function __construct() {
		// Call the CI_Model constructor
		parent::__construct();
	}

	public function getInbox($limit = null, $page = null) {
		$this->db->select('msg.id, msg.subject, msg.from_userid, msg.message, msg.opened, msg.added, usr.firstname, usr.lastname, usr.image_url, usr.city, st.code');

		$where = array(
			'msg.to_userid' => $this->session->userdata('userid'),
			'msg.draft' => '0',
			'msg.recipient_delete' => '0'
		);

		$this->db->from('messages msg');
		if ($this->session->userdata('type') == 'patient') {
			$this->db->join('doctor usr', 'usr.userid = msg.from_userid');
			$this->db->join('state st', 'st.id = usr.state_id', 'left');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->db->join('patient usr', 'usr.user_id = msg.from_userid');
			$this->db->join('state st', 'st.id = usr.state_id', 'left');
		}
		$this->db->where($where);

		$this->db->order_by('msg.id', 'DESC');

		if ($limit !== null) {
			$this->db->limit($limit, $page);
		}

		$query_result = $this->db->get();
		return $query_result->result();
	}

	public function searchInboxMessages($limit = null, $page = null, $search_text = '', $count_only = false) {
		if ($count_only == false) {
			$this->db->select('msg.id, msg.subject, msg.from_userid, msg.message, msg.opened, msg.added, usr.firstname, usr.lastname, usr.image_url, usr.city, st.code');
		}
		$this->db->from('messages msg');
		if ($this->session->userdata('type') == 'patient') {
			$this->db->join('doctor usr', 'usr.userid = msg.from_userid');
			$this->db->join('state st', 'st.id = usr.state_id', 'left');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->db->join('patient usr', 'usr.user_id = msg.from_userid');
			$this->db->join('state st', 'st.id = usr.state_id', 'left');
		}
		$this->db->where(array(
			'msg.to_userid' => $this->session->userdata('userid'),
			'msg.draft' => '0',
			'msg.recipient_delete' => '0'
		));

		if ($search_text) {
			$search_text = $this->db->escape_str($search_text);
			$this->db->where("(msg.subject LIKE '%" . $search_text . "%' ESCAPE '!' OR  msg.message LIKE '%" . $search_text . "%' ESCAPE '!' )");
		}
		if ($count_only) {
			return $this->db->count_all_results();
		}

		$this->db->order_by('msg.id', 'DESC');
		if ($limit > 0) {
			$this->db->limit($limit, $page);
		}
		$query_result = $this->db->get();
		return $query_result->result();
	}

	public function countInbox() {
		$this->db->select('msg.id');

		$where = array(
			'msg.to_userid' => $this->session->userdata('userid'),
			'msg.draft' => '0',
			'msg.recipient_delete' => '0'
		);

		$this->db->where("'" . __METHOD__ . "' = '" . __METHOD__ . "'");

		$this->db->from('messages msg');
		if ($this->session->userdata('type') == 'patient') {
			$this->db->join('doctor usr', 'usr.userid = msg.from_userid');
			//$this->db->join('state st', 'st.id = usr.state_id');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->db->join('patient usr', 'usr.user_id = msg.from_userid');
			//$this->db->join('state st', 'st.id = usr.state_id');
		}
		$this->db->where($where);
		return $this->db->count_all_results();
	}

	public function getSentBox($limit = null, $page = null) {

		$this->db->select('msg.id, msg.subject, msg.to_userid, msg.message, msg.opened, msg.added, usr.firstname, usr.lastname, usr.image_url, usr.city, st.code');

		$where = array(
			'msg.from_userid' => $this->session->userdata('userid'),
			'msg.draft' => '0',
			'msg.sender_delete' => '0'
		);

		$this->db->from('messages msg');

		if ($this->session->userdata('type') == 'patient') {
			//if I'm patient, I'm sending message to doctor only
			//so get doctor information
			$this->db->join('doctor usr', 'usr.userid = msg.to_userid');
			$this->db->join('state st', 'st.id = usr.state_id', 'left');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->db->join('patient usr', 'usr.user_id = msg.to_userid');
			$this->db->join('state st', 'st.id = usr.state_id', 'left');
		}
		$this->db->where($where);

		$this->db->order_by('msg.id', 'DESC');

		if ($limit > 0) {
			$this->db->limit($limit, $page);
		}

		$query_result = $this->db->get();

		return $query_result->result();
	}

	public function searchSentMessages($limit = null, $page = null, $search_text = '', $count_only = false) {
		if ($count_only == false) {
			$this->db->select('msg.id, msg.subject, msg.from_userid, msg.message, msg.opened, msg.added, usr.firstname, usr.lastname, usr.image_url, usr.city, st.code');
		}
		$this->db->from('messages msg');
		if ($this->session->userdata('type') == 'patient') {
			$this->db->join('patient usr', 'usr.user_id = msg.from_userid');
			$this->db->join('state st', 'st.id = usr.state_id', 'left');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->db->join('doctor usr', 'usr.userid = msg.from_userid');
			$this->db->join('state st', 'st.id = usr.state_id', 'left');
		}
		$this->db->where(array(
			'msg.from_userid' => $this->session->userdata('userid'),
			'msg.draft' => '0',
			'msg.sender_delete' => '0'
		));
		if ($search_text) {
			$search_text = $this->db->escape_str($search_text);
			$this->db->where("(msg.subject LIKE '%" . $search_text . "%' ESCAPE '!' OR  msg.message LIKE '%" . $search_text . "%' ESCAPE '!' )");
		}
		if ($count_only) {
			return $this->db->count_all_results();
		}

		$this->db->order_by('msg.id', 'DESC');
		if ($limit > 0) {
			$this->db->limit($limit, $page);
		}
		$query_result = $this->db->get();
		return $query_result->result();
	}

	public function countSentBox() {
		$this->db->select('msg.id');

		$where = array(
			'msg.from_userid' => $this->session->userdata('userid'),
			'msg.draft' => '0',
			'msg.sender_delete' => '0'
		);

		if ($this->session->userdata('type') == 'patient') {
			$this->db->from('messages as msg, doctor as usr');
			$this->db->where($where);
			$this->db->where('usr.userid = msg.to_userid');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->db->from('messages as msg, patient as usr');
			$this->db->where($where);
			$this->db->where('usr.user_id = msg.to_userid');
		}

		return $this->db->count_all_results();
	}

	public function getDraftBox($limit = null, $page = null) {
		$this->db->select('msg.id, msg.subject, msg.to_userid, msg.message, msg.opened, msg.added, usr.firstname, usr.lastname, usr.image_url, usr.city, st.code');

		$where = array(
			'msg.from_userid' => $this->session->userdata('userid'),
			'msg.draft' => '1',
			'msg.sender_delete' => '0'
		);

		$this->db->from('messages msg');

		if ($this->session->userdata('type') == 'patient') {
			$this->db->join('patient usr', 'usr.user_id = msg.from_userid');
			$this->db->join('state st', 'st.id = usr.state_id', 'left');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->db->join('doctor usr', 'usr.userid = msg.from_userid');
			$this->db->join('state st', 'st.id = usr.state_id', 'left');
		}
		$this->db->where($where);

		$this->db->order_by('msg.id', 'DESC');

		if ($limit > 0) {
			$this->db->limit($limit, $page);
		}

		$query_result = $this->db->get();
		return $query_result->result();
	}

	public function searchDraftMessages($limit = null, $page = null, $search_text = '', $count_only = false) {
		if ($count_only == false) {
			$this->db->select('msg.id, msg.subject, msg.to_userid, msg.message, msg.opened, msg.added, usr.firstname, usr.lastname, usr.image_url, usr.city, st.code');
		}
		$this->db->from('messages msg');
		if ($this->session->userdata('type') == 'patient') {
			$this->db->join('patient usr', 'usr.user_id = msg.from_userid');
			$this->db->join('state st', 'st.id = usr.state_id', 'left');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->db->join('doctor usr', 'usr.userid = msg.from_userid');
			$this->db->join('state st', 'st.id = usr.state_id', 'left');
		}
		$this->db->where(array(
			'msg.from_userid' => $this->session->userdata('userid'),
			'msg.draft' => '1',
			'msg.sender_delete' => '0'
		));
		if ($search_text) {
			$search_text = $this->db->escape_str($search_text);
			$this->db->where("(msg.subject LIKE '%" . $search_text . "%' ESCAPE '!' OR  msg.message LIKE '%" . $search_text . "%' ESCAPE '!' )");
		}
		if ($count_only) {
			return $this->db->count_all_results();
		}

		$this->db->order_by('msg.id', 'DESC');

		if ($limit > 0) {
			$this->db->limit($limit, $page);
		}
		$query_result = $this->db->get();
		return $query_result->result();
	}

	public function countDraftBox() {
		$this->db->select('msg.id');

		$where = array(
			'msg.from_userid' => $this->session->userdata('userid'),
			'msg.draft' => '1',
			'msg.sender_delete' => '0'
		);

		if ($this->session->userdata('type') == 'patient') {
			$this->db->from('messages as msg, doctor as usr');
			$this->db->where($where);
			$this->db->where('usr.userid = msg.to_userid');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->db->from('messages as msg, patient as usr');
			$this->db->where($where);
			$this->db->where('usr.user_id = msg.to_userid');
		}
		return $this->db->count_all_results();
	}

	public function getTrashBox($limit = null, $page = null, $id = null) {
		if ($id == null) {
			$id = $this->session->userdata('userid');
		}
		$id = $this->db->escape(intval($id));

		$this->db->select('msg.id, msg.subject, msg.to_userid, msg.from_userid, msg.message, msg.opened, msg.added');

		//usr.firstname, usr.lastname
		$this->db->from('messages as msg');
		$this->db->where("(msg.from_userid = $id AND msg.sender_delete = 1)");
		$this->db->or_where("(msg.to_userid = $id and msg.recipient_delete = 1)");

		$this->db->order_by('msg.updated', 'DESC');

		if ($limit > 0) {
			$this->db->limit($limit, $page);
		}

		$query_result = $this->db->get();

		$result = $query_result->result();

		$return = array();
		$users = array();

		foreach( $result as $message ) {
			//get sender firstname, lastname and image_url
			if (!array_key_exists($message->from_userid, $users)) {
				//get user id
				$user_id = $this->db->escape(intval($message->from_userid));

				//first check doctor table
				$this->db->select('usr.firstname, usr.lastname, usr.image_url, usr.city, st.code, u.type')
						->from('doctor as usr')
						->where('userid', $user_id)
						->join('state st', 'st.id = usr.state_id', 'left')
						->join('user u', 'u.id = usr.userid')
						->limit(1);

				$user_query = $this->db->get();
				$user_row = $user_query->row();

				if (is_object($user_row)) {
					$users[$message->from_userid] = $user_row;
				} else {
					//then check patient table
					$this->db->select('usr.firstname, usr.lastname, usr.image_url, usr.city, st.code, u.type')
							->from('patient as usr')
							->where('user_id', $user_id)
							->join('state st', 'st.id = usr.state_id', 'left')
                            ->join('user u', 'u.id = usr.user_id')
							->limit(1);
					$user_query = $this->db->get();
					$user_row = $user_query->row();
					$users[$message->from_userid] = $user_row;
				}
			}

			$message->firstname = $users[$message->from_userid]->firstname;
			$message->lastname = $users[$message->from_userid]->lastname;
			$message->image_url = $users[$message->from_userid]->image_url;
			$message->city = $users[$message->from_userid]->city;
			$message->code = $users[$message->from_userid]->code;
			$message->type = $users[$message->from_userid]->type;

			$return[] = $message;

		}

		return $return;
	}

	public function searchTrashMessages($limit = null, $page = null, $search_text = '', $count_only = false) {
		$id = $this->db->escape(intval($this->session->userdata('userid')));
		if ($count_only == false) {
			$this->db->select('msg.id, msg.subject, msg.to_userid, msg.from_userid, msg.message, msg.opened, msg.added');
		}

		$this->db->from('messages msg');

		if ($search_text) {
			$search_text = $this->db->escape_str($search_text);
			$this->db->where("(msg.subject LIKE '%" . $search_text . "%' ESCAPE '!' OR  msg.message LIKE '%" . $search_text . "%' ESCAPE '!' )");
		}
		$this->db->where("((msg.from_userid = $id AND msg.sender_delete = 1) OR (msg.to_userid = $id and msg.recipient_delete = 1))");
		if ($count_only) {
			return $this->db->count_all_results();
		}

		$this->db->order_by('msg.updated', 'DESC');
		if ($limit > 0) {
			$this->db->limit($limit, $page);
		}

		$query_result = $this->db->get();

		$result = $query_result->result();

		$return = array();
		$users = array();

		foreach( $result as $message ) {
			//get sender firstname, lastname and image_url
			if (!array_key_exists($message->from_userid, $users)) {
				//get user id
				$user_id = $this->db->escape(intval($message->from_userid));

				//first check doctor table
				$this->db->select('usr.firstname, usr.lastname, usr.image_url, usr.city, st.code')
						->from('doctor as usr, state as st')
						->where('userid', $user_id)
						->where('usr.state_id = st.id')
						->limit(1);

				$user_query = $this->db->get();
				$user_row = $user_query->row();

				if (is_object($user_row)) {
					$users[$message->from_userid] = $user_row;
				} else {
					//then check patient table
					$this->db->select('usr.firstname, usr.lastname, usr.image_url, usr.city, st.code')
							->from('patient as usr, state as st')
							->where('user_id', $user_id)
							->where('usr.state_id = st.id')
							->limit(1);
					$user_query = $this->db->get();
					$user_row = $user_query->row();
					$users[$message->from_userid] = $user_row;
				}
			}

			$message->firstname = $users[$message->from_userid]->firstname;
			$message->lastname = $users[$message->from_userid]->lastname;
			$message->image_url = $users[$message->from_userid]->image_url;
			$message->city = $users[$message->from_userid]->city;
			$message->code = $users[$message->from_userid]->code;

			$return[] = $message;

		}

		return $return;
	}

	public function countTrashBox() {
		$id = $this->session->userdata('userid');
		$id = $this->db->escape(intval($id));

		$this->db->select('msg.id');

		//usr.firstname, usr.lastname
		$this->db->from('messages as msg');
		$this->db->where("(msg.from_userid = $id AND msg.sender_delete = 1)");
		$this->db->or_where("(msg.to_userid = $id and msg.recipient_delete = 1)");

		return $this->db->count_all_results();
	}

	public function searchMessages($search_text, $limit = null, $page = null, $id = null) {
		if ($id == null) {
			$id = $this->session->userdata('userid');
		}
		$id = $this->db->escape(intval($id));
		$me = $id;

		$this->db->select('msg.id, msg.subject, msg.message, msg.to_userid, msg.from_userid, msg.added, msg.opened');

		//usr.firstname, usr.lastname
		$this->db->from('messages as msg');
		$this->db->where("(msg.from_userid = $id AND msg.sender_delete = 0)");
		$this->db->or_where("(msg.to_userid = $id and msg.recipient_delete = 0)");
		$this->db->where("(msg.from_userid = $id AND msg.draft = 0)");
//        $this->db->like('msg.subject', $search_text);
//        $this->db->or_like('msg.message', $search_text);
		$this->db->where("(msg.subject LIKE '%" . $search_text . "%' ESCAPE '!' OR  msg.message LIKE '%" . $search_text . "%' ESCAPE '!' )");

		//die( $this->db->get_compiled_select() );

		if ($limit > 0) {
			$this->db->limit($limit, $page);
		}

		//die( $this->db->get_compiled_select() );

		$query_result = $this->db->get();

		$return = array();
		$users = array();

		foreach ($query_result->result() as $message) {
			$message->to_username = "";
			$message->to_firstname = "";
			$message->to_lastname = "";
			$message->to_image_url = "";
			$message->to_city = "";
			$message->to_code = "";

			//first check to username
			if (!array_key_exists($message->to_userid, $users)) {

				//get user id
				$user_id = $this->db->escape(intval($message->to_userid));

				//first check doctor table
				$this->db->select('usr.firstname, usr.lastname, usr.image_url, usr.city, st.code')
						->from('doctor as usr, state as st')
						->where('userid', $user_id)
						->where('usr.state_id = st.id')
						->limit(1);

				$user_query = $this->db->get();
				$user_row = $user_query->row();

				if (is_object($user_row)) {
					$users[$message->to_userid] = $user_row;
				} else {
					//then check patient table
					$this->db->select('usr.firstname, usr.lastname, usr.image_url, usr.city, st.code')
							->from('patient as usr, state as st')
							->where('user_id', $user_id)
							->where('usr.state_id = st.id')
							->limit(1);

					$user_query = $this->db->get();
					$user_row = $user_query->row();
					$users[$message->to_userid] = $user_row;
				}
			}

			if (is_object($users[$message->to_userid]) && $users[$message->to_userid]->firstname) {
				$message->to_username = $users[$message->to_userid]->firstname . ' ' . $users[$message->to_userid]->lastname;
				$message->to_firstname = $users[$message->to_userid]->firstname;
				$message->to_lastname = $users[$message->to_userid]->lastname;
				$message->to_image_url = $users[$message->to_userid]->image_url;
				$message->to_city = $users[$message->to_userid]->city;
				$message->to_code = $users[$message->to_userid]->code;
			}

			//now check from username
			if (!array_key_exists($message->from_userid, $users)) {
				//get user id
				$user_id = $this->db->escape(intval($message->from_userid));

				//first check doctor table
				$this->db->select('usr.firstname, usr.lastname, usr.image_url, usr.city, st.code')
						->from('doctor as usr, state as st')
						->where('userid', $user_id)
						->where('usr.state_id = st.id')
						->limit(1);

				$user_query = $this->db->get();
				$user_row = $user_query->row();

				if (is_object($user_row)) {
					$users[$message->from_userid] = $user_row;
				} else {
					//then check patient table
					$this->db->select('usr.firstname, usr.lastname, usr.image_url, usr.city, st.code')
							->from('patient as usr, state as st')
							->where('user_id', $user_id)
							->where('usr.state_id = st.id')
							->limit(1);
					$user_query = $this->db->get();
					$user_row = $user_query->row();
					$users[$message->from_userid] = $user_row;
				}
			}

			if ($users[$message->from_userid]->firstname) {
				$message->from_username = $users[$message->from_userid]->firstname . ' ' . $users[$message->from_userid]->lastname;
				$message->from_firstname = $users[$message->from_userid]->firstname;
				$message->from_lastname = $users[$message->from_userid]->lastname;
				$message->from_image_url = $users[$message->from_userid]->image_url;
				$message->from_city = $users[$message->from_userid]->city;
				$message->from_code = $users[$message->from_userid]->code;
			}

			$return[] = $message;
		}

		return $return;
	}

	public function countSearch($search_text) {
		$id = $this->session->userdata('userid');
		$id = $this->db->escape(intval($id));

		$this->db->select('msg.id');

		//usr.firstname, usr.lastname
		$this->db->from('messages as msg');
		$this->db->where("(msg.from_userid = $id AND msg.sender_delete = 0)");
		$this->db->or_where("(msg.to_userid = $id and msg.recipient_delete = 0)");
		$this->db->where("(msg.from_userid = $id AND msg.draft = 0)");
		$this->db->like('msg.subject', $search_text);
		$this->db->or_like('msg.message', $search_text);

		//echo $this->db->get_compiled_select();

		return $this->db->count_all_results();
	}

	public function getRecipients($id = null) {
		if ($id == null) {
			$id = $this->session->userdata('userid');
		}

		if ($this->session->userdata('type') == 'patient') {
			$this->db->select('fav.d_id as id, usr.firstname, usr.lastname');
			$this->db->from('favorite as fav, doctor as usr');
			$this->db->where('usr.userid = fav.d_id');
			$this->db->where('fav.p_id', $id);
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->db->select('fav.p_id as id, usr.firstname, usr.lastname');
			$this->db->from('favorite as fav, patient as usr');
			$this->db->where('usr.user_id = fav.p_id');
			$this->db->where('fav.d_id', $id);
		}
		$this->db->order_by('usr.firstname', 'ASC');

		$query_result = $this->db->get();
		return $query_result->result();
	}

	public function addMessage($data) {
		$this->db->insert('messages', $data);
		return $this->db->insert_id();
	}

	public function getMessage($message_id) {
		$query = $this->db->get_where('messages', array('id' => $message_id), '1', '0');
		return $query->row();
	}

	public function getDoctorName($id) {
		$this->db->select('firstname, lastname');
		$this->db->from('doctor');
		$this->db->where('userid', $id);
		$this->db->limit('1', '0');

		$query = $this->db->get();
		$row = $query->row();

		if (is_object($row)) {
			$ret = $row->firstname . ' ' . $row->lastname;
		} else {
			$ret = '';
		}

		return $ret;
	}

	public function getPatientName($id) {
		$this->db->select('firstname, lastname');
		$this->db->from('patient');
		$this->db->where('user_id', $id);
		$this->db->limit('1', '0');

		$query = $this->db->get();
		$row = $query->row();

		if (is_object($row)) {
			$ret = $row->firstname . ' ' . $row->lastname;
		} else {
			$ret = '';
		}

		return $ret;
	}

	public function setView($message_id) {
		$data = array(
			'opened' => 1
		);
		$this->db->update('messages', $data, array('id' => $message_id));

		return $this->db->affected_rows();
	}

	public function fixMessageForDisplay($message, $me = null, $user_type = null) {

		if (null === $me) {
			$me = $this->session->userdata('userid');
		}

		if (null === $user_type) {
			$user_type = $this->session->userdata('type');
		}

		//now process message
		if ( is_object($message) ) {
			//1. Get sender (from_userid) image, firstname, lastname

			$this->db->select('firstname, lastname, image_url');

			if ( $me == $message->from_userid ) {
				//I'm the sender
				if ($user_type == 'patient') {
					//look into patient table
					$this->db->from('patient');
					$this->db->where('user_id', $message->from_userid);
				} elseif ($user_type == 'doctor') {
					//look into doctor table
					$this->db->from('doctor');
					$this->db->where('userid', $message->from_userid);
				}
			} else {
				//I'm the receiver, so if I'm patient look into doctor table, otherwise doctor table

				if ($user_type == 'patient') {
					//doctor sent me this message
					$this->db->from('doctor');
					$this->db->where('userid', $message->from_userid);
				} elseif ($user_type == 'doctor') {
					//patient sent me this message
					$this->db->from('patient');
					$this->db->where('user_id', $message->from_userid);
				}
			}

			$this->db->limit('1', '0');
			$query = $this->db->get();
			$row = $query->row();

			if (is_object($row)) {
				$message->sender_name = $row->firstname . ' ' . $row->lastname;
				$message->sender_image_url = $row->image_url;
			} else {
				$message->sender_name = '';
				$message->sender_image_url = '';
			}

			unset($row);

			//2. get receiver (to_userid) firstname, lastname, if receiver is me then name will be me
			if ($message->to_userid == $me)
			{
				//message receiver is me, so no need to get my details
				$message->receiver_name = 'Me';
			}
			else
			{
				//I'm the sender, so if I'm doctor get details for patient, if I'm patient get details for doctor
				$this->db->select('firstname, lastname, image_url');

				if ($user_type == 'patient') {
					$this->db->from('doctor');
					$this->db->where('userid', $message->to_userid);
				} elseif ($user_type == 'doctor') {
					$this->db->from('patient');
					$this->db->where('user_id', $message->to_userid);
				}

				$this->db->limit('1', '0');
				$query = $this->db->get();
				$row = $query->row();

				if (is_object($row)) {
					$message->receiver_name = $row->firstname . ' ' . $row->lastname;
					//$message->image_url = $row->image_url;
				} else {
					$message->receiver_name = '';
					//$message->image_url = '';
				}

			}

			return $message;
		}

		return false;
	}

	public function getLatestFiveReplysForMessage($message_id, &$replies, $count = 0) {
		if ($count == 5) {
			return;
		}

		$message = $this->getMessage($message_id);

		$me = $this->session->userdata('userid');

		//check message is deleted or not
		if ($message->from_userid == $me) {
			//me is the sender
			if ($message->sender_delete == 1 || $message->sender_delete == 2) {
				return;
			}
		} else {
			if ($message->recipient_delete == 1 || $message->recipient_delete == 2) {
				return;
			}
		}


		$message = $this->fixMessageForDisplay($message);

		$replies[] = $message;

		if ($message->parent) {
			$this->getLatestFiveReplysForMessage($message->parent, $replies, ++$count);
		}
	}

	public function updateMessage($data, $where) {
		$this->db->update('messages', $data, $where);

		return $this->db->affected_rows();
	}

	public function getCounters() {
		$data['index_count'] = $this->countInbox();
		$data['sent_count'] = $this->countSentBox();
		$data['drafts_count'] = $this->countDraftBox();
		$data['trash_count'] = $this->countTrashBox();

		return $data;
	}

}
