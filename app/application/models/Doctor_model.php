<?php

/**
 * Class Doctor_Model
 *
 * @property CI_DB_query_builder $db
 */
class Doctor_Model extends CI_Model
{

    public function get_all_doctors($limit = null, $page = null, $company_id = null){
        $this->db->select('doctor.*, user.time as created_at, user.is_active as active, user.user_email, user.user_password, user.login_date_time as login_date_time, user.is_joined as is_joined');
//        $this->db->join('doctor_specialization', 'doctor_specialization.userid = doctor.userid');
        $this->db->join('user', 'user.id = doctor.userid');
//        $this->db->group_by('doctor.id');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        if($company_id !== null){
            $this->db->where('doctor.company_id', $company_id);
        }
        return $this->db->get('doctor')->result();
    }

    public function get_all_doctors_count($company_id = null){
        if($company_id !== null){
            return count($this->get_all_doctors(null, null, $company_id));
        }
        return count($this->get_all_doctors());
    }

    public function update_doctor_image($image_url, $user_id)
    {
        $this->db->set('image_url', $image_url);
        $this->db->where('id', $user_id);
        $this->db->update('doctor');
    }


    public function getListByStateOrSpecialityOrName($name, $speciality, $stateId)
    {
        if ($speciality == '' && $name == '' && $stateId == '') {
            return [];
        }

        //only name
        if ($name != '' && $stateId == '' && $speciality == '') {
            $this->db->or_like('doctor.firstname', $name);
            $this->db->or_like('doctor.lastname', $name);
        }
        //only state
        elseif ($name == '' && $stateId != '' && $speciality == '') {
            $this->db->or_where('doctor.state_id', $stateId);
        } elseif ($name == '' && $stateId == '' && $speciality != '') {
            // only speciality
            $this->db->or_where('doctor_specialization.speciality', $speciality);
            $this->db->join('doctor_specialization', 'doctor_specialization.userid = doctor.userid');
        } else {
            $this->db->or_like('doctor.firstname', $name);
            $this->db->or_like('doctor.lastname', $name);
            $this->db->or_where('doctor.state_id', $stateId);
            $this->db->or_where('doctor_specialization.speciality', $speciality);
            $this->db->join('doctor_specialization', 'doctor_specialization.userid = doctor.userid');
        }

        $this->db->select('doctor.id, doctor.firstname, doctor.lastname, doctor.image_url, doctor.userid, doctor.location');

        // Run the query
        return $this->db->get('doctor')->result();
    }


    public function getListByStateOrName($name, $stateId)
    {
        if ($name == '' && $stateId == '') {
            return null;
        }

        //only name
        if ($name != '' && $stateId == '') {
            $this->db->or_like('firstname', $name);
            $this->db->or_like('lastname', $name);
        }
        //only state
        elseif ($name == '' && $stateId != '') {
            $this->db->or_where('state_id', $stateId);
        } else {
            $this->db->or_like('firstname', $name);
            $this->db->or_like('lastname', $name);
            $this->db->or_where('state_id', $stateId);
        }

        $this->db->select('id, firstname, lastname, image_url, userid, location');

        // Run the query
        return $this->db->get('doctor')->result();
    }

    public function get_list_by_name_email_phone($value = null, $company_id = null){
        $this->db->select('doctor.*, user.time as created_at, user.is_active as active, user.user_email, user.user_password, user.login_date_time as login_date_time, user.is_joined as is_joined');
        $this->db->join('user', 'user.id = doctor.userid');
        if ($value != '') {
            $this->db->or_like('CONCAT( `doctor`.`firstname`,  \' \', `doctor`.`lastname` )', $value);
            $this->db->or_like('user.user_email', $value);
            $this->db->or_like('doctor.phone_mobile', $value);
        }
        if ($company_id != '') {
            $this->db->where('doctor.company_id', $company_id);
        }
        return $this->db->get('doctor')->result();
    }


    /**
     * @param string $email
     * @param string $password
     * @return int User Id
     */
    protected function createUser($email, $password, $parent_id = null)
    {
        $data = [
            'user_email' => $email,
            'user_name' => $email,
            'user_password' => $password,
            'type' => "doctor"
        ];
        if($parent_id != null){
            $data['parent_id'] = $parent_id;
        }
        $this->db->insert('user', $data);

        return $this->db->insert_id();
    }

    /**
     * @param $firstName
     * @param $lastName
     * @param $email
     * @param $practise
     * @param $npi
     * @param $stateId
     * @param $password
     * @return bool
     */
    public function createDoctor($firstName, $lastName, $email, $password, $company_id_ref, $state_id = null, $phone = null, $city = null, $zip = null, $parent_id = null, $specialty = null)
    {
        $this->db->trans_start();

        $userId = $this->createUser($email, $password, $parent_id);

//        $this->load->model('consultationtype_model');
//        $this->consultationtype_model->add([
//            'doctor_id' => $userId,
//            'duration_id' => 2,
//            'rate' => '10'
//        ]);

        $data = [];
        $data = [
            'firstname' => $firstName,
            'lastname' => $lastName,
            'email' => $email,
            'userid' => $userId,
            'company_id' => $company_id_ref
        ];
        if($state_id != null){
            $data['state_id'] = $state_id;
        }
        if($phone != null){
            $data['phone_mobile'] = $phone;
        }
        if($phone != null){
            $data['phone_mobile'] = $phone;
        }
        if($city != null){
            $data['city'] = $city;
        }
        if($zip != null){
            $data['zip'] = $zip;
        }
        if($specialty != null){
            $CI =& get_instance();
            $CI->load->model('login_model');
            $CI->login_model->insert_doctor_specialty($specialty, $userId);
        }
        $this->db->insert('doctor', $data);

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function add_doctor_other_states($userId, $o_li){
        $this->db->insert('doctor_other_licenses', [
            'userid' => $userId,
            'state_id' => $o_li,
        ]);
    }

    public function add_doctor_languages($userId, $l_id){
        $this->db->insert('doctor_language', [
            'userid' => $userId,
            'language_id' => $l_id
        ]);
    }

    public function remove_doctor_other_states($userID){
        $this->db->where('userid', $userID);
        $this->db->delete('doctor_other_licenses');
    }

    public function remove_doctor_languages($userID){
        $this->db->where('userid', $userID);
        $this->db->delete('doctor_language');
    }

    /**
     * @return array
     */
    public function getSpecialtyList()
    {
        $this->db->order_by('name', 'ASC');
        $this->db->from('specialty');
        $rows = $this->db->get()->result();

        $result = [];
        foreach ($rows as $row) {
            $result[$row->id] = $row->name;
        }

        return $result;
    }

    /**
     * Create new language
     *
     * @param string $name
     * @return int
     */
    public function addLanguage($name)
    {
        $this->db->insert('language', [
            'name' => $name,
        ]);

        return $this->db->insert_id();
    }

    /**
     * @return array
     */
    public function getLanguageList()
    {
        $this->db->order_by('name', 'ASC');
        $this->db->from('language');
        $rows = $this->db->get()->result();

        $result = [];
        foreach ($rows as $row) {
            $result[$row->id] = $row->name;
        }

        return $result;
    }

    //doctor settings related functions
    public function get_doctor_payment_rate($doctor_id)
    {
        return $this->db->select('payment_rate, duration, consultation_type_is_active as is_active')
            ->from('doctor')
            ->where('userid', $doctor_id)
            ->limit('1')
            ->get()
            ->row();
    }

    public function get_doctor_stripe($doctor_id)
    {
        return $this->db->select('stripe_one, stripe_two')
            ->from('doctor')
            ->where('userid', $doctor_id)
            ->limit('1')
            ->get()
            ->row();
    }

    public function updateDoctor($data, $where) {
        if ( is_array($data) && !empty($data) && is_array($where) && !empty($where) ) {
            $this->db->update('doctor', $data, $where);

            return $this->db->affected_rows();
        }

        return NULL;
    }

    //get doctor information

    public function get_doctor($id = null)
    {
        if ( null === $id )
            $id = $this->session->userdata("userid");

        return $this->db->join('user', 'doctor.userid =user.id')
            ->where('userid', $id)
            ->get('doctor')
            ->row();
    }

    public function get_doctor_by_patient_id($id = null)
    {
        if ( null === $id )
            $id = $this->session->userdata("userid");

//        return $this->db->join('user', 'doctor.userid =user.id')
//            ->where('doctor.userid', $id)
//            ->get('doctor')
//            ->row();

        return $this->db->join('user', 'doctor.userid = user.id')
            ->where('user.parent_id', $id)
            ->where('user.type', 'doctor')
            ->select('doctor.*, user.user_email as user_email, user.is_joined as is_joined')
            ->get('doctor')
            ->row();
    }

    public function get_doctors_specialization($id)
    {
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor_specialization');
        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function get_doctors_degree($id)
    {
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor_degree');
        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

//    public function get_doctors_patients($id) {
//        $this->db->where('d_id', $id);
//        $query = $this->db->get('favorite');
//        $result = array();
//        if ($query->num_rows() > 0) {
//            foreach ($query->result() as $row) {
//                $result[] = $row;
//            }
//            return $result;
//        } else
//            return "No result found";
//    }

    /*
     * get doctor favorite patient
     */
    public function get_doctors_patients($id)
    {
        //$id = $this->session->userdata('userid');
        $this->db->select('fav.p_id as id, usr.*');
        $this->db->from('favorite as fav, patient as usr');
        $this->db->where('usr.user_id = fav.p_id');
        $this->db->where('fav.d_id', $id);

		$this->db->order_by('usr.firstname', 'ASC');

		$query_result = $this->db->get();
		return ['general_info' => $query_result->result()];
    }

    public function search_doctors_patients($id, $patient_name)
    {
        $this->db->select('fav.p_id as id, usr.*');
        $this->db->from('favorite as fav, patient as usr');
        $this->db->where('usr.user_id = fav.p_id');
        $this->db->where('fav.d_id', $id);
        $this->db->group_start();
        $this->db->or_like('usr.firstname', $patient_name);
        $this->db->or_like('usr.lastname', $patient_name);
        $this->db->group_end();

		$this->db->order_by('usr.firstname', 'ASC');

		$query_result = $this->db->get();
		return ['general_info' => $query_result->result()];
    }

    public function get_doctor_patients($id = null, $limit = 4)
    {
        if ( null === $id )
            $id = $this->session->userdata('userid');

        $this->db->select('usr.*, st.code');
        $this->db->from('favorite as fav, patient as usr');
        $this->db->join('state st', 'st.id = usr.state_id', 'left');
        $this->db->where('usr.user_id = fav.p_id');
        $this->db->where('fav.d_id', $id);

        $this->db->order_by('usr.firstname', 'ASC');
        if ( is_numeric( $limit ) ) {
            $this->db->limit($limit);
        }

        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function search_doctor_patients( $text="", $id = null, $limit = 4)
    {
        if ( null === $id )
            $id = $this->session->userdata('userid');

        $this->db->select('usr.*, st.code');
        $this->db->from('favorite as fav, patient as usr');
        $this->db->join('state st', 'st.id = usr.state_id', 'left');
        $this->db->where('usr.user_id = fav.p_id');
        $this->db->where('fav.d_id', $id);
        $this->db->group_start();
        $this->db->like('usr.firstname', $text);
        $this->db->or_like('usr.lastname', $text);
        $this->db->group_end();

        $this->db->order_by('usr.firstname', 'ASC');
        if ( is_numeric( $limit ) ) {
            $this->db->limit($limit);
        }

        $query_result = $this->db->get();
        return $query_result->result();
    }


    public function get_doctors_experience($id)
    {
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor_experience');

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function get_doctors_available($id, $month, $date)
    {
        //$this->db->group_by('month');
        $this->db->where('userid', $id);
        $this->db->where('month', $month);
        $this->db->where('start_date', $date);
        $this->db->where('status', "yes");
        $query = $this->db->get('available_time');

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function get_doctors_professiona_membership($id)
    {
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor_professional_membership');

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function get_doctors_licenced_states($id)
    {
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor_licenced_states');

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function get_doctors_awards($id)
    {
        $this->db->select('*');
        $this->db->from('doctor_award');
        $this->db->where('userid', $id);
        $query_result = $this->db->get();
        if ($query_result->num_rows() > 0) {
            return $query_result->result();
        } else {
            return null;
        }
    }

    public function get_doctors_language($id)
    {
        $this->db->select('language.name');
        $this->db->where('doctor_language.userid', $id);
        $this->db->join('language', 'language.id = doctor_language.language_id');
        $query = $this->db->get('doctor_language');
        $result = array();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else
            return [];
    }

    public function get_doctors_education($id)
    {
        $this->db->where('userid', $id);
        $query = $this->db->get('doctor_education');
        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return "No result found";
    }

    public function getPastAppointmentForDoctors($id)
    {
        $now = time();
        $this->db->select('appointment.*, appointment.id AS appointment_id, reason.name AS reason_name, patient.firstname, patient.lastname, appointment.status as app_status');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('doctors_id', $id);
        $this->db->group_start();
        $this->db->where('appointment.start_date_stamp <', $now );
        $this->db->or_where("appointment.status", 6);
        $this->db->group_end();
        $this->db->where("appointment.status !=", 5);
        $this->db->order_by('appointment.start_date_stamp', 'DESC');
        $query = $this->db->get();

        return $query->result();
    }

    public function getFutureAppointmentForDoctors($id)
    {
        $now = time();
        $this->db->select('appointment.*, appointment.id AS appointment_id, reason.name AS reason_name, patient.firstname, patient.lastname, appointment.status as app_status');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('doctors_id', $id);
        $this->db->where("appointment.start_date_stamp >=", $now);
        $this->db->group_start();
        $this->db->where("appointment.status", 2);
        $this->db->or_where("appointment.status", 3);
        $this->db->group_end();
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        $query = $this->db->get();

        return $query->result();
    }
	
	public function getUpcomingAppointmentForDoctor($id, $coming_time, $current_time)
    {
		$this->db->select('appointment.*, appointment.id AS appointment_id, reason.name AS reason_name, patient.firstname, patient.lastname, appointment.status as app_status');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('doctors_id', $id);
        $this->db->where("FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') <=", $coming_time);
        $this->db->where("FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') >=", $current_time);
//		$this->db->where("FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i') = DATE_FORMAT(NOW() + INTERVAL 5 MINUTE, '%Y-%m-%d %H:%i')"); // it also works
		
//		$this->db->where("appointment.id NOT IN (SELECT al.appointment_id FROM appointment_log al WHERE al.appointment_id = appointment.id AND (al.status = 'delayed') )", null, false); // this is correct, but may be i will remove it
	
        $this->db->group_start();
        $this->db->where("(appointment.status = 2 OR appointment.status = 3)");
        $this->db->group_end();
        $this->db->order_by('appointment.start_date_stamp', 'DESC');
		$this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

	public function getCurrentAppointmentForDoctor($id, $current_time)
    {
		$this->db->select('appointment.*, appointment.id AS appointment_id, reason.name AS reason_name, patient.firstname, patient.lastname, appointment.status as app_status');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('doctors_id', $id);
        $this->db->where("FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') <=", $current_time);
        $this->db->where("FROM_UNIXTIME(appointment.end_date_stamp, '%Y-%m-%d %H:%i:%s') >=", $current_time);
//		$this->db->where("FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i') = DATE_FORMAT(NOW() + INTERVAL 5 MINUTE, '%Y-%m-%d %H:%i')"); // it also works

//		$this->db->where("appointment.id NOT IN (SELECT al.appointment_id FROM appointment_log al WHERE al.appointment_id = appointment.id AND (al.status = 'delayed') )", null, false); // this is correct, but may be i will remove it

        $this->db->group_start();
        $this->db->where("(appointment.status = 2 OR appointment.status = 3)");
        $this->db->group_end();
        $this->db->order_by('appointment.start_date_stamp', 'DESC');
		$this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

	public function getUpcomingAppointment_delayedForDoctor($id, $coming_time, $current_time)
    {
		$this->db->select('appointment.*, appointment.id AS appointment_id, reason.name AS reason_name, patient.firstname, patient.lastname, appointment.status as app_status');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('doctors_id', $id);
        $this->db->where("FROM_UNIXTIME(appointment.delay_time, '%Y-%m-%d %H:%i') <=", $coming_time);
        $this->db->where("FROM_UNIXTIME(appointment.delay_time, '%Y-%m-%d %H:%i') >=", $current_time);
//		$this->db->where("FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i') = DATE_FORMAT(NOW() + INTERVAL 5 MINUTE, '%Y-%m-%d %H:%i')"); // it also works

//		$this->db->where("appointment.id NOT IN (SELECT al.appointment_id FROM appointment_log al WHERE al.appointment_id = appointment.id AND (al.status = 'delayed') )", null, false); // this is correct, but may be i will remove it

        $this->db->group_start();
        $this->db->where("(appointment.status = 2 OR appointment.status = 3)");
        $this->db->group_end();
        $this->db->order_by('appointment.start_date_stamp', 'DESC');
		$this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

    public function getPendingAppointmentForDoctors($id)
    {
        $now = time();
        $this->db->select('appointment.*, appointment.id AS appointment_id, reason.name AS reason_name, patient.firstname, patient.lastname, appointment.status as app_status');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('doctors_id', $id);
        $this->db->where("appointment.start_date_stamp >=", $now);
        $this->db->where("appointment.status", 1);
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        $query = $this->db->get();

        return $query->result();
    }

    public function getCanceledAppointmentForDoctors($id)
    {
        $this->db->select('appointment.*, appointment.id AS appointment_id, reason.name AS reason_name, patient.firstname, patient.lastname, appointment.status as app_status');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('doctors_id', $id);
        $this->db->where("appointment.status", 5);
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function getDoctorAvailableDate( $id = null ) {
        if ( null === $id )
            $id = $this->session->userdata("userid");

        $start_date = date('Y-m-d', time());
        $start_date_stamp = strtotime($start_date . '00:00:00');

        $results =  $this->db
            ->distinct()
            ->select('start_date_stamp')
            ->from('available_time')
            ->where('userid', $id)
            ->where('start_date_stamp >=', $start_date_stamp)
            ->get()
            ->result();

        $ret = array();

        if ( is_array($results) && !empty($results) ) {
            foreach ( $results as $result ) {
                $ret[] = date('Y-m-d', $result->start_date_stamp);
            }
        }

        return $ret;
    }

    public function remove_doctor($userid){
        $this->db->delete('doctor', array('userid' => $userid));
        $this->db->delete('user', array('id' => $userid));
    }

    public function doctor_check_waive( $patient_id, $doctor_id = null ) {
        if ( null === $doctor_id )
            $doctor_id = $this->session->userdata('userid');

        return $this->db->select('id')
            ->from('waive_fee')
            ->where('doctor_id', $doctor_id)
            ->where('patient_id', $patient_id)
            ->count_all_results();
    }


}