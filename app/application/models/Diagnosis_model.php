<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Diagnosis_model extends CI_Model
{

    public function add($data){
        $this->db->insert('diagnosis', $data);
        return $this->db->insert_id();
    }

    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update('diagnosis', $data);
    }

    public function getAll($limit = null, $page = null){
        $this->db->order_by('id', 'ASC');
        $this->db->from('diagnosis');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        return $this->db->get()->result();
    }

    public function getAll_count(){
        return count($this->getAll());
    }

    public function remove($id){
        $this->db->delete('diagnosis', [
            'id' => $id
        ]);
    }

    public function check_exists($name, $code)
    {
        $this->db->where('name', $name);
        $this->db->where('code', $code);
        $query = $this->db->get('diagnosis');
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

}