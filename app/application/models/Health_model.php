<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Health_Model extends CI_Model
{

    public function get($data = [])
    {
        $this->db->select(implode(', ', array_keys(array_filter($data, function ($value) {
            return ($value == '');
        }))));
        foreach ($data as $index => $value) {
            if ($value != '') {
                $this->db->where($index, $value);
            }
        }
        return $this->db->get('health')->result();
    }

    public function update_by_field($fields = [], $data = []){
        foreach($fields as $index => $value){
            $this->db->where($index, $value);
        }
        $this->db->update('health', $data);
    }

    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update('health', $data);
    }
}