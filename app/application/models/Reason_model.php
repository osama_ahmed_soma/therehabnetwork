<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Reason_Model extends CI_Model
{

    public function add($reason){
        $this->db->insert('reason', [
            'name' => $reason
        ]);
        return $this->db->insert_id();
    }

    public function update($id, $name){
        $this->db->where('id', $id);
        $this->db->update('reason', [
            'name' => $name
        ]);
    }

    public function getList($limit = null, $page = null)
    {
        $this->db->order_by('id', 'ASC');
        $this->db->from('reason');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        return $this->db->get()->result();
    }

    public function getList_count(){
        return count($this->getList());
    }

    public function getById($id)
    {
        $this->db->where('id', $id)->limit(1);

        $query = $this->db->get('reason');

        return $query->row();
    }

    public function check_exists($reason)
    {
        $this->db->where('name', $reason);
        $query = $this->db->get('reason');
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function remove($id){
        $this->db->delete('reason', [
            'id' => $id
        ]);
    }
}