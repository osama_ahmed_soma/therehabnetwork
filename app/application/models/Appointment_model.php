<?php

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\Role;

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Appointment_Model extends CI_Model
{
    public function create($doctorId, $patientId, $availId, $reasonId, $notes)
    {

    }

    public function getAll($limit = null, $page = null){
        $this->db->select('appointment.*, reason.name AS reason_name, doctor.firstname as doctor_firstname, doctor.lastname as doctor_lastname, patient.firstname as patient_firstname, patient.lastname as patient_lastname');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function getAll_count(){
        return count($this->getAll());
    }

    public function getById($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('appointment');

        $rows = $query->result();

        return empty($rows) ? null : reset($rows);
    }

    public function getDetailedById($id)
    {
        $this->db->select([
            'appointment.id AS appointment_id',
            'patients_id',
            'doctors_id',
            'reason_id',
            'reason',
            'subjective',
            'objective',
            'assessment',
            'plan',
            'notes',
            'duration',

            'patient.firstname',
            'patient.lastname',
            'patient.email',
            'patient.address',
            'patient.phone_home',
            'patient.phone_mobile',

            'available_time.start_date',
        ]);
        $this->db->join('available_time', 'available_time.id = appointment.available_id');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->where('appointment.id', $id);
        $query = $this->db->get('appointment');

        $rows = $query->result();

        return empty($rows) ? null : reset($rows);
    }

    public function get_appointment_by_id_for_consultation_notification($a_id, $type = 'doctor'){
        $this->db->select([
            'appointment.*',
            $type.'.firstname AS firstname',
            $type.'.lastname AS lastname',
            ($type == 'patient') ? 'reason.name AS primary_symptom' : 'reason.name AS reason_name'

        ]);
        $this->db->from('appointment');
        if($type == 'doctor'){
            $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        }
        if($type == 'patient'){
            $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        }
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where('appointment.id', $a_id);
        $query = $this->db->get();

        $rows = $query->result();

        return empty($rows) ? null : reset($rows);
    }

    public function updateById($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('appointment', $data);
    }

    public function getTimeArray()
    {
        $start = "00:00";
        $end = "23:45";
        $tStart = strtotime($start);
        $tEnd = strtotime($end);
        $tNow = $tStart;

        $result = [];

        while ($tNow <= $tEnd) {
            $result[date("H:i", $tNow)] = date("g:i a", $tNow);

            $tNow = strtotime('+15 minutes', $tNow);
        };

        return $result;
    }

    public function generateTokboxSession($apiKey, $apiSecret)
    {
        $tokbox = new OpenTok($apiKey, $apiSecret);
        $session = $tokbox->createSession(array('mediaMode' => MediaMode::ROUTED));

        return $session->getSessionId();
    }

    public function generateTokboxToken($apiKey, $apiSecret, $sessionId)
    {
        $tokbox = new OpenTok($apiKey, $apiSecret);

        return $tokbox->generateToken($sessionId, ['role' => Role::PUBLISHER]);
    }

    public function saveSession($id, $session)
    {
        $data = ['tokbox_session' => $session];

        return $this->updateById($id, $data);
    }

    public function endSession($id)
    {
        $appointment = $this->getById($id);
        //calc session duration
        $duration = DateTime::createFromFormat('Y-m-d H:i:s', $appointment->session_start)
            ->diff(new DateTime('now'))
            ->s;

        $this->db->set('duration', 'duration + ' . $duration, false);

        $data = [
            'session_end' => date('Y-m-d H:i:s'),
        ];

        $this->db->where('id', $id);
        $this->db->update('appointment', $data);
    }

    public function endCall($id)
    {
        $appointment = $this->getById($id);
        if ( $appointment->doctors_id == $this->session->userdata('userid') ) {
            $data = [
                'status' => 6,
            ];

            $this->db->where('id', $id);
            $this->db->update('appointment', $data);
        }

    }

    /* below functions is for add new patient appointment */
    public function get_doctors_available($id, $month, $date)
    {
        //$this->db->group_by('month');
        $this->db->where('userid', $id);
        $this->db->where('month', $month);
        $this->db->group_start();
        $this->db->where('start_date_stamp >=', strtotime($date . '00:00:00'));
        $this->db->where('end_date_stamp <=', strtotime($date . '23:59:59'));
        $this->db->group_end();
        $this->db->where('status', "yes");
        $query = $this->db->get('available_time');
        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $result[] = $row;
            }
            return $result;
        } else
            return "No result found";
    }

    public function check_appointment_available($doctorId, $startDate, $endDate, $field = 'doctors_id', $condition = 'AND')
    {
        $this->db->select('id');
        $this->db->from('appointment');
        $this->db->where($field, $doctorId);
        if ($condition == 'AND') {
            $this->db->where('start_date_stamp >=', $startDate);
            $this->db->where('end_date_stamp <=', $endDate);
        } else {
            $this->db->where("FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') <=", $startDate);
            $this->db->where("FROM_UNIXTIME(appointment.end_date_stamp, '%Y-%m-%d %H:%i:%s') >=", $endDate);
        }
        return $this->db->count_all_results();
    }

    public function check_appointment_available_for_doctor( $doctorUserrId, $available_time_id ) {
        return $this->db->select('id')
            ->from('available_time')
            ->where('userid', intval($doctorUserrId))
            ->where('id', intval($available_time_id))
            ->count_all_results();
    }

    public function add_appointment($data) {
        $this->db->insert('appointment', $data);
        return $this->db->insert_id();
    }

    public function update_appointment($data, $where) {
        $this->db->update('appointment', $data, $where);

        return $this->db->affected_rows();
    }

    public function get_appointment_by_id( $id ) {
        if ( is_numeric($id) && $id > 0 ) {
            return $this->db->select('*')
                ->from('appointment')
                ->where('id', intval($id))
                ->get()
                ->row();
        }
        return null;
    }

    public function appointment_today($limit = null, $page = null){
        $this->db->select('appointment.*, reason.name AS reason_name, doctor.firstname as doctor_firstname, doctor.lastname as doctor_lastname, patient.firstname as patient_firstname, patient.lastname as patient_lastname');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where("appointment.start_date_stamp >=", strtotime(date('Y-m-d 00:00:00', time())));
        $this->db->where("appointment.end_date_stamp <=", strtotime(date('Y-m-d 23:59:59', time())));
        $this->db->where("appointment.status !=", 5);
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function appointment_today_count(){
        return count($this->appointment_today());
    }

    public function appointment_pending($limit = null, $page = null){
        $this->db->select('appointment.*, reason.name AS reason_name, doctor.firstname as doctor_firstname, doctor.lastname as doctor_lastname, patient.firstname as patient_firstname, patient.lastname as patient_lastname');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where("appointment.status", 1);
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function appointment_search($date_time_array, $provider_search, $patient_search){
        $this->db->select('appointment.*, reason.name AS reason_name, doctor.firstname as doctor_firstname, doctor.lastname as doctor_lastname, patient.firstname as patient_firstname, patient.lastname as patient_lastname');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        if (!empty($date_time_array) && $date_time_array['date_search'] != '') {
            $this->db->where('appointment.start_date_stamp >=', $date_time_array['date_search_start']);
            $this->db->where('appointment.start_date_stamp <', $date_time_array['date_search_end']);
        }
        if($provider_search != '') {
            $this->db->group_start();
            $this->db->or_like('doctor.firstname', $provider_search);
            $this->db->or_like('doctor.lastname', $provider_search);
            $this->db->group_end();
        }
        if ($patient_search != '') {
            $this->db->group_start();
            $this->db->or_like('patient.firstname', $patient_search);
            $this->db->or_like('patient.lastname', $patient_search);
            $this->db->group_end();
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function appointment_today_search($date_time_array, $provider_search, $patient_search){
        $this->db->select('appointment.*, reason.name AS reason_name, doctor.firstname as doctor_firstname, doctor.lastname as doctor_lastname, patient.firstname as patient_firstname, patient.lastname as patient_lastname');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where("appointment.status !=", 5);
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        if (!empty($date_time_array) && $date_time_array['date_search'] != '') {
            $this->db->where('appointment.start_date_stamp >=', $date_time_array['date_search_start']);
            $this->db->where('appointment.start_date_stamp <', $date_time_array['date_search_end']);
        } else {
            $this->db->where("appointment.start_date_stamp >=", strtotime(date('Y-m-d 00:00:00', time())));
            $this->db->where("appointment.end_date_stamp <=", strtotime(date('Y-m-d 23:59:59', time())));
        }
        if($provider_search != '') {
            $this->db->group_start();
            $this->db->or_like('doctor.firstname', $provider_search);
            $this->db->or_like('doctor.lastname', $provider_search);
            $this->db->group_end();
        }
        if ($patient_search != '') {
            $this->db->group_start();
            $this->db->or_like('patient.firstname', $patient_search);
            $this->db->or_like('patient.lastname', $patient_search);
            $this->db->group_end();
        }
//        if ($date_search != '' || $provider_search != '' || $patient_search != ''){
//            $this->db->group_start();
//            /*if ($date_search != '') {
//                $this->db->or_like('appointment.firstname', $value);
//            }*/
//            if($provider_search != '') {
//                $this->db->or_like('doctor.firstname', $provider_search);
//                $this->db->or_like('doctor.lastname', $provider_search);
//            }
//            if ($patient_search != '') {
//                $this->db->or_like('patient.firstname', $patient_search);
//                $this->db->or_like('patient.lastname', $patient_search);
//            }
//            $this->db->group_end();
//        }
        $query = $this->db->get();
        return $query->result();
    }

    public function appointment_pending_search($date_time_array, $provider_search, $patient_search){
        $this->db->select('appointment.*, reason.name AS reason_name, doctor.firstname as doctor_firstname, doctor.lastname as doctor_lastname, patient.firstname as patient_firstname, patient.lastname as patient_lastname');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where("appointment.status", 1);
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        if ($date_time_array['date_search'] != '') {
            $this->db->where('appointment.start_date_stamp >=', $date_time_array['date_search_start']);
            $this->db->where('appointment.start_date_stamp <', $date_time_array['date_search_end']);
        }
        if($provider_search != '') {
            $this->db->group_start();
            $this->db->or_like('doctor.firstname', $provider_search);
            $this->db->or_like('doctor.lastname', $provider_search);
            $this->db->group_end();
        }
        if ($patient_search != '') {
            $this->db->group_start();
            $this->db->or_like('patient.firstname', $patient_search);
            $this->db->or_like('patient.lastname', $patient_search);
            $this->db->group_end();
        }
//        if ($provider_search != '' || $patient_search != ''){
//            $this->db->group_start();
//            if($provider_search != '') {
//                $this->db->like('doctor.firstname', $provider_search);
//                $this->db->like('doctor.lastname', $provider_search);
//            }
//            if ($patient_search != '') {
//                $this->db->like('patient.firstname', $patient_search);
//                $this->db->like('patient.lastname', $patient_search);
//            }
//            $this->db->group_end();
//        }
        $query = $this->db->get();
        return $query->result();
    }

    public function appointment_cancelled_search($date_time_array, $provider_search, $patient_search){
        $this->db->select('appointment.*, reason.name AS reason_name, doctor.firstname as doctor_firstname, doctor.lastname as doctor_lastname, patient.firstname as patient_firstname, patient.lastname as patient_lastname');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where("appointment.status", 5);
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        if ($date_time_array['date_search'] != '') {
            $this->db->where('appointment.start_date_stamp >=', $date_time_array['date_search_start']);
            $this->db->where('appointment.start_date_stamp <', $date_time_array['date_search_end']);
        }
        if($provider_search != '') {
            $this->db->group_start();
            $this->db->or_like('doctor.firstname', $provider_search);
            $this->db->or_like('doctor.lastname', $provider_search);
            $this->db->group_end();
        }
        if ($patient_search != '') {
            $this->db->group_start();
            $this->db->or_like('patient.firstname', $patient_search);
            $this->db->or_like('patient.lastname', $patient_search);
            $this->db->group_end();
        }
//        if ($date_search != '' || $provider_search != '' || $patient_search != ''){
//            $this->db->group_start();
//            /*if ($date_search != '') {
//                $this->db->or_like('appointment.firstname', $value);
//            }*/
//            if($provider_search != '') {
//                $this->db->or_like('doctor.firstname', $provider_search);
//                $this->db->or_like('doctor.lastname', $provider_search);
//            }
//            if ($patient_search != '') {
//                $this->db->or_like('patient.firstname', $patient_search);
//                $this->db->or_like('patient.lastname', $patient_search);
//            }
//            $this->db->group_end();
//        }
        $query = $this->db->get();
        return $query->result();
    }

    public function appointment_pending_count(){
        return count($this->appointment_pending());
        /*$this->db->select('appointment.*, reason.name AS reason_name, doctor.firstname as doctor_firstname, doctor.lastname as doctor_lastname, patient.firstname as patient_firstname, patient.lastname as patient_lastname');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where("appointment.status", 1);
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        $query = $this->db->get();
        return $query->result();*/
    }

    public function appointment_cancelled($limit = null, $page = null){
        $this->db->select('appointment.*, reason.name AS reason_name, doctor.firstname as doctor_firstname, doctor.lastname as doctor_lastname, patient.firstname as patient_firstname, patient.lastname as patient_lastname');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('doctor', 'doctor.userid = appointment.doctors_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where("appointment.status", 5);
        $this->db->order_by('appointment.start_date_stamp', 'ASC');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function appointment_cancelled_count(){
        return count($this->appointment_cancelled());
    }

    public function remove_appointment($appointmentid){
        $this->db->delete('appointment', [
            'id' => $appointmentid
        ]);
    }

    public function get_appointment_log_a_id($a_id, $coming_time, $current_time){
        $this->db->select('*');
        $this->db->from('appointment_log');
        $this->db->where("appointment_log.appointment_id ", $a_id);
//        $this->db->where("appointment_log.created_at <=", $coming_time);
//        $this->db->where("appointment_log.created_at >=", $current_time);
        $this->db->where("appointment_log.status", 'delayed');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

    public function get_appointment_log_delay_by_doctor_patient_id($data = []){
        if(!$data){
            return false;
        }
        $this->db->select('appointment.*,  appointment.id AS appointment_id, reason.name AS reason_name, patient.firstname, patient.lastname, appointment.status as app_status');
        $this->db->from('appointment_log');
        $this->db->join("appointment", 'appointment.id = appointment_log.appointment_id');
        $this->db->join('patient', 'patient.user_id = appointment.patients_id');
        $this->db->join('reason', 'reason.id = appointment.reason_id');
        $this->db->where("appointment.doctors_id", $data['doctor_id']);
        $this->db->where("appointment_log.status", 'delayed');
        $this->db->where("appointment_log.created_at >=", $data['current_time']);
        $this->db->where("appointment_log.created_at >=", $data['current_time']);
        $this->db->group_start();
        $this->db->where("(appointment.status = 2 OR appointment.status = 3)");
        $this->db->group_end();
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

}