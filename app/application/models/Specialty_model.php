<?php

/**
 * Class Specialty Model
 *
 * @property CI_DB_query_builder $db
 */
class Specialty_Model extends CI_Model
{
    public function getList($limit = null, $page = null)
    {
        $this->db->order_by('id', 'ASC');
        $this->db->from('specialty');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        $rows = $this->db->get()->result();

        $result = [];
        foreach ($rows as $row) {
            $result[$row->id] = $row->name;
        }

        return $result;
    }
    public function getList_count()
    {
        return count($this->getList());
    }

    public function remove($id){
        $this->db->delete('specialty', [
            'id' => $id
        ]);
    }

    public function check_exists($specialty)
    {
        $this->db->where('name', $specialty);
        $query = $this->db->get('specialty');
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update('specialty', $data);
    }

    /**
     * Create new speciality
     *
     * @param $name
     * @return mixed
     */
    public function create($name)
    {
        $this->db->insert('specialty', [
            'name' => $name,
        ]);

        return $this->db->insert_id();
    }

    public function getById($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('specialty');

        $rows = $query->result();

        return empty($rows) ? null : reset($rows);
    }
}