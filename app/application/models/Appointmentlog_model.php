<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Appointmentlog_model extends CI_Model
{

    public function add($data){
        $this->db->insert('appointment_log', $data);
        return $this->db->insert_id();
    }

    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update('appointment_log', $data);
    }

    public function getList($limit = null, $page = null)
    {
        $this->db->order_by('id', 'ASC');
        $this->db->from('appointment_log');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        return $this->db->get()->result();
    }

    public function getList_count(){
        return count($this->getList());
    }

    public function getById($id)
    {
        $this->db->where('id', $id)->limit(1);

        $query = $this->db->get('appointment_log');

        return $query->row();
    }

    public function getByFields($data)
    {
        $this->db->select('appointment_log.*');
        foreach ($data as $field => $value){
            $this->db->where($field, $value);
        }
        $this->db->join('appointment', 'appointment.id = appointment_log.appointment_id');

        return $this->db->get('appointment_log')->result();
    }

    public function check_exists($data)
    {
        foreach ($data as $field => $value){
            $this->db->where($field, $value);
        }
        $this->db->join('appointment', 'appointment.id = appointment_log.appointment_id');
        $query = $this->db->get('appointment_log');
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function remove($id){
        $this->db->delete('appointment_log', [
            'id' => $id
        ]);
    }
}