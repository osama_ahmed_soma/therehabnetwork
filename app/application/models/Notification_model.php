<?php
/**
 * Created by PhpStorm.
 * User: ziku
 * Date: 8/26/16
 * Time: 12:02 PM
 */
class Notification_model extends CI_Model
{

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function add_notitication( $data ) {
        $this->db->insert('notification', $data);
        return $this->db->insert_id();
    }

    public function countUnreadNotifications( $id = null ) {
        if ( null === $id ) {
            $id = $this->session->userdata('userid');
        }

        return $this->db->select('id')
            ->from('notification')
            ->where('to_userid', $id)
            ->where('view', 0)
            ->where('deleted', 0)
            ->count_all_results();
    }

    public function getUnreadNotifications( $id = null, $count = 4 ) {
        if ( null === $id ) {
            $id = $this->session->userdata('userid');
        }

        //get usertype
        $this->load->model('login_model');
        $user = $this->login_model->get_user($id);

        if ( is_object($user) ) {
            if ( $user->type == 'patient' ) {
                //get doctor
                return $this->db->select('notification.id, notification.message, notification.added, doctor.image_url')
                    ->from('notification')
                    ->join('doctor', 'notification.from_userid = doctor.userid')
                    ->where('notification.to_userid', $id)
                    ->where('notification.view', 0)
                    ->where('notification.deleted', 0)
                    ->order_by('notification.id', 'DESC')
                    ->limit($count)
                    ->get()
                    ->result();
            }
            elseif ( $user->type == 'doctor' ) {
                //get patient
                return $this->db->select('notification.id, notification.message, notification.added, patient.image_url')
                    ->from('notification')
                    ->join('patient', 'notification.from_userid = patient.user_id')
                    ->where('notification.to_userid', $id)
                    ->where('notification.view', 0)
                    ->where('notification.deleted', 0)
                    ->order_by('notification.id', 'DESC')
                    ->limit($count)
                    ->get()
                    ->result();
            }
        }


    }

    public function getNotifications( $limit = null, $page = null )
    {
        $id = $this->session->userdata('userid');

        //get usertype
        $this->load->model('login_model');
        $user = $this->login_model->get_user($id);

        if ( is_object($user) ) {
            if ( $user->type == 'patient' ) {
                //get doctor
                return $this->db->select('notification.id, notification.view, notification.message, notification.added, doctor.image_url, doctor.firstname, doctor.lastname')
                    ->from('notification')
                    ->join('doctor', 'notification.from_userid = doctor.userid')
                    ->where('notification.to_userid', $id)
                    ->where('notification.deleted', 0)
                    ->where('notification.added >', strtotime('-30 day', time()))
                    ->order_by('notification.id', 'DESC')
                    ->limit($limit, $page)
                    ->get()
                    ->result();
            }
            elseif ( $user->type == 'doctor' ) {
                //get patient
                return $this->db->select('notification.id, notification.view, notification.message, notification.added, patient.image_url, patient.firstname, patient.lastname')
                    ->from('notification')
                    ->join('patient', 'notification.from_userid = patient.user_id')
                    ->where('notification.to_userid', $id)
                    ->where('notification.deleted', 0)
                    ->where('notification.added >', strtotime('-30 day', time()))
                    ->order_by('notification.id', 'DESC')
                    ->limit($limit, $page)
                    ->get()
                    ->result();
            }
        }
    }

    public function countNotifications()
    {
        $id = $this->session->userdata('userid');

        return $this->db->select('notification.id')
            ->from('notification')
            ->where('notification.to_userid', $id)
            ->where('notification.deleted', 0)
            ->where('notification.added > ', strtotime('-30 day', time()))
            ->order_by('notification.id', 'DESC')
            ->count_all_results();
    }

    public function readNotification($noficationID){
        $this->db->where('id', $noficationID);
        $this->db->update('notification', [
            'view' => 1
        ]);
    }

    public function readAllNotifications_doctor($doctorID){
        $this->db->where('to_userID', $doctorID);
        $this->db->where('to_usertype', 'doctor');
        $this->db->update('notification', [
            'view' => 1
        ]);
    }

    public function readAllNotifications_patient($patieentID){
        $this->db->where('to_userID', $patieentID);
        $this->db->where('to_usertype', 'patient');
        $this->db->update('notification', [
            'view' => 1
        ]);
    }

    public function deleteNotification( $noficationID ) {
        $this->db->where('id', $noficationID);
        $this->db->update('notification', [
            'deleted' => 1
        ]);
    }

    public function getNotification( $notification_id, $user_id = null ) {
        if( null === $user_id )
            $user_id = $this->session->userdata('userid');

        if ( is_numeric($notification_id) ) {
            return $this->db->select('notification.id')
                ->from('notification')
                ->where('notification.to_userid', $user_id)
                ->where('notification.id', $notification_id)
                ->count_all_results();
        } else {
            return 0;
        }
    }

}