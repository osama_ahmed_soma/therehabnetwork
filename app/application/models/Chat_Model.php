<?php


class Chat_Model extends CI_Model
{
    public function find_users_for_chat($contact_user_id)
    {
        $this->db->select('*');
        $this->db->from('contact_list');
        $this->db->join('user', 'user.id = contact_list.to_contact_person');
        $this->db->where('contact_user', $contact_user_id);
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function find_chat_partner($partner_id)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('id', $partner_id);
        $query_result = $this->db->get();
        return $query_result->row();
    }

//    public function message_save($receiver, $sender, $message){
////        $data_array = array(
////          'to_userid' => $receiver,
////          'from_userid' => $sender,
////          'messages' => $message
////        );
//        $query = "INSERT INTO private_messages (to_userid,from_userid,messages) VALUES (?,?,?)";
//        $this->db->query($query, array($receiver,$sender,$message));
//        //$this->db->insert('private_messages', $data_array);
//        //return $this->db->insert_id();
//        return $this->db->insert_id();
//    }
    public function message_save($receiver, $sender, $message)
    {
        $data_array = array(
            'to_userid' => $receiver,
            'from_userid' => $sender,
            'messages' => $message,
            'opened' => 0,
            'recipient_delete' => 0,
            'sender_delete' => 0
        );
//            $query = "INSERT INTO private_messages (to_userid,from_userid,messages) VALUES (?,?,?)";
//            $this->db->query($query, array($receiver,$sender,$message));
        $this->db->insert('private_messages', $data_array);
        //return $this->db->insert_id();
        return $this->db->insert_id();
    }


}