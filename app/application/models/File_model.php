<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class File_model extends CI_Model
{

	public $path;

	public function __construct()
	{
		parent::__construct();
		$this->path = DOCUMENTS_DIR . '/' . $this->session->userdata('type') . '/' . $this->session->userdata('userid') . '/';
	}

	function insert($data)
	{
		$data = trim_it($data);
		$check = $this->db->insert('files_upload', array(
			'filename' => $data['file_name'],
			'filesize' => $data['file_size'] * 1024,
			'filetype' => ltrim($data['file_ext'], '.'),
			'user_id' => $this->session->userdata("userid"),
			'date_time' => date('Y-m-d H:i:s'),
			'title' => (empty($data['title'])) ? 'Untitled File' : $data['title'],
//			'title' => (empty($data['title'])) ? $data['file_name'] : $data['title'] ,
		));
		if ($check) {
			return $this->get_file($this->db->insert_id());
		}
		return false;
	}

	function update($id, $data)
	{
		$data = trim_it($data);
		$data['title'] = (empty($data['title'])) ? 'Untitled File' : $data['title'];
		$this->db->set($data);
		$this->db->where('id', $id);
		$this->db->where('user_id', $this->session->userdata("userid"));
		$check = $this->db->update('files_upload');
		if ($check) {
			return true;
		}
		return false;
	}

	public function get_file($file_id)
	{
		return $this->db->where('id', $file_id)->where('user_id', $this->session->userdata('userid'))->get('files_upload')->row();
	}

	public function files($limit, $page)
	{
		if ($limit !== null) {
			$this->db->limit($limit, $page);
		}
		$q = $this->db->where('user_id', $this->session->userdata('userid'))->order_by('id', 'DESC')->get('files_upload');
		return $q->result();
	}

	public function search_files($limit = null, $page = null, $search_text = '', $count_only = false)
	{
		$this->db->where('user_id', $this->session->userdata('userid'))->order_by('id', 'DESC')->from('files_upload');
		if ($search_text) {
			$this->db->where("(title LIKE '%" . $search_text . "%' ESCAPE '!' OR  filename LIKE '%" . $search_text . "%' ESCAPE '!' )");
		}
		if ($count_only) {
			return $this->db->count_all_results();
		}
		if ($limit > 0) {
			$this->db->limit($limit, $page);
		}
		$query_result = $this->db->get();
		return $query_result->result();
	}

	public function count_files()
	{
		$this->db->where('user_id', $this->session->userdata('userid'))->from('files_upload');
		return $this->db->count_all_results();
	}

	public function delete($file_id)
	{
		$file = $this->get_file($file_id);
		$this->db->where('id', $file_id)->where('user_id', $this->session->userdata('userid'))->delete('files_upload');
		$success = (bool) $this->db->affected_rows();
		$this->delete_shares_of_file($file_id);
		if (is_file($this->path . $file->filename)) {
			@unlink($this->path . $file->filename);
		}
		return $success;
	}

	public function delete_shares_of_file($file_id)
	{
		$this->db->where('file_id', $file_id)->where('shared_by_id', $this->session->userdata('userid'))->delete('shared_files');
	}

	public function delete_shared($id)
	{
		$this->db->where('id', $id)->where('shared_by_id', $this->session->userdata('userid'))->delete('shared_files');
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		return false;
	}

	public function shared_files($file_id)
	{
		$this->db->select('CONCAT_WS(" ", usr.firstname, usr.lastname) AS shared_with, f.title, sf.date_time, f.filename, sf.id', false);
		$this->db->where('shared_by_id', $this->session->userdata('userid'));
		$this->db->where('file_id', $file_id);
		if ($this->session->userdata('type') == 'patient') {
			$this->db->join('doctor usr', 'usr.userid = sf.shared_to_id');
		} elseif ($this->session->userdata('type') == 'doctor') {
			$this->db->join('patient usr', 'usr.user_id = sf.shared_to_id');
		}
		$this->db->join('files_upload f', 'f.id = sf.file_id');

		$query = $this->db->get('shared_files sf');
		return $query->result();
	}

	function get_shared_file($file_id, $shared_to_id)
	{
		return $this->db->where('file_id', $file_id)
						->where('shared_by_id', $this->session->userdata('userid'))
						->where('shared_to_id', $shared_to_id)
						->get('shared_files')
						->row();
	}

	function get_file_shared($file_id, $shared_by_id)
	{
		return $this->db->select('files_upload.*')
						->where('file_id', $file_id)
						->where('shared_by_id', $shared_by_id)
						->where('shared_to_id', $this->session->userdata('userid'))
						->join('files_upload', 'shared_files.file_id = files_upload.id')
						->get('shared_files')
						->row();
	}

	function share_file($shared_to_id, $file_id)
	{
		$check = $this->db->insert('shared_files', array(
			'shared_to_id' => $shared_to_id,
			'file_id' => $file_id,
			'shared_by_id' => $this->session->userdata("userid"),
			'date_time' => date('Y-m-d H:i:s'),
		));
		if ($check) {
			return $this->db->insert_id();
		}
		return false;
	}

	function get_files_shared_by_patient($patient_id)
	{
		$q = $this->db
				->select('files_upload.title, files_upload.filename, files_upload.filesize, shared_files.date_time, files_upload.id')
				->where('shared_to_id', $this->session->userdata('userid'))
				->where('shared_by_id', $patient_id)
				->join('files_upload', 'shared_files.file_id = files_upload.id')
				->order_by('id', 'DESC')
				->get('shared_files');
		return $q->result();
	}

}
