<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Timezone_Model extends CI_Model
{
    public function getList()
    {
        $this->db->order_by('name', 'ASC');
        $this->db->from('time_zone');
        $rows = $this->db->get()->result();

        $result = [];
        foreach ($rows as $row) {
            $result[$row->id] = $row->name;
        }

        return $result;
    }
}