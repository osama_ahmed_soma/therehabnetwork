<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Duration_model extends CI_Model
{

    public function add($time){
        $this->db->insert('duration', [
            'time' => $time
        ]);
        return $this->db->insert_id();
    }

    public function update($id, $time){
        $this->db->where('id', $id);
        $this->db->update('duration', [
            'time' => $time
        ]);
    }

    public function getList($limit = null, $page = null)
    {
        $this->db->order_by('id', 'ASC');
        $this->db->from('duration');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        return $this->db->get()->result();
    }

    public function getById($id)
    {
        $this->db->where('id', $id)->limit(1);

        $query = $this->db->get('duration');

        return $query->row();
    }

    public function remove($id){
        $this->db->delete('duration', [
            'id' => $id
        ]);
    }
}