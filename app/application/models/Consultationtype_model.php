<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class Consultationtype_model extends CI_Model
{

    public function add($data){
        $this->db->insert('doctor_consultation_types', $data);
        return $this->db->insert_id();
    }

    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update('doctor_consultation_types', $data);
    }

    public function getList($limit = null, $page = null)
    {
        $this->db->select('doctor_consultation_types.id, doctor_consultation_types.doctor_id, , duration.time as duration, doctor_consultation_types.rate');
        $this->db->join('duration', 'duration.id = doctor_consultation_types.duration_id');
        $this->db->order_by('doctor_consultation_types.id', 'ASC');
        $this->db->from('doctor_consultation_types');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        return $this->db->get()->result();
    }

    public function getByDoctorList($doctor_id)
    {
        $this->db->select('doctor_consultation_types.id, doctor_consultation_types.doctor_id, duration.time as duration, duration.id as duration_id, doctor_consultation_types.rate');
        $this->db->join('duration', 'duration.id = doctor_consultation_types.duration_id');
        $this->db->order_by('doctor_consultation_types.id', 'ASC');
        $this->db->from('doctor_consultation_types');
        $this->db->where('doctor_consultation_types.doctor_id', $doctor_id);
        return $this->db->get()->result();
    }

    public function getById($id)
    {
        $this->db->where('id', $id)->limit(1);

        $query = $this->db->get('doctor_consultation_types');

        return $query->row();
    }

    public function remove($id){
        $this->db->delete('doctor_consultation_types', [
            'id' => $id
        ]);
    }

    public function isExists($doctorID, $duration = null, $payment_rate = null, $consultation_type_id = null){
        $this->db->where('doctor_id', $doctorID);
        if ($duration !== null) {
            $this->db->where('duration_id', $duration);
        }
        if ($payment_rate !== null) {
            $this->db->where('rate', $payment_rate);
        }
        if ($consultation_type_id !== null) {
            $this->db->where('id !=', $consultation_type_id);
        }
        $query = $this->db->get('doctor_consultation_types');
        return (count($query->row()) > 0) ? true: false ;
    }

    public function get_consultation_by_id( $doctor_consultation_types_id ) {
        return $this->db->select('doctor_consultation_types.id, doctor_consultation_types.doctor_id, duration.time as duration, duration.id as duration_id, doctor_consultation_types.rate')
            ->join('duration', 'duration.id = doctor_consultation_types.duration_id')
            ->from('doctor_consultation_types')
            ->where('doctor_consultation_types.id', $doctor_consultation_types_id)
            ->limit(1)
            ->get()
            ->row();
    }
}