<?php

/**
 * Class State_Model
 *
 * @property CI_DB_query_builder $db
 */
class State_Model extends CI_Model
{
    public function getList()
    {
        $this->db->order_by('name', 'ASC');
        $this->db->from('state');
        $rows = $this->db->get()->result();

        $result = [];
        foreach ($rows as $row) {
            $result[$row->id] = $row->name;
        }

        return $result;
    }

    public function get_by_id($id){
        $this->db->from('state');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }
}