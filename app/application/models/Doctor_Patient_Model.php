<?php

class Doctor_Patient_Model extends CI_Model
{
    public function find_doctor_patient($doctor_id)
    {
        $this->db->select('*');
        $this->db->from('doctor_appointment');
        $this->db->where('doctor_id', $doctor_id);
        $query_result = $this->db->get();
        return $query_result->result();
    }
}