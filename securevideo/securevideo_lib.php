<?php
//api identifier
//
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

require_once $siteRoot .'securevideo/restclient.php';

class Securevideo_Lib {
    private $api_identifier, $api_secret_key, $api_identifier1, $api_secret_key1, $api_key_decoded, $base_url;


    public function __construct() {
        //set developer keys
        $this->api_identifier1 = '8ded5a8b1aaa43719051a9a01a389bcdf1ed7837b11245a5abd02551c9411174'; //sandbox
        $this->api_secret_key1 = 'f1b2c58e367f422e86a679be6dc3f1162bf99176b1d244c484ed65dd792abc10'; //sandbox

        $this->api_identifier = '15343b61231c48bb8933bb4d366b3161a48cb697de744cb08f9e28d13463deb5';
        $this->api_secret_key = 'a58695812bc4495d9d205b1fd5faacd4bd65751fa78a4a42ae956497b9e3301f';

        $this->api_key_decoded = 'MTUzNDNiNjEyMzFjNDhiYjg5MzNiYjRkMzY2YjMxNjFhNDhjYjY5N2RlNzQ0Y2IwOGY5ZTI4ZDEzNDYzZGViNTphNTg2OTU4MTJiYzQ0OTVkOWQyMDViMWZkNWZhYWNkNGJkNjU3NTFmYTc4YTRhNDJhZTk1NjQ5N2I5ZTMzMDFm';
        $this->base_url = "https://api.securevideo.com/";
    }

    public function createUser( $user_data ) {
        $api = new RestClient(array(
            'base_url' => $this->base_url,
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Basic ' . $this->api_key_decoded
            ),
            'user_agent' => 'my virtual doctor'
        ));

        $result = $api->post('v2/admin/patients', json_encode($user_data));
    }

    public function getAllUser() {
        $api = new RestClient(array(
            'base_url' => $this->base_url,
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Basic ' . $this->api_key_decoded
            ),
            'user_agent' => 'my virtual doctor'
        ));

        $result = $api->get('user');

        if ( $result->response != '') {
            $result1 = $result->decode_response();

            if ( is_array($result1) ) {
                $ret = array(
                    'status' => '1', //user created successfully
                    'message' => 'Successful',
                    'data' => $result1
                );
            } else {
                $ret = array(
                    'status' => '2', //Some error, probably user already exist
                    'message' => $result1,
                    'data' => ''
                );
            }
        }

        if( $result->error != '') {
            $result1 = $result->error;
            $ret = array(
                'status' => '3', //error calling api
                'message' => 'API Call Error',
                'data' => $result1
            );
        }

        return $ret;

        /*
         //example response
         Array
            (
                [status] => 1
                [message] => Successful
                [data] => Array
                    (
                        [0] => stdClass Object
                            (
                                [SystemUserId] => 4933
                                [FullName] => A. Standin
                                [EmailAddress] => real38@mccoy.com
                                [SmsNumber] =>
                                [DefaultResolution] => high
                                [TimeZoneWindowsId] => Central Standard Time
                                [HelpNumber] =>
                                [VideoId] => securevideo+sv000000493354781437
                                [SystemRoles] => H,P
                            )

                        [1] => stdClass Object
                            (
                                [SystemUserId] => 19148
                                [FullName] => Amod Vardhan
                                [EmailAddress] => amod.vardhan@e-zest.in
                                [SmsNumber] =>
                                [DefaultResolution] => high
                                [TimeZoneWindowsId] => India Standard Time
                                [HelpNumber] => 212-555-5555
                                [VideoId] => securevideo+sv000001914811276042
                                [SystemRoles] => H,P
                            )
                    )
            )
         */
    }

    public function getUser( $user_id ) {
        $ret = array();

        if ( is_numeric($user_id) ) {
            $api = new RestClient(array(
                'base_url' => $this->base_url,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $this->api_key_decoded
                ),
                'user_agent' => 'my virtual doctor'
            ));

            $result = $api->get('user/' . intval($user_id));

            if ( $result->response != '') {
                $result1 = $result->decode_response();

                if ( is_object($result1) && !isset($result1->Message) ) {
                    $ret = array(
                        'status' => '1', //user created successfully
                        'message' => 'Successful',
                        'data' => $result1
                    );
                } else {
                    $ret = array(
                        'status' => '2', //Some error, probably user already exist
                        'message' => $result1->Message,
                        'data' => ''
                    );
                }
            }

            if( $result->error != '') {
                $result1 = $result->error;
                $ret = array(
                    'status' => '3', //error calling api
                    'message' => 'API Call Error',
                    'data' => $result1
                );
            }
        } else {
            $ret = array(
                'status' => '2', //error calling api
                'message' => 'Invalid user id or user not found',
            );
        }

        return $ret;

        /*
         //example response for successful
         Array
            (
                [status] => 1
                [message] => Successful
                [data] => stdClass Object
                    (
                        [SystemUserId] => 19148
                        [FullName] => Amod Vardhan
                        [EmailAddress] => amod.vardhan@e-zest.in
                        [SmsNumber] =>
                        [DefaultResolution] => high
                        [TimeZoneWindowsId] => India Standard Time
                        [HelpNumber] => 212-555-5555
                        [VideoId] => securevideo+sv000001914811276042
                        [SystemRoles] => H,P
                    )

            )
        //example response for error
        Array
            (
                [status] => 2
                [message] => Invalid user id or user not found
            )
         */
    }

    public function userEmailExist ( $user_email ) {
        $user_exist = false;
        $users = $this->getAllUser();
        if ( $users['status'] == 1) {
            foreach ( $users['data'] as $user ) {
                if ( $user->EmailAddress == $user_email ) {
                    $user_exist = true;
                    break;
                }
            }
        }

        return $user_exist;
    }

    public function createNewUser( $user_data ) {
        //user
        if ( is_array($user_data) ) {
            $api = new RestClient(array(
                'base_url' => $this->base_url,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $this->api_key_decoded
                ),
                'user_agent' => 'my virtual doctor'
            ));

            $result = $api->post('user', json_encode($user_data));

            if ( $result->response != '') {
                $result1 = $result->decode_response();

                if ( is_object($result1) && !isset($result1->Message) ) {
                    $ret = array(
                        'status' => '1', //user created successfully
                        'message' => 'Successful',
                        'data' => $result1
                    );
                } else {
                    $ret = array(
                        'status' => '2', //Some error, probably user already exist
                        'message' => $result1->Message,
                        'data' => ''
                    );
                }
            }

            if( $result->error != '') {
                $result1 = $result->error;
                $ret = array(
                    'status' => '3', //error calling api
                    'message' => 'API Call Error',
                    'data' => $result1
                );
            }
        } else {
            $ret = array(
                'status' => '2', //error calling api
                'message' => 'Invalid user data provided',
            );
        }

        return $ret;

        /*
         //success return
        Array
            (
                [status] => 1
                [message] => Successful
                [data] => stdClass Object
                    (
                        [SystemUserId] => 33084
                        [FullName] => Nurul Umbhiya P
                        [EmailAddress] => zikubd@gmail.com
                        [SmsNumber] =>
                        [DefaultResolution] => high
                        [TimeZoneWindowsId] => Bangladesh Standard Time
                        [HelpNumber] => 212-555-5555
                        [VideoId] => securevideo+sv000003308436605387
                        [SystemRoles] => P
                    )

            )
        //error return
        Array
        (
            [status] => 2
            [message] => The e-mail address zikubd@gmail.com is already in use.
            [data] =>
        )
         */

    }

    public function updateUser( $user_id, $user_data ) {
        //user
        if ( is_numeric($user_id) && is_array($user_data) ) {
            $api = new RestClient(array(
                'base_url' => $this->base_url,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $this->api_key_decoded
                ),
                'user_agent' => 'my virtual doctor'
            ));

            $result = $api->put('user/' . $user_id, json_encode($user_data));

            if ( $result->response != '') {
                $result1 = $result->decode_response();

                if ( is_object($result1) && !isset($result1->Message) ) {
                    $ret = array(
                        'status' => '1', //user created successfully
                        'message' => 'Successful',
                        'data' => $result1
                    );
                } else {
                    $ret = array(
                        'status' => '2', //Some error, probably user already exist
                        'message' => $result1->Message,
                        'data' => ''
                    );
                }
            }

            if( $result->error != '') {
                $result1 = $result->error;
                $ret = array(
                    'status' => '3', //error calling api
                    'message' => 'API Call Error',
                    'data' => $result1
                );
            }
        } else {
            $ret = array(
                'status' => '2', //error calling api
                'message' => 'Invalid user data provided',
            );
        }

        return $ret;

        /*
         //success return
        Array
        (
            [status] => 1
            [message] => Successful
            [data] => stdClass Object
                (
                    [SystemUserId] => 33084
                    [FullName] => Nurul Umbhiya
                    [EmailAddress] => ziku.bd@gmail.com
                    [SmsNumber] =>
                    [DefaultResolution] => high
                    [TimeZoneWindowsId] => Bangladesh Standard Time
                    [HelpNumber] => 212-555-5555
                    [VideoId] => securevideo+sv000003308436605387
                    [SystemRoles] => P
                )

        )
        //error return
        Array
        (
            [status] => 2
            [message] => The e-mail address zikubd@gmail.com is already in use.
            [data] =>
        )
         */

    }

    public function deleteUser( $user_id ) {
        //user
        if ( is_numeric($user_id) ) {
            $api = new RestClient(array(
                'base_url' => $this->base_url,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $this->api_key_decoded
                ),
                'user_agent' => 'my virtual doctor'
            ));

            $result = $api->delete('user/' . $user_id);

            if ( $result->response != '') {
                $result1 = $result->decode_response();

                if ( is_object($result1) && isset($result1->Message) ) {
                    $ret = array(
                        'status' => '2', //Some error, probably user already exist
                        'message' => $result1->Message,
                        'data' => ''
                    );
                }
            } else {
                $ret = array(
                    'status' => '1', //user created successfully
                    'message' => 'User deleted successfully.',
                    'data' => ''
                );
            }

            if( $result->error != '') {
                $result1 = $result->error;
                $ret = array(
                    'status' => '3', //error calling api
                    'message' => 'API Call Error',
                    'data' => $result1
                );
            }
        } else {
            $ret = array(
                'status' => '2', //error calling api
                'message' => 'Invalid user data provided',
            );
        }

        return $ret;

        /*
         //success return

        //error return
        Array
        (
            [status] => 2
            [message] => User not found
            [data] =>
        )
         */

    }

    public function getSessionsForUser( $user_id ) {
        if ( is_numeric($user_id) ) {
            $api = new RestClient(array(
                'base_url' => $this->base_url,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $this->api_key_decoded
                ),
                'user_agent' => 'my virtual doctor'
            ));

            $result = $api->get('session/' . $user_id);

            if ( $result->response != '') {
                $result1 = $result->decode_response();

                if ( is_array($result1) && !empty($result1) ) {
                    $ret = array(
                        'status' => '1', //user created successfully
                        'message' => 'Successful',
                        'data' => $result1
                    );
                } else {
                    $ret = array(
                        'status' => '2', //Some error, probably user already exist
                        'message' => $result1,
                        'data' => ''
                    );
                }
            }

            if( $result->error != '') {
                $result1 = $result->error;
                $ret = array(
                    'status' => '3', //error calling api
                    'message' => 'API Call Error',
                    'data' => $result1
                );
            }
        } else {
            $ret = array(
                'status' => '2', //error calling api
                'message' => 'Invalid user data provided',
            );
        }

        return $ret;
    }

    public function createNewSession( $doctor_id, $session_data ) {
        if ( is_numeric($doctor_id) && is_array($session_data) ) {
            $api = new RestClient(array(
                'base_url' => $this->base_url,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $this->api_key_decoded
                ),
                'user_agent' => 'my virtual doctor'
            ));

            $result = $api->post('session/' . $doctor_id, json_encode($session_data));

            if ( $result->response != '') {
                $result1 = $result->decode_response();

                if ( is_object($result1) && !isset($result1->Message) ) {
                    $ret = array(
                        'status' => '1', //user created successfully
                        'message' => 'Successful',
                        'data' => $result1
                    );
                } else {
                    $ret = array(
                        'status' => '2', //Some error, probably user already exist
                        'message' => $result1->Message,
                        'data' => ''
                    );
                }
            }

            if( $result->error != '') {
                $result1 = $result->error;
                $ret = array(
                    'status' => '3', //error calling api
                    'message' => 'API Call Error',
                    'data' => $result1
                );
            }
        } else {
            $ret = array(
                'status' => '2', //error calling api
                'message' => 'Invalid user data provided',
            );
        }

        return $ret;
    }

    public function updateSession( $session_id, $session_data ) {
        if ( is_numeric($session_id) && is_array($session_data) ) {
            $api = new RestClient(array(
                'base_url' => $this->base_url,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $this->api_key_decoded
                ),
                'user_agent' => 'my virtual doctor'
            ));

            $result = $api->put('session/' . $session_id, json_encode($session_data));

            if ( $result->response != '') {
                $result1 = $result->decode_response();

                if ( is_object($result1) && !isset($result1->Message) ) {
                    $ret = array(
                        'status' => '1', //user created successfully
                        'message' => 'Successful',
                        'data' => $result1
                    );
                } else {
                    $ret = array(
                        'status' => '2', //Some error, probably user already exist
                        'message' => $result1->Message,
                        'data' => ''
                    );
                }
            }

            if( $result->error != '') {
                $result1 = $result->error;
                $ret = array(
                    'status' => '3', //error calling api
                    'message' => 'API Call Error',
                    'data' => $result1
                );
            }
        } else {
            $ret = array(
                'status' => '2', //error calling api
                'message' => 'Invalid user data provided',
            );
        }

        return $ret;
    }

    public function deleteSession( $session_id ) {
        //user
        if ( is_numeric($session_id) ) {
            $api = new RestClient(array(
                'base_url' => $this->base_url,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $this->api_key_decoded
                ),
                'user_agent' => 'my virtual doctor'
            ));

            $result = $api->delete('session/' . $session_id);

            if ( $result->response != '') {
                $result1 = $result->decode_response();

                if ( is_object($result1) && isset($result1->Message) ) {
                    $ret = array(
                        'status' => '2', //Some error, probably user already exist
                        'message' => $result1->Message,
                        'data' => ''
                    );
                }
            } else {
                $ret = array(
                    'status' => '1', //user created successfully
                    'message' => 'Session deleted successfully.',
                    'data' => ''
                );
            }

            if( $result->error != '') {
                $result1 = $result->error;
                $ret = array(
                    'status' => '3', //error calling api
                    'message' => 'API Call Error',
                    'data' => $result1
                );
            }
        } else {
            $ret = array(
                'status' => '2', //error calling api
                'message' => 'Invalid user data provided',
            );
        }

        return $ret;

        /*
         //success return

        //error return
        Array
        (
            [status] => 2
            [message] => User not found
            [data] =>
        )
         */

    }

    /*
     * For security reasons, you must perform the redirect within 15 seconds of obtaining the Login URI
     */
    public function userLogin(  $user_id, $data = array('RedirectToUriOnExpiry' => HTTP_SERVER) ) {
        if ( is_numeric($user_id) ) {
            $api = new RestClient(array(
                'base_url' => $this->base_url,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $this->api_key_decoded
                ),
                'user_agent' => 'my virtual doctor'
            ));

            $result = $api->post('login/' . $user_id, json_encode($data));

            if ( $result->response != '') {
                $result1 = $result->decode_response();

                if ( is_object($result1) && !isset($result1->Message) ) {
                    $ret = array(
                        'status' => '1', //user created successfully
                        'message' => 'Successful',
                        'data' => $result1
                    );
                } else {
                    $ret = array(
                        'status' => '2', //Some error, probably user already exist
                        'message' => $result1->Message,
                        'data' => ''
                    );
                }
            }

            if( $result->error != '') {
                $result1 = $result->error;
                $ret = array(
                    'status' => '3', //error calling api
                    'message' => 'API Call Error',
                    'data' => $result1
                );
            }
        } else {
            $ret = array(
                'status' => '2', //error calling api
                'message' => 'Invalid user data provided',
            );
        }

        return $ret;

        /*
         //success response
        {
        "LoginGuid": "c1a714554134958fda34d32ebb4c070de1579430ba1471ba8d4c0b2b6e8ba8d6",
        "RedirectToUriOnExpiry": "https://www.mywebsite.com/HandleSecureVideoLoginExpiration",
        "LoginUri": "https://hub.securevideo.com/Account/Login?auto=c1a714554134958fda34d32ebb4c070de1579430ba1471ba8d4c0b2b6e8ba8d6"
        }
         */
    }

    public function loginPatient(  $user_id, $data = array('RedirectToUriOnExpiry' => HTTP_SERVER) ) {
        if ( is_numeric($user_id) ) {
            $api = new RestClient(array(
                'base_url' => $this->base_url,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $this->api_key_decoded
                ),
                'user_agent' => 'my virtual doctor'
            ));

            $result = $api->post('login/' . $user_id, json_encode($data));

            if ( $result->response != '') {
                $result1 = $result->decode_response();

                if ( is_object($result1) && !isset($result1->Message) ) {
                    $ret = array(
                        'status' => '1', //user created successfully
                        'message' => 'Successful',
                        'data' => $result1
                    );
                } else {
                    $ret = array(
                        'status' => '2', //Some error, probably user already exist
                        'message' => $result1->Message,
                        'data' => ''
                    );
                }
            }

            if( $result->error != '') {
                $result1 = $result->error;
                $ret = array(
                    'status' => '3', //error calling api
                    'message' => 'API Call Error',
                    'data' => $result1
                );
            }
        } else {
            $ret = array(
                'status' => '2', //error calling api
                'message' => 'Invalid user data provided',
            );
        }

        return $ret;
    }

    public function loginDoctor(  $user_id, $data = array('RedirectToUriOnExpiry' => HTTP_SERVER) ) {
        if ( is_numeric($user_id) ) {
            $api = new RestClient(array(
                'base_url' => $this->base_url,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $this->api_key_decoded
                ),
                'user_agent' => 'my virtual doctor'
            ));

            $result = $api->post('login/' . $user_id, json_encode($data));

            if ( $result->response != '') {
                $result1 = $result->decode_response();

                if ( is_object($result1) && !isset($result1->Message) ) {
                    $ret = array(
                        'status' => '1', //user created successfully
                        'message' => 'Successful',
                        'data' => $result1
                    );
                } else {
                    $ret = array(
                        'status' => '2', //Some error, probably user already exist
                        'message' => $result1->Message,
                        'data' => ''
                    );
                }
            }

            if( $result->error != '') {
                $result1 = $result->error;
                $ret = array(
                    'status' => '3', //error calling api
                    'message' => 'API Call Error',
                    'data' => $result1
                );
            }
        } else {
            $ret = array(
                'status' => '2', //error calling api
                'message' => 'Invalid user data provided',
            );
        }

        return $ret;
    }

    public function echoPre( $data ) {
        echo "<pre>"; print_r($data); echo "</pre>";
    }

}


$obj = new Securevideo_Lib();

//get all users
//$ret = $obj->getAllUser();
//$obj->echoPre($ret);

//get specific user
//$ret = $obj->getUser('aaa');
//$obj->echoPre($ret);

//create new user
$user_data_patient = array(
    'FullName' => 'Nurul Umbhiya P',
    'EmailAddress' => 'zikubd@gmail.com',
    'DefaultResolution' => 'high',
    'TimeZoneWindowsId' => 'Bangladesh Standard Time',
    'HelpNumber' => '212-555-5555',
    'SystemRoles' => 'P',
    'ShouldAutoDeliverCode' => 'E' //send emails
);
/*
//details for ShouldAutoDeliverCode

    "E": If ParticipantEmailAddress exists, the SecureVideo system will send via e-mail a Session invite and Session reminders to this participant.
    "S": If ParticipantSmsNumber exists, the SecureVideo system will send via text message a Session invite and Session reminders to this participant.
    "N": No automatic delivery of the code will occur.  The Session host, or your system, will be responsible for providing invites and reminders to the Participant.

 */

//$ret = $obj->createNewUser($user_data_patient);
//$obj->echoPre($ret);

$user_data_doctor = array(
    'FullName' => 'Nurul Umbhiya H',
    'EmailAddress' => 'ziku.bd@gmail.com',
    'DefaultResolution' => 'high',
    'TimeZoneWindowsId' => 'Bangladesh Standard Time',
    'HelpNumber' => '212-555-5555',
    'SystemRoles' => 'H',
    'ShouldAutoDeliverCode' => 'E' //send emails
);

//update user
$update_user_data = array(
    'FullName' => 'Nurul Umbhiya', //required
    'EmailAddress' => 'ziku.bd@gmail.com', //required
    'TimeZoneWindowsId' => 'Bangladesh Standard Time', //required
    'HelpNumber' => '212-555-5555', //required
);
$user_id = '33084';

//$ret = $obj->updateUser($user_id, $update_user_data);
//$obj->echoPre($ret);

//delete user
//$ret = $obj->deleteUser($user_id);
//$obj->echoPre($ret);

//create sessions / appointment
/*
{
 "ScheduleTs": "2014-04-16T15:00:00",
 "IsRecorded": true,
 "Participants": [
 {
 "SecureVideoUserId": "12"
 }
 ]
}
 */