<?php
define('MVD_SITE', true);
require("../includes/application_top.php");
?>
<!doctype html>
<html>
   <head>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
      <meta name="viewport" content="width=device-width">
      <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
      <link rel="stylesheet" href="compiled/flipclock.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
      <title>My Virtual Doctor</title>
      <script type="text/javascript">
      	function calculateROI(){
	        var numofproviders = $('#numofproviders').val();
	        var expectedvisits = $('#expectedvisits').val();
			var averagecharge = $('#averagecharge').val();
			
			if(numofproviders == "" || expectedvisits == "" || averagecharge == ""){
				
				if(numofproviders == ""){$('#numofproviders').focus();return false;}
				if(expectedvisits == ""){$('#expectedvisits').focus();return false;}
				if(averagecharge == ""){$('#averagecharge').focus();return false;}
				
				return false;
			}else{
				
				var numofproviders = parseFloat($('#numofproviders').val());
	        	var expectedvisits = parseFloat($('#expectedvisits').val());
				var averagecharge = parseFloat($('#averagecharge').val());
				
				var result = numofproviders * expectedvisits * averagecharge * 260;	
				$('#result').text("$"+result.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
				//toogle element
				$("#collapseExample").hide();
				$("#collapseExample").toggle('slow');
			}
			
	        
	        //$('#result').text(result.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
		}
      	
		function resetField(fieldId){
			var fieldValue = $('#'+fieldId).val();
			if(isNaN(fieldValue)){
				$('#'+fieldId).val("");
				$('#'+fieldId).focus();
			}
		}
		
      	
      </script>
   </head>
   <body>
      <div class="logfrom-main-header">
         <img src="image/logo.png"  /> 
         <div class="header-right">
         	<a href="<?php echo HTTP_SERVER; ?>">
            <ul>
               <li>Home</li>
            </ul>
            </a>
         </div>
      </div>
      <div class="log-back">
         <div class="container">
            <div class="logfrom-main">
               <div class="logfrom-left-trials">
               	<h1>Sign up now for your<br />90-Day Free trial</h1>
               </div>
               <div class="logfrom-left">
                  <h3> Happier patients equals a healthier practice. </h3>
               </div>
               <div class="signup-right">
               		<?php 
					if (isset($_GET['status']) && $_GET['status'] === "success"){?>
						<h2 class="">Form Submitted!</h2><p>Thank you for your time, someone from our side will be in touch with you shortly.</p><br /><br /><br /><br /><br />
					<?php }else{?>
	                  <h2 class="">Get Started Now</h2>
	                  <p class="">Fill out the information below and get started today.  Discover just how simple practicing telemedicine can be.</p>
	                  <div>
	                     <form data-role="form" action="email.php" name="registrationForm" method="post" class="ng-invalid ng-invalid-required ng-dirty">
	                        <div id="fullname-wrapper" class="form-group has-feedback margin-bottom-sm">
	                           <input class="form-control ng-pristine ng-valid" type="text" name="first_name" data-key-trigger="register({'event': event})" data-ng-model="my.name" placeholder="First Name" required>
	                        </div>
	                        <div id="fullname-wrapper" class="form-group has-feedback margin-bottom-sm">
	                           <input class="form-control ng-pristine ng-valid" type="text" name="last_name" data-key-trigger="register({'event': event})" data-ng-model="my.name" placeholder="Last Name">
	                        </div>
	                        <div id="username-wrapper" class="form-group has-feedback margin-bottom-sm" data-ng-class="{ 'has-error' :  registrationForm.username.$invalid  &amp;&amp; my.submitted}">
	                           <input class="form-control ng-isolate-scope ng-pristine ng-invalid ng-invalid-required ng-valid-email" autocomplete="off" type="email" name="email_add" data-key-trigger="register({'event': event})" data-ng-model="my.username" data-ng-change="my.auth_error = ''" placeholder="Email address" data-as-popover="my.submitted &amp;&amp; registrationForm.username.$invalid" data-as-type="emailreg" required="required">
	                        </div>
	                        <div id="password-wrapper" class="form-group has-feedback" data-ng-class="{ 'has-error' :  (!my.password || registrationForm.password.$invalid)  &amp;&amp; my.submitted}">
	                           <input name="phone_number" type="text" class="form-control ng-isolate-scope ng-pristine ng-valid ng-valid-minlength" autocomplete="off" placeholder="Phone Number" data-key-trigger="register({'event': event})" data-password-policy="" data-policy-error-count="my.policy_error_count" data-ng-minlength="8" data-ng-change="my.auth_error = ''" data-ng-model="my.password"  required="required">
	                           <ul class="error-list"></ul>
	                           <input type="password" name="password_hidden" autocomplete="off" style="display:none">
	                        </div>
	                        <div id="consent" class="form-group has-feedback" data-ng-class="{ 'has-error' :  (!my.consent || registrationForm.consent.$invalid) &amp;&amp; my.submitted }">
	                           <span class="required-checkbox consent-needed" data-ng-class="{ 'consent-needed' : !my.consent }">
	                           <input type="checkbox" name="consent" data-key-trigger="register({'event': event})" class="cursor ng-isolate-scope ng-pristine ng-invalid ng-invalid-required" data-ng-class="{'checkbox-error' : !my.consent &amp;&amp; my.submitted }" data-ng-change="my.auth_error = ''" data-ng-model="my.consent" required="required" data-as-popover="my.submitted &amp;&amp; (!my.consent || registrationForm.consent.$invalid)" data-as-type="consent"> 
	                           </span>
	                           <div>
	                              <p class="tou-wrapper">
	                                 <label class="cursor" data-ng-click="my.consent = true">I agree to My Virtual Doctor's<br /><a href="<?php echo HTTP_SERVER; ?>termandconditions.html" target="_blank">Terms and Conditions</a></label>
	                                 </br>
	                              </p>
	                           </div>
	                        </div>
	                        <input type="submit" id="btn-login"  class="btn btn-info sing login" value="Start Practicing Telehealth Today">
	                  </div>
	                  </form>
	                  <?php }?>
                  
               </div>
            </div>
         </div>
      </div>
      <div class="calculator-main">
         <div class="container">
            <h1>My Virtual Doctor is simplifying telemedicine </h1>
            <p>Find out how much you can increase the revenue of your practice by implementing our turnkey telemedicine solution.</p>
            <div class="calculator-box-main">
               <div class="calculator-box">
                  <h6>Number of Providers</h6>
                  <p><input class="form-control ng-pristine ng-valid" id="numofproviders" type="text" name="numofproviders" required onkeyup="resetField('numofproviders')"></p>
               </div>
               <div class="calculator-box">
                  <h6>Expected number of telemedicine visits per day, per provider</h6>
                  <p><input class="form-control ng-pristine ng-valid" id="expectedvisits" type="text" name="expectedvisits" required onkeyup="resetField('expectedvisits')"></p>
               </div>
               <div class="calculator-box">
                  <h6>Average charge for telemedicine visits<br><span>If you're not sure, use $75 </span></h6>
                  <p><input  class="form-control ng-pristine ng-valid" id="averagecharge" type="text" name="averagecharge" required onkeyup="resetField('averagecharge')" value="75"></p>
                 
               </div>
               <p>
                  <button id="my_button" class="btn btn-primary" type="button" onclick="return calculateROI();">
                  Calculate Revenue
                  </button>
               </p>
               <div class="collapse" id="collapseExample">
                  <div class="card card-block">
                     <p>By adding My Virtual Doctor to your practice, you'd increase revenue by </p>
                     <h1><span id="result"></span><span>/year</span></h1>
                     <a class="green-outline-button text-center" href="<?php echo HTTP_SERVER; ?>app/register/doctor">Get Started</a>
                  </div>
               </div>
               <div id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
   
      <div class="logfrom-botem-one">
         <div class="container">
            <div class="logfrom-botem-main-left">
               <h1>What Is My Virtual Doctor?</h1>
               <p>My Virtual Doctor is a HIPAA-compliant telehealth software company that enables physicians to deliver remote, interactive healthcare services directly to patients while improving financial and operational efficiencies. Our technology simultaneously gives patients better access to high-quality, cost-effective care and a wider choice of providers qualified to address their individual needs.  </p>
            </div>
            <div class="logfrom-botem-main-right">
               <img src="image/shutterstock_90664207.png"  /> 
            </div>
         </div>
      </div>
      <div class="logfrom-botem">
         <div class="container">
            <div class="logfrom-botem-main">
               <div class="virtual-doctor-back">
                  <h1>Why My Virtual Doctor?</h1>
                  <div class="logfrom-botem-box">
                     <img src="image/chart2-128.png"  class="icon"/>
                     <h4> Increase Revenue </h4>
                     <p>See more patients, reduce no shows, increase patient engagement and grow your patient retention rate.</p>
                  </div>
                  <div class="logfrom-botem-box">
                     <img src="image/oYPS__reduce_zoom_picture_photo-128.png"  class="icon"/>
                     <h4> Reduce Overhead </h4>
                     <p>Free up exam rooms, lower office supply costs and cut the administrative tasks of staff in half. Gone is the need for manual patient data entry or check-ins.</p>
                  </div>
                  <div class="logfrom-botem-box" style="    margin-right: 0px !important;">
                     <img src="image/road-sign-convenience-store-128.png"  class="icon"/>
                     <h4> Convenience </h4>
                     <p>Increase flexibility with appointment times and both patients and doctors alike can access appointments remotely.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
     <div class="footer-bottem">
    <div class="container">
       <div class="col-md-12 col-sm-12">
            <div class="col-md-3 col-sm-3">
                <div class="footer-nav">
                	<h3>About</h3>
                    <ul>
                        <li><a href="<?php echo HTTP_SERVER; ?>our-mission.html">Our Mission</a></li>
                        <li><a href="<?php echo HTTP_SERVER; ?>privacypolicy.html">Privacy Policy</a></li>
                        <li><a href="<?php echo HTTP_SERVER; ?>termandconditions.html">Terms and Conditions</a></li>
                        <li> <a href="<?php echo HTTP_SERVER; ?>contact-us.html">Contact  </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="footer-nav ">
               	 <h3>For Patients</h3>
                    <ul>
                        <li><a href="<?php echo HTTP_SERVER; ?>login.html">Log in</a></li>
                        <li><a href="<?php echo HTTP_SERVER; ?>signup.html">Sign up</a></li>
                        <li><a href="<?php echo HTTP_SERVER; ?>blog.html">Blog</a></li>
                        <li><a href="<?php echo HTTP_SERVER; ?>faqs.html">FAQs</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="footer-nav ">
                <h3>For Doctors</h3>
                    <ul>
                        <li><a href="<?php echo HTTP_SERVER; ?>app/login"> Log in</a></li>
                        <li><a href="<?php echo HTTP_SERVER; ?>request-a-demo.html">Sign up</a></li>
                        <li><a href="<?php echo HTTP_SERVER; ?>blog.html">Blog</a></li>
                        <li><a href="<?php echo HTTP_SERVER; ?>providers.html">Our Platform</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="footer-nav ">
                 <h3>Connect with Us</h3>
                   <ul style="margin-top: 14px;margin-bottom: 8px;"> 
                       <i class="fa fa-twitter"></i>
                       <i class="fa fa-facebook-f"></i>
                       <i class="fa fa-linkedin"></i>
                       <i class="fa fa-pinterest-p"></i>                    
                   </ul>
            	</div>
                <div class="footer-nav footer-logo-area">
                   <ul>
                      <li><a href="<?php echo HTTP_SERVER; ?>"><img src="<?php echo HTTP_SERVER; ?>img/logo.png" class="img-responsive" width="180px"></a></li>
                   </ul>
                </div>
        </div>
        
         </div>
  </div>
        

  <div class="footer-bottem copyright-section ">
   
         <div class="col-md-12 col-sm-12 copyright new">
            <p> 2016 My Virtual Doctor. All Right Reserved.  <span>Made In Florida <img src="<?php echo HTTP_SERVER; ?>img/mp.png"></span>  </p>
        </div>
    
</div>

<script>
/*! Main */
jQuery(document).ready(function($) {
  
		var num = 95; //number of pixels before modifying styles
		$(window).bind('scroll', function () {
			if ($(window).scrollTop() > num) {
				$('#navbar-main').addClass('fixed');
			} else {
				$('#navbar-main').removeClass('fixed');
			}
		});
});


</script>	<!-- Bootstrap 3.3.5 -->
	<script src="<?php echo HTTP_SERVER; ?>js/bootstrap.js"></script>
	<script src="<?php echo HTTP_SERVER; ?>admin/AdminLTE/plugins/bootstrap-rating/bootstrap-rating.min.js"></script>








    

</div>
      

   </body>
</html>	