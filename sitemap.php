<?php
define( 'MVD_SITE', true );
require("includes/application_top.php");
//deny direct access
$html = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
$html .= "<url><loc>".HTTP_SERVER."</loc></url>";
$html .= "<url><loc>".HTTP_SERVER."how-it-works.html</loc></url>";	
$html .= "<url><loc>".HTTP_SERVER."our-mission.html</loc></url>";	
$html .= "<url><loc>".HTTP_SERVER."contact-us.html</loc></url>";	

			$qry = "SELECT CONCAT(  '".HTTP_SERVER."doctor/', url,'.html' ) url_path FROM mvd101_doctors WHERE status = ?";
			//$query = "select * from mvd101_health_tips where id = ? LIMIT 1";
			$result = $db->prepare($qry);
			$result->execute( array('publish') );
			
			if ( $result->rowCount() > 0 ) {
						;
				
				while($data = $result->fetch())
				{
					$html .= "<url><loc>".$data['url_path']."</loc></url>";	
				}
			}
			
			unset($data);
			unset($result);
			

$html .= "<url><loc>".HTTP_SERVER."blog-for-doctors.html</loc></url>";	

$arr = array();
$select = "SELECT CONCAT(  '".HTTP_SERVER."blog-for-doctors/', tips.url,'.html' ) url_path";
$table = " FROM `" . DB_PREFIX . "health_tips` as tips, `" . DB_PREFIX . "doctors` as doc";
$where = " WHERE doc.id = tips.doctorId AND tips.status = 'publish' AND tips.post_type = 2";
$sql = $select . $table . $where;
		$result = $db->prepare($sql);
			$result->execute( $arr );
			
			if ( $result->rowCount() > 0 ) {
						;
				
				while($data = $result->fetch())
				{
					$html .= "<url><loc>".$data['url_path']."</loc></url>";	
				}
			}
			
			unset($data);
			unset($result);


$html .= "<url><loc>".HTTP_SERVER."blog-for-patients.html</loc></url>";	


$arr = array();
$select = "SELECT CONCAT(  '".HTTP_SERVER."blog-for-patients/', tips.url,'.html' ) url_path";
$table = " FROM `" . DB_PREFIX . "health_tips` as tips, `" . DB_PREFIX . "doctors` as doc";
$where = " WHERE doc.id = tips.doctorId AND tips.status = 'publish' AND tips.post_type = 1";
$sql = $select . $table . $where;
		$result = $db->prepare($sql);
			$result->execute( $arr );
			
			if ( $result->rowCount() > 0 ) {
						;
				
				while($data = $result->fetch())
				{
					$html .= "<url><loc>".$data['url_path']."</loc></url>";	
				}
			}
			
			unset($data);
			unset($result);



	




$html .= '</urlset>';	

echo "<h2>SiteMap generated successfully. <a href='".HTTP_SERVER."sitemap.xml'>Click here</a> to see sitemap in xml format.</h2>";

unset($read);



		@unlink('sitemap.xml');
        $file = "sitemap.xml";
        $file_handle = fopen($file,"a");
        fwrite($file_handle, $html . "\r\n");
        fclose($file_handle);
?>