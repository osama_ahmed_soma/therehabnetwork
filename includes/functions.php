<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}


function getTimeZoneHtml2( $name, $required = 'false', $selected = '', $first_option = '<option value="">Select Timezone</option>' ) {
	$timezones = array(
			"Atlantic Standard Time" => "Atlantic Standard Time",
			"Eastern Standard Time" => "Eastern Standard Time",
			"Central Standard Time" => "Central Standard Time",
			"US Mountain Standard Time" => "US Mountain Standard Time",
			"Pacific Standard Time" => "Pacific Standard Time",
			"Alaskan Standard Time" => "Alaskan Standard Time",
			"Hawaiian Standard Time" => "Hawaiian Standard Time",
	);

	$ret = "<select name='$name' id='$name' class='form-control' ";

	$ret .= "> $first_option";

	foreach ( $timezones as $key => $val ) {
		$sel = '';
		if ( $selected == $key) {
			$sel = 'selected="selected"';
		}
		$ret .= "<option value='$key' $sel>$val</option>";
	}

	$ret .= '</select>';

	return $ret;
}

function getTimeZoneHtml( $name, $required = 'false', $selected = '', $first_option = '<option value="">Select Timezone</option>' ) {
	$timezones = array(
			"Hawaiian Standard Time" => "Hawaiian Standard Time",
			"Alaskan Standard Time" => "Alaskan Standard Time",
			"Pacific Standard Time" => "Pacific Standard Time",
			"US Mountain Standard Time" => "US Mountain Standard Time",
			"Mountain Standard Time" => "Mountain Standard Time",
			"Central Standard Time" => "Central Standard Time",
			"Canada Central Standard Time" => "Canada Central Standard Time",
			"Eastern Standard Time" => "Eastern Standard Time",
			"Atlantic Standard Time" => "Atlantic Standard Time",
			"Newfoundland Standard Time" => "Newfoundland Standard Time",
			"Dateline Standard Time" => "Dateline Standard Time",
			"UTC-11" => "UTC-11",
			"Pacific Standard Time (Mexico)" => "Pacific Standard Time (Mexico)",
			"Mountain Standard Time (Mexico)" => "Mountain Standard Time (Mexico)",
			"Central America Standard Time" => "Central America Standard Time",
			"Central Standard Time (Mexico)" => "Central Standard Time (Mexico)",
			"SA Pacific Standard Time" => "SA Pacific Standard Time",
			"Venezuela Standard Time" => "Venezuela Standard Time",
			"Paraguay Standard Time" => "Paraguay Standard Time",
			"Central Brazilian Standard Time" => "Central Brazilian Standard Time",
			"SA Western Standard Time" => "SA Western Standard Time",
			"Pacific SA Standard Time" => "Pacific SA Standard Time",
			"E. South America Standard Time" => "E. South America Standard Time",
			"Argentina Standard Time" => "Argentina Standard Time",
			"SA Eastern Standard Time" => "SA Eastern Standard Time",
			"Greenland Standard Time" => "Greenland Standard Time",
			"Montevideo Standard Time" => "Montevideo Standard Time",
			"Bahia Standard Time" => "Bahia Standard Time",
			"UTC-02" => "UTC-02",
			"Mid-Atlantic Standard Time" => "Mid-Atlantic Standard Time",
			"Azores Standard Time" => "Azores Standard Time",
			"Cape Verde Standard Time" => "Cape Verde Standard Time",
			"Morocco Standard Time" => "Morocco Standard Time",
			"UTC" => "UTC",
			"GMT Standard Time" => "GMT Standard Time",
			"Greenwich Standard Time" => "Greenwich Standard Time",
			"W. Europe Standard Time" => "W. Europe Standard Time",
			"Central Europe Standard Time" => "Central Europe Standard Time",
			"Romance Standard Time" => "Romance Standard Time",
			"Central European Standard Time" => "Central European Standard Time",
			"W. Central Africa Standard Time" => "W. Central Africa Standard Time",
			"Namibia Standard Time" => "Namibia Standard Time",
			"Jordan Standard Time" => "Jordan Standard Time",
			"GTB Standard Time" => "GTB Standard Time",
			"Middle East Standard Time" => "Middle East Standard Time",
			"Egypt Standard Time" => "Egypt Standard Time",
			"Syria Standard Time" => "Syria Standard Time",
			"E. Europe Standard Time" => "E. Europe Standard Time",
			"South Africa Standard Time" => "South Africa Standard Time",
			"FLE Standard Time" => "FLE Standard Time",
			"Turkey Standard Time" => "Turkey Standard Time",
			"Israel Standard Time" => "Israel Standard Time",
			"Arabic Standard Time" => "Arabic Standard Time",
			"Kaliningrad Standard Time" => "Kaliningrad Standard Time",
			"Arab Standard Time" => "Arab Standard Time",
			"E. Africa Standard Time" => "E. Africa Standard Time",
			"Iran Standard Time" => "Iran Standard Time",
			"Arabian Standard Time" => "Arabian Standard Time",
			"Azerbaijan Standard Time" => "Azerbaijan Standard Time",
			"Russian Standard Time" => "Russian Standard Time",
			"Mauritius Standard Time" => "Mauritius Standard Time",
			"Georgian Standard Time" => "Georgian Standard Time",
			"Caucasus Standard Time" => "Caucasus Standard Time",
			"Afghanistan Standard Time" => "Afghanistan Standard Time",
			"Pakistan Standard Time" => "Pakistan Standard Time",
			"West Asia Standard Time" => "West Asia Standard Time",
			"India Standard Time" => "India Standard Time",
			"Sri Lanka Standard Time" => "Sri Lanka Standard Time",
			"Nepal Standard Time" => "Nepal Standard Time",
			"Central Asia Standard Time" => "Central Asia Standard Time",
			"Bangladesh Standard Time" => "Bangladesh Standard Time",
			"Ekaterinburg Standard Time" => "Ekaterinburg Standard Time",
			"Myanmar Standard Time" => "Myanmar Standard Time",
			"SE Asia Standard Time" => "SE Asia Standard Time",
			"N. Central Asia Standard Time" => "N. Central Asia Standard Time",
			"China Standard Time" => "China Standard Time",
			"North Asia Standard Time" => "North Asia Standard Time",
			"Singapore Standard Time" => "Singapore Standard Time",
			"W. Australia Standard Time" => "W. Australia Standard Time",
			"Taipei Standard Time" => "Taipei Standard Time",
			"Ulaanbaatar Standard Time" => "Ulaanbaatar Standard Time",
			"North Asia East Standard Time" => "North Asia East Standard Time",
			"Tokyo Standard Time" => "Tokyo Standard Time",
			"Korea Standard Time" => "Korea Standard Time",
			"Cen. Australia Standard Time" => "Cen. Australia Standard Time",
			"AUS Central Standard Time" => "AUS Central Standard Time",
			"E. Australia Standard Time" => "E. Australia Standard Time",
			"AUS Eastern Standard Time" => "AUS Eastern Standard Time",
			"West Pacific Standard Time" => "West Pacific Standard Time",
			"Tasmania Standard Time" => "Tasmania Standard Time",
			"Yakutsk Standard Time" => "Yakutsk Standard Time",
			"Central Pacific Standard Time" => "Central Pacific Standard Time",
			"Vladivostok Standard Time" => "Vladivostok Standard Time",
			"New Zealand Standard Time" => "New Zealand Standard Time",
			"UTC+12" => "UTC+12",
			"Fiji Standard Time" => "Fiji Standard Time",
			"Magadan Standard Time" => "Magadan Standard Time",
			"Kamchatka Standard Time" => "Kamchatka Standard Time",
			"Tonga Standard Time" => "Tonga Standard Time",
			"Samoa Standard Time" => "Samoa Standard Time",
	);

	$ret = "<select name='$name' class='form-control' ";

	if ( $required == true ) {
		$ret .= 'required="required"';
	}

	$ret .= "> $first_option";

	foreach ( $timezones as $key => $val ) {
		$sel = '';
		if ( $selected == $key) {
			$sel = 'selected="selected"';
		}
		$ret .= "<option value='$key' $sel>$val</option>";
	}

	$ret .= '</select>';

	return $ret;
}

function date_difference_in_minutes($from_time, $to_time) {
	$to_time = strtotime( $to_time );
	$from_time = strtotime( $from_time );
	return round(abs($to_time - $from_time) / 60,2). " minute";
}

function check_date_relavence( $date ) {
	$current = strtotime(date("Y-m-d"));
	$date    = strtotime($date);

	$datediff = $date - $current;
	$difference = floor($datediff/(60*60*24));

	return $difference;

	//below is for reference
	if($difference==0) {
		//echo 'today';
	}
	else if($difference > 1) 	{
		//echo 'Future Date';
	}
	else if($difference > 0) 	{
		//echo 'tomarrow';
	}
	else if($difference < -1) 	{
		//echo 'Long Back';
	}
	else 	{
		//echo 'yesterday';
	}
}

function getWeekday($date) {
	return date('w', strtotime($date));
}

function getNumberToDay( $number ) {
	$day = "";
	switch ( $number ) {
		case '1':
			$day = "Monday";
			break;
		case '2':
			$day = "Tuesday";
			break;
		case '3':
			$day = "Wednesday";
			break;
		case '4':
			$day = "Thursday";
			break;
		case '5':
			$day = "Friday";
			break;
		case '6':
			$day = "Saturday";
			break;
		default:
			$day = "";
	}
	return $day;
}

function getNumberToTime( $number ) {
	$time = "";
	switch ( $number ) {
		case '0':
			$time = '12:00 AM';
			break;
		case '1':
			$time = '1:00 Am';
			break;
		case '2':
			$time = '2:00 AM';
			break;
		case '3':
			$time = '3:00 AM';
			break;
		case '4':
			$time = '4:00 AM';
			break;
		case '5':
			$time = '5:00 AM';
			break;
		case '6':
			$time = '6:00 AM';
			break;
		case '7':
			$time = '7:00 AM';
			break;
		case '8':
			$time = '8:00 AM';
			break;
		case '9':
			$time = '9:00 AM';
			break;
		case '10':
			$time = '10:00 AM';
			break;
		case '11':
			$time = '11:00 AM';
			break;
		case '12':
			$time = '12:00 PM';
			break;
		case '13':
			$time = '1:00 PM';
			break;
		case '14':
			$time = '2:00 PM';
			break;
		case '15':
			$time = '3:00 PM';
			break;
		case '16':
			$time = '4:00 PM';
			break;
		case '17':
			$time = '5:00 PM';
			break;
		case '18':
			$time = '6:00 PM';
			break;
		case '19':
			$time = '7:00 PM';
			break;
		case '20':
			$time = '8:00 PM';
			break;
		case '21':
			$time = '9:00 PM';
			break;
		case '22':
			$time = '10:00 PM';
			break;
		case '23':
			$time = '11:00 PM';
			break;
		default:
			$time = "";
			break;
	}
	return $time;
}


function number2time( $index ) {
	$time = "";

	switch ($index) {
		case '1':
			$time = '15 Minutes';
			break;

		case '2':
			$time = '30 Minutes';
			break;

		default:
			$time = "";
	}

	return $time;
}

function number2minutes( $index ) {
	$arr = array(
			'1' => '15',
			'2' => '30',
			'3' => '45',
			'4' => '60',
	);

	if ( array_key_exists($index, $arr) ) {
		return $arr[ $index ];
	}
	return '';
}

function trim_words($text, $num_words = 55, $more = null) {
	/*
	if (null === $more) {
		$more = __('&hellip;');
	}
	*/
	$text = html_entity_decode($text);

	$text = strip_tags($text);

	$words_array = preg_split("/[\n\r\t ]+/", $text, $num_words + 1, PREG_SPLIT_NO_EMPTY);
	$sep = ' ';


	if (count($words_array) > $num_words) {
		array_pop($words_array);
		$text = implode($sep, $words_array);
	} else {
		$text = implode($sep, $words_array);
	}

	$text = $text . $more;

	return $text;
}

function timeAgo($time_ago) {
	$cur_time = time();
	$time_elapsed = $cur_time - $time_ago;
	$seconds = $time_elapsed ;
	$minutes = round($time_elapsed / 60 );
	$hours = round($time_elapsed / 3600);
	$days = round($time_elapsed / 86400 );
	$weeks = round($time_elapsed / 604800);
	$months = round($time_elapsed / 2600640 );
	$years = round($time_elapsed / 31207680 );
	if($seconds <= 60){
		return  "$seconds seconds ago";
	}
	else if($minutes <=60){
		if($minutes==1){
			return  "one minute ago";
		}
		else{
			return  "$minutes minutes ago";
		}
	}
	else if($hours <=24){
		if($hours==1){
			return  "an hour ago";
		}else{
			return  "$hours hours ago";
		}
	}
	else if($days <= 7){
		if($days==1){
			return  "yesterday";
		}else{
			return  "$days days ago";
		}
	}
	else if($weeks <= 4.3){
		if($weeks==1){
			return  "a week ago";
		}else{
			return  "$weeks weeks ago";
		}
	}
	else if($months <=12){
		if($months==1){
			return  "a month ago";
		}else{
			return  "$months months ago";
		}
	}
	else{
		if($years==1){
			return  "one year ago";
		}else{
			return  "$years years ago";
		}
	}
}


// function to check type of logged in user i.e. if user is a doctor or patient
function checkUserSessionType ($userLevel) {
	if( isset($_SESSION["mvdoctorVisitorUserLevel"]) && $_SESSION["mvdoctorVisitorUserLevel"] == $userLevel){
		return true;
	}
	else
	{
		echo "<script>window.location='" .  HTTP_SERVER . "authorizationerror.html'</script>";
	}
}


//function to clean user input
function sanitize($input, $condition = 1) {
	if ($condition == 1) {
		return htmlentities($input, ENT_QUOTES, 'UTF-8');
	} elseif ($condition == 2) {
		$return = iconv('UTF-8', 'UTF-8//IGNORE', $input);
		return htmlentities($return, ENT_QUOTES, 'UTF-8');
	} elseif ($condition == 3) {
		$input = strip_tags($input);
		$input = html_entity_decode($input);
		$input = strip_tags($input);
		//Allow quotes for database entry
		$search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
				'@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
				'@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
				'@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
		);

		$input = preg_replace($search, '', $input);
		$input = strip_tags($input);

		return htmlentities($input);
	} elseif ($condition == 4) {
		$input = strip_tags( trim($input) );
		//Allow quotes for database entry
		$search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
				'@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
				'@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
				'@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
		);

		$input = preg_replace($search, '', $input);
		$input = strip_tags($input);

		return $input;
	}
}

function array_map_deep($array, $callback) {
	$new = array();
	if( is_array($array) ) foreach ($array as $key => $val) {
		if (is_array($val)) {
			$new[$key] = array_map_deep($val, $callback);
		} else {
			$new[$key] = call_user_func($callback, $val);
		}
	}
	else $new = call_user_func($callback, $array);
	return $new;
}

function getToken() {
	// create a form token to protect against CSRF
	if ( isset($_SESSION['ls_session']['token']) ) {
		return $_SESSION['ls_session']['token'];
	}
	$token = bin2hex(getRandomString(32));
	return $_SESSION['ls_session']['token'] = $token;
}

//Function to enforce encoding to avoid illegal characters
function enforceEncoding($input) {
	$input = mb_convert_encoding($input, mb_internal_encoding(), 'UTF-8');
	return $input;
}

//function to redirect page
function redirectPage($target = 'Same') {
	if($target!=""){
		if($target == 'Same'){
			echo "<script>window.location='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."'</script>";
			exit();
		}else{
			echo "<script>window.location='$target'</script>";
			exit();
		}
	}
}

//Pre tag echo for arrays
function echoPre($array) {
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}

//function for sort order
function setSortOrder($table, $sortField, $sortOrderExisting, $sortOrderTarget, $sortParentField = '', $sortParent = 0){
	//if both values arent the same then proceed
	if($sortOrderExisting!=$sortOrderTarget) {
		global $db;
		$arr = array();
		$sql = "";
		
		//checking sql whether the target sort order exists in database or not 
		$sql = "SELECT * FROM $table WHERE $sortField=?";
		$arr[] = $sortOrderTarget;

		if($sortParentField != ""){
			$sql .= " AND $sortParentField = ?";
			$arr[] = $sortParent;
		}

		try {
			$result = $db->prepare( $sql );
			$result->execute( $arr );
		} catch (Exception $Exception) {
			exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
		}


		//if target sort order exists then proceed
		if ( $result->rowCount() > 0 ) {
			$sql = "";
			$arr = array();

			//conditions depending on the sort order being greater or less than target sort order
			if ( $sortOrderExisting > $sortOrderTarget ) {
				$sortField = $sortField+1;
			} else {
				$sortField = $sortField-1;
			}

			$sql = "UPDATE $table SET $sortField= ? WHERE $sortField <= ? and $sortField > ?";
			$arr[] = $sortField;
			$arr[] = $sortOrderTarget;
			$arr[] = $sortOrderExisting;
			
			if( $sortParentField != "" ) {
				$sql .= " AND $sortParentField = ?";
				$arr[] = $sortParent;
			}

			try {
				$result2 = $db->prepare( $sql );
				$result2->execute( $arr );
			} catch (Exception $Exception) {
				exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
			}
		}
		
	}
	
}

//generate coupon iamge
function generateCouponImage($couponUserId, $couponCode, $couponExpiry){
	//declate global var
	global $siteRoot;
	//GD Functions
	$NewImage =imagecreatefromjpeg($siteRoot . "images/coupon.jpg");
	$TextColor = imagecolorallocate($NewImage, 000, 000, 000);
	imagestring($NewImage, 3, 110, 238, $couponCode, $TextColor);
	imagestring($NewImage, 3, 94, 194, $couponExpiry, $TextColor);
	imagejpeg($NewImage, $siteRoot . 'images/coupons/coupon-'.$couponUserId.'.jpg',100);
}

//generate a random string
function getRandomString($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $string = '';    

    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters)-1)];
    }

    return strtoupper($string);
}

//function fast insert and update
function fastInsertUpdate($act = "add", $databaseTable, $postArray, $excludeArray = "", $sqlVars = "", $echoSql = 0){
	//vars
	$insertCounter = 0;
	//if array
	if ( is_array( $postArray ) ) {
		//Insert Block
		if ( $act == "add" ) {
			$sqlInsert1 = "INSERT INTO $databaseTable (";
			$sqlInsert2 = " (";
			$arr = array();
			
			foreach ($postArray as $key=>$value) {
				//exclude array
				$keyAllow = 1;
				if ( is_array( $excludeArray ) && in_array($key, $excludeArray) ) {
					$keyAllow = 0;
				}		
				
				//if key passes through exclude check
				if ( $keyAllow == 1 ) {
					$insertCounter++;
					if($insertCounter!=1){
						$sqlInsert1 .= ",";
						$sqlInsert2 .= ",";
					}			
					//assign to sql strings
					$sqlInsert1 .= "$key";
					$sqlInsert2 .= '?';
					$arr[] = sanitize($value);
				}
						
			}
			
			$sqlInsert1 .= ")";
			$sqlInsert2 .= ")";
			
			
			
			$sqlInsert = $sqlInsert1." values ".$sqlInsert2;			
			
			if($echoSql == 1){
				echo "SQL IS: ".$sqlInsert;
			}

			try {
				global $db;
				$result = $db->prepare( $sqlInsert );
				$result->execute($arr);
				return array('status' => 'success', 'id' => $db->lastInsertId());

			} catch (Exception $Exception) {
				return array('status' => 'error', 'error' => 'SQL ERROR: '.$Exception->getMessage( ).'<br />SQL WAS: '.$sqlInsert);
			}
			
		//Update Block	
		} else {

			$sqlUpdate = "UPDATE $databaseTable SET ";
			$arr = array();
			
			$updateCounter=0;
			foreach ($postArray as $key=>$value){				
				//exclude array
				$keyAllow = 1;
				if ( is_array( $excludeArray ) && in_array( $key, $excludeArray ) ) {
					$keyAllow = 0;
				}		
				
				//if key allow
				if($keyAllow == 1){
					$updateCounter++;
					//add comma
					if($updateCounter>1){
						$sqlUpdate .= ", ";
					}
					
					$sqlUpdate .= "$key = ?";
					$arr[] = sanitize($value);
				}
				
			}

			if ( is_array($sqlVars) ) {
				$sqlUpdate .= ' WHERE ';
				foreach ($sqlVars as $sqlVarKey => $sqlVarValue) {
					$sqlUpdate .= "$sqlVarKey = ?";
					$arr[] = $sqlVarValue;
				}
			} else {
				$sqlUpdate .= $sqlVars;
			}
			

			
			if($echoSql == 1){
				echo "SQL IS: ".$sqlUpdate;
			}

			try {
				global $db;
				$result = $db->prepare( $sqlUpdate );
				$result->execute($arr);
				return array('status' => 'success');

			} catch (Exception $Exception) {
				return array('status' => 'error', 'error' => 'SQL ERROR: '.$Exception->getMessage( ).'<br />SQL WAS: '.$sqlUpdate);
			}
		}
	} else {
		return array('status' => 'error', 'error' => 'The POST is not an array!');
	}
}

//getField function to get fields from database directly
function getField($table, $leftField, $rightField, $getField, $sqlVars = "", $returnThis = '', $echoSql = 0) {
	
	if(!strstr($rightField, "'")){
		$rightField = "'".$rightField."'";	
	}
	
	$sql = "SELECT $getField from $table where $leftField = ? $sqlVars";
	
	//if sql echo
	if($echoSql == 1){
		return $sql;		
	}

	global $db;


	try {
		$result = $db->prepare($sql);
		$result->execute(array($rightField));

		if ( $result->rowCount() > 0 ) {
			$data = $result->fetch();
			return $data[ $getField ];
		} else {
			return "";
		}
	} catch (Exception $Exception) {

		if($returnThis!=''){
			exit( "<strong>MYSQL ERROR</strong>:<br /><strong>SQL QUERY:</strong> $sql<br /><strong>SQL ERROR:</strong> ".$Exception->getMessage( ) );
		}else{
			return $returnThis;
		}
	}
	
}

//format date
function formatDate ($date, $format = '1'){
	if($format === '1'){	//return US date m/d/Y
		$date = explode("-",$date);
		//add 0 before month and date if < 10
		if($date[1]<10 and strlen($date[1])<2){
			$date[1] = "0".$date[1];
		}
		if($date[2]<10 and strlen($date[2])<2){
			$date[2] = "0".$date[2];
		}
		$return = $date[1]."/".$date[2]."/".$date[0];
	}elseif ($format == '2'){	//return mysql format date Y-m-d
		$date = explode("/",$date);
		//add 0 before month and date if < 10
		if($date[1]<10 and strlen($date[1])<2){
			$date[1] = "0".$date[1];
		}
		if($date[0]<10 and strlen($date[0])<2){
			$date[0] = "0".$date[0];
		}
		$return = $date[2]."-".$date[0]."-".$date[1];
	}
	
	return $return;
}

//timeConversion 12 to 24 and 24 to 12
function changeTime($timeString, $timeFormat = 12) {
	if($timeFormat == 12 or $timeFormat == '12'){
		return date("g:i a", STRTOTIME($timeString));
	}else{
		return date("H:i:s", STRTOTIME($timeString));
	}
}


//function to get rights for a level / role
function getLevelFunctions($levelId, $functionModule){
	global $db;
	$arr = array();

	$sql = "SELECT functionValue FROM ".DB_PREFIX."users_levels_functions where functionModule = ? and levelId = ?";
	$arr[] = $functionModule;
	$arr[] = $levelId;

	try {
		$result = $db->prepare($sql);
		$result->execute($arr);

		if ( $result->rowCount() > 0 ) {
			$data = $result->fetch();
			return $data['functionValue'];
		} else {
			return 0;
		}
	} catch (Exception $Exception) {
		exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
	}
}

//function to change font color for negative values
function negativeClass($fieldValue, $fieldType = 'number'){
	
	if($fieldType == 'number'){	
		if($fieldValue>-1){
	 	   	return "smallTextboxWhite";
	    }else{
	    	return "smallTextboxWhiteRedFont";
		}
	}else{
		if($fieldValue<=100){
	 	   	return "smallTextboxWhite";
	    }else{
	    	return "smallTextboxWhiteRedFont";
		}
	}
}

//get all selectbox values and show options
function generateSelect($selectName, $selectSql, $optionId, $optionName, $selectVars = "", $showBoth = "no", $selectMessage = "", $selectedValue = ""){
	
	if($selectVars != ""){
		$return = '<select name="'.$selectName.'" id="'.$selectName.'" '.$selectVars.'>';	
	}else{
		$return = '<select name="'.$selectName.'" id="'.$selectName.'">';	
	}
	
	//if select message
	if($selectMessage!=""){
		$return .= '<option value="">'.$selectMessage.'</option>';
	}

	global $db;

	$result = $db->prepare( $selectSql );
	$result->execute();
	
	
	if ( $result->rowCount() > 0 ) {

		foreach ( $result->fetchAll() as $rs ) {
			
			$field1 = $rs[ $optionId ];
			
			//if want to show multiple fields
			$optionNameExplode = explode("|",$optionName);
					
			if ( count( $optionNameExplode ) > 1 ) {
				$field2 = "";
				$fieldCounter=0;
				foreach ($optionNameExplode as $optionNameE){
					$fieldCounter++;
					if($fieldCounter>1){
						$field2 .= " ";
					}
					if($optionNameE!=""){
						$field2 .= html_entity_decode($rs["$optionNameE"]);	
					}
				}
			}else{
				$field2 = html_entity_decode($rs["$optionName"]);	
			}			
			
			//if selected			
			if($selectedValue == $field1 or $selectedValue == $field2){
				$sel = "selected";
			}else{
				$sel = "";
			}
			
			if($showBoth == "yes"){
				$return .= '<option '.$sel.' value="'.$field1.'">('.$field1.') '.$field2.'</option>';
			}else{
				$return .= '<option '.$sel.' value="'.$field1.'">'.$field2.'</option>';
			}
		}
	}
	
	//if project manager
	if($selectName == "jobProjectManager"){
		$return .= "<option "; 
		if($selectedValue == "nopm"){
			$return .= "selected ";
		}
		$return .= "value = \"nopm\">--- No Project Manager</option>";
	}
	
	$return .= '</select>';
	
	return $return;
}

//format number
function formatNumber ($number, $decimal = 0, $changeColor=0){
	
	$return = number_format($number,$decimal,".","");
	
	if($changeColor == 1){ //negative values
		if($return<0){
			return "<font color='#C00000'>$return</font>";
		}else{
			return $return;
		}
	}elseif ($changeColor == 2){	//com% if greater than 100
		if($return>100){
			return "<font color='#C00000'>$return</font>";
		}else{
			return $return;
		}
	}else{
		return $return;
	}
	
	
}


//getMax function to get max field from database
function getMaxField($table, $getField, $sqlVars = "") {

	global $db;

	$sql = "SELECT max($getField) as maxField from $table $sqlVars";

	try {
		$result = $db->prepare($sql);
		$result->execute();

		if ( $result->rowCount() > 0 ) {
			$data = $result->fetch();
			return $data['maxField'];
		} else {
			return "";
		}

	} catch (Exception $Exception) {
		exit( "<strong>MYSQL ERROR</strong>:<br /><strong>SQL QUERY:</strong> $sql<br /><strong>SQL ERROR:</strong> ".$Exception->getMessage( ) );
	}
}

//getMax function to get max field from database
function getMinField($table, $getField, $sqlVars = "") {
	global $db;
	$sql = "SELECT min($getField) as minField from $table $sqlVars";

	try {
		$result = $db->prepare($sql);
		$result->execute();

		if ( $result->rowCount() > 0 ) {
			$data = $result->fetch();
			return $data['minField'];
		} else {
			return "";
		}

	} catch (Exception $Exception) {
		exit( "<strong>MYSQL ERROR</strong>:<br /><strong>SQL QUERY:</strong> $sql<br /><strong>SQL ERROR:</strong> ".$Exception->getMessage( ) );
	}
}

//getSum function to get sum field from database
function getSumField($table, $getField, $sqlVars = "") {
	global $db;
	$sql = "SELECT sum($getField) as sumField from $table $sqlVars";

	try {
		$result = $db->prepare($sql);
		$result->execute();

		if ( $result->rowCount() > 0 ) {
			$data = $result->fetch();
			return $data['sumField'];
		} else {
			return "";
		}

	} catch (Exception $Exception) {
		exit( "<strong>MYSQL ERROR</strong>:<br /><strong>SQL QUERY:</strong> $sql<br /><strong>SQL ERROR:</strong> ".$Exception->getMessage( ) );
	}
}


//getCount function to get count field from database
function getCountField($table, $getField, $sqlVars = "", $echoSql = 0){
	global $db;
	$sql = "SELECT count($getField) as countField from $table $sqlVars";
	
	if($echoSql == 1){
		echo $sql;
		exit();
	}

	try {
		$result = $db->prepare($sql);
		$result->execute();

		if ( $result->rowCount() > 0 ) {
			$data = $result->fetch();
			return $data['countField'];
		} else {
			return "";
		}

	} catch (Exception $Exception) {
		exit( "<strong>MYSQL ERROR</strong>:<br /><strong>SQL QUERY:</strong> $sql<br /><strong>SQL ERROR:</strong> ".$Exception->getMessage( ) );
	}
	
}

//file upload function
function uploadFile ($file, $dir, $typeArray = 'image', $renameTo = ''){

	//max size
	$maxSize = 10485760;
	
	//file variables
	$fileType = strtolower($file["type"]);
	$fileSize = $file["size"];	
	
	//file extention
	$fileExtention = explode(".",$file["name"]);
	$fileExtentionCount = count($fileExtention);	
	$fileExtention = strtolower($fileExtention[$fileExtentionCount-1]);
	
	if($renameTo!=''){
		$fileName = $renameTo.".".$fileExtention;
	}else{
		$fileName = $file["name"];
	}
	
	//type check
	if($typeArray == 'image'){
		if($fileExtention != "jpg" and $fileExtention != "png" and $fileExtention != "gif" and $fileExtention != "jpeg"){
			return "error|The file must be a valid JPG, PNG or GIF image.";
		}
	}elseif ($typeArray == "file"){
		if($fileExtention != "txt" and $fileExtention != "doc" and $fileExtention != "rtf" and $fileExtention != "pdf"){
			return "error|The file must be a valid TXT, DOC, RTF or PDF image.";
		}
	}

	//size check
	if($fileSize>$maxSize){
		return "error|The file size must not be larger than 10MB (10485760 KB)";
	}
	
	//other errors
	if($file["error"] > 0){
		return "error|". $file["error"];
	}
	
	//uploading file
	if(move_uploaded_file($file["tmp_name"],$dir.$fileName)){
		return "success|".$fileName;	      
	}else{
		return "error|File upload error, please try again";
	}
		
}



//function to add 2 decimals
function roundNumber ($numberString){
	$newStr = explode(".",$numberString);	
	if($newStr[1]!=""){
		if(substr($newStr[1],1,1) == ""){
			$digitsAfterPoint = substr($newStr[1],0,1)."0";
		}else{
			$digitsAfterPoint = substr($newStr[1],0,2);
		}
		return $newStr[0].".".$digitsAfterPoint;	   		
	}else{
		return $newStr[0];
	}
}


//file upload function
function uploadMedia($upload_dir,$pic,$oldfile="",$newName=""){
           
$extlimit = "yes";
$limitedext = array(".pdf",".swf", ".docx", ".doc", ".txt", ".xls", ".xlsx", ".jpg", ".gif", ".png", ".ttf");
  
  if (!is_dir("$upload_dir")) { 
 	$error="The directory <b>($upload_dir)</b> doesn't exist"; 
 	return "error|".$error;
  } 
  if (!is_writeable("$upload_dir")){ 
     $error="The directory <b>($upload_dir)</b> is NOT writable, Please CHMOD (777)"; 
     return "error|".$error;
  }
  if($oldfile!=""){
  	$file =  $oldfile; 
    if(file_exists($upload_dir.$file)){ 
		unlink($upload_dir.$file);				
    }
  }
            
  $ext = strrchr($pic['name'],'.'); 
  $ext = strtolower($ext);
  if (($extlimit == "yes") && (!in_array($ext,$limitedext))) { 
        $error="Format not recognized. Please upload a PDF File. Click <a href='javascript:history.back()'>here</a> to go back";                    
        return "error|".$error;
  } 
  $filename =  $pic['name']; 
  $fileExplode=explode(".",$filename);
  $fileExt=$fileExplode[1];
  
  if($newName!=""){
  	$filename=$newName.".".$fileExt;
  }
  
  if(file_exists($upload_dir.$filename)){ 
       unlink($upload_dir.$filename);
  }  
  
  if (move_uploaded_file($pic['tmp_name'],$upload_dir.$filename)) {
	  chmod($upload_dir.$filename, 0644);
	  return "success|".$filename;
  }else{ 
      $error="There was a problem saving your file, please try again!";                    
      return "error|".$error;
  }
}

/* function uploadDoctorPicture( $image_field = 'featured_image' ) {

	$error_msg = '';
	$image_path = SITE_ROOT_HOME . 'images/doctors/full/';
	if($_FILES[$image_field]['name']) {
		$MyIMage = new upload($_FILES[$image_field]);
		if ($MyIMage->uploaded) {
			//process full image
			$MyIMage->image_resize = false;
			$MyIMage->process($image_path);
			$return_name = $MyIMage->file_dst_name;
			if (!$MyIMage->processed) {
				$error_msg .= ' - Error : ' . $MyIMage->error;
			}
			//clean
			$MyIMage->clean();

		} else {
			$error_msg .= " - There was a problem uploading one of your images.";
		}
	}else{
		$error_msg .= " - Image not found";
	}

	//if error
	if ( $error_msg != '' ) {
		return "error|$error_msg";
	} else {
		return "success|$return_name";
	}
} */

function uploadDoctorPicture( $image_field = 'featured_image' ) {
	require_once SITE_ROOT . 'includes/classes/class.upload.php';
	$error_msg = '';
	$image_path = SITE_ROOT . 'images/doctors/full/';	
	
	if($_FILES[$image_field]['name']) {
		$MyIMage = new upload($_FILES[$image_field]);
		if ($MyIMage->uploaded) {
			//process full image
			$MyIMage->image_resize = false;
			$MyIMage->process($image_path);
			$return_name = $MyIMage->file_dst_name;
			if (!$MyIMage->processed) {
				$error_msg .= ' - Error : ' . $MyIMage->error;
			}
			//clean
			$MyIMage->clean();

		} else {
			$error_msg .= " - There was a problem uploading one of your images.";
		}
	}else{
		$error_msg .= " - Image not found";
	}

	//if error
	if ( $error_msg != '' ) {		return array('status' => 'error', 'data' => $error_msg);	} else {		return array('status' => 'success', 'data' => array('img1' => $return_name));	}
}

function uploadUserImage( $image_field = 'user_image' ) {
	require_once SITE_ROOT . 'includes/classes/class.upload.php';
	$error_msg = '';
	$image_path = SITE_ROOT . 'images/user/full/';
	$image_path2 = SITE_ROOT . 'images/user/thumb/';

	if($_FILES[$image_field]['name']) {
		$MyIMage = new upload($_FILES[$image_field]);
		if ($MyIMage->uploaded) {
			//process full image
			$MyIMage->image_resize = false;
			$MyIMage->process($image_path);
			$return_name1 = $MyIMage->file_dst_name;
			if (!$MyIMage->processed) {
				$error_msg .= ' - Error : ' . $MyIMage->error;
			}

			//process expanded image
			$MyIMage->image_resize = true;
			$MyIMage->image_x = 87;
			$MyIMage->image_y = 87;

			$MyIMage->process($image_path2);
			$return_name2 = $MyIMage->file_dst_name;
			if (!$MyIMage->processed) {
				$error_msg .= ' - Error (Thumbnail) : ' . $MyIMage->error;
			}


			//clean
			$MyIMage->clean();

		} else {
			$error_msg .= " - There was a problem uploading one of your images.";
		}
	} else {
		$error_msg .= " - Image not found";
	}

	//if error
	if ( $error_msg != '' ) {
		return array('status' => 'error', 'data' => $error_msg);
	} else {
		return array('status' => 'success', 'data' => array('img1' => $return_name1, 'img2' => $return_name2));
	}
}
//upload and resize
function uploadAndResize($image_path, $image_field, $image_width_x, $image_width_y, $image_thumb_path = "", $image_thumb_x = "", $image_thumb_y = "", $image_save_original = 0, $image_path_original = '', $image_thumb_y_force = 0){
	
	$error_msg = '';	
	if($_FILES[$image_field]['name']) {
		$MyIMage = new upload($_FILES[$image_field]);
		if ($MyIMage->uploaded) {
			
			if($image_width_x!="" and $image_width_y!=""){			
				//process expanded image
				$MyIMage->image_resize         = true;
				$MyIMage->image_x              = $image_width_x;
				$MyIMage->image_y        	   = $image_width_y;
				$MyIMage->process($image_path);
				$return_name = $MyIMage->file_dst_name;
				if (!$MyIMage->processed) {
				  $error_msg .= ' - Error : ' . $MyIMage->error;
				}	
			
			}
			
			if($image_thumb_x!="" and $image_thumb_y!=""){			
				//process expanded image
				$MyIMage->image_resize         = true;
				$MyIMage->image_x              = $image_thumb_x;
				$MyIMage->image_y        	= $image_thumb_y;
				if($image_thumb_y_force == 1){
					$MyIMage->image_ratio_y = false;
				}
				$MyIMage->process($image_thumb_path);
				
				if (!$MyIMage->processed) {
				  $error_msg .= ' - Error (Thumbnail) : ' . $MyIMage->error;
				}	
			
			}
			
			if($image_save_original == 1 and $image_path_original!=''){				
				//process thumb image
				$MyIMage->image_resize = false;
				$MyIMage->process($image_path_original);					
				$return_name = $MyIMage->file_dst_name;
				if (!$MyIMage->processed) {
				  $error_msg .= ' - Error : ' . $MyIMage->error;
				}				
			}
			
			//clean
			$MyIMage->clean();
		} else {
			$error_msg .= " - There was a problem uploading one of your images.";
		}	
	}else{
		$error_msg .= " - Image not found";
	}
	
	//if error
	if($error_msg!=''){
		return "error|$error_msg";
	}else{
		return "success|$return_name";
	}
}

function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
	$url = 'http://www.gravatar.com/avatar/';
	$url .= md5( strtolower( trim( $email ) ) );
	$url .= "?s=$s&d=$d&r=$r";
	if ( $img ) {
		$url = '<img src="' . $url . '"';
		foreach ( $atts as $key => $val )
			$url .= ' ' . $key . '="' . $val . '"';
		$url .= ' />';
	}
	return $url;
}