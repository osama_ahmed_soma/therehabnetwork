<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

try {
    $db = new PDO("mysql:host=$databaseServer;dbname=$databaseName;charset=utf8", $databaseUser, $databasePassword);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $db->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'utf8'");
    $db->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET @@SESSION.sql_mode='TRADITIONAL,NO_AUTO_VALUE_ON_ZERO'");
} catch (PDOException $Exception) {
    exit ( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}