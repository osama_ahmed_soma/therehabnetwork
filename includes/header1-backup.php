<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
?>
<header>
	
    <nav class="navbar mg-bt-none navbar-default home-menu" id="navbar-main">
    			<nav class="top-menu">
                    <div class="login-hm-btn-sec">
                    	<ul>
                            <li><a href="#">For Providers</a>
                            	<ul>
									<?php if (!isset($_SESSION["mvdoctorVisitorUserLevel"])) {?>
                                    <li><a href="<?php echo HTTP_SERVER;?>doctor-login.html">Login</a> </li>
                                    <?php } ?>
                                    <li><a href="<?php echo HTTP_SERVER;?>our-platform.html">Our Platform</a> </li>
                                    <li><a href="<?php echo HTTP_SERVER;?>request-a-demo.html">Request Demo</a> </li>
                                </ul>
                            </li>
                            <?php if (!isset($_SESSION["mvdoctorVisitorUserLevel"])) {?>
                            <li><a href="<?php echo HTTP_SERVER; ?>login.html" type="button" class="btn btn-info sing">Login</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </nav>
                <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-2">
                    <ul class="nav navbar-nav navbar-right top-nav">
                        <li><a href="#">For Doctors</a> </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-brand" href="<?php echo HTTP_SERVER; ?>"><img src="<?php echo HTTP_SERVER; ?>img/logo.png" width="250px" class="img-responsive"></a> </div>
					<div class="tagline-logo">Your Choice. Your	Health. Your Way.</div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <?php if ( !isset($_SESSION['mvdoctorVisitornUserId']) ):  ?>
                        <div class="mt-7 ml-10 navbar-right home-page-sign-up"><a href="<?php echo HTTP_SERVER . 'signup.html'; ?>" class="btn btn-info sing">Signup</a></div>
                    <?php endif; ?>

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo HTTP_SERVER; ?>">Home</a> </li>
						<li><a href="<?php echo HTTP_SERVER; ?>our-platform.html">How it Works </a> </li>
                        <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">About   <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php /*?><li><a href="<?php echo HTTP_SERVER;?>how-it-works.html">How It Works</a> </li><?php */?>
                                <li><a href="<?php echo HTTP_SERVER;?>our-mission.html">Our Mission </a> </li>
                                <!--<li><a href="<?php echo HTTP_SERVER ?>partners.html">Our Partners</a></li>-->
                                <li><a href="<?php echo HTTP_SERVER; ?>contact-us.html">Contact </a> </li>
                            </ul>

                        </li>
						<li><a href="<?php echo HTTP_SERVER; ?>blog.html">Blog</a> </li>

                        <?php /*?><li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">For Doctors   <span class="caret"></span></a>
                             <ul class="dropdown-menu" role="menu">
								<?php if (!isset($_SESSION["mvdoctorVisitorUserLevel"])) {?>
                                <li><a href="<?php echo HTTP_SERVER;?>doctor-login.html">Login</a> </li>
								<?php } ?>
                                <li><a href="<?php echo HTTP_SERVER;?>our-platform.html">Our Platform</a> </li>
                                <li><a href="<?php echo HTTP_SERVER;?>request-a-demo.html">Request Demo</a> </li>

                            </ul>
                        </li><?php */?>

                        <?php if ( isset($_SESSION['mvdoctorVisitornUserId']) ):  ?>
                           <li class="dropdown user-login-btn">
                            <a href="#" class="dropdown-toggle btn btn-default btn-user" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <?php echo $_SESSION["mvdoctorVisitorUserFname"] . ' ' . $_SESSION["mvdoctorVisitorUserLname"]; ?> <span><i class="fa active-setting fa-caret-down"></i></span>
                            </a>
                            <ul class="dropdown-menu user-login-menu">
                                <?php
                                $dashboard_url = ( $_SESSION["mvdoctorVisitorUserLevel"] == 4) ? 'patient.html' : 'doctors/dashboard.html';
                                $settings_url = ( $_SESSION["mvdoctorVisitorUserLevel"] == 4) ? 'patient/profile-settings.html' : 'doctors/general-information.html';
                                ?>
                                <li><a href="<?php echo HTTP_SERVER . $dashboard_url; ?>" class=""><span><i class="fa fa-unlock-alt"></i></span> Dashboard</a></li>
                                <li><a href="<?php echo HTTP_SERVER . $settings_url; ?>" class=""><span><i class="fa fa-unlock-alt"></i></span> Profile Setting</a></li>
                                <li><a href="<?php echo HTTP_SERVER; ?>logout.html" class=""><span><i class="fa fa-unlock-alt"></i></span> Log out</a></li>
                            </ul>
                             <li class="hidden">
                                    <div class="navbar-login hiddden">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <p class="text-center">
                                                    <img src="<?php
                                                    if ( isset($_SESSION["mvdoctorVisitoruserPicture"]) && $_SESSION["mvdoctorVisitoruserPicture"] != '' ) {
                                                        echo HTTP_SERVER . 'images/user/thumb/' . $_SESSION["mvdoctorVisitoruserPicture"];
                                                    } else {
                                                        echo get_gravatar($_SESSION["mvdoctorVisitorUserEmail"],87);
                                                    } ?>" class="icon-size" />
                                                </p>
                                            </div>
                                            <div class="col-lg-8">
                                                <p class="text-left"><strong><?php echo $_SESSION["mvdoctorVisitorUserTitle"]; ?></strong></p>
                                                <p class="text-left small"><?php echo $_SESSION["mvdoctorVisitorUserEmail"]; ?></p>
                                                <p class="text-left">
                                                    <a href="<?php echo HTTP_SERVER; ?>patient/settings.html" class="btn btn-default btn-user btn-block btn-sm">Profile Setting</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider hidden"></li>
                                 <li class="hidden">
                                    <div class="navbar-login navbar-login-session">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p>
                                                    <a href="<?php echo HTTP_SERVER; ?>logout.html" class="btn btn-default dark-pink btn-block">Log out</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                        </li>
                        <?php endif; ?>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
        <!-- /.container-fluid -->
    </nav>
</header>
<?php if ( isset( $_SESSION["mvdoctorVisitorUserLevel"] ) && intval( $_SESSION["mvdoctorVisitorUserLevel"] ) === 4 && $do == 'search' ):  ?>
 <section class="dashboard-new-section clearfix">
    <div class="container">
        <div class="row">
            <div class="nav-pills">
                    <ul>
                        <div class="col-md-2 col-sm-2 col-xs-4 dns-icon <?php echo $sub_page == '' || $sub_page == 'dashboard' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-calendar"></i> Dashboard</a></div>
                        <div class="col-md-2 col-sm-2 col-xs-4 dns-icon <?php echo $sub_page == 'health' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/health.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-heart"></i> My Health</a></div>
                        <div class="col-md-2 col-sm-2 col-xs-4 dns-icon  <?php echo $sub_page == 'providers' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/providers.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-star"></i>  My Providers</a>

                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-4 dns-icon  <?php echo $sub_page == 'pinned-articles' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/pinned-articles.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-paperclip"></i>  Pinned Articles</a></div>
                        <div class="col-md-2 col-sm-2 col-xs-4 dns-icon  <?php echo $sub_page == 'profile-settings' || $sub_page == 'change-password' || $sub_page == 'notification-settings' ? 'active' : ''; ?>"><a href="#vtab1" data-toggle="tab"> <i class="fa fa-gears"></i> Account Settings</a>
                            <ul>
                                <li class="<?php echo $sub_page == 'profile-settings' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/profile-settings.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-gears"></i> Profile</a></li>
                                <li class="<?php echo $sub_page == 'change-password' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/change-password.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-gears"></i>  Change Password</a></li>
                                <li class="<?php echo $sub_page == 'notification-settings' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/notification-settings.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-gears"></i>  Notifications</a></li>
                            </ul>

                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-4 dns-icon  <?php echo $sub_page == 'support' ? 'active' : ''; ?>"><a href="<?php echo HTTP_SERVER . 'patient/support.html' ?>" data-toggle="tab" class="main_tab"> <i class="fa fa-users"></i> Help and Support</a></div>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">

        jQuery(function ($){
            $('a.main_tab').on('click', function (e) {
                window.location = $(this).attr('href'); // newly activated tab
                //e.relatedTarget // previous active tab
                return false;
            })
        });

    </script>
<?php endif; ?>
