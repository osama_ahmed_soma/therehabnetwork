<?php
/* -------------------------------------------------
// Filename: config.php
// Purpose: application config
//----------------------------------------------- */

//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}
		

// Define the webserver and path parameters
define("HTTP_SERVER", $httpServer);
define('HTTPS_SERVER', $siteRoot); // eg, https://localhost - should not be empty for productive servers

define('TEST_MODE', false);

// Site name
define('SITE_NAME', 'My Virtual Doctor');
define('SITE_COMPANY', 'My Virtual Doctor');
define('SITE_ADDRESS', '4126 Havenridge Dr. ');
define('SITE_WEBURL', 'http://projects.dev/myvirtualdoctor/');
define('SITE_ADDRESS_2', '');
define('SITE_LOCALITY', 'Corona');
define('SITE_REGION', 'California');
define('SITE_REGION_ABB', 'CA');
define('SITE_POSTAL_CODE', '94283');
define('SITE_COUNTRY', 'USA');
define('SITE_PHONE', '(951) 454-1346');
define('SITE_FAX', '');
define('SITE_KEY', 't!*LD[d&S)3LK|:opSI GMdV;J=P/=W{7bu=5ZTzk+iB}Jbh27/Kd(:[kI.x ,+U\')');

// * DIR_FS_* = Filesystem directories (local/physical)
define('SITE_ROOT', $siteRoot);

// define our database connection
define('DB_PREFIX', 'mvd101_');

//Contact email
define("CONTACT_NAME", "My Virtual Doctor");
define("CONTACT_EMAIL", "admin@myvirtualdoctor.com");