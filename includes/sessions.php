<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}

//if user is logged in
if($do != 'home' and $do != "password-reset" and $do != "" and $do != "error404" and $do != "ajax" and $do != "doctor" and $do != 'login'
	and $do != "facebook" and $do != "partners" and $do != 'how-it-works' and $do != 'blog' and $do != "search1"
	and $do != 'signup' and $do != 'request-a-demo' and $do != 'contact-us' and $do != 'our-platform' and $do != 'authorizationerror'
	and $do != 'doctor-signup' and $do != 'doctor-login' and $do != 'our-mission' and $do != 'payment_form' and $do != 'resources' and $do != 'doctor-dashboard-new' and $do != 'patient-dashboard' and $do != 'patient-search'  and $do != 'signup-new' and $do != 'home-old' and $do != 'faqs' and $do != 'privacypolicy' and $do != 'termandconditions' and $do != 'providers' and $do != 'ROI-calculator' and $do != 'become-a-provider') {

	if( !isset($_SESSION["mvdoctorVisitornUserId"]) ) {

		echo "<script>window.location='" .  HTTP_SERVER . "authorizationerror.html'</script>";

	} else if ( isset($_SESSION["mvdoctorVisitornUserId"]) ) {

		//this is for doctor pages security
		if ( ($do == 'doctors' ) && !isset($_SESSION["mvdoctorID"]) && intval($_SESSION["mvdoctorVisitorUserLevel"]) !== 5 ) {
			echo "<script>window.location='" .  HTTP_SERVER . "authorizationerror.html'</script>";
		} elseif ( $do == 'patient' && !isset($_SESSION["mvdoctorID"]) &&  intval($_SESSION["mvdoctorVisitorUserLevel"]) !== 4 ) {
			echo "<script>window.location='" .  HTTP_SERVER . "authorizationerror.html'</script>";
		}
	}
}