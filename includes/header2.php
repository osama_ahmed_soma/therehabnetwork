<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

//get category data
$speciality_data = array();
$speciality_special_data = array();
$location_data = array();

$search_speciality = "";
$search_location = "";
if ( isset($_GET['speciality']) && $_GET['speciality'] != '' ) {
    $search_speciality = intval( $_GET['speciality'] );
}

if ( isset($_GET['location']) && $_GET['location'] != '' ) {
    $search_location = intval( $_GET['location'] );
}
/*
try {
    $query = "SELECT id, categoryName, categoryType, categoryFeatured FROM " . DB_PREFIX . "category WHERE `categoryType` in ('speciality', 'location') AND categoryStatus = 1 order by `categoryOrder` asc";
    $result = $db->prepare($query);
    $result->execute();

    if ( $result->rowCount() > 0 ) {
        foreach ( $result->fetchAll() as $category_data ) {
            if ( $category_data['categoryType'] === 'speciality' ) {
                if ( $category_data['categoryFeatured'] == 1 ) {
                    $speciality_special_data[] = $category_data;
                } else {
                    $speciality_data[] = $category_data;
                }
            } elseif ( $category_data['categoryType'] === 'location' ) {
                $location_data[] = $category_data;
            }
        }
    }

} catch (Exception $Exception) {
    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
}
*/
?>
<header class="after-login header_3">
    <nav class="navbar navbar-default" id="navbar-main">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header top-logo col-lg-3 col-sm-12">
                    <button type="button" class="navbar-toggle  collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-logo" href="<?php echo HTTP_SERVER; ?>"><img src="<?php echo HTTP_SERVER; ?>img/logo.png"  width="250px"></a> </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse col-sm-12 col-lg-7 min-width-search" id="bs-example-navbar-collapse-1">
                    <?php /*
                    <form class="navbar-form col-sm-12 search-md-form-cs navbar-left" role="search" method="get" action="<?php echo HTTP_SERVER; ?>search.html" id="navbarSearchForm">
                        <div class="input-group col-sm-5  col-xs-12  hm-search"><span class="input-group-addon"><i class="fa  fa-user-md"></i></span>
                            <select data-placeholder="Select A Specialty" name="speciality" id="speciality_top" class="chosen-select form-control paitent-dropdown" tabindex="5">
                                <option value=""></option>
                                <?php if ( !empty( $speciality_special_data )): ?>
                                    <optgroup label="Popular Specialties">
                                        <?php
                                        foreach ( $speciality_special_data as $item ) {
                                            if ( $item['id'] == $search_speciality ) {
                                                $selected = 'selected="selected"';
                                            } else {
                                                $selected = "";
                                            }
                                            echo "<option class='fa fa-home' value='{$item['id']}' $selected> {$item['categoryName']}</option>";
                                        }
                                        ?>
                                    </optgroup>
                                <?php endif; ?>

                                <?php if ( !empty( $speciality_data )): ?>
                                    <optgroup label="All Specialties">
                                        <?php
                                        foreach ( $speciality_data as $item ) {
                                            if ( $item['id'] == $search_speciality ) {
                                                $selected = 'selected="selected"';
                                            } else {
                                                $selected = "";
                                            }
                                            echo "<option value='{$item['id']}' $selected>{$item['categoryName']}</option>";
                                        }
                                        ?>
                                    </optgroup>
                                <?php endif; ?>

                            </select>
                        </div>
                        <div class="input-group col-sm-5  col-xs-12  hm-search" style="z-index: 1 !important;"><span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                            <select data-placeholder="Select A State" name="location" id="location_top" class="chosen-select form-control paitent-dropdown" tabindex="5">
                                <option value=""></option>
                                <?php if ( !empty( $location_data )): ?>
                                    <?php
                                    foreach ( $location_data as $item ) {
                                        if ( $item['id'] == $search_location ) {
                                            $selected = 'selected="selected"';
                                        } else {
                                            $selected = "";
                                        }
                                        echo "<option value='{$item['id']}' $selected>{$item['categoryName']}</option>";
                                    }
                                    ?>
                                <?php endif; ?>

                            </select>
                        </div>
                        <button type="submit" class="btn btn-default col-md-1  col-sm-2 pull-right btn-search"><i class="fa fa-search-plus"></i></button>
                        <!-- <span class="search-reset btn" id="reset_search"><i class="fa fa-undo"></i></span> -->
                    </form> */ ?>

					<div class=" col-md-6 col-xs-12 afterloginmenuright  pull-right">
                    <ul class="nav navbar-nav navbar-right mb-full after-login-top-menu">
                        <li class=" hidden"><a href="<?php echo HTTP_SERVER; ?>">Home</a> </li>

                        <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">About   <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php /*?><li><a href="<?php echo HTTP_SERVER;?>how-it-works.html">How It Works</a> </li><?php */?>
                                <li><a href="<?php echo HTTP_SERVER;?>our-mission.html">Our Mission </a> </li>
                                <!--<li><a href="<?php echo HTTP_SERVER ?>partners.html">Our Partners</a></li>-->
                                <li><a href="#">Contact </a> </li>
                            </ul>

                        </li>
						<li ><a href="<?php echo HTTP_SERVER; ?>blog.html">Blog</a> </li>

                       <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">For Doctors   <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                               <li><a href="<?php echo HTTP_SERVER;?>doctor-login.html">Login</a> </li>
							
                                <li><a href="<?php echo HTTP_SERVER;?>our-platform.html">Our Platform</a> </li>
                                <li><a href="<?php echo HTTP_SERVER;?>request-a-demo.html">Request Demo</a> </li>
                            </ul>
                        </li>
                    	
                        <li class="alert-dropdown  hidden"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-gear"></i><span class="count">10</span></a>
                            <ul class="dropdown-menu">
                                <li><a href=""><i class="fa fa-envelope"></i> Massage Alert</a></li>
                                <li><a href=""><i class="fa fa-envelope"></i> Massage Alert</a></li>
                                <li><a href=""><i class="fa fa-envelope"></i> Massage Alert</a></li>
                                <li><a href=""><i class="fa fa-envelope"></i> Massage Alert</a></li>
                                <li><a href=""><i class="fa fa-envelope"></i> Massage Alert</a></li>
                            </ul>
                        </li>
                        <li class="alert-dropdown  hidden"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell"></i><span class="count">10</span></a>
                            <ul class="dropdown-menu">
                                <li><a href=""><i class="fa fa-envelope"></i> Massage Alert</a></li>
                                <li><a href=""><i class="fa fa-envelope"></i> Massage Alert</a></li>
                                <li><a href=""><i class="fa fa-envelope"></i> Massage Alert</a></li>
                                <li><a href=""><i class="fa fa-envelope"></i> Massage Alert</a></li>
                                <li><a href=""><i class="fa fa-envelope"></i> Massage Alert</a></li>
                            </ul>
                        </li>



                        <li class="dropdown user-login-btn">
                            <a href="#" class="dropdown-toggle btn btn-default btn-user" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                My Account <span><i class="fa active-setting fa-caret-down"></i></span>
                            </a>
                            <ul class="dropdown-menu user-login-menu">
                            	<?php if( $_SESSION["mvdoctorVisitorUserLevel"] == 4) { ?>
                            	<li><a href="<?php echo HTTP_SERVER; ?>patient.html"><span><i class="fa fa-user"></i></span> <?php echo $_SESSION["mvdoctorVisitorUserFname"] . ' ' . $_SESSION["mvdoctorVisitorUserLname"]; ?></a></li>
								<?php } elseif( $_SESSION["mvdoctorVisitorUserLevel"] == 5) { ?>
								<li><a href="<?php echo HTTP_SERVER; ?>doctor-dashboard.html"><span><i class="fa fa-user"></i></span> <?php echo $_SESSION["mvdoctorVisitorUserFname"] . ' ' . $_SESSION["mvdoctorVisitorUserLname"]; ?></a></li>
								
								<?php } ?>
								
								<?php if( $_SESSION["mvdoctorVisitorUserLevel"] == 4) { ?>
                            	<li><a href="<?php echo HTTP_SERVER; ?>patient/settings.html" class=""><span><i class="fa fa-gear"></i></span> Profile Setting</a></li>
								<?php } elseif( $_SESSION["mvdoctorVisitorUserLevel"] == 5) { ?>
							<li><a href="<?php echo HTTP_SERVER; ?>doctor-dashboard/setting.html" class=""><span><i class="fa fa-gear"></i></span> Profile Settings</a></li>
								<?php } ?>
								
                                <li><a href="<?php echo HTTP_SERVER; ?>logout.html" class=""><span><i class="fa fa-unlock-alt"></i></span> Log out</a></li>
                               
                            </ul>
                             <li class="hidden">
                                    <div class="navbar-login hiddden">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <p class="text-center">
                                                    <img src="<?php
                                                    if ( isset($_SESSION["mvdoctorVisitoruserPicture"]) && $_SESSION["mvdoctorVisitoruserPicture"] != '' ) {
                                                        echo HTTP_SERVER . 'images/user/thumb/' . $_SESSION["mvdoctorVisitoruserPicture"];
                                                    } else {
                                                        echo get_gravatar($_SESSION["mvdoctorVisitorUserEmail"],87);
                                                    } ?>" class="icon-size" />
                                                </p>
                                            </div>
                                            <div class="col-lg-8">
                                                <p class="text-left"><strong><?php echo $_SESSION["mvdoctorVisitorUserTitle"]; ?></strong></p>
                                                <p class="text-left small"><?php echo $_SESSION["mvdoctorVisitorUserEmail"]; ?></p>
                                                <p class="text-left">
                                                    <a href="<?php echo HTTP_SERVER; ?>patient/settings.html" class="btn btn-default btn-user btn-block btn-sm">Profile Setting</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider hidden"></li>
                                 <li class="hidden">
                                    <div class="navbar-login navbar-login-session">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p>
                                                    <a href="<?php echo HTTP_SERVER; ?>logout.html" class="btn btn-default dark-pink btn-block">Log out</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                        </li>
                    </ul>
                </div></div>
    </nav>
</header>
<script type="text/javascript">
    jQuery(function($){
        $('.gotoProfile').click(function(){
            var url = '<?php echo HTTP_SERVER ?>patient.html';
            window.location = url;
            return false;
        });
    });
</script>