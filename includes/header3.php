<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

//get category data
$speciality_data = array();
$speciality_special_data = array();
$location_data = array();

$search_speciality = "";
$search_location = "";
if ( isset($_GET['speciality']) && $_GET['speciality'] != '' ) {
    $search_speciality = intval( $_GET['speciality'] );
}

if ( isset($_GET['location']) && $_GET['location'] != '' ) {
    $search_location = intval( $_GET['location'] );
}
?>
<header class="header_3">
    <nav class="navbar navbar-default" id="navbar-main">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header top-logo col-lg-3 col-sm-12">
                    <button type="button" class="navbar-toggle  collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-logo" href="<?php echo HTTP_SERVER; ?>"><img src="<?php echo HTTP_SERVER; ?>img/logo.png"  width="250px"></a> </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse col-sm-12 col-lg-7 min-width-search" id="bs-example-navbar-collapse-1">
                     <div class="pull-right col-md-6 col-xs-12  header-3-nav">
                    <ul class="nav navbar-nav navbar-right after-login-top-menu mb-full">
                        <li><a href="<?php echo HTTP_SERVER; ?>">Home</a> </li>

                        <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">About   <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php /*?><li><a href="<?php echo HTTP_SERVER;?>how-it-works.html">How It Works</a> </li><?php */?>
                                <li><a href="<?php echo HTTP_SERVER;?>our-mission.html">Our Mission </a> </li>
                                <!--<li><a href="<?php echo HTTP_SERVER ?>partners.html">Our Partners</a></li>-->
                                <li><a href="#">Contact </a> </li>
                            </ul>

                        </li>

                        <li ><a href="<?php echo HTTP_SERVER; ?>blog.html">Blog</a> </li>

                        <?php /*?><li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dashboard  <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Login</a> </li>
                                <li><a href="#">Our Platform</a> </li>
                            </ul>
                        </li><?php */?>
                    
                    <li class="nav navbar-nav top-header navbar-right header-3-top-signup">
                    <button type="button" class="btn btn-info sing"  data-toggle="modal" data-target="#myModal">Sign Up / Log In</button>  </li>
                     </ul>
                </div>
    </nav>
</header>