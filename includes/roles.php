<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}

///* modules variables *///

//all modules
$moduleAll = 0;

if( isset($_SESSION["ensembleSessionUserLevel"]) ) {
	
	$sessionUserLevel = $_SESSION["ensembleSessionUserLevel"];
	$sqlAllowedLevels = "SELECT * FROM " . DB_PREFIX . "users_levels_functions WHERE levelId = ?";
	$result = $db->prepare($sqlAllowedLevels);
	$result->execute( array($sessionUserLevel) );

	foreach ( $result->fetchAll() as $rsAllowedLevels ) {
		$allowedFunctionModule = $rsAllowedLevels["functionModule"];
		$allowedFunctionValue = $rsAllowedLevels["functionValue"];
		${$allowedFunctionModule} = $allowedFunctionValue;
	}
}