<?php
/* -------------------------------------------------
// Filename: application_top.php
// Purpose: load necessary files and classes.
//----------------------------------------------- */

# PHP error reporting. supported values are given below.
# 0 - Turn off all error reporting
# 1 - Running errors
# 2 - Running errors + notices
# 3 - All errors except notices and warnings
# 4 - All errors except notices
# 5 - All errors

//deny direct access
if ( !defined('MVD_SITE') ) {
	die('You are not authorized to view this page');
}


define('CODE_DEBUG', '5');

//define host
$httpHost = $_SERVER['HTTP_HOST'];


//switch host
switch ($httpHost) {

	case 'www.myvirtualdoctor.com':
	case 'myvirtualdoctor.com':
		$siteStatus = 'myvirtualdoctor';
		$siteStatus = 'aws';
		break;

	case 'excesssol.com':
	case 'www.excesssol.com':
		$siteStatus = 'excesssol';
		break;
	case 'myvirtualdoctor.local':
		$siteStatus = 'myvirtualdoctor.local';
		break;
	case '198.57.243.182':
		$siteStatus = 'hostgator';
		break;
		
	case '52.87.205.22':
		$siteStatus = 'aws';
		break;	

	case 'therehabnetwork.com':
		$siteStatus = 'hostgator_rehab';
		break;

	default:
		$siteStatus = 'local';
		break;
}


//switch site status
switch ($siteStatus) {

	case 'myvirtualdoctor';

		//paths
		$httpServer = $_SERVER['REQUEST_SCHEME'] . '://'.$httpHost.'/';
		$httpServerHome = $_SERVER['REQUEST_SCHEME'] . '://'.$httpHost.'/';
		$siteRoot = '/home2/myvirtualdoc/public_html_old/';
		$siteRootHome = '/home2/myvirtualdoc/public_html/';
		//database
		$databaseServer = 'localhost';
		$databaseName = 'myvirtua_ldoctor';
		$databaseUser = 'myvirtua_ldoc';
		$databasePassword = '}t(e6I#mzo~[';
		break;

	case 'excesssol':

		//paths
		$httpServer = 'http://'.$httpHost.'/projects/arsalan/myvirtualdoctor/php/';
		$httpServerHome = 'http://'.$httpHost.'/projects/arsalan/myvirtualdoctor/php/';
		$siteRoot = '/home3/reaverte/public_html/projects/arsalan/myvirtualdoctor/php/';
		$siteRootHome = '/home3/reaverte/public_html/projects/arsalan/myvirtualdoctor/php/';
		//database
		$databaseServer = 'excesssol.com';
		$databaseName = 'reaverte_myvirtualdoctor';
		$databaseUser = 'reaverte_mvd';
		$databasePassword = 'SK098#';
		break;
	case 'hostgator':
		
		$protocol = 'http';
		if ($_SERVER['SERVER_PORT'] == 443) {
			$protocol = 'https';
		}

		//paths
		$httpServer = $protocol . '://'.$httpHost.'/~myvirtualdoc/';
		$httpServerHome = $protocol . '://'.$httpHost.'/~myvirtualdoc/';
		$siteRoot = '/home2/myvirtualdoc/public_html_old/';
		$siteRootHome = '/home2/myvirtualdoc/public_html_old/';
		//database
		$databaseServer = 'localhost';
		$databaseName = 'myvirtua_ldoctor';
		$databaseUser = 'myvirtua_ldoc';
		$databasePassword = '}t(e6I#mzo~[';
		break;	

	case 'hostgator_rehab':
		
		$protocol = 'http';
		if ($_SERVER['SERVER_PORT'] == 443) {
			$protocol = 'https';
		}

		//paths
		$httpServer = $protocol . '://'.$httpHost.'/dev/';
		$httpServerHome = $protocol . '://'.$httpHost.'/dev/';
		$siteRoot = '/home2/myvirtualdoc/public_html_old/therehabnetwork/dev/';
		$siteRootHome = '/home2/myvirtualdoc/public_html_old/therehabnetwork/dev/';
		//database
		$databaseServer = 'localhost';
		$databaseName = 'myvirtua_ldoctor';
		$databaseUser = 'myvirtua_ldoc';
		$databasePassword = '}t(e6I#mzo~[';
		break;	
		
	case 'aws':

		//paths
		$httpServer = $_SERVER['REQUEST_SCHEME'] . '://'.$httpHost.'/';
		$httpServerHome = $_SERVER['REQUEST_SCHEME'] . '://'.$httpHost.'/';
		$siteRoot = 'c:/wamp/www/';
		$siteRootHome = 'c:/wamp/www/';
		//database
		$databaseServer = 'localhost';
		$databaseName = 'myvirtua_ldoctor';
		$databaseUser = 'root';
		$databasePassword = '';
		break;	
	case 'myvirtualdoctor.local':

		//paths
		$httpServer = $_SERVER['REQUEST_SCHEME'] . '://'.$httpHost.'/';
		$httpServerHome = $_SERVER['REQUEST_SCHEME'] . '://'.$httpHost.'/';
		$siteRoot = '/var/www/myvirtualdoctor/public_html/';
		$siteRootHome = '/var/www/myvirtualdoctor/public_html/';
		//database
		$databaseServer = 'localhost';
		$databaseName = 'myvirtua_ldoctor';
		$databaseUser = 'myvirtua_ldoc';
		$databasePassword = '}t(e6I#mzo~[';
		break;		
	default:
		
		//paths
		$httpServer = 'http://homepage.800216101ba685ad079c.healthcareblocks.com/';
		$httpServerHome = 'http://homepage.800216101ba685ad079c.healthcareblocks.com/';
		$siteRoot = '/app/';
		$siteRootHome = '/app/';

		//database
		$databaseServer = 'localhost';
		$databaseName = 'myvirtualdoctor';
		$databaseUser = 'root';
		$databasePassword = 'myhome';
		
		break;
}

//var_dump(filter_input(INPUT_SERVER, 'SERVER_PORT'));
//var_dump($protocol);
//var_dump($httpServer);
//var_dump($httpServerHome);
//var_dump($siteRoot);
//var_dump($siteRootHome);
//phpinfo();
//die(__LINE__);

//Load Configuration
require($siteRoot . 'includes/config.php');
//require($siteRoot . 'includes/database.php');

// Set debug level from config file
switch(CODE_DEBUG) {
	case 0: error_reporting(0); break;
	case 1: error_reporting(E_ERROR | E_WARNING | E_PARSE); break;
	case 2: error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE); break;
	case 3: error_reporting(E_ALL ^ (E_NOTICE | E_WARNING)); break;
	case 4: error_reporting(E_ALL ^ E_NOTICE); break;
	case 5: error_reporting(E_ALL); break;
	default:
   error_reporting(E_ALL);
}

// Page Load Time
if(TEST_MODE) {
	$time = microtime();
	$time = explode(" ", $time);
	$time = $time[1] + $time[0];
	$start = $time;
}

// functions
require(SITE_ROOT . 'includes/functions.php');


require(SITE_ROOT . 'includes/database.php');

//If not generating a sitemap
if( !isset( $_GET["xmlVersion"] ) ) {
	// Grab page key for switch
	if ( isset( $_REQUEST["do"] ) ) {

		$do = htmlentities($_REQUEST["do"],ENT_QUOTES,'UTF-8');

		if ( $do == 'tmpls' || $do == 'event_json' ) {
			//do nothing
		} elseif( $do != '' && !file_exists(SITE_ROOT . 'pages/' . $do .'.php') ) {
			$do = "error404";
		}

	} else {
		$do = "home";
	}

	if ( $do == 'logout' ) {
		$page = "";
		if ( $_SESSION["mvdoctorVisitorUserLevel"] == 4 ) {
			$page = "<script>window.location='". HTTP_SERVER ."';</script>";
		}
		elseif ( $_SESSION["mvdoctorVisitorUserLevel"] == 5 ) {
			$page = "<script>window.location='". HTTP_SERVER ."doctor-login.html?msg=1';</script>";
		}
		session_destroy();

		echo $page;
		exit();
	}

	if ( $do == 'return-dashboard' ) {
		if( isset($_SESSION["mvdoctorVisitorUserLevel"]) ) {

			if ( $_SESSION["mvdoctorVisitorUserLevel"] == '4' ) {
				echo "<script>window.location='" .  HTTP_SERVER . "patient.html'</script>";
			}
			elseif ( $_SESSION["mvdoctorVisitorUserLevel"] == '5' ) {
				echo "<script>window.location='" .  HTTP_SERVER . "doctors.html'</script>";
			}
			else {
				echo "<script>window.location='" .  HTTP_SERVER . "authorizationerror.html'</script>";
			}

		}
		else {
			echo "<script>window.location='" .  HTTP_SERVER . "authorizationerror.html'</script>";
		}
	}

	if( isset( $_REQUEST["page"] ) ) {
		$sub_page = htmlentities($_REQUEST["page"],ENT_QUOTES,'UTF-8');
		if ( $do !== 'ajax' && !file_exists(SITE_ROOT . 'pages/modules/' . $do .'/'. $sub_page .'.php')){
			$do = "error404";
		}
	} else {
		$sub_page = "";
	}

	//get id from post slug
	if ( isset($_GET['id']) ) {
		$continue = false;
		if ( $do == 'blog') {
			$table = DB_PREFIX . 'health_tips';
			$continue = true;
		} elseif ( $do == 'doctor' ) {
			$continue = true;
			$table = DB_PREFIX . 'doctors';
		}

		if ( $continue ) {
			$query = 'SELECT id FROM `' . $table . '` WHERE url = ? LIMIT 1';
			$st = $db->prepare($query);
			$st->execute( array( $_GET['id'] ) );
			if ( $st->rowCount() > 0 ) {
				$st = $st->fetch();
				$_GET['id'] = $st['id'];
			} else {
				$do = "error404";
			}
		}
		$continue = null;
	}
	
	
	// sessions file
	require(SITE_ROOT . 'includes/sessions.php');

	//pagination gile
	require(SITE_ROOT . 'includes/pagination.php');

	require_once( SITE_ROOT . 'includes/libs/Facebook/autoload.php' );

}

	
	
//exploding
$doExplode = explode("_",$do);

if ( !function_exists( 'password_verify' ) ) {
	require_once (SITE_ROOT . 'includes/pass_lib.php');
}

//html meta for SEO
require_once 'meta.php';
