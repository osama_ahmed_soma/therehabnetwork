	<?php
//deny direct access
if ( !defined('MVD_SITE') ) {
    die('You are not authorized to view this page');
}

if ( !isset($_SESSION['mvdoctorVisitornUserId']) ) {
if ( !isset($fb) ) {
    $fb_config = [
        'app_id' => '558726107616262',
        'app_secret' => '4c571b508a5e25aa5c81031748e82426',
        'default_graph_version' => 'v2.2',
    ];
    $fb = new Facebook\Facebook( $fb_config );
}

$helper = $fb->getRedirectLoginHelper();
$permissions = ['email']; // Optional permissions
$loginUrl = $helper->getLoginUrl( HTTP_SERVER . 'facebook.html', $permissions);
/*
?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog  first-dialog" role="document">
        <div class="account-wall">
            <div class="auth-dialog login-dialog">
                <div class="bbm-modal__topbar">
                    <h3>Log In</h3>
                    <a href="#" class="external close-link" data-dismiss="modal"><i class="fa  fa-times-circle"></i></a>
                </div>
                <div class="bbm-modal__section">
                    <div class="centered-mobile">
                        <div class="content">
                            <p class="text-center ">Welcome back!</p>
                            <a class="method method-button method-button-facebook  hidden" data-method="Facebook" href="<?php echo htmlspecialchars($loginUrl); ?>">
                                <i class="fa fa-facebook"></i> Log In with Facebook
                            </a>
                            <div class="or-divider hidden"><span>OR</span></div>
                            <div class="collapse" id="collapseExample">
                                <div class="">
                                    <div id="login_error12"></div>
                                    <form method="post" action="<?php echo HTTP_SERVER; ?>ajax/login.html" id="login_form">
                                        <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                                        <fieldset class="pure-group">

                                            <div chiro="component:EmailField">
                                                <input type="text" name="userEmail" class="field email-field pure-input-1" placeholder="Email Address">
                                                <div class="error-message"></div>
                                            </div>

                                            <div chiro="component:PasswordField">
                                                <div class="show-password-container">
                                                    <span class="show-password" style="display:none;">SHOW</span>
                                                    <input type="password" name="loginPassword" class="password-field field pure-input-1" placeholder="Password">
                                                </div>

                                                <div class="password-meter" style="display: block;">
                                                    <div class="progress">
                                                        <div class="progress-bar complexity-bar"></div>
                                                    </div>

                                                    <div class="error-message error-message-password-signup"></div>

                                                </div>
                                                <div class="no-password-meter" style="display: none;">
                                                    <div class="error-message"></div>
                                                </div>
                                            </div>

                                            <div chiro="component:SubmitButton">
                                                <button type="submit" class="btn btn-default btn-block">Submit</button>
                                            </div>

                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                            <a class="method method-button method-button-email" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" data-method="Email" href="" >
                                <i class="fa fa-envelope"></i> Log In with Email
                            </a>


                        </div>
                        <div class="forgot-password-container">
                            <a class="forgot-password" data-toggle="modal" data-dismiss="modal" href="#forget">Forgot password?</a>
                        </div>
                    </div>
                </div>
                <div class="bbm-modal__bottombar">
                    New to My Virtual Doctor?   <a  data-toggle="modal" data-dismiss="modal"  href="#signup" class="signup">Create a free account now</a>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="forget" tabindex="-1" role="dialog" aria-labelledby="forgetLabel">
    <div class="modal-dialog secound-dialog" role="document">
        <div class="account-wall">
            <div class="auth-dialog login-dialog">
                <div class="bbm-modal__topbar">
                    <h3>Reset password</h3>
                    <a href="#" class="external close-link" data-dismiss="modal"><i class="fa  fa-times-circle"></i></a>
                </div>
                <div class="bbm-modal__section">
                    <div class="pure-g">
                        <div class="pure-u-1 content">
                            <p class="intro pure-hidden-mobile">Hey, we all forget things sometimes. Let’s take care of this for you.</p>
                            <p class="pure-hidden-mobile">Enter your email address below and we’ll send you instructions on how to change your password.</p>
                            <div id="password_reset_error"></div>
                            <form class="pure-form" method="post" id="reset_password">
                                <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                                <fieldset>
                                    <div><div chiro="component:EmailField">
                                            <input type="text" name="emailAddress" class="field email-field pure-input-1" placeholder="Email Address">
                                            <div class="error-message"></div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="callout-button">
                                    <button type="submit" class="pure-button callout-button submit">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="auth-done" style="display:none">
                        An email has been sent with a link to reset your password. If you are still having issues logging in, please <a class="contact external" href="mailto:support@betterdoctor.com">contact us</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="forgetLabel">
    <div class="modal-dialog  singup-dialog" role="document" >
        <div class="account-wall  sing-main">
            <div class="auth-dialog login-dialog">
                <div class="bbm-modal__topbar">
                    <h3> Let’s Get Started - Create An Account</h3>
                    <a href="#" class="external close-link" data-dismiss="modal"><i class="fa  fa-times-circle"></i></a>
                    
                </div>
                <div class="bbm-modal__section  sing-withd">
                    <div class="pure-g">
                        <div class="pure-u-1 content"><div><div>
                                    <p class="intro pure-hidden-desktop">Finding the best telehealth care has never been easier. Join us today. It's FREE!</p>
                                    <!--
                                    <a class="method method-button method-button-facebook" data-method="Facebook" href="<?php echo htmlspecialchars($loginUrl); ?>">
                                        <i class="fa fa-facebook"></i> Signup with Facebook
                                    </a>

                                    <div class="or-divider"><span class="signup-color">OR</span></div>
                                    -->
                                    <div class="collapse" id="collapseExample2">
                                        <div id="reigster_form_error"></div>
                                        <form method="post" action="#" id="reigster_form">
                                            <input type="hidden" name="token" value="<?php echo getToken(); ?>">
                                            <fieldset class="pure-group">
                                                <div><div chiro="component:EmailField">
                                                        <input type="text" name="emailAddress" class="field email-field pure-input-1" placeholder="Email Address">
                                                        <div class="error-message"></div>
                                                    </div>
                                                </div>

                                                <div chiro="component:PasswordField">

                                                    <div class="show-password-container">
                                                        <span class="show-password" style="display:none;">SHOW</span>
                                                        <input type="password" name="password" class="password-field field pure-input-1" placeholder="Password">
                                                    </div>

                                                    <div class="password-meter" style="display: block;">
                                                        <div class="progress">
                                                            <div class="progress-bar complexity-bar"></div>
                                                        </div>

                                                        <div class="error-message error-message-password-signup"></div>

                                                    </div>
                                                    <div class="no-password-meter" style="display: none;">
                                                        <div class="error-message"></div>
                                                    </div>

                                                </div>

                                                <div chiro="component:SubmitButton">
                                                    <button type="submit" class="btn btn-default btn-block">Submit</button>
                                                </div>


                                            </fieldset>
                                        </form>
                                    </div>
                                    <a class="method method-button method-button-email" data-toggle="collapse" href="#collapseExample2" aria-expanded="false" aria-controls="collapseExample" data-method="Email" href="" >
                                        <i class="fa fa-envelope"></i> Signup with Email
                                    </a>

                                    <p class="disclaimer">By signing up, I agree to My Virtual Doctor <a class="external" href="/terms" target="_blank">Terms of Use</a> and <a class="external" href="/privacy" target="_blank">Privacy Policy</a></p>
                                    <p class="already-account">Already have an account? <a class="login"  data-toggle="modal" data-dismiss="modal" href="#myModal">Log In Here   </a> <br /> Doctor <a class="login" href="http://www.excesssol.com/projects/arsalan/myvirtualdoctor/php/doctor-login.html">Sign in</a></p>
                                    
                                    
                                    <p class="intro pure-hidden-mobile"></p>
                                    <p class="pure-hidden-mobile"></p>

                                </div>
                            </div></div>
                    </div>
                    <div class="auth-done" style="display:none">
                        An email has been sent with a link to reset your password. If you are still having issues logging in, please <a class="contact external" href="mailto:support@betterdoctor.com">contact us</a>
                    </div>
                </div>
				<div class="sing-up-right">
                        <h1>Your Choice. Your Health. Your Way.</h1>
                        <p>Your search for the best telehealth physician ends here. Create a free account to start managing your healthcare the way you prefer. Easily find the best doctors in the directory, schedule online appointments, pin  your favorite healthcare info, and more. </p>
                    </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    jQuery(function($){
        //login form
        $('#login_form').submit(function(){
            var th = $(this);
            var data = $( "#login_form" ).serializeArray();

            th.find('button').attr('disabled', true);

            $.ajax({

                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=login",
                data:data,
                method:"POST",
                dataType:'json'

            }).error(function () {

                th.find('button').removeAttr('disabled');

            }).done(function () {

                th.find('button').removeAttr('disabled');

            }).success(function (data) {
                console.log(data);
                //data = JQuery.parseJSON(data);
                if ( data.success ) {
                    window.location = '<?php echo HTTP_SERVER; ?>patient.html';
                }

                if ( data.error ) {
                    var str = "";
                    $.each(data.error, function(idx, error){
                        str += '<div class="alert alert-danger alert-dismissible" role="alert">\<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\<strong>Error!</strong> ' + error + '</div>';
                    });
                    $('#login_error12').html(str);
                }

            }, 'json');

            return false;
        });

        //reset password form
        $('#reset_password').submit(function(){
            var th = $(this);
            var data = $( "#reset_password" ).serializeArray();

            th.find('button').attr('disabled', true);

            $.ajax({

                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=forgot_password",
                data:data,
                method:"POST",
                dataType:"json"

            }).error(function () {

                th.find('button').removeAttr('disabled');

            }).done(function () {

                th.find('button').removeAttr('disabled');

            }).success(function (data) {
                //data = JQuery.parseJSON(data);
                if ( data.success ) {
                    var str = "";
                    str += '<div class="alert alert-success alert-dismissible" role="alert">\<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\<strong>Success!</strong> ' + data.success + '</div>';
                    $('#password_reset_error').html(str);
                }

                if ( data.error ) {
                    var str = "";
                    $.each(data.error, function(idx, error){
                        str += '<div class="alert alert-danger alert-dismissible" role="alert">\<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\<strong>Error!</strong> ' + error + '</div>';
                    });
                    $('#password_reset_error').html(str);
                }

            }, 'json');

            return false;
        });

        //reigster_form
        $('#reigster_form').submit(function(){
            var th = $(this);
            var data = $( "#reigster_form" ).serializeArray();

            th.find('button').attr('disabled', true);

            $.ajax({

                url: "<?php echo HTTP_SERVER; ?>index.php?do=ajax&page=register_with_email",
                data: data,
                method:"POST",
                dataType:"json"

            }).error(function () {

                th.find('button').removeAttr('disabled');

            }).done(function () {

                th.find('button').removeAttr('disabled');

            }).success(function (data) {
                //data = JQuery.parseJSON(data);
                if ( data.success ) {
                    var str = "";
                    str += '<div class="alert alert-success alert-dismissible" role="alert">\<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\<strong>Success!</strong> ' + data.success + '</div>';
                    $('#reigster_form_error').html(str);
                    setTimeout(function(){
                        window.location = '<?php echo HTTP_SERVER; ?>patient/profile-settings.html';
                    }, 3000);
                }

                if ( data.error ) {
                    var str = "";
                    $.each(data.error, function(idx, error){
                        str += '<div class="alert alert-danger alert-dismissible" role="alert">\<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\<strong>Error!</strong> ' + error + '</div>';
                    });
                    $('#reigster_form_error').html(str);
                }

            }, 'json');

            return false;
        });
    });
</script>

<?php */ } ?>

<?php if ( ! ($do == 'partners') ): ?>

<!--<div class="footer">
    <div class="container">
        <h1 class="Doctor-heading">Are you a Doctor?</h1>
        <p class="Doctor-pra"> Find the best virtual doctor today. It’s free and easy.</p>
        <a href="#"><div class="Know-but"> Sign up </div></a>
    </div>
</div>-->
<?php
    /*
<div class="footer">
    <div class="container">
        <div class="col-md-8">
        	<h1>Your Choice.  Your Practice. Your Way.</h1>
            <h4>Telehealth software designed to save you valuable time and increase revenues. </h4>
            <p style=" font-size: 17px;line-height: 29px; margin-left: 0px;"><i class="fa fa-user-plus"></i> More than 100 doctors online right now.<br  /> <i class="fa fa-leaf"></i> Signup instantly, get help and get healthy!</p>
        </div>
        <div class="col-md-4 ft-form-sec">
        	<input type="text" class="form-control" placeholder="Full Name">
            <input type="text" class="form-control" placeholder="Email Address">
            <div class="input-group">
                <div class="checkbox">
                    <label>
                        <input id="login-remember" type="checkbox" name="remember" value="1"> I agree with <a href="#">Terms & Conditions.</a>
                    </label>
                </div>
            </div>
            <input type="submit" class="btn btn-info btn-block" value="Submit">
        </div>
    </div>
</div>*/
endif; ?>


<div class="footer-bottem">
    <div class="container">
       <div class="col-md-12 col-sm-12">
            <div class="col-md-3 col-sm-3">
                <div class="footer-nav">
                	<h3>About</h3>
                    <ul>
                        <li><a href="<?php echo HTTP_SERVER;?>our-mission.html">Our Mission</a></li>
                        <li><a href="<?php echo HTTP_SERVER;?>privacypolicy.html">Privacy Policy</a></li>
                        <li><a href="<?php echo HTTP_SERVER;?>termandconditions.html">Terms and Conditions</a></li>
                        <li> <a href="<?php echo HTTP_SERVER; ?>contact-us.html">Contact  </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="footer-nav ">
               	 <h3>For Patients</h3>
                    <ul>
                        <li><a href="<?php echo HTTP_SERVER . 'login.html'; ?>">Log in</a></li>
                        <li><a href="<?php echo HTTP_SERVER . 'signup.html'; ?>">Sign up</a></li>
                        <li><a href="<?php echo HTTP_SERVER; ?>blog.html">Blog</a></li>
                        <li><a href="<?php echo HTTP_SERVER; ?>faqs.html">FAQs</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="footer-nav ">
                <h3>For Doctors</h3>
                    <ul>
                        <li><a href="<?php echo HTTP_SERVER;?>app/login"> Log in</a></li>
                        <li><a href="<?php echo HTTP_SERVER;?>app/register/doctor">Sign up</a></li>
                        <li><a href="<?php echo HTTP_SERVER; ?>blog.html">Blog</a></li>
                        <li><a href="<?php echo HTTP_SERVER . 'providers.html'; ?>">Our Platform</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="footer-nav ">
                 <h3>Connect with Us</h3>
                   <ul style="margin-top: 14px;margin-bottom: 8px;"> 
                       <i class="fa fa-twitter"></i>
                       <i class="fa fa-facebook-f"></i>
                       <i class="fa fa-linkedin"></i>
                       <i class="fa fa-pinterest-p"></i>                    
                   </ul>
            	</div>
                <div class="footer-nav footer-logo-area">
                   <ul>
                      <li><a href="<?php echo HTTP_SERVER; ?>"><img src="<?php echo HTTP_SERVER; ?>img/logo.png"  class="img-responsive" width="180px"></a></li>
                   </ul>
                </div>
        </div>
        
         </div>
  </div>
        

  <div class="footer-bottem copyright-section ">
   
         <div class="col-md-12 col-sm-12 copyright new">
            <p> <?php echo date('Y'); ?> My Virtual Doctor. All Right Reserved.  <span>Made In Florida <img src="<?php echo HTTP_SERVER; ?>/img/mp.png" /></span>  </p>
        </div>
    
</div>

<script>
/*! Main */
jQuery(document).ready(function($) {
  
		var num = 95; //number of pixels before modifying styles
		$(window).bind('scroll', function () {
			if ($(window).scrollTop() > num) {
				$('#navbar-main').addClass('fixed');
			} else {
				$('#navbar-main').removeClass('fixed');
			}
		});
});


</script>