<?php

$SEO_META_TITLE = "";
$SEO_META_DESC = "";
$SEO_META_IMAGE = "";
$SEO_META_IMAGE_HEIGHT = "";
$SEO_META_IMAGE_WEIGHT = "";

//get default seo data for blog
try {
    $query = "select * from `" . DB_PREFIX ."seo` where page_slug = ? LIMIT 1";
    $st = $db->prepare($query);
    $st->execute( array($do) );

    if ( $st->rowCount() ) {
        $seo_data = $st->fetch();
        $seo_data['image'] = unserialize(base64_decode($seo_data['image']));
    } else {
        $seo_data = array();
    }

} catch (Exception $Exception) {
    exit("DataBase Error {$Exception->getCode()}:" . $Exception->getMessage());
}

if ( $do == 'blog'  ):

    if ( !empty( $_GET['id'] ) ) {

        try {
            $query = "select title, seo_title, excerpt, seo_meta_desc, image  from `" . DB_PREFIX ."health_tips` where id = ? LIMIT 1";
            $result = $db->prepare($query);
            $result->execute( array( $_GET['id']  ));

            if ($result->rowCount() > 0) {
                $data = $result->fetch();
                $data['image'] = unserialize(base64_decode($data['image']));

                //Get SEO TITLE
                if (trim($data['seo_title']) != '') {
                    $SEO_META_TITLE = sanitize($data['seo_title'], 3);
                } else if (trim($data['title']) != '') {
                    $SEO_META_TITLE = sanitize($data['title'], 3);
                } else if( !empty($seo_data) && $seo_data['title'] != '') {
                    //get default seo title
                    $SEO_META_TITLE = $seo_data['title'];
                }

                //GET SEO Description
                if (!empty($data['seo_meta_desc'])) {
                    $SEO_META_DESC = sanitize($data['seo_meta_desc'], 3);

                } else if (!empty($data['excerpt'])) {
                    $SEO_META_DESC = sanitize($data['excerpt'], 3);
                } else if( !empty($seo_data) && $seo_data['meta_desc'] != '') {
                    //get default seo description
                    $SEO_META_DESC = sanitize( $seo_data['meta_desc'], 3 );
                }

                //get feature image
                if ( is_array($data['image']) &&  array_key_exists('img1', $data['image']) && $data['image']['img1'] != '' ) {
                    $featured_image = $data['image']['img1'];
                } else if ( is_array($seo_data['image']) && array_key_exists('img1', $seo_data['image']) && $seo_data['image']['img1'] != '' ) {
                    $featured_image = $seo_data['image']['img1'];
                } else {
                    $featured_image = '';
                }

                if ( $featured_image != '' ) {
                    $featured_image_path = SITE_ROOT . 'images/health_tips/full/' . $featured_image;
                    $featured_image_url = HTTP_SERVER . 'images/health_tips/full/' . $featured_image;

                    if ( file_exists( $featured_image_path ) ) {
                        list($featured_image_width, $featured_image_height) = getimagesize($featured_image_path);
                        $SEO_META_IMAGE = $featured_image_url;
                        $SEO_META_IMAGE_HEIGHT = $featured_image_height;
                        $SEO_META_IMAGE_WEIGHT = $featured_image_width;

                        unset($featured_image_url, $featured_image_height, $featured_image_width);

                    }
                }

                unset($featured_image, $data);
            }

        } catch (Exception $Exception) {
            exit("DataBase Error {$Exception->getCode()}:" . $Exception->getMessage());
        }

    } else {
        //get default value for seo
        if( !empty($seo_data) && $seo_data['title'] != '') {
            $SEO_META_TITLE = sanitize( $seo_data['title'], 3);
        }

        if( !empty($seo_data) && $seo_data['meta_desc'] != '') {
            //get default seo description
            $SEO_META_DESC = sanitize( $seo_data['meta_desc'], 3 );
        }

        if ( is_array($seo_data['image']) && array_key_exists('img1', $seo_data['image']) && $seo_data['image']['img1'] != '' ) {
            $featured_image = $seo_data['image']['img1'];

            if ( $featured_image != '' ) {
                $featured_image_path = SITE_ROOT . 'images/health_tips/full/' . $featured_image;
                $featured_image_url = HTTP_SERVER . 'images/health_tips/full/' . $featured_image;

                if ( file_exists( $featured_image_path ) ) {
                    list($featured_image_width, $featured_image_height) = getimagesize($featured_image_path);
                    $SEO_META_IMAGE = $featured_image_url;
                    $SEO_META_IMAGE_HEIGHT = $featured_image_height;
                    $SEO_META_IMAGE_WEIGHT = $featured_image_width;

                    unset($featured_image_url, $featured_image_height, $featured_image_width);
                }
            }

            unset($featured_image);

        }
    }


elseif ( $do == 'doctor' && !empty( $_GET['id'] ) ):

    //get doctor data
    try {
        $query = "SELECT doctor_name, seo_title, seo_meta_desc, featured_image, ratings, experience, overview  FROM `" . DB_PREFIX . "doctors` WHERE id = ? LIMIT 1";
        $result = $db->prepare($query);
        $result->execute( array( $_GET['id'] ) );

        if ( $result->rowCount() > 0 ) {
            $data = $result->fetch();

            if ( $data['ratings'] < 1 ) {
                $data['ratings'] = 0;
            }


            //first check for seo title
            if (trim($data['seo_title']) != '') {
                $SEO_META_TITLE = sanitize($data['seo_title'], 3);
            } else if (trim($data['doctor_name']) != '') {
                $SEO_META_TITLE = sanitize($data['doctor_name'], 3);
            } else if( !empty($seo_data) && $seo_data['title'] != '') {
                //get default seo title
                $SEO_META_TITLE = sanitize( $seo_data['title'], 3);
            }

            //GET SEO Description
            if ( trim($data['seo_meta_desc']) != '') {
                $SEO_META_DESC = sanitize($data['seo_meta_desc'], 3);

            } elseif ( $data['overview'] != '' ) {
                $SEO_META_DESC = trim_words($data['overview'], 55);

            } else if( !empty($seo_data) && $seo_data['meta_desc'] != '') {
                //get default seo description
                $SEO_META_DESC = sanitize( $seo_data['meta_desc'], 3);
            }

            //get feature image
            if ( $data['featured_image'] != '' ) {
                $featured_image = 'images/doctors/full/' . $data['featured_image'];
            } else if ( is_array($seo_data['image']) && array_key_exists('img1', $seo_data['image']) && $seo_data['image']['img1'] != '' ) {
                $featured_image = 'images/health_tips/full/' .$seo_data['image']['img1'];
            } else {
                $featured_image = '';
            }

            if ( $featured_image != '' ) {
                $featured_image_path = SITE_ROOT . $featured_image;
                $featured_image_url = HTTP_SERVER . $featured_image;

                if ( file_exists( $featured_image_path ) ) {
                    list($featured_image_width, $featured_image_height) = getimagesize($featured_image_path);
                    $SEO_META_IMAGE = $featured_image_url;
                    $SEO_META_IMAGE_HEIGHT = $featured_image_height;
                    $SEO_META_IMAGE_WEIGHT = $featured_image_width;

                    unset($featured_image_url, $featured_image_height, $featured_image_width);
                }
            }

            unset($featured_image);

            //check for categories used or not
            $search_terms1 = array (
                '[speciality]',
                '[location]',
                '[education]'
            );
            $term_exist = false;
            foreach ( $search_terms1 as $search_term ) {

                if (strpos($SEO_META_TITLE, $search_term) !== false) {
                    $term_exist = true;
                    break;
                }

                if (strpos($SEO_META_DESC, $search_term) !== false) {
                    $term_exist = true;
                    break;
                }

            }

            if ( $term_exist ) {
                //get category data
                try {
                    $query = "SELECT ct.categoryName, ct.categoryType FROM `" . DB_PREFIX . "category` as ct, `" . DB_PREFIX . "category_data` as ct_data  WHERE ct.categoryType in ('speciality', 'location', 'education') AND ct.categoryStatus = 1 AND ct_data.postId = ? AND ct_data.postType='doctors' AND ct.id = ct_data.categoryId order by ct.categoryOrder asc";
                    $result = $db->prepare($query);
                    $result->execute( array( $_GET['id'] ) );

                    if ( $result->rowCount() > 0 ) {
                        foreach ( $result->fetchAll() as $category_data ) {
                            if ( $category_data['categoryType'] === 'speciality' ) {
                                $speciality_data[] = $category_data;
                            } elseif ( $category_data['categoryType'] === 'location' ) {
                                $location_data[] = $category_data;
                            } elseif ( $category_data['categoryType'] === 'education' ) {
                                $education_data[] = $category_data;
                            }
                        }

                        $search_terms_data['speciality'] = sanitize( implode(', ', $speciality_data), 3);
                        $search_terms_data['location'] = sanitize( implode(', ', $location_data), 3);
                        $search_terms_data['education'] = sanitize( implode(', ', $education_data), 3);

                        foreach ( $search_terms_data as $search_term => $value ) {
                            $SEO_META_TITLE = str_replace('[' . $search_term . ']', $value, $SEO_META_TITLE);
                            $SEO_META_DESC = str_replace('[' . $search_term . ']', $value, $SEO_META_DESC);
                        }
                    }

                    unset($search_terms1, $search_terms_data, $speciality_data, $location_data, $education_data);

                } catch (Exception $Exception) {
                    exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
                }
            }

            $search_terms2 = array (
                'experience' => sanitize( $data['experience'], 3),
                'user_rating' => sanitize( $data['ratings'], 3)
            );

            foreach ( $search_terms2 as $search_term => $value ) {
                $SEO_META_TITLE = str_replace('[' . $search_term . ']', $value, $SEO_META_TITLE);
                $SEO_META_DESC = str_replace('[' . $search_term . ']', $value, $SEO_META_DESC);
            }

            unset($search_terms2);
        }

    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }

elseif ( $do == 'search' ):

    try {

        //get default value for seo
        if( !empty($seo_data) && $seo_data['title'] != '') {
            $SEO_META_TITLE = sanitize( $seo_data['title'], 3 );
        }

        if( !empty($seo_data) && $seo_data['meta_desc'] != '') {
            //get default seo description
            $SEO_META_DESC = sanitize( $seo_data['meta_desc'], 3 );
        }

        if ( is_array($seo_data['image']) && array_key_exists('img1', $seo_data['image']) && $seo_data['image']['img1'] != '' ) {
            $featured_image = $seo_data['image']['img1'];



            if ( $featured_image != '' ) {
                $featured_image_path = SITE_ROOT . 'images/health_tips/full/' . $featured_image;
                $featured_image_url = HTTP_SERVER . 'images/health_tips/full/' . $featured_image;

                if ( file_exists( $featured_image_path ) ) {
                    list($featured_image_width, $featured_image_height) = getimagesize($featured_image_path);
                    $SEO_META_IMAGE = $featured_image_url;
                    $SEO_META_IMAGE_HEIGHT = $featured_image_height;
                    $SEO_META_IMAGE_WEIGHT = $featured_image_width;

                    unset($featured_image_url, $featured_image_height, $featured_image_width);
                }
            }
            unset($featured_image);

        }



        if ( isset($_GET['speciality']) && $_GET['speciality'] != '' ) {
            $speciality_id = intval( $_GET['speciality'] );
            $query = "SELECT categoryName FROM `" . DB_PREFIX . "category` WHERE id = ? LIMIT 1";
            $result = $db->prepare($query);
            $result->execute( array( $speciality_id ) );

            if ( $result->rowCount() ) {
                $speciality_name = $result->fetch();
                $speciality_name = sanitize( $speciality_name['categoryName'], 3);

                $SEO_META_TITLE = str_replace('[speciality]', $speciality_name, $SEO_META_TITLE);
                $SEO_META_DESC = str_replace('[speciality]', $speciality_name, $SEO_META_DESC);

                unset($speciality_name);
            }

            unset($speciality_id, $result);
        }

    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }


    try {
        if ( isset($_GET['location']) && $_GET['location'] != '' ) {
            $location_id = intval( $_GET['location'] );
            $query = "SELECT categoryName FROM `" . DB_PREFIX . "category` WHERE id = ? LIMIT 1";
            $result = $db->prepare($query);
            $result->execute( array( $location_id ) );

            if ( $result->rowCount() ) {
                $speciality_name = $result->fetch();
                $speciality_name = sanitize( $speciality_name['categoryName'], 3);

                $SEO_META_TITLE = str_replace('[location]', $speciality_name, $SEO_META_TITLE);
                $SEO_META_DESC = str_replace('[location]', $speciality_name, $SEO_META_DESC);

                unset($speciality_name);
            }

            unset($location_id, $result);
        }

    } catch (Exception $Exception) {
        exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
    }


elseif ( !empty($seo_data) ):
        try {
            //get default value for seo
            if( !empty($seo_data) && $seo_data['title'] != '') {
                $SEO_META_TITLE = sanitize( $seo_data['title'], 3 );
            }

            if( !empty($seo_data) && $seo_data['meta_desc'] != '') {
                //get default seo description
                $SEO_META_DESC = sanitize( $seo_data['meta_desc'], 3 );
            }

            if ( is_array($seo_data['image']) && array_key_exists('img1', $seo_data['image']) && $seo_data['image']['img1'] != '' ) {
                $featured_image = $seo_data['image']['img1'];



                if ( $featured_image != '' ) {
                    $featured_image_path = SITE_ROOT . 'images/health_tips/full/' . $featured_image;
                    $featured_image_url = HTTP_SERVER . 'images/health_tips/full/' . $featured_image;

                    if ( file_exists( $featured_image_path ) ) {
                        list($featured_image_width, $featured_image_height) = getimagesize($featured_image_path);
                        $SEO_META_IMAGE = $featured_image_url;
                        $SEO_META_IMAGE_HEIGHT = $featured_image_height;
                        $SEO_META_IMAGE_WEIGHT = $featured_image_width;

                        unset($featured_image_url, $featured_image_height, $featured_image_width);
                    }
                }
                unset($featured_image);

            }
        } catch (Exception $Exception) {
            exit( "DataBase Error {$Exception->getCode()}:". $Exception->getMessage( ) );
        }

else:
    //set all default value here
    $SEO_META_TITLE = SITE_NAME;



endif;

unset($seo_data);
