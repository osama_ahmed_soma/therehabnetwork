<header class="header">
    <div class="top_header">
        <div class="container">
            <div class="col-sm-6">
                <div class="row">
                    <ul>
                        <li><a href="#"> <i class="fa fa-envelope-o"></i> Info@therehab.com</a></li>
                        <li class="dark"><a href="#"> <i class="fa fa-phone"></i> 888.958.2885</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 no-pad">
                <div class="row">
                    <ul class="right">
                        <li class="active"><a href="#"> <i class="fa fa-lock"></i>  LOG IN</a></li>
                        <li><a href="#"> Enterprise</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="main_header">
        <div class="container">
            <div class="logo"><a href="#"> <img src="images/logo.jpg" alt="" /></a></div>
            <div class="nav">
                <ul>
                    <li><a href="#"> For Providers </a> </li>
                    <li><a href="#"> For Consumers</a> </li>
                    <li class="active"><a href="#">Find a Provider</a> </li>
                    <li><a href="#"> Enterprise</a> </li>
                    <li><a href="#"> Pricing</a> </li>
                </ul>
            </div>
        </div>
    </div>
</header>